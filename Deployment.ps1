param(
$servers,
$environment,
$configType
)

############################################################################################################################################################
#
# Start with Web-deploy function
#
############################################################################################################################################################
function web-deploy($servers, $jsonpath="$env:CI_PROJECT_DIR/definition.json",$environment) {

$main=(Get-Content $jsonpath|ConvertFrom-Json)


#getting details from JSON

$sites=@()
$data=@()
($data+=$main.Deploy.ENV.$environment.VirtualSites).foreach{


$sites+=$_.PSobject.properties.Name

}
   if (!($sites ))
	{
		$sites=$null
	}
   else{
            #Do Nothing
		}
$sitescount=$sites.count
$appPools=@()
$data=@()
($data+=$main.Deploy.ENV.$environment.AppPools).foreach{

$appPools+=$_.PSobject.properties.Name

}
if (!($appPools ))
		{
			$appPools=$null
		}
		else{
            #Do Nothing 
		}
$appPoolscount=$appPools.count
$websites=@()
$data=@()
($data+=$main.Deploy.ENV.$environment.sites).foreach{

$websites+=$_.PSobject.properties.Name

}
if (!($websites ))
		{
			$websites=$null
		}
		else{
            # Do Nothing 
		}
$websitescount=$websites.Count
$user="$env:username"
$SVCPWD="$env:password"
$PWd=ConvertTo-SecureString -String $SVCPWD -AsPlainText -Force
$cred=New-Object -TypeName System.Management.Automation.PSCredential $user, $PWd


# check virtul sites (components) are present then initate deployment 
if($sitescount -ne "0")
{
foreach($item in $sites)
{

foreach($srvr in $servers){
$v=@()
($v+=$main.Deploy.ENV.$environment.VirtualSites)|Where-Object{$_.name -eq $item}

    $compName=$v.$item.name
    
    $SiteName=$v.$item.Sitename
    
    $AppPool=$v.$item.AppPool
    

if($srvr -ne "" -and (Test-Connection -cn $srvr -BufferSize 16 -Count 1 -ea 0 -quiet))
{
    Write-Host "**************Deployment workflow started in $srvr****************" 
    
    net use \\$srvr /user:$user $SVCPWD

    if(!([string]::IsNullOrEmpty($AppPool)) -or !([string]::IsNullOrEmpty($SiteName)) -or !([string]::IsNullOrEmpty($compName)))
                    {                                                
                        Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock{iisreset /stop}
                    }  

# Copy zip code    

    If ( Test-Path "\\$srvr\C$\temp\$item.zip" ){

    write-host " Removing old code zip and extracted code from temp in $srvr " -ForegroundColor Yellow
    Remove-Item "\\$srvr\C$\temp\$item.zip" -Force -Verbose
    if(test-path "\\$srvr\C$\temp\$item"){
    Remove-Item "\\$srvr\C$\temp\$item" -Force -Recurse
    }
    write-host " Copy code zip and extracted code to temp in $srvr " -ForegroundColor Green
    copy-item "Binaries\Web\$item.zip" "\\$srvr\C$\temp\" -Force

    }
    else{

    write-host " Copy code zip and extracted code to temp in $srvr " -ForegroundColor Green
    copy-item "Binaries\Web\$item.zip" "\\$srvr\C$\temp\" -Force
    }

  
Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock {

$Sitem=$using:item

$json=$using:main
$env=$using:environment
Import-Module WebAdministration

Write-Host " working on deployment for $Sitem" -ForegroundColor Green
$i=@()
($i+=$json.Deploy.ENV.$env.VirtualSites)|Where-Object{$_.name -eq $Sitem}

$compName=$i.$Sitem.name
$SiteName=$i.$Sitem.Sitename

$AppPool=$i.$Sitem.AppPool
$PhysicalPath=$i.$Sitem.PhysicalPath
$AnonymousAuth=$i.$Sitem.AnonymousAuth
$WindowsAuth=$i.$Sitem.WindowsAuth
$AspNetImpersonation=$i.$Sitem.AspNetImpersonation


    Write-Host "**************************************** Working with AppPools ************************************"
try{

        if(Test-Path ('IIS:\AppPools\'+ "$AppPool") -PathType Container) {

        Write-Warning "The App Pool $AppPool already exists Removing..."

        Remove-Item "IIS:\AppPools\$AppPool" -Force -Recurse -ErrorAction Stop

        Write-host "Delete appPool :" $AppPool -ForegroundColor Yellow
        }
       
       if(!(Test-Path ('IIS:\AppPools\'+ "$AppPool") -PathType Container)) {
       Write-Host "Creating AppPool:" $AppPool -ForegroundColor Green
      $J=@()
       ($J+=$json.Deploy.ENV.$env.AppPools)|Where-Object{$_.Name -eq $AppPool}
      
       $AppPoolName=$J.$AppPool.Name
      
       $DotNetVersion=$j.$AppPool.DotNetVersion
       $enable32BitAppOnWin64=$j.$AppPool.Enable32BitApps
       $enable32Bit=[System.Convert]::ToBoolean($enable32BitAppOnWin64)
       $ManagedPipelineMode=$J.$AppPool.ManagedPipelineMode
       $QueueLength=$J.$AppPool.QueueLength
       $UserName=$J.$AppPool.UserName
       $password=$J.$AppPool.Password
      
      
        Write-Host "****************Creating AppPool : $AppPoolName ****************"
         $pool= New-WebAppPool $AppPoolName -Force -ErrorAction Stop
         if([string]::IsNullOrEmpty($appPool))
                                         {
                                            IISReset
                                            $Pool = Get-Item  IIS:\AppPools\$AppPoolName -Force
                                         }

      
                              #Set app pool properties
                                               if(!([string]::IsNullOrEmpty($UserName)))
                                               {
                                            if($UserName -ieq "LocalSystem")
                                            {
                                                #AppPool Identity Type is set to 'LocalSystem'
                                                
                                               $pool.processModel.identityType = 0
                                            }
                                            elseif($UserName -ieq "LocalService")
                                            {
                                                #AppPool Identity Type is set to 'LocalService'
                                                $pool.processModel.identityType = 1
                                            }
                                            elseif($UserName -ieq "NetworkService")
                                            {
                                                #AppPool Identity Type is set to 'NetworkService'
                                                $pool.processModel.identityType = 2
                                            }
                                            elseif($UserName -ieq "ApplicationPoolIdentity")
                                            {
                                                #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                                $pool.processModel.identityType = 4
                                            }
                                            elseif(!([string]::IsNullOrEmpty($password)))
                                            {
                                                #AppPool Identity Type is set to CustomUser. Use credentials provided in XML
                                                $pool.processModel.identityType = 3
                                                $pool.processModel.userName = $UserName
                                                $pool.processModel.password = $password
                                            }
                                            else
                                            {
                                                Write-Host " AppPool UserName or Password : Invalid. AppPool Identity is set to ApplicationPoolIdentity"
                                                #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                                $pool.processmodel.identityType = 4   
                                            }
                                         }
                                               else
                                               {
                                            #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                            $pool.processmodel.identityType = 4
                                         }                                         
                                               
                                               #Check if properties of AppPool component not empty and then assign. If empty default values would get assigned
                                               if(!([string]::IsNullOrEmpty($DotNetVersion)))
                                               {
                                                if( $DotNetVersion -eq "No Managed Code")
                                                  {
                                                   $pool.managedRuntimeVersion = ""
                                                  }
                                                  else{
                                                   $pool.managedRuntimeVersion = $DotNetVersion
                                                  }
                                         }
                                               
                                               if(!([string]::IsNullOrEmpty($enable32Bit)))
                                               {                                     
                                               $pool.enable32BitAppOnWin64 = $enable32Bit
                                         }
      
                                               if(!([string]::IsNullOrEmpty($ManagedPipelineMode)))
                                               {
                                              $pool.managedPipelineMode = $ManagedPipelineMode
                                               }
      
                                               if(!([string]::IsNullOrEmpty($QueueLength)))
                                               {
                                                  $pool.queueLength = [int]$QueueLength
                                               }
      
                                               
      
                                               $pool | Set-Item -Force -ErrorAction Stop  
                                               Write-Host "The App Pool $pool Created successfully" -ForegroundColor Green
                                                               
                                          }
        
      
}
catch{

        Write-Host  "Could not process AppPool $AppPool. Exception Messsage - $($error[0].Exception)"
        Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"
}# end AppPool
Write-Host "**************************************** END AppPools ********************************************"-ForegroundColor Green
# starting Try blcok for web sites
try{
        Write-Host "**************************************** Working with Sites ***************************************" -ForegroundColor Green

               
                # Createing website
                
                     if($SiteName -ne $null -and $SiteName -ne "")
                     {

                     Try{
                                    $appcmdPath = "$Env:SystemRoot\System32\inetsrv\appcmd.exe" 

                                    

                                   #Remove existing WebSite if the DeleteExisting option is set to True in xml inputs of that component
                                   if(!(Test-Path ("IIS:\Sites\" + "$SiteName") -PathType Container) )
                                   {

                                          # featch site details from JSON
                                          $k=@()
                                             ($k+=$json.Deploy.ENV.$env.Sites)|Where-Object{$_.Name -eq $SiteName}

                                             
                                             $Site=$k.$SiteName.name
                                             
                                             $AppPool=$k.$SiteName.AppPool
                                             $BindingPort=$k.$SiteName.BindingPort
                                             $PhysicalPath=$k.$SiteName.PhysicalPath
                                             
                                             $EnabledProtocol=$k.$SiteName.EnabledProtocol
                                             $AnonymousAuth=$k.$SiteName.AnonymousAuth
                                             $WindowsAuth=$k.$SiteName.WindowsAuth
                                             $AspNetImpersonation=$k.$SiteName.AspNetImpersonation


                                            Write-Host "****************Creating WebSite : $Site****************"

                                            if(Test-Path ("IIS:\AppPools\"+"$($AppPool)") -PathType Container)
                                        { 
                                             $protocols=$($EnabledProtocol).Split(',').Split(';')  
                                             $bindInfo=$($BindingPort).Split(',').Split(';')                                             

                                             $bindings = @()
                                             $count = 0

                                             #Multiple Bindings for website will be formed here based on EnabledProtocol and BindingPort
                                             foreach($item in $protocols)
                                             {
                                                $bindings+= "@{protocol="+$item+";bindingInformation=$(@($bindInfo)[$count])}"  
                                                $count++
                                             }

                                            mkdir $PhysicalPath -Force                                                                                  
                                            New-Item "IIS:\Sites\$Site" -bindings $bindings -physicalPath $PhysicalPath -ApplicationPool $AppPool -ErrorAction Stop

                                            Try{
                                            #Apply Anonymous authentication setting
                                            if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                            {
                                                Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $k.$SiteName.AnonymousAuth -PSPath IIS:\ -Location "$Site"
                                                Write-Host "AnonymouseAuth setting updated for Site:$Site"
                                            }

                                            #Apply Windows authentication setting
                                            if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                            {
                                               
                                                if($WindowsAuth -ieq "windows")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq "basic")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq $false)
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Write-Host "WindowsAuth and BasicAuth are disabled for Site:$Site"                
                                                }
                                            }
                                
                                            #Apply ASP.NET Impersonation setting
                                            if(!([string]::IsNullOrEmpty($AspNetImpersonation)))
                                            {
                                                &$appcmdPath set config "$Site" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                                Write-Host "Asp.Net Impersonation enabled for Site:$Site" 
                                            }
                                            }
                                            Catch{
                                                Write-Host "Could not process Anonymous Authentication or Windows Authentication or AspNetImpersonation for WebSite:$Site. Exception Messsage - $($error[0].Exception)"
                                                Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"  
                                            } 
                                        }
                                        else
                                        {
                                            Write-Host "Could not process website $Site. As AppPool $($AppPool) not found"
                                             
                                        }    

                                     }   

                                     else{

                                     Write-Warning " website $sitename is already exist"
                                     }
                                    # done with site creatioin

                                    }
                                    catch
                                    {
                                      Write-Host "Could not process website $Site. Exception Messsage - $($error[0].Exception)"
                                      Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"
                                         
                                    }
                     }#end if for website





Write-Host "**************************************** END with Sites *******************************************"
}# end of Try blcok for web sites
catch{

    # Write-Host "Sites : Issue during deployment workflow in $ServerName. Exception Messsage - $($error[0].Exception)" 
    # Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)" 
 }

# starting Try blcok for web virtual sites

Try{
                                Write-Host "****************Root Virtual site: $compName****************"

                                if($compName -ne $null -and $compName -ne "")
                                {
								$i=@()
                                ($i+=$json.Deploy.ENV.$env.VirtualSites)|Where-Object{$_.name -eq $Sitem}


                                 $compName=$i.$Sitem.name
                                 $SiteName=$i.$Sitem.Sitename
                                 $AppPool=$i.$Sitem.AppPool
                                 $PhysicalPath=$i.$Sitem.PhysicalPath
                                 $AnonymousAuth=$i.$Sitem.AnonymousAuth
                                 $WindowsAuth=$i.$Sitem.WindowsAuth
                                 $AspNetImpersonation=$i.$Sitem.AspNetImpersonation

                                $tempPath = "C:\temp\"
                                Write-Host "Extracting Packages to temp Directory and copy to App path" -ForegroundColor Yellow  

                                                  
                                $appcmdPath = "$Env:SystemRoot\System32\inetsrv\appcmd.exe"

                                $VDIRExists = $false

                                if(([string]::IsNullOrEmpty((get-item "IIS:\Sites\$($SiteName)\$compName" -ErrorAction Ignore).PhysicalPath)))
                                {
                                    $VDIRExists = $false
                                }
                                else
                                {
                                    $VDIRExists = $true
                                }

                                #Remove existing WebApp if the DeleteExisting option is set to True in xml inputs of that component
                                if($VDIRExists -ieq $true)
                                {
                                    Remove-Item "IIS:\Sites\$($SiteName)\$compName" -Force -Recurse -ErrorAction Stop                                                                                      
                                            
                                    #After deletion just reverify if webapp physical path attribute
                                    if(([string]::IsNullOrEmpty((get-item "IIS:\Sites\$($SiteName)\$compName" -ErrorAction Ignore).PhysicalPath)))
                                    {
                                        Write-Host "Deleted WebApp: $compName"
                                        $VDIRExists = $false
                                    }
                                    else
                                    {
                                        $VDIRExists = $true
                                    }
                                }  
                                
                                #Verify if WebApp does not exists and delete. Then extract from zip  
                                if (!($VDIRExists))
                                {           
                                   if((Test-Path "IIS:\Sites\$($SiteName)" -PathType Container) -and (Test-Path "IIS:\AppPools\$($AppPool)" -PathType Container))
                                    { 
                                                         
                                    Add-Type -AssemblyName System.IO.Compression.FileSystem
                                              function Unzip
                                              {
                                                  param([string]$zipfile, [string]$outpath)
                                              
                                                  [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
                                              }
                                             
                                             if(test-path "$tempPath\$compname")
                                             {
                                              Remove-Item "$tempPath\$compname" -Force -Recurse
                                             }
                                              
                                            Unzip "$tempPath\$($compname).zip" "$tempPath\$compname"

                                             if(test-path "$PhysicalPath")
                                             {
                                              Remove-Item "$PhysicalPath" -Force -Recurse
                                             }
                                          
                                          robocopy.exe "$tempPath\$compname" "$PhysicalPath" /MIR /mt:4 /nfl /ndl /np /R:2 /W:5

                                    New-Item IIS:\Sites\$($SiteName)\$compname -physicalPath "$($PhysicalPath)" -type Application -force -ErrorAction Stop
                                    #Set Application Pool
                                    Set-ItemProperty "IIS:\Sites\$($SiteName)\$compname" -name applicationPool -value $AppPool -ErrorAction Stop



                                     Try{
                                    #Apply Anonymous authentication setting
                                    if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                    {
                                       Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $AnonymousAuth -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop
                                        Write-Host "AnonymouseAuth setting updated for App:$compName"
                                    }

                                    #Apply Windows authentication setting
                                    if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                    {
                                        if($WindowsAuth -ieq "windows")
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop              
                                            Write-Host "WindowsAuth enabled for App:$compname"
                                        }
                                        elseif($getApp.WindowsAuth -ieq "basic")
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop              
                                            Write-Host "BasicAuth enabled for App:$compName"
                                        }
                                        elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop                
                                            Write-Host "WindowsAuth enabled for App:$compName"
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($getApp.SiteName)/$compName" -ErrorAction Stop                
                                            Write-Host "BasicAuth enabled for App:$compName"
                                        }
                                        elseif($WindowsAuth -ieq $false)
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop   
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop                
                                            Write-Host "WindowsAuth and BasicAuth disabled for App:$compName"
                                        }
                                    }
                             
                                    #Apply ASP.NET Impersonation setting
                                    if($AspNetImpersonation)
                                    {
                                    
                                        &$appcmdPath set config "$($SiteName)/$compName" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                        Write-Host "Asp.Net Impersonation enabled for App:"$compname
                                    }                                            
                                        
                                                              
                                 }# end of try block
                                 catch
                                 {
                                         
                                    Write-Host "Deleting default web.config if any in physical path. To avoid issues for setting Anonymous Authentication or Windows Authentication or AspNetImpersonation"
                                    $defaultConfig = $($PhysicalPath).trim('\').trim('/')
                                    if(Test-Path "$defaultConfig\web.config")
                                    {
                                        Remove-Item "$defaultConfig\web.config" -Force
                                    }  
                                    #Apply Anonymous authentication setting
                                    if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                    {
                                        Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $AnonymousAuth -PSPath IIS:\ -Location "$($SiteName)/$compname" -ErrorAction Stop
                                        Write-Host "AnonymouseAuth setting updated for App:$compname"
                                    }

                                    #Apply Windows authentication setting
                                    if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                    {
                                        if($WindowsAuth -ieq "windows")
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath "IIS:\Sites\$SiteName\$compname" -ErrorAction Stop              
                                            Write-Host "WindowsAuth enabled for App:$compname"
                                        }
                                        elseif($WindowsAuth -ieq "basic")
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath "IIS:\Sites\$SiteName\$compname" -ErrorAction Stop              
                                            Write-Host "BasicAuth enabled for App:$compname"
                                        }
                                        elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath "IIS:\Sites\$SiteName\$compname" -ErrorAction Stop                
                                            Write-Host "WindowsAuth enabled for App:$compname"
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath "IIS:\Sites\$SiteName\$compname" -ErrorAction Stop                
                                            Write-Host "BasicAuth enabled for App:$compname"
                                        }
                                        elseif($WindowsAuth -ieq $false)
                                        {

                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath "IIS:\Sites\$SiteName\$compname" -ErrorAction Stop   
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath "IIS:\Sites\$SiteName\$compname" -ErrorAction Stop                
                                            Write-Host "WindowsAuth and BasicAuth disabled for App:$compname"
                                        }
                                    }
                             
                                    #Apply ASP.NET Impersonation setting
                                    if(!([string]::IsNullOrEmpty($AspNetImpersonation)))
                                    {
                                        &$appcmdPath set config "$($SiteName)/$compname" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                        Write-Host "Asp.Net Impersonation enabled for App:$compname"
                                    }     
                                    }
                                                              
                                 }

                                 else
                                 {
                                  Write-Host "No website or AppPool found with the name: $($SiteName) or $($AppPool)"

                                 }

                                }
                                else
                                {
                                 
                                 Write-Host "Root Virtual site : $compname already exist"
                                        
                                
                                }


                                }# end of Virtual site


    
    }#end try

    catch{
            Write-Host "Could not process Root WebApp $App. Exception Messsage - $($error[0].Exception)"
            Write-Host "ERROR Line No. : $($error[0].invocationinfo.ScriptLineNumber)"
            net use \\$ServerName /delete 
    }

    }#end of script block

}#end of IF server validation 
        if(!([string]::IsNullOrEmpty($AppPool)) -or !([string]::IsNullOrEmpty($SiteName)) -or !([string]::IsNullOrEmpty($compName)))
                                                              {                                                
                                                                     Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock{iisreset /start}
                                                              }  
else
{
$exception="Connectivity issue to Server:$Srvr"
Write-Error -Exception $exception
}
}#end of servers
}#end foreach items full deploy

}

# check virtul sites (components) are present then initate deployment else check for website deployment
elseif($websitesCount -ne "0" -and $sitesCount -eq "0")
{


  Foreach ($item in $websites)
  {

   foreach($srvr in $servers){
$i=@()
   ($i+=$main.Deploy.ENV.$environment.Sites)|Where-Object{$_.Name -eq $item}



    if($srvr -ne "" -and (Test-Connection -cn $srvr -BufferSize 16 -Count 1 -ea 0 -quiet))
{
    Write-Host "**************Deployment workflow started in $srvr****************" 
    
    #Remotely connect to deployment server
    $user="$env:username"
$SVCPWD="$env:password"
$PWd=ConvertTo-SecureString -String $SVCPWD -AsPlainText -Force
$cred=New-Object -TypeName System.Management.Automation.PSCredential $user, $PWd 
    net use \\$srvr /user:$user $SVCPWD 

    if(!([string]::IsNullOrEmpty($AppPool)) -or !([string]::IsNullOrEmpty($SiteName)) -or !([string]::IsNullOrEmpty($compName)))
                    {                                                
                        Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock{iisreset /stop}
                    }  

                # Copy zip code   

    If ( Test-Path "\\$srvr\C$\temp\$item.zip" ){

    write-host " Removing old code zip and extracted code from temp in $srvr " -ForegroundColor Yellow
    Remove-Item "\\$srvr\C$\temp\$item.zip" -Force -Verbose
    if(test-path "\\$srvr\C$\temp\$item"){
    Remove-Item "\\$srvr\C$\temp\$item" -Force -Recurse
    }
    write-host " Copy code zip and extracted code to temp in $srvr " -ForegroundColor Green
    copy-item "Binaries\Web\$item.zip" "\\$srvr\C$\temp\" -Force

    }
    else
    {
    write-host " Copy code zip and extracted code to temp in $srvr " -ForegroundColor Green
    copy-item "Binaries\Web\$item.zip" "\\$srvr\C$\temp\" -Force

    }

   # #Add server to trusted host of runner

                    <#if(!@((Get-Item WSMan:\localhost\Client\TrustedHosts).value).Contains("*"))
                    {                        
                        Set-Item -Path WSMan:\localhost\Client\TrustedHosts -Value "*" -Force
                        "Machine added to Trusted-Hosts of runner"
                    }
                    #> 
              
       
Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock {

$Sitem=$using:item
$json=$using:main
$env=$using:environment

Import-Module WebAdministration

Write-Host " working on deployment for $Sitem" -ForegroundColor Green
$i=@()
($i+=$json.Deploy.ENV.$env.sites)|Where-Object{$_.name -eq $Sitem}



$SiteName=$i.$Sitem.Name
$AppPool=$i.$Sitem.AppPool



    Write-Host "**************************************** Working with AppPools ************************************"
try{

        if(Test-Path ('IIS:\AppPools\'+ "$AppPool") -PathType Container) {

        Write-Warning "The App Pool $AppPool already exists.Removing.."

        Remove-Item "IIS:\AppPools\$AppPool" -Force -Recurse -ErrorAction Stop

        Write-host "Delete appPool :" $AppPool -ForegroundColor Yellow
        }
       
       if(!(Test-Path ('IIS:\AppPools\'+ "$AppPool") -PathType Container)) {
       Write-Host "Creating AppPool:" $AppPool -ForegroundColor Green
    $J=@()
       ($J+=$json.Deploy.ENV.$env.AppPools)|Where-Object{$_.Name -eq $AppPool}
      
       $AppPoolName=$J.$AppPool.Name
      
       $DotNetVersion=$j.$AppPool.DotNetVersion
       $enable32BitAppOnWin64=$j.$AppPool.Enable32BitApps
       $enable32Bit=[System.Convert]::ToBoolean($enable32BitAppOnWin64)
       $ManagedPipelineMode=$J.$AppPool.ManagedPipelineMode
       $QueueLength=$J.$AppPool.QueueLength
       $UserName=$J.$AppPool.UserName
       $password=$J.$AppPool.Password
      
      
        Write-Host "****************Creating AppPool : $AppPoolName ****************"
         $pool= New-WebAppPool $AppPoolName -Force -ErrorAction Stop
         if([string]::IsNullOrEmpty($appPool))
                                         {
                                            IISReset
                                            $Pool = Get-Item  IIS:\AppPools\$AppPoolName -Force
                                         }

      
                              #Set app pool properties
                                               if(!([string]::IsNullOrEmpty($UserName)))
                                               {
                                            if($UserName -ieq "LocalSystem")
                                            {
                                                #AppPool Identity Type is set to 'LocalSystem'
                                                
                                               $pool.processModel.identityType = 0
                                            }
                                            elseif($UserName -ieq "LocalService")
                                            {
                                                #AppPool Identity Type is set to 'LocalService'
                                                $pool.processModel.identityType = 1
                                            }
                                            elseif($UserName -ieq "NetworkService")
                                            {
                                                #AppPool Identity Type is set to 'NetworkService'
                                                $pool.processModel.identityType = 2
                                            }
                                            elseif($UserName -ieq "ApplicationPoolIdentity")
                                            {
                                                #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                                $pool.processModel.identityType = 4
                                            }
                                            elseif(!([string]::IsNullOrEmpty($password)))
                                            {
                                                #AppPool Identity Type is set to CustomUser. Use credentials provided in XML
                                                $pool.processModel.identityType = 3
                                                $pool.processModel.userName = $UserName
                                                $pool.processModel.password = $password
                                            }
                                            else
                                            {
                                                Write-Host " AppPool UserName or Password : Invalid. AppPool Identity is set to ApplicationPoolIdentity"
                                                #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                                $pool.processmodel.identityType = 4   
                                            }
                                         }
                                               else
                                               {
                                            #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                            $pool.processmodel.identityType = 4
                                         }                                         
                                               
                                               #Check if properties of AppPool component not empty and then assign. If empty default values would get assigned
                                             if(!([string]::IsNullOrEmpty($DotNetVersion)))
                                               {
                                                if( $DotNetVersion -eq "No Managed Code")
                                                  {
                                                   $pool.managedRuntimeVersion = ""
                                                  }
                                                  else{
                                                   $pool.managedRuntimeVersion = $DotNetVersion
                                                  }
                                         }
                                               
                                               if(!([string]::IsNullOrEmpty($enable32Bit)))
                                               {                                     
                                               $pool.enable32BitAppOnWin64 = $enable32Bit
                                         }
      
                                               if(!([string]::IsNullOrEmpty($ManagedPipelineMode)))
                                               {
                                              $pool.managedPipelineMode = $ManagedPipelineMode
                                               }
      
                                               if(!([string]::IsNullOrEmpty($QueueLength)))
                                               {
                                                  $pool.queueLength = [int]$QueueLength
                                               }
      
                                               
      
                                               $pool | Set-Item -Force -ErrorAction Stop  
                                               Write-Host "The App Pool $pool Created successfully" -ForegroundColor Green
                                                               
                                          }
        
      
}
catch{

        Write-Host  "Could not process AppPool $AppPool. Exception Messsage - $($error[0].Exception)"
        Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"
}# end AppPool
Write-Host "**************************************** END AppPools ********************************************"-ForegroundColor Green
# starting Try blcok for web sites
try{
        Write-Host "**************************************** Working with Sites ***************************************" -ForegroundColor Green

               
                # Createing website
                 $tempPath = "C:\temp\"
                 Write-Host "Extracting Packages to temp Directory and copy to App path" -ForegroundColor Yellow  
                                                             
                
                     if($SiteName -ne $null -and $SiteName -ne "")
                     {

                     Try{
                                    $appcmdPath = "$Env:SystemRoot\System32\inetsrv\appcmd.exe" 

                                    

                                   #Remove existing WebSite if the DeleteExisting option is set to True in xml inputs of that component
                                   if(!(Test-Path ("IIS:\Sites\" + "$SiteName") -PathType Container) )
                                   {
                                     #Write-Host "website $SiteName doesnot exist  " -ForegroundColor Green

               

                                       # Remove-Item "IIS:\Sites\$SiteName" -Force -Recurse -ErrorAction Stop
                                        #Write-Host "Deleted WebSite: $SiteName" -ForegroundColor Yellow
                                   

                                          # featch site details from JSON
                                          $k=@()
                                             ($k+=$json.Deploy.ENV.$env.Sites)|Where-Object{$_.Name -eq $SiteName}

                                             
                                             $Site=$k.$SiteName.name
                                             $AppPool=$k.$SiteName.AppPool
                                             $BindingPort=$k.$SiteName.BindingPort
                                             $PhysicalPath=$k.$SiteName.PhysicalPath
                                             $EnabledProtocol=$k.$SiteName.EnabledProtocol
                                             $AnonymousAuth=$k.$SiteName.AnonymousAuth
                                             $WindowsAuth=$k.$SiteName.WindowsAuth
                                             $AspNetImpersonation=$k.$SiteName.AspNetImpersonation


                                              

                                            Write-Host "****************Creating WebSite : $Site****************"

                                            if(Test-Path ("IIS:\AppPools\"+"$($AppPool)") -PathType Container)
                                        { 
                                             $protocols=$($EnabledProtocol).Split(',').Split(';')  
                                             $bindInfo=$($BindingPort).Split(',').Split(';')                                             

                                             $bindings = @()
                                             $count = 0

                                             #Multiple Bindings for website will be formed here based on EnabledProtocol and BindingPort
                                             foreach($item in $protocols)
                                             {
                                                $bindings+= "@{protocol="+$item+";bindingInformation=$(@($bindInfo)[$count])}"  
                                                $count++
                                             }
                                            
                                              Add-Type -AssemblyName System.IO.Compression.FileSystem
                                              function Unzip
                                              {
                                                  param([string]$zipfile, [string]$outpath)
                                              
                                                  [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
                                              }
                                             
                                             if(test-path "$tempPath\$Site")
                                             {
                                              Remove-Item "$tempPath\$Site" -Force -Recurse
                                             }
                                              
                                            Unzip "$tempPath\$($Site).zip" "$tempPath\$Site"

                                             if(test-path "$PhysicalPath")
                                             {
                                              Remove-Item "$PhysicalPath" -Force -Recurse
                                              New-Item -Path "$PhysicalPath" -ItemType Directory -Force
                                             }
                                            else
                                            {
                                              New-Item -Path "$PhysicalPath" -ItemType Directory -Force
                                            }
                                            robocopy.exe "$tempPath\$Site" "$PhysicalPath" /MIR /mt:4 /nfl /ndl /np /R:2 /W:5
                                            Remove-Item "$tempPath\$Site" -Force -Recurse
                                            }
                                            #Create a website                                                                                        
                                            New-Item "IIS:\Sites\$Site" -bindings $bindings -physicalPath $PhysicalPath.Replace("/","\") -ApplicationPool $AppPool -ErrorAction Stop

                                            #Verify if physical path exists. Create if not. To enable auth or impersonation settings for site
                                                        
                                          Try{
                                            #Apply Anonymous authentication setting
                                           if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                            {
                                                Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $k.$SiteName.AnonymousAuth -PSPath IIS:\ -Location "$Site"
                                                Write-Host "AnonymouseAuth setting updated for Site:$Site"
                                            }

                                            #Apply Windows authentication setting
                                            if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                            {
                                               
                                                if($WindowsAuth -ieq "windows")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq "basic")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq $false)
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Write-Host "WindowsAuth and BasicAuth are disabled for Site:$Site"                
                                                }
                                            }
                                
                                            #Apply ASP.NET Impersonation setting
                                            
                                            if(!([string]::IsNullOrEmpty($AspNetImpersonation)))
                                            {
                                                &$appcmdPath set config "$Site" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                                Write-Host "Asp.Net Impersonation enabled for Site:$Site" 
                                            }
                                                                           
                                            }
                                            Catch{
                                                Write-Host "Could not process Anonymous Authentication or Windows Authentication or AspNetImpersonation for WebSite:$Site. Exception Messsage - $($error[0].Exception)"
                                                Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"  
                                            } 
                                        }    

                                        

                                     else{
                                     if((Test-Path ("IIS:\Sites\" + "$SiteName") -PathType Container) )
                                   {
                                     #Write-Host "website $SiteName doesnot exist  " -ForegroundColor Green

                                     Write-Warning " website $sitename is already exist and hence removing"

               

                                        Remove-Item "IIS:\Sites\$SiteName" -Force -Recurse -ErrorAction Stop
                                        Write-Host "Deleted WebSite: $SiteName" -ForegroundColor Yellow
                                   

                                          # featch site details from JSON
                                          $k=@()
                                             ($k+=$json.Deploy.ENV.$env.Sites)|Where-Object{$_.Name -eq $SiteName}

                                             
                                             $Site=$k.$SiteName.name
                                             $AppPool=$k.$SiteName.AppPool
                                             $BindingPort=$k.$SiteName.BindingPort
                                             $PhysicalPath=$k.$SiteName.PhysicalPath
                                             $EnabledProtocol=$k.$SiteName.EnabledProtocol
                                             $AnonymousAuth=$k.$SiteName.AnonymousAuth
                                             $WindowsAuth=$k.$SiteName.WindowsAuth
                                             $AspNetImpersonation=$k.$SiteName.AspNetImpersonation


                                              

                                            Write-Host "****************Creating WebSite : $Site****************"

                                            if(Test-Path ("IIS:\AppPools\"+"$($AppPool)") -PathType Container)
                                        { 
                                             $protocols=$($EnabledProtocol).Split(',').Split(';')  
                                             $bindInfo=$($BindingPort).Split(',').Split(';')                                             

                                             $bindings = @()
                                             $count = 0

                                             #Multiple Bindings for website will be formed here based on EnabledProtocol and BindingPort
                                             foreach($item in $protocols)
                                             {
                                                $bindings+= "@{protocol="+$item+";bindingInformation=$(@($bindInfo)[$count])}"  
                                                $count++
                                             }
                                            
                                             
                                              Add-Type -AssemblyName System.IO.Compression.FileSystem
                                              function Unzip
                                              {
                                                  param([string]$zipfile, [string]$outpath)
                                              
                                                  [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
                                              }
                                             
                                             if(test-path "$tempPath\$Site")
                                             {
                                              Remove-Item "$tempPath\$Site" -Force -Recurse
                                             }
                                              
                                            Unzip "$tempPath\$($Site).zip" "$tempPath\$Site"

                                             if(test-path "$PhysicalPath")
                                             {
                                              Remove-Item "$PhysicalPath" -Force -Recurse
                                              New-Item "$PhysicalPath" -ItemType Directory -Force
                                             }
                                            else {
                                              New-Item -Path "$PhysicalPath" -ItemType Directory -Force
                                            }
                                            robocopy.exe "$tempPath\$Site" "$PhysicalPath" /MIR /mt:4 /nfl /ndl /np /R:2 /W:5
                                            Remove-Item "$tempPath\$Site" -Force -Recurse
                                            }
                                            #Create a website                                                                                        
                                            New-Item "IIS:\Sites\$Site" -bindings $bindings -physicalPath $PhysicalPath.Replace("/","\") -ApplicationPool $AppPool -ErrorAction Stop

                                            #Verify if physical path exists. Create if not. To enable auth or impersonation settings for site
                                                        

                                            Try{
                                            #Apply Anonymous authentication setting
                                            if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                            {
                                                Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $k.$SiteName.AnonymousAuth -PSPath IIS:\ -Location "$Site"
                                                Write-Host "AnonymouseAuth setting updated for Site:$Site"
                                            }

                                            #Apply Windows authentication setting
                                            if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                            {
                                               
                                                if($WindowsAuth -ieq "windows")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq "basic")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq $false)
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Write-Host "WindowsAuth and BasicAuth are disabled for Site:$Site"                
                                                }
                                            }
                                
                                            #Apply ASP.NET Impersonation setting
                                            if(!([string]::IsNullOrEmpty($AspNetImpersonation)))
                                            {
                                                &$appcmdPath set config "$Site" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                                Write-Host "Asp.Net Impersonation enabled for Site:$Site" 
                                            }
                                                                           
                                            }
                                            Catch{
                                                Write-Host "Could not process Anonymous Authentication or Windows Authentication or AspNetImpersonation for WebSite:$Site. Exception Messsage - $($error[0].Exception)"
                                                Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"  
                                            } 
                                        }
                                        else
                                        {
                                            Write-Host "Could not process website $Site. As AppPool $($AppPool) not found"
                                             
                                        }    

                                     } 


                                                                                
                                     
                                     
                                    # done with site creatioin

                                    }
                                    catch
                                    {
                                      Write-Host "Could not process website $Site. Exception Messsage - $($error[0].Exception)"
                                      Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"
                                         
                                    }
                     }#end if for website





Write-Host "**************************************** END with Sites *******************************************"
}# end of Try blcok for web sites
catch{

    # Write-Host "Sites : Issue during deployment workflow in $ServerName. Exception Messsage - $($error[0].Exception)" 
    # Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)" 
 }


}# end of script block



  }# end of server connection
   }# end server foreach
  }#end of main foreach

}
else{
    Write-host " something went wrong. please reach devops admin to know more details" -ForegroundColor Red
}





 # end IF Full Deploy

# end else if in full deployment


}
#############################################################################################################################################################################
#
# END With Web-deploy function
#
#############################################################################################################################################################################

#############################################################################################################################################################################
#
# Start Win-Service Function
#
#############################################################################################################################################################################
function win-service([string]$jsonpath,[string[]]$servers,[string[]]$components,$environment){


$user="$env:username"
$SVCPWD="$env:password"
$passwrd=ConvertTo-SecureString -String $SVCPWD -AsPlainText -Force
$cred=New-Object -TypeName System.Management.Automation.PSCredential $user, $passwrd

if(Test-Path $jsonpath)
{
$main=Get-Content $jsonpath |ConvertFrom-Json

$services=@()
$data=@()

($data+=$main.Deploy.ENV.$environment.Services).foreach{
$Services+=$_.PSobject.properties.Name

}
   if (!($Services ))
	{
		$Services=$null
	}
   else{
            #Do Nothing
			
		}
  foreach($svc in $services)
           
        {
		$i=@()
		($i+=$main.Deploy.ENV.$environment.Services)|Where-Object{$_.name -match $svc}
        
        Write-Host "Display svc info of :"$svc -ForegroundColor Green
         $SvcName=$i.$svc.Name
         $Displayname=$i.$svc.DisplayName
         $Description=$i.$svc.Description
         $Binpath=$i.$svc.Binpath
         $Startuptype=$i.$svc.StartUpType
         $UserName=$i.$svc.UserName
         $pass=$i.$svc.Password
         $ServiceStart=$i.$svc.ServiceStart
           

         Write-host " ----------------------------------------------------[Deployment start ]------------------------------------------------" -ForegroundColor Green

         #looping all server

         foreach($srvr in $servers)
         {
            $srvr = $srvr.Trim()
            Try{

                #Checking connectivity to Server

                if($srvr -ne "" -and (Test-Connection -cn $srvr -BufferSize 16 -Count 1 -ea 0 -quiet))
                {
                    Write-Host "**************Deployment  started for $SvcName in $srvr****************" -ForegroundColor Cyan
					Write-Warning "Net command started.."
                     #Remotely connect to deployment server
                     $user="$env:username"
                     $SVCPWD="$env:password"
                     $PWd=ConvertTo-SecureString -String $SVCPWD -AsPlainText -Force
                     $cred=New-Object -TypeName System.Management.Automation.PSCredential $user, $PWd 
                    net use \\$srvr /user:$user $SVCPWD  
					Write-Warning "Net command executed..."
                   
                    Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock {                     
                        set-executionpolicy unrestricted -Force -ErrorAction Stop
                    } 

                    #Verify Windows Services

                    if(!([string]::IsNullOrEmpty($SvcName)))
                    {
                       #Test if service code exists in Binaries path or not

                       if(Test-Path "Binaries\Services\$SvcName.zip")
                       {
						write-warning "Testing path..."
                          # delete the existing package in server level
                            if(Test-Path "\\$srvr\c$\temp\$SvcName.zip")

                            {
                              Remove-Item "\\$srvr\c$\temp\$SvcName.zip" -Force -Verbose -ErrorAction Ignore

                            }
                         
                         $global:lastexitcode = 0 

                          #Copy Service.zip file to Destination server
						  write-warning "Copying binary....Service name is $svcName "
                         # Xcopy.exe "Binaries\Services\$SvcName.zip" "\\$srvr\C$\temp\" /S /I /Y /R /F
						  Copy-Item -Path "Binaries\Services\$SvcName.zip" -Destination "\\$srvr\C$\temp\" -Force
							write-warning "Copied binary"
                          if($global:lastexitcode -ne 0)
                                    {
                                        throw "Copy of $($SvcName).zip to target server - $srvr failed"
                                    }
                            #Remotely connecting to server and deploying the service related code

                           Invoke-Command -ComputerName $srvr -Credential $cred -ErrorAction Stop -ScriptBlock {
                           $j=$using:i
                           $service=$using:svc
                           $envi=$using:environment
                            Write-Host "Display services info of the " $using:svc -ForegroundColor Green
                               $SvcName=$j.$service.Name
							   write-warning "$SvcName"
                               $Displayname=$j.$service.DisplayName
                               $Description=$j.$service.Description
                               $Binpath=$j.$service.Binpath
                               $Startuptype=$j.$service.StartUpType
                               $UserName=$j.$service.UserName
                               $pass=$j.$service.Password
                               $ServiceStart=$j.$service.ServiceStart


                           #Stop the process of service to avoid the service hung during the service deletion

                           Get-Process -processname $([System.IO.Path]::GetFileNameWithoutExtension($Binpath)) -ErrorAction Ignore|where {($_.Path -ieq $Binpath)}|select id|stop-process -force -ErrorAction Ignore

                           $tempPath=""

                           sc.exe delete "$SvcName"
                           

                           if((Get-Service $SvcName -ErrorAction Ignore) -eq $null)
                           {

                            if($Binpath -ne $null -and $Binpath -ne "")
                            {
                              $exepath=[System.IO.Path]::GetDirectoryName($Binpath)							  
                              write-host "$exepath" -Verbose
							  Remove-Item "$exepath" -Recurse -Force -Verbose 
                              $tempPath = "C:\temp"

                              
                              
                              if(Test-Path $tempPath\$service)
                              {
                               Remove-Item "$tempPath\$service" -Force -Recurse -ErrorAction SilentlyContinue -Verbose
                               New-Item "$tempPath\$service" -ItemType Directory -Force -ErrorAction SilentlyContinue -Verbose
                              }
                              else
                              {

                                New-Item $tempPath\$service -ItemType Directory -Force -ErrorAction SilentlyContinue
                              }

                                #Extracting Packages to temp Directory and copy to App path
                              Write-Host "Extracting Packages to temp Directory and copy to App path" -ForegroundColor Yellow
                                 
                                 Add-Type -AssemblyName System.IO.Compression.FileSystem
                                    function Unzip
                                    {
                                        param([string]$zipfile, [string]$outpath)
                                    
                                        [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
                                    }

                                    Unzip "$tempPath\$($service).zip" "$tempPath\$service"

                                 robocopy "$tempPath\$service" "$exepath" /MIR /mt:4 /nfl /ndl /np /R:2 /W:5
                                 Write-Host "Copied WindowsService Package content to Bin Path" 


                                 Write-Host "****************Creating Service : $service****************"
								
                                 
                                    
                                 #Creation of new service
                                        if(!([string]::IsNullOrEmpty($Description)))
                                        {   
                                            New-Service -Name $SvcName -DisplayName $Displayname -Description $Description -BinaryPathName $Binpath -Credential $cred -StartupType $Startuptype -ErrorAction Stop
                                        }
                                        else
                                        {   
                                            New-Service -Name $SvcName -DisplayName $Displayname -BinaryPathName $Binpath -Credential $cred -StartupType $Startuptype -ErrorAction Stop
                                        }


                                                    $ConfigNewName = $envi
                                                        
                                                    if(!($ConfigNewName -eq ""))
                                                    {

                                                        if(Test-Path "$($exepath)\$SvcName.config.$ConfigNewName")
                                                        {
                                                            Move-Item "$($exepath)\$SvcName.config.$ConfigNewName" -Destination "$($exepath)\$SvcName.config" -Force -Verbose
                                                        }
                                                        elseif(Test-Path "$($exepath)\$SvcName.$ConfigNewName.config")
                                                        {
                                                            Move-Item "$($exepath)\$SvcName.$ConfigNewName.config" -Destination "$($exepath)\$SvcName.config" -Force -Verbose
                                                        }
                                                        elseif(Test-Path "$exepath\App.$ConfigNewName.config")
                                                        {
                                                            Move-Item "$exepath\App.$ConfigNewName.config" -Destination "$($exepath)\App.config" -Force -Verbose
                                                        }
                                                        elseif(Test-Path "$exepath\App.config.$ConfigNewName")
                                                        {
                                                            Move-Item "$exepath\App.config.$ConfigNewName" -Destination "$($exepath)\App.config" -Force -Verbose
                                                        }
                                                        else
                                                        {
                                                            Write-Host "$SvcName Configs not renamed. Config could not be renamed as file not found"
                                                        }
                                                         if ($ServiceStart -eq "True")
                                                    {
                                                    Write-Warning "Starting service"
                                                    Start-Service -Name $SvcName -Verbose -ErrorAction Stop
                                                    }
                                                    else{
                                                    #Do Nothing
                                                    }
                                                    }
                                                    elseif(!($ConfigPaths -eq ""))
                                                    {
                                                        Write-Warning "ConfigPaths for WindowsService is declared. Configs not renamed and should be taken care in DeployConfigs task"
                                                    }
                                                     if ($ServiceStart -eq "True")
                                                    {
                                                    Write-Warning "Starting service"
                                                    Start-Service -Name $SvcName -Verbose -ErrorAction Stop
                                                    }
                                                    else{
                                                    #Do Nothing
                                                    }
                                                   
                                         
                                 
                                

                            
                            }#end of Binpath validation
                            else
                                {
								  Write-Host "Service BinPath not provided" -ForegroundColor Red
                                        
                                }


                           }# end service if
                           else
                               {
                                   Write-Host "Service already exists"
                                   Get-Service $service -ErrorAction SilentlyContinue
                                    
                               }
                           

                           #end of Try
                           #end of catch

                           else
                            {                                
                                Write-Host "No service details found with  $service" -ForegroundColor Red
                                 
                            }
           

                           }#end of script block

                       }
                       else
                       {
                       if($SvcName.zip -eq "")
                                    {
                                        Write-Host "$SvcName Package Name is empty for the WindowsService:$svc" -ForegroundColor Red
                                    }
                                    else
                                    {                                   
                                        Write-Host "Package Not found - Binaries\Services\$SvcName.zip" -ForegroundColor Red
                                    }
                                     
                       
                       }

                    }
                    else
                    {

                    }
                    
                }# end if server online or not 

                }#end Try block
                catch{


                }# end catch block
            

         }#end servers foreach       
    
    }#end foreach
}
else{

           Write-Host "$jsonpath is not found. Please verify and Try Again " -ForegroundColor Red
         }
}         
#######################################################################################################################################################################
#
# End With Win-Service function
#
#######################################################################################################################################################################


#######################################################################################################################################################################
#
# Start with deploy-config funtion 
#
#######################################################################################################################################################################
function deploy-config([string]$jsonpath,[string[]]$servers,$environment)
{

$user="$env:username"
$SVCPWD="$env:password"
$passwrd=ConvertTo-SecureString -String $SVCPWD -AsPlainText -Force
$cred=New-Object -TypeName System.Management.Automation.PSCredential $user, $passwrd


if(Test-Path $jsonpath){

$data=Get-Content $jsonpath | ConvertFrom-Json

# gettting details of web apps 

$sites=@()
$appinfo=@()
($appinfo+=$data.Deploy.ENV.$environment.Sites).foreach{

$sites+=$_.PSobject.properties.name
}
   if (!($sites ))
	{
		$sites=$null
	}
   else{
            #Do Nothing
		}
$vsites=@()
$appinfo=@()
($appinfo+=$data.Deploy.ENV.$environment.VirtualSites).foreach{

$vsites+=$_.PSobject.properties.name
}
   if (!($vsites ))
	{
		$vsites=$null
	}
   else{
            #Do Nothing
		}
# fetching services information from json
$services=@()
$svcinfo=@()
($svcinfo+=$data.Deploy.ENV.$environment.Services).foreach{

$services+=$_.Psobject.Properties.name
}
   if (!($services ))
	{
		$services=$null
	}
   else{
            #Do Nothing
		}
$configpath="$pwd\Binaries\configs"


Write-host " Working config deployment for all components " -ForegroundColor Green

foreach($item in $sites){
$i=@()
   ($i+=$data.Deploy.ENV.$environment.Sites)|Where-Object{$_.Name -ieq "$item"}

    $name=$i.$item.name
    $destination=$i.$item.physicalpath
    $dest=$destination.Replace(":","$")

    $ENV=$environment

    $configpath="$pwd\Binaries\configs\$ENV"
    if(Test-Path $configpath)
    {

    Write-host "Working on web config deployment for $item" -ForegroundColor Green

    foreach ($srvr in $servers)
    {

       Write-host " Config Deployment in: $srvr" -ForegroundColor Green
       
      if(Test-Path "\\$srvr\$dest")
      {
        Copy-Item "$configpath\$item\*.*" -Destination "\\$srvr\$dest" -Verbose -Force -Recurse -ErrorAction Stop
       }
       else
       {
        Write-Host " $dest path not found in the $srvr" -ForegroundColor Red
       }

    } # end of servers in foreach
  

  }
  else {
  Write-Warning "$configpath is not available"
  }
}# end foreach for web apps


foreach($item in $vsites){
$i=@()
   ($i+=$data.Deploy.ENV.$environment.VirtualSites)|Where-Object{$_.Name -ieq "$item"}

    $name=$i.$item.name
    $destination=$i.$item.physicalpath
    $dest=$destination.Replace(":","$")
    $ENV=$environment

    $configpath="$pwd\Binaries\configs\$ENV"
    if(Test-Path $configpath)
    {
    Write-host "Working on web config deployment for $item" -ForegroundColor Green

    foreach ($srvr in $servers)
    {

       Write-host " Config Deployment in: $srvr" -ForegroundColor DarkCyan



      if(Test-Path "\\$srvr\$dest")
      {
       Write-Warning "\\$srvr\$dest"
        Copy-Item "$configpath\$item\*.*" -Destination "\\$srvr\$dest" -Verbose -Force -Recurse -ErrorAction Stop
       }
       else
       {
        Write-Host " $dest path not found in the $srvr" -ForegroundColor Red
       }

    } # end of servers in foreach
  
}
else{
Write-Warning "$configpath is not available"
}

}# end foreach for web apps


foreach($item in $Services){
$i=@()
   ($i+=$data.Deploy.ENV.$environment.Services)|Where-Object{$_.Name -ieq "$item"}

    $name=$i.$item.name
    $Binpath=$i.$item.Binpath  
    $dest=([System.IO.Path]::GetDirectoryName($Binpath)).Replace(':','$').trim('\').trim('/')
    $ENV=$environment

    $configpath="$pwd\Binaries\configs\$ENV"
    if(Test-Path $configpath)
    {
    Write-host "Working on web config deployment for $item" -ForegroundColor Green

    foreach ($srvr in $servers)
    {

       Write-host " Config Deployment in: $srvr" -ForegroundColor DarkCyan



      if(Test-Path "\\$srvr\$dest")
      {
       Write-Warning "\\$srvr\$dest"
        Copy-Item "$configpath\$item\*.*" -Destination "\\$srvr\$dest" -Verbose -Force -Recurse -ErrorAction Stop
       }
       else
       {
        Write-Host " $dest path not found in the $srvr" -ForegroundColor Red
       }

    } # end of servers in foreach
  
  }
  else {
  Write-Warning "$configpath is not available"
  }

}






}

else
{
 
 Write-Host "$jsonpath not found" -ForegroundColor Red

}

}
############################################################################################################################################################
#
#End with deploy-config function
#
############################################################################################################################################################
function deploy-config-transform($configuration,$deleteAfterTransformation,$sourcezip,$destination,$destinationzip)
{
write-warning "sourcezip is $sourcezip "
Remove-Item $destination -Recurse -force -errorAction SilentlyContinue
$latestWebConfigTranformator = "$env:CI_PROJECT_DIR/WebConfigTransformRunner.exe"
Add-Type -AssemblyName System.IO.Compression.FileSystem
    function Unzip
        {
            param([string]$zipfile, [string]$outpath)
            [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
        }
		
Unzip $sourcezip $destination
write-warning "Extracted successfully"
#cacls $destination /t /e /g everyone:f
$wwwRoot = "$destination"   
#$wwwRoot= "D:\config\DotNetWebApp"
$webConfigDirs = Get-ChildItem -Path $wwwRoot -Filter "web*.config" | Select -Property Directory -Unique
ForEach ($directory in $webConfigDirs.Directory){
    # Check if file for transformation is present
    $transformationSource = (Get-ChildItem -Path $directory -Filter ("web." + $configuration + ".config"))
    if ($transformationSource) {
        # Found a file to apply transformations
        $guid = [Guid]::NewGuid().ToString()
        $transformArguments = @("""" + (Join-Path -Path $directory -ChildPath "web.config") + """",`
                                """" + $transformationSource[0].FullName + """",`
                                """" + (Join-Path -Path $directory -ChildPath $guid) + """")
        $transformArguments

        $transformationProcess = Start-Process -FilePath $latestWebConfigTranformator -ArgumentList $transformArguments -Wait -PassThru -NoNewWindow
        $transformationProcess
        if ($transformationProcess.ExitCode -ne 0) {
            "Exiting due to web.config transformation tool having returned an error, exit code: " + $transformationProcess.ExitCode
            exit $transformationProcess.ExitCode
        }
        # Delete original web.config and rename the created one
        Remove-Item -Path (Join-Path -Path $directory -ChildPath "web.config")
        Rename-Item -Path (Join-Path -Path $directory -ChildPath $guid) -NewName (Join-Path -Path $directory -ChildPath "web.config") 
    }
    if ($deleteAfterTransformation) {
        # Delete all web.*.config files
        ForEach ($file in (Get-ChildItem -Path $directory -Filter "web.*.config")){
            Remove-Item -Path $file.FullName
        }
    }
}
mkdir $destinationzip -Force
Compress-Archive -U -Path $destination\* -DestinationPath $destinationzip
write-warning "Removing $destination content"
Get-ChildItem -Path $destination -Include * -File -Recurse | foreach { $_.Delete()}
}

$jsonpath="$env:CI_PROJECT_DIR/definition.json"
#$jsonpath="$env:CI_PROJECT_DIR/definition.json"
$main=(Get-Content $jsonpath|ConvertFrom-Json)

            ##########################################################
            $json_appPools=@()
			$data=@()
            ($data+=$main.Deploy.ENV.$environment.AppPools).foreach{


            $json_appPools+=$_.PSobject.properties.Name


            }
			if (!($json_appPools ))
			{
			$count_json_appPools="0"
			}
			else{
            $count_json_appPools=$json_appPools.count
			}
            ##########################################################

            $json_sites=@()
			$data=@()
            ($data+=$main.Deploy.ENV.$environment.Sites).foreach{


            $json_sites+=$_.PSobject.properties.Name
            }
			if (!($json_sites ))
			{
			$count_json_sites="0"
			}
			else{
            $count_json_sites=$json_sites.count
			}
            #########################################################

            $json_vsites=@()
			$data=@()
            ($data+=$main.Deploy.ENV.$environment.VirtualSites).foreach{


            $json_vsites+=$_.PSobject.properties.Name

            }

			if (!($json_vsites ))
			{
			$count_json_vsites="0"
			}
			else{
            $count_json_vsites=$json_vsites.count
			}
            ##########################################################
            $json_Services=@()
			$data=@()
            ($data+=$main.Deploy.ENV.$environment.Services).foreach{


            $json_Services+=$_.PSobject.properties.Name

            }
			if (!($json_Services ))
			{
			$count_json_Services="0"
			}
			else{
            $count_json_Services=$json_Services.count
			}
		if($count_json_appPools -ne "0" -and $count_json_sites -eq "0" -and $count_json_vsites -eq "0" -and $count_json_Services -eq "0"  )
            {

						Write-error "Provide required sites info in Json"

            }

        elseif($count_json_appPools -eq "0" -and $count_json_sites -ne "0" -and $count_json_vsites -ne "0" -and $count_json_Services -ne "0" )
            {
						if ($configType -eq "Copy")
						{
						write-warning "Calling Deploy Config"
                        deploy-config -servers "$servers" -jsonpath "$env:CI_PROJECT_DIR/definition.json" -environment "$environment"
						}
						elseif($configType -eq "Transform")
						{
						write-warning "Calling Deploy Config Transform"
                        deploy-config-transform -configuration "$environment" -deleteAfterTransformation $false -sourcezip "$env:CI_PROJECT_DIR\Binaries\Services\$env:project.zip" -destination "$env:CI_PROJECT_DIR\$env:project" -destinationzip "$env:CI_PROJECT_DIR\Binaries\Web\$env:project"
						}
                        elseif($configType -eq "None")
						{
						write-warning "Config Type Choosen None"
                        }
						else{
						write-warning "Please provide Copy/Transform/None value for Config Type"
						exit 1
						}
						write-warning "Calling Service"
                        win-service -servers "$servers" -jsonpath "$env:CI_PROJECT_DIR/definition.json" -environment "$environment"
						
						

            }

            elseif($count_json_appPools -eq "0" -and $count_json_sites -ne "0" -and $count_json_vsites -ne "0" -and $count_json_Services -eq "0" )
            {
						Write-Host "Provide App Pool info in Json."	-ForegroundColor Red
                        exit 1					

            }
            elseif($count_json_appPools -eq "0" -and $count_json_sites -ne "0" -and $count_json_vsites -eq "0" -and $count_json_Services -eq "0" )
            {
						Write-Host "Provide App Pool info in Json."	-ForegroundColor Red
                        exit 1					

            }

        elseif($count_json_appPools -ne "0" -and $count_json_sites -ne "0" -and $count_json_vsites -eq "0" -and $count_json_Services -eq "0" )
            {
						if ($configType -eq "Copy")
						{
						write-warning "Calling Deploy Config"
                        deploy-config -servers "$servers" -jsonpath "$env:CI_PROJECT_DIR/definition.json" -environment "$environment"
						}
						elseif($configType -eq "Transform")
						{
						write-warning "Calling Deploy Config Transform"
                        deploy-config-transform -configuration "$environment" -deleteAfterTransformation $true -sourcezip $env:CI_PROJECT_DIR\Binaries\$env:project.zip -destination $env:CI_PROJECT_DIR\$env:project -destinationzip $env:CI_PROJECT_DIR\Binaries\Web\$env:project
						}
						elseif($configType -eq "None")
						{
						write-warning "Config Type Choosen None"
                        }
						else{
						write-warning "Please provide Copy/Transform/None value for Config Type"
						exit 1
						}
						write-warning "Calling web"
                        web-deploy -servers "$servers" -jsonpath "$env:CI_PROJECT_DIR/definition.json" -environment "$environment"
						

            }

        elseif($count_json_appPools -ne "0" -and $count_json_sites -eq "0" -and $count_json_vsites -ne "0" -and $count_json_Services -eq "0" )
            {

						Write-Host "Provide required sites info in Json" -ForegroundColor Red
                        exit 1

            }
        elseif($count_json_appPools -ne "0" -and $count_json_sites -eq "0" -and $count_json_vsites -ne "0" -and $count_json_Services -ne "0" )
            {
						if ($configType -eq "Copy")
						{
						write-warning "Calling Deploy Config"
                        deploy-config -servers "$servers" -jsonpath "$env:CI_PROJECT_DIR/definition.json" -environment "$environment"
						}
						elseif($configType -eq "Transform")
						{
						write-warning "Calling Deploy Config Transform"
                        deploy-config-transform -configuration "$environment" -deleteAfterTransformation $true -sourcezip $env:CI_PROJECT_DIR\Binaries\Services\$env:project.zip -destination $env:CI_PROJECT_DIR\$env:project -destinationzip $env:CI_PROJECT_DIR\Binaries\Web\$env:project
						}
						elseif($configType -eq "None")
						{
						write-warning "Config Type Choosen None"
                        }
						else{
						write-warning "Please provide Copy/Transform/None value for Config Type"
						exit 1
						}
						write-warning "Calling Service"
                        win-service -servers "$servers" -jsonpath "$env:CI_PROJECT_DIR/definition.json" -environment "$environment"
						

            }
        elseif($count_json_appPools -ne "0" -and $count_json_sites -ne "0" -and $count_json_vsites -ne "0" -and $count_json_Services -eq "0" )
            {
						if ($configType -eq "Copy")
						{
						write-warning "Calling Deploy Config"
                        deploy-config -servers "$servers" -jsonpath "$env:CI_PROJECT_DIR/definition.json" -environment "$environment"
						}
						elseif($configType -eq "Transform")
						{
						write-warning "Calling Deploy Config Transform"
                        deploy-config-transform -configuration "$environment" -deleteAfterTransformation $true -sourcezip $env:CI_PROJECT_DIR\Binaries\$env:project.zip -destination $env:CI_PROJECT_DIR\$env:project -destinationzip $env:CI_PROJECT_DIR\Binaries\Web\$env:project
						}
						elseif($configType -eq "None")
						{
						write-warning "Config Type Choosen None"
                        }
						else{
						write-warning "Please provide Copy/Transform/None value for Config Type"
						exit 1
						}
						write-warning "Calling web"
                        web-deploy -servers "$servers" -jsonpath "$env:CI_PROJECT_DIR/definition.json" -environment "$environment"
			            

            }
        elseif($count_json_appPools -eq "0" -and $count_json_sites -eq "0" -and $count_json_vsites -eq "0" -and $count_json_Services -ne "0" )
            {
						if ($configType -eq "Copy")
						{
						write-warning "Calling Deploy Config"
                        deploy-config -servers "$servers" -jsonpath "$env:CI_PROJECT_DIR/definition.json" -environment "$environment"
						}
						elseif($configType -eq "Transform")
						{
						write-warning "Calling Deploy Config Transform"
                        deploy-config-transform -configuration "$environment" -deleteAfterTransformation $true -sourcezip "$env:CI_PROJECT_DIR\Binaries\Services\$env:project.zip" -destination "$env:CI_PROJECT_DIR\$env:project" -destinationzip "$env:CI_PROJECT_DIR\Binaries\Web\$env:project"
						}
						elseif($configType -eq "None")
						{
						write-warning "Config Type Choosen None"
                        }
						else{
						write-warning "Please provide Copy/Transform/None value for Config Type"
						exit 1
						}
						write-warning "Calling Service"
                        win-service -servers "$servers" -jsonpath "$env:CI_PROJECT_DIR/definition.json" -environment "$environment"
                        

            }

        else {
						if ($configType -eq "Copy")
						{
						write-warning "Calling Deploy Config"
                        deploy-config -servers "$servers" -jsonpath "$env:CI_PROJECT_DIR/definition.json" -environment "$environment"
						}
						elseif($configType -eq "Transform")
						{
						write-warning "Calling Deploy Config Transform"
                        deploy-config-transform -configuration "$environment" -deleteAfterTransformation $true -sourcezip $env:CI_PROJECT_DIR\Binaries\Services\$env:project.zip -destination $env:CI_PROJECT_DIR\$env:project -destinationzip $env:CI_PROJECT_DIR\Binaries\Web\$env:project
						}
						elseif($configType -eq "None")
						{
						write-warning "Config Type Choosen None"
                        }
						else{
						write-warning "Please provide Copy/Transform/None value for Config Type"
						exit 1
						}
						write-warning "Calling web, service and config functions"
                        web-deploy -servers "$servers" -jsonpath "$env:CI_PROJECT_DIR/definition.json" -environment "$environment"
                        win-service -servers "$servers" -jsonpath "$env:CI_PROJECT_DIR/definition.json" -environment "$environment"
                        
            }			
