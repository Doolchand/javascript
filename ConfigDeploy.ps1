﻿param(

  [string[]]$servers,
  [string[]]$components,
  [string]$environment
)

$jsonpath="$env:CI_PROJECT_DIR/definition.json"
$main=(Get-Content $jsonpath|ConvertFrom-Json)

# get the detials 

$sites=@()
($data=$main.Deploy.ENV.$environment.Sites).foreach{


 $sites+=$_.PsObject.Properties.Name
}


$user="$env:username"
$SVCPWD="$env:password"
$PWd=ConvertTo-SecureString -String $SVCPWD -AsPlainText -Force
$cred=New-Object -TypeName System.Management.Automation.PSCredential $user, $PWd

# check the sites are present 

if( $sites -ne "" -and $sites -ne $null)
{

  foreach($item in $components)
  {
    if($sites -contains $item)
     {
       #$item="RPGMP-git"
       ($i=$main.Deploy.ENV.$environment.Sites)| Where-Object {$_.Name -eq $item}

          $CompName=$i.$item.Name
          $Dest=$i.$item.PhysicalPath.Replace("/","\")
          $Destpath=$Dest.replace(":","$")
          Write-Host "Copy $environment config for RPGMP API into $Destpath " -ForegroundColor Green



          
          foreach($srvr in $servers)
          {

          
           if(!([string]::IsNullOrEmpty($CompName)) -or !([string]::IsNullOrEmpty($Destpath)))
                    {                                                
                        Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock{iisreset /stop}
                    }  


            if((Test-Path "\\$srvr\$Destpath") -and (Test-Path $env:CI_PROJECT_DIR/Binaries/APIConfigs/$environment))
            {
             
              
              Copy-Item "$env:CI_PROJECT_DIR/Binaries/APIConfigs/$environment/*.*" "\\$srvr\$Destpath" -Force -Recurse -Verbose
               

            }

            else
            {
              
              if(Test-Path "\\$srvr\$Destpath")
              {
                write-host " destination path found \\$srvr\$Destpath" 

              }
              else
              {

                Write-Host " invlaid destination path \\$srvr\$Destpath" -ForegroundColor Red
                exit 1
              }

               if(Test-Path "$env:CI_PROJECT_DIR/Binaries/APIConfigs/$environment")
              {
                write-host " destination path found $env:CI_PROJECT_DIR/Binaries/APIConfigs/$environment" -ForegroundColor Green

              }
              else
              {

                Write-Host " invlaid destination path $env:CI_PROJECT_DIR/Binaries/APIConfigs/$environment" -ForegroundColor Red
                exit 1
              }



              
            }


            
           if(!([string]::IsNullOrEmpty($CompName)) -or !([string]::IsNullOrEmpty($Destpath)))
                    {                                                
                        Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock{iisreset /start}
                    }  

          }
          
     }

     
     else
     {
     # do nothing"
     }

  } # end foreach
}
else
{
 Write-Host " No sites information found in definition.json " -ForegroundColor Red
 exit 1
}