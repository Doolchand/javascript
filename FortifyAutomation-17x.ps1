﻿ #region Notes & Instructions
# This script is created and maintained by the Fortify Team - PLEASE DO NOT MAKE CHANGES TO IT WITHOUT REVIEW AND APPROVAL FROM THE FORTIFY TEAM
# Support & Contact : # Ambika_Adinarayanan@dell.com

# What does this Script do
    # 1. Gets the latest Rulepacks from the server
    # 2. Cleans the BuildId 
    # 3. Translates the Code 
    # 4. Scans the Code
    # 5. Uploads the FPR if required
    # 6. Generates Reports

# Requirements
    # 1. Build Infrastructure needs to be in place with latest version of Fortify SCA installed 
    # 2. All Fortify related activities will happen in D:\FortifyScan Folder. 
        # D:\FortifyScan\<ProjectName>\FortifySource\<BuildId> - Input directory
        # D:\FortifyScan\FortifyScripts - Copy Script here to execute
    # 3. Fortify Containers for the project has already been created in the fortify server to upload the FPRs 
    # 4. Build to be running on a service account
    # 5. Access to upload and upload token has to be available for the service account
    # 6. Enviroment Variables to be set on the Build Server
        # JAVA_HOME
    # 7. Builds have to set to a WEEKLY frequency only

# Fortify Build Setup
    # Copy all the source code to the FortifySource folder within the D:\FortifyScan\<ProjectName>\FortifySource\<BuildId>
    # Copy all the libraries / JARs to the FortifySource folder within the D:\FortifyScan\<ProjectName>\FortifySource\References
    
# How to Integrate into Builds - Tested for TeamCity, Jenkins, TFS
    # Refer to https://cybersecurity.one.dell.com/CGE/Cons/SitePages/FortifyAutomation-HowTo.aspx 

# Input parameters to execute the script
    # [0]: ProjectName - to create a working folder
    # [1]: BuildId - to provide a build reference; can be %build.counter%"
    # [2]: Lang - .NET / Java / Others
    # [3]: NET_Java_Version - .NET Version / Java Version / PLSQL or TSQL for scripts and N for others
    # [4]: UploadFPR - Y / N - Yes or No to upload the FPR to the Fortify Server
        # [5]: SSCProjectVersionId 
        # [6]: SSCAuthTokenToUpload
    # [7]: MemoryAllocation  #-Xmx12G -Xss16M
    # [8]: ThreadAllocation  # -j 3
    # [9]: ExternalAssembliesRefPath

# Input String in Build PowerShell Command Window
# Syntax
    # "ProjectName" "BuildId" "Lang" "VS_Java_Version" "Upload" "SSCProjectContainerName" "SSCProjectSolutionContainerName" "SSCAuthTokenToUpload" "MemoryAllocation" "ThreadAllocation"
# Example
    # Java: "ABC" "24" "Java" "1.7" "N" "N" "N" "N" "N" "N"
    # Java: "ABC" "24" "Java" "1.7" "Y" "12345" "0e66d36d-ab24-4aa7-bf10-4a97b041cee1" "N" "N" "N"
    # .NET: "ABC" "Dev_12.45" ".NET" "12.0" "Y" "12345" "0e66d36d-ab24-4aa7-bf10-4a97b041cee1" "N" "N" "N"
    # MemoryOptions: "ABC" "24" "Java" "1.7" "N" "N" "N" "-Xmx4G" "2" "N"
    # ExternalAssembliesRefPath: "ABC" "24" "Java" "1.7" "N" "N" "N" "-Xmx12G" "-j 2" "D:\Workspace\Bin"
#endregion Notes & Instructions



# FORTIFY AUTOMATION - Begin Script 
    #region Variable Declarations
    # Argument List 
    $ProjectName = $args[0];
    $BuildId =  $args[1];  
    $Lang = $args[2]; 
    $NET_Java_Version = $args[3]; 
    $UploadFPR = $args[4]; 
    $SSCProjectVersionId = $args[5]; 
    $SSCAuthTokenToUpload = $args[6];
    $MemoryAllocation = $args[7]; 
    $ThreadAllocation = $args[8]; 
    $ExternalAssembliesRefPath = $args[9] + ";";
    $ScanDrivePath = $args[10];
    $FortifySln = $args[11];


    if (($MemoryAllocation -eq "N") -or ($MemoryAllocation -eq "n") -or ($MemoryAllocation -eq ""))
    {
        $MemoryAllocation = "";
    }
    if (($ThreadAllocation -eq "N") -or ($ThreadAllocation -eq "n") -or ($ThreadAllocation -eq ""))
    {
        $ThreadAllocation = "";
    }
    if (($ScanDrivePath -eq $null) -or ($ScanDrivePath -eq "N") -or ($ScanDrivePath -eq "n") -or  ($ScanDrivePath -eq ""))
    {
        $ScanDrivePath = "D";
    }
    echo "Fortify Command: $ScanDrivePath\FortifyAutomation-Linux.sh $ProjectName $BuildId $Lang $NET_Java_Version $UploadFPR $SSCProjectVersionId $SSCAuthTokenToUpload $MemoryAllocation $ThreadAllocation $ExternalAssembliesRefPath $ScanDrivePath"
    
    
    # Dynamic Constants
    $FortifyBaseFolder = $ScanDrivePath + ":\FortifyScan"
    $FortifyProjectFolder = $FortifyBaseFolder + "\" + $projectName  
    # MS Assemblies and Java JARs
    $MSNetAssemblies = "C:\Windows\Microsoft.NET\Framework64\v4.0.30319;" # MS Framework Folder
    $JavaAssemblies = "C:\Program Files (x86)\Java\jre1.8.0_45\**/*;" # Java JDK / SDK Folder  # $JavaAssemblies = $env:JAVA_HOME;
    $JavaHome = $env:JAVA_HOME
    
    # Constants
    $CurrentDate = (Get-Date).ToString('dd-MM-yyyy-HH-mm')
    $SSCURL="https://fortify.dell.com/ssc"


    #testData
    #$CurrentDate = "04-09-2017-06-34"
    $ScanCode = "Y"
    
    $FortifySource = $FortifyProjectFolder + "\FortifySource\" + $buildId + "\" # + ";" Adding ; for Fortify Source is causing issue in Java Scan
    $AllReferences = $FortifyProjectFolder + "\FortifySource\References\"
    
    # File Initializations logs
    $FortifyLog = $FortifyProjectFolder + "\" + $ProjectName + "_fortify_" + $CurrentDate + ".log"
    $TransLogFile = $FortifyProjectFolder + "\" + $ProjectName + "_trans_" + $CurrentDate + ".log"
	$ScanLogFile = $FortifyProjectFolder + "\" + $ProjectName + "_scan_" + $CurrentDate + ".log"

    $FortifyFileList = $FortifyProjectFolder + "\ScanFileList_" + $CurrentDate + ".log"
	
    $FortifyCurrentFPRName = $FortifyProjectFolder + "\" + $ProjectName + "_" + $CurrentDate + "_Current" + ".fpr"
	$FortifyLastFPRName = $FortifyProjectFolder + "\" + $ProjectName + "_" + $CurrentDate + "_Last" + ".fpr"
    $FortifyMergedFPRName = $FortifyProjectFolder + "\" + $ProjectName + "_" + $CurrentDate + ".fpr"

    $FortifyReportName = $FortifyProjectFolder + "\" + $ProjectName + "_" + $CurrentDate + ".xml"
    $FortifyTempFolder = $FortifyProjectFolder 
    $FortifyTempReportName = $FortifyProjectFolder + "\" + $ProjectName + "_temp-" + $CurrentDate + ".xml"
    $FortifyHTMLReportName = $FortifyProjectFolder + "\" + $ProjectName + "_" + $CurrentDate + ".html"

    echo $FortifyLog

    # Method to Log data into the Fortify Log File
    function LogData($string)
    {
        if (!(Test-Path $FortifyProjectFolder))
        {
            $FortifyLog = $FortifyBaseFolder + "\" + $ProjectName + "_fortify_" + $CurrentDate + ".log"
        }
        $string | out-file -Filepath $FortifyLog -append
    }


    # method to log Script Usage data
    function LogScriptData()
    {
        $LogFolder = "\\ausdwfotfyscn01.aus.amer.dell.com\FortifyScriptLog\FA_AppLog\"
        $LogFileName = $LogFolder + $ProjectName + "_" + $BuildId + "_" + $Lang + "_" + $NET_Java_Version + "_" + $UploadFPR + "_" + $SSCProjectVersionId + "_" + $CurrentDate + ".log"
        echo $LogFileName
            "Logging Script Information ---- " | out-file -Filepath $LogFileName -append -noclobber
        $LogFileData = $ProjectName + "_" + $BuildId + "_" + $Lang + "_" + $NET_Java_Version + "_" + $UploadFPR + "_" + $SSCProjectVersionId + "_" + $SSCAuthTokenToUpload + "_" + $MemoryAllocation + "_" + $ThreadAllocation + "_" + $ExternalAssembliesRefPath
            $LogFileData | out-file -Filepath $LogFileName -append -noclobber
        $LogUserData = "Server - " + $env:computername + "  ;  User - " + $env:username
            $LogUserData | out-file -Filepath $LogFileName -append -noclobber
    }


    # LOGGING Scirpt usage 
    Echo "----------Logging Script Data-------------------"
    #LogScriptData 
    echo "-----------Script Data Logged------------------\n\n"

    LogData "FortifyAutomation.ps1 $args"
    echo "FortifyAutomation.ps1 $args"

    if (($SSCProjectVersionId -eq "") -or ($SSCProjectVersionId -eq "n") -or ($SSCProjectVersionId -eq "N") -or ($SSCProjectVersionId -eq "NO") -or ($SSCAuthTokenToUpload -eq "") -or ($SSCAuthTokenToUpload -eq "n") -or ($SSCAuthTokenToUpload -eq "N") -or ($SSCAuthTokenToUpload -eq "NO"))
    {
        LogData "Project Version ID and/or Token not present. Scan will be done without reporting."
        $ReportingEnabled="N"
    }
    else
    {
        LogData "Project Version Id present; Reports will be generated, proceeding with scan"
        $ReportingEnabled="Y"
        # test data
        #$ScanCode="N"
    }
    LogData "ReportingEnabled -- $ReportingEnabled"
    LogData "ScanCode -- $ScanCode"

    if (!(Test-Path $FortifySource))
    {
        LogData "Fortify Source Not Available - $FortifySource"
    }
    else
    { 
        if ( ($ProjectName -eq "") -or ($ProjectName -eq "N") -or ($ProjectName -eq "n") -or 
             ($BuildId -eq "") -or ($BuildId -eq "N") -or ($BuildId -eq "n") -or 
             ($Lang -eq "") -or ($Lang -eq "n") -or ($Lang -eq "N") -or
             ($NET_Java_Version -eq "") -or #($NET_Java_Version -eq "N") -or ($NET_Java_Version -eq "n") -or
             ($SSCProjectVersionId -eq "") -or ($SSCProjectVersionId -eq "N") -or ($SSCProjectVersionId -eq "n") -or
             ($SSCAuthTokenToUpload -eq "") -or ($SSCAuthTokenToUpload -eq "n") -or ($SSCAuthTokenToUpload -eq "N") 
            )
        {
            LogData "Mandatory fields not entered - ProjectName, BuildId, Lang , NET_Java_Version, SSCProjectVersionId, SSCAuthTokenToUpload"
        }
        else 
        {
            $langArray = $Lang.split(';')
            $versionArray = $NET_Java_Version.split(';')

            if( @($langArray).count -ne  @($versionArray).count)
            {
                LogData "The number of languages and the number of associated versions do not match - Please verify"
                LogData "Lang = $Lang"
                LogData "NET_Java_Version = $NET_Java_Version"
            }
            else
            {
                # LOGGING VALUES           
                #region LogData
                LogData "-----------------------------"
                LogData "    Initialized Values..."
                LogData "-----------------------------\n\n"
                LogData "    Argument List"
                LogData "-----------------------------"
                # Argument List 
                LogData "ProjectName = $ProjectName";
                LogData "BuildId = $BuildId";  
                LogData "Lang = $Lang"; 
                LogData "NET_Java_Version = $NET_Java_Version";
                LogData "UploadFPR = $UploadFPR ";
                LogData "SSCProjectVersionId = $SSCProjectVersionId "; 
                LogData "SSCAuthTokenToUpload = $SSCAuthTokenToUpload ";
                LogData "MemoryAllocation = $MemoryAllocation "; 
                LogData "ThreadAllocation =$ThreadAllocation ";
                LogData "ExternalAssembliesRefPath = $ExternalAssembliesRefPath ";
                LogData "ScanDrivePath = $ScanDrivePath";
                LogData "-----------------------------"
                LogData "    Constants List"
                LogData "-----------------------------"
                # Constants        
                LogData "FortifyBaseFolder = $FortifyBaseFolder"
                LogData "FortifyProjectFolder = $FortifyProjectFolder "
                LogData "CurrentDate = $CurrentDate"
                LogData "SSCURL= $SSCURL"
                LogData "JavaHome = $JavaHome";
                LogData "FortifySource = $FortifySource "
                LogData "AllReferences = $AllReferences"

                # MS Assemblies and Java JARs
                LogData "MSNetAssemblies = $MSNetAssemblies"
                LogData "JavaAssemblies = $JavaAssemblies"

                # logs
                LogData "FortifyLog = $FortifyLog"
                LogData "TransLogFile = $TransLogFile"
	            LogData "ScanLogFile = $ScanLogFile"
	
                LogData "FortifyCurrentFPRName = $FortifyCurrentFPRName "
	            LogData "FortifyReportName = $FortifyReportName "
                LogData "FortifyFileList = $FortifyFileList "
	            LogData "FortifyTempFolder = $FortifyTempFolder"
                LogData "FortifyLastFPRName = $FortifyLastFPRName"
	            LogData "FortifyTempReportName = $FortifyTempReportName"
                LogData "-----------------------------"

                #endregion LogData

                if($ReportingEnabled -eq "Y")
                {
                    LogData "Reporting is enabled, downloading last FPR from server..."
                    fortifyclient -url "$SSCURL" -authtoken "$SSCAuthTokenToUpload" downloadFPR -file "$FortifyLastFPRName" -applicationVersionID $SSCProjectVersionId
                    LogData "LastExitCode: " $LASTEXITCODE
                    if (Test-Path $FortifyLastFPRName) {
                          LogData $FortifyLastFPRName found and downloaded
                        }
                        else
                        {
                            LogData $FortifyLastFPRName not found
                            $NoLastFile = "Y"
                        }
                }

                if($ScanCode -eq "Y")
                {
                    #region FORTIFYSCAN
                    LogData "---------- Begin FORTIFY Scan -----------------"
                        # UPDATE RULEPACKS
                        #region UpdateRulepacks
                        LogData "------ Updating Rulepacks ------"
                        try
                        {
                            fortifyupdate.cmd -url $SSCURL -acceptKey
                            LogData "LastExitCode: " $LASTEXITCODE
                            if($LASTEXITCODE -ne 0)
                            {
                                LogData "Rulepack update failed! "
                            }
                        }
                        catch
                        {
                            LogData "------ RULEPACK UPDATE FAILED!!"
                        }
                        LogData "------ Rulepacks Updated ------"
                        #endregion UpdateRulepacks

                        # CLEAN
                        #region FortifyClean1
                    LogData "Starting Clean..."
                    LogData "sourceanalyzer.exe -b $buildId -clean"
                        sourceanalyzer.exe -b $buildId -clean
                        LogData "LastExitCode: " $LASTEXITCODE
                    LogData "--- Clean done ---"
                    #endregion FortifyClean1

                        #TRANSLATE
                        #region FortifyTranslation
                        foreach ($l in $langArray)
                        {
                            LogData "Starting Translation phase for $l..."
    
                            if (($Lang -eq ".NET") -or ($Lang -eq ".net") -or ($Lang -eq ".Net"))
                            {
                                LogData "----- .NET SCAN --------"
                                #sourceanalyzer -b mybuild -dotnet-version 4.5 -libdirs 3rdParty\Lib myCode\*.cs
                                LogData "sourceanalyzer.exe -b $buildId -dotnet-version $NET_Java_Version -libdirs $AllReferences;$ExternalAssembliesRefPath;$FortifySource; $MemoryAllocation $ThreadAllocation -debug -verbose -logfile $transLogFile $FortifySource"
                                    sourceanalyzer.exe -b "$buildId" -dotnet-version "$NET_Java_Version" -libdirs "$AllReferences$ExternalAssembliesRefPath$FortifySource;" $MemoryAllocation $ThreadAllocation -debug -verbose -logfile "$transLogFile" "$FortifySource"
                                #    LogData "LastExitCode: " $LASTEXITCODE
                                #LogData "sourceanalyzer.exe -b $buildId -dotnet-version $NET_Java_Version -libdirs $AllReferences;$ExternalAssembliesRefPath;$FortifySource; -debug -verbose -logfile $transLogFile -project-root $FortifySource msbuild /t:rebuild $FortifySln"
                                #    sourceanalyzer.exe -b "$buildId" -dotnet-version "$NET_Java_Version" -libdirs "$AllReferences$ExternalAssembliesRefPath$FortifySource;"  -debug -verbose -logfile "$transLogFile" -project-root "$FortifySource" msbuild /t:rebuild "$FortifySln"
                                
                            }
                            else
                            {
                                if (($Lang -eq "Java") -or ($Lang -eq "JAVA") -or ($Lang -eq "java"))
                                {
                                    LogData "----- JAVA SCAN --------"
                                    #sourceanalyzer -b mybuild -cp lib/dependency.jar "src/**/*.java"
                                    LogData "sourceanalyzer.exe -b $buildId -source $NET_Java_Version -cp $JavaHome$JavaAssemblies$AllReferences$ExternalAssembliesRefPath$FortifySource; -mt -debug -verbose -logfile $transLogFile $FortifySource"
                                        sourceanalyzer.exe -b "$buildId" -source "$NET_Java_Version" -cp "$JavaHome$JavaAssemblies$AllReferences$ExternalAssembliesRefPath$FortifySource;" -mt -debug -verbose -logfile "$transLogFile" "$FortifySource"
                                        #-extdirs
                                        LogData "LastExitCode: " $LASTEXITCODE
                                }
                                else
                                {
                                    if (($Lang -eq "Py") -or ($Lang -eq "PY") -or ($Lang -eq "python") -or ($Lang -eq "PYTHON"))
                                    {
                                        LogData "----- PYTHON SCAN --------"
                                        LogData "sourceanalyzer.exe -b $buildId -source $NET_Java_Version -mt -debug -verbose -logfile $transLogFile $FortifySource"
                                            sourceanalyzer.exe -b "$buildId" -source "$NET_Java_Version" -mt -debug -verbose -logfile "$transLogFile" "$FortifySource"
                                            LogData "LastExitCode: " $LASTEXITCODE
                                    }
                                    else
                                    {
                                        if (($Lang -eq "XCode") -or ($Lang -eq "XCODE") -or ($Lang -eq "xcode"))
                                        {
                                            LogData "----- XCODE SCAN --------"
                                            #sourceanalyzer -b mybuild xcodebuild -project myproject.xcodeproj
                                            LogData "sourceanalyzer.exe -b $buildId -mt -debug -verbose -logfile $transLogFile xcodebuild -project  $FortifySource"
                                                sourceanalyzer.exe -b "$buildId" -mt -debug -verbose -logfile "$transLogFile" xcodebuild -project "$FortifySource"
                                                LogData "LastExitCode: " $LASTEXITCODE
                                        }
                                        else
                                        {
                                            if (($Lang -eq "SQL") -or ($Lang -eq "sql") -or ($Lang -eq "Sql"))
                                            {
                                                LogData "----- SQL SCAN --------"
                                                #sourceanalyzer -b mybuild -Dcom.fortify.sca.fileextensions.sql=PLSQL -sql-language PLSQL *.sql
                                                #sourceanalyzer -b mybuild -Dcom.fortify.sca.fileextensions.sql=TSQL -sql-language TSQL *.sql
                                                LogData "sourceanalyzer.exe -b $buildId -mt -debug -verbose -logfile $transLogFile -Dcom.fortify.sca.fileextensions.sql=$NET_Java_Version -sql-language $NET_Java_Version $FortifySource"
                                                    sourceanalyzer.exe -b "$buildId" -mt -debug -verbose -logfile "$transLogFile" -Dcom.fortify.sca.fileextensions.sql=$NET_Java_Version -sql-language $NET_Java_Version "$FortifySource"
                                                    LogData "LastExitCode: " $LASTEXITCODE
                                            }
                                            else
                                            {
                                                LogData "----- OTHER SCAN --------"
                                                #sourceanalyzer -b mybuild .
                                                LogData "sourceanalyzer.exe -b $buildId -mt -debug -verbose -logfile $transLogFile $FortifySource"
                                                sourceanalyzer.exe -b "$buildId" -mt -debug -verbose -logfile "$transLogFile" "$FortifySource"
                                                LogData "LastExitCode: " $LASTEXITCODE
                                            }
                                        }
                                    }
                                }
                            }
                            LogData "--- $l Translate done ---"
                        }
                        #endregion FortifyTranslation

                        # DELETE OLD FPR FILE, IF EXISTS
                        if (Test-Path $FortifyCurrentFPRName) {
                          Remove-Item $FortifyCurrentFPRName
                        }

                        # SCAN
                        #region FortifyScan
                        LogData "Starting Scan Phase..."
                        LogData "sourceanalyzer.exe -b $buildId -debug -verbose -logfile $scanLogFile -scan $MemoryAllocation $ThreadAllocation -f $FortifyCurrentFPRName"
                            sourceanalyzer.exe -b "$buildId" -debug -verbose -logfile "$scanLogFile" -scan $MemoryAllocation $ThreadAllocation -f "$FortifyCurrentFPRName"  
                            LogData "LastExitCode: " $LASTEXITCODE
                        LogData "--- Scan done ---"
                        #endregion FortifyScan

                        # LOG ALL FILES THAT WERE PART OF THE SCAN
                        #region FortifyShowFiles
                        LogData "Logging list of files that have been scanned..."
                        LogData "sourceanalyzer.exe -b $buildId -show-files > $FortifyFileList"
                            sourceanalyzer.exe -b "$buildId" -show-files > "$FortifyFileList"
                            LogData "LastExitCode: " $LASTEXITCODE  
                        LogData "--- List of files logged ---"
                        #endregion FortifyShowFiles
            	
                        # UPLOAD FPR	
                        #region FortifyScanUpload
                        if (($UploadFPR -eq "y") -or ($UploadFPR -eq "Y") -or ($UploadFPR -eq "yes") -or ($UploadFPR -eq "Yes") -or ($UploadFPR -eq "YES"))
                        {
	                        LogData "Starting upload..."\
                            LogData "fortifyclient -url $SSCURL -authtoken $SSCAuthTokenToUpload uploadFPR -file $FortifyCurrentFPRName -applicationVersionID $SSCProjectVersionId" 
                            try
                            {
	                            fortifyclient -url "$SSCURL" -authtoken "$SSCAuthTokenToUpload" uploadFPR -file "$FortifyCurrentFPRName" -applicationVersionID $SSCProjectVersionId 
                                LogData "LastExitCode: " $LASTEXITCODE
	                        }
                            catch
                            {
                                LogData "UPLOAD FAILED!!"
                            }
                            LogData "Scan has been uploaded to SSC!"
                        }
                        else
                        {
	                        LogData "Upload option not enabled; Scan report not uploaded!"
                        }
                        #endregion FortifyScanUpload

                        # CLEAN
                        #region FortifyClean2
                        LogData "Starting Clean..."
                        LogData "sourceanalyzer.exe -b $buildId -clean"
                            sourceanalyzer.exe -b "$buildId" -clean
                        LogData "--- Clean done ---"
                        #endregion FortifyClean2

                    LogData "---------- FORTIFY Scan Completed -----------------"
                    #endregion FORTIFYSCAN
                }
                
                
                # REPORTING
                if($ReportingEnabled -eq "Y")
                {
                    LogData "Reporting is enabled, downloading last FPR from server..."
                    #fortifyclient -url "$SSCURL" -authtoken "$SSCAuthTokenToUpload" downloadFPR -file "$FortifyLastFPRName" -applicationVersionID $SSCProjectVersionId
                    LogData "LastExitCode: " $LASTEXITCODE
                    LogData "Last fpr downloaded"

                    if ((Test-Path $FortifyLastFPRName) -and (Test-Path $FortifyCurrentFPRName))
                    {
                        LogData "Last and Current FPR Files found. Proceeding with Merge."
                        FPRUtility -merge -project $FortifyCurrentFPRName -source $FortifyLastFPRName -f $FortifyMergedFPRName
                        LogData "LastExitCode: " $LASTEXITCODE
                        
                        LogData "Merging done. Proceeding with Report Generation..."
                            
                            ReportGenerator.bat -format xml -f $FortifyReportName -source $FortifyMergedFPRName
                            LogData "LastExitCode: " $LASTEXITCODE

                                #region GetContentFromMergedFPR
                                [xml]$xmlContent = Get-Content $FortifyReportName

                                $ProjSummary=$xmlContent.ReportDefinition.ReportSection[1]
                                $CodeInfo = $ProjSummary.SubSection[0].Text
                                $ScanInfo = $ProjSummary.SubSection[1].Text

                                $CodeInfoArr = @()
                                $CodeInfoArr = $CodeInfo.Split("`n");
                                $CodeInfoArr
                                $CurrentFileCount = $CodeInfoArr[1]
                                $CurrentFileCount=$CurrentFileCount.Replace("Number of Files: ", '')
                                $CurrentFileCount
                                $CurrentFileCount=[int]$CurrentFileCount
                                $CurrentFileCount
                
                                $ScanInfoArr = @()
                                $ScanInfoArr = $ScanInfo.Split("`n")

                                $style="<H2>Fortify Automated Build Findings Report</H2>"
                                $style = $style + "<table> <tr > <td width='20%' > Code Summary </td> <td width='80%' > <table> <tr> <td> " + $CodeInfoArr[0] + " </td> </tr> <tr> <td> " + $CodeInfoArr[1] + " </td> </tr> <tr> <td> " + $CodeInfoArr[2] + " </td> </tr> <tr> <td> " + $CodeInfoArr[3] + " </td> </tr></table> </td> </tr> "
                                $style = $style +        " <tr > <td width='20%' > Scan Summary </td> <td width='80%' > <table> <tr> <td> " + $ScanInfoArr[0] + " </td> </tr> <tr> <td> " + $ScanInfoArr[1] + " </td> </tr> <tr> <td> " + $ScanInfoArr[2] + " </td> </tr> <tr> <td> " + $ScanInfoArr[3] + " </td> </tr></table> </td> </tr> "
                                $style = $style + " <tr> <td > </td> <td > </td> </tr>  <tr> <td > </td> <td > </td> </tr> "
                
                                $IssueSummary=$xmlContent.ReportDefinition.ReportSection.SubSection[1].IssueListing.Chart.GroupingSection
                                $IssueSummary=$IssueSummary | sort { $_.groupTitle }
                                #endregion GetContentFromMergedFPR

                            ReportGenerator.bat -format xml -f $FortifyTempReportName -source $FortifyLastFPRName
                            LogData "LastExitCode: " $LASTEXITCODE

                                #region GetContentFromLastFPR
                                [xml]$xmlContentTemp = Get-Content $FortifyTempReportName

                                $ProjSummaryPrev=$xmlContentTemp.ReportDefinition.ReportSection[1]
                                $CodeInfoPrev = $ProjSummaryPrev.SubSection[0].Text
                                $ScanInfoPrev = $ProjSummaryPrev.SubSection[1].Text

                                $CodeInfoPrevArr = @()
                                $CodeInfoPrevArr = $CodeInfoPrev.Split("`n");
                                $CodeInfoPrevArr
                                $PrevFileCount = $CodeInfoPrevArr[1]
                                $PrevFileCount=$PrevFileCount.Replace("Number of Files: ", '')
                                $PrevFileCount=[int]$PrevFileCount
                                $PrevFileCount
                
                                $ScanInfoPrevArr = @()
                                $ScanInfoPrevArr = $ScanInfoPrev.Split("`n")

                                #$style = $style + "<table> <tr > <td width='20%' > Code Summary </td> <td width='80%' > $CodeInfo </td> </tr> "
                                $style = $style + "<tr > <td width='20%' > Previous Scan Code Summary </td> <td width='80%' > <table> <tr> <td> " + $CodeInfoPrevArr[0] + " </td> </tr> <tr> <td> " + $CodeInfoPrevArr[1] + " </td> </tr> <tr> <td> " + $CodeInfoPrevArr[2] + " </td> </tr> <tr> <td> " + $CodeInfoPrevArr[3] + " </td> </tr></table> </td> </tr> "
                                $style = $style + "<tr > <td width='20%' > Previous Scan Summary </td> <td width='80%' > <table> <tr> <td> " + $ScanInfoPrevArr[0] + " </td> </tr> <tr> <td> " + $ScanInfoPrevArr[1] + " </td> </tr> <tr> <td> " + $ScanInfoPrevArr[2] + " </td> </tr> <tr> <td> " + $ScanInfoPrevArr[3] + " </td> </tr></table> </td> </tr> "
                                $style = $style + " <tr> <td > </td> <td > </td> </tr>  <tr> <td > </td> <td > </td> </tr> "
                
                        
                                $style = $style + " <tr > <td width='20%'> Issue Summary: </td> <td width='80%'> </td> </tr> "
                                $style = $style + " <tr > <td colspan=2> <table style='border: 1px solid black;'> "

                                $IssueSummaryPrev=$xmlContentTemp.ReportDefinition.ReportSection.SubSection[1].IssueListing.Chart.GroupingSection
                                $IssueSummaryPrev=$IssueSummaryPrev | sort { $_.groupTitle }
                                #endregion GetContentFromLastFPR

                            $DeltaFileCount=$CurrentFileCount - $PrevFileCount

                            $style = $style + " <tr style='border: 1px solid black; font-weight:bold'> <td width='20%' style='border: 1px solid black;'>#</td> <td width='20%' style='border: 1px solid black;'> Category </td> <td width='20%' style='border: 1px solid black;'> Current Scan &nbsp;&nbsp;&nbsp; </td> <td width='20%' style='border: 1px solid black;'> Previous Scan &nbsp;&nbsp;&nbsp; </td> <td width='20%' style='border: 1px solid black;'> Difference </td>  </tr> "

                            $style = $style + " <tr style='border: 1px solid black; font-weight:bold'> <td width='40%' colspan='2' style='border: 1px solid black; textalign: right'> FILE COUNT </td> <td width='20%' style='border: 1px solid black; align: center'> " + $CurrentFileCount + "&nbsp;&nbsp;&nbsp; </td> <td width='20%' style='border: 1px solid black; align: center'> "+ $PrevFileCount +" </td> <td width='20%' style='border: 1px solid black;'> " + ($CurrentFileCount - $PrevFileCount) + " </td>  </tr> "

                            #check for each variable and assign to a new variable
                            
                            #region SetIssueCountForEachCategoryInMergedReport
                            
                            $i=0
                            $IssueCount = @(0,0,0,0)
                            $LastIssueCount = @(0,0,0,0)
                            $Category = @("Critical","High","Medium","Low")
                            $Delta = @(0,0,0,0)
                            foreach($e in ($IssueSummary)) 
                            {

                                $CategoryName = $IssueSummary[$i].groupTitle
                                if($CategoryName -eq "Critical")
                                {
                                    $IssueCount[0] = $IssueSummary[$i].count
                                }
                                else
                                {
                                    if($CategoryName -eq "High")
                                    {
                                        $IssueCount[1] = $IssueSummary[$i].count
                                    }
                                    else
                                    {
                                        if($CategoryName -eq "Medium")
                                        {
                                            $IssueCount[2] = $IssueSummary[$i].count
                                        }
                                        else
                                        {
                                            if($CategoryName -eq "Low")
                                            {
                                                $IssueCount[3] = $IssueSummary[$i].count
                                            }
                                        }
                                    }
                                }

                                if (Test-Path $FortifyTempReportName) 
                                {
                                    $j=0
                                    foreach($prev in ($IssueSummaryPrev))
                                    {
                                        if($IssueSummaryPrev[$j].groupTitle -eq "Critical")
                                        {
                                            $LastIssueCount[0] = [int]$IssueSummaryPrev[$j].count
                                    
                                        }
                                        else
                                        {
                                            if($IssueSummaryPrev[$j].groupTitle -eq "High")
                                            {
                                                $LastIssueCount[1] = [int]$IssueSummaryPrev[$j].count
                                                #$Delta[1] = [int]$IssueCount[1] - [int]$LastIssueCount[1]
                                            }
                                            else
                                            {                                   
                                                if($IssueSummaryPrev[$j].groupTitle -eq "Medium")
                                                {
                                                    $LastIssueCount[2] = [int]$IssueSummaryPrev[$j].count
                                                    #$Delta[2] = [int]$IssueCount[2] - [int]$LastIssueCount[2]
                                                }
                                                else
                                                {
                                                    if($IssueSummaryPrev[$j].groupTitle -eq "Low")
                                                    {
                                                        $LastIssueCount[3] = [int]$IssueSummaryPrev[$j].count
                                                        #$Delta[3] = [int]$IssueCount[3] - [int]$LastIssueCount[3]
                                                    }
                                                }
                                            }
                                            }
                                        
                                        $j=$j+1
                                    }
                                }
                                $i=$i+1
                            }
                            #endregion SetIssueCountForEachCategoryInMergedReport
                            

                            for($i=0; $i -lt 4; $i++)
                            {
                                $Delta[$i] = [int]$IssueCount[$i] - [int]$LastIssueCount[$i]
                                (if($Delta[$i] -lt 0))
                                {
                                    $Delta[$i] = $Delta[$i] + (2*$Delta[$i])
                                }
                                $style = $style + " <tr style='border: 1px solid black;'> <td width='20%' style='border: 1px solid black;'>$i</td> <td width='20%' style='border: 1px solid black;'> " + $Category[$i] + " </td> <td width='20%' style='border: 1px solid black;'> " + $IssueCount[$i] + " </td> <td width='20%' style='border: 1px solid black;'> " + $LastIssueCount[$i] + " </td> <td width='20%' style='border: 1px solid black;'> " + $Delta[$i] + " </td>  </tr> "
                            }
                            $style = $style + " </table> </td></tr></table>"

                            #region PieChart
                            #    # load the appropriate assemblies 
                            #    [void][Reflection.Assembly]::LoadWithPartialName(“System.Windows.Forms”) 
                            #    [void][Reflection.Assembly]::LoadWithPartialName(“System.Windows.Forms.DataVisualization")

                            #    # create chart object 
                            #    $Chart = New-object System.Windows.Forms.DataVisualization.Charting.Chart 
                            #    $Chart.Width = 500 
                            #    $Chart.Height = 400 
                            #    $Chart.Left = 40 
                            #    $Chart.Top = 30

                            #    # create a chartarea to draw on and add to chart 
                            #    $ChartArea = New-Object System.Windows.Forms.DataVisualization.Charting.ChartArea 
                            #    $Chart.ChartAreas.Add($ChartArea)
                            #    # add title and axes labels 
                            #    [void]$Chart.Titles.Add(“Issue Summary”) 
                            #    $ChartArea.AxisX.Title = “Issue Category” 
                            #    $ChartArea.AxisY.Title = “Issue Count”

                            #    # add data to chart 
                            #    $IssueCategory = New-Object System.Collections.Specialized.OrderedDictionary
                            #    #$IssueCategory.Add("Critical",$IssueCount[3])
                            #    #$IssueCategory.Add("High",$IssueCount[2])
                            #    #$IssueCategory.Add("Medium",$IssueCount[1])
                            #    #$IssueCategory.Add("Low",$IssueCount[0])
                    
                            #    $IssueCategory.Add("Low",$IssueCount[3])
                            #    $IssueCategory.Add("Medium",$IssueCount[2])
                            #    $IssueCategory.Add("High",$IssueCount[1])
                            #    $IssueCategory.Add("Critical",$IssueCount[0])
                    
                            #    [void]$Chart.Series.Add(“Data”) 
                            #    #$Chart.Series[“Data”].Points.DataBindXY($IssueCategory.Keys, $IssueCategory.Values)

                            #    $Chart.Series["Data"].ChartType = [System.Windows.Forms.DataVisualization.Charting.SeriesChartType]::StackedBar
                            #    $Chart.Series[“Data”].Points.DataBindXY($IssueCategory.Keys, $IssueCategory.Values)
                            #    for($i=3;$i -ge 0;$i--)
                            #    {
                            #        if($Chart.Series[“Data”].Points[$i].AxisLabel -eq "Critical")
                            #        {    
                            #            $Chart.Series[“Data”].Points[3].Color = [System.Drawing.Color]::Crimson
                            #            $Chart.Series[“Data”].Points[3].Label = $Chart.Series[“Data”].Points[3].YValues #$Chart.Series[“Data”].Points[$i].AxisLabel + ": " + $Chart.Series[“Data”].Points[$i].YValues
                            #        }
                            #        if($Chart.Series[“Data”].Points[$i].AxisLabel -eq "High")
                            #        {    
                            #            $Chart.Series[“Data”].Points[2].Color = [System.Drawing.Color]::OrangeRed
                            #            $Chart.Series[“Data”].Points[2].Label = $Chart.Series[“Data”].Points[2].YValues 
                            #        }
                            #        if($Chart.Series[“Data”].Points[$i].AxisLabel -eq "Medium")
                            #        {    
                            #            $Chart.Series[“Data”].Points[1].Color = [System.Drawing.Color]::Yellow
                            #            $Chart.Series[“Data”].Points[1].Label = $Chart.Series[“Data”].Points[1].YValues 
                            #        }
                            #        if($Chart.Series[“Data”].Points[$i].AxisLabel -eq "Low")
                            #        {    
                            #            $Chart.Series[“Data”].Points[0].Color = [System.Drawing.Color]::LightYellow
                            #            $Chart.Series[“Data”].Points[0].Label = $Chart.Series[“Data”].Points[0].YValues 
                            #        }
                            #    }

                            #    # change chart area colour 
                            #    $Chart.BackColor = [System.Drawing.Color]::Transparent
                            #    # make bars into 3d cylinders 
                            #    $Chart.Series[“Data”][“DrawingStyle”] = “Cylinder”


                            #    $Chart.Width = 1000

                                # save chart to file 
                            #    $FortifyIssueChart = $FortifyProjectFolder + "\Chart.png”
                            #    $Chart.SaveImage($FortifyIssueChart, “PNG”)

                            #    $images = Get-ChildItem $FortifyIssueChart
                            #    $ImageHTML = $images | % {
                            #      $ImageBits = [Convert]::ToBase64String((Get-Content $_ -Encoding Byte))
                            #      "<img src=data:image/png;base64,$($ImageBits) alt='My Image'/>"
                            #    }


                            #endregion PieChart
      
                        #ConvertTo-Html -Title "Fortify Automated Build - Scan Report" -PreContent $imageHTML -Body $style  | Out-File $FortifyHTMLReportName
                        ConvertTo-Html -Title "Fortify Automated Build - Scan Report" -Body $style  | Out-File $FortifyHTMLReportName
                        LogData "HTML Report generated; please verify"

                        if ((Test-Path $FortifyLastFPRName) -and (Test-Path $FortifyCurrentFPRName))
                        {
                            Remove-Item $FortifyLastFPRName
                            Remove-Item $FortifyCurrentFPRName
                            $XMLFiles = $FortifyProjectFolder + "\*.xml" 
                            Remove-Item $XMLFiles
                        }



                        LogData "LastExitCode: " $LASTEXITCODE

                    }


                }
            
            }
        }
    }


