param(
$servers,
$jsonpath,
$components,
$environment
)


if ($components -eq "Full")
    {
        $comDeploy = "False"
        $FullDeploy= "True"
}
else{
$comDeploy = "True"
$FullDeploy= "False"
}


$main=(Get-Content $jsonpath|ConvertFrom-Json)


#getting details from JSON

$sites=@()
($data=$main.Deploy.ENV.$environment.VirtualSites).foreach{


$sites+=$_.PSobject.properties.Name

#$CompVals=$_.PSobject.properties.Value
}

$appPools=@()
($data=$main.Deploy.ENV.$environment.AppPools).foreach{


$appPools+=$_.PSobject.properties.Name


}
$websites=@()
($data=$main.Deploy.ENV.$environment.sites).foreach{


$websites+=$_.PSobject.properties.Name

}
$user="$env:svcuser"

Write-host "$user"

Write-host "$env:svcpassword"
$SVCPWD="$env:svcpassword"
Write-host "$SVCPWD"
$PWd=ConvertTo-SecureString -String $SVCPWD -AsPlainText -Force
$cred=New-Object -TypeName System.Management.Automation.PSCredential $user, $PWd


# Component specific deployment 
if($comDeploy -ne $false)
{

foreach($item in $components)
{

if($sites -contains $item) 
  {
($i=$main.Deploy.ENV.$environment.VirtualSites)|Where-Object{$_.name -eq $item}


    $compName=$i.$item.Name
    $SiteName=$i.$item.Sitename
    $AppPool=$i.$item.AppPool

foreach($srvr in $servers){

if($srvr -ne "" -and (Test-Connection -cn $srvr -BufferSize 16 -Count 1 -ea 0 -quiet)){
Write-Host "**************Deployment workflow started in $srvr****************" 
    #Remotely connect to deployment servers 
    net use \\$srvr /user:$user $SVCPWD

# Add server to trust host
if(!@((Get-Item WSMan:\localhost\Client\TrustedHosts).value).Contains("*"))
                    {                        
                        Set-Item -Path WSMan:\localhost\Client\TrustedHosts -Value "*" -Force
                        "Machine added to Trusted-Hosts of agent server"
                    } 
  
 # Copy zip code    

    If ( Test-Path "\\$srvr\C$\temp\$item.zip" ){

    write-host " Removing old code zip and extracted code from temp in $srvr " -ForegroundColor Yellow
    Remove-Item "\\$srvr\C$\temp\$item.zip" -Force -Verbose
    Remove-Item "\\$srvr\C$\temp\$item" -Force -Recurse

    write-host " Copy code zip and extracted code to temp in $srvr " -ForegroundColor Green
    copy-item "Binaries\Web\$item.zip" "\\$srvr\C$\temp\" -Force -Recurse

    }

    else
    {
    write-host " Copy code zip and extracted code to temp in $srvr " -ForegroundColor Green
    copy-item "Binaries\Web\$item.zip" "\\$srvr\C$\temp\" -Force -Recurse
    }
    

if(!([string]::IsNullOrEmpty($AppPool)) -or !([string]::IsNullOrEmpty($SiteName)) -or !([string]::IsNullOrEmpty($compName)))
                    {                                                
                        Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock{iisreset /stop}
                    }  



  
Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock {
$Sitem=$using:item
$json=$using:main
$env=$using:environment
Import-Module WebAdministration 
Write-Host " working on deployment for $Sitem" -ForegroundColor Green
($i=$json.Deploy.ENV.$env.VirtualSites)|Where-Object{$_.name -eq $Sitem}


$compName=$i.$Sitem.Name
$SiteName=$i.$Sitem.Sitename
$AppPool=$i.$Sitem.AppPool
$PhysicalPath=$i.$Sitem.PhysicalPath
$AnonymousAuth=$i.$Sitem.AnonymousAuth
$WindowsAuth=$i.$Sitem.WindowsAuth
$AspNetImpersonation=$i.$Sitem.AspNetImpersonation


    Write-Host "**************************************** Working with AppPools ************************************"
    try{

        if(Test-Path ('IIS:\AppPools\'+ "$AppPool") -PathType Container) {

        Write-Warning "The App Pool $AppPool already exists"

        Remove-Item "IIS:\AppPools\$AppPool" -Force -Recurse -ErrorAction Stop

        Write-host "Delete appPool :" $AppPool -ForegroundColor Yellow

       
       Write-Host "Creating AppPool:" $AppPool -ForegroundColor Green
      
       ($J=$json.Deploy.ENV.$env.AppPools)|Where-Object{$_.Name -eq $AppPool}
      
       $AppPoolName=$J.$AppPool.Name
       Write-Host "$AppPoolName"
      
       $DotNetVersion=$j.$AppPool.DotNetVersion
       $enable32BitAppOnWin64=$j.$AppPool.Enable32BitApps
       $enable32Bit=[System.Convert]::ToBoolean($enable32BitAppOnWin64)
       $ManagedPipelineMode=$J.$AppPool.ManagedPipelineMode
       $QueueLength=$J.$AppPool.QueueLength
       $UserName=$J.$AppPool.UserName
       $password=$J.$AppPool.Password
      
      
        Write-Host "****************Creating AppPool : $AppPoolName ****************"
         $pool= New-WebAppPool $AppPoolName -Force -ErrorAction Stop
         if([string]::IsNullOrEmpty($appPool))
                                         {
                                            IISReset
                                            $Pool = Get-Item  IIS:\AppPools\$AppPoolName -Force
                                         }


      
                              #Set app pool properties
                                              
                                 
                                               if(!([string]::IsNullOrEmpty($UserName)))
                                               {
                                            if($UserName -ieq "LocalSystem")
                                            {
                                                #AppPool Identity Type is set to 'LocalSystem'
                                                
                                               $pool.processModel.identityType = 0
                                            }
                                            elseif($UserName -ieq "LocalService")
                                            {
                                                #AppPool Identity Type is set to 'LocalService'
                                                $pool.processModel.identityType = 1
                                            }
                                            elseif($UserName -ieq "NetworkService")
                                            {
                                                #AppPool Identity Type is set to 'NetworkService'
                                                $pool.processModel.identityType = 2
                                            }
                                            elseif($UserName -ieq "ApplicationPoolIdentity")
                                            {
                                                #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                                $pool.processModel.identityType = 4
                                            }
                                            elseif(!([string]::IsNullOrEmpty($password)))
                                            {
                                                #AppPool Identity Type is set to CustomUser. Use credentials provided in XML
                                                $pool.processModel.identityType = 3
                                                $pool.processModel.userName = $UserName
                                                $pool.processModel.password = $password
                                            }
                                            else
                                            {
                                                Write-Host " AppPool UserName or Password : Invalid. AppPool Identity is set to ApplicationPoolIdentity"
                                                #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                                $pool.processmodel.identityType = 4   
                                            }
                                         }
                                               else
                                               {
                                            #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                            $pool.processmodel.identityType = 4
                                         }                                         
                                               
                                               #Check if properties of AppPool component not empty and then assign. If empty default values would get assigned
                                               if(!([string]::IsNullOrEmpty($DotNetVersion)))
                                               {
                                                  if( $DotNetVersion -eq "No Managed Code")
                                                  {
                                                   $pool.managedRuntimeVersion = ""
                                                  }
                                                  else{
                                                   $pool.managedRuntimeVersion = $DotNetVersion
                                                  }
                                                  
                                               }
                                              
                                               
                                               if(!([string]::IsNullOrEmpty($enable32Bit)))
                                               {                                     
                                               $pool.enable32BitAppOnWin64 = $enable32Bit
                                         }
      
                                               if(!([string]::IsNullOrEmpty($ManagedPipelineMode)))
                                               {
                                              $pool.managedPipelineMode = $ManagedPipelineMode
                                               }
      
                                               if(!([string]::IsNullOrEmpty($QueueLength)))
                                               {
                                                  $pool.queueLength = [int]$QueueLength
                                               }
      
                                               
      
                                               $pool | Set-Item -Force -ErrorAction Stop  
                                               Write-Host "The App Pool $pool Created successfully" -ForegroundColor Green
                                                               
                                          }

        else{

        if(!(Test-Path ('IIS:\AppPools\'+ "$AppPool") -PathType Container)) {

        Write-Warning "The App Pool $AppPool does not exist"

        #Remove-Item "IIS:\AppPools\$AppPool" -Force -Recurse -ErrorAction Stop

        #Write-host "Delete appPool :" $AppPool -ForegroundColor Yellow

       
       Write-Host "Creating AppPool:" $AppPool -ForegroundColor Green
      
       ($J=$json.Deploy.ENV.$env.AppPools)|Where-Object{$_.Name -eq $AppPool}
      
       $AppPoolName=$J.$AppPool.Name
      
       $DotNetVersion=$j.$AppPool.DotNetVersion
       $enable32BitAppOnWin64=$j.$AppPool.Enable32BitApps
       $enable32Bit=[System.Convert]::ToBoolean($enable32BitAppOnWin64)
       $ManagedPipelineMode=$J.$AppPool.ManagedPipelineMode
       $QueueLength=$J.$AppPool.QueueLength
       $UserName=$J.$AppPool.UserName
       $password=$J.$AppPool.Password
      
      
        Write-Host "****************Creating AppPool : $AppPoolName ****************"
         $pool= New-WebAppPool $AppPoolName -Force -ErrorAction Stop
         if([string]::IsNullOrEmpty($appPool))
                                         {
                                            IISReset
                                            $Pool = Get-Item  IIS:\AppPools\$AppPoolName -Force
                                         }

      
                              #Set app pool properties
                                               if(!([string]::IsNullOrEmpty($UserName)))
                                               {
                                            if($UserName -ieq "LocalSystem")
                                            {
                                                #AppPool Identity Type is set to 'LocalSystem'
                                                
                                               $pool.processModel.identityType = 0
                                            }
                                            elseif($UserName -ieq "LocalService")
                                            {
                                                #AppPool Identity Type is set to 'LocalService'
                                                $pool.processModel.identityType = 1
                                            }
                                            elseif($UserName -ieq "NetworkService")
                                            {
                                                #AppPool Identity Type is set to 'NetworkService'
                                                $pool.processModel.identityType = 2
                                            }
                                            elseif($UserName -ieq "ApplicationPoolIdentity")
                                            {
                                                #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                                $pool.processModel.identityType = 4
                                            }
                                            elseif(!([string]::IsNullOrEmpty($password)))
                                            {
                                                #AppPool Identity Type is set to CustomUser. Use credentials provided in XML
                                                $pool.processModel.identityType = 3
                                                $pool.processModel.userName = $UserName
                                                $pool.processModel.password = $password
                                            }
                                            else
                                            {
                                                Write-Host " AppPool UserName or Password : Invalid. AppPool Identity is set to ApplicationPoolIdentity"
                                                #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                                $pool.processmodel.identityType = 4   
                                            }
                                         }
                                               else
                                               {
                                            #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                            $pool.processmodel.identityType = 4
                                         }                                         
                                               
                                               #Check if properties of AppPool component not empty and then assign. If empty default values would get assigned
                                               if(!([string]::IsNullOrEmpty($DotNetVersion)))
                                               {
                                                 if( $DotNetVersion -eq "No Managed Code")
                                                  {
                                                   $pool.managedRuntimeVersion = ""
                                                  }
                                                  else{
                                                   $pool.managedRuntimeVersion = $DotNetVersion
                                                  }
                                             }
                                               
                                               if(!([string]::IsNullOrEmpty($enable32Bit)))
                                               {                                     
                                               $pool.enable32BitAppOnWin64 = $enable32Bit
                                         }
      
                                               if(!([string]::IsNullOrEmpty($ManagedPipelineMode)))
                                               {
                                              $pool.managedPipelineMode = $ManagedPipelineMode
                                               }
      
                                               if(!([string]::IsNullOrEmpty($QueueLength)))
                                               {
                                                  $pool.queueLength = [int]$QueueLength
                                               }
      
                                               
      
                                               $pool | Set-Item -Force -ErrorAction Stop  
                                               Write-Host "The App Pool $pool Created successfully" -ForegroundColor Green
                                                               
                                          }
        }
      
}
catch{

        Write-Host  "Could not process AppPool $AppPool. Exception Messsage - $($error[0].Exception)"
        Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"
}# end AppPool
Write-Host "**************************************** END AppPools ********************************************"-ForegroundColor Green
# starting Try blcok for web sites
try{
        Write-Host "**************************************** Working with Sites ***************************************" -ForegroundColor Green

               
                # Createing website
                

                     if($SiteName -ne $null -and $SiteName -ne "")
                     {

                     Try{
                                    $appcmdPath = "$Env:SystemRoot\System32\inetsrv\appcmd.exe" 

                                   #Remove existing WebSite if the DeleteExisting option is set to True in xml inputs of that component
                                   if(!(Test-Path ("IIS:\Sites\" + "$SiteName") -PathType Container))
                                   {
                                        #Remove-Item "IIS:\Sites\$SiteName" -Force -Recurse -ErrorAction Stop
                                        #Write-Host "Deleted WebSite: $SiteName" -ForegroundColor Yellow
                                        Write-Host "Creating wesite $SiteName " -ForegroundColor Green
            
                                        Write-Host "****************WebSite: $Site****************"
                                   

                                          # featch site details from JSON
                                          
                                             ($k=$json.Deploy.ENV.$env.Sites)|Where-Object{$_.Name -eq $SiteName}

                                             
                                             $Site=$k.$SiteName.name
                                             $AppPool=$k.$SiteName.AppPool
                                             $BindingPort=$k.$SiteName.BindingPort
                                            $PhysicalPath=$k.$SiteName.PhysicalPath
                                             $EnabledProtocol=$k.$SiteName.EnabledProtocol
                                             $AnonymousAuth=$k.$SiteName.AnonymousAuth
                                             $WindowsAuth=$k.$SiteName.WindowsAuth
                                             $AspNetImpersonation=$k.$SiteName.AspNetImpersonation


                                            Write-Host "****************Creating WebSite : $Site****************"

                                            if(Test-Path ("IIS:\AppPools\"+"$($AppPool)") -PathType Container)
                                        { 
                                             $protocols=$($EnabledProtocol).Split(',').Split(';')  
                                             $bindInfo=$($BindingPort).Split(',').Split(';')                                             
                                             $bindings = @()
                                            $count = 0
                                             #Multiple Bindings for website will be formed here based on EnabledProtocol and BindingPort
                                             foreach($item in $protocols)
                                             {
                                                $bindings+= "@{protocol="+$item+";bindingInformation=$(@($bindInfo)[$count])}"  
                                                $count++
                                             }
                                            $tempPath = "C:\temp\"
                                             Write-Host "Extracting Packages to temp Directory and copy to App path" -ForegroundColor Yellow  
                          
                                            Add-Type -AssemblyName System.IO.Compression.FileSystem
                                              function Unzip
                                              {
                                                  param([string]$zipfile, [string]$outpath)
                                              
                                                  [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
                                              }
                                             
                                             if(test-path "$tempPath\$Site")
                                             {
                                              Remove-Item "$tempPath\$Site" -Force -Recurse
                                             }
                                              
                                            Unzip "$tempPath\$($Site).zip" "$tempPath\$Site"
                                            Copy-Item $tempPath\$Site\* $PhysicalPath -Recurse -Force -Verbose

                                            #Create a website                                                                                        
                                            New-Item "IIS:\Sites\$Site" -bindings $bindings -physicalPath $PhysicalPath.replace("/","\") -ApplicationPool $AppPool -ErrorAction Stop

                                            Try{
                                            #Apply Anonymous authentication setting
                                            if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                            {
                                                Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $AnonymousAuth -PSPath IIS:\ -Location "$Site"
                                                Write-Host "AnonymouseAuth setting updated for Site:$Site"
                                            }

                                            #Apply Windows authentication setting
                                            if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                            {
                                               
                                                if($WindowsAuth -ieq "windows")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq "basic")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq $false)
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Write-Host "WindowsAuth and BasicAuth are disabled for Site:$Site"                
                                                }
                                            }
                                
                                            #Apply ASP.NET Impersonation setting
                                            if(!([string]::IsNullOrEmpty($AspNetImpersonation)))
                                            {
                                                &$appcmdPath set config "$Site" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                                Write-Host "Asp.Net Impersonation enabled for Site:$Site" 
                                            }
                                            }
                                            Catch{
                                                Write-Host "Could not process Anonymous Authentication or Windows Authentication or AspNetImpersonation for WebSite:$Site. Exception Messsage - $($error[0].Exception)"
                                                Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"  
                                            } 
                                        }
                                        else
                                        {
                                            Write-Host "Could not process website $Site. As AppPool $($AppPool) not found"
                                             
                                        }    
                                    }
                                    else
                                    {
                                    Write-Warning "wesite $SiteName  already exist"
                                    }
                                        
                                    # done with site creatioin

                                    }
                                    catch
                                    {
                                      Write-Host "Could not process website $Site. Exception Messsage - $($error[0].Exception)"
                                      Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"
                                         
                                    }
                     }#end if for website





Write-Host "**************************************** END with Sites *******************************************"
}# end of Try blcok for web sites
catch{

    # Write-Host "Sites : Issue during deployment workflow in $ServerName. Exception Messsage - $($error[0].Exception)" 
    # Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)" 
 }

# starting Try blcok for web virtual sites

Try{
                                Write-Host "****************Root Virtual site: $compName****************"

                                if($compName -ne $null -and $compName -ne "")
                                {
                                ($i=$json.Deploy.ENV.$env.VirtualSites)|Where-Object{$_.name -eq $Sitem}


                                 $compName=$i.$Sitem.name
                                 $SiteName=$i.$Sitem.Sitename
                                 $AppPool=$i.$Sitem.AppPool
                                 $PhysicalPath=$i.$Sitem.PhysicalPath
                                 $AnonymousAuth=$i.$Sitem.AnonymousAuth
                                 $WindowsAuth=$i.$Sitem.WindowsAuth
                                 $AspNetImpersonation=$i.$Sitem.AspNetImpersonation
                                # Createing website
                                   $tempPath = "C:\temp\"

                                                               
                                $appcmdPath = "$Env:SystemRoot\System32\inetsrv\appcmd.exe"

                                $VDIRExists = $false

                                if(([string]::IsNullOrEmpty((get-item "IIS:\Sites\$($SiteName)\$compName" -ErrorAction Ignore).PhysicalPath)))
                                {
                                    $VDIRExists = $false
                                }
                                else
                                {
                                    $VDIRExists = $true
                                }

                                #Remove existing WebApp if the DeleteExisting option is set to True in xml inputs of that component
                                if($VDIRExists -ieq $true)
                                {
                                    Remove-Item "IIS:\Sites\$($SiteName)\$compName" -Force -Recurse -ErrorAction Stop                                                                                      
                                            
                                    #After deletion just reverify if webapp physical path attribute
                                    if(([string]::IsNullOrEmpty((get-item "IIS:\Sites\$($SiteName)\$compName" -ErrorAction Ignore).PhysicalPath)))
                                    {
                                        Write-Host "Deleted WebApp: $compName"
                                        $VDIRExists = $false
                                    }
                                    else
                                    {
                                        $VDIRExists = $true
                                    }
                                }  
                                
                                #Verify if WebApp does not exists and delete. Then extract from zip  
                                if (!($VDIRExists))
                                {           
                                   if((Test-Path "IIS:\Sites\$($SiteName)" -PathType Container) -and (Test-Path "IIS:\AppPools\$($AppPool)" -PathType Container))
                                    { 
                                    Write-Host "****************Creating WebApp : $($SiteName)/$compName****************"                        


                                          Add-Type -AssemblyName System.IO.Compression.FileSystem
                                              function Unzip
                                              {
                                                  param([string]$zipfile, [string]$outpath)
                                              
                                                  [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
                                              }
                                             
                                             if(test-path "$tempPath\$compName")
                                             {
                                              Remove-Item "$tempPath\$compName" -Force -Recurse
                                             }
                                              
                                            Unzip "$tempPath\$($compName).zip" "$tempPath\$compName"

                                             if(test-path "$PhysicalPath")
                                             {
                                              Remove-Item "$PhysicalPath" -Force -Recurse
                                             }
                                            Copy-Item $tempPath\$compname\* $PhysicalPath -Recurse -Force -Verbose
                                    
                                    New-Item IIS:\Sites\$($SiteName)\$compname -physicalPath "$($PhysicalPath)" -type Application -force -ErrorAction Stop
                                    #Set Application Pool
                                    Set-ItemProperty "IIS:\Sites\$($SiteName)\$App" -name applicationPool -value $AppPool -ErrorAction Stop


                                     Try{
                                    #Apply Anonymous authentication setting
                                    if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                    {
                                        Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $AnonymousAuth -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop
                                        Write-Host "AnonymouseAuth setting updated for App:$compName"
                                    }

                                    #Apply Windows authentication setting
                                    if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                   {
                                        if($WindowsAuth -ieq "windows")
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop              
                                            Write-Host "WindowsAuth enabled for App:$compName"
                                        }
                                        elseif($getApp.WindowsAuth -ieq "basic")
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop              
                                            Write-Host "BasicAuth enabled for App:$compName"
                                        }
                                        elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop                
                                            Write-Host "WindowsAuth enabled for App:$compName"
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop                
                                            Write-Host "BasicAuth enabled for App:$compName"
                                        }
                                        elseif($WindowsAuth -ieq $false)
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop   
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop                
                                            Write-Host "WindowsAuth and BasicAuth disabled for App:$compName"
                                        }

                                    }
                             
                                    #Apply ASP.NET Impersonation setting
                                    if($AspNetImpersonation)
                                    {
                                    
                                        &$appcmdPath set config "$($SiteName)/$compName" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                        Write-Host "Asp.Net Impersonation enabled for App:"$compname
                                    }                                            
                                          
                                 }# end of try block
                                 catch
                                 {
                                         
                                        Write-Host "Deleting default web.config if any in physical path. To avoid issues for setting Anonymous Authentication or Windows Authentication or AspNetImpersonation"
                                    $defaultConfig = $($PhysicalPath).trim('\').trim('/')
                                    if(Test-Path "$defaultConfig\web.config")
                                    {
                                        Remove-Item "$defaultConfig\web.config" -Force
                                    }  
                                    #Apply Anonymous authentication setting
                                    if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                    {
                                        Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $AnonymousAuth -PSPath IIS:\ -Location "$($SiteName)/$compname" -ErrorAction Stop
                                        Write-Host "AnonymouseAuth setting updated for App:$compname"
                                    }

                                    #Apply Windows authentication setting
                                    if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                    {
                                        if($WindowsAuth -ieq "windows")
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($SiteName)/$compname" -ErrorAction Stop              
                                            Write-Host "WindowsAuth enabled for App:$compname"
                                        }
                                        elseif($WindowsAuth -ieq "basic")
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($SiteName)/$compname" -ErrorAction Stop              
                                            Write-Host "BasicAuth enabled for App:$compname"
                                        }
                                        elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($SiteName)/$compname" -ErrorAction Stop                
                                            Write-Host "WindowsAuth enabled for App:$compname"
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($SiteName)/$compname" -ErrorAction Stop                
                                            Write-Host "BasicAuth enabled for App:$compname"
                                        }
                                        elseif($WindowsAuth -ieq $false)
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$($SiteName)/$compname" -ErrorAction Stop   
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$($SiteName)/$compname" -ErrorAction Stop                
                                            Write-Host "WindowsAuth and BasicAuth disabled for App:$compname"
                                        }
                                    }
                             
                                    #Apply ASP.NET Impersonation setting
                                    if(!([string]::IsNullOrEmpty($AspNetImpersonation)))
                                    {
                                        &$appcmdPath set config "$($SiteName)/$compname" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                        Write-Host "Asp.Net Impersonation enabled for App:$compname"
                                    }     
                                    }
                                 }

                                 else
                                 {
                                  Write-Host "No website or AppPool found with the name: $($SiteName) or $($AppPool)"

                                 }

                                }
                                else
                                {
                                 
                                 Write-Host "Root Virtual site : $compname already exist"
                                        
                                
                                }


                                }# end of Virtual site

    
    }#end try

    catch{
    #do nothing
    }


} #end of script block

if(!([string]::IsNullOrEmpty($AppPool)) -or !([string]::IsNullOrEmpty($SiteName)) -or !([string]::IsNullOrEmpty($compName)))
                    {                                                
                        Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock{iisreset /start}
                    }  



}# end if for sever verifcation

}#end foreach




}

# check virtul sites (components) are present then initate deployment else check for website deployment
elseif($websites -contains $item)
{

write-host " working on web site deployment " -ForegroundColor Green
foreach($srvr in $servers){

   ($i=$main.Deploy.ENV.$environment.Sites)|Where-Object{$_.Name -eq $item}

   
    $SiteName=$i.$item.Name
    $AppPool=$i.$item.AppPool

    if($srvr -ne "" -and (Test-Connection -cn $srvr -BufferSize 16 -Count 1 -ea 0 -quiet))
{
    Write-Host "**************Deployment workflow started in $srvr****************" 
    
    #Remotely connect to deployment server 
    #net use \\$srvr /user:$username $Pwd 

    if(!([string]::IsNullOrEmpty($AppPool)) -or !([string]::IsNullOrEmpty($SiteName)) -or !([string]::IsNullOrEmpty($compName)))
                    {                                                
                        Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock{iisreset /stop}
                    }  

                # Copy zip code   

    If (Test-Path "\\$srvr\C$\temp\$item.zip"){

    write-host " Removing old code zip and extracted code from temp in $srvr " -ForegroundColor Yellow
    Remove-Item "\\$srvr\C$\temp\$item.zip" -Force -Verbose
    Remove-Item "\\$srvr\C$\temp\$item" -Force -Recurse

    write-host " Copy code zip and extracted code to temp in $srvr " -ForegroundColor Green
    copy-item "Binaries\Web\$item.zip" "\\$srvr\C$\temp\" -Force

    }
    else
    {
     write-host " Copy code zip and extracted code to temp in $srvr " -ForegroundColor Green
    copy-item "Binaries\Web\$item.zip" "\\$srvr\C$\temp\" -Force
    }


              
       
Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock {

$Sitem=$using:item
$json=$using:main
$env=$using:environment

Import-Module WebAdministration

Write-Host " working on deployment for $Sitem" -ForegroundColor Green
($i=$json.Deploy.ENV.$env.sites)|Where-Object{$_.name -eq $Sitem}



$SiteName=$i.$Sitem.Name
$AppPool=$i.$Sitem.AppPool



    Write-Host "**************************************** Working with AppPools ************************************"
try{

        if(Test-Path ('IIS:\AppPools\'+ "$AppPool") -PathType Container) {

        Write-Warning "The App Pool $AppPool already exists"

        Remove-Item "IIS:\AppPools\$AppPool" -Force -Recurse -ErrorAction Stop

        Write-host "Delete appPool :" $AppPool -ForegroundColor Yellow
        }
       
       if(!(Test-Path ('IIS:\AppPools\'+ "$AppPool") -PathType Container)) {
       Write-Host "Creating AppPool:" $AppPool -ForegroundColor Green
      
       ($J=$json.Deploy.ENV.$env.AppPools)|Where-Object{$_.Name -eq $AppPool}
      
       $AppPoolName=$J.$AppPool.Name
      
       $DotNetVersion=$j.$AppPool.DotNetVersion
       $enable32BitAppOnWin64=$j.$AppPool.Enable32BitApps
       $enable32Bit=[System.Convert]::ToBoolean($enable32BitAppOnWin64)
       $ManagedPipelineMode=$J.$AppPool.ManagedPipelineMode
       $QueueLength=$J.$AppPool.QueueLength
       $UserName=$J.$AppPool.UserName
       $password=$J.$AppPool.Password
      
      
        Write-Host "****************Creating AppPool : $AppPoolName ****************"
         $pool= New-WebAppPool $AppPoolName -Force -ErrorAction Stop
         if([string]::IsNullOrEmpty($appPool))
                                         {
                                            IISReset
                                            $Pool = Get-Item  IIS:\AppPools\$AppPoolName -Force
                                         }

      
                              #Set app pool properties
                                               if(!([string]::IsNullOrEmpty($UserName)))
                                               {
                                            if($UserName -ieq "LocalSystem")
                                            {
                                                #AppPool Identity Type is set to 'LocalSystem'
                                                
                                               $pool.processModel.identityType = 0
                                            }
                                            elseif($UserName -ieq "LocalService")
                                            {
                                                #AppPool Identity Type is set to 'LocalService'
                                                $pool.processModel.identityType = 1
                                            }
                                            elseif($UserName -ieq "NetworkService")
                                            {
                                                #AppPool Identity Type is set to 'NetworkService'
                                                $pool.processModel.identityType = 2
                                            }
                                            elseif($UserName -ieq "ApplicationPoolIdentity")
                                            {
                                                #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                                $pool.processModel.identityType = 4
                                            }
                                            elseif(!([string]::IsNullOrEmpty($password)))
                                            {
                                                #AppPool Identity Type is set to CustomUser. Use credentials provided in XML
                                                $pool.processModel.identityType = 3
                                                $pool.processModel.userName = $UserName
                                                $pool.processModel.password = $password
                                            }
                                            else
                                            {
                                                Write-Host " AppPool UserName or Password : Invalid. AppPool Identity is set to ApplicationPoolIdentity"
                                                #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                                $pool.processmodel.identityType = 4   
                                            }
                                         }
                                               else
                                               {
                                            #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                            $pool.processmodel.identityType = 4
                                         }                                         
                                               
                                               #Check if properties of AppPool component not empty and then assign. If empty default values would get assigned
                                               if(!([string]::IsNullOrEmpty($DotNetVersion)))
                                               {
                                                if( $DotNetVersion -eq "No Managed Code")
                                                  {
                                                   $pool.managedRuntimeVersion = ""
                                                  }
                                                  else{
                                                   $pool.managedRuntimeVersion = $DotNetVersion
                                                  }  
                                            }
                                               
                                               if(!([string]::IsNullOrEmpty($enable32Bit)))
                                               {                                     
                                               $pool.enable32BitAppOnWin64 = $enable32Bit
                                         }
      
                                               if(!([string]::IsNullOrEmpty($ManagedPipelineMode)))
                                               {
                                              $pool.managedPipelineMode = $ManagedPipelineMode
                                               }
      
                                               if(!([string]::IsNullOrEmpty($QueueLength)))
                                               {
                                                  $pool.queueLength = [int]$QueueLength
                                               }
      
                                               
      
                                               $pool | Set-Item -Force -ErrorAction Stop  
                                               Write-Host "The App Pool $pool Created successfully" -ForegroundColor Green
                                                               
                                          }
        
      
}
catch{

        Write-Host  "Could not process AppPool $AppPool. Exception Messsage - $($error[0].Exception)"
        Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"
}# end AppPool
Write-Host "**************************************** END AppPools ********************************************"-ForegroundColor Green
# starting Try blcok for web sites
try{
        Write-Host "**************************************** Working with Sites ***************************************" -ForegroundColor Green

               
                # Createing website
                 $tempPath = "C:\temp\"
                             Write-Host "Extracting Packages to temp Directory and copy to App path" -ForegroundColor Yellow  
                                            
                
                     if($SiteName -ne $null -and $SiteName -ne "")
                     {

                     Try{
                                    $appcmdPath = "$Env:SystemRoot\System32\inetsrv\appcmd.exe" 

                                    

                                   #Remove existing WebSite if the DeleteExisting option is set to True in xml inputs of that component
                                   if(!(Test-Path ("IIS:\Sites\" + "$SiteName") -PathType Container) )
                                   {
                                     #Write-Host "website $SiteName doesnot exist  " -ForegroundColor Green

               

                                       # Remove-Item "IIS:\Sites\$SiteName" -Force -Recurse -ErrorAction Stop
                                        #Write-Host "Deleted WebSite: $SiteName" -ForegroundColor Yellow
                                   

                                          # featch site details from JSON
                                          
                                             ($k=$json.Deploy.ENV.$env.Sites)|Where-Object{$_.Name -eq $SiteName}

                                             
                                             $Site=$k.$SiteName.name
                                             $AppPool=$k.$SiteName.AppPool
                                             $BindingPort=$k.$SiteName.BindingPort
                                             $PhysicalPath=$k.$SiteName.PhysicalPath
                                             $EnabledProtocol=$k.$SiteName.EnabledProtocol
                                             $AnonymousAuth=$k.$SiteName.AnonymousAuth
                                             $WindowsAuth=$k.$SiteName.WindowsAuth
                                             $AspNetImpersonation=$k.$SiteName.AspNetImpersonation


                                              

                                            Write-Host "****************Creating WebSite : $Site****************"

                                            if(Test-Path ("IIS:\AppPools\"+"$($AppPool)") -PathType Container)
                                        { 
                                             $protocols=$($EnabledProtocol).Split(',').Split(';')  
                                             $bindInfo=$($BindingPort).Split(',').Split(';')                                             

                                             $bindings = @()
                                             $count = 0

                                             #Multiple Bindings for website will be formed here based on EnabledProtocol and BindingPort
                                             foreach($item in $protocols)
                                             {
                                                $bindings+= "@{protocol="+$item+";bindingInformation=$(@($bindInfo)[$count])}"  
                                                $count++
                                             }
                                            
                                          Add-Type -AssemblyName System.IO.Compression.FileSystem
                                              function Unzip
                                              {
                                                  param([string]$zipfile, [string]$outpath)
                                              
                                                  [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
                                              }
                                             
                                             if(test-path "$tempPath\$Site")
                                             {
                                              Remove-Item "$tempPath\$Site" -Force -Recurse
                                             }
                                              
                                            Unzip "$tempPath\$($Site).zip" "$tempPath\$Site"

                                             if(test-path "$PhysicalPath")
                                             {
                                              Remove-Item "$PhysicalPath" -Force -Recurse
                                             }
                               

                                            robocopy.exe "$tempPath\$Site" "$PhysicalPath" /MIR /mt:4 /nfl /ndl /np /R:2 /W:5
                                            
                                            #Create a website                                                                                        
                                            New-Item "IIS:\Sites\$Site" -bindings $bindings -physicalPath $PhysicalPath.replace("/","\") -ApplicationPool $AppPool -ErrorAction Stop

                                           Try{
                                            #Apply Anonymous authentication setting
                                            if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                            {
                                                Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $k.$SiteName.AnonymousAuth -PSPath IIS:\ -Location "$Site"
                                                Write-Host "AnonymouseAuth setting updated for Site:$Site"
                                            }

                                            #Apply Windows authentication setting
                                            if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                            {
                                               
                                                if($WindowsAuth -ieq "windows")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq "basic")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq $false)
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Write-Host "WindowsAuth and BasicAuth are disabled for Site:$Site"                
                                                }
                                            }
                                
                                            #Apply ASP.NET Impersonation setting
                                            if(!([string]::IsNullOrEmpty($AspNetImpersonation)))
                                            {
                                                &$appcmdPath set config "$Site" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                                Write-Host "Asp.Net Impersonation enabled for Site:$Site" 
                                            }
                                                                           
                                            }
                                            Catch{
                                                Write-Host "Could not process Anonymous Authentication or Windows Authentication or AspNetImpersonation for WebSite:$Site. Exception Messsage - $($error[0].Exception)"
                                                Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"  
                                            } 
                                        }
                                        else
                                        {
                                            Write-Host "Could not process website $Site. As AppPool $($AppPool) not found"
                                             
                                        }    

                                     }   

                                     else{
                                     if((Test-Path ("IIS:\Sites\" + "$SiteName") -PathType Container) )
                                   {
                                     #Write-Host "website $SiteName doesnot exist  " -ForegroundColor Green

                                     Write-Warning " website $sitename is already exist and hence removing"

              

                                        Remove-Item "IIS:\Sites\$SiteName" -Force -Recurse -ErrorAction Stop
                                        Write-Host "Deleted WebSite: $SiteName" -ForegroundColor Yellow
                                   

                                          # featch site details from JSON
                                          
                                             ($k=$json.Deploy.ENV.$env.Sites)|Where-Object{$_.Name -eq $SiteName}

                                             
                                             $Site=$k.$SiteName.name
                                             $AppPool=$k.$SiteName.AppPool
                                             $BindingPort=$k.$SiteName.BindingPort
                                             $PhysicalPath=$k.$SiteName.PhysicalPath
                                             $EnabledProtocol=$k.$SiteName.EnabledProtocol
                                             $AnonymousAuth=$k.$SiteName.AnonymousAuth
                                             $WindowsAuth=$k.$SiteName.WindowsAuth
                                             $AspNetImpersonation=$k.$SiteName.AspNetImpersonation


                                              

                                            Write-Host "****************Creating WebSite : $Site****************"

                                            if(Test-Path ("IIS:\AppPools\"+"$($AppPool)") -PathType Container)
                                        { 
                                             $protocols=$($EnabledProtocol).Split(',').Split(';')  
                                             $bindInfo=$($BindingPort).Split(',').Split(';')                                             

                                             $bindings = @()
                                             $count = 0

                                             #Multiple Bindings for website will be formed here based on EnabledProtocol and BindingPort
                                             foreach($item in $protocols)
                                             {
                                                $bindings+= "@{protocol="+$item+";bindingInformation=$(@($bindInfo)[$count])}"  
                                                $count++
                                             }
                                            Add-Type -AssemblyName System.IO.Compression.FileSystem
                                              function Unzip
                                              {
                                                  param([string]$zipfile, [string]$outpath)
                                              
                                                  [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
                                              }
                                             
                                             if(test-path "$tempPath\$Site")
                                             {
                                              Remove-Item "$tempPath\$Site" -Force -Recurse
                                             }
                                              
                                            Unzip "$tempPath\$($Site).zip" "$tempPath\$Site"

                                             if(test-path "$PhysicalPath")
                                             {
                                              Remove-Item "$PhysicalPath" -Force -Recurse
                                             }
                                            
                                            robocopy.exe "$tempPath\$Site" "$PhysicalPath" /MIR /mt:4 /nfl /ndl /np /R:2 /W:5                                                                                   
                                            #Create a website                                                                                        
                                            New-Item "IIS:\Sites\$Site" -bindings $bindings -physicalPath $PhysicalPath.replace("/","\") -ApplicationPool $AppPool -ErrorAction Stop

                                            #Verify if physical path exists. Create if not. To enable auth or impersonation settings for site
                                                        
                                            Try{
                                            #Apply Anonymous authentication setting
                                            if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                            {
                                                Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $k.$SiteName.AnonymousAuth -PSPath IIS:\ -Location "$Site"
                                                Write-Host "AnonymouseAuth setting updated for Site:$Site"
                                            }

                                            #Apply Windows authentication setting
                                            if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                            {
                                               
                                                if($WindowsAuth -ieq "windows")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq "basic")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq $false)
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Write-Host "WindowsAuth and BasicAuth are disabled for Site:$Site"                
                                                }
                                            }
                                
                                            #Apply ASP.NET Impersonation setting
                                            if(!([string]::IsNullOrEmpty($AspNetImpersonation)))
                                            {
                                                &$appcmdPath set config "$Site" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                                Write-Host "Asp.Net Impersonation enabled for Site:$Site" 
                                            }
                                                                           
                                            }
                                            Catch{
                                                Write-Host "Could not process Anonymous Authentication or Windows Authentication or AspNetImpersonation for WebSite:$Site. Exception Messsage - $($error[0].Exception)"
                                                Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"  
                                            } 
                                        }
                                        else
                                        {
                                            Write-Host "Could not process website $Site. As AppPool $($AppPool) not found"
                                             
                                        }    

                                     } 


                                                                                
                                     
                                     }
                                    # done with site creatioin

                                    }
                                    catch
                                    {
                                      Write-Host "Could not process website $Site. Exception Messsage - $($error[0].Exception)"
                                      Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"
                                         
                                    }
                     }#end if for website





Write-Host "**************************************** END with Sites *******************************************"
}# end of Try blcok for web sites
catch{

    # Write-Host "Sites : Issue during deployment workflow in $ServerName. Exception Messsage - $($error[0].Exception)" 
    # Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)" 
 }



}# end of script block

if(!([string]::IsNullOrEmpty($AppPool)) -or !([string]::IsNullOrEmpty($SiteName)) -or !([string]::IsNullOrEmpty($compName)))
                    {                                                
                        Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock{iisreset /start}
                    }  

  }# end of server connection
   }# end server foreach
  

}

else{

   Write-Warning " $sites and $item not for deployment"
}# end Item verification






}#end foreach components

} # end Data




else{
write-host " Deployment choosen as FULL Deploy" -ForegroundColor Green

if($FullDeploy -ne $false)
{

#


# check virtul sites (components) are present then initate deployment 
if($sites -ne "")
{
foreach($item in $sites)
{

foreach($srvr in $servers){
($v=$main.Deploy.ENV.$environment.VirtualSites)|Where-Object{$_.name -eq $item}

    $compName=$v.$item.name
    $SiteName=$v.$item.Sitename
    $AppPool=$v.$item.AppPool

if($srvr -ne "" -and (Test-Connection -cn $srvr -BufferSize 16 -Count 1 -ea 0 -quiet))
{
    Write-Host "**************Deployment workflow started in $srvr****************" 
    
    #Remotely connect to deployment server 
   net use \\$srvr /user:$user $SVCPWD

    if(!([string]::IsNullOrEmpty($AppPool)) -or !([string]::IsNullOrEmpty($SiteName)) -or !([string]::IsNullOrEmpty($compName)))
                    {                                                
                        Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock{iisreset /stop}
                    }  

# Copy zip code    

    If ( Test-Path "\\$srvr\C$\temp\$item.zip" ){

    write-host " Removing old code zip and extracted code from temp in $srvr " -ForegroundColor Yellow
    Remove-Item "\\$srvr\C$\temp\$item.zip" -Force -Verbose
    if(test-path "\\$srvr\C$\temp\$item"){
    Remove-Item "\\$srvr\C$\temp\$item" -Force -Recurse
    }
    write-host " Copy code zip and extracted code to temp in $srvr " -ForegroundColor Green
    copy-item "Binaries\Web\$item.zip" "\\$srvr\C$\temp\" -Force

    }
    else{

    write-host " Copy code zip and extracted code to temp in $srvr " -ForegroundColor Green
    copy-item "Binaries\Web\$item.zip" "\\$srvr\C$\temp\" -Force
    }

  
Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock {

$Sitem=$using:item
$json=$using:main
$env=$using:environment
Import-Module WebAdministration

Write-Host " working on deployment for $Sitem" -ForegroundColor Green
($i=$json.Deploy.ENV.$env.VirtualSites)|Where-Object{$_.name -eq $Sitem}

$compName=$i.$Sitem.name
$SiteName=$i.$Sitem.Sitename
$AppPool=$i.$Sitem.AppPool
$PhysicalPath=$i.$Sitem.PhysicalPath
$AnonymousAuth=$i.$Sitem.AnonymousAuth
$WindowsAuth=$i.$Sitem.WindowsAuth
$AspNetImpersonation=$i.$Sitem.AspNetImpersonation


    Write-Host "**************************************** Working with AppPools ************************************"
try{

        if(Test-Path ('IIS:\AppPools\'+ "$AppPool") -PathType Container) {

        Write-Warning "The App Pool $AppPool already exists"

        Remove-Item "IIS:\AppPools\$AppPool" -Force -Recurse -ErrorAction Stop

        Write-host "Delete appPool :" $AppPool -ForegroundColor Yellow
        }
       
       if(!(Test-Path ('IIS:\AppPools\'+ "$AppPool") -PathType Container)) {
       Write-Host "Creating AppPool:" $AppPool -ForegroundColor Green
      
       ($J=$json.Deploy.ENV.$env.AppPools)|Where-Object{$_.Name -eq $AppPool}
      
       $AppPoolName=$J.$AppPool.Name
      
       $DotNetVersion=$j.$AppPool.DotNetVersion
       $enable32BitAppOnWin64=$j.$AppPool.Enable32BitApps
       $enable32Bit=[System.Convert]::ToBoolean($enable32BitAppOnWin64)
       $ManagedPipelineMode=$J.$AppPool.ManagedPipelineMode
       $QueueLength=$J.$AppPool.QueueLength
       $UserName=$J.$AppPool.UserName
       $password=$J.$AppPool.Password
      
      
        Write-Host "****************Creating AppPool : $AppPoolName ****************"
         $pool= New-WebAppPool $AppPoolName -Force -ErrorAction Stop
         if([string]::IsNullOrEmpty($appPool))
                                         {
                                            IISReset
                                            $Pool = Get-Item  IIS:\AppPools\$AppPoolName -Force
                                         }

      
                              #Set app pool properties
                                               if(!([string]::IsNullOrEmpty($UserName)))
                                               {
                                            if($UserName -ieq "LocalSystem")
                                            {
                                                #AppPool Identity Type is set to 'LocalSystem'
                                                
                                               $pool.processModel.identityType = 0
                                            }
                                            elseif($UserName -ieq "LocalService")
                                            {
                                                #AppPool Identity Type is set to 'LocalService'
                                                $pool.processModel.identityType = 1
                                            }
                                            elseif($UserName -ieq "NetworkService")
                                            {
                                                #AppPool Identity Type is set to 'NetworkService'
                                                $pool.processModel.identityType = 2
                                            }
                                            elseif($UserName -ieq "ApplicationPoolIdentity")
                                            {
                                                #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                                $pool.processModel.identityType = 4
                                            }
                                            elseif(!([string]::IsNullOrEmpty($password)))
                                            {
                                                #AppPool Identity Type is set to CustomUser. Use credentials provided in XML
                                                $pool.processModel.identityType = 3
                                                $pool.processModel.userName = $UserName
                                                $pool.processModel.password = $password
                                            }
                                            else
                                            {
                                                Write-Host " AppPool UserName or Password : Invalid. AppPool Identity is set to ApplicationPoolIdentity"
                                                #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                                $pool.processmodel.identityType = 4   
                                            }
                                         }
                                               else
                                               {
                                            #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                            $pool.processmodel.identityType = 4
                                         }                                         
                                               
                                               #Check if properties of AppPool component not empty and then assign. If empty default values would get assigned
                                               if(!([string]::IsNullOrEmpty($DotNetVersion)))
                                               {
                                                if( $DotNetVersion -eq "No Managed Code")
                                                  {
                                                   $pool.managedRuntimeVersion = ""
                                                  }
                                                  else{
                                                   $pool.managedRuntimeVersion = $DotNetVersion
                                                  }
                                         }
                                               
                                               if(!([string]::IsNullOrEmpty($enable32Bit)))
                                               {                                     
                                               $pool.enable32BitAppOnWin64 = $enable32Bit
                                         }
      
                                               if(!([string]::IsNullOrEmpty($ManagedPipelineMode)))
                                               {
                                              $pool.managedPipelineMode = $ManagedPipelineMode
                                               }
      
                                               if(!([string]::IsNullOrEmpty($QueueLength)))
                                               {
                                                  $pool.queueLength = [int]$QueueLength
                                               }
      
                                               
      
                                               $pool | Set-Item -Force -ErrorAction Stop  
                                               Write-Host "The App Pool $pool Created successfully" -ForegroundColor Green
                                                               
                                          }
        
      
}
catch{

        Write-Host  "Could not process AppPool $AppPool. Exception Messsage - $($error[0].Exception)"
        Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"
}# end AppPool
Write-Host "**************************************** END AppPools ********************************************"-ForegroundColor Green
# starting Try blcok for web sites
try{
        Write-Host "**************************************** Working with Sites ***************************************" -ForegroundColor Green

               
                # Createing website
                
                     if($SiteName -ne $null -and $SiteName -ne "")
                     {

                     Try{
                                    $appcmdPath = "$Env:SystemRoot\System32\inetsrv\appcmd.exe" 

                                    

                                   #Remove existing WebSite if the DeleteExisting option is set to True in xml inputs of that component
                                   if(!(Test-Path ("IIS:\Sites\" + "$SiteName") -PathType Container) )
                                   {
                                     #Write-Host "website $SiteName doesnot exist  " -ForegroundColor Green

               

                                       # Remove-Item "IIS:\Sites\$SiteName" -Force -Recurse -ErrorAction Stop
                                        #Write-Host "Deleted WebSite: $SiteName" -ForegroundColor Yellow
                                   

                                          # featch site details from JSON
                                          
                                             ($k=$json.Deploy.ENV.$env.Sites)|Where-Object{$_.Name -eq $SiteName}

                                             
                                             $Site=$k.$SiteName.name
                                             $AppPool=$k.$SiteName.AppPool
                                             $BindingPort=$k.$SiteName.BindingPort
                                             $PhysicalPath=$k.$SiteName.PhysicalPath
                                             $EnabledProtocol=$k.$SiteName.EnabledProtocol
                                             $AnonymousAuth=$k.$SiteName.AnonymousAuth
                                             $WindowsAuth=$k.$SiteName.WindowsAuth
                                             $AspNetImpersonation=$k.$SiteName.AspNetImpersonation


                                            Write-Host "****************Creating WebSite : $Site****************"

                                            if(Test-Path ("IIS:\AppPools\"+"$($AppPool)") -PathType Container)
                                        { 
                                             $protocols=$($EnabledProtocol).Split(',').Split(';')  
                                             $bindInfo=$($BindingPort).Split(',').Split(';')                                             

                                             $bindings = @()
                                             $count = 0

                                             #Multiple Bindings for website will be formed here based on EnabledProtocol and BindingPort
                                             foreach($item in $protocols)
                                             {
                                                $bindings+= "@{protocol="+$item+";bindingInformation=$(@($bindInfo)[$count])}"  
                                                $count++
                                             }
                                            $tempPath = "C:\temp\"
                                              Write-Host "Extracting Packages to temp Directory and copy to App path" -ForegroundColor Yellow  
                                               
                                              Add-Type -AssemblyName System.IO.Compression.FileSystem
                                              function Unzip
                                              {
                                                  param([string]$zipfile, [string]$outpath)
                                              
                                                  [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
                                              }
                                             
                                             if(test-path "$tempPath\$Site")
                                             {
                                              Remove-Item "$tempPath\$Site" -Force -Recurse
                                             }
                                              
                                            Unzip "$tempPath\$($Site).zip" "$tempPath\$Site"

                                             if(test-path "$PhysicalPath")
                                             {
                                              Remove-Item "$PhysicalPath" -Force -Recurse
                                             }

                                            robocopy.exe "$tempPath\$Site" "$PhysicalPath" /MIR /mt:4 /nfl /ndl /np /R:2 /W:5                                                                                   
                                            New-Item "IIS:\Sites\$Site" -bindings $bindings -physicalPath $PhysicalPath.replace("/","\") -ApplicationPool $AppPool -ErrorAction Stop

                                            Try{
                                            #Apply Anonymous authentication setting
                                            if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                            {
                                                Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $k.$SiteName.AnonymousAuth -PSPath IIS:\ -Location "$Site"
                                                Write-Host "AnonymouseAuth setting updated for Site:$Site"
                                            }

                                            #Apply Windows authentication setting
                                            if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                            {
                                               
                                                if($WindowsAuth -ieq "windows")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq "basic")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq $false)
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Write-Host "WindowsAuth and BasicAuth are disabled for Site:$Site"                
                                                }
                                            }
                                
                                            #Apply ASP.NET Impersonation setting
                                            if(!([string]::IsNullOrEmpty($AspNetImpersonation)))
                                            {
                                                &$appcmdPath set config "$Site" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                                Write-Host "Asp.Net Impersonation enabled for Site:$Site" 
                                            }
                                            }
                                            Catch{
                                                Write-Host "Could not process Anonymous Authentication or Windows Authentication or AspNetImpersonation for WebSite:$Site. Exception Messsage - $($error[0].Exception)"
                                                Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"  
                                            } 
                                        }
                                        else
                                        {
                                            Write-Host "Could not process website $Site. As AppPool $($AppPool) not found"
                                             
                                        }    

                                     }   

                                     else{

                                     Write-Warning " website $sitename is already exist"
                                     }
                                    # done with site creatioin

                                    }
                                    catch
                                    {
                                      Write-Host "Could not process website $Site. Exception Messsage - $($error[0].Exception)"
                                      Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"
                                         
                                    }
                     }#end if for website





Write-Host "**************************************** END with Sites *******************************************"
}# end of Try blcok for web sites
catch{

    # Write-Host "Sites : Issue during deployment workflow in $ServerName. Exception Messsage - $($error[0].Exception)" 
    # Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)" 
 }

# starting Try blcok for web virtual sites

Try{
                                Write-Host "****************Root Virtual site: $compName****************"

                                if($compName -ne $null -and $compName -ne "")
                                {

                                ($i=$json.Deploy.ENV.$env.VirtualSites)|Where-Object{$_.name -eq $Sitem}


                                 $compName=$i.$Sitem.name
                                 $SiteName=$i.$Sitem.Sitename
                                 $AppPool=$i.$Sitem.AppPool
                                 $PhysicalPath=$i.$Sitem.PhysicalPath
                                 $AnonymousAuth=$i.$Sitem.AnonymousAuth
                                 $WindowsAuth=$i.$Sitem.WindowsAuth
                                 $AspNetImpersonation=$i.$Sitem.AspNetImpersonation

                                $tempPath = "C:\temp\"
                                Write-Host "Extracting Packages to temp Directory and copy to App path" -ForegroundColor Yellow  

                                                  
                                $appcmdPath = "$Env:SystemRoot\System32\inetsrv\appcmd.exe"

                                $VDIRExists = $false

                                if(([string]::IsNullOrEmpty((get-item "IIS:\Sites\$($SiteName)\$compName" -ErrorAction Ignore).PhysicalPath)))
                                {
                                    $VDIRExists = $false
                                }
                                else
                                {
                                    $VDIRExists = $true
                                }

                                #Remove existing WebApp if the DeleteExisting option is set to True in xml inputs of that component
                                if($VDIRExists -ieq $true)
                                {
                                    Remove-Item "IIS:\Sites\$($SiteName)\$compName" -Force -Recurse -ErrorAction Stop                                                                                      
                                            
                                    #After deletion just reverify if webapp physical path attribute
                                    if(([string]::IsNullOrEmpty((get-item "IIS:\Sites\$($SiteName)\$compName" -ErrorAction Ignore).PhysicalPath)))
                                    {
                                        Write-Host "Deleted WebApp: $compName"
                                        $VDIRExists = $false
                                    }
                                    else
                                    {
                                        $VDIRExists = $true
                                    }
                                }  
                                
                                #Verify if WebApp does not exists and delete. Then extract from zip  
                                if (!($VDIRExists))
                                {           
                                   if((Test-Path "IIS:\Sites\$($SiteName)" -PathType Container) -and (Test-Path "IIS:\AppPools\$($AppPool)" -PathType Container))
                                    { 
                                                         
                                    Add-Type -AssemblyName System.IO.Compression.FileSystem
                                              function Unzip
                                              {
                                                  param([string]$zipfile, [string]$outpath)
                                              
                                                  [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
                                              }
                                             
                                             if(test-path "$tempPath\$compname")
                                             {
                                              Remove-Item "$tempPath\$compname" -Force -Recurse
                                             }
                                              
                                            Unzip "$tempPath\$($compname).zip" "$tempPath\$compname"

                                             if(test-path "$PhysicalPath")
                                             {
                                              Remove-Item "$PhysicalPath" -Force -Recurse
                                             }
                                          
                                          robocopy.exe "$tempPath\$compname" "$PhysicalPath" /MIR /mt:4 /nfl /ndl /np /R:2 /W:5

                                    New-Item IIS:\Sites\$($SiteName)\$compname -physicalPath "$($PhysicalPath)" -type Application -force -ErrorAction Stop
                                    #Set Application Pool
                                    Set-ItemProperty "IIS:\Sites\$($SiteName)\$compname" -name applicationPool -value $AppPool -ErrorAction Stop



                                     Try{
                                    #Apply Anonymous authentication setting
                                    if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                    {
                                       Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $AnonymousAuth -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop
                                        Write-Host "AnonymouseAuth setting updated for App:$compName"
                                    }

                                    #Apply Windows authentication setting
                                    if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                    {
                                        if($WindowsAuth -ieq "windows")
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop              
                                            Write-Host "WindowsAuth enabled for App:$compname"
                                        }
                                        elseif($getApp.WindowsAuth -ieq "basic")
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop              
                                            Write-Host "BasicAuth enabled for App:$compName"
                                        }
                                        elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop                
                                            Write-Host "WindowsAuth enabled for App:$compName"
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$($getApp.SiteName)/$compName" -ErrorAction Stop                
                                            Write-Host "BasicAuth enabled for App:$compName"
                                        }
                                        elseif($WindowsAuth -ieq $false)
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop   
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$($SiteName)/$compName" -ErrorAction Stop                
                                            Write-Host "WindowsAuth and BasicAuth disabled for App:$compName"
                                        }
                                    }
                             
                                    #Apply ASP.NET Impersonation setting
                                    if($AspNetImpersonation)
                                    {
                                    
                                        &$appcmdPath set config "$($SiteName)/$compName" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                        Write-Host "Asp.Net Impersonation enabled for App:"$compname
                                    }                                            
                                        
                                                              
                                 }# end of try block
                                 catch
                                 {
                                         
                                    Write-Host "Deleting default web.config if any in physical path. To avoid issues for setting Anonymous Authentication or Windows Authentication or AspNetImpersonation"
                                    $defaultConfig = $($PhysicalPath).trim('\').trim('/')
                                    if(Test-Path "$defaultConfig\web.config")
                                    {
                                        Remove-Item "$defaultConfig\web.config" -Force
                                    }  
                                    #Apply Anonymous authentication setting
                                    if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                    {
                                        Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $AnonymousAuth -PSPath IIS:\ -Location "$($SiteName)/$compname" -ErrorAction Stop
                                        Write-Host "AnonymouseAuth setting updated for App:$compname"
                                    }

                                    #Apply Windows authentication setting
                                    if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                    {
                                        if($WindowsAuth -ieq "windows")
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath "IIS:\Sites\$SiteName\$compname" -ErrorAction Stop              
                                            Write-Host "WindowsAuth enabled for App:$compname"
                                        }
                                        elseif($WindowsAuth -ieq "basic")
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath "IIS:\Sites\$SiteName\$compname" -ErrorAction Stop              
                                            Write-Host "BasicAuth enabled for App:$compname"
                                        }
                                        elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                        {
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath "IIS:\Sites\$SiteName\$compname" -ErrorAction Stop                
                                            Write-Host "WindowsAuth enabled for App:$compname"
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath "IIS:\Sites\$SiteName\$compname" -ErrorAction Stop                
                                            Write-Host "BasicAuth enabled for App:$compname"
                                        }
                                        elseif($WindowsAuth -ieq $false)
                                        {

                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath "IIS:\Sites\$SiteName\$compname" -ErrorAction Stop   
                                            Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath "IIS:\Sites\$SiteName\$compname" -ErrorAction Stop                
                                            Write-Host "WindowsAuth and BasicAuth disabled for App:$compname"
                                        }
                                    }
                             
                                    #Apply ASP.NET Impersonation setting
                                    if(!([string]::IsNullOrEmpty($AspNetImpersonation)))
                                    {
                                        &$appcmdPath set config "$($SiteName)/$compname" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                        Write-Host "Asp.Net Impersonation enabled for App:$compname"
                                    }     
                                    }
                                                              
                                 }

                                 else
                                 {
                                  Write-Host "No website or AppPool found with the name: $($SiteName) or $($AppPool)"

                                 }

                                }
                                else
                                {
                                 
                                 Write-Host "Root Virtual site : $compname already exist"
                                        
                                
                                }


                                }# end of Virtual site


    
    }#end try

    catch{
            Write-Host "Could not process Root WebApp $App. Exception Messsage - $($error[0].Exception)"
            Write-Host "ERROR Line No. : $($error[0].invocationinfo.ScriptLineNumber)"
            net use \\$ServerName /delete 
    }

    }#end of script block

}#end of IF server validation 
        if(!([string]::IsNullOrEmpty($AppPool)) -or !([string]::IsNullOrEmpty($SiteName)) -or !([string]::IsNullOrEmpty($compName)))
                                                              {                                                
                                                                     Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock{iisreset /start}
                                                              }  
else
{
$exception="Connectivity issue to Server:$Srvr"
Write-Error -Exception $exception
}
}#end of servers
}#end foreach items full deploy

}

# check virtul sites (components) are present then initate deployment else check for website deployment
elseif($websites.Count -ne "0" -and $sites.Count -eq "0")
{


  Foreach ($item in $websites)
  {

   foreach($srvr in $servers){

   ($i=$main.Deploy.ENV.$environment.Sites)|Where-Object{$_.Name -eq $item}



    if($srvr -ne "" -and (Test-Connection -cn $srvr -BufferSize 16 -Count 1 -ea 0 -quiet))
{
    Write-Host "**************Deployment workflow started in $srvr****************" 
    
    #Remotely connect to deployment server 
    net use \\$srvr /user:$username $Pwd 

    if(!([string]::IsNullOrEmpty($AppPool)) -or !([string]::IsNullOrEmpty($SiteName)) -or !([string]::IsNullOrEmpty($compName)))
                    {                                                
                        Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock{iisreset /stop}
                    }  

                # Copy zip code   

    If ( Test-Path "\\$srvr\C$\temp\$item.zip" ){

    write-host " Removing old code zip and extracted code from temp in $srvr " -ForegroundColor Yellow
    Remove-Item "\\$srvr\C$\temp\$item.zip" -Force -Verbose
    Remove-Item "\\$srvr\C$\temp\$item" -Force -Recurse

    write-host " Copy code zip and extracted code to temp in $srvr " -ForegroundColor Green
    copy-item "Binaries\Web\$item.zip" "\\$srvr\C$\temp\" -Force

    }
    else
    {
    write-host " Copy code zip and extracted code to temp in $srvr " -ForegroundColor Green
    copy-item "Binaries\Web\$item.zip" "\\$srvr\C$\temp\" -Force

    }

    #Add server to trusted host of runner
                    if(!@((Get-Item WSMan:\localhost\Client\TrustedHosts).value).Contains("*"))
                    {                        
                        Set-Item -Path WSMan:\localhost\Client\TrustedHosts -Value "*" -Force
                        "Machine added to Trusted-Hosts of runner"
                    }  
              
       
Invoke-Command -Credential $cred -ComputerName $srvr -ErrorAction Stop -ScriptBlock {

$Sitem=$using:item
$json=$using:main
$env=$using:environment

Import-Module WebAdministration

Write-Host " working on deployment for $Sitem" -ForegroundColor Green
($i=$json.Deploy.ENV.$env.sites)|Where-Object{$_.name -eq $Sitem}



$SiteName=$i.$Sitem.Name
$AppPool=$i.$Sitem.AppPool



    Write-Host "**************************************** Working with AppPools ************************************"
try{

        if(Test-Path ('IIS:\AppPools\'+ "$AppPool") -PathType Container) {

        Write-Warning "The App Pool $AppPool already exists"

        Remove-Item "IIS:\AppPools\$AppPool" -Force -Recurse -ErrorAction Stop

        Write-host "Delete appPool :" $AppPool -ForegroundColor Yellow
        }
       
       if(!(Test-Path ('IIS:\AppPools\'+ "$AppPool") -PathType Container)) {
       Write-Host "Creating AppPool:" $AppPool -ForegroundColor Green
      
       ($J=$json.Deploy.ENV.$env.AppPools)|Where-Object{$_.Name -eq $AppPool}
      
       $AppPoolName=$J.$AppPool.Name
      
       $DotNetVersion=$j.$AppPool.DotNetVersion
       $enable32BitAppOnWin64=$j.$AppPool.Enable32BitApps
       $enable32Bit=[System.Convert]::ToBoolean($enable32BitAppOnWin64)
       $ManagedPipelineMode=$J.$AppPool.ManagedPipelineMode
       $QueueLength=$J.$AppPool.QueueLength
       $UserName=$J.$AppPool.UserName
       $password=$J.$AppPool.Password
      
      
        Write-Host "****************Creating AppPool : $AppPoolName ****************"
         $pool= New-WebAppPool $AppPoolName -Force -ErrorAction Stop
         if([string]::IsNullOrEmpty($appPool))
                                         {
                                            IISReset
                                            $Pool = Get-Item  IIS:\AppPools\$AppPoolName -Force
                                         }

      
                              #Set app pool properties
                                               if(!([string]::IsNullOrEmpty($UserName)))
                                               {
                                            if($UserName -ieq "LocalSystem")
                                            {
                                                #AppPool Identity Type is set to 'LocalSystem'
                                                
                                               $pool.processModel.identityType = 0
                                            }
                                            elseif($UserName -ieq "LocalService")
                                            {
                                                #AppPool Identity Type is set to 'LocalService'
                                                $pool.processModel.identityType = 1
                                            }
                                            elseif($UserName -ieq "NetworkService")
                                            {
                                                #AppPool Identity Type is set to 'NetworkService'
                                                $pool.processModel.identityType = 2
                                            }
                                            elseif($UserName -ieq "ApplicationPoolIdentity")
                                            {
                                                #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                                $pool.processModel.identityType = 4
                                            }
                                            elseif(!([string]::IsNullOrEmpty($password)))
                                            {
                                                #AppPool Identity Type is set to CustomUser. Use credentials provided in XML
                                                $pool.processModel.identityType = 3
                                                $pool.processModel.userName = $UserName
                                                $pool.processModel.password = $password
                                            }
                                            else
                                            {
                                                Write-Host " AppPool UserName or Password : Invalid. AppPool Identity is set to ApplicationPoolIdentity"
                                                #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                                $pool.processmodel.identityType = 4   
                                            }
                                         }
                                               else
                                               {
                                            #AppPool Identity Type is set to 'ApplicationPoolIdentity'
                                            $pool.processmodel.identityType = 4
                                         }                                         
                                               
                                               #Check if properties of AppPool component not empty and then assign. If empty default values would get assigned
                                               if(!([string]::IsNullOrEmpty($DotNetVersion)))
                                               {
                                                if( $DotNetVersion -eq "No Managed Code")
                                                  {
                                                   $pool.managedRuntimeVersion = ""
                                                  }
                                                  else{
                                                   $pool.managedRuntimeVersion = $DotNetVersion
                                                  }
                                         }
                                               
                                               if(!([string]::IsNullOrEmpty($enable32Bit)))
                                               {                                     
                                               $pool.enable32BitAppOnWin64 = $enable32Bit
                                         }
      
                                               if(!([string]::IsNullOrEmpty($ManagedPipelineMode)))
                                               {
                                              $pool.managedPipelineMode = $ManagedPipelineMode
                                               }
      
                                               if(!([string]::IsNullOrEmpty($QueueLength)))
                                               {
                                                  $pool.queueLength = [int]$QueueLength
                                               }
      
                                               
      
                                               $pool | Set-Item -Force -ErrorAction Stop  
                                               Write-Host "The App Pool $pool Created successfully" -ForegroundColor Green
                                                               
                                          }
        
      
}
catch{

        Write-Host  "Could not process AppPool $AppPool. Exception Messsage - $($error[0].Exception)"
        Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"
}# end AppPool
Write-Host "**************************************** END AppPools ********************************************"-ForegroundColor Green
# starting Try blcok for web sites
try{
        Write-Host "**************************************** Working with Sites ***************************************" -ForegroundColor Green

               
                # Createing website
                 $tempPath = "C:\temp\"
                 Write-Host "Extracting Packages to temp Directory and copy to App path" -ForegroundColor Yellow  
                                                             
                
                     if($SiteName -ne $null -and $SiteName -ne "")
                     {

                     Try{
                                    $appcmdPath = "$Env:SystemRoot\System32\inetsrv\appcmd.exe" 

                                    

                                   #Remove existing WebSite if the DeleteExisting option is set to True in xml inputs of that component
                                   if(!(Test-Path ("IIS:\Sites\" + "$SiteName") -PathType Container) )
                                   {
                                     #Write-Host "website $SiteName doesnot exist  " -ForegroundColor Green

               

                                       # Remove-Item "IIS:\Sites\$SiteName" -Force -Recurse -ErrorAction Stop
                                        #Write-Host "Deleted WebSite: $SiteName" -ForegroundColor Yellow
                                   

                                          # featch site details from JSON
                                          
                                             ($k=$json.Deploy.ENV.$env.Sites)|Where-Object{$_.Name -eq $SiteName}

                                             
                                             $Site=$k.$SiteName.name
                                             $AppPool=$k.$SiteName.AppPool
                                             $BindingPort=$k.$SiteName.BindingPort
                                             $PhysicalPath=$k.$SiteName.PhysicalPath
                                             $EnabledProtocol=$k.$SiteName.EnabledProtocol
                                             $AnonymousAuth=$k.$SiteName.AnonymousAuth
                                             $WindowsAuth=$k.$SiteName.WindowsAuth
                                             $AspNetImpersonation=$k.$SiteName.AspNetImpersonation


                                              

                                            Write-Host "****************Creating WebSite : $Site****************"

                                            if(Test-Path ("IIS:\AppPools\"+"$($AppPool)") -PathType Container)
                                        { 
                                             $protocols=$($EnabledProtocol).Split(',').Split(';')  
                                             $bindInfo=$($BindingPort).Split(',').Split(';')                                             

                                             $bindings = @()
                                             $count = 0

                                             #Multiple Bindings for website will be formed here based on EnabledProtocol and BindingPort
                                             foreach($item in $protocols)
                                             {
                                                $bindings+= "@{protocol="+$item+";bindingInformation=$(@($bindInfo)[$count])}"  
                                                $count++
                                             }
                                            
                                              Add-Type -AssemblyName System.IO.Compression.FileSystem
                                              function Unzip
                                              {
                                                  param([string]$zipfile, [string]$outpath)
                                              
                                                  [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
                                              }
                                             
                                             if(test-path "$tempPath\$Site")
                                             {
                                              Remove-Item "$tempPath\$Site" -Force -Recurse
                                             }
                                              
                                            Unzip "$tempPath\$($Site).zip" "$tempPath\$Site"

                                             if(test-path "$PhysicalPath")
                                             {
                                              Remove-Item "$PhysicalPath" -Force -Recurse
                                              New-Item -Path "$PhysicalPath" -ItemType Directory -Force
                                             }
                                            else
                                            {
                                              New-Item -Path "$PhysicalPath" -ItemType Directory -Force
                                            }
                                            robocopy.exe "$tempPath\$Site" "$PhysicalPath" /MIR /mt:4 /nfl /ndl /np /R:2 /W:5
                                            Remove-Item "$tempPath\$Site" -Force -Recurse
                                            }
                                            #Create a website                                                                                        
                                            New-Item "IIS:\Sites\$Site" -bindings $bindings -physicalPath $PhysicalPath.replace("/","\") -ApplicationPool $AppPool -ErrorAction Stop

                                            #Verify if physical path exists. Create if not. To enable auth or impersonation settings for site
                                                        
                                          Try{
                                            #Apply Anonymous authentication setting
                                           if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                            {
                                                Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $k.$SiteName.AnonymousAuth -PSPath IIS:\ -Location "$Site"
                                                Write-Host "AnonymouseAuth setting updated for Site:$Site"
                                            }

                                            #Apply Windows authentication setting
                                            if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                            {
                                               
                                                if($WindowsAuth -ieq "windows")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq "basic")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq $false)
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Write-Host "WindowsAuth and BasicAuth are disabled for Site:$Site"                
                                                }
                                            }
                                
                                            #Apply ASP.NET Impersonation setting
                                            
                                            if(!([string]::IsNullOrEmpty($AspNetImpersonation)))
                                            {
                                                &$appcmdPath set config "$Site" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                                Write-Host "Asp.Net Impersonation enabled for Site:$Site" 
                                            }
                                                                           
                                            }
                                            Catch{
                                                Write-Host "Could not process Anonymous Authentication or Windows Authentication or AspNetImpersonation for WebSite:$Site. Exception Messsage - $($error[0].Exception)"
                                                Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"  
                                            } 
                                        }    

                                        

                                     else{
                                     if((Test-Path ("IIS:\Sites\" + "$SiteName") -PathType Container) )
                                   {
                                     #Write-Host "website $SiteName doesnot exist  " -ForegroundColor Green

                                     Write-Warning " website $sitename is already exist and hence removing"

               

                                        Remove-Item "IIS:\Sites\$SiteName" -Force -Recurse -ErrorAction Stop
                                        Write-Host "Deleted WebSite: $SiteName" -ForegroundColor Yellow
                                   

                                          # featch site details from JSON
                                          
                                             ($k=$json.Deploy.ENV.$env.Sites)|Where-Object{$_.Name -eq $SiteName}

                                             
                                             $Site=$k.$SiteName.name
                                             $AppPool=$k.$SiteName.AppPool
                                             $BindingPort=$k.$SiteName.BindingPort
                                             $PhysicalPath=$k.$SiteName.PhysicalPath
                                             $EnabledProtocol=$k.$SiteName.EnabledProtocol
                                             $AnonymousAuth=$k.$SiteName.AnonymousAuth
                                             $WindowsAuth=$k.$SiteName.WindowsAuth
                                             $AspNetImpersonation=$k.$SiteName.AspNetImpersonation


                                              

                                            Write-Host "****************Creating WebSite : $Site****************"

                                            if(Test-Path ("IIS:\AppPools\"+"$($AppPool)") -PathType Container)
                                        { 
                                             $protocols=$($EnabledProtocol).Split(',').Split(';')  
                                             $bindInfo=$($BindingPort).Split(',').Split(';')                                             

                                             $bindings = @()
                                             $count = 0

                                             #Multiple Bindings for website will be formed here based on EnabledProtocol and BindingPort
                                             foreach($item in $protocols)
                                             {
                                                $bindings+= "@{protocol="+$item+";bindingInformation=$(@($bindInfo)[$count])}"  
                                                $count++
                                             }
                                            
                                             
                                              Add-Type -AssemblyName System.IO.Compression.FileSystem
                                              function Unzip
                                              {
                                                  param([string]$zipfile, [string]$outpath)
                                              
                                                  [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
                                              }
                                             
                                             if(test-path "$tempPath\$Site")
                                             {
                                              Remove-Item "$tempPath\$Site" -Force -Recurse
                                             }
                                              
                                            Unzip "$tempPath\$($Site).zip" "$tempPath\$Site"

                                             if(test-path "$PhysicalPath")
                                             {
                                              Remove-Item "$PhysicalPath" -Force -Recurse
                                              New-Item "$PhysicalPath" -ItemType Directory -Force
                                             }
                                            else {
                                              New-Item -Path "$PhysicalPath" -ItemType Directory -Force
                                            }
                                            robocopy.exe "$tempPath\$Site" "$PhysicalPath" /MIR /mt:4 /nfl /ndl /np /R:2 /W:5
                                            Remove-Item "$tempPath\$Site" -Force -Recurse
                                            }
                                            #Create a website                                                                                        
                                            New-Item "IIS:\Sites\$Site" -bindings $bindings -physicalPath $PhysicalPath.replace("/","\") -ApplicationPool $AppPool -ErrorAction Stop

                                            #Verify if physical path exists. Create if not. To enable auth or impersonation settings for site
                                                        

                                            Try{
                                            #Apply Anonymous authentication setting
                                            if(!([string]::IsNullOrEmpty($AnonymousAuth)))
                                            {
                                                Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/anonymousAuthentication" -Name Enabled -Value $k.$SiteName.AnonymousAuth -PSPath IIS:\ -Location "$Site"
                                                Write-Host "AnonymouseAuth setting updated for Site:$Site"
                                            }

                                            #Apply Windows authentication setting
                                            if(!([string]::IsNullOrEmpty($WindowsAuth)))
                                            {
                                               
                                                if($WindowsAuth -ieq "windows")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq "basic")
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif(($WindowsAuth -ieq "windowsandbasic") -or ($WindowsAuth -ieq "windowsbasic"))
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "WindowsAuth enabled for Site:$Site"
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value True -PSPath IIS:\ -Location "$Site" -ErrorAction Stop                
                                                    Write-Host "BasicAuth enabled for Site:$Site"
                                                }
                                                elseif($WindowsAuth -ieq $false)
                                                {
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/windowsAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/BasicAuthentication" -Name Enabled -Value False -PSPath IIS:\ -Location "$Site" -ErrorAction Stop
                                                    Write-Host "WindowsAuth and BasicAuth are disabled for Site:$Site"                
                                                }
                                            }
                                
                                            #Apply ASP.NET Impersonation setting
                                            if(!([string]::IsNullOrEmpty($AspNetImpersonation)))
                                            {
                                                &$appcmdPath set config "$Site" -section:"system.web/identity" /impersonate:"$($AspNetImpersonation)"
                                                Write-Host "Asp.Net Impersonation enabled for Site:$Site" 
                                            }
                                                                           
                                            }
                                            Catch{
                                                Write-Host "Could not process Anonymous Authentication or Windows Authentication or AspNetImpersonation for WebSite:$Site. Exception Messsage - $($error[0].Exception)"
                                                Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"  
                                            } 
                                        }
                                        else
                                        {
                                            Write-Host "Could not process website $Site. As AppPool $($AppPool) not found"
                                             
                                        }    

                                     } 


                                                                                
                                     
                                     
                                    # done with site creatioin

                                    }
                                    catch
                                    {
                                      Write-Host "Could not process website $Site. Exception Messsage - $($error[0].Exception)"
                                      Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)"
                                         
                                    }
                     }#end if for website





Write-Host "**************************************** END with Sites *******************************************"
}# end of Try blcok for web sites
catch{

    # Write-Host "Sites : Issue during deployment workflow in $ServerName. Exception Messsage - $($error[0].Exception)" 
    # Write-Host "ERROR Line No : $($error[0].invocationinfo.ScriptLineNumber)" 
 }


}# end of script block



  }# end of server connection
   }# end server foreach
  }#end of main foreach

}
else{
    Write-host " something went wrong. please reach devops admin to know more details" -ForegroundColor Red
}





} # end IF Full Deploy

}# end else if in full deployment