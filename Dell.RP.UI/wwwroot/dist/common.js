/*!
 * Bootstrap Auto-dismiss alerts Common Global Scripts
 */
(function($) {
    $('.alert[data-auto-dismiss]').each(function(index, element) {
        var $element = $(element),
            timeout = $element.data('auto-dismiss') || 5000;
        setTimeout(function() {
            $element.alert('close');
        }, timeout);
    });
})(jQuery);