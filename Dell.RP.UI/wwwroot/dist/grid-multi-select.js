
$(function () {
    $('.multiselect-ui').multiselect({
        includeSelectAllOption: true,
        enableFiltering: true,
        maxHeight: 200,
        numberDisplayed: 1,
        enableCaseInsensitiveFiltering: true,
        filterBehavior: 'text',
        includeResetOption: true,
        resetText: "Clear All",
        optionClass: function (element) {
            var value = $(element).val();
            if (value % 2 === 0) {
                return 'grid-OptionClass';
            }
            else {
                return 'grid-OptionClass';
            }
        }
    });
    $('.multiselect.dropdown-toggle').trigger('click');
});

