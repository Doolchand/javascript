import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule, BrowserXhr } from '@angular/http';
import { RouterModule } from '@angular/router';
import { NgProgressModule, NgProgressInterceptor, NgProgressBrowserXhr } from 'ngx-progressbar'; 
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { NgIdleModule } from '@ng-idle/core';
import { Ng2Webstorage } from 'ngx-webstorage';
import { DatepickerModule } from 'ng2-bootstrap';
import { DatePipe } from '@angular/common';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BrowserModule } from '@angular/platform-browser'
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
// Router Module
import { routes } from './app.routes';
//Components
import { AppComponent } from './components/app/app.component';
import { HomeComponent } from './components/home/home.component';
import { NavComponent } from './components/navbar/nav.component';
import { CheckboxCellCommonComponent } from './components/checkbox-renderer.component'; 
/*import { RoutingRulesComponent } from "./components/routing-rules/routing-rules.component"*/;
import { ErrorGridComponent } from './components/error-grid/error-grid.component';
import { FulfillmentSiteAssignmentComponent } from "./components/fulfillment-siteassignment/fulfillment-siteassignment.component";
import { DemandsplitComponent } from "./components/demand-split/demand-split.component";
import { TreeViewComponent } from "./components/tree-view/tree-view.component";
import { RoutingRulesComponent } from "./components/routingrules/routingrules.component";
import { SourcingRuleComponent } from "./components/sourcing-rule/sourcing-rule.component";
import { SupplyPlanningFhcComponent } from "./components/supply-planning-fhc/supply-planning-fhc.component";
import { Copyroutingrules } from "./components/routingrulescopy/routingrulescopy.component";
import { MaintenanceComponent } from "./components/maintenance/maintenance.component";


// Services
import { LoginService } from './services/login.service';
import { UserMaintenanceService } from './services/user-maintenance.service';
import { UtilitiesService } from './services/utilities.service'; 
import { FulfillmentSiteAssignmentService } from "./services/fulfillment-siteassignment.service";
 
import { demandsplitService } from "./services/demand-split.service";
import { routingrulesService } from './services/routingrules.service';
import { SourcingRuleService } from "./services/sourcing-rule.service";
import { SupplyPlanningFhcService } from "./services/supply-planning-fhc.service";

// ag-grid
import { AgGridModule } from 'ag-grid-angular';
import { Grid } from 'ag-grid/main';
import { LoginComponent } from './components/login/login.component';
import { GetErrorGridService } from './services/get-error-grid.service';
import { RoutingRulesService } from "./services/routing-rules.service";
import {
    TreeviewModule,
    DropdownTreeviewComponent,
    TreeviewConfig,
    TreeviewI18nDefault,
    TreeviewI18n,
    DefaultTreeviewEventParser,
    TreeviewEventParser,
    TreeviewItem
} from "ngx-treeview";

import { TreeModule } from 'angular-tree-component';
import { MultiTierBOM } from './components/multi-tier-bom/multi-tier-bom.component';
import { MultiTierBOMService } from './services/multi-tier-bom.service';
import { CheckboxRenderer } from './components/multi-tier-bom/checkbox-renderer.component';
import { L10ItemList } from './components/l10-item-list/l10-item-list.component';
import { CommodityCodeList } from './components/commodity-code-list/commodity-code-list.component';
import { CommodityCodeListService } from './services/commodity-code-list.service';
import { L10ItemListService } from './services/l10-item-list.service';
import { ButtonRendererComponent } from './components/sourcing-rule/button-renderer.component';
import { buttonreder } from './components/routingrules/button-reder.component';
import { CheckboxCommodity } from './components/commodity-code-list/checkbox-commodity.component';
import { CurrentweekDatepickerComponent } from './components/sourcing-rule/currentweek-datepicker.component'


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        NavComponent,
        CheckboxCellCommonComponent,
        ErrorGridComponent,
        LoginComponent,
        FulfillmentSiteAssignmentComponent,
        RoutingRulesComponent,
        DemandsplitComponent,
        TreeViewComponent,
        MultiTierBOM,
        CheckboxRenderer,
        ButtonRendererComponent,
        CurrentweekDatepickerComponent,
        L10ItemList,
        CommodityCodeList,
        RoutingRulesComponent,
        SourcingRuleComponent,
        SupplyPlanningFhcComponent,
        Copyroutingrules,
        CheckboxCommodity,
        MaintenanceComponent
       
    ],
    imports: [
        CommonModule,
        TreeviewModule,
        HttpModule,
        FormsModule,
        NgProgressModule,       
        Ng2Webstorage,
        AngularMultiSelectModule,
        MultiselectDropdownModule,
        BrowserModule,
        NgMultiSelectDropDownModule.forRoot(),
        DatepickerModule.forRoot(),
        AgGridModule.withComponents([
            CheckboxCellCommonComponent,
            CheckboxRenderer,
            ButtonRendererComponent,
            CurrentweekDatepickerComponent,
            CheckboxCommodity
        ]),
        NgIdleModule.forRoot(),
        AccordionModule.forRoot(),
        ButtonsModule.forRoot(),
        TabsModule.forRoot(),
        RouterModule.forRoot(routes, { useHash: true, }),
        TreeviewModule.forRoot(),
        TreeModule.forRoot(),
    ],
    providers: [      
        LoginService,
        UserMaintenanceService,
        UtilitiesService,      
        DatePipe,
        FulfillmentSiteAssignmentService,
        GetErrorGridService,
        RoutingRulesService,
        demandsplitService,
        TreeviewConfig,
        { provide: TreeviewI18n, useClass: TreeviewI18nDefault },
        { provide: TreeviewEventParser, useClass: DefaultTreeviewEventParser },
        { provide: BrowserXhr, useClass: NgProgressBrowserXhr },
        MultiTierBOMService,
        CommodityCodeListService,
        L10ItemListService,
        routingrulesService,
        SourcingRuleService,
        SupplyPlanningFhcService,
    ]
})
export class AppModuleShared {
}
