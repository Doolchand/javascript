import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ICellEditorAngularComp } from "ag-grid-angular/main";
//import {SessionStorageService} from 'ngx-webstorage';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-bootstrap-datepicker',
    template: `
        <span #calenderEditor><datepicker (selectionDone)="onClick($event)" [showWeeks]="false"></datepicker></span>
        
        <select class="form-control" name="params" #ddlEditor (change)="RoutingsChange($event)" [(ngModel)]="params.value">
        <option value=""></option>
        <option *ngFor="let x of currWeekData; " [value]="x.itemName">{{x.itemName| uppercase}}</option>
        </select>
    `
    //<option selected="selected" value="">-- Select --</option>
    //<input type="text" name="effectiveDate" #textEditor [(ngModel)]="params.value">
})
export class BootstrapDatepickerComponent implements ICellEditorAngularComp {
    private params: any;
    @ViewChild('calenderEditor') calenderEditor: ElementRef;
    @ViewChild('ddlEditor') ddlEditor: ElementRef;
    public selectedDate: Date = new Date();
    public currWeekData: any = [];
    public userId:string;
    //public selectedVal:any;
    

    constructor(private _sessionSt: SessionStorageService){
        this.userId = this._sessionSt.retrieve('userID');
        //this.fgaComAttService = _fgaFcCommonfiltersService;
    }

    agInit(params: any): void {      
            var object = {
                UserID: this.userId,
                Region: 'abc'         
        };

        const datePicker: HTMLElement = this.calenderEditor.nativeElement as HTMLElement;
        const ddl: HTMLElement = this.ddlEditor.nativeElement as HTMLElement;
        

        if(params.node.data.timePeriodCalcMethod === "CURRENT DATE FORWARD ROLL"){
            datePicker.style.display = "none";
            ddl.style.display = "block";
           // this.selectedVal = params;
            this.params = params;        
        //this.getcurrentWeeks = this.params.context.DropDownRender.getCurrentWeekData;
        }
        else{   
            datePicker.style.display = "block";         
            ddl.style.display = "none";
            this.params = params;
        }
        loadJavaScript();
    }

    RoutingsChange(event: any) {            
        //this.params.data[this.params.colDef.field] = event.currentTarget.value;
        this.params.value = event.currentTarget.value;
        this.params.node.setSelected(true);
    }

    getValue(): any {     
        if(this.params.node.data.timePeriodCalcMethod === "CURRENT DATE FORWARD ROLL"){
            loadJavaScript();
            return this.params.value;
        }
        else{ 
            //return `${this.selectedDate.getMonth() + 1}/${this.selectedDate.getDate()}/${this.selectedDate.getFullYear()}`;
            var year = this.selectedDate.getFullYear();
            var month = (1 + this.selectedDate.getMonth()).toString();
            month = month.length > 1 ? month : '0' + month;
            var day = this.selectedDate.getDate().toString();
            day = day.length > 1 ? day : '0' + day;
            return month + '/' + day + '/' + year;
        }
    }

    isPopup(): boolean {
        return true;
    }

    onClick(date: Date) {
        this.selectedDate = date;
        this.params.api.stopEditing();
    }
}

function loadJavaScript(){
    var url = '/dist/grid-multi-select.js';
    var s = document.createElement('script');
    s.setAttribute('src', url);
    document.head.appendChild(s);
}