// import the required animation functions from the angular animations module
import { trigger, state, animate, transition, style } from '@angular/animations';

export const effectElement =
   // trigger name for attaching this animation to an element using the [@effectElement] syntax   
   trigger('effectElement', [
    // What happens when effectElement is true
    state('true' , style({ display: 'block' })),
    // What happens when effectElement is false
    state('false', style({ display: 'none' })),
    // transition
    transition('true => false', animate('3000ms')),
    transition('false => true', animate('3000ms')),
  ]);