// import the required animation functions from the angular animations module
import { trigger, state, animate, transition, style } from '@angular/animations';

export const slideInOutAnimation =
   // trigger name for attaching this animation to an element using the [@triggerName] syntax
   trigger('slideInOutAnimation', [

    state('small', style({
        transform: 'scale(1)',
    })),
    state('large', style({ 
        transform: 'scale(1.2)',
    })),
    transition('small <=> large', animate('300ms ease-in', style({
        transform: 'translateY(40px)'
      }))),

      
   ]);