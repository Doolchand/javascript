import { Routes } from '@angular/router';
import { NavComponent } from './components/navbar/nav.component';
import { HomeComponent } from './components/home/home.component';
import { FulfillmentSiteAssignmentComponent } from './components/fulfillment-siteassignment/fulfillment-siteassignment.component';
//import { RoutingRulesComponent } from "./components/routing-rules/routing-rules.component";
 
import { TreeViewComponent } from './components/tree-view/tree-view.component';
import { DemandsplitComponent } from "./components/demand-split/demand-split.component";
import { MultiTierBOM } from './components/multi-tier-bom/multi-tier-bom.component';
import { CommodityCodeList } from './components/commodity-code-list/commodity-code-list.component';
import { L10ItemList } from './components/l10-item-list/l10-item-list.component';
import {SourcingRuleComponent } from './components/sourcing-rule/sourcing-rule.component';
import { RoutingRulesComponent } from './components/routingrules/routingrules.component';
import { SupplyPlanningFhcComponent } from './components/supply-planning-fhc/supply-planning-fhc.component';
import { Copyroutingrules } from './components/routingrulescopy/routingrulescopy.component';
import { MaintenanceComponent } from './components/maintenance/maintenance.component';



export const routes: Routes = [
    { path: 'home', component: HomeComponent, pathMatch: 'full' },
    { path: 'fulfillmentSiteAssignment', component: FulfillmentSiteAssignmentComponent, pathMatch: 'full' },
    { path: 'treeView', component:TreeViewComponent, pathMatch:'full'},
    { path: 'routingrules', component: RoutingRulesComponent, pathMatch: 'full' },
    { path: 'demandsplit', component: DemandsplitComponent, pathMatch: 'full' },
    { path: 'supplyPlanningFhc', component: SupplyPlanningFhcComponent, pathMatch: 'full' },
	{ path: 'multiTierBOM', component: MultiTierBOM, pathMatch: 'full' },
    { path: 'l10ItemList', component: L10ItemList, pathMatch: 'full' },
    { path: 'commodityCodeList', component: CommodityCodeList, pathMatch: 'full' },
    { path: 'SourcingRuleComponent', component: SourcingRuleComponent, pathMatch: 'full' },
    { path: 'maintenanceComponent', component: MaintenanceComponent, pathMatch: 'full' },
    { path: '**', component: HomeComponent },
    { path: 'copyroutingrules', component: Copyroutingrules, pathMatch: 'full' }
  ];


