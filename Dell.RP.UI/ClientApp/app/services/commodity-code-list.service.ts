﻿import { Injectable } from '@angular/core';
import { Response, Headers, Http, RequestOptions, ResponseContentType } from "@angular/http";
import { environment } from '../../environments/environment';
import { UtilitiesService } from './../services/utilities.service';

// Excel Functions
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Observable } from 'rxjs';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class CommodityCodeListService {

    apiUrl: string = environment.URL;
    headers: Headers;
    options: RequestOptions;

    constructor(private _http: Http, private _utilitiesService: UtilitiesService) {
        this.headers = new Headers({
            'Content-Type': 'application/json', 'Cache-Control': 'no-cache', 'Pragma': 'no-cache',
            'Accept': 'q=0.8;application/json;q=0.9'
        });
        this.options = new RequestOptions({ headers: this.headers, withCredentials: true });
    }

    // Upload Excel Data
    private extractData(res: Response) {
        return res;
    }

    GetFilterViewResult(multiTierBom: any) {
        let url = this.apiUrl + '' + 'CommodityCodeList/GetFilterViewResult';
        return this._http.post(url, multiTierBom, this.options);
    }

    UpdateCommodityCodeListDetails(param: any): Observable<any> {
        let url = this.apiUrl + 'CommodityCodeList/UpdateCommodityCodeListDetails'
        return this._http.post(url, param, this.options)
            .map(this.extractData);
    }

    CommodityCodeListExcelExport(json: any[]) {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, "Commodity Code List");
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_' + this._utilitiesService.getDatetime() + EXCEL_EXTENSION);
    }

}