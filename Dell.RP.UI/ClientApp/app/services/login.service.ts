import { Injectable } from '@angular/core';
//import { HttpClient, HttpHeaders, HttpErrorResponse  } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Response, RequestMethod, Http, Headers, RequestOptions, URLSearchParams, ResponseContentType } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {
  url : string = environment.URL;
  headers: Headers;
  options: RequestOptions;
  
    constructor(private http: Http) {
        this.headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'q=0.8;application/json;q=0.9' });
        this.options = new RequestOptions({ headers: this.headers, responseType: ResponseContentType.Text, withCredentials: true });
    }

    GetUserData() {       
        return this.http.get(this.url + 'Auth/Login', { withCredentials: true });
    }

    GetSessionSetting() {
        return this.http.get(this.url + 'Auth/GetSessionSettings', { withCredentials: true });
    }   

  updateSession(param: any): Observable<any>{    
      let url = this.url + 'Auth/updateUserSession';
      return this.http.post(url, param, this.options)     
  }

  private handleError(err) {    
    console.log(err.message);
    return Observable.throw(err.message);
  }

}
