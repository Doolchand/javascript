﻿import { Injectable } from '@angular/core';
import { Response, Headers, Http, RequestOptions, ResponseContentType } from "@angular/http";
import { environment } from '../../environments/environment';
import { UtilitiesService } from './../services/utilities.service';

// Excel Functions
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Observable } from 'rxjs';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class RoutingRulesService {

    apiUrl: string = environment.URL;
    headers: Headers;
    options: RequestOptions;

    constructor(private _http: Http, private _utilitiesService: UtilitiesService) {
        this.headers = new Headers({
            'Content-Type': 'application/json', 'Cache-Control': 'no-cache', 'Pragma': 'no-cache',
            'Accept': 'q=0.8;application/json;q=0.9'
        });
        this.options = new RequestOptions({ headers: this.headers, withCredentials: true });
    }

    // Import Excel Template
    ImportExcelTemplate(param: any) {
        let url = this.apiUrl + 'EmcProductHierarchyMaintenance/ImportExcelTemplate';
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ responseType: ResponseContentType.Blob, headers, withCredentials: true });
        var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        this._http.post(url, param, options)
            .map(res => res.blob())
            .subscribe(
                data => {
                    FileSaver.saveAs(data, 'EMCProductHierarchyTemplate' + '_' + this._utilitiesService.getDatetime() + '.xlsx');
                },
                err => {
                    console.error(err);
                }
            );
    }

    // Upload Excel Data
    private extractData(res: Response) {
        return res;
    }

    uploadExcelData(param: any): Observable<any> {
        let url = this.apiUrl + 'EmcProductHierarchyMaintenance/UploadExcelData';
        return this._http
            .post(url, param, this.options)
            .map(this.extractData);
    }

    uploadLobData(param: any): Observable<any> {
        let url = this.apiUrl + 'EmcProductHierarchyMaintenance/UploadLobData';
        return this._http
            .post(url, param, this.options)
            .map(this.extractData);
    }

    getHierarchyLevel() {
        // Make the HTTP request:
        return this._http.get(this.apiUrl + 'EmcProductHierarchyMaintenance/GetHierarchyLevel', { withCredentials: true });
    };

    getHierarchyMember(hierarchyLevel, filter) {
        // Make the HTTP request:
        return this._http.get(this.apiUrl + 'EmcProductHierarchyMaintenance/GetHierarchyMember?hierarchyLevel=' + hierarchyLevel + '&filter=' + filter, { withCredentials: true });

    };

    GetEmcFilterViewResult(levelId, memeberId: any) {
        let url = this.apiUrl + '' + 'EmcProductHierarchyMaintenance/GetEmcFilterViewResult?levelId=' + levelId;
        return this._http.post(url, memeberId, this.options).map(this.extractData);
    }

    GetCreateEmcFilterViewResult(selected_FamBase: any) {
        let url = this.apiUrl + '' + 'EmcProductHierarchyMaintenance/GetEmcCreateFilterViewResult';
        return this._http.post(url, selected_FamBase, this.options).map(this.extractData);
    }

    ExcelExport(json: any[]) {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'EMCProductHierarchyMaintenance': worksheet }, SheetNames: ['EMCProductHierarchyMaintenance'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, "EMCProduct_Hierarchy_Maintenance");
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_' + this._utilitiesService.getDatetime() + EXCEL_EXTENSION);
    }

    public exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'EMCErrorData': worksheet }, SheetNames: ['EMCErrorData'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    updateHierarchyMemberCreationDetails(param: any) {
        let url = this.apiUrl + 'EmcProductHierarchyMaintenance/UpdateMemberCreationDetails'
        return this._http
            .post(url, param, this.options)
            .map(this.extractData);
    }

    //getSequenceNumber() {
    //    // Make the HTTP request:
    //    return this._http.get(this.apiUrl + 'EmcProductHierarchyMaintenance/GetSequenceNumber', { withCredentials: true });
    //};

    getFamily() {
        // Make the HTTP request:
        return this._http.get(this.apiUrl + 'EmcProductHierarchyMaintenance/GetFamily', { withCredentials: true }).map((data: any) => data.json());
    };

    getItemType() {
        // Make the HTTP request:
        return this._http.get(this.apiUrl + 'EmcProductHierarchyMaintenance/GetItemType', { withCredentials: true }).map((data: any) => data.json());
    };

}