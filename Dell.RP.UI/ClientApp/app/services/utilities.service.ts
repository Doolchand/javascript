import { Injectable } from '@angular/core';
import { Response, RequestMethod, Http, Headers, RequestOptions, URLSearchParams, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import * as FileSaver from 'file-saver';
import { DatePipe } from '@angular/common';

@Injectable()
export class UtilitiesService {

    constructor(private datePipe: DatePipe) { }

    ReplaceSplCharFromString(data: string) {
        return data.includes("!!") ? data.replace("!!", "'") : data;
    }


    ReplaceSplCharFromStringList(data: string[], variableName: string) {

        return data.forEach(function (item) {
            item[variableName] = item[variableName].split("!!").join("'") //item[variableName].replace("!!", "'")
        });
    }

    getDatetime() {
        var d = new Date();
        return this.datePipe.transform(d, "yyyyMMdd") + "_" + d.getHours().toString() + d.getMinutes().toString() + d.getSeconds().toString();
    }

    //This function is for validate date 
    IsDateValid(date) {
        var dateResult = this.ConvertDateInProperFormat(date);
        var isCorrectDate = dateResult.split('*')[0];
        var modifiedDate = dateResult.split('*')[1];
        var isCorrectDateByRegex = false;
        if (isCorrectDate == 'true') {
            date = modifiedDate;
            isCorrectDateByRegex = this.CheckDateByRegex(date);

        }
        else {
            isCorrectDateByRegex = false;

        }
        //alert(isCorrectDateByRegex);
        return isCorrectDateByRegex;
    }

    CheckDateByRegex(date) {
        /*
        -valid format mm/dd/yyyy
        - check leap year 
        - check 31st and 30th date in as required in the month 
        */
        //this is for mm/dd/yyyy
        var dateRegcheck = /^(((0[1-9]|1[012])\/(?!00|29)([012]\d)|(0[13-9]|1[012])\/(29|30)|(0[13578]|1[02])\/31)\/(18|19|20)\d{2}|02\/29\/((18|19|20)(0[48]|[2468][048]|[13579][26])|2000))$/
        //var dateRegcheck =/^[^<>\s\@]+(\@[^<>\s\@]+(\.[^<>\s\@]+)+)$/;
        if (date.toUpperCase().trim().match(dateRegcheck)) {
            return true;
        }
        else {
            return false;
        }
    }

    ConvertDateInProperFormat(date) {
        var isValid = 'false';
        var datePartArr = [];
        datePartArr = date.split('/');
        var maxTwoDigitRegCheck = /^[0-9]?[0-9]$/
        var maxFourDigitRegCheck = /^[0-9][0-9][0-9][0-9]$/
        var finalResultDate = ''
        //check for date contains two '/' or not 
        if (datePartArr.length == 3) {
            var month = datePartArr[0];
            var day = datePartArr[1];
            var year = datePartArr[2];

            if (day.toUpperCase().trim().match(maxTwoDigitRegCheck) && month.toUpperCase().trim().match(maxTwoDigitRegCheck) && day.toUpperCase().trim().match(maxTwoDigitRegCheck)) {
                if (day.length == 1) {
                    day = '0' + day;
                }
                if (month.length == 1) {
                    month = '0' + month;
                }
                isValid = 'true';
            }
            else {
                isValid = 'false';
            }
        }
        else {
            isValid = 'false';
        }

        finalResultDate = isValid + '*' + month + "/" + day + "/" + year;
        return finalResultDate;
    }
}
