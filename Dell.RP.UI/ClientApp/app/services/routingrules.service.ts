﻿import { Injectable } from '@angular/core';
import { Response, Headers, Http, RequestOptions, ResponseContentType } from "@angular/http";
import { environment } from '../../environments/environment';
import { UtilitiesService } from './../services/utilities.service';
import { TreeviewItem, TreeviewConfig } from "ngx-treeview";


// Excel Functions
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Observable } from 'rxjs';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';


@Injectable()

export class routingrulesService {
    apiUrl: string = environment.URL;
    headers: Headers;
    options: RequestOptions;


    constructor(private _http: Http, private _utilitiesService: UtilitiesService) {
        this.headers = new Headers({
            'Content-Type': 'application/json', 'Cache-Control': 'no-cache', 'Pragma': 'no-cache',
            'Accept': 'q=0.8;application/json;q=0.9'
        });
        this.options = new RequestOptions({ headers: this.headers, withCredentials: true });
    }

    //GetEmcFilterViewResult(SearchView:any) {
    //     
    //    let url = this.apiUrl + '' + 'EmcProductHierarchyMaintenance/GetEmcFilterViewResult?levelId=' + levelId;
    //    return this._http.post(url, memeberId, this.options).map(this.extractData);
    //}

    private extractData(res: Response) {
        return res;
    }



    getMasterDataData(key: string) {
        // Make the HTTP request:
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/GetMasterDataData?key=' + key, { withCredentials: true });
    };



    GetRoutingDropdownvalue(deleteParams: any) {
        let url = this.apiUrl + '' + 'RoutingRules/GetRoutingDropdownvalue'
        return this._http.post(url, deleteParams, this.options).map(this.extractData);
    }




    GetRoutingRuladdupdateGrid(deleteParams: any) {
        let url = this.apiUrl + '' + 'RoutingRules/GetRoutingRuladdupdateGrid'
        return this._http.post(url, deleteParams, this.options).map(this.extractData);
    }


    DeleteRoutingRules(deleteParams: any) {
        let url = this.apiUrl + '' + 'RoutingRules/DeleteRoutingRules'
        return this._http.post(url, deleteParams, this.options).map(this.extractData);
    }



    

    SaveRauting(Saveparam: any) {

       // let url = this.apiUrl + '' + 'RoutingRules/SaveRauting?fulfillmentid=' + fulfillmentid + '&product=' + product
        let url = this.apiUrl + '' + 'RoutingRules/SaveRauting'
        return this._http.post(url, Saveparam, this.options).map(this.extractData);
    }




    DateValidate(Saveparam: any) {

        // let url = this.apiUrl + '' + 'RoutingRules/SaveRauting?fulfillmentid=' + fulfillmentid + '&product=' + product
        let url = this.apiUrl + '' + 'RoutingRules/DateValidate'
        return this._http.post(url, Saveparam, this.options).map(this.extractData);
    }


    


    GetGeographyData() {
        
        // Make the HTTP request:
        return this._http.get(this.apiUrl + 'RoutingRules/GetGeographyData', { withCredentials: true });
    };



    

    GetFulfilment() {
        
        // Make the HTTP request:
        return this._http.get(this.apiUrl + 'RoutingRules/GetFulfilment', { withCredentials: true });
    };


    GetDate() {
        
        // Make the HTTP request:
        return this._http.get(this.apiUrl + 'RoutingRules/GetDate', { withCredentials: true });
    };



    GetFGA() {

        // Make the HTTP request:
        return this._http.get(this.apiUrl + 'RoutingRules/GetFGA', { withCredentials: true });
    };

    
    
    GetEffectiveEndDate() {

        // Make the HTTP request:
        return this._http.get(this.apiUrl + 'RoutingRules/GetEffectiveEndDate', { withCredentials: true });
    };




    GetRoutingRulesViewResult(SearchView: any) {
        
        let url = this.apiUrl + 'RoutingRules/GetRoutingRulesViewResult';
        return this._http.post(url, SearchView, this.options).map(this.extractData);
    }


    


    SubmiteCopyRules(SearchView: any) {

        let url = this.apiUrl + 'RoutingRules/SubmiteCopyRules';
        return this._http.post(url, SearchView, this.options).map(this.extractData);
    }


    RetriveRoutingRule(SearchView: any) {
        
        let url = this.apiUrl + 'RoutingRules/RetriveRoutingRule';
        return this._http.post(url, SearchView, this.options).map(this.extractData);
    }



    




    GetManufacturingSiteData(Fulfillment: string) {
        // Make the HTTP request:
        
        return this._http.get(this.apiUrl + 'RoutingRules/GetManufacturingSiteData?key=' + Fulfillment, { withCredentials: true });

    };



    public exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_' + this._utilitiesService.getDatetime() + EXCEL_EXTENSION);
    }



    ExcelExport(json: any[]) {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, "RoutingRules");
    }




    importDemandDetails(param): Observable<any> {

        var methodType = "IMPORT";
        var flag = "I";
        let url = this.apiUrl + 'DemandSplit/uploadDemandData';
        return this._http.post(url, param, this.options).map(this.extractData);
    }

}