﻿import { Injectable } from '@angular/core';
import { Response, Headers, Http, RequestOptions, ResponseContentType } from "@angular/http";
import { environment } from '../../environments/environment';
import { UtilitiesService } from './../services/utilities.service';
import { TreeviewItem, TreeviewConfig } from "ngx-treeview";

// Excel Functions
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Observable } from 'rxjs';
import { FulFillmentSearchInput } from '../Model/treeview/FulFillmentSearchInput';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()

export class SourcingRuleService {

    apiUrl: string = environment.URL;
    headers: Headers;
    options: RequestOptions;
    constructor(private _http: Http, private _utilitiesService: UtilitiesService) {   
        this.headers = new Headers({
            'Content-Type': 'application/json', 'Cache-Control': 'no-cache', 'Pragma': 'no-cache',
            'Accept': 'q=0.8;application/json;q=0.9'
        });
        this.options = new RequestOptions({ headers: this.headers, withCredentials: true });
    }

    private extractData(res: Response) {
        return res;
    }

    getGMPChannelGeoTreeData(strString: any) {
        return this._http.get(this.apiUrl + 'SourcingRule/GetProductTreeData?strString=' + strString, { withCredentials: true });
    };

    getGMPProductTreeData(strString: any) {
        return this._http.get(this.apiUrl + 'SourcingRule/GetProductTreeData?strString=' + strString, { withCredentials: true });
    };

    GetChannelProductsDDL() {
       // return this._http.get(this.apiUrl + 'SourcingRule/GetChannelProducts', { withCredentials: true });
        return this._http.get(this.apiUrl + 'SourcingRule/GetChannelProducts', { withCredentials: true }).map((data: any) => data.json());
    };

    GetChannelGeosDDL() {
        // return this._http.get(this.apiUrl + 'SourcingRule/GetChannelProducts', { withCredentials: true });
        return this._http.get(this.apiUrl + 'SourcingRule/GetChannelGeo', { withCredentials: true }).map((data: any) => data.json());
    };

    GetSites() {
        // return this._http.get(this.apiUrl + 'SourcingRule/GetChannelProducts', { withCredentials: true });
        return this._http.get(this.apiUrl + 'SourcingRule/GetSites', { withCredentials: true }).map((data: any) => data.json());
    };

    GetSourcingRuleLineItems(channelId: string, productId:string ) {
        // return this._http.get(this.apiUrl + 'SourcingRule/GetChannelProducts', { withCredentials: true });
        return this._http.get(this.apiUrl + 'SourcingRule/GetSourcingRuleLineItems?startChannelId=' + channelId+ '&startProductId=' + productId, { withCredentials: true }).map((data: any) => data.json());
    };

    GetSingleRuleLineItems(channelId: string, productId: string) {
        // return this._http.get(this.apiUrl + 'SourcingRule/GetChannelProducts', { withCredentials: true });
        return this._http.get(this.apiUrl + 'SourcingRule/GetSingleRuleLineItems?startChannelId=' + channelId + '&startProductId=' + productId, { withCredentials: true }).map((data: any) => data.json());
    };

    SaveSourcingRule(sourcingRuleGrid: any) {
        let url = this.apiUrl + 'SourcingRule/SaveSourcingRule';
        return this._http.post(url, sourcingRuleGrid, this.options);
    }

    DeleteSourcingRuleCollection(productId:string, channelId:string) {
        let url = this.apiUrl + 'SourcingRule/DeleteSourcingRuleCollection?productId=' + productId+ '&channelId=' + channelId;
        return this._http.post(url, "", this.options);
          //  .map(this.extractData);
    }   

    GetCopiedProccessData(sourcingRuleGrid: any) {
        let url = this.apiUrl + 'SourcingRule/GetCopiedProccessData';
        return this._http.post(url, sourcingRuleGrid, this.options);
    }

    ExcelExport(json: any[]) {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, "SourcingRule");
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_' + this._utilitiesService.getDatetime() + EXCEL_EXTENSION);
    }
}
