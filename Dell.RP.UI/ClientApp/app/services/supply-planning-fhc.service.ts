﻿import { Injectable } from '@angular/core';
import { Response, Headers, Http, RequestOptions, ResponseContentType } from "@angular/http";
import { environment } from '../../environments/environment';
import { UtilitiesService } from './../services/utilities.service';
import { TreeviewItem, TreeviewConfig } from "ngx-treeview";

// Excel Functions
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Observable } from 'rxjs';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class SupplyPlanningFhcService {

    apiUrl: string = environment.URL;
    headers: Headers;
    options: RequestOptions;

    constructor(private _http: Http, private _utilitiesService: UtilitiesService) {
        this.headers = new Headers({
            'Content-Type': 'application/json', 'Cache-Control': 'no-cache', 'Pragma': 'no-cache',
            'Accept': 'q=0.8;application/json;q=0.9'
        });
        this.options = new RequestOptions({ headers: this.headers, withCredentials: true });
    }

    private extractData(res: Response) {
        return res;
    }

    getManSite() {
        return this._http.get(this.apiUrl + 'SupplyPlanningFhc/GetManSite', { withCredentials: true });
    };

    getFamilyParentDD() {
        return this._http.get(this.apiUrl + 'SupplyPlanningFhc/GetFamilyParentDD', { withCredentials: true });
    };

    getRPStatus() {
        return this._http.get(this.apiUrl + 'SupplyPlanningFhc/GetRPStatus', { withCredentials: true });
    };

    bindPOUILock() {
        return this._http.get(this.apiUrl + 'SupplyPlanningFhc/BindPOUILock', { withCredentials: true });
    };

    getExportData(params) {
        let url = this.apiUrl + '' + 'SupplyPlanningFhc/GetRPSupplyPlanningFhcExportDetails';
        return this._http.post(url, params, this.options).map(this.extractData);
    }

    //for export data excel download
    public exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'RP_FHC_Data': worksheet }, SheetNames: ['RP_FHC_Data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }
    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_' + this._utilitiesService.getDatetime() + EXCEL_EXTENSION);
    }

    //for Upload Template download
    public exportAsExcelFileUploadTemplate(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'RP_FHC_Data': worksheet }, SheetNames: ['RP_FHC_Data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFileUploadTemplate(excelBuffer, excelFileName);
    }
    private saveAsExcelFileUploadTemplate(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName +  EXCEL_EXTENSION);
    }

    getStatusForInsertFHC(params) {
        let url = this.apiUrl + '' + 'SupplyPlanningFhc/InsertFhc';
        return this._http.post(url, params, this.options);
    };

    getErrGridDetails() {
        return this._http.get(this.apiUrl + 'SupplyPlanningFhc/GetErrGridDetails', { withCredentials: true });
    };

    getLockStatusFHC(flag) {
        //let url = this.apiUrl + '' + 'SupplyPlanningFhc/GetLockStatusFHC';
        //return this._http.post(url, params, this.options).map(this.extractData);
        return this._http.get(this.apiUrl + 'SupplyPlanningFhc/GetLockStatusFHC?flag=' + flag, { withCredentials: true });
    };

    getValidRecStatusFHC() {
        return this._http.get(this.apiUrl + 'SupplyPlanningFhc/GetValidRecStatusFHC', { withCredentials: true });
    };

    updateAutoUnlockUIFHC() {
        return this._http.get(this.apiUrl + 'SupplyPlanningFhc/UpdateAutoUnlockUIFHC', { withCredentials: true });
    };

}