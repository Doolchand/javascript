﻿import { Injectable } from '@angular/core';
import { Response, Headers, Http, RequestOptions, ResponseContentType } from "@angular/http";
import { environment } from '../../environments/environment';
import { UtilitiesService } from './../services/utilities.service';
import { TreeviewItem, TreeviewConfig } from "ngx-treeview";

// Excel Functions
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Observable } from 'rxjs';
//import { FulFillmentSearchInput, FulfillmentAddUpdateParams } from '../Model/tree-view-data';
import { FulfillmentAddUpdateParams } from '../Model/treeview/FulfillmentAddUpdateParams';
import { FulFillmentSearchInput } from '../Model/treeview/FulFillmentSearchInput';

import { FulfillmentRuleDetails } from '../Model/fulfillment/FulfillmentRuleDetails';
import { FullFillmentRuleData } from '../Model/fulfillment/FullFillmentRuleData';
import { UpdateValidateDateInputParams } from '../Model/fulfillment/UpdateValidateDateInputParams';
import { MainSiteDDLInputParams } from '../Model/fulfillment/MainSiteDDLInputParams';
import { FulfillmentDeleteInputParams } from '../Model/fulfillment/FulfillmentDeleteInputParams';
import { FulfillmentDelete } from '../Model/fulfillment/FulfillmentDelete';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class FulfillmentSiteAssignmentService {

    apiUrl: string = environment.URL;
    headers: Headers;
    options: RequestOptions;

    

    constructor(private _http: Http, private _utilitiesService: UtilitiesService) {
        this.headers = new Headers({
            'Content-Type': 'application/json', 'Cache-Control': 'no-cache', 'Pragma': 'no-cache',
            'Accept': 'q=0.8;application/json;q=0.9'
        });
        this.options = new RequestOptions({ headers: this.headers, withCredentials: true });
    }

    
    //// Upload Excel Data
    private extractData(res: Response) {
        return res;
    }

    getMasterDataData(key:string ) {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/GetMasterDataData?key=' + key , { withCredentials: true });
    };

    GetAccountNameFullfilment() {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/GetAccountNameFullfilment', { withCredentials: true });
    };

    GetChannelType(channelId:string) {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/GetChannelType?channelId=' + channelId , { withCredentials: true });
    };

    getGeoCountries() {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/GetGeoCountries', { withCredentials: true });
    };

    getChannelSegments() {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/GetChannelSegments', { withCredentials: true });
    };

    getGeoTreeData() {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/GetGeoTreeData', { withCredentials: true });
    };

    getChannelTreeData() {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/GetChannelTreeData', { withCredentials: true });
    }

    getFSAFiscalCalendarInfo_BeginDate() {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/GetFSAFiscalCalendarInfo_BeginDate', { withCredentials: true });
    }

    getFSAFiscalCalendarInfo_EndDate() {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/GetFSAFiscalCalendarInfo_EndDate', { withCredentials: true });
    }

    getProductTreeData(strString: any) {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/GetProductTreeData?strString=' + strString, { withCredentials: true });
    };
    getProductFHCTreeData(strString: any) {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/GetProductFHCTreeData?strString=' + strString, { withCredentials: true });
    };

    retrieveRoutingDetailsByRoutingId(routingID: any) {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/retrieveRoutingDetailsByRoutingId?routingID=' + routingID, { withCredentials: true });
    };

    DeleteRoutingDetailsAndHeaders(singleDeleteFlag: any, id: any) {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/DeleteRoutingDetailsAndHeaders?singleDeleteFlag=' + singleDeleteFlag +'&id=' + id, { withCredentials: true });
    };

    retrieveRoutingDetails(strRetriveInputParams: FulFillmentSearchInput) {
        let url = this.apiUrl + '' + 'FullFillmentSiteManagement/retrieveRoutingDetails';
        return this._http.post(url, strRetriveInputParams, this.options).map(this.extractData);
    }

    InsertValidateEffectiveDates(_FullFillmentRuleData: FullFillmentRuleData ) {
        let url = this.apiUrl + '' + 'FullFillmentSiteManagement/InsertValidateEffectiveDates';
        return this._http.post(url, _FullFillmentRuleData, this.options).map(this.extractData);
    }

    AddRoutingdetails(_FullFillmentRuleData: FullFillmentRuleData): Observable<any> {
        let url = this.apiUrl + '' + 'FullFillmentSiteManagement/AddRoutingdetails';
        return this._http.post(url, _FullFillmentRuleData, this.options).map(this.extractData);
    }

    AddUpdateRoutingRule_EditSection(_fulfillmentAddUpdateParams: FulfillmentAddUpdateParams) {
        let url = this.apiUrl + '' + 'FullFillmentSiteManagement/AddUpdateRoutingRule_EditSection';
        return this._http.post(url, _fulfillmentAddUpdateParams, this.options).map(this.extractData);
    }

    AddFullFillmentsites(_FullFillmentRuleData: FullFillmentRuleData) {
        let url = this.apiUrl + '' + 'FullFillmentSiteManagement/AddFullFillmentsites';
        return this._http.post(url, _FullFillmentRuleData, this.options).map(this.extractData);
    }

    UpdateValidateEffectiveDates(lstUpdateValidateParams:UpdateValidateDateInputParams) {
        let url = this.apiUrl + '' + 'FullFillmentSiteManagement/UpdateValidateEffectiveDates';
        return this._http.post(url, lstUpdateValidateParams, this.options).map(this.extractData);
    }

    updatefulfillment(updateFulfillment: FullFillmentRuleData) {
        let url = this.apiUrl + '' + 'FullFillmentSiteManagement/updatefulfillment';
        return this._http.post(url, updateFulfillment, this.options).map(this.extractData);
    }

    DeleteFulfillDetails(lstFulfillDeleteSite: FullFillmentRuleData) {
        let url = this.apiUrl + '' + 'FullFillmentSiteManagement/DeleteFulfillDetails';
        return this._http.post(url, lstFulfillDeleteSite, this.options).map(this.extractData);
    }

    getProductFGATreeData(strString: any) {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/GetProductFGATreeData?strString=' + strString, { withCredentials: true });
    };

    getRegionName(country: any) {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/getRegionName?country=' + country, { withCredentials: true });
    };

    GetNonVirtualSites(ddlMainSiteInputParams: MainSiteDDLInputParams) {
        let url = this.apiUrl + '' + 'FullFillmentSiteManagement/GetNonVirtualSites';
        return this._http.post(url, ddlMainSiteInputParams, this.options).map(this.extractData);
    }

    IsVirtualSite(siteName: any) {
        return this._http.get(this.apiUrl + 'FullFillmentSiteManagement/IsVirtualSite?siteName=' + siteName, { withCredentials: true });
    };
    
    GetFulfillmentSearchResult(strSearchInput: FulFillmentSearchInput) {
        let url = this.apiUrl + '' + 'FullFillmentSiteManagement/GetFulfillmentSearchResult';
        return this._http.post(url, strSearchInput, this.options).map(this.extractData);
    }

    DeleteRoutingDetails_fulfillment(deleteParams: FulfillmentDelete) {
        let url = this.apiUrl + '' + 'FullFillmentSiteManagement/DeleteRoutingDetails_fulfillment' 
        return this._http.post(url, deleteParams, this.options).map(this.extractData);
    }

    //deleteRoutingDetailsHeader(deleteParams: any) {
    //    let url = this.apiUrl + '' + 'FullFillmentSiteManagement/DeleteRoutingDetailsHeader';
    //    return this._http.post(url, deleteParams, this.options).map(this.extractData);
    //}


    GetFulfillmentRetrive(strRetriveInput: FulFillmentSearchInput) {
        let url = this.apiUrl + '' + 'FullFillmentSiteManagement/GetFulfillmentRetrive';
        return this._http.post(url, strRetriveInput, this.options).map(this.extractData);
    }

    public exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'FulfillmentData': worksheet }, SheetNames: ['FulfillmentData'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

        private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_' + this._utilitiesService.getDatetime() + EXCEL_EXTENSION);
    }
}