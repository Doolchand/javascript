import { Injectable } from '@angular/core';
import { Response, RequestMethod, Http, Headers, RequestOptions, URLSearchParams, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { UtilitiesService} from './../services/utilities.service';
import 'rxjs/add/operator/map';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class UserMaintenanceService {

    localURL: string = environment.URL;

    headers: Headers;
    options: RequestOptions;

    constructor(private http: Http, private _utilitiesService: UtilitiesService) {
        this.headers = new Headers({
            'Content-Type': 'application/json', 'Cache-Control': 'no-cache', 'Pragma': 'no-cache',
            'Accept': 'q=0.8;application/json;q=0.9'
        });
        this.options = new RequestOptions({ headers: this.headers, withCredentials: true });
    }

    GetRoleList(userType :string) {
        return this.http.get(this.localURL + 'Authorization/GetRoleList?userType=' + userType, { withCredentials: true });
    }

    GetTenantList() {
        return this.http.get(this.localURL + 'Authorization/GetUserType', { withCredentials: true });
    }

    GetLOBList() {
        return this.http.get(this.localURL + 'Authorization/GetLOB', { withCredentials: true });
    }
    

    GetUserGroupList()
    {
        return this.http.get(this.localURL + 'Authorization/GetUserGroupList', { withCredentials: true });
    }

    GetRegionList(userId : string) {
        return this.http.get(this.localURL + 'Authorization/GetLoggedInUserRegionBasedOnRole?UserId=' + userId, { withCredentials: true });
    }

    GetUserNameList(tenant: string, region:any) {
        return this.http.post(this.localURL + 'Authorization/GetUserNameList?tenant=' + tenant, region,this.options);
    }

    //Will return Role Id , Role Name and User Type
    GetUserRoles() {
        return this.http.get(this.localURL + 'Authorization/GetUserRoles', { withCredentials: true });
    }

    //Return all screens ID , Name and Role having write access
    GetScreenAuthorizationList() {
        return this.http.get(this.localURL + 'Authorization/GetScreenAuthorizationList', { withCredentials: true });
    }

    GetUserDetails(param: any): Observable<any> {
        let url = this.localURL + 'Authorization/GetUserDetails'
        return this.http
            .post(url, param, this.options)
            .map(this.extractData);
    }


    AddUsers(param: any): Observable<any> {
        let url = this.localURL + 'Authorization/AddUsers'
        return this.http
            .post(url, param, this.options)
            .map(this.extractData);
    }

    UpdateUsers(param: any): Observable<any> {
        let url = this.localURL + 'Authorization/UpdateUsers'
        return this.http
            .post(url, param, this.options)
            .map(this.extractData);
    }


    ValidateUserName(name: string) {
        return this.http.get(this.localURL + 'Authorization/GetUsersLdapDetails?name=' + name, { withCredentials: true });
    }

    ExcelExport(json: any[]) {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, "UserMaintenance");
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_' + this._utilitiesService.getDatetime() + EXCEL_EXTENSION);
    } 

      getUppHeaderList(){
        return true;
      }

    //Helper methods
    private extractData(res: Response) {
        return res;
    }

    // Screen wise roles having page access
    GetRolesCanAccessScreenList() {
        return this.http.get(this.localURL + 'Authorization/GetRolesCanAccessScreenList', { withCredentials: true });
    }
  
}

