﻿import { Injectable } from '@angular/core';
import { Response, Headers, Http, RequestOptions, ResponseContentType } from "@angular/http";
import { environment } from '../../environments/environment';
import { UtilitiesService } from './../services/utilities.service';
import { TreeviewItem, TreeviewConfig } from "ngx-treeview";


// Excel Functions
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Observable } from 'rxjs';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';


@Injectable()

export class demandsplitService {
    apiUrl: string = environment.URL;
    headers: Headers;
    options: RequestOptions;


    constructor(private _http: Http, private _utilitiesService: UtilitiesService) {
        this.headers = new Headers({
            'Content-Type': 'application/json', 'Cache-Control': 'no-cache', 'Pragma': 'no-cache',
            'Accept': 'q=0.8;application/json;q=0.9'
        });
        this.options = new RequestOptions({ headers: this.headers, withCredentials: true });
    }

    //GetEmcFilterViewResult(SearchView:any) {
    //     
    //    let url = this.apiUrl + '' + 'EmcProductHierarchyMaintenance/GetEmcFilterViewResult?levelId=' + levelId;
    //    return this._http.post(url, memeberId, this.options).map(this.extractData);
    //}

    private extractData(res: Response) {
        return res;
    }

    GetDemandSplitViewResult(SearchView: any) {
       
        let url = this.apiUrl + 'DemandSplit/GetDemandSplitViewResult';
        return this._http.post(url, SearchView, this.options).map(this.extractData);
    }


    getProduct() {
        // Make the HTTP request:
        return this._http.get(this.apiUrl + 'DemandSplit/getProduct', { withCredentials: true }).map((data: any) => data.json());
    };



    getChennal() {
        // Make the HTTP request:
        return this._http.get(this.apiUrl + 'DemandSplit/getChennal', { withCredentials: true }).map((data: any) => data.json());
    };

    getGeography() {
        // Make the HTTP request:
        return this._http.get(this.apiUrl + 'DemandSplit/getGeography', { withCredentials: true }).map((data: any) => data.json());
    };

    public exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_' + this._utilitiesService.getDatetime() + EXCEL_EXTENSION);
    }



    ExcelExport(json: any[]) {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, "UnspecifiedDemandSplit");
    }




    importDemandDetails(param): Observable<any> {
       
        var methodType = "IMPORT";
        var flag = "I";
        let url = this.apiUrl + 'DemandSplit/uploadDemandData';
        return this._http.post(url, param, this.options).map(this.extractData);
    }

}