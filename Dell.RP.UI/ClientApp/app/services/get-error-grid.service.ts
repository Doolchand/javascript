import { Injectable } from '@angular/core';
import { environment} from '../../environments/environment';
import { Response, RequestMethod, Http, Headers, RequestOptions, URLSearchParams, ResponseContentType } from '@angular/http';

@Injectable()
export class GetErrorGridService {
  envURL: string = environment.URL;
  constructor(private http: Http) { }

  getErrorList(param) {
      let url = this.envURL + 'ErrorLogging/GetErrorList?procName=' + param;
      return this.http.get(url, { withCredentials: true });
  }

}
