﻿import { Data } from "@angular/router";

export class SourcingRuleLineItem {
    isNewRule: boolean;
    isNewKit: boolean;
    isNewBox: boolean;
    isNewMerge: boolean;
    boxSite: string;
    boxSplit: number;
    channel: string;
    channelId: string;
    channelName: string;
    fromDate: Date;
    fromDateCalId: string;
    fromDateStr: string;
    kitSite: string;
    mergeSite: string;
    mergeSplit: number;
    parentRouteID: string;
    product: string;
    productID: string;
    productName: string;
    routeID: string;
    ruleIndex: string;
    sourcingRuleKey: string;
    sysLastModifiedby: string;
    sysLastModifiedDate: string;
    toDate: Date;
    toDateCalId: string;
    toDateStr: string;
}

