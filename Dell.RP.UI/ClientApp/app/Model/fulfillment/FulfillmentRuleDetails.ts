export class FulfillmentRuleDetails {
    fulfillmentSiteId: string;
    effectiveBeginDate: string;
    effectiveEndDate: string;
    beginDateValue: string;
    endDateValue: string;
    hiddenRoutingId: string;
    hiddlenFlag: string;
}
