export class UpdateValidateDateInputParams {
    routing: string;
    strEffectiveBeginDate: string;
    strEffectiveEndDate: string;
}
