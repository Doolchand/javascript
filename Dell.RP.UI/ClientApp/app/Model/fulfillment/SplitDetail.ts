export class SplitDetail {
    id: string;
    site: string;
    splitPercentage: string;
    mode: string;
}
