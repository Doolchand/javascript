import { DropDown } from '../fulfillment/DropDown';
export class MainSiteDDLInputParams {
    selectedCountry: DropDown;  
    selectedGeoTreeNodeValue: string;
    selectedGeography: DropDown; 
}
