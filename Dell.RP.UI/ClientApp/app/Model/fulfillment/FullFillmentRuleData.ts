export class FullFillmentRuleData {
    Geography: string;
    Channel: string;
    SupplyChainType: string;
    AllSites: string[];
    product_id: string;
    item1: string;
    Routing: string;
    Split: string;
    sites: string;
    ship_via: string;
    sys_source: string;
    sys_created_by: string;
    sys_creation_date: string;
    last_modified_by: string;
    last_modified_date: string;
    sys_Ent_State: string;
    producttype: string;
    geoLevel: string;
    channelLevel: string;
    accountName: string;
    accountId: string;
    effectiveBeginDate: string;
    effectiveEndDate: string;
}
