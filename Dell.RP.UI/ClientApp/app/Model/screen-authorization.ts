export class ScreenAuthorization {
    functionId: string;
    functionName: string;
    roleId: string;
}
