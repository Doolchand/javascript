﻿export class SourcingRuleGridCreate {
    channel: string;
    product: string;

    fromDateStr: string; 
    romDateStrVisible: boolean;

    toDateStr: string;
    toDateStrVisible: boolean;

    kitSite: string;
    kitSiteVisible: boolean;

    boxSite: string;
    boxSiteVisible: boolean;

    boxSplit: string;
    boxSplitVisible: boolean;

    mergeSite: string;
    mergeSiteVisible: boolean;

    mergeSplit: string;
    mergeSplitVisible: boolean;   

    sourcingRuleKey: string;
    sysLastModifiedby: string;
    sysLastModifiedDate: string;

}

