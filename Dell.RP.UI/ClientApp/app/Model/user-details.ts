export class UserDetails {
    userId: string;
    userType: string;
    lobs: string;
    userName: string;
    userDomain: string;
    region: string;
    userEmail: string;
    userRole: string;
    userGroup: string;
    isActive: boolean;
    updatedBy: string;
    updatedDate: string;



    //region: string;
    //userName: string;
    //userGroup: string;
    //userType: string;
    //isActive: boolean;
}
