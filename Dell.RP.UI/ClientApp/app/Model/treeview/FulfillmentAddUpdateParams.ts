import { FulfillmentRuleDetails } from '../fulfillment/FulfillmentRuleDetails';
import { FulFillmentSearchInput } from '../treeview/FulFillmentSearchInput';
export class FulfillmentAddUpdateParams {
    objAddUpdateParams: FulFillmentSearchInput;
    objAddUpdateRuleData: FulfillmentRuleDetails[]; 
    objselectedRow: FulfillmentRuleDetails;
    fulfillUpdateMode: boolean;
    strIntermRoutingID: string;
    typeValue: string;
    longtick: string;
    sysSource: string;
    strUserName: string;
    strAccountName: string;
    strSysEnt: string; 
}