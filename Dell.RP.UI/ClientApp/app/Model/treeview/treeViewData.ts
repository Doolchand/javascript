import { node } from '../treeview/node';

export class treeViewData {
    header: string;
    selectedDPValue: string;
    msgNote: string;
    isSearchEnable: Boolean;
    nodesData: node[];
    treeViewType: string;
    headerMsg: string;
}

