export class FulFillmentSearchInput {
    Channelid: string;
    Geoid: string;
    GeoType: string;
    ChannelType: string;
    Lookupproduct: string;
    StrProductId: string;
    Item: string;
    ItemType: string;
    Ssc: string;
    ShipVia: string;
    UseShipVia: boolean;
    StrsearcAccountID: string;
    effectiveStartDate: string;
    effectiveEndDate: string;
}
