import { children } from '../treeview/children';
export class node {
    id: string;
    name: string;
    level: string;
    children?: children[];
}
