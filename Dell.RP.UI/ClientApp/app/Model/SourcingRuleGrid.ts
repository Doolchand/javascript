﻿export class SourcingRuleGrid {
    isNewRule: boolean;
    isNewKit: boolean;
    isNewBox: boolean;
    isNewMerge: boolean;

    channel: string;
    channelId: string;
    channelName: string;
    channelVisible: boolean;

    product: string;
    productID: string;
    productName: string;
    productVisible: boolean;

    fromDate: string;
    fromDateCalId:string ;
    fromDateStr: string ;
    fromDateVisible: boolean;

    toDate: Date;
    toDateCalId:string ;
    toDateStr:string ;
    toDateVisible: boolean;

    kitSite: string;
    kitSiteVisible: boolean;

    boxSite: string;
    boxSiteVisible: boolean;

    boxSplit: number;
    boxSplitVisible: boolean;

    mergeSite: string;
    mergeSiteVisible: boolean;

    mergeSplit: number;
    mergeSplitVisible: boolean;

    parentRouteID: string;
    routeID: string;

    ruleIndex: number;
    sourcingRuleKey: string;
    sysLastModifiedby: string;
    sysLastModifiedDate: string;

    editButtonVisible: boolean;
    deleteButtonVisible: boolean;
    copyButtonVisible: boolean
}

