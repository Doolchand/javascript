﻿import { Component, AfterViewInit, ViewChild, ElementRef } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
@Component({
    selector: 'checkbox-cell',
    template: `<input *ngIf="isHavingWRAccess" type="checkbox" [checked]="params.value" (change)="onChange($event)">
                <div *ngIf="!isHavingWRAccess" class="checkboxText">{{params.value==true?"Yes":"No"}}</div>`,
})

export class CheckboxRenderer implements ICellRendererAngularComp {

    @ViewChild('.checkbox') checkbox: ElementRef;

    public params: any;
    private multiTier: boolean = false;
    private isHavingWRAccess = false;

    agInit(params: any): void {
        this.params = params;
        
        if (params.colDef.field == "multiTier") {
            if (params.context.componentParent.isUserHavingWriteAccess) {
                this.multiTier = true;
                this.isHavingWRAccess = true;
            } else {
                this.multiTier = true;
                this.isHavingWRAccess = false;
            }

        }

    }

    constructor() { }

    refresh(): boolean {
        return false;
    }

    public onChange(event: any) {

        this.params.data[this.params.colDef.field] = event.currentTarget.checked;
        this.params.context.componentParent.gridBtnClear = true;
        let nodeData = this.params.data;
        let selectedRow =
        {
            itemId: nodeData.itemId,
            commodityCode: nodeData.commodityCode,
            itemDescription: nodeData.itemDescription,
            multiTier: nodeData.multiTier,
            updatedBy: nodeData.updatedBy,
            updatedDate: nodeData.updatedDate
        };
        let index = this.params.context.componentParent.changedRows.findIndex(x => x.itemId == selectedRow.itemId);
        if (index == -1) {
            this.params.context.componentParent.changedRows.push(selectedRow);
        } else {
            this.params.context.componentParent.changedRows.splice(index, 1);
        }

    }
}