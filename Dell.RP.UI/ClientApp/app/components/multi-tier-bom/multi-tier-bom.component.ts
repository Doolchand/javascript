﻿import { Component, OnInit } from '@angular/core';
import { GridOptions } from 'ag-grid';

import { MultiTierBOMService } from '../../services/multi-tier-bom.service';
import { CheckboxRenderer } from './checkbox-renderer.component';
import { UserRoles } from '../../Model/user-roles';
import { SessionStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

@Component({
    selector: 'app-multi-tier-bom',
    templateUrl: './multi-tier-bom.component.html',
    styleUrls: ['./multi-tier-bom.component.css']
    //encapsulation: ViewEncapsulation.None
})
export class MultiTierBOM implements OnInit {

    maxRecords = 10000;
    colorCode: string;
    successMsg: string = "";
    public hideShowAlertMsg = false;
    showGrid: boolean = false;

    //Filter section
    IsFilterViewResultBtnDisable = false;
    IsFilterClearBtnDisable = false;
    itemIdInput = "";
    commodity_code = [];
    selected_items = [];
    dropdownSettings = {};
    itemDescInput = "";
    isChanged = false;

    //Multi Tier BOM grid
    private gridOptionsName: GridOptions;
    private contextName;
    private componentsName;
    rowDataName;
    private gridApiName;
    private frameworkComponentsName;
    private gridColumnApiName;
    getGridDataParam: multiTierBom;
    changedRows: any[] = [];
    gridBtnClear = false;

    private pageSize = 20;
    gridData = [];
    startIndex = 0;
    private exportClicked = false;

    //Roles
    userRoles: UserRoles[];
    screenId = "3002";
    isUserHavingWriteAccess: boolean = false;
    isActiveScreen: boolean = true;
    userHavingWriteAccess: string[];
    listOfRoleHavingAccess: string[];
    userTenantTypeAccessList: string[];
    isUserHavingUserMaintenance: boolean = false;

    constructor(private _multiTierService: MultiTierBOMService, private _router: Router, private _sessionSt: SessionStorageService) {
        this.Authorization();
        this.gridOptionsName = <GridOptions>{
            contextName: {
                componentParentName: this
            },
        };
        this.gridOptionsName.columnDefs = [
            //{
            //    headerName: "",
            //    width: 35,
            //    pinned: "left",
            //    suppressSizeToFit: true,
            //    suppressFilter: true,
            //    headerCheckboxSelectionFilteredOnly: true,
            //    editable: false,
            //    checkboxSelection: function (params) {
            //        if (params.columnApi.getRowGroupColumns().length === 0) {
            //            params.context.componentParent.changedRows = [];
            //        }
            //        return params.columnApi.getRowGroupColumns().length === 0;
            //    },
            //    headerCheckboxSelection: function (params) {
            //        return params.columnApi.getRowGroupColumns().length === 0;
            //    }
            //},
            {
                headerName: "ITEM ID",
                field: "itemId",
                suppressSizeToFit: false,
                tooltipField: "lob_Id",
                editable: false,
                colId: "itemId"
            },
            {
                headerName: "ITEM DESC",
                field: "itemDescription",
                suppressSizeToFit: false,
                tooltipField: "lob_Desc",
                editable: false,
                colId: "itemDescription"

            },
            {
                headerName: "COMMODITY CODE",
                field: "commodityCode",
                suppressSizeToFit: false,
                //width: 180,
                tooltipField: "brand_Id",
                editable: false,
                colId: "commodityCode"

            },
            {
                headerName: "MULTI TIER",
                field: "multiTier",
                suppressSizeToFit: false,
                tooltipField: "multiTier",
                editable: false,
                cellClass: "checkBox",
                cellRenderer: "checkboxRenderer",
                onCellClicked: this.oncellEditingStarted
            },
            {
                headerName: "LAST MODIFIED BY",
                field: "updatedBy",
                suppressSizeToFit: false,
                tooltipField: "updatedBy",
                editable: false,

            },
            {
                headerName: "LAST MODIFIED DATE",
                field: "updatedDate",
                suppressSizeToFit: false,
                tooltipField: "updatedDate",
                editable: false,

            },

        ];
        this.gridOptionsName.headerHeight = 18;
        this.gridOptionsName.rowSelection = "multiple";
        this.gridOptionsName.paginationPageSize = 20;
        this.gridOptionsName.defaultColDef = {
            editable: true,
            enableValue: true
        };
        this.gridOptionsName.rowData = [];
        this.contextName = { componentParent: this };
        this.componentsName = {};
        this.frameworkComponentsName = {
            checkboxRenderer: CheckboxRenderer,
        };
    }


    ngOnInit(): void {

        this.dropdownSettings = {
            singleSelection: true,
            text: "ALL",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class singleSelect"
        };

        this.selected_items[0] = { id: "ALL", itemName: "ALL" };
        this.getCommodityCode();
        //this.getGridDataParam = {
        //    itemId: "",
        //    commodityCode: "",
        //    itemDescription: "",
        //    multiTier: false,
        //    updatedBy: null,
        //    updatedDate: null
        //};
        //this.getGridData({
        //    itemId: "",
        //    commodityCode: "",
        //    itemDescription: "",
        //    multiTier: false,
        //    updatedBy: null,
        //    updatedDate: null
        //});
        this.ClearMessage();
    }

    Authorization() {
        var rolesHavingWriteAccess = this._sessionSt.retrieve('screenAuthorization') != null ? this._sessionSt.retrieve('screenAuthorization') : [];
        this.userHavingWriteAccess = rolesHavingWriteAccess[rolesHavingWriteAccess.findIndex(x => x.functionId == this.screenId)]["roleId"].split(',');
        this.userTenantTypeAccessList = this._sessionSt.retrieve('userTenantTypeAccessList') != null ? this._sessionSt.retrieve('userTenantTypeAccessList') : [];

        if (this._sessionSt.retrieve('userRoles') === null) {
            this.isActiveScreen = false;
            this._router.navigateByUrl('/errorMessage');
            return;
        }
        else {
            this.userRoles = this._sessionSt.retrieve('userRoles');
            for (var i = 0; i < this.userRoles.length; i++) {
                if (this.userHavingWriteAccess.includes(this.userRoles[i].roleId.toString())) {
                    this.isUserHavingWriteAccess = true;
                    return;
                }
            }

            if (!(this.isUserHavingWriteAccess || this.userTenantTypeAccessList.includes("GMP"))) {
                this.isActiveScreen = false;
                this._router.navigateByUrl('/errorMessage');
                return;
            }
        }
    }

    //get data for commodity code dropdown
    getCommodityCode() {
        this.ClearMessage();
        this.commodity_code = [];
        //this.IsFilterViewResultBtnDisable = true;
        this._multiTierService.GetCommodityCodeDDL()
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {                   
                    for (var i = 0; i < data.length; i++) {
                        this.commodity_code.push({
                            id: data[i].id,
                            itemName: data[i].itemName
                        })
                    }
                   // this.selected_items = this.commodity_code[0];
                },
                err => console.log(err),
            );
    }
    onItemSelect(item: any) {
        console.log(item);
        console.log(this.selected_items);
    }
    OnItemDeSelect(item: any) {
        console.log(item);
        console.log(this.selected_items);
    }
    onSelectAll(items: any) {
        console.log(items);
    }
    onDeSelectAll(items: any) {
        console.log(items);
    }


    GetFilterViewResult(events: any) {
        
        this.ClearMessage();
        console.log(this.itemIdInput);
        console.log(this.itemDescInput);
        console.log(this.commodity_code);
        this.getGridDataParam = {
            itemId: this.itemIdInput.toUpperCase(),
            commodityCode: this.selected_items[0] == null ? "" : this.selected_items[0].itemName,
            itemDescription: this.itemDescInput.toUpperCase(),
            multiTier: false,
            updatedBy: null,
            updatedDate: null
        };

        this.getGridData(this.getGridDataParam);

    }

    getGridData(filter: any) {
        let startIndexS = 0;
        let endIndex = startIndexS + this.pageSize;
        this._multiTierService.GetFilterViewResult(filter, startIndexS, endIndex)
            .map((data: any) => data.json()).subscribe((data: any) => {
                if (data != null && data.length > 0) {
                    if (data.length > this.maxRecords) {
                        if (!this.exportClicked == true) {
                            var alertMsg = "Only " + this.maxRecords + " records are shown in the grid";
                            window.alert(alertMsg);
                        }
                    }
                    this.rowDataName = data.slice(0, this.maxRecords);
                    this.showGrid = true;
                    //if (this.exportClicked == true) {
                    //    this.exportClicked = false;
                    //    this.GenerateExcelName(this.rowDataName.filter(x => x.multiTier == true));
                    //}
                } else {
                    console.log("no data to display");
                    this.DisplayMessage("No records found", false);
                    this.rowDataName = [];
                }
            },
                err => console.log(err))
    }

    //getGridData(filter: any) {
    //    this.rowDataName = [];
    //    this._multiTierService.GetFilterViewResultCount(filter).subscribe((record_count: any) => {
    //        let arr = new Array();
    //        const RECORDS_TO_GET = 50000;
    //        record_count = Number(record_count._body);
    //        let mod = record_count % RECORDS_TO_GET;
    //        let numberOfIters = Math.floor(record_count / RECORDS_TO_GET);

    //        let multiTierViewResultArray = new Array();
    //        if (numberOfIters == 0) {
    //            multiTierViewResultArray[0] = this._multiTierService.GetFilterViewResult(filter, 0 , mod);
    //        } else {
    //            let start = 0;
    //            let i = 0;
    //            for (i = 0; i < numberOfIters; i++) {
    //                multiTierViewResultArray[i] = this._multiTierService.GetFilterViewResult(filter, start, RECORDS_TO_GET);
    //                start += RECORDS_TO_GET;
    //            }
    //            multiTierViewResultArray[i] = this._multiTierService.GetFilterViewResult(filter, start, mod);
    //        }
    //        forkJoin([...multiTierViewResultArray]).subscribe(list => {
    //            this.populateData(list);
    //        })
    //    })
    //    //this._multiTierService.GetFilterViewResult(filter)
    //    //    .map((data: any) => data.json()).subscribe((data: any) => {
    //    //        if (data.length > 0) {
    //    //            this.rowDataName = data;
    //    //        } else {
    //    //            console.log("No data to display");
    //    //        }
    //    //    },
    //    //        err => console.log(err))
    //}


    //populateData(list: any) {
    //    if (list != null) {
    //        let data = new Array();
    //        for (let i = 0; i < list.length; i++) {
    //            data = data.concat(JSON.parse(list[i]._body));
    //        }
    //        this.rowDataName = data;
    //    } else {
    //        this.hideShowAlertMsg = true;
    //    }
    //}
    oncellEditingStarted(event: any) {
        
        //event.node.setSelected(true);
        console.log('checkbox cell clicked!');
        //event.context.componentParent.gridBtnClear = true;

        //let nodeData = event.node.data;
        //let selectedRow =
        //{
        //    itemId: nodeData.itemId,
        //    commodityCode: nodeData.commodityCode,
        //    itemDescription: nodeData.itemDescription,
        //    multiTier: nodeData.multiTier,
        //    updatedBy: nodeData.updatedBy,
        //    updatedDate: nodeData.updatedDate
        //};
        //let index = event.context.componentParent.changedRows.findIndex(x => x.itemId == selectedRow.itemId);
        //if (index == -1) {
        //    event.context.componentParent.changedRows.push(selectedRow);
        //    this.isChanged = true;

        //} else {
        //    event.context.componentParent.changedRows.splice(index, 1);
        //    if (event.context.componentParent.changedRows.length == 0) {
        //        this.isChanged = false;
        //    } else {
        //        this.isChanged = true;
        //    }
        //}
    }

    onGridReadyName(params) {
        this.gridApiName = params.api;
        this.gridColumnApiName = params.columnApi;
        this.gridOptionsName.api.sizeColumnsToFit();

    }

    onSelectionChanged(event: any) {
        let rowsSelection = this.gridOptionsName.api.getSelectedRows();
        if (rowsSelection.length > 0) {
            console.log(rowsSelection.values);
        }
        else {

        }
    }

    onClickBtnExcelExport(event: any) {
        //if (this.rowDataName === undefined) {
        //    this.DisplayMessage("Data in grid is blank", true);
        //    this.colorCode = "danger";
        //}
        //else {
        //    this.exportClicked = true;
        //    this.getGridData(this.getGridDataParam);
        //    //this.GenerateExcelName(this.rowDataName.filter(x => x.multiTier == true));
        //}

        //this.getGridDataParam = {
        //    itemId: this.itemIdInput.toUpperCase(),
        //    commodityCode: this.selected_items[0] == null ? "" : this.selected_items[0].itemName,
        //    itemDescription: this.itemDescInput.toUpperCase(),
        //    multiTier: false,
        //    updatedBy: null,
        //    updatedDate: null
        //};
        this._multiTierService.GetMultiTierViewResultExcel(this.getGridDataParam)
            .map((data: any) => data.json()).subscribe((data: any) => {
                console.log(data);
                if (data.length > 0) {
                    this.exportClicked = true;
                    this.GenerateExcelName(data);
                } else {
                    this.DisplayMessage("Data in grid is blank", false);
                }
            },
                err => console.log(err))  

    }

    onClickBtnSave(event: any) {
        var alertMsg = "Are you sure, you want to save?";
        if (window.confirm(alertMsg)) {
            this.ClearMessage();
            this.gridApiName.stopEditing();
            if (this.changedRows.length>0) {
                let rowsSelection = JSON.parse(JSON.stringify(this.changedRows));

                this._multiTierService.UpdateMultiTierBOMDetails(rowsSelection)
                    .subscribe(
                        (data: any) => {
                            var returnCode = data._body.split('~')[0];
                            var returnMsg = data._body.split('~')[1];
                            this.hideShowAlertMsg = true;

                            if (returnMsg == "SUCCESS") {
                                this.DisplayMessage("Records successfully Updated.", true);
                                this.gridOptionsName.api.deselectAllFiltered();
                                this.gridOptionsName.api.deselectAll();
                                this.getGridData(this.getGridDataParam);
                            }
                            else {
                                this.DisplayMessage(returnMsg, false);

                            }
                        },
                        err => console.log(err), // error
                    );
            }
            this.changedRows = [];
        }
    }

    GenerateExcelName(rowData: any[]) {

        this.ClearMessage();
        let param = [];
        for (var i = 0; i < rowData.length; i++) {
            param.push({
                ["Item ID"]: rowData[i].itemId,
                ["Item Description"]: rowData[i].itemDescription,
                ["Commodity Code"]: rowData[i].commodityCode,
                ["Multi Tier"]: rowData[i].multiTier ? "Y" : "N",
                ["Updated By"]: rowData[i].updatedBy,
                ["Updated Date"]: rowData[i].updatedDate
            })
        }
        this._multiTierService.MultiTierExcelExport(param);
    }

    //clear filters
    clearFilterData() {
        this.ClearMessage();
        //this.commodity_code = [];
        this.selected_items = [];
        this.itemIdInput = "";
        this.itemDescInput = "";
    }

    //clear error/success message
    ClearMessage() {
        this.hideShowAlertMsg = false;
        this.successMsg = "";
    }

    DisplayMessage(msg, isSuccess) {
        this.hideShowAlertMsg = true;
        this.successMsg = msg;
        this.colorCode = isSuccess == true ? "success" : "danger";
    }

    clearGridDataName(event: any) {
        this.gridBtnClear = false;
        this.getGridData(this.getGridDataParam);
        this.changedRows = [];
        this.ClearMessage();

    }
}

//function createNewRowMultiTier() {
//    var newRow = {
//        'itemId': "",
//        'itemDescription': "",
//        'multiTier': false,
//        'updatedBy': "",
//        'updatedDate': ""
//    };
//    return newRow;
//}

export interface multiTierBom {
    'itemId': string,
    'commodityCode': string,
    'itemDescription': string,
    'multiTier': boolean,
    'updatedBy': string,
    'updatedDate': string,
}