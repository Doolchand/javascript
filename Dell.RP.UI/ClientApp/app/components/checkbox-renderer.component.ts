import {Component, AfterViewInit, ViewChild, ElementRef} from "@angular/core";
import {ICellRendererAngularComp} from "ag-grid-angular";
@Component({
    selector: 'checkbox-cell',
    template: `<input type="checkbox" [checked]="params.value" (change)="onChange($event)">`,
    }) 
    export class CheckboxCellCommonComponent implements ICellRendererAngularComp {
    
    @ViewChild('.checkbox') checkbox: ElementRef;
    
    public params: any;

    agInit(params: any): void {
        this.params = params;
    }
    
    constructor() { }

    refresh(): boolean {
        return false;
    }
    
    public onChange(event: any) {
        this.params.data[this.params.colDef.field] = event.currentTarget.checked;
		this.params.node.setSelected(true);
    }
}