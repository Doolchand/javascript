﻿import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { SessionStorageService } from 'ngx-webstorage';
import * as XLSX from 'xlsx';
import { UserRoles } from '../../Model/user-roles';
import { FulfillmentSiteAssignmentService } from '../../services/fulfillment-siteassignment.service';
import { UtilitiesService } from '../../services/utilities.service';
import { Router, ActivatedRoute, Params, Data } from '@angular/router';
import { GetErrorGridService } from '../../services/get-error-grid.service';
//import { treeViewData, node, children, FulFillmentSearchInput, FulfillmentAddUpdateParams } from '../../Model/tree-view-data';
import { treeViewData } from '../../Model/treeview/treeViewData';
import { node } from '../../Model/treeview/node';
import { children } from '../../Model/treeview/children';
import { FulFillmentSearchInput } from '../../Model/treeview/FulFillmentSearchInput';
import { FulfillmentAddUpdateParams } from '../../Model/treeview/FulfillmentAddUpdateParams';
import { TreeViewComponent } from '../../components/tree-view/tree-view.component';
import { SplitDetail } from '../../Model/fulfillment/SplitDetail';
import { FulfillmentRuleDetails } from '../../Model/fulfillment/FulfillmentRuleDetails';
import { FullFillmentRuleData } from '../../Model/fulfillment/FullFillmentRuleData';
import { UpdateValidateDateInputParams } from '../../Model/fulfillment/UpdateValidateDateInputParams';
import { MainSiteDDLInputParams } from '../../Model/fulfillment/MainSiteDDLInputParams';
import { DropDown } from '../../Model/fulfillment/DropDown';
import { FulfillmentDeleteInputParams } from '../../Model/fulfillment/FulfillmentDeleteInputParams';
import { FulfillmentDelete } from '../../Model/fulfillment/FulfillmentDelete';

import { parse } from 'cfb/types';
import { DateFormatter } from 'ng2-bootstrap';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { Inject } from "@angular/core";
import { DOCUMENT } from '@angular/platform-browser';

declare var jQuery: any;

@Component({
    selector: 'app-fulfillment-siteassignment',
    templateUrl: './fulfillment-siteassignment.component.html',
    styleUrls: ['./fulfillment-siteassignment.component.css'],
    encapsulation: ViewEncapsulation.None,

})
export class FulfillmentSiteAssignmentComponent implements OnInit {

    @ViewChild('fakeClick') fakeClick: ElementRef;
    @ViewChild('file') FileName: any;
    @ViewChild(TreeViewComponent) treeviewChild: TreeViewComponent;

    //geo 
    geographyData = [];
    selectedGeogrphy = [];
    geogrphyDropdownSettings = {};
    isCountryDdlOpen: boolean = true;
    selectedGeogrphyTreeData: string;
    selectedGeogrphyTreeData_label: string;
    isGeographyTree: boolean = true;
    nodesGeography: node[];

    selectedCountry = [];
    countryData = [];
    countryDropdownSettings = {};

    //tree variables
    nodes: node[];
    treeViewNodesFilterdData: node[];
    treeViewDetailData: treeViewData = new treeViewData();
    options: any;


    //product 
    productData = [];
    selectedProduct = [];
    productDropdownSettings = {};
    isProductTree: boolean = true;
    selectedProductTreeData: string;
    selectedProductTreeData_label: string;
    nodesProducts: node[];

    //channel
    channelData = [];
    selectedChannel = [];
    channelDropdownSettings = {};
    isChannelTree: boolean = true;
    selectedChennelTreeData: string;
    selectedChennelTreeData_label: string;

    nodesChannel: node[];

    selectedSegment = [];
    segmentData = [];
    segmentDropdownSettings = {};
    isSegmentDdlOpen: boolean = true;


    ////Account name 
    //selectedAccountName = [];
    //accountNameData = [];
    //accountNameDropdownSettings = {};

    //sypply chain type 
    selectedSupplyChainType = [];
    supplyChainTypeData = [];
    supplyChainTypeDropdownSettings = {};

    //search 
    //strSearchInputParams: FulFillmentSearchInput;
    strSearchInputParams: FulFillmentSearchInput = new FulFillmentSearchInput();
    private rowData;
    private gridOptionsName: GridOptions;
    gridBtnExcelExport: boolean = false;
    gridBtnUpdate: boolean = false;
    gridBtnDelete: boolean = false;

    colorCode: string;
    successMsg = '';
    hideShowAlertMsg = false;

    onselectedGeogrphyTreeDataChanged(selectedTreeValue: string): void {
        this.selectedGeogrphyTreeData = selectedTreeValue;
        this.selectedGeogrphyTreeData_label = this.selectedGeogrphyTreeData.split('__')[0];
    }

    onselectedProductTreeDataChanged(selectedTreeValue: string): void {
        this.selectedProductTreeData = selectedTreeValue;
        this.selectedProductTreeData_label = this.selectedProductTreeData.split('__')[0];
    }

    onselectedChannelTreeDataChanged(selectedTreeValue: string): void {
        this.selectedChennelTreeData = selectedTreeValue;
        this.selectedChennelTreeData_label = this.selectedChennelTreeData.split('__')[0];
    }



    //Edit section - start


    onselectedGeogrphyTreeDataChanged_edit(selectedTreeValue: string): void {
        this.selectedGeogrphyTreeData_edit = selectedTreeValue;
        this.selectedGeogrphyTreeData_label_edit = this.selectedGeogrphyTreeData_edit.split('__')[0];
    }

    onselectedProductTreeDataChanged_edit(selectedTreeValue: string): void {
        this.selectedProductTreeData_edit = selectedTreeValue;
        this.selectedProductTreeData_label_edit = this.selectedProductTreeData_edit.split('__')[0];
    }

    onselectedChannelTreeDataChanged_edit(selectedTreeValue: string): void {
        this.selectedChennelTreeData_edit = selectedTreeValue;
        this.selectedChennelTreeData_label_edit = this.selectedChennelTreeData_edit.split('__')[0];
    }


    //btnClear_Edit: boolean = false;
    btnRetrive_Edit: boolean = true;


    //geo edit
    geographyData_edit = [];
    selectedGeogrphy_edit = [];
    geogrphyDropdownSettings_edit = {};
    isCountryDdlOpen_edit: boolean = true;
    selectedGeogrphyTreeData_edit: string;
    selectedGeogrphyTreeData_label_edit: string;
    isGeographyTree_edit: boolean = true;
    nodesGeography_edit: node[];
    isGeographyTreeDisable_edit: boolean = false;

    selectedCountry_edit = [];
    countryData_edit = [];
    countryDropdownSettings_edit = {};

    //tree variables
    nodes_edit: node[];
    treeViewNodesFilterdData_edit: node[];
    treeViewDetailData_edit: treeViewData = new treeViewData();
    options_edit: any;


    //product 
    productData_edit = [];
    selectedProduct_edit = [];
    productDropdownSettings_edit = {};
    isProductTree_edit: boolean = true;
    selectedProductTreeData_edit: string;
    selectedProductTreeData_label_edit: string;
    nodesProducts_edit: node[];
    isProductTreeDisable_edit: boolean = false;


    //channel
    channelData_edit = [];
    selectedChannel_edit = [];
    channelDropdownSettings_edit = {};
    isChannelTree_edit: boolean = true;
    selectedChennelTreeData_edit: string;
    selectedChennelTreeData_label_edit: string;
    lstChannelType_edit = [];
    isChannelTreeDisable_edit: boolean = false;

    nodesChannel_edit: node[];

    selectedSegment_edit = [];
    segmentData_edit = [];
    segmentDropdownSettings_edit = {};
    isSegmentDdlOpen_edit: boolean = true;

    //sypply chain type 
    selectedSupplyChainType_edit = [];
    supplyChainTypeData_edit = [];
    supplyChainTypeDropdownSettings_edit = {};


    isMainFulfillment: boolean = true;
    isFulfillmentTempGrid: boolean = true;
    isFulfillmentRuleGrid: boolean = true;
    isddlGeography_edit: boolean = false;
    isddlCountry_edit: boolean = false;
    isddlProduct_edit: boolean = false;
    isddlChannel_edit: boolean = false;
    isddlSegment_edit: boolean = false;
    isddlSupplychain_edit: boolean = false;

    selectedMainListSites_edit = [];
    mainListSitesData_edit = [];
    mainListSitesDropdownSettings_edit = {};

    selecteEffectiveBeginDate_edit = [];
    EffectiveBeginDateData_edit = [];
    EffectiveBeginDateDropdownSettings_edit = {};

    selecteEffectiveEndDate_edit = [];
    EffectiveEndDateData_edit = [];
    EffectiveEndDateDropdownSettings_edit = {};

    txtSplitPercentage: string = '';
    isCreateSaveCollapse: boolean = true;


    //grid fulfillment temp 
    public rowData_split = [];
    private gridOptionsFulfillmentSplits: GridOptions;
    mode: string = 'Add';
    private gridColumnApiFulfillmentSplit;
    private gridApiFulfillmentSplit;
    splitTotal: number = 0;
    rowSelection_split: string = 'single';

    //grid fulfillment rule
    private gridOptionsFulfillment_rules: GridOptions;
    public rowData_rule = [];
    rowSelection_rules: string = 'single';
    private gridColumnApiFulfillment_rule;
    private gridApiFulfillment_rule;

    isddlMainSite_edit = false;
    istxtSplit = false;
    updateTempSplitGridId = '';

    fulfillUpdateMode: boolean = false;

    routingRuleDt = [];
    //Edit section - end

    deleteParams: FulfillmentDeleteInputParams[];

    //Roles
    userRoles: UserRoles[];
    screenId = "2001";
    isUserHavingWriteAccess: boolean = false;
    isActiveScreen: boolean = true;
    userHavingWriteAccess: string[];
    listOfRoleHavingAccess: string[];
    userTenantTypeAccessList: string[];
    isUserHavingUserMaintenance: boolean = false;
    
    constructor(@Inject(DOCUMENT) private document: Document, private _datePipe: DatePipe, private _router: Router, private _fulfillmentSiteAssignmentServiceService: FulfillmentSiteAssignmentService, private _errorGridService: GetErrorGridService, private _sessionSt: SessionStorageService, private _utilitiesService: UtilitiesService) {
       this.Authorization();

        this.selectedGeogrphyTreeData = '';
        this.selectedProductTreeData = '';
        this.selectedChennelTreeData = '';

        //Edit section - start
        this.selectedGeogrphyTreeData_edit = '';
        this.selectedProductTreeData_edit = '';
        this.selectedChennelTreeData_edit = '';
        //Edit section -end 

        //grid -view - start
        this.gridOptionsName = <GridOptions>{
            contextName: {
                componentParentName: this
            },
            //rowSelection: this.rowSelection,
        };
        this.gridOptionsName.columnDefs = [
            {
                headerName: "",
                field: "id",
                width: 30,
                pinned: "left",
                tooltipField: "id",
                suppressSizeToFit: true,
                suppressFilter: true,
                hide: false,
                headerCheckboxSelectionFilteredOnly: true,
                editable: false,
                checkboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                },
                headerCheckboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                }
            },

            { headerName: "Geography", field: "geographyName", suppressSizeToFit: false, tooltipField: "Geography", editable: false, width: 190, },
            { headerName: "Product/FHC/FGA", field: "productFga", suppressSizeToFit: false, tooltipField: "Product/FHC/FGA", editable: false, width: 190, },
            { headerName: "Channel", field: "channelName", suppressSizeToFit: false, tooltipField: "Channel", editable: false, width: 190, },
            { headerName: "Effective Begin Date", field: "effectiveStartDate", suppressSizeToFit: false, tooltipField: "Effective Begin Date", editable: false, width: 190, },
            { headerName: "Effective End Date", field: "effectiveEndDate", suppressSizeToFit: false, tooltipField: "Effective End Date", editable: false, width: 190, },

            { headerName: "Fulfillment Site", field: "ps", suppressSizeToFit: false, tooltipField: "Fulfillment Site", editable: false, width: 190, },

            { headerName: "Supply Chain Type", field: "ssc", suppressSizeToFit: false, tooltipField: "Supply Chain Type", editable: false, width: 190, },
            { headerName: "Shipvia Type", field: "shipVia", suppressSizeToFit: false, tooltipField: "Shipvia Type", editable: false, width: 190, },
            { headerName: "Account ID", field: "accountId", suppressSizeToFit: false, tooltipField: "Account ID", editable: false, width: 190, },
            { headerName: "Account Name", field: "accountName", suppressSizeToFit: false, tooltipField: "Account Name", editable: false, width: 190, },
            { headerName: "Last Modified By", field: "sysLastModifiedBy", suppressSizeToFit: false, tooltipField: "Last Modified By", editable: false, width: 190, },
            { headerName: "Last Modified Date", field: "sysLastModifiedDate", suppressSizeToFit: false, tooltipField: "Last Modified Date", editable: false, width: 190, },

        ];
        this.gridOptionsName.headerHeight = 18;
        this.gridOptionsName.rowSelection = "multiple";
        this.gridOptionsName.paginationPageSize = 14;
        this.gridOptionsName.defaultColDef = {
            editable: true,
            enableValue: true
        };
        this.gridOptionsName.rowData = [];

        //grid -view - end

        //grid -fulfilment temp add update - start
        this.gridOptionsFulfillmentSplits = <GridOptions>{
            contextName: {
                componentParentName: this
            },
            rowSelection: this.rowSelection_split,

        };
        this.gridOptionsFulfillmentSplits.columnDefs = [
            {
                headerName: "",
                field: "id",
                width: 30,
                pinned: "left",
                tooltipField: "id",
                suppressSizeToFit: true,
                suppressFilter: true,
                hide: false,
                headerCheckboxSelectionFilteredOnly: true,
                editable: false,
                checkboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                },
                headerCheckboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                }
            },

            { headerName: "Site", field: "site", suppressSizeToFit: false, tooltipField: "Site", editable: false, width: 250, },
            { headerName: "Split Percentage", field: "splitPercentage", suppressSizeToFit: false, tooltipField: "Split Percentage", editable: false, width: 250, },
            { headerName: "Mode", field: "mode", suppressSizeToFit: false, hide: true, tooltipField: "Mode", editable: false, width: 168, },

        ];
        this.gridOptionsFulfillmentSplits.headerHeight = 18;
        this.gridOptionsFulfillmentSplits.rowSelection = "single";
        this.gridOptionsFulfillmentSplits.paginationPageSize = 14;
        this.gridOptionsFulfillmentSplits.defaultColDef = {
            editable: true,
            enableValue: true
        };
        this.gridOptionsFulfillmentSplits.rowData = [];

        //grid -fulfilment temp add update - start
        this.gridOptionsFulfillment_rules = <GridOptions>{
            contextName: {
                componentParentName: this
            },
            rowSelection: this.rowSelection_rules,

        };
        this.gridOptionsFulfillment_rules.columnDefs = [
            {
                headerName: "",
                field: "id",
                width: 30,
                pinned: "left",
                tooltipField: "id",
                suppressSizeToFit: true,
                suppressFilter: true,
                hide: false,
                headerCheckboxSelectionFilteredOnly: true,
                editable: false,
                checkboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                },
                headerCheckboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                }
            },

            { headerName: "Site", field: "fulfillmentSiteId", suppressSizeToFit: false, tooltipField: "Site", editable: false, width: 400, },
            { headerName: "Effective Begin Date", field: "effectiveBeginDate", suppressSizeToFit: false, tooltipField: "Effective Begin Date", editable: false, width: 360, },
            { headerName: "Effective End Date", field: "effectiveEndDate", suppressSizeToFit: false, tooltipField: "Effective End Date", editable: false, width: 360, },

            { headerName: "Begin Date Value", field: "beginDateValue", hide: true, suppressSizeToFit: false, tooltipField: "Begin Date Value", editable: false, width: 350, },
            { headerName: "End Date Value", field: "endDateValue", hide: true, suppressSizeToFit: false, tooltipField: "End Date Value", editable: false, width: 350, },
            { headerName: "Routing ID", field: "hiddenRoutingId", hide: true, suppressSizeToFit: false, tooltipField: "Routing ID", editable: false, width: 350, },
            { headerName: "Hidden Flag", field: "hiddlenFlag", hide: true, suppressSizeToFit: false, tooltipField: "Hidden Flag", editable: false, width: 350, },

        ];
        this.gridOptionsFulfillment_rules.headerHeight = 18;
        this.gridOptionsFulfillment_rules.rowSelection = "single";
        this.gridOptionsFulfillment_rules.paginationPageSize = 14;
        this.gridOptionsFulfillment_rules.defaultColDef = {
            editable: true,
            enableValue: true
        };
        this.gridOptionsFulfillment_rules.rowData = [];

        //grid -fulfilment temp add update - start

    }

    Authorization() {
        debugger;
        var rolesHavingWriteAccess = this._sessionSt.retrieve('screenAuthorization') != null ? this._sessionSt.retrieve('screenAuthorization') : [];
        this.userHavingWriteAccess = rolesHavingWriteAccess[rolesHavingWriteAccess.findIndex(x => x.functionId == this.screenId)]["roleId"].split(',');
        this.userTenantTypeAccessList = this._sessionSt.retrieve('userTenantTypeAccessList') != null ? this._sessionSt.retrieve('userTenantTypeAccessList') : [];

        if (this._sessionSt.retrieve('userRoles') === null) {
            this.isActiveScreen = false;
            this._router.navigateByUrl('/errorMessage');
            return;
        }
        else {
            this.userRoles = this._sessionSt.retrieve('userRoles');
            for (var i = 0; i < this.userRoles.length; i++) {
                debugger;
                if (this.userHavingWriteAccess.includes(this.userRoles[i].roleId.toString())) {
                    this.isUserHavingWriteAccess = true;
                    return;
                }
            }

            if (!(this.isUserHavingWriteAccess || this.userTenantTypeAccessList.includes("RP"))) {
                this.isActiveScreen = false;
                this._router.navigateByUrl('/errorMessage');
                return;
            }
        }
    }

    clearErrorMsg() {
        this.hideShowAlertMsg = false;
        this.successMsg = '';

    }


    //initilasying the dropdown members
    ngOnInit() {
        this.clearErrorMsg();
        this.geogrphyDropdownSettings = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        };

        this.countryDropdownSettings = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        };

        this.productDropdownSettings = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        };

        this.channelDropdownSettings = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        };

        this.segmentDropdownSettings = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        };

        //this.accountNameDropdownSettings = {
        //    singleSelection: true,
        //    text: "Select ...",
        //    selectAllText: 'Select All',
        //    unSelectAllText: 'UnSelect All',
        //    enableSearchFilter: true,
        //    enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
        //    classes: "myclass custom-class singleSelect"
        //};

        this.supplyChainTypeDropdownSettings = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        };

        this.getGeogrphyData();
        this.getCountryData();
        this.getProductData();
        this.getChannelData();
        this.getSegmentData();
        //this.getAccountNameData();
        this.getSupplyChainTypeData();


        this.selectedChannel[0] = { id: "ALL", itemName: "ALL" };
        this.selectedGeogrphy[0] = { id: "ALL", itemName: "ALL" };
        this.selectedProduct[0] = { id: "ALL", itemName: "ALL" };
        this.selectedSupplyChainType[0] = { id: "ALL", itemName: "ALL" };

        this.selectedCountry[0] = { id: "ALL", itemName: "ALL" };
        this.selectedSegment[0] = { id: "ALL", itemName: "ALL" };

        this.selectedChannel_edit[0] = { id: "0", itemName: "--SELECT--" };
        this.selectedGeogrphy_edit[0] = { id: "0", itemName: "--SELECT--" };
        this.selectedProduct_edit[0] = { id: "0", itemName: "--SELECT--" };
        this.selectedSupplyChainType_edit[0] = { id: "0", itemName: "--SELECT--" };
        this.selectedCountry_edit[0] = { id: "0", itemName: "--SELECT--" };
        this.selectedSegment_edit[0] = { id: "0", itemName: "--SELECT--" };

        //Edit section - start
        this.geogrphyDropdownSettings_edit = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        };

        this.countryDropdownSettings_edit = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        };

        this.productDropdownSettings_edit = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        };

        this.channelDropdownSettings_edit = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        };

        this.segmentDropdownSettings_edit = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        };

        this.supplyChainTypeDropdownSettings_edit = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        };

        this.mainListSitesDropdownSettings_edit = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        }

        this.EffectiveBeginDateDropdownSettings_edit = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        }

        this.EffectiveEndDateDropdownSettings_edit = {
            singleSelection: true,
            text: "Select ...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results',
            classes: "myclass custom-class singleSelect"
        }

        this.getGeogrphyData_edit();
        this.getCountryData_edit();
        this.getProductData_edit();
        this.getChannelData_edit();
        this.getSegmentData_edit();
        //this.getAccountNameData();
        this.getSupplyChainTypeData_edit();

        //Edit section - end

    }


    onGridReadyFulfilmentSplit(params) {
        this.gridApiFulfillmentSplit = params.api;
        this.gridColumnApiFulfillmentSplit = params.columnApi;
    }

    onGridReadyFulfilment_rules(params) {
        this.gridApiFulfillment_rule = params.api;
        this.gridColumnApiFulfillment_rule = params.columnApi;
    }

    onSelectionChanged_rules(event: any) {

    }

    onSelectionChanged(event: any) {
        let rowsSelection = this.gridOptionsName.api.getSelectedRows();
        if (rowsSelection.length > 0) {
        
            this.gridBtnUpdate = true;
            this.gridBtnDelete = true;
        }
        else {
            this.gridBtnUpdate = false;
            this.gridBtnDelete = false;
            //this.gridBtnsave = false;
            // alert("not selected");
        }
    }

    getGeoTreeDetail() {
        this.nodesGeography = [];

        this.treeViewDetailData.selectedDPValue = this.selectedGeogrphy[0].itemName;
        this.treeViewDetailData.nodesData = this.nodesGeography;
        this.treeViewDetailData.header = "Geo Tree Popup";
        this.treeViewDetailData.isSearchEnable = false;
        this.treeViewDetailData.msgNote = "";
        this.treeViewDetailData.treeViewType = "GEO";
        this.treeViewDetailData.headerMsg = "";

        this.treeviewChild.bindTreeData();
    }

    getProductTreeDetail() {
        this.nodesProducts = [];
        

        this.treeViewDetailData.selectedDPValue = this.selectedProduct[0].itemName;
        this.treeViewDetailData.nodesData = this.nodesProducts;
        //this.treeViewDetailData.header = "Product Tree Popup";
        this.treeViewDetailData.isSearchEnable = true;

        if (this.selectedProduct[0].itemName == "PRODUCT_TREE") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All PRODUCT LOB Names";
            this.treeViewDetailData.headerMsg = "Please enter PRODUCT LOB Name!";
            this.treeViewDetailData.treeViewType = "PRODUCT_TREE";
            this.treeViewDetailData.header = "Product Tree Popup";
        }
        if (this.selectedProduct[0].itemName == "FHC_TREE") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All FHC Names";
            this.treeViewDetailData.headerMsg = "Please enter FHC ID!";
            this.treeViewDetailData.treeViewType = "PRODUCT_FHC";
            this.treeViewDetailData.header = "FHC Popup";
        }
        if (this.selectedProduct[0].itemName == "FGA") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All FGA's";
            this.treeViewDetailData.headerMsg = "Please enter FGA ID!";
            this.treeViewDetailData.treeViewType = "PRODUCT_FGA";
            this.treeViewDetailData.header = "FGA Popup";
        }

        this.treeviewChild.bindTreeData();
    }


    getChannelTreeDetail() {
        this.nodesChannel = [];

        this.treeViewDetailData.selectedDPValue = this.selectedChannel[0].itemName;
        this.treeViewDetailData.nodesData = this.nodesChannel;
        this.treeViewDetailData.header = "Channel Tree Popup";
        this.treeViewDetailData.isSearchEnable = false;
        this.treeViewDetailData.msgNote = "";
        this.treeViewDetailData.headerMsg = "";
        this.treeViewDetailData.treeViewType = "CHANNEL";

        this.treeviewChild.bindTreeData();
    }

    searchFullfillmentData(mode) {
        
        
        if (mode == 'Search') {
            this.clearErrorMsg();
            if (document.getElementById('collapseHirearchy2') != null) {
                document.getElementById('collapseHirearchy2').className = 'panel-collapse collapse';
            }
        }
        

        var strFgaId: string = '';
        var productfga: string = '';
        var geoType: any = '';
        var channelType: any = '';
        var strfhcType: any = '';
        var isError: boolean = false;
        var errMsg: string = '';
        var channelid: string = '';
        var lookupproduct: string = '';
        var supplyChainId: string = '';
        var strProductId: string = '';
        var strsearcAccountID: string = '';

        var geoid: string = '';

        //geography
        debugger;
        if (this.selectedGeogrphy.length > 0) {
            if (this.selectedGeogrphy[0].itemName == 'ALL') {
                geoid = 'ALL';
                geoType = 'ALL_GEO';
            }
            else if (this.selectedGeogrphy[0].itemName == 'GEOGRAPHY_TREE') {
                if (this.selectedGeogrphyTreeData == '') {
                    isError = true;
                    errMsg += "Geography";
                }
                else {
                    var strLevel = this.selectedGeogrphyTreeData.split('__')[1];
                    var selectedGeoId = this.selectedGeogrphyTreeData.split('__')[0];

                    if (strLevel == 'ALL_GEO') {
                        geoType = 'ALL_GEO';
                    }
                    if (strLevel == '1') {
                        geoType = "REGION";
                    }
                    if (strLevel == '2') {
                        geoType = 'SUB_REGION';
                    }
                    if (strLevel == '3') {
                        geoType = 'AREA';
                    }
                    if (strLevel == '4') {
                        geoType = 'COUNTRY';
                    }

                    geoid = selectedGeoId;
                    if ((geoid != null && geoid != '') && geoid != 'ALL_GEO') {
                        geoid = geoid.substring(0, geoid.indexOf('('));
                    }
                    if (geoid == "ALL_GEO") {
                        geoid = "ALL";
                    }
                }
            }
            else if (this.selectedGeogrphy[0].itemName == 'COUNTRY') {
                if (this.selectedCountry.length > 0) {
                    geoid = this.selectedCountry[0].itemName;
                    if (geoid == 'ALL') {
                        geoid = 'ALL';
                    }
                    geoType = 'COUNTRY';
                }
                else {
                    isError = true;
                    if (errMsg == '') {
                        errMsg += 'Country';
                    }
                    else {
                        errMsg += ',Country';
                    }
                }
            }
        }
        else {
            isError = true;
            if (errMsg == '') {
                errMsg += 'Geography';
            }
            else {
                errMsg += ',Geography';
            }
        }
      
        
        //channel
        if (this.selectedChannel.length > 0) {
            channelType = this.selectedChannel[0].itemName;
            if (this.selectedChannel[0].itemName == 'ALL') {
                channelid = 'ALL';
            }
            else if (this.selectedChannel[0].itemName == "CHANNEL_TREE") {
                if (this.selectedChennelTreeData == '') {
                    isError = true;
                    if (errMsg == '') {
                        errMsg += 'Channel';
                    }
                    else {
                        errMsg += ',Channel';
                    }
                }
                else {
                    channelid = this.selectedChennelTreeData;
                    if ((channelid != null && channelid != '') && channelid != "SACHA(TOTSLS)-ALL_CHANNEL") {
                        channelid = channelid.substring(0, channelid.indexOf("("));
                    }
                    if (channelid == "SACHA(TOTSLS)-ALL_CHANNEL") {
                        channelid = "ALL";
                        channelType = "ALL";
                    }
                }
            }
            else if (this.selectedChannel[0].itemName == "SEGMENT") {
                //channelid = this.selectedSegment[0].itemName;
                if (this.selectedSegment.length > 0) {
                    channelid = this.selectedSegment[0].id;
                    if (channelid == "ALL") {
                        channelid = "ALL";
                    }
                }
                else {
                    isError = true;
                    if (errMsg == '') {
                        errMsg += 'Segment';
                    }
                    else {
                        errMsg += ',Segment';
                    }
                }
            }
        }
        else {
            isError = true;
            if (errMsg == '') {
                errMsg += 'Channel';
            }
            else {
                errMsg += ',Channel';
            }
        }


        //product section
        if (this.selectedSupplyChainType.length > 0) {
            supplyChainId = this.selectedSupplyChainType[0].itemName;
        }
        else {
            isError = true;
            if (errMsg == '') {
                errMsg += 'Supply Chain';
            }
            else {
                errMsg += ',Supply Chain';
            }
        }

        if (this.selectedProduct.length > 0) {
            lookupproduct = this.selectedProduct[0].itemName;
            if (this.selectedProduct[0].itemName == 'ALL') {
                strProductId = '';
            }
            else if (this.selectedProduct[0].itemName == 'PRODUCT_TREE') {
                var strLevel = this.selectedProductTreeData.split('__')[1];
                var selectedProdId = this.selectedProductTreeData.split('__')[0];

                if (selectedProdId == '') {
                    isError = true;
                    if (errMsg == '') {
                        errMsg += 'Product';
                    }
                    else {
                        errMsg += ',Product';
                    }
                }
                else {
                    if (selectedProdId != null && selectedProdId != '') {
                        //strProductId = selectedProdId.substring(0, strFgaId.indexOf("("));
                        strProductId = selectedProdId.split('(')[1].split(')')[0]
                    }

                }
            }

            else if (this.selectedProduct[0].itemName == 'FHC_TREE') {
                var strLevel = this.selectedProductTreeData.split('__')[1];
                var selectedFhcId = this.selectedProductTreeData.split('__')[0];

                if (strLevel == '1') {
                    strfhcType = 'FHC_ITEM';
                }
                if (strLevel == '2') {
                    strfhcType = "FGA_ITEM";
                }

                if (this.selectedProductTreeData == '') {
                    isError = true;
                    if (errMsg == '') {
                        errMsg += 'FHC';
                    }
                    else {
                        errMsg += ',FHC';
                    }
                }
                else {

                    if (strfhcType == "FGA_ITEM") {
                        strFgaId = selectedFhcId;
                        if (strFgaId != null && strFgaId != '') {
                            strFgaId = strFgaId.substring(0, strFgaId.indexOf("("));
                        }
                    }
                    else if (strfhcType == "FHC_ITEM") {
                        strFgaId = selectedFhcId;
                        if (strFgaId != null && strFgaId != '') {
                            strFgaId = strFgaId.substring(0, strFgaId.indexOf("("));
                        }
                    }
                }
            }


            else if (this.selectedProduct[0].itemName == "FGA") {

                strProductId = "";
                strfhcType = 'FGA_ITEM';

                if (this.selectedProductTreeData == '') {
                    isError = true;
                    if (errMsg == '') {
                        errMsg += 'Product';
                    }
                    else {
                        errMsg += ',Product';
                    }
                }
                else {
                    var strLevel = this.selectedProductTreeData.split('__')[1];
                    var selectedFgaId = this.selectedProductTreeData.split('__')[0];

                    strfhcType = "FGA_ITEM";
                    strFgaId = selectedFgaId;
                    if (strFgaId != null && strFgaId != '') {
                        strFgaId = strFgaId.substring(0, strFgaId.indexOf("("));
                    }
                }
            }
        }
        else {
            isError = true;
            if (errMsg == '') {
                errMsg += 'Product';
            }
            else {
                errMsg += ',Product';
            }
        }


        strsearcAccountID = null;
        
        //set search input params
        var objStrSearchInputParams: any = this.strSearchInputParams;

        objStrSearchInputParams.Geoid = geoid.toString();
        objStrSearchInputParams.GeoType = geoType.toString();
        objStrSearchInputParams.Channelid = channelid.toString();
        objStrSearchInputParams.ChannelType = channelType.toString();
        objStrSearchInputParams.Lookupproduct = lookupproduct.toString();
        objStrSearchInputParams.StrProductId = strProductId.toString();
        objStrSearchInputParams.Item = strFgaId.toString();
        objStrSearchInputParams.ItemType = strfhcType.toString();
        objStrSearchInputParams.Ssc = supplyChainId.toString();
        objStrSearchInputParams.ShipVia = null;
        objStrSearchInputParams.UseShipVia = false;
        objStrSearchInputParams.StrsearcAccountID = strsearcAccountID;

        if (!isError) {

            //call service for search 
            this.rowData = []
            this._fulfillmentSiteAssignmentServiceService.GetFulfillmentSearchResult(objStrSearchInputParams)
                .map((data: any) => data.json()).subscribe((data: any) => {
                    if (data.length > 0) {
                        this.rowData = data;
                        this.gridBtnExcelExport = true;
                    }

                    else {
                        this.DisplayMsg('No Rules Exists', 'Fail');
                    }
                },
                    err => console.log(err),
                );
        }
        else {
            errMsg = "Please select " + errMsg;
            this.DisplayMsg(errMsg, 'fail')
        }
    }

    DisplayMsg(msg: any, code: any) {
        this.hideShowAlertMsg = true;
        if (code == 'success') {
            this.successMsg = msg;
            this.colorCode = 'success';
        }
        else {
            this.successMsg = msg;
            this.colorCode = 'danger';
        }
        return false;
    }

    onSelectGeogrphy(event: any) {

        if (this.selectedGeogrphy[0].itemName == "ALL") {
            this.isCountryDdlOpen = true;
            this.isGeographyTree = true;
            this.selectedGeogrphyTreeData = '';
            this.selectedGeogrphyTreeData_label=''
        }
        else if (this.selectedGeogrphy[0].itemName == "GEOGRAPHY_TREE") {
            this.isCountryDdlOpen = true;
            this.isGeographyTree = false;
        }
        else if (this.selectedGeogrphy[0].itemName == "COUNTRY") {
            this.isCountryDdlOpen = false;
            this.isGeographyTree = true;
            this.selectedGeogrphyTreeData = '';
            this.selectedGeogrphyTreeData_label = '';
        }
        else {
            this.isCountryDdlOpen = true;
            this.isGeographyTree = true;
            this.selectedGeogrphyTreeData = '';
            this.selectedGeogrphyTreeData_label = '';
        }
    }

    onDeSelectGeogrphy() {
        this.isCountryDdlOpen = true;
        this.isGeographyTree = true;
        this.selectedGeogrphyTreeData = '';
        this.selectedGeogrphyTreeData_label = '';
    }

    onSelectedProduct() {

        this.selectedProductTreeData = '';
        this.selectedProductTreeData_label = '';

        if (this.selectedProduct[0].itemName == "PRODUCT_TREE" || this.selectedProduct[0].itemName == "FHC_TREE" || this.selectedProduct[0].itemName == "FGA") {
            this.isProductTree = false;
            
        }
        else {
            this.isProductTree = true;
            
        }
    }

    onDeSelectedProduct() {
        this.selectedProductTreeData = '';
        this.selectedProductTreeData_label = '';
        this.isProductTree = true;
    }



    onSelectChannel() {
        this.selectedChennelTreeData = '';
        this.selectedChennelTreeData_label = '';

        if (this.selectedChannel[0].itemName == "ALL") {
            this.isChannelTree = true;
            this.isSegmentDdlOpen = true;
        }
        else if (this.selectedChannel[0].itemName == "CHANNEL_TREE") {
            this.isChannelTree = false;
            this.isSegmentDdlOpen = true;
        }
        else if (this.selectedChannel[0].itemName == "SEGMENT") {
            this.isChannelTree = true;
            this.isSegmentDdlOpen = false;
        }
        else {
            this.isChannelTree = true;
            this.isSegmentDdlOpen = true;
        }
    }

    onDeSelectChannel() {
        this.selectedChennelTreeData = '';
        this.selectedChennelTreeData_label = '';

        this.isChannelTree = true;
        this.isSegmentDdlOpen = true;
    }

    getGeogrphyData() {
        this._fulfillmentSiteAssignmentServiceService.getMasterDataData('GEO')
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.geographyData = [];
                        for (var i = 0; i < data.length; i++) {

                            this.geographyData.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })
                        }
                    }
                },
                err => console.log(err),
            );

    }

    getCountryData() {

        this._fulfillmentSiteAssignmentServiceService.getGeoCountries()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.countryData = [];
                        this.countryData.push({ id: '0', itemName: 'ALL' });
                        for (var i = 0; i < data.length; i++) {

                            this.countryData.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })
                        }
                    }
                },
                err => console.log(err),
            );

    }

    getProductData() {

        this._fulfillmentSiteAssignmentServiceService.getMasterDataData('PRODUCT')
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.productData = [];
                        for (var i = 0; i < data.length; i++) {

                            this.productData.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })
                        }
                    }
                },
                err => console.log(err),
            );
    }

    getChannelData() {

        this._fulfillmentSiteAssignmentServiceService.getMasterDataData('CHANNEL')
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.channelData = [];
                        for (var i = 0; i < data.length; i++) {

                            this.channelData.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })
                        }
                    }
                },
                err => console.log(err),
            );
    }


    getSegmentData() {

        this._fulfillmentSiteAssignmentServiceService.getChannelSegments()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.segmentData = [];
                        this.segmentData.push({ id: '0', itemName: 'ALL' });
                        for (var i = 0; i < data.length; i++) {

                            this.segmentData.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })
                        }
                    }
                },
                err => console.log(err),
            );
            
    }

    getSupplyChainTypeData() {

        this._fulfillmentSiteAssignmentServiceService.getMasterDataData('SUPPLYCHAINTYPE')
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.supplyChainTypeData = [];
                        for (var i = 0; i < data.length; i++) {

                            this.supplyChainTypeData.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })
                        }
                    }
                },
                err => console.log(err),
            );
    }

    FulfillmentExportExcel() {
        this.GenerateExcel(this.rowData);
    }

    GenerateExcel(rowData) {
        let param = [];
        for (var i = 0; i < rowData.length; i++) {

            param.push({
                ["GEOGRAPHY"]: rowData[i].geographyName,
                ["PRODUCT/FHC/FGA"]: rowData[i].productFga,
                ["CHANNEL"]: rowData[i].channelName,
                ["EFFECTIVE BEGIN DATE"]: rowData[i].effectiveStartDate,
                ["EFFECTIVE END DATE"]: rowData[i].effectiveEndDate,
                ["FULFILLMENT SITE"]: rowData[i].ps,
                ["SUPPLY CHAIN TYPE"]: rowData[i].ssc,
                ["SHIPVIA TYPE"]: rowData[i].shipVia,
                ["ACCOUNT ID"]: rowData[i].accountId,
                ["ACCOUNT NAME"]: rowData[i].accountName,
                ["LAST MODIFIED BY"]: rowData[i].sysLastModifiedBy,
                ["LAST MODIFIED DATE"]: rowData[i].sysLastModifiedDate
            });
        }
        this._fulfillmentSiteAssignmentServiceService.exportAsExcelFile(param, "FulfillmentRules");
    }

    FulfillmentDelete() {

        debugger;
        var singleDeleteFlagParam: string = "NO";

        this.deleteParams = [];
        var lstDelete: FulfillmentDelete = new FulfillmentDelete(); 
        //var deleteParams: FulfillmentDeleteInputParams[];

        //FulfillmentDelete
        let rowsSelection = this.gridOptionsName.api.getSelectedRows();

        for (var i = 0; i < rowsSelection.length; i++) {
            var recDelete: FulfillmentDeleteInputParams = new FulfillmentDeleteInputParams();
            recDelete.ID = rowsSelection[i].id.toString();
            recDelete.singleDeleteFlag = singleDeleteFlagParam;

            this.deleteParams.push(recDelete);
        }

        lstDelete.deleteParam = this.deleteParams;
        //let id = rowsSelection[0].id;
        if (rowsSelection.length > 0) {
            var outDeleteRoutingCount: any = 0;
            var outDeleteRoutingHeaderCount: any = 0;

            //var deleteParams =
            //{
            //    singleDeleteFlag: singleDeleteFlagParam,
            //    ID: id
            //};

           // let count = 0;

            this._fulfillmentSiteAssignmentServiceService.DeleteRoutingDetails_fulfillment(lstDelete)
                .subscribe(
                    (data: any) => {
                        if (data != null) {
                            data = data._body;
                            //count = data;

                        
                            var res: string[] = data.split(',');


                            //let count1 = 0;
                            //count1 = this.deleteRoutingDetailHeaders(deleteParams);

                            debugger;


                            if (parseInt(res[0]) > 0 && parseInt(res[1]) > 0) {
                                this.DisplayMsg('Record Deleted Successfully', 'success');
                                this.searchFullfillmentData('Delete_Fulfillment');
                            }
                            else {
                                this.DisplayMsg('Record Not Deleted Successfully', 'Fail');
                            }
                        }
                    },
                    err => console.log(err),
                );

        }
        else {
            this.DisplayMsg('Please select row to update from grid', 'Fail');
        }
    }

    //deleteRoutingDetailHeaders(deleteParams: any) {
    //    var count = 0;
    //    this._fulfillmentSiteAssignmentServiceService.deleteRoutingDetailsHeader(deleteParams)
    //        .subscribe(
    //            (data: any) => {
    //                if (data != null) {
    //                    data = JSON.parse(data._body);
    //                    count = data;

    //                    //alert(data);
    //                }
    //            },
    //            err => console.log(err),
    //        );
    //    return count;
    //}

    //Edit section - start
    getGeogrphyData_edit() {
        this._fulfillmentSiteAssignmentServiceService.getMasterDataData('GEO')
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.geographyData_edit = [];

                        this.geographyData_edit.push({ id: '0', itemName: '--SELECT--' });

                        for (var i = 0; i < data.length; i++) {

                            this.geographyData_edit.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })
                        }
                    }
                },
                err => console.log(err),
            );
    }

    getCountryData_edit() {
        this._fulfillmentSiteAssignmentServiceService.getGeoCountries()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.countryData_edit = [];
                        this.countryData_edit.push({ id: '0', itemName: '--SELECT--' });
                        for (var i = 0; i < data.length; i++) {

                            this.countryData_edit.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })
                        }
                    }
                },
                err => console.log(err),
            );
    }

    getProductData_edit() {
        this._fulfillmentSiteAssignmentServiceService.getMasterDataData('PRODUCT')
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.productData_edit = [];
                        this.productData_edit.push({ id: '0', itemName: '--SELECT--' });

                        for (var i = 0; i < data.length; i++) {

                            this.productData_edit.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })
                        }
                    }
                },
                err => console.log(err),
            );
    }

    getChannelData_edit() {
        this._fulfillmentSiteAssignmentServiceService.getMasterDataData('CHANNEL')
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.channelData_edit = [];
                        this.channelData_edit.push({ id: '0', itemName: '--SELECT--' });
                        for (var i = 0; i < data.length; i++) {

                            this.channelData_edit.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })
                        }
                    }
                },
                err => console.log(err),
            );
    }

    getSegmentData_edit() {
        this._fulfillmentSiteAssignmentServiceService.getChannelSegments()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.segmentData_edit = [];
                        this.segmentData_edit.push({ id: '0', itemName: '--SELECT--' });
                        for (var i = 0; i < data.length; i++) {

                            this.segmentData_edit.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })
                        }
                    }
                },
                err => console.log(err),
            );
    }

    getSupplyChainTypeData_edit() {
        this._fulfillmentSiteAssignmentServiceService.getMasterDataData('SUPPLYCHAINTYPE_edit')
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.supplyChainTypeData_edit = [];
                        this.supplyChainTypeData_edit.push({ id: '0', itemName: '--SELECT--' });
                        for (var i = 0; i < data.length; i++) {

                            this.supplyChainTypeData_edit.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            });

                            this.supplyChainTypeData_edit.slice()
                        }
                    }
                },
                err => console.log(err),
            );
    }

    onSelectGeogrphy_edit(event: any) {

        this.selectedGeogrphyTreeData_edit = '';
        this.selectedGeogrphyTreeData_label_edit = '';

        if (this.selectedGeogrphy_edit[0].itemName == "ALL") {
            this.isCountryDdlOpen_edit = true;
            this.isGeographyTree_edit = true;
        }
        else if (this.selectedGeogrphy_edit[0].itemName == "GEOGRAPHY_TREE") {
            this.isCountryDdlOpen_edit = true;
            this.isGeographyTree_edit = false;
        }
        else if (this.selectedGeogrphy_edit[0].itemName == "COUNTRY") {
            this.isCountryDdlOpen_edit = false;
            this.isGeographyTree_edit = true;
        }
        else {
            this.isCountryDdlOpen_edit = true;
            this.isGeographyTree_edit = true;
        }
    }


    onDeSelectGeogrphy_edit() {
        this.selectedGeogrphyTreeData_edit = '';
        this.selectedGeogrphyTreeData_label_edit = '';

        this.isCountryDdlOpen_edit = true;
        this.isGeographyTree_edit = true;

    }

    onSelectedProduct_edit() {

        this.selectedProductTreeData_edit = '';
        this.selectedProductTreeData_label_edit = '';
        if (this.selectedProduct_edit[0].itemName == "PRODUCT_TREE" || this.selectedProduct_edit[0].itemName == "FHC_TREE" || this.selectedProduct_edit[0].itemName == "FGA") {
            this.isProductTree_edit = false;
        }
        else {
            this.isProductTree_edit = true;
        }
    }

    onDeSelectedProduct_edit() {
        this.selectedProductTreeData_edit = '';
        this.selectedProductTreeData_label_edit = '';
        this.isProductTree_edit = true;
    }

    onSelectChannel_edit() {
        this.selectedChennelTreeData_edit = '';
        this.selectedChennelTreeData_label_edit = '';
        if (this.selectedChannel_edit[0].itemName == "ALL") {
            this.isChannelTree_edit = true;
            this.isSegmentDdlOpen_edit = true;
        }
        else if (this.selectedChannel_edit[0].itemName == "CHANNEL_TREE") {
            this.isChannelTree_edit = false;
            this.isSegmentDdlOpen_edit = true;
        }
        else if (this.selectedChannel_edit[0].itemName == "SEGMENT") {
            this.isChannelTree_edit = true;
            this.isSegmentDdlOpen_edit = false;
        }
        else {
            this.isChannelTree_edit = true;
            this.isSegmentDdlOpen_edit = true;
        }
    }

    onDeSelectChannel_edit() {
        this.selectedChennelTreeData_edit = '';
        this.selectedChennelTreeData_label_edit = '';
        this.isChannelTree_edit = true;
        this.isSegmentDdlOpen_edit = true;
    }

    getGeoTreeDetail_edit() {
        this.nodesGeography_edit = [];

        this.treeViewDetailData.selectedDPValue = this.selectedGeogrphy_edit[0].itemName;
        this.treeViewDetailData.nodesData = this.nodesGeography_edit;
        this.treeViewDetailData.header = "Geo Tree Popup";
        this.treeViewDetailData.isSearchEnable = false;
        this.treeViewDetailData.msgNote = "";
        this.treeViewDetailData.treeViewType = "GEO_edit";
        this.treeViewDetailData.headerMsg = "";
        this.treeviewChild.bindTreeData();
    }

    getChannelTreeDetail_edit() {
        this.nodesChannel_edit = [];

        this.treeViewDetailData.selectedDPValue = this.selectedChannel_edit[0].itemName;
        this.treeViewDetailData.nodesData = this.nodesChannel_edit;
        this.treeViewDetailData.header = "Channel Tree Popup";
        this.treeViewDetailData.isSearchEnable = false;
        this.treeViewDetailData.msgNote = "";
        this.treeViewDetailData.headerMsg = "";
        this.treeViewDetailData.treeViewType = "CHANNEL_edit";

        this.treeviewChild.bindTreeData();
    }

    getProductTreeDetail_edit() {
        this.nodesProducts_edit = [];

        this.treeViewDetailData.selectedDPValue = this.selectedProduct_edit[0].itemName;
        this.treeViewDetailData.nodesData = this.nodesProducts_edit;
        //this.treeViewDetailData.header = "Product Tree Popup";
        this.treeViewDetailData.isSearchEnable = true;

        if (this.selectedProduct_edit[0].itemName == "PRODUCT_TREE") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All PRODUCT LOB Names";
            this.treeViewDetailData.headerMsg = "Please enter PRODUCT LOB Name!";
            this.treeViewDetailData.treeViewType = "PRODUCT_TREE_edit";
            this.treeViewDetailData.header = "Product Tree Popup";
        }
        if (this.selectedProduct_edit[0].itemName == "FHC_TREE") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All FHC Names";
            this.treeViewDetailData.headerMsg = "Please enter FHC ID!";
            this.treeViewDetailData.treeViewType = "PRODUCT_FHC_edit";
            this.treeViewDetailData.header = "FHC Popup";
        }
        if (this.selectedProduct_edit[0].itemName == "FGA") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All FGA's";
            this.treeViewDetailData.headerMsg = "Please enter FGA ID!";
            this.treeViewDetailData.treeViewType = "PRODUCT_FGA_edit";
            this.treeViewDetailData.header = "FGA Popup";
        }

        this.treeviewChild.bindTreeData();
    }

    SaveRule_edit() {

        this.clearErrorMsg();
        
        var isSelected: boolean = false;
        var strGeographyId: string = '';
        var strProductTree: string = '';
        var strChannelId: string = '';
        var strSupplyChainId: string = '';
        var strShipVia: string = '';
        var strAccountName: string = '';
        var strAccountId = '';

        

        strProductTree = this.selectedProduct_edit[0].itemName;
        var routing = '';
        strSupplyChainId = this.selectedSupplyChainType_edit[0].itemName;

        if (this.selectedSupplyChainType_edit[0].itemName == "BTP") {
                //if (ShipViaDrop.SelectedIndex > 0) {
                //    strShipVia = ShipViaDrop.SelectedValue;
                //}
                //else {
                //    strShipVia = string.Empty;
                //}
                //strAccountName = strAccountId = string.Empty;
                //if (ddlaccountname1.SelectedIndex > 0) {
                //    if (ddlaccountname1.SelectedValue.Last.indexOf('(') > -1) {
                //        strAccountId = ddlaccountname1.SelectedValue.substring(ddlaccountname1.SelectedValue.Last.indexOf('(')).Trim().TrimStart('(').TrimEnd(')');
                //        strAccountName = ddlaccountname1.SelectedValue.substring(0, ddlaccountname1.SelectedValue.Last.indexOf('(')).TrimEnd('(');
                //    }
                //}
            }
            var strSysSource: string = 'MOSSUI' // TODO : have to set configuaration   ConfigurationManager.AppSettings["MOSSUI"];
            //TODO : have to set params SPSite siteColl = null;
            //TODO : have to set params SPWeb rootSite = null;
            //TODO : have to set params SPUser spUser = null;
            var strUserName: string = '  ';
            var longTick: number;
            var strSysEnt: string = '';
            //var dateNow:Date;
            //TODO : have to set params siteColl = SPContext.Current.Site;
            //TODO : have to set params rootSite = siteColl.RootWeb;
            //TODO : have to set params spUser = rootSite.CurrentUser;
            //TODO : have to set params strUserName = spUser.ToString();
            //dateNow = new Date;
            //dateNow = Date.now;
            longTick = this.getTicks();
            var strSysEnt: string = 'MOSSUI';    //TODO: ConfigurationManager.AppSettings["sys_ent_state"].ToString();
            var hiddenvalue = '';

            var geoLevel: string = '';
            var channelLevel: string = '';
            var geotype: string = '';
            var channelType: string = '';
            var geoIdSearch: string = '';
            var channelIdSearch: string = '';
            var validationFlag: boolean = true;

            geotype = this.selectedGeogrphy_edit[0].itemName;
            channelType = this.selectedChannel_edit[0].itemName;

            var strBeginDate: string = '';
            var strEndDate: string = '';
        
            if (this.selectedGeogrphy_edit[0].itemName == "GEOGRAPHY_TREE") {

                var geoLevel_edit = this.selectedGeogrphyTreeData_edit.split('__')[1];
                var geoId = this.selectedGeogrphyTreeData_edit.split('__')[0];
                strGeographyId = geoId;

                if (strGeographyId != null && strGeographyId != '' && strGeographyId != "ALL_GEO") {
                    strGeographyId = strGeographyId.substring(0, strGeographyId.indexOf("("));
                }

                if (strGeographyId == "ALL_GEO") {
                    strGeographyId = "ALL_GEO";
                    geoLevel = "ALL_GEO";
                    geoIdSearch = "ALL";
                }

                if (geoLevel_edit == 'ALL_GEO') {
                    geoLevel = 'ALL_GEO';
                }
                if (geoLevel_edit == '1') {
                    geoLevel = "REGION";
                }
                if (geoLevel_edit == '2') {
                    geoLevel = 'SUB_REGION';
                }
                if (geoLevel_edit == '3') {
                    geoLevel = 'AREA';
                }
                if (geoLevel_edit == '4') {
                    geoLevel = 'COUNTRY';
                }
                geoIdSearch = strGeographyId;
            }
            else if (this.selectedGeogrphy_edit[0].itemName == "COUNTRY") {

                //var geoLevel_edit = this.selectedGeogrphyTreeData_edit.split('__')[1];
                //var geoId = this.selectedGeogrphyTreeData_edit.split('__')[0];
                //strGeographyId = geoId;

                debugger;
                strGeographyId = this.selectedCountry_edit[0].id; //comboGeoCreate.SelectedValue;
                geoLevel = "COUNTRY";
                geoIdSearch = this.selectedCountry_edit[0].id; //comboGeoCreate.SelectedValue;


                //if (strGeographyId != null && strGeographyId != '') {
                //    strGeographyId = strGeographyId.substring(0, strGeographyId.indexOf("("));
                //}

                geoLevel = "COUNTRY";
                geoIdSearch = strGeographyId;
            }
            else if (this.selectedGeogrphy_edit[0].itemName == "ALL") {

                strGeographyId = "ALL_GEO";
                geoLevel = "ALL_GEO";
                geoIdSearch = "ALL";
            }
            if (this.selectedChannel_edit[0].itemName == "CHANNEL_TREE") {

                var channelevel_edit = this.selectedChennelTreeData_edit.split('__')[1];
                var chanlId = this.selectedChennelTreeData_edit.split('__')[0];
                strChannelId = chanlId;

                if (strChannelId != null && strChannelId != "SACHA(TOTSLS)-ALL_CHANNEL") {
                    strChannelId = strChannelId.substring(0, strChannelId.indexOf("("));
                }
                if (strChannelId == "SACHA(TOTSLS)") {
                    strChannelId = "SACHA";
                    channelLevel = "ALL_CHANNEL";
                }
                if (strChannelId == "SACHA(TOTSLS)-ALL_CHANNEL") {
                    strChannelId = "SACHA";
                    channelLevel = "ALL_CHANNEL";
                }

                //get channel level
                channelLevel = this.getChannelLevel(strChannelId);
            }
            else if (this.selectedChannel_edit[0].itemName == "SEGMENT") {
                strChannelId = this.selectedSegment_edit[0].id;
                channelLevel = "SEGMENT";
            }
            else if (this.selectedChannel_edit[0].itemName == "ALL") {
                strChannelId = "SACHA";
                channelLevel = "ALL_CHANNEL";

            }

            var strProductId: any = '';
        if (this.selectedProduct_edit[0].itemName == 'PRODUCT_TREE') {
                debugger;
                var proLevel = this.selectedProductTreeData_edit.split('__')[1];
                var proId = this.selectedProductTreeData_edit.split('__')[0];

                if (proId != null && proId != '') {
                    //strProductId = proId.substring(0, proId.indexOf("("));
                    strProductId = proId.substring(proId.indexOf("(") + 1, proId.indexOf(")"));
                }
            }

            var lookupproduct = this.selectedProduct_edit[0].itemName;
            var strFgaId: string = '';
            var typeValue: string = '';
            var isError: boolean = false;
            var isVirtualSitePresent: boolean = false;

            //check for isvirtual site 
            for (var i = 0; i < this.rowData_split.length; i++) {
                var site = this.rowData_split[i].site;
                if (this.checkIsVirtualSite(site)) {
                    isVirtualSitePresent = true;
                }
            }

            var splitTotal: number = 0;

            splitTotal = this.splitTotal;

            debugger;
            if (!this.IsUniqueSite()) {
                var msg = 'Same Site Cannot Be Configured More Than Once for a Given Geography, Product/FGA, Channel and Supply Chain Type.';
                this.DisplayMsg(msg, 'Fail');
                isError = true;
            }
            else if (this.rowData_split.length > 0 && splitTotal != 100) {
                var msg = 'Split percentage should be equal to 100%.';
                this.DisplayMsg(msg, 'Fail');
                isError = true;
            }
            else if (isVirtualSitePresent && this.rowData_split.length > 1) {
                var msg = 'Virtual Site cannot be selected in combination of other sites.';
                this.DisplayMsg(msg, 'Fail');
                isError = true;
            }
            else {
                if (strProductTree == "FGA") {
                    strFgaId = '';
                    typeValue = 'PRODUCT_TREE' //TODO: ConfigurationManager.AppSettings["productTreeValue"].ToString();


                    var proLevel = this.selectedProductTreeData_edit.split('__')[1];
                    var proId = this.selectedProductTreeData_edit.split('__')[0];
                    strFgaId = proId;
                    if (strFgaId != null && strFgaId != '') {
                        strFgaId = strFgaId.substring(0, strFgaId.indexOf("("));
                    }
                    typeValue = "FGA_ITEM";
                }
                if (strProductTree == "FHC_TREE") {
                    strProductId = '';
                    strFgaId = '';

                    var fhcLevel = this.selectedProductTreeData_edit.split('__')[1];
                    var fhcId = this.selectedProductTreeData_edit.split('__')[0];


                    if (fhcLevel == '1') {
                        typeValue = 'FHC_ITEM';
                    }
                    if (fhcLevel == '2') {
                        typeValue = "FGA_ITEM";
                    }

                    if (typeValue == "FGA_ITEM") {
                        strFgaId = fhcId;
                        if (strFgaId != null && strFgaId != '') {
                            strFgaId = strFgaId.substring(0, strFgaId.indexOf("("));
                        }
                        typeValue = "FGA_ITEM";
                    }
                    else if (typeValue == "FHC_ITEM") {
                        strFgaId = fhcId;
                        if (strFgaId != null && strFgaId != '') {
                            strFgaId = strFgaId.substring(0, strFgaId.indexOf("("));
                        }
                        typeValue = "FHC_ITEM";
                    }

                }
                if (strProductTree == "ALL") {
                    strProductId = "ALL_PRODUCT";
                    strFgaId = '';
                    typeValue = 'PRODUCT_TREE';//TODO : ConfigurationManager.AppSettings["productTreeValue"].ToString();
                }
                if (strProductTree == "PRODUCT_TREE") {
                    var proLevel = this.selectedProductTreeData_edit.split('__')[1];
                    var proId = this.selectedProductTreeData_edit.split('__')[0];
                    typeValue = 'PRODUCT_TREE';// ConfigurationManager.AppSettings["productTreeValue"].ToString();
                    strProductId = proId;
                    if (strProductId != null && strProductId != '') {
                        //strProductId = strProductId.substring(0, strProductId.indexOf("("));
                        strProductId = strProductId.substring(strProductId.indexOf("(") + 1, strProductId.indexOf(")"));
                    }
                    strFgaId = '';
                }

                if (this.selecteEffectiveBeginDate_edit.length > 0 && this.selecteEffectiveBeginDate_edit[0].id !="0") {
                    strBeginDate = this.selecteEffectiveBeginDate_edit[0].id 
                }
                if (this.selecteEffectiveEndDate_edit.length > 0 && this.selecteEffectiveEndDate_edit[0].id != "0") {
                    strEndDate = this.selecteEffectiveEndDate_edit[0].id 
                }

                var fulfillmentSiteValue: string = '';
                var splitPercentValue: string = '';
                var strEffectiveBeginDate: string = '';
                var strEffectiveEndDate: string = '';
                var result: number = -1; //default value
                var strIntermRoutingID: string = '';

                if (!isError) {
                    //for (var k = 0; k < this.rowData_rule.length; k++) {

                        var selectedRow = this.gridOptionsFulfillment_rules.api.getSelectedRows();
                        if (selectedRow.length > 0) {

                            strEffectiveBeginDate = selectedRow[0].beginDateValue //grdViewDates.Rows[k].Cells[4].Text.ToString().Replace("&nbsp;", " ");
                            strEffectiveEndDate = selectedRow[0].endDateValue //grdViewDates.Rows[k].Cells[5].Text.ToString().Replace("&nbsp;", " ");

                            var _fulfillmentAddUpdateParams: FulfillmentAddUpdateParams = new FulfillmentAddUpdateParams();

                            var objStrRetriveInput: any = this.strSearchInputParams;
                            objStrRetriveInput.Geoid = strGeographyId.toString();
                            objStrRetriveInput.GeoType = geotype.toString();
                            objStrRetriveInput.Channelid = strChannelId.toString();
                            objStrRetriveInput.ChannelType = channelType.toString();
                            objStrRetriveInput.Lookupproduct = strProductTree.toString();
                            objStrRetriveInput.StrProductId = strProductId.toString();
                            objStrRetriveInput.Item = strFgaId.toString();
                            objStrRetriveInput.ItemType = '';
                            objStrRetriveInput.Ssc = strSupplyChainId.toString();
                            objStrRetriveInput.ShipVia = null;
                            objStrRetriveInput.UseShipVia = false;
                            objStrRetriveInput.StrsearcAccountID = strAccountId.toString();
                            objStrRetriveInput.effectiveStartDate = strEffectiveBeginDate;
                            objStrRetriveInput.effectiveEndDate = strEffectiveEndDate;


                            var objSelectedRow: FulfillmentRuleDetails = new FulfillmentRuleDetails();

                            objSelectedRow.hiddenRoutingId = selectedRow[0].hiddenRoutingId;
                            objSelectedRow.fulfillmentSiteId = selectedRow[0].fulfillmentSiteId;
                            objSelectedRow.effectiveBeginDate = selectedRow[0].effectiveBeginDate;
                            objSelectedRow.effectiveEndDate = selectedRow[0].effectiveEndDate;
                            objSelectedRow.beginDateValue = selectedRow[0].beginDateValue;
                            objSelectedRow.endDateValue = selectedRow[0].endDateValue;
                            objSelectedRow.hiddlenFlag = selectedRow[0].hiddlenFlag;

                            _fulfillmentAddUpdateParams.objAddUpdateParams = objStrRetriveInput;
                            _fulfillmentAddUpdateParams.objAddUpdateRuleData = this.rowData_rule;
                            _fulfillmentAddUpdateParams.objselectedRow = objSelectedRow;
                            _fulfillmentAddUpdateParams.fulfillUpdateMode = this.fulfillUpdateMode;
                            _fulfillmentAddUpdateParams.strIntermRoutingID = strIntermRoutingID;
                            _fulfillmentAddUpdateParams.typeValue = typeValue;
                            _fulfillmentAddUpdateParams.longtick = longTick.toString();
                            _fulfillmentAddUpdateParams.sysSource = strSysSource;
                            _fulfillmentAddUpdateParams.strUserName = strUserName;
                            _fulfillmentAddUpdateParams.strAccountName = strAccountName;
                            _fulfillmentAddUpdateParams.strSysEnt = strSysEnt;

                            this.AddUpdateRoutingRule_EditSection(_fulfillmentAddUpdateParams, selectedRow, isError);

                        }
                        else {

                            var msg = 'Please select a row.';
                            this.DisplayMsg(msg, 'fail');
                            return false;
                        }

                }
            }
        
    }


    AddUpdateRoutingRule_EditSection(_fulfillmentAddUpdateParams: FulfillmentAddUpdateParams, selectedRow: any, isError: any) {
        this._fulfillmentSiteAssignmentServiceService.AddUpdateRoutingRule_EditSection(_fulfillmentAddUpdateParams)
        .subscribe(
            (data: any) => {
                if (data != null) {

                    var result: string[] = data._body.toString().split('::');
                    if (result[0] == "1") {
                        this.DisplayMsg(result[2].toString(), 'Fail');
                        return false;
                    }
                    else if (result[0] == "0") {
                        this.searchFullfillmentData('save_rule');
                       var  msg = 'Record Saved Successfully';
                        this.DisplayMsg(msg, 'success');
                    }
                }
            },
                err => console.log(err),
            );

    }

    getTicks() {
        var d = new Date();
       return ((d.getTime() * 10000) + 621355968000000000);
    }


    //DeleteFulfillDetails(lstFulfillDeleteSite: FullFillmentRuleData) {
    //    this._fulfillmentSiteAssignmentServiceService.DeleteFulfillDetails(lstFulfillDeleteSite)
    //        .map((data: any) => data.json()).subscribe((data: any) => {
    //            if (data.length > 0) {
    //                data = JSON.parse(data);
    //                return data;
    //            }
    //        },
    //            err => console.log(err),
    //        );
    //}

    //updatefulfillment(updateFulfillment: FullFillmentRuleData) {
    //    this._fulfillmentSiteAssignmentServiceService.updatefulfillment(updateFulfillment)
    //        .map((data: any) => data.json()).subscribe((data: any) => {
    //            if (data.length > 0) {
    //                data = JSON.parse(data);
    //                return data;
    //                //return boolien value 
    //                //alert();
    //                //this.routingRuleDt = data;
    //            }
    //        },
    //            err => console.log(err),
    //        );
    //}

    //UpdateValidateDates(lstUpdateValidateParams:UpdateValidateDateInputParams) {

    //    var startDate = lstUpdateValidateParams.strEffectiveBeginDate;
    //    var endDate = lstUpdateValidateParams.strEffectiveEndDate;
    //    var routingId = lstUpdateValidateParams.routing;
    //    var fixDate = '12/31/9999';
    //    //Convert the strings into standard date formats.

    //    var sDate = this._datePipe.transform(startDate, 'MM-DD-YYYY');
    //    var eDate = this._datePipe.transform(endDate, 'MM-DD-YYYY');
    //    var fDate = this._datePipe.transform(fixDate, 'MM-DD-YYYY');

       

    //    if (sDate != null && sDate != " " && sDate != '') {
    //        if (sDate == fDate) {
    //            var msg = 'Invalid Start Date';
    //            this.DisplayMsg(msg, 'Fail');
    //            return false;
    //        }
    //    }
    //    if (eDate != null && eDate != " " && eDate != '') {
    //        if (eDate == fDate) {
    //            var msg = 'Invalid End Date';
    //            this.DisplayMsg(msg, 'Fail');
    //            return false;
    //        }
    //    }

    //    //Validation: Start date should less than end date
    //    if (sDate != null && eDate != null && sDate != " " && sDate != '' && eDate != " " && eDate != '') {
    //        if (sDate > eDate) {
    //            var msg = 'Effective Start Date must be less than Effective End Date';
    //            this.DisplayMsg(msg, 'Fail');
    //            return false;
    //        }
    //    }

    //    //Call validation function by passing the dates.
    //    //if (startDate != null && endDate != null && startDate != " " && startDate != string.Empty && endDate != " " && endDate != string.Empty)
    //    //{
    //    if (routingId != null) {

    //        this.UpdateValidateEffectiveDates(lstUpdateValidateParams);
    //    }
    //    //}

    //    return true;
    //}

    //UpdateValidateEffectiveDates(lstUpdateValidateParams: UpdateValidateDateInputParams) {
        
    //    this._fulfillmentSiteAssignmentServiceService.UpdateValidateEffectiveDates(lstUpdateValidateParams)
    //        .map((data: any) => data.json()).subscribe((data: any) => {
    //            if (data.length > 0) {
    //                data = JSON.parse(data);
    //                return data;
    //                //return boolien value 
    //                //alert();
    //                //this.routingRuleDt = data;
    //            }
    //        },
    //            err => console.log(err),
    //        );
    //}

    //AddRoutingdetails(_FullFillmentRuleData: FullFillmentRuleData) {
    //    var count = 0;
    //    Observable.create(observer => {
    //        this._fulfillmentSiteAssignmentServiceService.AddRoutingdetails(_FullFillmentRuleData)

    //            .map((data: any) => data.json())
    //            .finally(() => observer.complete())
    //            .subscribe((data: any) => {
    //                //.subscribe((data: any) => {
    //                if (data != null) {

    //                    data = Promise.resolve(JSON.parse(data));
    //                    count = data;
    //                    //return boolien value 
    //                    //alert();
    //                    //this.routingRuleDt = data;
    //                }
    //            },
    //                err => console.log(err),
    //            );

            
    //    });

    //    return count;
    //}


    //AddFullFillmentsites(_FullFillmentRuleData: FullFillmentRuleData) {

    //    debugger;
    //    var count = 0;
    //    Observable.create(observer => {
    //        this._fulfillmentSiteAssignmentServiceService.AddFullFillmentsites(_FullFillmentRuleData)
    //            .map((data: any) => data.json())
    //            .finally(() => observer.complete())
    //            .subscribe((data: any) => {

    //                debugger;
    //                if (data != null) {

    //                    data = JSON.parse(data);
    //                    count = data;
    //                    //return boolien value 
    //                    //alert();
    //                    //this.routingRuleDt = data;
    //                }
    //            },
    //                err => console.log(err),
    //            );

    //        return count;
    //    });
    //}

    //InsertValidateDates(_FullFillmentRuleData: FullFillmentRuleData) {


    //    var startDate = _FullFillmentRuleData.effectiveBeginDate;
    //    var endDate = _FullFillmentRuleData.effectiveEndDate;
    //    var fixDate = '12/31/9999';
    //    //Convert the strings into standard date formats.
        

    //    var sDate = this._datePipe.transform(startDate, 'MM-DD-YYYY');
    //    var eDate = this._datePipe.transform(endDate, 'MM-DD-YYYY');
    //    var fDate = this._datePipe.transform(fixDate, 'MM-DD-YYYY');

    //    if (sDate != null && sDate != " " && sDate != '') {
    //        if (sDate == fDate) {
    //            var msg = 'Invalid Start Date';
    //            this.DisplayMsg(msg, 'Fail');
    //            return false;
    //        }
    //    }
    //    if (eDate != null && eDate != " " && eDate != '') {
    //        if (eDate == fDate) {
    //            var msg = 'Invalid End Date';
    //            this.DisplayMsg(msg, 'Fail');
    //            return false;
    //        }
    //    }

    //    //Validation: Start date should less than end date
    //    if (sDate != null && eDate != null && sDate!= " " && sDate != '' && eDate != " " && eDate != '') {
    //        if (sDate > eDate) {
    //            var msg = 'Effective Start Date must be less than Effective End Date';
    //            this.DisplayMsg(msg, 'Fail');
    //            return false;
    //        }
    //    }

    //    if (_FullFillmentRuleData != null) {
           
    //        this._fulfillmentSiteAssignmentServiceService.InsertValidateEffectiveDates(_FullFillmentRuleData)
    //            .map((data: any) => data.json()).subscribe((data: any) => {
    //                if (data.length > 0) {
    //                    data = JSON.parse(data);

    //                    return data; //return boolien value 
    //                    //alert();
    //                    //this.routingRuleDt = data;
    //                }
    //            },
    //                err => console.log(err),
    //            );

    //    }

    //    return true;

    //}

    //retrieveRoutingDetailsByRoutingId(routingID) {
    //    this.routingRuleDt = [];
    //    this._fulfillmentSiteAssignmentServiceService.retrieveRoutingDetailsByRoutingId(routingID)
    //        .map((data: any) => data.json()).subscribe((data: any) => {
    //            if (data.length > 0) {
    //                    data = JSON.parse(data);
    //                    this.routingRuleDt = data;
    //                }
    //            },
    //            err => console.log(err),
    //        );
    //}

    //retrieveRoutingDetails(strRetriveInputParams:FulFillmentSearchInput) {
    //    this.routingRuleDt = [];
    //    this._fulfillmentSiteAssignmentServiceService.retrieveRoutingDetails(strRetriveInputParams)
    //        .map((data: any) => data.json()).subscribe((data: any) => {
    //            if (data.length > 0) {
    //                data = JSON.parse(data);
    //                this.routingRuleDt = data;
    //            }
    //        },
    //            err => console.log(err),
    //        );
    //}

    IsUniqueSite() {
        var isUnique:boolean = true;

        var sRow = this.gridOptionsFulfillment_rules.api.getSelectedRows();

        if (sRow.length > 0) {
            var site = sRow[0].fulfillmentSiteId.split(',');
            for (var i = 0; i < site.length; i++) {
                var s = site[i].substring(0, site[i].indexOf("(")); //site[i].indexOf("(");

                for (var j = 0; j < site.length; j++) {
                    var s1 = site[j].substring(0, site[j].indexOf("(")) //site[j].indexOf("(");

                    if (i != j && s.trim() == s1.trim()) {
                        isUnique = false;
                        break;
                    }
                }
                if (!isUnique) break;
            }
        }
        return isUnique;
}


FulfillmentUpdate()
{
    this.fulfillUpdateMode = true;
}

    FulfillmentRetrieve_Edit() {
        this.clearErrorMsg();
        
        var errMsg: any = '';

        //fill effective dates 
        this.GetFSAFiscalCalendarInfo_BeginDate();
        this.GetFSAFiscalCalendarInfo_EndDate();

        debugger;
        //code structure taken from retrive function 
        var strGeographyId: any = '';
        var strProductTree: any = '';
        var strProductId: any = '';
        var strFgaId: any = '';
        var channelType: any = '';
        if (this.selectedProduct_edit.length > 0) {
            strProductTree = this.selectedProduct_edit[0].itemName;

            
            if (this.selectedProduct_edit[0].itemName == 'FGA') {
                var proLevel = this.selectedProductTreeData_edit.split('__')[1];
                var proId = this.selectedProductTreeData_edit.split('__')[0];

                if (proId != null && proId != '') {
                    strFgaId = proId.substring(0, proId.indexOf("("));
                }
            }

            
            if (this.selectedProduct_edit[0].itemName == 'PRODUCT_TREE') {
                var proLevel = this.selectedProductTreeData_edit.split('__')[1];
                var proId = this.selectedProductTreeData_edit.split('__')[0];

                if (proId != null && proId != '') {
                    //strProductId = proId.substring(0, proId.indexOf("("));
                    strProductId = proId.substring(proId.indexOf("(")+1, proId.indexOf(")"));
                }
            }
        }
        else {

            isError = true;
            if (errMsg == '') {
                errMsg += 'Product';
            }
            else {
                errMsg += ',Product';
            }
        }

        //ToDo -have to check for product select ALL
        var strChannelId: any = '';
        var strSupplyChainId: any = '';

        if (this.selectedSupplyChainType_edit.length > 0) {
            strSupplyChainId = this.selectedSupplyChainType_edit[0].itemName;
        }
        else {
            isError = true;
            if (errMsg == '') {
                errMsg += 'Supply Chain Type';
            }
            else {
                errMsg += ',Supply Chain Type';
            }
        }

        var strShipVia: any = '';
        var geoLevel: any = '';

        if (this.selectedGeogrphy_edit.length > 0) {
            geoLevel = this.selectedGeogrphy_edit[0].itemName;
            if (geoLevel == 'ALL') {
                geoLevel = 'ALL_GEO';
            }
        }
        else {
            isError = true;
            if (errMsg == '') {
                errMsg += 'Geography';
            }
            else {
                errMsg += ',Geography';
            }
        }

        if (this.selectedChannel_edit.length > 0) {
             channelType=this.selectedChannel_edit[0].itemName;
            if (channelType == 'ALL') {
                channelType = 'ALL_CHANNEL';
            }
        }
        else {
            isError = true;
            if (errMsg == '') {
                errMsg += 'Channel';
            }
            else {
                errMsg += ',Channel';
            }
        }

        var geoLevelVal: any = '';
        var channelLevel: any = '';
        var isError: any = false;
        
        var strAccountID: any = '';
        var strfhcType = '';


        //fill master fulfillment dropdown - start
        if (this.selectedGeogrphy_edit.length > 0 && this.selectedCountry_edit.length > 0) {
            this.FillMasterSiteDDL();
        }


        //fill master fulfillment dropdown - end

        //PRODUCT Set params - start 
        if (strProductTree == "FGA") {
            strProductId = '';
        }
        if (strProductTree == "ALL") {
            strProductId = "ALL_PRODUCT";
            strFgaId = '';
        }
        if (strProductTree == "FHC_TREE") {
            strProductId = '';
            strFgaId = '';

            var fhcLevel = this.selectedProductTreeData_edit.split('__')[1];
            var fhcId = this.selectedProductTreeData_edit.split('__')[0];
            

            if (fhcLevel == '1') {
                strfhcType = 'FHC_ITEM';
            }
            if (fhcLevel == '2') {
                strfhcType = "FGA_ITEM";
            }

            if (strfhcType == "FGA_ITEM") {
                strFgaId = fhcId;
                if (strFgaId != null && strFgaId !='') {
                    strFgaId = strFgaId.substring(0, strFgaId.indexOf("("));
                }
            }
            else if (strfhcType == "FHC_ITEM") {
                strFgaId = fhcId;
                if (strFgaId != null && strFgaId != '') {
                    strFgaId = strFgaId.substring(0, strFgaId.indexOf("("));
                }
            }
        }
        if (strProductTree == "PRODUCT_TREE") {
            var proLevel = this.selectedProductTreeData_edit.split('__')[1];
            var proId = this.selectedProductTreeData_edit.split('__')[0];
            strProductId = proId;
            if (strProductId != null && strProductId != '') {
                //strProductId = strProductId.substring(0, strProductId.indexOf("("));
                strProductId = strProductId.substring(strProductId.indexOf("(")+1, strProductId.indexOf(")"));
            }
            strFgaId = '';
        }


        if (this.selectedGeogrphy_edit.length > 0) {
            if (this.selectedGeogrphy_edit[0].itemName == "--SELECT--") {
                isError = true;
                if (errMsg == '') {
                    errMsg += 'Geography';
                }
                else {
                    errMsg += ',Geography';
                }

                strGeographyId = '';
            }
            else if (this.selectedCountry_edit.length == 0)
            {
                isError = true;
                if (errMsg == '') {
                    errMsg += 'Country';
                }
                else {
                    errMsg += ',Country';
                }
            }
            else if (this.selectedCountry_edit[0].itemName == "--SELECT--" && geoLevel != "GEOGRAPHY_TREE" && geoLevel != "ALL_GEO") {
                isError = true;
                if (errMsg == '') {
                    errMsg += 'Geography';
                }
                else {
                    errMsg += ',Geography';
                }

                strGeographyId = '';
            }
        }

        if (geoLevel == "GEOGRAPHY_TREE") {

            var geographyLevel = this.selectedGeogrphyTreeData_edit.split('__')[1];
            var geoId = this.selectedGeogrphyTreeData_edit.split('__')[0];

            if (geoId == null || geoId == '') {
                isError = true;
                if (errMsg == '') {
                    errMsg += 'Geography';
                }
                else {
                    errMsg += ',Geography';
                }

                strGeographyId = '';
            }
        }

            if (strProductTree == "--SELECT--") {
                    isError = true;
                    if (errMsg == '') {
                        errMsg += 'Product/FGA/FHC';
                    }
                    else {
                        errMsg += ',Product/FGA/FHC';
                    }
        }

        if (strProductTree == 'PRODUCT_TREE') {
            var productLevel = this.selectedProductTreeData_edit.split('__')[1];
            var proId = this.selectedProductTreeData_edit.split('__')[0];

            if (proId == null || proId == '') {
                isError = true;
                if (errMsg == '') {
                    errMsg += 'Product';
                }
                else {
                    errMsg += ',Product';
                }
            }
        }
        if (strProductTree == "FHC_TREE") {

            var fhcLevel = this.selectedProductTreeData_edit.split('__')[1];
            var fhcId = this.selectedProductTreeData_edit.split('__')[0];

            if (fhcId == null || fhcId == '') {
                isError = true;
                if (errMsg == '') {
                    errMsg += 'FHC';
                }
                else {
                    errMsg += ',FHC';
                }
            }
        }
        if (strProductTree == "FGA") {
            var fgaLevel = this.selectedProductTreeData_edit.split('__')[1];
            var fgaId = this.selectedProductTreeData_edit.split('__')[0];

            if (fgaId == null || fgaId == '') {
                isError = true;
                if (errMsg == '') {
                    errMsg += 'FGA';
                }
                else {
                    errMsg += ',FGA';
                }
            }
        }
        
        if (this.selectedChannel_edit.length ==0 || this.selectedChannel_edit[0].itemName == "--SELECT--") {

                if (errMsg == '') {
                    errMsg += 'Channel';
                }
                else {
                    errMsg += ',Channel';
                }
                isError = true;
                strChannelId = '';
        }
        else if (this.selectedSegment_edit.length ==0)
        {
            if (errMsg == '') {
                errMsg += 'Segment';
            }
            else {
                errMsg += ',Segment';
            }
            isError = true;
        }
            else if (this.selectedSegment_edit[0].itemName == "--SELECT--" && channelType != "CHANNEL_TREE" && channelType != "ALL_CHANNEL") {
                if (errMsg == '') {
                    errMsg += 'Channel';
                }
                else {
                    errMsg += ',Channel';
                }
                isError = true;
                strChannelId = '';
        }

        if (channelType == "CHANNEL_TREE") {

            var channelevel_edit = this.selectedChennelTreeData_edit.split('__')[1];
            var chanlId = this.selectedChennelTreeData_edit.split('__')[0];
            if (chanlId == null || chanlId == '') {
                if (errMsg == '') {
                    errMsg += 'Channel';
                }
                else {
                    errMsg += ',Channel';
                }
                isError = true;
                strChannelId = '';
            }
            
        }
        if (strSupplyChainId == "--SELECT--" && strfhcType != "FGA_ITEM") {

                if (errMsg == '') {
                    errMsg += 'Supply Chain Type';
                }
                else {
                    errMsg += ',Supply Chain Type';
                }
                isError = true;

                strChannelId = '';
        }

        
        //set search input params
        var objStrRetriveInputParams: any = this.strSearchInputParams;

        objStrRetriveInputParams.Geoid = strGeographyId.toString();
        objStrRetriveInputParams.GeoType = geoLevelVal.toString();
        objStrRetriveInputParams.Channelid = strChannelId.toString();
        objStrRetriveInputParams.ChannelType = channelType.toString();
        objStrRetriveInputParams.Lookupproduct = strProductTree.toString();
        objStrRetriveInputParams.StrProductId = strProductId.toString();
        objStrRetriveInputParams.Item = strFgaId.toString();
        objStrRetriveInputParams.ItemType = strfhcType.toString();
        objStrRetriveInputParams.Ssc = strSupplyChainId.toString();
        objStrRetriveInputParams.ShipVia = null;
        objStrRetriveInputParams.UseShipVia = false;
        objStrRetriveInputParams.StrsearcAccountID = strAccountID.toString();

        
        if (!isError) {
            var retriveData = [];
            this._fulfillmentSiteAssignmentServiceService.GetFulfillmentRetrive(objStrRetriveInputParams)
                .map((data: any) => data.json()).subscribe((data: any) => {
                    if (data.length > 0) {
                        this.rowData_rule = data;
                        this.enableDisableOnRetrieve(true);

                        this.isFulfillmentRuleGrid = false;
                    }

                    else {
                        this.DisplayMsg('No Rules Exists', 'Fail');
                        this.enableDisableOnRetrieve(true);
                        this.document.body.scrollTop = 0;
                    }
                },
                    err => console.log(err),
                );
        }
        else
        {
            errMsg = 'Please select ' + errMsg;
            this.DisplayMsg(errMsg, 'fail');
            this.document.body.scrollTop = 0;
        }
    }

    enableDisableOnRetrieve(isDiable:boolean) {
        this.isMainFulfillment = !isDiable;
        this.isddlGeography_edit = isDiable;
        this.isddlCountry_edit = isDiable;
        this.isddlProduct_edit = isDiable;
        this.isddlChannel_edit = isDiable;
        this.isddlSegment_edit = isDiable;
        this.isddlSupplychain_edit = isDiable;
        this.btnRetrive_Edit = !isDiable;
        this.isProductTreeDisable_edit = isDiable; 
        this.isGeographyTreeDisable_edit = isDiable; 
        this.isChannelTreeDisable_edit = isDiable; 
        //this.btnClear_Edit = !isDiable;
    }

    FulfillmentClear_Edit() {
        this.clearErrorMsg();
        this.splitTotal = 0;
        this.rowData_rule = [];
        this.isFulfillmentTempGrid = true;

        this.rowData_split = [];
        this.isFulfillmentRuleGrid = true;

        this.txtSplitPercentage = '';
        this.isMainFulfillment = true;

        this.mainListSitesData_edit = [];
        this.selectedMainListSites_edit = [];

        this.EffectiveBeginDateData_edit = [];
        this.selecteEffectiveBeginDate_edit = [];
        this.EffectiveEndDateData_edit = [];
        this.selecteEffectiveEndDate_edit = [];

        this.selectedChannel_edit[0] = { id: "0", itemName: "--SELECT--" };
        this.selectedGeogrphy_edit[0] = { id: "0", itemName: "--SELECT--" };
        this.selectedProduct_edit[0] = { id: "0", itemName: "--SELECT--" };
        this.selectedSupplyChainType_edit[0] = { id: "0", itemName: "--SELECT--" };
        this.selectedCountry_edit[0] = { id: "0", itemName: "--SELECT--" };
        this.selectedSegment_edit[0] = { id: "0", itemName: "--SELECT--" };
        this.selecteEffectiveBeginDate_edit[0] = { id: " ", itemName: " " };
        this.selecteEffectiveEndDate_edit[0] = { id: " ", itemName: " " };

        this.isddlGeography_edit = false;
        this.isddlCountry_edit = false;
        this.isddlProduct_edit = false;
        this.isddlChannel_edit = false;
        this.isddlSegment_edit = false;
        this.isddlSupplychain_edit = false;

        this.btnRetrive_Edit = true;
        //this.btnClear_Edit = false;
        this.successMsg = '';

        this.selectedGeogrphyTreeData_label_edit = '';
        this.selectedProductTreeData_label_edit = '';
        this.selectedChennelTreeData_label_edit = '';
        this.selectedGeogrphyTreeData_edit = '';
        this.selectedProductTreeData_edit = '';
        this.selectedChennelTreeData_edit = '';

        this.isGeographyTree_edit = true;
        this.isChannelTree_edit = true;
        this.isProductTree_edit = true;

        this.isCountryDdlOpen_edit = true;
        this.isSegmentDdlOpen_edit = true;

        this.isProductTreeDisable_edit = false;
        this.isGeographyTreeDisable_edit = false;
        this.isChannelTreeDisable_edit = false;
    }

    ClearRoutingRule_edit() {
        this.successMsg = '';
        this.hideShowAlertMsg = false;
        this.selectedMainListSites_edit[0] = { id: "0", itemName: "--SELECT--" };
        this.txtSplitPercentage = '';
        this.selecteEffectiveBeginDate_edit[0] = { id: "0", itemName: " " };
        this.selecteEffectiveEndDate_edit[0] = { id: "0", itemName: " " };
        this.isddlMainSite_edit = false;
    }

    ClearRule_edit() {
        this.gridOptionsFulfillment_rules.api.deselectAll();
    }

    DeleteRule_edit() {
        var id:string ='';
        var singleDeleteFlag:string = "YES";
        var chkflag: boolean = true;

        let selectedRow = this.gridOptionsFulfillment_rules.api.getSelectedRows();
        if (selectedRow.length > 0) {

            if (selectedRow[0].hiddlenFlag == "Newval") {
                var x = [];
                for (var i = 0; i < this.rowData_rule.length; i++) {
                    if (!(selectedRow.find(x => x.hiddlenFlag == this.rowData_rule[i].hiddlenFlag && x.fulfillmentSiteId == this.rowData_rule[i].fulfillmentSiteId && x.effectiveBeginDate == this.rowData_rule[i].effectiveBeginDate && x.effectiveEndDate == this.rowData_rule[i].effectiveEndDate && x.hiddenRoutingId == this.rowData_rule[i].hiddenRoutingId))) {
                        x.push(this.rowData_rule[i]);
                    }
                }

                if (selectedRow.length > 0) {

                    this.rowData_rule = x;

                    this.gridApiFulfillment_rule.setRowData(this.rowData_rule);
                }
                var msg = 'Record Deleted Successfully.';
                this.DisplayMsg(msg, 'success');

            }
            else {
                id = selectedRow[0].hiddenRoutingId;
                this._fulfillmentSiteAssignmentServiceService.DeleteRoutingDetailsAndHeaders(singleDeleteFlag,id)
                    .subscribe(
                        (data: any) => {
                            if (data != null) {
                                data = data._body;
                                var res:string[] = data.split(',');
                                if (res != null && parseInt(res[0]) > 0 && parseInt(res[1]) > 0) {

                                    var x = [];
                                    for (var i = 0; i < this.rowData_rule.length; i++) {
                                        if (!(selectedRow.find(x => x.hiddlenFlag == this.rowData_rule[i].hiddlenFlag && x.fulfillmentSiteId == this.rowData_rule[i].fulfillmentSiteId && x.effectiveBeginDate == this.rowData_rule[i].effectiveBeginDate && x.effectiveEndDate == this.rowData_rule[i].effectiveEndDate && x.hiddenRoutingId == this.rowData_rule[i].hiddenRoutingId))) {
                                            x.push(this.rowData_rule[i]);
                                        }
                                    }
                                    if (selectedRow.length > 0) {
                                        this.rowData_rule = x;
                                        this.gridApiFulfillment_rule.setRowData(this.rowData_rule);
                                    }

                                    this.searchFullfillmentData('delete_rule');

                                    var msg = 'Record Deleted Successfully.';
                                    this.DisplayMsg(msg,'sucess');
                                }
                                else {
                                    var msg = 'Error occured while deleting the row. Try again.';
                                    this.DisplayMsg(msg, 'Fail');
                                }
                            }
                        },
                        err => console.log(err),
                    );
            }
        }
        else {

            var msg = 'Please select a row.';
            this.DisplayMsg(msg, 'Fail');
        }
    }

    DeleteSite_edit() {
        let selectedRow = this.gridOptionsFulfillmentSplits.api.getSelectedRows();

        if (selectedRow.length > 0) {

            var x = [];
            for (var i = 0; i < this.rowData_split.length; i++) {
                if (!(selectedRow.find(x => x.id == this.rowData_split[i].id))) {
                    x.push(this.rowData_split[i]);
                }
            }
            if (selectedRow.length > 0) {
                this.rowData_split = x;
                this.gridApiFulfillmentSplit.setRowData(this.rowData_split);
            }

            this.splitTotal = this.splitTotal - parseFloat(selectedRow[0].splitPercentage);
        }
        else {
            var errMsg = 'Please select a row.';

            this.DisplayMsg(errMsg, 'fail');
            return false;
        }
    }

    UpdateSiteInfo_edit() {
        this.clearErrorMsg();
        let selectedRow = this.gridOptionsFulfillmentSplits.api.getSelectedRows();

        if (selectedRow.length > 0) {
            let id = selectedRow[0].id;
            let site = selectedRow[0].site;
            let per = selectedRow[0].splitPercentage;

            let getVal = this.mainListSitesData_edit.find(i => i.itemName = site);

            this.selectedMainListSites_edit = [];

            this.selectedMainListSites_edit.push({ id: getVal.id, itemName: getVal.itemName });

            this.txtSplitPercentage = per;

            this.isddlMainSite_edit = true;
            this.updateTempSplitGridId = id;

            if (this.selectedMainListSites_edit.length > 0) {

                //call for check virtual site or not 
                let strsite = this.selectedMainListSites_edit[0].itemName;
                if (this.checkIsVirtualSite(strsite)) {
                    this.txtSplitPercentage = '100';
                    this.istxtSplit = true;
                }
                else {
                    this.istxtSplit = false;
                }

            }

        }
        else {
            var errMsg = 'Please select a row.';
            this.DisplayMsg(errMsg,'fail');
            return false;
        }
    }

    checkIsVirtualSite(site:any) {
        let isVirtual: boolean = false;
        this._fulfillmentSiteAssignmentServiceService.IsVirtualSite(site)
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        isVirtual = data;
                    }
                },
                err => console.log(err),
        );
        return isVirtual;
    }
    GetFSAFiscalCalendarInfo_BeginDate() {
        this.EffectiveBeginDateData_edit = [];

        this._fulfillmentSiteAssignmentServiceService.getFSAFiscalCalendarInfo_BeginDate()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.EffectiveBeginDateData_edit.push({ id: ' ', itemName: ' ' });
                        for (var i = 0; i < data.length; i++) {
                            this.EffectiveBeginDateData_edit.push({
                                id: data[i].id,
                                itemName: data[i].itemName 
                            });
                        }
                    }
                },
                err => console.log(err),
        );

        this.selecteEffectiveBeginDate_edit[0] = { id: ' ', itemName: ' ' };

    }


    AddUpdateRoutingRule_edit() {
        if (this.ValidateAddUpdateRule()) {
            if (this.updateTempSplitGridId != '') {
                let selectedRow = this.gridOptionsFulfillmentSplits.api.getSelectedRows();

                if (this.updateTempSplitGridId != '' && selectedRow.length > 0) {
                    let currentTotal = this.splitTotal - parseFloat(selectedRow[0].splitPercentage) + parseFloat(this.txtSplitPercentage);
                    if (currentTotal > 100) {
                        var msg = 'Split Percentage should be 100';
                        this.DisplayMsg(msg, 'Fail');
                        return false;
                    }
                    else {
                        for (var j = 0; j < this.rowData_split.length; j++) {
                            if (this.rowData_split[j].id == selectedRow[0].id) {
                                this.rowData_split[j].splitPercentage = this.txtSplitPercentage;
                            }
                        }
                        this.gridApiFulfillmentSplit.setRowData(this.rowData_split);
                        this.updateTempSplitGridId = '';
                    }

                }
            }
            else {
                var splitdata: SplitDetail;
                var count = 0;
                debugger;
                this.splitTotal = 0;

                if (this.rowData_split.length > 0) {
                    count = this.rowData_split.length + 1;

                }
                else {
                    count = 1;
                }

                for (var i = 0; i < this.rowData_split.length; i++) {
                    this.splitTotal += parseFloat(this.rowData_split[i].splitPercentage);
                }

                var siteName = this.selectedMainListSites_edit[0].itemName;
                var m = this.mode;
                var splitPer = this.txtSplitPercentage;
                splitdata = { id: count.toString(), site: siteName, splitPercentage: splitPer, mode: m }

                if ((this.splitTotal + parseFloat(splitdata.splitPercentage)) > 100) {
                    var msg = 'Split Percentage should be 100';
                    this.DisplayMsg(msg, 'Fail');
                    return false;
                }
                else {
                    this.splitTotal += parseFloat(splitdata.splitPercentage);
                    this.rowData_split.push(splitdata);

                    this.gridApiFulfillmentSplit.setRowData(this.rowData_split);

                    if (this.rowData_split.length > 0) {
                        this.isFulfillmentTempGrid = false;
                    }

                    if (this.rowData_split.length > 0 && this.splitTotal == 100) {
                        this.BindFulfillmentRuleGrid();
                        this.selecteEffectiveBeginDate_edit[0] = { id: "0", itemName: " " };
                        this.selecteEffectiveEndDate_edit[0] = { id: "0", itemName: " " };
                    }
                    this.selectedMainListSites_edit[0] = { id: "0", itemName: "--SELECT--" };
                    this.txtSplitPercentage = '';
                    //this.selecteEffectiveBeginDate_edit[0] = { id: "0", itemName: " " };
                    //this.selecteEffectiveEndDate_edit[0] = { id: "0", itemName: " " };
                }
            }
        }
        //else
        //{
        //    alert('no');
        //}
    }

    BindFulfillmentRuleGrid() {
        var count = 0;
        var siteName = '';
        var effectiveBeginDate = '';
        var effectiveEndDate = '';
        var beginDateValue = '';
        var endDateValue = '';
        var hiddenFlag ='Newval'
        if (this.rowData_rule.length > 0) {
            count = this.rowData_rule.length + 1;
        }
        else {
            count = 1;
        }

        for (var i = 0; i < this.rowData_split.length; i++) {
            if (siteName == '') {
                siteName = this.rowData_split[i].site + '(' + this.rowData_split[i].splitPercentage +')';
            }
            else {
                siteName += ', ' + this.rowData_split[i].site + '(' + this.rowData_split[i].splitPercentage + ')';
            }
        }

        if (this.selecteEffectiveBeginDate_edit.length > 0 && this.selecteEffectiveBeginDate_edit[0].id !="0") {
            effectiveBeginDate = this.selecteEffectiveBeginDate_edit[0].itemName; 
            beginDateValue = this.selecteEffectiveBeginDate_edit[0].id;
        }

        if (this.selecteEffectiveEndDate_edit.length > 0 && this.selecteEffectiveEndDate_edit[0].id != "0") {
            effectiveEndDate = this.selecteEffectiveEndDate_edit[0].itemName;
            endDateValue = this.selecteEffectiveEndDate_edit[0].id;
        }

        var fulfillmentRuleData = { hiddenRoutingId: ' ', fulfillmentSiteId: siteName, effectiveBeginDate: effectiveBeginDate, effectiveEndDate: effectiveEndDate, beginDateValue: beginDateValue,endDateValue:endDateValue, hiddlenFlag:hiddenFlag} 

        this.rowData_rule.push(fulfillmentRuleData);

        this.gridApiFulfillment_rule.setRowData(this.rowData_rule);

        if (this.rowData_rule.length > 0) {
            this.isFulfillmentRuleGrid = false;

            this.isFulfillmentRuleGrid = false;
            this.splitTotal = 0;
            this.rowData_split = [];
        }
    }
    
    GetFSAFiscalCalendarInfo_EndDate() {
        this.EffectiveEndDateData_edit = [];

        this._fulfillmentSiteAssignmentServiceService.getFSAFiscalCalendarInfo_EndDate()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.EffectiveEndDateData_edit.push({ id: ' ', itemName: '' });
                        for (var i = 0; i < data.length; i++) {
                            this.EffectiveEndDateData_edit.push({
                                id: data[i].id,
                                itemName: data[i].itemName 
                            });
                        }
                    }
                },
                err => console.log(err),
        );

        this.selecteEffectiveEndDate_edit[0] = { id: ' ', itemName: ' ' };
    }


    FillMasterSiteDDL() {
        debugger;
        this.mainListSitesData_edit = [];

        var ddlMainSiteInputParams: MainSiteDDLInputParams = new MainSiteDDLInputParams();
        var selectedCountry: DropDown = new DropDown();
        var selectedGeo: DropDown = new DropDown();

        selectedCountry.Id = this.selectedCountry_edit[0].id;
        selectedCountry.ItemName = this.selectedCountry_edit[0].itemName;

        selectedGeo.Id = this.selectedGeogrphy_edit[0].id;
        selectedGeo.ItemName = this.selectedGeogrphy_edit[0].itemName;

        ddlMainSiteInputParams.selectedCountry = selectedCountry;
        ddlMainSiteInputParams.selectedGeography = selectedGeo;
        ddlMainSiteInputParams.selectedGeoTreeNodeValue = this.selectedGeogrphyTreeData_edit;

        this._fulfillmentSiteAssignmentServiceService.GetNonVirtualSites(ddlMainSiteInputParams)
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.mainListSitesData_edit.push({ id: '0', itemName: '--SELECT--' });
                        for (var i = 0; i < data.length; i++) {
                            this.mainListSitesData_edit.push({
                                id: i,
                                itemName: data[i]
                            });
                        }
                    }
                },
                err => console.log(err),
        );

        this.selectedMainListSites_edit[0] = { id: "0", itemName: "--SELECT--" };

    }


    getChannelLevel(channelId:string) {
        var channelLevel: string = '';
        this._fulfillmentSiteAssignmentServiceService.GetChannelType(channelId)
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.lstChannelType_edit = [];
                        this.lstChannelType_edit = data;
                        channelLevel = this.lstChannelType_edit[0];
                    }
                },
                err => console.log(err),
            );
        return channelLevel;
    }

    getRegionName(country: string) {
        var regionName: string = '';
        this._fulfillmentSiteAssignmentServiceService.getRegionName(country)
            .subscribe(
            (data: any) => {
                debugger;
                    if (data != null) {
                        regionName = data._body;
                    }
                },
                err => console.log(err),
            );
        return regionName;
    }

    ValidateAddUpdateRule() {
        
        var errMsg = '';
        var iserror: boolean = false;

        if (this.selectedMainListSites_edit.length ==0 || this.selectedMainListSites_edit[0].itemName == '--SELECT--' || this.txtSplitPercentage == '' || this.txtSplitPercentage =='0') {
            errMsg = 'Enter the Required fields.'
            iserror = true;
        }

        if (iserror) {
            this.DisplayMsg(errMsg, 'Fail');
            return false;
        }
        else {
            return true;
        }
    }

    NumericValidater(event: any) {
        
        var regexp = '^^[0-9,\.?]$';
        var txtValue = '';
        if (!event.key.toString().match(regexp)) {
            txtValue = this.txtSplitPercentage.replace(event.key.toString(), ''); 
            this.txtSplitPercentage = txtValue;
        }
    }

    windowScrollup() {
        window.scroll(0,0);
    //or document.body.scrollTop = 0;
    //or document.querySelector('body').scrollTo(0,0)
    //...
    }
}

function loadJavaScript() {
    var url = '/dist/grid-multi-select.js';
    var s = document.createElement('script');
    s.setAttribute('src', url);
    document.head.appendChild(s);
}





