﻿import { Component, OnInit } from '@angular/core';
import { GridOptions } from 'ag-grid';

import { L10ItemListService } from '../../services/l10-item-list.service';
import { UserRoles } from '../../Model/user-roles';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
        selector: 'app-l10-item-list',
        templateUrl: './l10-item-list.component.html',
        styleUrls: ['./l10-item-list.component.css']
        //encapsulation: ViewEncapsulation.None
}) 
export class L10ItemList implements OnInit {
    isActiveScreen: boolean = true;

    //maxRowSizeImportandExport: number = 5000;
    maxRecords = 10000;
    colorCode: string;
    successMsg: string = "";
    public hideShowAlertMsg = false;

    //Filter section
    IsFilterViewResultBtnDisable = false;
    IsFilterClearBtnDisable = false;
    itemIdInput = "";
    commodity_code = [];
    selected_items = [];
    dropdownSettings = {};
    itemDescInput = "";

    
    private gridOptionsName: GridOptions;
    private contextName;
    private componentsName;
    private rowDataName;
    private gridApiName;
    private frameworkComponentsName;
    private gridColumnApiName;
    gridOn = false;
    exportClicked = false;
    getGridDataParam: l10Item;
    changedRows: any[] = [];
    excelExportChangedRows: any[] = [];
    gridBtnClear = false;
    private components;
    getItemClass: any[] = [];
    private isBtnSave = false;

    //Roles
    userRoles: UserRoles[];
    screenId = "3002";
    isUserHavingWriteAccess: boolean = false;
    //isActiveScreen: boolean = true;
    userHavingWriteAccess: string[];
    listOfRoleHavingAccess: string[];
    userTenantTypeAccessList: string[];
    isUserHavingUserMaintenance: boolean = false;

    constructor(private _l10ItemListService: L10ItemListService, private _router: Router, private _sessionSt: SessionStorageService) {
        this.Authorization();
        this.gridOptionsName = <GridOptions>{
            contextName: {
                componentParentName: this
            },
        };
        this.gridOptionsName.columnDefs = [
            {
                headerName: "Item Id",
                field: "id",
                suppressSizeToFit: false,
                //tooltipField: "lob_Id",
                editable: false,
                colId: "id"
            },
            {
                headerName: "Name",
                field: "description",
                suppressSizeToFit: false,
                //tooltipField: "lob_Desc",
                editable: false,
                colId: "description"
            },
            {
                headerName: "Mark L10",
                field: "l10Class",
                suppressSizeToFit: false,
                //width: 180,
                //tooltipField: "brand_Id",
                editable: function (params) {
                    if (!params.context.componentParent.isUserHavingWriteAccess) {
                        return false;
                    } else {
                        return true;
                    }
                },
                colId: "l10Class",
                cellClass: 'multi-select-grid',
                cellEditor: "getItemClass"

            },
            {
                headerName: "Last Modified By",
                field: "lastModifiedBy",
                suppressSizeToFit: false,
                tooltipField: "updatedBy",
                editable: false,
                colId:"lastModifiedBy"

            },
            {
                headerName: "Last Modified Date",
                field: "lastModifiedDate",
                suppressSizeToFit: false,
                //tooltipField: "updatedDate",
                editable: false,
            },

        ];
        this.components = {
            getItemClass: getItemClass()
        }
        this.gridOptionsName.headerHeight = 18;
        this.gridOptionsName.rowSelection = "multiple";
        this.gridOptionsName.paginationPageSize = 20;
        this.gridOptionsName.defaultColDef = {
            editable: true,
            enableValue: true
        };
        this.gridOptionsName.rowData = [];
        this.contextName = { componentParent: this };
        this.componentsName = {};        
    }


    ngOnInit(): void {

        this.dropdownSettings = {
            singleSelection: true,
            text: "Select Commodity Code",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class singleSelect"
        };

        //this.getCommodityCode();
        //this.getGridData({
        //    itemId: "",
        //    commodityCode: "",
        //    itemDescription: "",
        //    multiTier: false,
        //    updatedBy: null,
        //    updatedDate: null
        //});
        this.ClearMessage();
        this.getItemClass.push({
            id: "Select...",
            itemName: "Select..."
        },
        {
            id: "L10_MOD",
            itemName: "L10_MOD"
        });
    }

    Authorization() {
        var rolesHavingWriteAccess = this._sessionSt.retrieve('screenAuthorization') != null ? this._sessionSt.retrieve('screenAuthorization') : [];
        this.userHavingWriteAccess = rolesHavingWriteAccess[rolesHavingWriteAccess.findIndex(x => x.functionId == this.screenId)]["roleId"].split(',');
        this.userTenantTypeAccessList = this._sessionSt.retrieve('userTenantTypeAccessList') != null ? this._sessionSt.retrieve('userTenantTypeAccessList') : [];

        if (this._sessionSt.retrieve('userRoles') === null) {
            this.isActiveScreen = false;
            this._router.navigateByUrl('/errorMessage');
            return;
        }
        else {
            this.userRoles = this._sessionSt.retrieve('userRoles');
            for (var i = 0; i < this.userRoles.length; i++) {
                if (this.userHavingWriteAccess.includes(this.userRoles[i].roleId.toString())) {
                    this.isUserHavingWriteAccess = true;
                    return;
                }
            }

            if (!(this.isUserHavingWriteAccess || this.userTenantTypeAccessList.includes("GMP"))) {
                this.isActiveScreen = false;
                this._router.navigateByUrl('/errorMessage');
                return;
            }
        }
    }
    
    onItemSelect(item: any) {
        console.log(item);
        console.log(this.selected_items);
    }
    OnItemDeSelect(item: any) {
        console.log(item);
        console.log(this.selected_items);
    }
    onSelectAll(items: any) {
        console.log(items);
    }
    onDeSelectAll(items: any) {
        console.log(items);
    }

    //Getl10ItemListDetails(events: any) {
    //    this.ClearMessage();
    //    console.log(this.itemIdInput);
    //    console.log(this.itemDescInput);
        
    //    this.getGridDataParam = {
    //        Id: this.itemIdInput.toUpperCase(),
    //        Description: this.itemDescInput.toUpperCase(),
    //        Class: null,
    //        LastModifiedBy: null,
    //        LastModifiedDate: null,
            
    //    };

    //    this.getGridData(this.getGridDataParam);

    //}
    
    Getl10FilterViewResult(events: any) {
        //debugger;
        this.ClearMessage();
        console.log(this.itemIdInput);
        console.log(this.itemDescInput);
        //debugger;
        this.getGridDataParam = {
            id: this.itemIdInput.trim(),
            description: this.itemDescInput.trim() == null ? "" : this.itemDescInput.toUpperCase(),
            l10Class: null,
            lastModifiedBy: null,
            lastModifiedDate: null,            
        };

        this.getGridData(this.getGridDataParam);
    }

    getGridData(filter: any) {
        this._l10ItemListService.Getl10FilterViewResult(filter)
            .map((data: any) => data.json()).subscribe((data: any) => {
                console.log(data);
                if (data.length > 0) {
                    if (data.length > this.maxRecords) {
                        if (!this.exportClicked == true) {
                            var alertMsg = "Only " + this.maxRecords + " records are shown in the grid";
                            window.alert(alertMsg);
                        }
                    }
                    this.gridOn = true;
                    this.rowDataName = data.slice(0, this.maxRecords);
                    //if (this.exportClicked == true) {
                    //    this.exportClicked = false;
                    //    this.GenerateExcelName(this.rowDataName.filter(x => x.l10Class == "L10_MOD"));
                    //}
                } else {
                    console.log("No data to display");
                }
            },
                err => console.log(err))
    }

    oncellEditingStarted(event: any) {
        //debugger;
        event.node.setSelected(true);
        console.log('Selection Changed!!');
        event.context.componentParent.gridBtnClear = true;
        
        let nodeData = event.node.data;        
        let selectedRow = 
            {
            id: nodeData.id,
            description: nodeData.description,
            l10Class: nodeData.l10Class == "L10_MOD" ? "" :"L10_MOD",            
            lastModifiedBy: nodeData.lastModifiedBy,
            lastModifiedDate: nodeData.lastModifiedDate
        };
        let index = event.context.componentParent.changedRows.findIndex(x => x.itemId == selectedRow.id);
        if (index == -1) {
            event.context.componentParent.changedRows.push(selectedRow);

        } else {            
            event.context.componentParent.changedRows.splice(index, 1);
        }
    }

    onGridReadyName(params) {
        this.gridApiName = params.api;
        this.gridColumnApiName = params.columnApi;
        this.gridOptionsName.api.sizeColumnsToFit();
        
    }

    onSelectionChanged(event: any) {
        let rowsSelection = this.gridOptionsName.api.getSelectedRows();
        if (rowsSelection.length > 0) {
            console.log(rowsSelection.values);
        }
        else {
            
        }
    }

    onClickBtnExcelExport(event: any) {
        var getGridDataParam = {
            id: this.itemIdInput.trim() == null ? "" : this.itemIdInput.trim(),
            description: this.itemDescInput.trim() == null ? "" : this.itemDescInput.toUpperCase(),
            l10Class: null,
            lastModifiedBy: null,
            lastModifiedDate: null,
        };
        this._l10ItemListService.Getl10FilterViewResultExcel(getGridDataParam)
            .map((data: any) => data.json()).subscribe((data: any) => {
                console.log(data);
                if (data.length > 0) {
                    this.exportClicked = true;
                    this.GenerateExcelName(data);          
                } else {
                    this.DisplayMessage("Data in grid is blank", false);
                }
            },
                err => console.log(err))        
    }

    onClickBtnSave(event: any) {

        var alertMsg = "Are you sure, you want to save?";
        if (window.confirm(alertMsg)) {
            //debugger;
            this.ClearMessage();
            this.gridApiName.stopEditing();

            let rowsSelection = JSON.parse(JSON.stringify(this.changedRows));

            this._l10ItemListService.Updatel10ItemListDetails(rowsSelection)
                .subscribe(
                    (data: any) => {
                        //debugger;
                        var returnCode = data._body.split('~')[0];
                        //var returnMsg = data._body.split('~')[1];
                        this.hideShowAlertMsg = true;

                        if (returnCode == "Data has been Updated Successfully") {
                            this.DisplayMessage("Records successfully Updated.", true);
                            this.gridOptionsName.api.deselectAllFiltered();
                            //this.isBtnSave = true;
                            this.getGridData(this.getGridDataParam);
                        }
                        else {
                            this.DisplayMessage("Record Updation Failed!!", false);

                        }
                    },
                    err => console.log(err), // error
                );
            //this.excelExportChangedRows = this.changedRows;
            this.changedRows = [];
        } 
    }

    GenerateExcelName(rowData: any[]) {

        this.ClearMessage();
        let param = [];
        for (var i = 0; i < rowData.length; i++) {
            param.push({
                ["ITEM ID"]: rowData[i].id,
                ["NAME"]: rowData[i].description,
                ["ITEM CLASS"]: rowData[i].l10Class,
                ["LAST MODIFIED BY"]: rowData[i].lastModifiedBy,                
                ["LAST MODIFIED DATE"]: rowData[i].lastModifiedDate,
            })
        }
        //this.changedRows = [];
        this._l10ItemListService.MultiTierExcelExport(param);
    }

    //clear filters
    clearFilterData() {
        this.ClearMessage();
        this.commodity_code = [];
        this.selected_items = [];
        this.itemIdInput = "";
        this.itemDescInput = "";
    }

    //clear error/success message
    ClearMessage() {
        this.hideShowAlertMsg = false;
        this.successMsg = "";
    }

    DisplayMessage(msg, isSuccess) {
        this.hideShowAlertMsg = true;
        this.successMsg = msg;
        this.colorCode = isSuccess == true ? "success" : "danger";
    }

    clearGridDataName(event: any) {
        this.gridBtnClear = false;
        this.getGridData(this.getGridDataParam);
        this.ClearMessage();

    }


}

function loadJavaScript() {
    var url = '/dist/grid-multi-select.js';
    var s = document.createElement('script');
    s.setAttribute('src', url);
    document.head.appendChild(s);
} 

function getItemClass() {
    function MultiSelectDropDown() { }

    MultiSelectDropDown.prototype.getGui = function () {
        return this.eGui;
    };
    MultiSelectDropDown.prototype.getValue = function () {
        var arr = [];
        for (var i = 0; i < this.eGui.firstChild.selectedOptions.length; i++) {

            var selectedItemValueGridDD = this.eGui.firstChild.selectedOptions[i].text;
            arr.push(selectedItemValueGridDD);
            var commaValues = arr.join(",");
        }
        this.value = commaValues;
        return this.value;
    };
    MultiSelectDropDown.prototype.isPopup = function () {
        return true;
    };
    MultiSelectDropDown.prototype.search = function () {
        console.log(this);
    }
    MultiSelectDropDown.prototype.afterGuiAttached = function () {
        var changedWidth = this.myDDWid + "px";
        this.eGui.parentElement.style.width = changedWidth;
    };
    MultiSelectDropDown.prototype.init = function (params) {
        //debugger;
        var multiDD: any = [];
        multiDD = params.context.componentParent.getItemClass;
        var myarr = params.value.toString().split(",");

        this.myDDWid = params.column.actualWidth;

        var tempElement = document.createElement("div");
        tempElement.setAttribute('id', 'multiSel');
        tempElement.setAttribute('l10Class', 'multiselect singleSelect');

        var tempElementDiv = document.createElement("select");
        tempElementDiv.setAttribute('l10Class', 'multiselect-ui custom-select form-control');
        tempElementDiv.setAttribute('id', 'multiselect-ui');

        for (var i = 0; i < multiDD.length; i++) {
            var option = document.createElement("option");
            option.setAttribute("value", multiDD[i].id);
            option.text = multiDD[i].itemName;
            for (var j = 0; j < myarr.length; j++) {
                if (multiDD[i].id === myarr[j]) {
                    option.setAttribute("selected", "selected");
                }
            }
            if (option.text == "Select...") {
                option.setAttribute("selected", "selected");
            }
            tempElementDiv.appendChild(option);
            tempElement.appendChild(tempElementDiv);
        }
        loadJavaScript();
        this.eGui = tempElement;
    };
    return MultiSelectDropDown;
}



export interface l10Item {
    'id': string,
    'description': string,
    'lastModifiedBy': string,
    'l10Class': string,
    'lastModifiedDate': string
}