import { Component, OnInit } from '@angular/core';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { UserMaintenanceService } from '../../services/user-maintenance.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserRoles } from '../../Model/user-roles';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {

    userRoles: UserRoles[] = [];
    isUserHavingWriteAccess: boolean = false;
    constructor(private _router: Router, private _sessionSt: SessionStorageService, private _userMaintenanceService: UserMaintenanceService)
    {
       //todo:uncomment after login this.GetUserRoles();
    }
    
  ngOnInit(): void {
      if (this._sessionSt.retrieve('userRoles') !== null) {
          this.isUserHavingWriteAccess = true;
      }
  }
  GetUserRoles() {    
      if (this._sessionSt.retrieve('userRoles') === null) {
          this._userMaintenanceService.GetUserRoles()
              .map((data: any) => data.json())
              .subscribe(
              (data: any) => {
                  if (data === null || data === "") {
                      this._router.navigateByUrl('/errorMessage');
                  }
                  else {
                      for (var i = 0; i < data.length; i++) {
                          this.isUserHavingWriteAccess = true;
                          this.userRoles.push(data[i])
                      }
                      this._sessionSt.store('userRoles', this.userRoles);
                  }
              },
              err => console.log(err),
          );
     }
  }

}

