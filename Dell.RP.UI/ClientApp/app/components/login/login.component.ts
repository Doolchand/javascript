import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { UserRoles } from '../../Model/user-roles';
import { UserMaintenanceService } from '../../services/user-maintenance.service';
import { ScreenAuthorization } from '../../Model/screen-authorization';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { environment } from '../../../environments/environment';
declare var jQuery: any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [LoginService]
})


export class LoginComponent implements OnInit {
    url: string = environment.URL;
    idleState;
    idleStateMsg;
    stateColor;
    router;
    tokenID;

    timedOut = false;
    hideMsg = false;  
    userName: string;
    userDomain: string;
    userTenantTypeAccessList: string[];  

    @ViewChild('idleModal') idleModal: ElementRef;
    @ViewChild('idleModalBtn') idleModalBtn: ElementRef;
    userRoles: UserRoles[];
    screenAuthorization: ScreenAuthorization[];  

    constructor(private _router: Router, private idle: Idle, private _loginService: LoginService, private _localSt: LocalStorageService, private _sessionSt: SessionStorageService, private _userMaintenanceService: UserMaintenanceService) {

        //todo:uncomment after login

        this.router = _router;
        this.userRoles = [];
        this.screenAuthorization = [];
        this.userDomain = this._sessionSt.retrieve('domainName');
        this.userName = this._sessionSt.retrieve('userID');
        this.tokenID = this._sessionSt.retrieve('tokenID');

        //If SESSION is NEW 
        if ((this.userDomain === null && this.userName === null && this.tokenID === null) || (this.userDomain === "" && this.userName === "" && this.tokenID === "")) {
            this.getUserDetails();
        }

        if (this._sessionSt.retrieve('screenAuthorization') === null) {
            this.GetScreenAuthorizationList();
        }
        if (this._sessionSt.retrieve('RolesCanAccessScreen') === null) {
            this.GetRolesCanAccessScreenList();
        }

        if (this._sessionSt.retrieve('userRoles') === null) {
            this.GetUserRoles();
        }

        this.getSessionSetting(idle);
    }
    getSessionSetting(idle) {    
        this._loginService.GetSessionSetting()
            .subscribe(
            (data: any) => {              
                var setIdleTime = parseInt(data._body.split("~")[0]);
                var setTimeout = parseInt(data._body.split("~")[1]);
                 this.SessionInit(idle, setIdleTime, setTimeout);
                },
                err => console.log(err),
        );
    }
    SessionInit(idle, setIdleTime, setTimeout) {
         // sets an idle timeout of n seconds, for testing purposes.
        idle.setIdle(setIdleTime);
        // sets a timeout period of n seconds. after n seconds of inactivity, the user will be considered timed out.
        idle.setTimeout(setTimeout);
        // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
        idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

        idle.onIdleEnd.subscribe(() => {
            this.stateColor = 'success';
            this.hideMsg = false;
            jQuery("#myModal").modal("hide");
        });

        idle.onTimeout.subscribe(() => {
            this.idleState = 'closed';
            this.stateColor = 'danger';
            this.idleStateMsg = "Session Expired...";
            this.timedOut = true;
            this.hideMsg = true;
            jQuery('#myModal').modal({ backdrop: 'static', keyboard: false });
            this.updateSessionData(this.idleState);
            this.clearSession();
        });

        idle.onIdleStart.subscribe(() => {
            this.stateColor = 'warning';
            let el: HTMLElement = this.idleModalBtn.nativeElement as HTMLElement;
            el.click();
        });

        idle.onTimeoutWarning.subscribe((countdown) => {
            this.idleState = 'Idle';
            this.idleStateMsg = 'Session Will Get Expire in ' + countdown + ' seconds!';
            this.hideMsg = true;
        });
        this.reset(0);
    }

    getBaseUrl() {
        var pathArray = location.href.split('/');
        var protocol = pathArray[0];
        var host = pathArray[2];
        var url = protocol + '//' + host;
        return url;
    }

    ngOnInit() {
    }
    reset(fromPath) {
        this.idle.watch();
        this.idleState = 'active';
        this.stateColor = 'success';
        this.timedOut = false;
        this.hideMsg = false;
        this.updateSessionData(this.idleState);
        if (fromPath === 1) {
            window.location.reload();
        }
    }
    updateSessionData(idleState) {      
        var param = {
            tokenID: this.tokenID,
            state: idleState,
            applicationUrl: this.getBaseUrl(),
            loggedPage: "Home Page"
        }    
        this._loginService.updateSession(param).subscribe((data) => {           
        });
    }

    clearSession() {
        this._sessionSt.clear();
    }

    getUserDetails() {      
        this._loginService.GetUserData()
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {                  
                    this.userName = data.userID;
                    this.userDomain = data.domainName;
                    this._sessionSt.store('domainName', this.userDomain);

                    this._sessionSt.store('userID', this.userName);
                    this.tokenID = this.generateToken(20);
                    this._sessionSt.store('tokenID', this.tokenID);
                },
                err => console.log(err),
        );
    }

    generateToken(len) {
        var arr = new Uint8Array((len || 40) / 2)
        window.crypto.getRandomValues(arr)
        return Array.from(arr, dec2hex).join('');
    }
    GetUserRoles() {
        this._userMaintenanceService.GetUserRoles()
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    if (data === null || data === "") {
                        this.router.navigateByUrl('/errorMessage');
                    }
                    else {
                        for (var i = 0; i < data.length; i++) {
                            this.userRoles.push(data[i])
                        }
                        if (this.userRoles.length > 0) {
                            this.userTenantTypeAccessList = this.userRoles.map(item => item.userType)
                                .filter((value, index, self) => self.indexOf(value) === index)
                        }
                        this._sessionSt.store('userTenantTypeAccessList', this.userTenantTypeAccessList);
                        this._sessionSt.store('userRoles', this.userRoles);
                    }
                },
                err => console.log(err),
        );
    }
    GetScreenAuthorizationList() {
        this._userMaintenanceService.GetScreenAuthorizationList()
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    for (var i = 0; i < data.length; i++) {
                        this.screenAuthorization.push(data[i])
                    }
                    this._sessionSt.store('screenAuthorization', this.screenAuthorization);
                },
                err => console.log(err),
        );
    }
    GetRolesCanAccessScreenList() {
        this._userMaintenanceService.GetRolesCanAccessScreenList()
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    for (var i = 0; i < data.length; i++) {
                        this.screenAuthorization.push(data[i])
                    }
                    this._sessionSt.store('RolesCanAccessScreen', this.screenAuthorization);
                },
                err => console.log(err),
        );
    }
}
// dec2hex :: Integer -> String
function dec2hex(dec) {
    return ('0' + dec.toString(16)).substr(-2)
}

