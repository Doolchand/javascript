﻿import { Component, AfterViewInit, ViewChild, ElementRef } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
@Component({
    selector: 'checkbox-cell',
    template: `<input type="checkbox" [checked]="params.value" (change)="onChange($event)">`,
})

export class CheckboxCommodity implements ICellRendererAngularComp {

    @ViewChild('.checkbox') checkbox: ElementRef;

    public params: any;
    private multiTier: boolean = false;


    agInit(params: any): void {

        
        this.params = params;
        if (params.colDef.field == "markFlag") {

                        
            //this.multiTier = true;

            var getItem = params.context.componentParent.rowDataName;

            ////var getItem = params.context.componentParent.FamilyParentGridList;
            for (let i = 0; i < getItem.length; i++) {
                if (getItem[i].markFlag === true) {
                    //this.isHavingWriteAccess = false;
                    this.multiTier = true;
                }
            }


        }

    }

    constructor() { }

    refresh(): boolean {
        return false;
    }

    public onChange(event: any) {

        this.params.data[this.params.colDef.field] = event.currentTarget.checked;

    }
}