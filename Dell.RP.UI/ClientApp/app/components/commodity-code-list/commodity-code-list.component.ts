﻿import { Component, OnInit } from '@angular/core';
import { GridOptions } from 'ag-grid';

import { CheckboxCommodity } from './checkbox-commodity.component'
import { CommodityCodeListService } from '../../services/commodity-code-list.service';
import { UserRoles } from '../../Model/user-roles';
import { SessionStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

@Component({
        selector: 'app-commodity-code-list',
        templateUrl: './commodity-code-list.component.html',
        styleUrls: ['./commodity-code-list.component.css']
        //encapsulation: ViewEncapsulation.None
}) 
export class CommodityCodeList implements OnInit {
    //isActiveScreen: boolean = true;

    maxRowSizeImportandExport: number = 5000;
    colorCode: string;
    successMsg: string = "";
    public hideShowAlertMsg = false;

    //Filter section
    IsFilterViewResultBtnDisable = false;
    IsFilterClearBtnDisable = false;
    commodityIdInput = "";
    commodityNameInput = "";

    //Multi Tier BOM grid
    private gridOptionsName: GridOptions;
    private contextName;
    private componentsName;
    private rowDataName;
   // private rowDataName_temp = [];
    private gridApiName;
    private frameworkComponentsName;
    private gridColumnApiName;
    getGridDataParam: commodityCodeList;
    changedRows: any[] = [];
    gridBtnClear = false;
    gridBtnExcelExportFilter = false;



    //Roles
    userRoles: UserRoles[];
    screenId = "3004";
    isUserHavingWriteAccess: boolean = false;
    isActiveScreen: boolean = true;
    userHavingWriteAccess: string[];
    listOfRoleHavingAccess: string[];
    userTenantTypeAccessList: string[];
    isUserHavingUserMaintenance: boolean = false;

    constructor(private _CommodityCodeListService: CommodityCodeListService, private _router: Router, private _sessionSt: SessionStorageService) {
        this.Authorization();

        this.gridOptionsName = <GridOptions>{
            contextName: {
                componentParentName: this
            },
        };
        this.gridOptionsName.columnDefs = [
            {
                headerName: "COMMODITY ID",
                field: "commodityId",
                suppressSizeToFit: false,
                tooltipField: "lob_Id",
                editable: false,
                colId: "commodityId",
            },
            {
                headerName: "COMMODITY NAME",
                field: "commodityName",
                suppressSizeToFit: false,
                //width: 180,
                tooltipField: "brand_Id",
                editable: false,
                colId: "commodityName"

            },
            {
                headerName: "MARK FLAG",
                field: "markFlag",
                cellClass: "checkBox",
                editable: false,
                suppressSizeToFit: false,
                cellRenderer: "checkboxCommodity",
                tooltipField: "markFlag",
                onCellClicked: this.oncellEditingStarted
            },
            {
                headerName: "LAST MODIFIED BY",
                field: "updatedBy",
                suppressSizeToFit: false,
                tooltipField: "updatedBy",
                editable: false,

            },
            {
                headerName: "LAST MODIFIED DATE",
                field: "updatedDate",
                suppressSizeToFit: false,
                tooltipField: "updatedDate",
                editable: false,

            },

        ];
        this.gridOptionsName.headerHeight = 18;
        //this.gridOptionsName.rowSelection = "multiple";
        this.gridOptionsName.paginationPageSize = 20;
        this.gridOptionsName.defaultColDef = {
            editable: true,
            enableValue: true
        };
        this.gridOptionsName.rowData = [];
        this.contextName = { componentParent: this };
        this.componentsName = {};
        this.frameworkComponentsName = {
            checkboxCommodity: CheckboxCommodity,
        };
    }


    ngOnInit(): void {

        this.getGridData({
            commodityId: "",
            commodityName: "",
            markFlag: false,
            updatedBy: null,
            updatedDate: null
        },'search');
        this.ClearMessage();
    }

    Authorization() {
        debugger;
        var rolesHavingWriteAccess = this._sessionSt.retrieve('screenAuthorization') != null ? this._sessionSt.retrieve('screenAuthorization') : [];
        this.userHavingWriteAccess = rolesHavingWriteAccess[rolesHavingWriteAccess.findIndex(x => x.functionId == this.screenId)]["roleId"].split(',');
        this.userTenantTypeAccessList = this._sessionSt.retrieve('userTenantTypeAccessList') != null ? this._sessionSt.retrieve('userTenantTypeAccessList') : [];

        if (this._sessionSt.retrieve('userRoles') === null) {
            this.isActiveScreen = false;
            this._router.navigateByUrl('/errorMessage');
            return;
        }
        else {
            this.userRoles = this._sessionSt.retrieve('userRoles');
            for (var i = 0; i < this.userRoles.length; i++) {
                debugger;
                if (this.userHavingWriteAccess.includes(this.userRoles[i].roleId.toString())) {
                    this.isUserHavingWriteAccess = true;
                    return;
                }
            }

            if (!(this.isUserHavingWriteAccess || this.userTenantTypeAccessList.includes("GMP"))) {
                this.isActiveScreen = false;
                this._router.navigateByUrl('/errorMessage');
                return;
            }
        }
    }

    GetFilterViewResult(events: any) {
        this.ClearMessage();
        console.log(this.commodityIdInput);
        console.log(this.commodityNameInput);
        this.getGridDataParam = {
            commodityId: this.commodityIdInput,
            commodityName: this.commodityNameInput,
            markFlag: false,
            updatedBy: null,
            updatedDate: null
        };

        this.getGridData(this.getGridDataParam,'search');
        
    }

    GetFilterViewResult_edit() {
        this.getGridDataParam = {
            commodityId: this.commodityIdInput,
            commodityName: this.commodityNameInput,
            markFlag: false,
            updatedBy: null,
            updatedDate: null
        };

        this.getGridData(this.getGridDataParam,'search');

    }

    getGridData(filter: any,mode:string) {
        
        this.rowDataName = [];
        //var rowDataName_temp1 = [];
        this._CommodityCodeListService.GetFilterViewResult(filter)
            .map((data: any) => data.json()).subscribe((data: any) => {
                if (data.length > 0) {
                    
                    this.rowDataName = data;
                    if (mode == 'export') {
                        if (data.length > this.maxRowSizeImportandExport) {
                            this.DisplayMessage("Can't export more than " + this.maxRowSizeImportandExport + " Records.", true);
                            this.colorCode = "danger";
                        }
                        else {

                            this.GenerateExcelName(data);
                        }
                    }
                    //rowDataName_temp1 = data;
                    //this.rowDataName_temp = data;
                } else {
                    console.log("No data to display");
                }
            },
                err => console.log(err))
    }

    oncellEditingStarted(event: any) {

        
        event.node.setSelected(true);
        console.log('checkbox clicked!');
        event.context.componentParent.gridBtnClear = true;
        
        let nodeData = event.node.data;
        let selectedRow = 
            {
            commodityId: nodeData.commodityId,
            commodityName: nodeData.commodityName,
            markFlag: nodeData.markFlag,
            updatedBy: nodeData.updatedBy,
            updatedDate: nodeData.updatedDate
        };
        let index = event.context.componentParent.changedRows.findIndex(x => x.commodityId == selectedRow.commodityId);
        if (index == -1) {
            event.context.componentParent.changedRows.push(selectedRow);

        } else {            
            event.context.componentParent.changedRows.splice(index, 1);
        }
    }

    //onCellValueChanged(event: any) {
    //    alert();
    //}

    onGridReadyName(params) {
        this.gridApiName = params.api;
        this.gridColumnApiName = params.columnApi;
        this.gridOptionsName.api.sizeColumnsToFit();
        
    }

    OnFilterChangeName(event: any) {
        if (this.gridApiName.filterManager.advancedFilterPresent) {
            this.gridBtnExcelExportFilter = true;
            this.gridApiName.stopEditing();
        }
        else {
            this.gridBtnExcelExportFilter = false;
        }

    }

    onSelectionChanged(event: any) {
        let rowsSelection = this.gridOptionsName.api.getSelectedRows();
        if (rowsSelection.length > 0) {
            console.log(rowsSelection.values);
        }
        else {
            
        }
    }

    onClickBtnExcelExport(event: any) {
        //ExportExcel(); 
        this.getGridData({
            commodityId: "",
            commodityName: "",
            markFlag: false,
            updatedBy: null,
            updatedDate: null
        }, 'export');
        //this.getGridData();
    }

    //ExportExcel() {
    //    if (this.rowDataName.length > this.maxRowSizeImportandExport) {
    //        this.DisplayMessage("Can't export more than " + this.maxRowSizeImportandExport + " Records.", true);
    //        this.colorCode = "danger";
    //    }
    //    else {

    //        this.GenerateExcelName(this.rowDataName);
    //    }
    //}

    onClickBtnSmartExcelExport(event: any) {
        var filteredNodes = [];
        this.gridApiName.forEachNodeAfterFilter(function (node) {
            if (node.group) {
                console.log(node.key);
            } else {
                filteredNodes.push(node.data);
            }
        });

        if (filteredNodes.length > this.maxRowSizeImportandExport) {
            this.DisplayMessage("Can't export more than " + this.maxRowSizeImportandExport + " Records.", true);
            this.colorCode = "danger";
        }
        else {
            this.GenerateExcelName(filteredNodes);
        }
    }


    onClickBtnSave(event: any) {
        this.ClearMessage();
        this.gridApiName.stopEditing();

        let rowsSelection = JSON.parse(JSON.stringify(this.changedRows));

        if (rowsSelection.length > 0) {
            this._CommodityCodeListService.UpdateCommodityCodeListDetails(rowsSelection)
                .subscribe(
                    (data: any) => {
                        var returnCode = data._body.split('~')[0];
                        var returnMsg = data._body.split('~')[1];
                        this.hideShowAlertMsg = true;

                        if (returnMsg == "SUCCESS") {
                            this.DisplayMessage("Records successfully Updated.", true);
                            this.gridOptionsName.api.deselectAllFiltered();

                            this.GetFilterViewResult_edit();

                        }
                        else {
                            this.DisplayMessage(returnMsg, false);

                        }
                    },
                    err => console.log(err), // error
                );
            this.changedRows = [];
        }
        else {
            var msg = 'Please modify any data to update';
            this.DisplayMessage(msg, 'fail');
        }
    }

    onClickBtnRefresh() {
        this.GetFilterViewResult(null);
        this.gridBtnExcelExportFilter = false;
    }

    GenerateExcelName(rowData_temp: any[]) {

        debugger;
       // this.gridOptionsName.api.removeItems(this.changedRows);

        this.ClearMessage();
        let param = [];
        for (var i = 0; i < rowData_temp.length; i++) {
            param.push({
                ["Commodity ID"]: rowData_temp[i].commodityId,
                ["Commodity Name"]: rowData_temp[i].commodityName,
                ["Mark Flag"]: rowData_temp[i].markFlag ? "Y" : "N",
                ["Updated By"]: rowData_temp[i].updatedBy,
                ["Updated Date"]: rowData_temp[i].updatedDate
            })
        }
        this._CommodityCodeListService.CommodityCodeListExcelExport(param);
    }

    //clear filters
    clearFilterData() {
        this.ClearMessage();
        this.commodityIdInput = "";
        this.commodityNameInput = "";
    }

    //clear error/success message
    ClearMessage() {
        this.hideShowAlertMsg = false;
        this.successMsg = "";
    }

    DisplayMessage(msg, isSuccess) {
        this.hideShowAlertMsg = true;
        this.successMsg = msg;
        this.colorCode = isSuccess == true ? "success" : "danger";
    }

    clearGridDataName(event: any) {
        this.gridBtnClear = false;
        this.getGridData(this.getGridDataParam,'search');
        this.ClearMessage();

    }
}


export interface commodityCodeList {
    'commodityId': string,
    'commodityName': string,
    'markFlag': boolean,   
    'updatedBy': string,
    'updatedDate': string,
}