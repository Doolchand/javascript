﻿import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { SessionStorageService } from 'ngx-webstorage';
import * as XLSX from 'xlsx';
import { UserRoles } from '../../Model/user-roles';
import { RoutingRulesService } from "../../services/routing-rules.service";
import { UtilitiesService } from '../../services/utilities.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetErrorGridService } from '../../services/get-error-grid.service';

declare var jQuery: any;

@Component({
    selector: 'app-routing-rules',
    templateUrl: './routing-rules.component.html',
    styleUrls: ['./routing-rules.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class RoutingRulesComponent implements OnInit {

    @ViewChild('fakeClick') fakeClick: ElementRef;
    @ViewChild('file') FileName: any;

    //Authorisation 
    userRoles: UserRoles[];
    screenId = "3014";
    isUserHavingWriteAccess: boolean = false;
    isActiveScreen: boolean = true;
    userHavingWriteAccess: string[];
    userTenantTypeAccessList: string[];
    maxRowSizeImport: number = 5001;
    maxRowSizeExport: number = 30001;

    //Filter section
    HierarchyMemberDropdownLentgh: number = 0;
    myTexts = {};
    singleSelectDropdownSettingsGeography = {};
    singleSelectDropdownSettingsProduct = {};
    singleSelectDropdownSettingsChannel = {};
    singleSelectDropdownSettingsAccountName = {};
    singleSelectDropdownSettingsSupplyChain= {};

    //Hierarchy level dropdwon
    hierarchy_level = [];
    selected_levelId = [];
    freeTextsearch: string;
    SelectFamBase = [];
    getItemTypeData = [];
    selected_FamBase = [];
    multiSelectDropdownSettingsFamily = {};
    Select_Family: any[] = [];
    selected_FamID = [];
    isLOBinvalid: boolean = false;
    EnableDownloadTemplate: boolean = true;

    // Excel Import
    public _emcHierarchy: emcHierarchy[];
    enableFreeTextFor = ["108"];
    searchFreeTextHide: boolean = false;
    searchCreateFreeTextHide: boolean = false;
    selectedItemsMember = [];
    selectedItemsMemberSearcText = [];
    hierarchy_Member: any[] = [];
    selected_memberId = [];

    //create grid
    private gridCreateHMGName: GridOptions;
    private gridApiCreateHMGName;
    private gridApiCHMGNameColumnApiName;
    private rowDataNameHMG;
    private dataObjects = [];
    private contextCreateMember;
    private frameworkComponentsCreateName;
    private componentsCreateName;

    //Grid
    gridBtnsave: boolean = false;
    gridBtnClear: boolean = false;
    gridMemberBtnClear: boolean = false;
    gridBtnExcelExport: boolean = false;
    gridBtnExcelExportFilter: boolean = false;
    private gridOptionsName: GridOptions;
    private rowDataName;
    private gridColumnApiName;
    private gridApiName;
    private contextName;
    private frameworkComponentsName;
    private componentsName;

    //Button variable
    IsCreateFilterViewResultBtnDisable = true;
    IsCreateFilterClearBtnDisable = false;
    IsFilterViewResultBtnDisable = true;
    IsFilterClearBtnDisable = false;
    DisableUploadButton = false;
    gridBtnRefresh = false;
    errorExportBtn = false;

    //Import Variable
    excelfile: any;
    selectedfilename: string;
    gridMemberBtnAdd: boolean = false;

    //Error/Success Message     
    colorCode: string;
    successMsg: string = "";
    public hideShowAlertMsg = false;
    errImportParam = "P_SET_EMC_IMPORT";
    errCreateErrorParam = "P_INSERT_UPDATE_BASE";
    EnableUpload: boolean;

    // Lob ID Verification
    public _lobId: lobDataMapping[];
    DisableErrorExportBtn: boolean = true;
    private frameworkComponentsError;

    //Error Grid
    private errGridOptions: GridOptions;
    private errRowData;
    private errGridColumnApi;
    private errGridApi;
    private errContext;
    private errparam: any;
    private errgetRowNodeId;
    private gridColumnApiError;
    private gridApiError;
    griderrBtnExcelExport = false;
    rowCount: number = -1;
    createHMGTemp = [];

    //create member hierarchy 
    ParentMemberGrid = [];
    ParentMembertoOushtoDB = [];
    ItemTypeDropdwonGrid = [];
    private params: any;
    selectedItemsBaseSearchText = [];

    //User Access - EMC
    FGAAccess;
    UppAccess;

    EMCImportExcelHeader: string[] = [
        'LOB ID',
        'LOB DESC',
        'BRAND ID',
        'BRAND DESC',
        'FAMILY PARENT ID',
        'FAMILY PARENT DESC',
        'FAMILY ID',
        'FAMILY DESC',
        'BASE ID',
        'BASE DESC',
        'ITEM TYPE'
    ];

    constructor(private _router: Router, private _routingRulesService: RoutingRulesService, private _errorGridService: GetErrorGridService, private _sessionSt: SessionStorageService, private _utilitiesService: UtilitiesService) {
        var rolesHavingWriteAccess = this._sessionSt.retrieve('screenAuthorization') != null ? this._sessionSt.retrieve('screenAuthorization') : [];
        this.userHavingWriteAccess = rolesHavingWriteAccess[rolesHavingWriteAccess.findIndex(x => x.functionId == this.screenId)]["roleId"].split(',');
        this.userTenantTypeAccessList = this._sessionSt.retrieve('userTenantTypeAccessList') != null ? this._sessionSt.retrieve('userTenantTypeAccessList') : [];
        //Checking wheather User has FGA/UPP Access
        this.FGAAccess = this.userTenantTypeAccessList.includes("FGA");
        this.UppAccess = this.userTenantTypeAccessList.includes("UPP");
        this.userRoles = this._sessionSt.retrieve('userRoles');
        if (this.userRoles === null) {
            // User not allowed
            this._router.navigateByUrl('/errorMessage');
            return;
        }
        else {
            if (!this.UppAccess && !this.FGAAccess) {
                // Not allowed
                this._router.navigateByUrl('/errorMessage');
                return;
            }
            else if (!this.UppAccess && this.FGAAccess) {
                // Not allowed
                this._router.navigateByUrl('/errorMessage');
                return;
            }
            else if (this.UppAccess) {
                // Allowed
                for (var i = 0; i < this.userRoles.length; i++) {

                    if (this.userHavingWriteAccess.includes(this.userRoles[i].roleId.toString())) {
                        this.isUserHavingWriteAccess = true;
                        break;
                    }
                }
            }
            else {
                // Not allowed
                this._router.navigateByUrl('/errorMessage');
                return;
            }
        }

        this.ssInit();

        //Grid Create member hierarchy
        this.gridCreateHMGName = <GridOptions>{
            contextName: {
                componentParent: this
            },
        };
        this.gridCreateHMGName.columnDefs = [
            {
                headerName: "",
                width: 35,
                field: "mapID",
                pinned: "left",
                suppressSizeToFit: true,
                suppressFilter: true,
                headerCheckboxSelection: false,
                editable: false,
                headerCheckboxSelectionFilteredOnly: true,
                checkboxSelection: function (params) {
                    if (params.data.updatedBY == "") {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            },
            {
                headerName: "PARENT MEMBER",
                field: "parent_Member",
                suppressSizeToFit: true,
                width: 200,
                tooltipField: "parent_Member",
                editable: function (Rownode) {
                    if (Rownode.node.data.updatedBY == "")
                        return true;
                    else
                        return false;
                },
                colId: "ParentMemberGrid",
                cellClass: 'multi-select-grid',
                cellEditor: "getFamilyDropdown"
            },
            {
                headerName: "BASE ID",
                field: "base_Id",
                suppressSizeToFit: true,
                width: 200,
                tooltipField: "base_Id",
                editable: function (Rownode) {
                    if (Rownode.node.data.updatedBY == "")
                        return true;
                    else
                        return false;
                },

            },
            {
                headerName: "BASE DESCRIPTION",
                field: "base_Desc",
                suppressSizeToFit: true,
                width: 200,
                tooltipField: "base_Desc",
                editable: function (Rownode) {
                    if (Rownode.node.data.updatedBY == "")
                        return true;
                    else
                        return false;
                },

            },
            {
                headerName: "ITEM TYPE",
                field: "item_Type",
                suppressSizeToFit: true,
                width: 200,
                tooltipField: "item_Type",
                colId: "getItemTypeData",
                editable: function (Rownode) {
                    if (Rownode.node.data.updatedBY == "")
                        return true;
                    else
                        return false;
                },
                cellClass: 'multi-select-grid',
                cellEditor: "getItemType"
            },
            {
                headerName: "UPDATED BY",
                field: "updatedBY",
                suppressSizeToFit: true,
                width: 200,
                tooltipField: "updatedBY",
                editable: false

            },
            {
                headerName: "UPDATED DATE",
                field: "updatedDate",
                suppressSizeToFit: true,
                width: 200,
                tooltipField: "updatedDate",
                editable: false

            }
        ];
        this.gridCreateHMGName.headerHeight = 18;
        this.gridCreateHMGName.rowSelection = "multiple";
        this.gridCreateHMGName.paginationPageSize = 14;
        this.gridCreateHMGName.defaultColDef = {
            editable: true,
            enableValue: true
        };
        this.gridCreateHMGName.rowData = [];
        this.gridCreateHMGName.suppressRowClickSelection = true;
        this.gridCreateHMGName.animateRows = true;
        this.contextCreateMember = { componentsCreateName: this };
        this.componentsCreateName = {
            getItemType: getItemTypeDropdown(),
            getFamilyDropdown: getFamilyDropdown()
        }

        //Grid EMC Hierarchy Grid   
        this.gridOptionsName = <GridOptions>{
            contextName: {
                componentParentName: this
            },
        };
        this.gridOptionsName.columnDefs = [
            {
                headerName: "LOB ID",
                field: "lob_Id",
                suppressSizeToFit: false,
                tooltipField: "lob_Id",
                editable: false
            },
            {
                headerName: "LOB DESC",
                field: "lob_Desc",
                suppressSizeToFit: false,
                tooltipField: "lob_Desc",
                editable: false

            },
            {
                headerName: "BRAND ID",
                field: "brand_Id",
                suppressSizeToFit: false,
                //width: 180,
                tooltipField: "brand_Id",
                editable: false

            },
            {
                headerName: "BRAND DESC",
                field: "brand_Desc",
                suppressSizeToFit: false,
                tooltipField: "brand_Desc",
                editable: false
            },
            {
                headerName: "FAMILY PARENT ID",
                field: "family_Parent_Id",
                suppressSizeToFit: true,
                width: 120,
                tooltipField: "family_Parent_Id",
                editable: false

            },
            {
                headerName: "FAMILY PARENT DESC",
                field: "family_Parent_Desc",
                suppressSizeToFit: true,
                width: 120,
                tooltipField: "family_Parent_Desc",
                editable: false

            },
            {
                headerName: "FAMILY ID",
                field: "family_Id",
                suppressSizeToFit: false,
                tooltipField: "family_Id",
                editable: false

            },
            {
                headerName: "FAMILY DESC",
                field: "family_Desc",
                suppressSizeToFit: false,
                tooltipField: "family_Desc",
                editable: false

            },
            {
                headerName: "BASE ID",
                field: "base_Id",
                suppressSizeToFit: false,
                tooltipField: "base_Id",
                editable: false

            },
            {
                headerName: "BASE DESC",
                field: "base_Desc",
                suppressSizeToFit: false,
                tooltipField: "base_Desc",
                editable: false

            },
            {
                headerName: "ITEM TYPE",
                field: "item_Type",
                editable: false,
                suppressSizeToFit: false,
                tooltipField: "item_Type"

            },
            {
                headerName: "UPDATED BY",
                field: "updatedBY",
                suppressSizeToFit: false,
                tooltipField: "updatedBY",
                editable: false,

            },
            {
                headerName: "UPDATED DATE",
                field: "updatedDate",
                suppressSizeToFit: false,
                tooltipField: "updatedDate",
                editable: false,

            },

        ];
        this.gridOptionsName.headerHeight = 18;
        this.gridOptionsName.rowSelection = "multiple";
        this.gridOptionsName.paginationPageSize = 14;
        this.gridOptionsName.defaultColDef = {
            editable: true,
            enableValue: true
        };
        this.gridOptionsName.rowData = [];
        this.contextName = { componentParent: this };
        this.componentsName = {

        }

        //Error Grid Code
        this.errGridOptions = <GridOptions>{
            context: {
                componentParent: this
            },
        };
        this.errGridOptions.columnDefs = [
            {
                headerName: "ERROR ID",
                field: "errorId",
                width: 200,
                tooltipField: "errorId",
                suppressSizeToFit: true,

            },
            {
                headerName: "ERROR RECORD",
                field: "errorRecord",
                width: 470,
                suppressSizeToFit: true,
                tooltipField: "errorRecord"
            },
            {
                headerName: "Error Reason",
                field: "errorReason",
                width: 470,
                tooltipField: "errorReason",
                suppressSizeToFit: true
            },
            {
                headerName: "Error Date",
                field: "errorDate",
                width: 230,
                tooltipField: "errorDate",
                suppressSizeToFit: true
            }
        ];
        this.errGridOptions.headerHeight = 18;
        this.errGridOptions.rowSelection = "multiple";
        this.errGridOptions.paginationPageSize = 14;
        this.errGridOptions.defaultColDef = {
            enableValue: true
        };
        this.errRowData = [];
        this.errGridOptions.animateRows = true;
        this.errgetRowNodeId = function (data) {
            return data.errorId;
        };
    }

    //SS Dropdown On init
    ssInit() {
        this.multiSelectDropdownSettingsFamily = {
            enableSearch: true,
            // text: 'Select Family',
            checkedStyle: 'glyphicon',
            itemClasses: 'filter-dropdown',
            buttonClasses: 'btn btn-default btn-block',
            dynamicTitleMaxItems: 1,
            displayAllSelectedText: true,
            showUncheckAll: true,
            closeOnClickOutside: true,
            searchEmptyResult: '',
            //classes: "myclass custom-class", minHeight:200
        };

        this.myTexts = {
            checkAll: 'Select all',
            uncheckAll: 'Unselect all',
            checked: 'item selected',
            checkedPlural: 'items selected',
            defaultTitle: 'Select Family',
            allSelected: 'All selected',
            searchEmptyResult: '',
        };
    }

    //initilasying the dropdown members
    ngOnInit() {

        this.singleSelectDropdownSettingsGeography = {
            singleSelection: true,
            text: "Select Geography",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };

        this.singleSelectDropdownSettingsProduct = {
            singleSelection: false,
            text: "Select Product/FHC/FGA",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect all',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class disabled",
            //badgeShowLimit: 100,
            badgeShowLimit: 1
        };

        this.singleSelectDropdownSettingsChannel = {
            singleSelection: true,
            text: "Select Channel",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };

        this.singleSelectDropdownSettingsAccountName = {
            singleSelection: false,
            text: "Select Account Name",
            selectAllText: 'Select All',
            unSelectAllText: 'Unselect all',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class disabled",
            //badgeShowLimit: 100,
            badgeShowLimit: 1
        };
        this.singleSelectDropdownSettingsSupplyChain = {
            singleSelection: true,
            text: "Select Supply Chain Type",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };


        //Hierarchy Level data
        this.getHierarchyLevel();

        this.getFamily();

        this.getItemType();

    }

    //get Item type
    getItemType() {

        this._routingRulesService.getItemType()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        this.getItemTypeData = [];
                        for (var i = 0; i < data.length; i++) {

                            this.getItemTypeData.push({
                                id: data[i].itemTypeId,
                                itemName: data[i].itemTypeName
                            })
                            //this.productTypeListCommaSeperated += data[i].id + ",";
                            //this.productTypeListCommaSeperated.substring(0, this.productTypeListCommaSeperated.length - 1);
                        }
                    }
                },
                err => console.log(err),
            );
    }

    //get family
    getFamily() {
        this._routingRulesService.getFamily()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        this.SelectFamBase = [];
                        for (var i = 0; i < data.length; i++) {
                            this.SelectFamBase.push({
                                id: data[i].familyId,
                                name: data[i].familyName
                            });
                        }
                    }
                },
                err => console.log(err),
            );
    }

    //get data for hierarchy level
    getHierarchyLevel() {
        this.hierarchy_level = [];
        this.IsFilterViewResultBtnDisable = true;
        this._routingRulesService.getHierarchyLevel()
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    for (var i = 0; i < data.length; i++) {
                        this.hierarchy_level.push({
                            id: data[i].levelId,
                            itemName: data[i].levelName
                        })
                    }
                },
                err => console.log(err),
            );
    }

    private onSelectLevelDD(level: any) {
        this.closeDropdown();
        this.ClearMessage();
        this.rowDataName = [];
        this.hierarchy_Member = [];
        this.selected_memberId = [];
        this.freeTextsearch = "";
        this.IsFilterViewResultBtnDisable = true;
        this.IsFilterClearBtnDisable = true;
        this.gridBtnExcelExport = false;
        this.gridBtnExcelExportFilter = false;
        this.gridBtnClear = false;
        this.getHierarchyMember();
        this.HierarchyMemberDropdownLentgh = 0;
    }

    //on select of family
    onSelectFamily(event: any, isTrue) {
        this.rowDataNameHMG = [];
        this.griderrBtnExcelExport = false;
        this.errRowData = [];
        this.ClearMessage();
        if (this.selected_FamBase.length == 0) {

            this.IsCreateFilterClearBtnDisable = false;
            this.IsCreateFilterViewResultBtnDisable = true;
            this.gridMemberBtnAdd = false;
            this.gridBtnsave = false;
            this.gridMemberBtnClear = false;

        }
        else {
            this.IsCreateFilterClearBtnDisable = true;
            this.IsCreateFilterViewResultBtnDisable = false;
            this.gridMemberBtnAdd = false;
            this.gridBtnsave = false;
            this.gridMemberBtnClear = false;

        }
    }

    clearCreateFilterData() {
        this.selected_FamBase = [];
        this.IsCreateFilterClearBtnDisable = false;
        this.IsCreateFilterViewResultBtnDisable = true;
        this.gridMemberBtnAdd = false;
        this.gridMemberBtnClear = false;
        this.gridBtnsave = false;
        this.errRowData = [];
        this.ClearMessage();
        this.rowDataNameHMG = [];

    }

    //Filter and EMC Grid Section
    getHierarchyMember() {
        this.hierarchy_Member = [];
        if (this.enableFreeTextFor.includes(this.selected_levelId[0].id)) {
            this.searchFreeTextHide = true;
        }
        else {
            this.getChannelMemeberData(this.selected_levelId[0].id, "");
        }
    }

    getChannelMemeberData(levelID, freeTextsearch) {
        this._routingRulesService.getHierarchyMember(levelID, freeTextsearch)
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        if (data.length > 0) {
                            if (levelID == "104" || levelID == "105") {
                                this.searchFreeTextHide = false;
                                for (var i = 0; i < data.length; i++) {
                                    this.hierarchy_Member.push({
                                        id: data[i].id,
                                        itemName: data[i].itemName
                                    });
                                }
                            }
                            else if (data.length >= 1000) {
                                this.HierarchyMemberDropdownLentgh = data.length;
                                this.searchFreeTextHide = true;
                            }
                            else {
                                this.searchFreeTextHide = false;
                                for (var i = 0; i < data.length; i++) {
                                    this.hierarchy_Member.push({
                                        id: data[i].id,
                                        itemName: data[i].itemName
                                    });
                                }

                            }
                        }
                        else {
                            this.searchFreeTextHide = true;
                            this.IsFilterClearBtnDisable = true;
                        }
                    }
                },
                err => console.log(err), // error
            );
    }

    getHierarchyMemberFromSearchText(event) {
        this.rowDataName = [];
        if (this.freeTextsearch !== "") {
            this.IsFilterViewResultBtnDisable = false;
        }
        else {
            this.IsFilterViewResultBtnDisable = true;
        }
        this.hierarchy_Member = [];
        this.selectedItemsMember = [];
        this.selectedItemsMemberSearcText = [];
        var data = this.freeTextsearch.toUpperCase().split(',');
        for (var i = 0; i < data.length; i++) {
            this.selectedItemsMemberSearcText.push({
                id: data[i],
                itemName: data[i]
            });
        }
        // this.ChannelViewResult(event);
    }

    GetEmcFilterViewResult(events) {
        this.closeDropdown();
        let temp = this.selected_memberId;
        this.ClearMessage();
        this.rowDataName = [];
        if (this.enableFreeTextFor.includes(this.selected_levelId[0].id)) {
            temp = this.selectedItemsMemberSearcText;
        }
        if (this.HierarchyMemberDropdownLentgh >= 1000) {
            temp = this.selectedItemsMemberSearcText;
        }

        this._routingRulesService.GetEmcFilterViewResult(this.selected_levelId[0].id, temp)
            .map((data: any) => data.json()).subscribe((data: any) => {
                if (data.length > 0) {
                    this.gridBtnClear = true;
                    this.gridBtnExcelExport = true;
                    this.gridBtnExcelExportFilter = false;
                    this._utilitiesService.ReplaceSplCharFromStringList(data, "updatedBY");

                    this.rowDataName = data;
                }
                else {
                    this.gridBtnClear = false;
                    this.gridBtnExcelExport = false;
                    this.gridBtnExcelExportFilter = false;
                }
            },
                err => console.log(err), // error
            )
    }

    GetEmcCreateFilterViewResult(events) {
        this.closeDropdown();
        this.ClearMessage();
        this.ParentMemberGrid = [];
        this.ParentMembertoOushtoDB = [];
        this.gridMemberBtnAdd = true;
        //this.ParentMemberGrid.push({
        //    id: "DELINK",
        //    itemName: "DELINK"
        //});
        //this.ParentMembertoOushtoDB.push({
        //    id: "DELINK",
        //    itemName: "DELINK"
        //});

        for (var i = 0; i < this.selected_FamBase.length; i++) {
            var familylst = this.SelectFamBase.find(x => x.id == this.selected_FamBase[i])


            this.ParentMemberGrid.push(
                {
                    id: familylst.id,
                    //itemName:  familylst.id + '_' + familylst.name
                    itemName: familylst.name
                    //data[i].itemName + '_' + data[i].id
                });
            this.ParentMembertoOushtoDB.push(
                {
                    familyName: familylst.name,
                    //itemName:  familylst.id + '_' + familylst.name
                    familyId: familylst.id
                    //data[i].itemName + '_' + data[i].id
                });
            this.CreateHMGGridRefresh();
        }


    }

    CreateHMGGridRefresh() {
        this._routingRulesService.GetCreateEmcFilterViewResult(this.ParentMembertoOushtoDB)
            .map((data: any) => data.json()).subscribe((data: any) => {
                if (data != null) {
                    this._utilitiesService.ReplaceSplCharFromStringList(data, "updatedBY");

                    this.rowDataNameHMG = data;
                    this.createHMGTemp = data;
                }
            },
                err => console.log(err), // error
            )
    }

    closeDropdown() {
        const el: HTMLElement = this.fakeClick.nativeElement as HTMLElement;
        el.click();
    }

    //Import Section, export section
    onGridReadyName(params) {
        this.gridApiName = params.api;
        this.gridColumnApiName = params.columnApi;
        this.gridOptionsName.api.sizeColumnsToFit();
    }

    onGridReadyCHMGName(params) {

        this.gridApiCreateHMGName = params.api;
        this.gridApiCHMGNameColumnApiName = params.columnApi;
        this.gridCreateHMGName.api.sizeColumnsToFit();
    }

    btnerrExportExcel() {
        this.GenerateExcel(this.errRowData);
    }

    GenerateExcel(errRowData) {
        //this.ClearMessage();
        let param = [];
        for (var i = 0; i < errRowData.length; i++) {

            param.push({
                ["ERROR ID"]: errRowData[i].errorId,
                ["ERROR REASON"]: errRowData[i].errorReason,
                ["ERROR RECORD"]: errRowData[i].errorRecord,
                ["ERROR DATE"]: errRowData[i].errorDate
            });
        }
        this._routingRulesService.exportAsExcelFile(param, "EMCProductHierarchy_Maintenance_ErrorRecords");
    }

    OnFilterChangeName(event: any) {
        if (this.gridApiName.filterManager.advancedFilterPresent) {
            this.gridBtnExcelExportFilter = true;
        }
        else {
            this.gridBtnExcelExportFilter = false;
        }
    }

    OnFilterChangeCreate(event: any) {
        this.gridApiCreateHMGName.stopEditing();
    }

    oncellEditingStarted(event: any) {
        if (event.node.data.updatedBY == "") {
            event.node.setSelected(true);
        }
        else {
            event.node.setSelected(false);
        }

    }

    onGridReadyError(params) {
        this.gridApiError = params.api;
        this.gridColumnApiError = params.columnApi;
    }

    onSelectionChangedCreate(event: any) {
        let rowsSelection = this.gridCreateHMGName.api.getSelectedRows();
        if (rowsSelection.length > 0) {
            this.gridBtnsave = true;
        }
        else {
            this.gridBtnsave = false;
        }
    }

    btnExportExcelName() {
        if (this.rowDataName.length > this.maxRowSizeExport) {
            this.DisplayMessage("Can't export more than " + (this.maxRowSizeExport - 1) + " Records.", true);
            this.colorCode = "danger";
        }
        else {
            this.GenerateExcelName(this.rowDataName);
        }
    }

    GenerateExcelName(rowData: any[]) {

        //this.ClearMessage();
        let param = [];
        for (var i = 0; i < rowData.length; i++) {
            param.push({
                ["LOB ID"]: rowData[i].lob_Id,
                ["LOB DESC"]: rowData[i].lob_Desc,
                ["BRAND ID"]: rowData[i].brand_Id,
                ["BRAND DESC"]: rowData[i].brand_Desc,
                ["FAMILY PARENT ID"]: rowData[i].family_Parent_Id,
                ["FAMILY PARENT DESC"]: rowData[i].family_Parent_Desc,
                ["FAMILY ID"]: rowData[i].family_Id,
                ["FAMILY DESC"]: rowData[i].family_Desc,
                ["BASE ID"]: rowData[i].base_Id,
                ["BASE DESC"]: rowData[i].base_Desc,
                ["ITEM TYPE"]: rowData[i].item_Type,
                ["UPDATED BY"]: rowData[i].updatedBY,
                ["UPDATED DATE"]: rowData[i].updatedDate
            })
        }
        this._routingRulesService.ExcelExport(param);
    }

    addGridRowHMGFull(event: any) {
        this.ClearMessage();
        var newItem = this.addGridRowHMG();

        //To get sequence no. for new record
        //this._routingRulesService.getSequenceNumber()
        //    .subscribe(
        //    (data: any) => {
        //        newItem.base_Id =  data._body;
        //        },
        //        err => console.log(err),
        //    );
        debugger;
        this.gridApiCreateHMGName.updateRowData(newItem);
        this.gridApiCreateHMGName.forEachNode(function (node) {
            if (node.data.ruleId === "") {
                node.setSelected(true);
            }
        });
        this.gridApiCreateHMGName.paginationGoToLastPage();
        this.gridMemberBtnClear = true;
    }

    clearFilterData() {
        //this.dropdownRef.clearSearch();
        this.selected_levelId = [];
        this.selected_memberId = [];
        this.freeTextsearch = "";
        this.rowDataName = [];
        this.errRowData = [];

        this.IsFilterClearBtnDisable = false;
        this.IsFilterViewResultBtnDisable = true;
        this.gridBtnRefresh = false;
        this.gridBtnClear = false;
        //this.gridBtnsave = false;
        this.gridBtnExcelExport = false;
        this.ClearMessage();
    }

    clearGridDataName(nodeData: any) {
        if (this.selected_memberId.length > 0) {
            this.GetEmcFilterViewResult(nodeData);
            this.ClearMessage();
            //this.gridBtnClear = false;
            this.errRowData = [];
        }
        else {
            this.clearFilterData();
            //this.selectedItemsMemberList = [];
            this.errRowData = [];
        }
    }

    //On select of dropdowns
    onSelectAllMember(nodeData: any) {
        this.gridBtnExcelExportFilter = false;
        this.gridBtnClear = false;
        this.gridBtnExcelExport = false;
        this.rowDataName = [];
        if (this.selected_memberId.length > 0) {
            //this.IsFilterClearBtnDisable = true;
            this.IsFilterViewResultBtnDisable = false;
        } else {
            //this.IsFilterClearBtnDisable = false;
            this.IsFilterViewResultBtnDisable = true;
        }
    }

    onSelectMember(nodeData: any) {
        this.gridBtnExcelExportFilter = false;
        this.gridBtnClear = false;
        this.gridBtnExcelExport = false;
        this.rowDataName = [];
        if (this.selected_memberId.length > 0) {
            this.IsFilterViewResultBtnDisable = false;
        }
        else {
            this.IsFilterViewResultBtnDisable = true;
        }

    }

    //Messages section
    ClearMessage() {
        this.hideShowAlertMsg = false;
        this.successMsg = "";
    }

    ClearInvalidMessage() {
        //
        this.griderrBtnExcelExport = false;
        this.errRowData = [];
        this.FileName.nativeElement.value = "";
        this.isLOBinvalid = false;
        this.EnableDownloadTemplate = true;
        this.ClearMessage();
        //this.colorCode = "success";
    }

    DisplayMessage(msg, isSuccess) {
        this.EnableUpload = false;
        this.hideShowAlertMsg = true;
        this.successMsg = msg;
        this.colorCode = isSuccess == true ? "success" : "danger";
    }

    // Error Grid
    getErrorGrid(param) {
        this.errRowData = [];
        this._errorGridService.getErrorList(param)
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    this.griderrBtnExcelExport = true;
                    this.errRowData = data;
                },
                err => console.log(err), // error
            )
    }

    // Import Predefined Excel Template
    btnImportExcelTemplate() {
        this.hideShowAlertMsg = false;
        var param = {
            dropDownText: null,
            dropDownValue: null,
        }
        this._routingRulesService.ImportExcelTemplate(param);
    }

    // Excel Sheet Validation
    FileImportValidation($event) {

        this.EnableDownloadTemplate = true;
        // this.ClearMessage();
        //this.EnableUpload = true;
        this.isLOBinvalid = false;
        this.griderrBtnExcelExport = false;
        this.errRowData = [];
        this.ClearMessage();
        if ($event.target.files && $event.target.files[0]) {
            this.excelfile = $event.target.files[0];
            this.selectedfilename = this.excelfile.name;
            //var fileNameWithOutExtenstion = this.selectedfilename.substring(0, this.selectedfilename.lastIndexOf('.')).toUpperCase();
            var fileExt1 = this.selectedfilename.substring(this.selectedfilename.lastIndexOf('.') + 1).toUpperCase();
            if (fileExt1 != 'XLSX') {
                this.DisplayMessage("File has wrong extension!!", false);
                this.EnableUpload = false;
            }
            else {
                this.EnableUpload = true;
            }
        }
        else {
            this.EnableUpload = false;
        }

    }

    ValidateLobData() {
        this.FileName.nativeElement.value = "";
        this.ClearMessage();
        this.errRowData = [];
        var reader: FileReader = new FileReader();
        reader.onloadend = (e) => {
            var data = reader.result;
            var workbook = XLSX.read(data, { type: 'binary', cellDates: true, dateNF: 'mm/dd/yyyy;@' });
            let excelHeader: any = [];
            var first_sheet_name = workbook.SheetNames[0];
            var headers = XLSX.utils.sheet_to_json(workbook.Sheets[first_sheet_name], { header: 1 })[0];
            var dataObject: any = XLSX.utils.sheet_to_json(workbook.Sheets[first_sheet_name]);
            Object.values(headers).forEach(value => {
                excelHeader.push(value)
            });
            if (JSON.stringify(excelHeader) != JSON.stringify(this.EMCImportExcelHeader)) {
                this.DisplayMessage('Header name or order is incorrect', false);
            }
            else if (dataObject.length <= 0) {
                this.DisplayMessage("No Data Found In The Excelsheet.", false);
            }
            else if (dataObject.length > this.maxRowSizeImport) {
                this.DisplayMessage("Records in upload sheet cannot be more than " + (this.maxRowSizeImport - 1), false);
            }
            else if (dataObject.length > 0 && dataObject.length <= this.maxRowSizeImport) {
                // Prepare the LOB IDs for validation
                let lobData: any = [];
                for (var i = 0; i < dataObject.length; i++) {
                    lobData.push(dataObject[i]["LOB ID"]);
                }

                // Take only unique values
                var uniqueLobData = Array.from(new Set(lobData));
                this.getLobDataDetail(uniqueLobData);

                // Fetch the result of LOB ID Verification
                this.getValidationLobDataDetail();

            }

        }
        this.EnableUpload = true;
        reader.readAsBinaryString(this.excelfile);

    }

    // Upload Excel Data
    UploadExcelData() {
        this.errRowData = [];
        this.ClearInvalidMessage();
        var reader: FileReader = new FileReader();
        console.log("UPLOAD EXCEL SHEET" + this.selectedfilename);
        reader.onloadend = (e) => {
            var data = reader.result;
            var workbook = XLSX.read(data, { type: 'binary', cellDates: true, dateNF: 'mm/dd/yyyy;@' });
            let excelHeader: any = [];
            var first_sheet_name = workbook.SheetNames[0];
            var headers = XLSX.utils.sheet_to_json(workbook.Sheets[first_sheet_name], { header: 1 })[0];
            var dataObject: any = XLSX.utils.sheet_to_json(workbook.Sheets[first_sheet_name]);
            Object.values(headers).forEach(value => {
                excelHeader.push(value)
            });

            if (dataObject.length < 1) {
                this.DisplayMessage("No Data Found In The Excelsheet.", false);
            }
            else if (dataObject.length >= this.maxRowSizeImport) {
                this.DisplayMessage("Records in upload sheet cannot be more than" + (this.maxRowSizeImport - 1), false);
            }
            else if (dataObject.length >= 1 && dataObject.length < this.maxRowSizeImport) {
                // Upload the Excel sheet
                this.getEMCProductHierarchyDetail(dataObject);
                this.EMCHierarchyUploadExcel();
            }

        }
        this.FileName.nativeElement.value = "";
        this.EnableUpload = true;
        reader.readAsBinaryString(this.excelfile);
    }

    EMCHierarchyUploadExcel() {
        this.errRowData = [];
        this._routingRulesService.uploadExcelData(this._emcHierarchy).subscribe(
            (data: any) => {
                var returnCode = data._body.split("~")[0];
                var returnMsg = data._body.split("~")[1];
                returnMsg = returnMsg.split(".")[0];
                this.hideShowAlertMsg = true;
                this.FileName.nativeElement.value = "";
                if (returnCode == "0") {
                    this.EnableUpload = false;
                    this.colorCode = "success";
                    this.successMsg = returnMsg + ": Successfully Imported.";
                    this.DisplayMessage(this.successMsg, true);
                    this.getFamily();
                }
                else {
                    this.colorCode = "danger";
                    this.successMsg = returnMsg;
                    this.DisplayMessage(this.successMsg, false);
                    // Get Error from here
                    this.getErrorGrid(this.errImportParam);
                }
            }
        );
    }

    getEMCProductHierarchyDetail(dataObjects) {
        this._emcHierarchy = [];
        for (var i = 0; i < dataObjects.length; i++) {
            this._emcHierarchy.push({
                lob_id: typeof dataObjects[i]["LOB ID"] == 'undefined' ? "" : dataObjects[i]["LOB ID"],
                lob_desc: typeof dataObjects[i]["LOB DESC"] == 'undefined' ? "" : dataObjects[i]["LOB DESC"],
                brand_id: typeof dataObjects[i]["BRAND ID"] == 'undefined' ? "" : dataObjects[i]["BRAND ID"],
                brand_desc: typeof dataObjects[i]["BRAND DESC"] == 'undefined' ? "" : dataObjects[i]["BRAND DESC"],
                family_parent_id: typeof dataObjects[i]["FAMILY PARENT ID"] == 'undefined' ? "" : dataObjects[i]["FAMILY PARENT ID"],
                family_parent_desc: typeof dataObjects[i]["FAMILY PARENT DESC"] == 'undefined' ? "" : dataObjects[i]["FAMILY PARENT DESC"],
                family_id: typeof dataObjects[i]["FAMILY ID"] == 'undefined' ? "" : dataObjects[i]["FAMILY ID"],
                family_desc: typeof dataObjects[i]["FAMILY DESC"] == 'undefined' ? "" : dataObjects[i]["FAMILY DESC"],
                base_id: typeof dataObjects[i]["BASE ID"] == 'undefined' ? "" : dataObjects[i]["BASE ID"],
                base_desc: typeof dataObjects[i]["BASE DESC"] == 'undefined' ? "" : dataObjects[i]["BASE DESC"],
                item_type: typeof dataObjects[i]["ITEM TYPE"] == 'undefined' ? "" : dataObjects[i]["ITEM TYPE"]
                //updatedBy: dataObjects[i]["UPDATED BY"],
                //updatedDate: dataObjects[i]["UPDATED DATE"]
            });
        }
    }

    getLobDataDetail(dataObjects) {
        this._lobId = [];
        for (var i = 0; i < dataObjects.length; i++) {
            this._lobId.push({
                lob_id: dataObjects[i]
            });
        }
    }

    getValidationLobDataDetail(): any {
        this._routingRulesService.uploadLobData(this._lobId).subscribe(
            (data: any) => {
                var returnCode = data._body.split("~")[0];
                var returnMsg = data._body.split("~")[1];
                var verifyCode = data._body.split("~")[2];
                returnMsg = returnMsg.split(".")[0];
                this.hideShowAlertMsg = true;
                //
                if (verifyCode == "Y") {
                    this.EnableUpload = false;
                    this.successMsg = "Data Successfully Validated. Uploading.";
                    this.DisplayMessage(this.successMsg, true);
                    this.UploadExcelData();
                }
                else {
                    this.isLOBinvalid = true;
                    this.successMsg = "LOB Data not linked Yet, Want to Continue?";
                    this.EnableUpload = false;
                    this.colorCode = "danger";
                    this.EnableDownloadTemplate = false;
                    // Get Error from here
                    this.getErrorGrid(this.errImportParam);
                }
            }
        );
    }

    //Add grid row
    addGridRowHMG() {
        var newItem = {
            'mapID': null,
            'parent_Member': "",
            'base_Id': "",
            'base_Desc': null,
            'item_Type': null,
            'updatedBY': "",
            'updatedDate': "",
        };
        return newItem;
    }

    //create hierarchy member clear
    clearGridDataCreation(event: any) {
        this.rowDataNameHMG = [];
        this.gridMemberBtnClear = false;
        this.gridBtnsave = false;
        this.errRowData = [];
        this.ClearMessage();
        this.CreateHMGGridRefresh();
    }

    OnBodyScroll($event) {
        this.gridApiCreateHMGName.stopEditing();
    }

    //Inserting create hierarchyGrid data to db
    updateGridRowMemberCreation(event: any) {
        //this.hideShowAlertMsg = false;

        this.gridApiCreateHMGName.stopEditing();

        let rowsSelection = JSON.parse(JSON.stringify(this.gridCreateHMGName.api.getSelectedRows()));
        rowsSelection = this.convertFamilyDesctoID(rowsSelection);

        if (this.validateCreateMemberHierarchyData(rowsSelection)) {
            var selectedData = this.ConvertToUpperBaseIdAndDescription(rowsSelection);
            if (!this.validateDuplicateBaseID(selectedData)) {
                if (!this.validateDuplicateBaseDescription(selectedData)) {

                    this._routingRulesService.updateHierarchyMemberCreationDetails(rowsSelection)
                        .subscribe(
                            (data: any) => {

                                var returnCode = data._body.split("~")[0];
                                var returnMsg = data._body.split("~")[1];
                                //           this.hideShowAlertMsg = true;
                                if (returnCode == "0") {
                                    this.colorCode = "success";
                                    this.successMsg = returnMsg + ": Successfully Updated.";
                                    this.DisplayMessage(this.successMsg, true);
                                    this.gridCreateHMGName.api.deselectAllFiltered();
                                    this.errRowData = [];
                                    this.CreateHMGGridRefresh();
                                }
                                else {
                                    this.colorCode = "danger";
                                    this.successMsg = returnMsg;
                                    this.DisplayMessage(this.successMsg, false);
                                    // Get Error from here
                                    this.getErrorGrid(this.errCreateErrorParam);
                                }

                            },
                            err => console.log(err), // error
                        );
                }
                else {
                    this.DisplayMessage("Validation Failed, BASE DESCRIPTION should be Unique", false);
                }
            }
            else {
                this.DisplayMessage("Validation Failed, BASE ID should be Unique", false);
            }
        }
        else {
            this.DisplayMessage("Validation Failed, PARENT MEMBER, BASE ID, BASE DESCRIPTION, ITEM TYPE are mandatory", false);
        }
    }

    //Validations on Base ID uniqness
    validateCreateMemberHierarchyData(selectedData) {
        if (selectedData != undefined && selectedData != null && selectedData.length > 0) {
            for (var i = 0; i < selectedData.length; i++) {
                if (selectedData[i].base_Desc == undefined || selectedData[i].base_Desc == null || selectedData[i].base_Desc.trim() == "") {
                    return false;
                }
                if (selectedData[i].item_Type == undefined || selectedData[i].item_Type == null || selectedData[i].item_Type == "") {
                    return false;
                }
                if (selectedData[i].base_Id == undefined || selectedData[i].base_Id == null || selectedData[i].base_Id.trim() == "") {
                    return false;
                }
                if (selectedData[i].parent_Member == undefined || selectedData[i].parent_Member == null || selectedData[i].parent_Member == "") {
                    return false;
                }
            }
            return true;
        }

        // 
    }

    validateDuplicateBaseDescription(selectedData) {
        var valueArr = selectedData.map(function (item) { return item.base_Desc });
        var isDuplicate = valueArr.some(function (item, idx) {
            return valueArr.indexOf(item) != idx
        });
        return isDuplicate;
    }

    validateDuplicateBaseID(selectedData) {
        var valueArr = selectedData.map(function (item) { return item.base_Id });
        var isDuplicate = valueArr.some(function (item, idx) {
            return valueArr.indexOf(item) != idx
        });
        return isDuplicate;
    }

    ConvertToUpperBaseIdAndDescription(selectedData) {
        for (var i = 0; i < selectedData.length; i++) {
            if (selectedData.find(x => x.base_Desc == selectedData[i].base_Desc) != undefined) {
                selectedData[i].base_Desc = selectedData[i].base_Desc.toUpperCase();
            }
            if (selectedData.find(x => x.base_Id == selectedData[i].base_Id) != undefined) {
                selectedData[i].base_Id = selectedData[i].base_Id.toUpperCase();
            }
        }
        return selectedData;
    }

    //Convert Family Desc to ID
    convertFamilyDesctoID(selectedData) {

        for (var i = 0; i < selectedData.length; i++) {
            selectedData[i].parent_Member = this.SelectFamBase.find(x => x.name == selectedData[i].parent_Member).id;
        }
        return selectedData;
    }

}
export interface emcHierarchy {
    lob_id: string;
    lob_desc: string;
    brand_id: string;
    brand_desc: string;
    family_parent_id: string;
    family_parent_desc: string;
    family_id: string;
    family_desc: string;
    base_id: string;
    base_desc: string;
    item_type: string;
    // updatedBy: string;
    // updatedDate: string;
}

export interface lobDataMapping {
    lob_id: string;
}

export class AttributeFilter {
    FamilyParentId: string;
    RegionId: string;
}

function dpClicked() {
    let el: HTMLElement = this.fakeClick.nativeElement as HTMLElement;
    el.click();
}

export interface errorGridData {
    errorId: number;
    errorReason: string;
    errorRecord: string;
    errorDate: string;
}

function getItemTypeDropdown() {

    function MultiSelectDropDown() { }
    MultiSelectDropDown.prototype.getGui = function () {
        return this.eGui;
    };
    MultiSelectDropDown.prototype.getValue = function () {
        var arr = [];
        if (this.eGui.firstChild != null) {
            if (this.eGui.firstChild.selectedOptions.length > 0) {
                for (var i = 0; i < this.eGui.firstChild.selectedOptions.length; i++) {
                    var selectedItemValueGridDD = this.eGui.firstChild.selectedOptions[i].value;
                    arr.push(selectedItemValueGridDD);
                    var commaValues = arr.join(",");
                }
                this.value = commaValues;
                return this.value;
            }
            else {
                return this.value = "";
            }
        }
        else {
            return this.value = "";
        }
    };
    MultiSelectDropDown.prototype.isPopup = function () {
        return true;
    };
    MultiSelectDropDown.prototype.search = function () {
        console.log(this);
    }
    MultiSelectDropDown.prototype.afterGuiAttached = function () {
        var changedWidth = this.myDDWid + "px";
        this.eGui.parentElement.style.width = changedWidth;
    };
    MultiSelectDropDown.prototype.init = function (params) {
        var multiDD: any = [];
        //var multiDD = params.context.componentParent.demandGeoFunction(params);
        multiDD = params.context.componentsCreateName[params.column.colId];

        if (params.value != undefined) {
            var myarr = params.value.split(",");
        }
        else if (params.value == undefined) {
            var myarr: any = [];
        }

        this.myDDWid = params.column.actualWidth;

        var tempElement = document.createElement("div");
        tempElement.setAttribute('id', 'multiSel');
        tempElement.setAttribute('class', 'multiselect singleSelect');

        var tempElementDiv = document.createElement("select");
        tempElementDiv.setAttribute('class', 'multiselect-ui custom-select form-control');
        tempElementDiv.setAttribute('id', 'multiselect-ui');
        //tempElementDiv.setAttribute('multiple', 'multiple');

        for (var i = 0; i < multiDD.length; i++) {
            var option = document.createElement("option");
            option.setAttribute("value", multiDD[i].itemName);
            // option.setAttribute("itemName", multiDD[i].itemName);
            option.text = multiDD[i].itemName;
            for (var j = 0; j < myarr.length; j++) {
                if (multiDD[i].itemName === myarr[j]) {
                    option.setAttribute("selected", "selected");
                }
            }
            tempElementDiv.appendChild(option);
            tempElement.appendChild(tempElementDiv);
        }
        loadJavaScript();
        this.eGui = tempElement;
    };
    return MultiSelectDropDown;
}

function getFamilyDropdown() {
    function MultiSelectDropDown() { }
    MultiSelectDropDown.prototype.getGui = function () {
        return this.eGui;
    };
    MultiSelectDropDown.prototype.getValue = function () {
        var arr = [];
        if (this.eGui.firstChild != null) {
            if (this.eGui.firstChild.selectedOptions.length > 0) {
                for (var i = 0; i < this.eGui.firstChild.selectedOptions.length; i++) {
                    var selectedItemValueGridDD = this.eGui.firstChild.selectedOptions[i].value;
                    arr.push(selectedItemValueGridDD);
                    var commaValues = arr.join(",");
                }
                this.value = commaValues;
                return this.value;
            }
            else {
                return this.value = "";
            }
        }
        else {
            return this.value = "";
        }
    };
    MultiSelectDropDown.prototype.isPopup = function () {
        return true;
    };
    MultiSelectDropDown.prototype.search = function () {
        console.log(this);
    }
    MultiSelectDropDown.prototype.afterGuiAttached = function () {
        var changedWidth = this.myDDWid + "px";
        this.eGui.parentElement.style.width = changedWidth;
    };
    MultiSelectDropDown.prototype.init = function (params) {
        var multiDD: any = [];
        //var multiDD = params.context.componentParent.demandGeoFunction(params);
        multiDD = params.context.componentsCreateName[params.column.colId];

        if (params.value != undefined) {
            var myarr = params.value.split(",");
        }
        else if (params.value == undefined) {
            var myarr: any = [];
        }

        this.myDDWid = params.column.actualWidth;

        var tempElement = document.createElement("div");
        tempElement.setAttribute('id', 'multiSel');
        tempElement.setAttribute('class', 'multiselect singleSelect');

        var tempElementDiv = document.createElement("select");
        tempElementDiv.setAttribute('class', 'multiselect-ui custom-select form-control');
        tempElementDiv.setAttribute('id', 'multiselect-ui');
        //tempElementDiv.setAttribute('multiple', 'multiple');

        for (var i = 0; i < multiDD.length; i++) {
            var option = document.createElement("option");
            option.setAttribute("value", multiDD[i].itemName);
            // option.setAttribute("itemName", multiDD[i].itemName);
            option.text = multiDD[i].itemName;
            for (var j = 0; j < myarr.length; j++) {
                if (multiDD[i].itemName === myarr[j]) {
                    option.setAttribute("selected", "selected");
                }
            }
            tempElementDiv.appendChild(option);
            tempElement.appendChild(tempElementDiv);
        }
        loadJavaScript();
        this.eGui = tempElement;
    };
    return MultiSelectDropDown;
}

//To Enable bootstrap multi select inside grid for checkbox
function loadJavaScript() {
    var url = '/dist/grid-multi-select.js';
    var s = document.createElement('script');
    s.setAttribute('src', url);
    document.head.appendChild(s);
}

