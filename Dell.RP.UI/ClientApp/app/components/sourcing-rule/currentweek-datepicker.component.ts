import { Component, OnInit } from '@angular/core';
import { ICellEditorAngularComp } from "ag-grid-angular/main";
@Component({
  selector: 'app-bootstrap-datepicker',
    template: `
        <datepicker (selectionDone)="onClick($event)" [showWeeks]="false" [hidden]="specificDate"></datepicker>`
})
export class CurrentweekDatepickerComponent implements ICellEditorAngularComp {
    private params: any;
    private specificDate: boolean;

   // public selectedDate: Date = new Date();
    public selectedDate: string = "";
    public currentWeek: string = "";

    agInit(params: any): void {
        this.params = params;
        this.specificDate = !this.params.node.data.fromDateVisible;            
    }

    getValue(): any {
       // var date = `${this.selectedDate.getMonth() + 1}/${this.selectedDate.getDate()}/${this.selectedDate.getFullYear()}`;
        return this.selectedDate ;
    }

    isPopup(): boolean {
        return true;
    }

    //ValidationRuleId(data :string)
    //{
    //    this.currentWeek = data;
    //}

    onClick(date: Date) {
        var ExistingSelectedDateString =""
        if (this.params.value != undefined && this.params.value != "") {
            ExistingSelectedDateString = this.params.value; //`${this.params.value.getMonth() + 1}/${this.params.value.getDate()}/${this.params.value.getFullYear()}`;
        }
        
        var selectedDateString = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`;
        if (ExistingSelectedDateString != selectedDateString) {
            this.selectedDate = selectedDateString;
        }
        else {
            this.selectedDate = "";
        }
        this.params.api.stopEditing();
    }
}
