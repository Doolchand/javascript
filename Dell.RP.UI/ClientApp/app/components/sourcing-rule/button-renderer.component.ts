﻿import { Component, AfterViewInit, ViewChild, ElementRef } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
@Component({
    selector: 'app-button-renderer',
    template: '<button type="button"(click)="onClick($event)" [hidden]="isDisable" > {{ label }}</button>',
})

export class ButtonRendererComponent implements ICellRendererAngularComp {

    params;
    label: string;
    isDisable: boolean = false;
    //public params: any;
    //private multiTier: boolean = false;


    agInit(params: any): void {
        this.params = params;
        this.label = this.params.label || null;
        if (params.context.componentParent !=undefined && params.context.componentParent.isUserHavingWriteAccess != undefined) {
            this.isDisable = !(params.context.componentParent.isUserHavingWriteAccess);
        }
    }

    constructor() {
        console.log("Button");
    }

    refresh(params?: any): boolean {
        return true;
    }

    onClick($event) {
        if (this.params.onClick instanceof Function) {
            // put anything into params u want pass into parents component
            const params = {
                event: $event,
                rowData: this.params.node.data
                // ...something
            }
            this.params.onClick(params);

        }
    }
}