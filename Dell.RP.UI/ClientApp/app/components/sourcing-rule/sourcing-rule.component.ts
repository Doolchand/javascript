﻿import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { GridOptions, RowNode } from "ag-grid/main";
import { SessionStorageService } from 'ngx-webstorage';
import * as XLSX from 'xlsx';
import { UserRoles } from '../../Model/user-roles';
import { SourcingRuleService } from '../../services/sourcing-rule.service';
import { UtilitiesService } from '../../services/utilities.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetErrorGridService } from '../../services/get-error-grid.service';
//import { treeViewData, node, children, FulFillmentSearchInput } from '../../Model/tree-view-data';

import { treeViewData } from '../../Model/treeview/treeViewData';
import { node } from '../../Model/treeview/node';
import { children } from '../../Model/treeview/children';
import { FulFillmentSearchInput } from '../../Model/treeview/FulFillmentSearchInput';

import { TreeViewComponent } from '../../components/tree-view/tree-view.component';
import { SourcingRuleGrid } from '../../Model/SourcingRuleGrid';
import { SourcingRuleGridCreate } from '../../Model/SourcingRuleGridCreate';
import { SourcingRuleLineItem } from '../../Model/SourcingRuleLineItem';
import { Conditional } from '@angular/compiler';
import { CurrentweekDatepickerComponent } from '../sourcing-rule/currentweek-datepicker.component';
import { ButtonRendererComponent } from '../sourcing-rule/button-renderer.component';
import { forEach } from '@angular/router/src/utils/collection';
declare var jQuery: any;


@Component({
    selector: 'app-sourcing-rule',
    templateUrl: './sourcing-rule.component.html',
    styleUrls: ['./sourcing-rule.component.css']
})
/** SourcingRule component*/
export class SourcingRuleComponent {
    @ViewChild('fakeClick') fakeClick: ElementRef;
    @ViewChild('file') FileName: any;
    @ViewChild(TreeViewComponent) treeviewChild: TreeViewComponent;
    Sites = [];

    //Error/Success Message     
    colorCode: string;
    successMsg: string = "";
    public hideShowAlertMsg = false;
    returnMsg: string = "";

    //Channel Geoogrpahy search
    ChannelGeosData = [];
    selectedChannelGeo = [];

    //Product Tree Search
    selectedGMPProductTreeData: string = "ALL_PRODUCT";
    selectedGMPProductTreeData_label: string = this.selectedGMPProductTreeData; // = "ALL_PRODUCT(575)";
    nodesProduct: node[];
    onselectedGMPChannelGeoTreeDataChanged(selectedTreeValue: string): void {
        this.selectedGMPProductTreeData = selectedTreeValue;
        this.selectedGMPProductTreeData_label = this.selectedGMPProductTreeData.split('__')[0];
    }

    onselectedGMPProductTreeDataChanged(selectedTreeValue: string): void {
        this.selectedGMPProductTreeData = selectedTreeValue == "" ? "ALL_PRODUCT" : selectedTreeValue;
        this.selectedGMPProductTreeData_label = this.selectedGMPProductTreeData.split('__')[0];
        if (this.rowData != undefined) { this.rowData = []; }
    }

    //tree variables
    nodes: node[];
    treeViewNodesFilterdData: node[];
    treeViewDetailData: treeViewData = new treeViewData();
    options: any;


    //

    //Channel Product Dropdown Search
    ChannelProductsData = [];
    selectedChannelProducts = [];
    dropdownSettings = {};
    ChannelGeoDropdownSettings = {};

    //--- Create variable section  
    selectedChannelGeoCreate = [];
    //Product Tree Search
    selectedGMPProductCreateTreeData: string;
    selectedGMPProductCreateTreeData_label: string = "";
    onselectedGMPProductCreateTreeDataChanged(selectedTreeValue: string): void {
        this.selectedGMPProductCreateTreeData = selectedTreeValue;
        this.selectedGMPProductCreateTreeData_label = this.selectedGMPProductCreateTreeData.split('__')[0];
        if (this.rowDataCreate != undefined && !this.isCopyActiveEnable) { this.rowDataCreate = []; }
    }

    private gridOptionsCreate: GridOptions;
    private rowDataCreate;
    private gridColumnApiCreate;
    private gridApiCreate;
    private contextCreate;
    private getRowNodeIdCreate;
    rowCountFileGenCreate: number = -1;
    private contextNameCreate;
    sourcingRuleGridsCreate: SourcingRuleGrid[];
    sourcingRuleLineItemsCreate: SourcingRuleLineItem[];
    private frameworkComponentsNameCreate;
    private componentsCreate;
    IsHideAddBlankRow: boolean = true;
    addedIndex: number = -1;
    isSaveVisible: boolean = true;
    isCopyActiveEnable: boolean = false;
    // ---   Create variable section  End

    // Main grid 
    private gridOptions: GridOptions;
    private rowData;
    private gridColumnApi;
    private gridApi;
    private context;
    private getRowNodeId;
    rowCountFileGen: number = -1;
    private contextName;
    sourcingRuleGrids: SourcingRuleGrid[];
    sourcingRuleLineItems: SourcingRuleLineItem[];
    private frameworkComponentsName;
    rulePriorityRegex = /^[0-9]{1,2}$|^100$/;
    excelExportDisable: boolean = false;

    //Roles
    userRoles: UserRoles[];
    screenId = "3001";
    isUserHavingWriteAccess: boolean = false;
    isActiveScreen: boolean = true;
    userHavingWriteAccess: string[];
    listOfRoleHavingAccess: string[];
    userTenantTypeAccessList: string[];
    isUserHavingUserMaintenance: boolean = false;

    /** SourcingRule ctor */
    constructor(private _sourcingRuleService: SourcingRuleService, private _router: Router, private _sessionSt: SessionStorageService) {

       this.Authorization();
        //Main Grid Section
        this.MainGridDefination();

        this.CreateGridDefination();
    }

    ngOnInit(): void {

        this.dropdownSettings = {
            singleSelection: true,
            text: "ALL",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class singleSelect"
        };

        this.ChannelGeoDropdownSettings = {
            singleSelection: true,
            //  text: "ALL",
            //selectAllText: 'Select All',
            //unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class singleSelect"
        };

        // this.GetChannelProducts();
        this.GetChannelGeos();
        this.GetSites();
    }


    Authorization() {
        var rolesHavingWriteAccess = this._sessionSt.retrieve('screenAuthorization') != null ? this._sessionSt.retrieve('screenAuthorization') : [];
        this.userHavingWriteAccess = rolesHavingWriteAccess[rolesHavingWriteAccess.findIndex(x => x.functionId == this.screenId)]["roleId"].split(',');
        this.userTenantTypeAccessList = this._sessionSt.retrieve('userTenantTypeAccessList') != null ? this._sessionSt.retrieve('userTenantTypeAccessList') : [];

        if (this._sessionSt.retrieve('userRoles') === null) {
            this.isActiveScreen = false;
            this._router.navigateByUrl('/errorMessage');
            return;
        }
        else {
            this.userRoles = this._sessionSt.retrieve('userRoles');
            for (var i = 0; i < this.userRoles.length; i++) {
                if (this.userHavingWriteAccess.includes(this.userRoles[i].roleId.toString())) {
                    this.isUserHavingWriteAccess = true;
                    return;
                }
            }

            if (!(this.isUserHavingWriteAccess || this.userTenantTypeAccessList.includes("GMP"))) {
                this.isActiveScreen = false;
                this._router.navigateByUrl('/errorMessage');
                return;
            }
        }
    }

    GetChannelProducts() {
        this.ChannelProductsData = [];
        this._sourcingRuleService.GetChannelProductsDDL()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        this.ChannelProductsData = [];
                        for (var i = 0; i < data.length; i++) {

                            this.ChannelProductsData.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })

                        }
                    }
                },
                err => console.log(err),
            );
    }

    GetChannelGeos() {
        this.ChannelGeosData = [];
        this._sourcingRuleService.GetChannelGeosDDL()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        this.ChannelGeosData = [];
                        for (var i = 0; i < data.length; i++) {

                            this.ChannelGeosData.push({
                                id: data[i].id,
                                itemName: data[i].itemName + "(" + data[i].id + ")"
                            })

                        }
                    }
                },
                err => console.log(err),
            );
    }

    GetSites() {
        this.Sites = [];
        this._sourcingRuleService.GetSites()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        for (var i = 0; i < data.length; i++) {

                            this.Sites.push({
                                id: data[i].siteId,
                                itemName: data[i].siteName
                            })

                        }
                    }
                },
                err => console.log(err),
            );
    }

    getProductTreeDetail() {
        this.ClearMessage();
        //this.nodesProduct = [];
        //var msgNote = "< table > <tr><td><b>Instruction < /b> < /td > <td>To move up the Channel Hierarchy to a Level that is above the selected level, copy and paste 'ALLCHANNELS(1864)' into the data entry field.Then click on the 'Search' Button to be taken to the top of the Hierarchy, where you can expand down to the desired level.If the < Enter > button on your keyboard is used instead of 'Search' without first clicking on the page, a debugging screen will be presented and you’ll need to use the < Backspace > button to return to the previous screen. < /td>< /tr>< /table>";
        //this.treeViewDetailData.isSearchEnable = true;
        //this.treeViewDetailData.msgNote = "Instructions : To move up the Channel Hierarchy to a Level that is above the selected level, copy and paste 'ALLCHANNELS(1864)' into the data";
        //this.treeViewDetailData.treeViewType = "GMP_SR_Product";
        //this.treeViewDetailData.headerMsg = "";
        this.nodesProduct = [];

        //this.treeViewDetailData.selectedDPValue = this.selectedProduct[0].itemName;
        this.treeViewDetailData.nodesData = this.nodesProduct;
        this.treeViewDetailData.header = "Product Tree Popup";
        this.treeViewDetailData.isSearchEnable = true;

        this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All PRODUCT LOB Names";
        this.treeViewDetailData.headerMsg = "Please enter PRODUCT LOB Name!";
        this.treeViewDetailData.treeViewType = "GMP_SR_Product";

        this.treeviewChild.bindTreeData();
    }


    ///----- Create Section
    getCreateProductTreeDetail() {
        this.nodesProduct = [];
        this.treeViewDetailData.nodesData = this.nodesProduct;
        this.treeViewDetailData.header = "Product Tree Popup";
        this.treeViewDetailData.isSearchEnable = true;

        this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All PRODUCT LOB Names";
        this.treeViewDetailData.headerMsg = "Please enter PRODUCT LOB Name!";
        this.treeViewDetailData.treeViewType = "GMP_SR_Product_Create";
        this.treeviewChild.bindTreeData();
    }

    CreateGridDefination() {
        this.gridOptionsCreate = <GridOptions>{
            contextNameCreate: {
                componentParentNameCreate: this
            },
            rowSelection: 'single',
            stopEditingWhenGridLosesFocus: true,
        };
        this.gridOptionsCreate.rowData = [];
        // this.gridOptionsCreate.rowSelection = "multiple";
        this.gridOptionsCreate.paginationPageSize = 20;
        this.gridOptionsCreate.rowGroupPanelShow = "always";
        this.gridOptionsCreate.pivotPanelShow = "always";
        this.gridOptionsCreate.columnDefs = [
            {
                headerName: "",
                width: 35,
                pinned: "left",
                suppressSizeToFit: true,
                suppressFilter: true,
                headerCheckboxSelectionFilteredOnly: true,
                editable: false,
                checkboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                },
                headerCheckboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                }
            },
            {
                headerName: "Need to Remove",
                field: "sourcingRuleKey",
                tooltipField: "sourcingRuleKey",
                editable: false,
                hide: true
            },
            {
                headerName: "Channel",
                field: "channel",
                tooltipField: "channel",
                editable: false,
                hide: true,
            },
            {
                headerName: "Product",
                field: "product",
                tooltipField: "product",
                width: 180,
                editable: false,
                hide: true,
            },
            {
                headerName: "From Date",
                field: "fromDateStr",
                tooltipField: "fromDateStr",
                width: 180,
               // cellEditor: "datePicker",
                cellEditorFramework: CurrentweekDatepickerComponent,
                editable: function (Rownode) {
                    return Rownode.node.data.fromDateVisible;
                },
            },
            {
                headerName: "To Date",
                field: "toDateStr",
                tooltipField: "toDateStr",
                width: 110,
               // cellEditor: "datePicker",
                cellEditorFramework: CurrentweekDatepickerComponent,
                editable: function (Rownode) {
                    console.log(RowNode);
                    return Rownode.node.data.toDateVisible;
                },
            },
            {
                headerName: "Kitting",
                field: "kitSite",
                tooltipField: "kitSite",
                width: 150,
                cellClass: "multi-select-grid",
                cellEditor: "singleSelectDDNamed",
              //  onCellClicked: this.oncellEditingStartedCreate,
                colId: "Sites",
                editable: function (Rownode) {
                    if (Rownode.node.data.kitSiteVisible != undefined) {
                        return Rownode.node.data.kitSiteVisible;
                    }
                    return true;
                },
                // editable: false,
            },
            {
                headerName: "Boxing",
                field: "boxSite",
                tooltipField: "boxSite",
                width: 150,
                cellClass: "multi-select-grid",
                cellEditor: "singleSelectDDNamed",
                colId: "Sites",
                editable: function (Rownode) {
                    console.log(RowNode);
                    return Rownode.node.data.boxSiteVisible;
                },
            },
            {
                headerName: "KB Split",
                field: "boxSplit",
                tooltipField: "boxSplit",
                width: 100,
                editable: function (Rownode) {
                    console.log(RowNode);
                    return Rownode.node.data.boxSplitVisible;
                },
            },
            {
                headerName: "Merging",
                field: "mergeSite",
                tooltipField: "mergeSite",
                width: 150,
                cellClass: "multi-select-grid",
                cellEditor: "singleSelectDDNamed",
                colId: "Sites",
                editable: function (Rownode) {
                    console.log(RowNode);
                    return Rownode.node.data.mergeSiteVisible;
                },
            },
            {
                headerName: "BM Split",
                field: "mergeSplit",
                tooltipField: "mergeSplit",
                width: 100,
                editable: function (Rownode) {
                    console.log(RowNode);
                    return Rownode.node.data.mergeSplitVisible;
                },
            },
            {
                headerName: "Last Modified By",
                field: "sysLastModifiedby",
                tooltipField: "sysLastModifiedby",
                width: 150,
                editable: false,
                hide: true,
            },
            {
                headerName: "Last Modified Date",
                field: "sysLastModifiedDate",
                tooltipField: "sysLastModifiedDate",
                width: 150,
                editable: false,
                hide: true,
            },
            {
                headerName: "Copy",
                cellRenderer: 'buttonRenderer',
                hide: true,
                width: 100,
                cellRendererParams: {
                    onClick: this.CopyRuleCreate.bind(this),
                    label: 'Add New ROW'
                }
            },
            ,
            {
                headerName: "Delete",
                cellRenderer: 'buttonRenderer',
                width: 100,
                cellRendererParams: {
                    onClick: this.DeleteRuleCreate.bind(this),
                    label: 'Delete'
                }
            },
        ];
        this.contextNameCreate = { componentParentCreate: this };
        this.componentsCreate = {
            singleSelectDDNamed: singleSelectDDNamed(),
            datePicker: getDatePicker()
        };
        this.frameworkComponentsNameCreate = {
            buttonRenderer: ButtonRendererComponent,
        };
    }

    RetriveClick(event) {
        this.ClearMessage();
        this.sourcingRuleGridsCreate = [];
        this.addedIndex = -1;

        var productID = this.selectedGMPProductCreateTreeData_label.split('(')[1].split(')')[0];

        // this._sourcingRuleService.GetSingleRuleLineItems("1865", "60825")
        this._sourcingRuleService.GetSingleRuleLineItems(this.selectedChannelGeoCreate[0].id, productID)
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        this.sourcingRuleGridsCreate = data;
                        this.AppendIndexInData(data);
                        this.IsHideAddBlankRow = false;
                    }
                    else {
                        this.IsHideAddBlankRow = true;
                    }
                },
                err => console.log(err),
            );
    }

    AppendIndexInData(data: any[]) {
        this.ClearMessage();
        this.rowDataCreate = [];
        for (var i = 0; i < data.length; i++) {
            data[i].sourcingRuleKey = i;
        }
        this.rowDataCreate = data;

    }

    createClearClick() {
        this.ClearMessage();
        this.rowDataCreate = [];
        this.selectedChannelGeoCreate = [];
        this.selectedGMPProductCreateTreeData_label = '';
        this.addedIndex = -1;
        this.isCopyActiveEnable = false;
    }



    CopyRuleCreate(a: any) {
        this.ClearMessage();
        var newRowIndex = a.rowData.sourcingRuleKey + 1;
        var selectedRule = JSON.parse(JSON.stringify(a.rowData));
        this.addedIndex += 1;
        selectedRule.sourcingRuleKey = "Add" + this.addedIndex;
        // let selctedIndex = -1;
        // var newRowdata = JSON.parse(JSON.stringify(this.rowDataCreate))
        if (this.rowDataCreate != undefined) {
            this.rowDataCreate.splice(selectedRule.sourcingRuleKey + 1, 0, selectedRule)
            //// //for (var i = 0; i < this.rowDataCreate.length; i++) {
            //// //    if (selectedRule.sourcingRuleKey < this.rowDataCreate[i].sourcingRuleKey)
            //// //    {
            //// //        this.rowDataCreate[i] = newRowdata[i - 1];
            //// //    }
            //// //    if (selectedRule.sourcingRuleKey === this.rowDataCreate[i].sourcingRuleKey + 1 ) {
            //// //        this.rowDataCreate[i] = selectedRule;
            //// //        this.rowDataCreate[i].sourcingRuleKey = "";
            //// //        selctedIndex = i;
            //// //    }
            //// //    else {
            //// //        this.rowDataCreate[i] = this.rowDataCreate[i];
            //// //    }
            //// //}            
            ////// this.rowDataCreate[4].sourcingRuleKey = 15;
            //// //for (var i = selectedRule.sourcingRuleKey+ 1; i < this.rowDataCreate.length - selectedRule.sourcingRuleKey; i++) {
            //// //    this.rowDataCreate[i +1].sourcingRuleKey = i + 1;
            //// //}
            ////// this.gridOptionsCreate.api.updateRowData({ add: selectedRule, addIndex: selectedRule.sourcingRuleKey + 1 });

            this.gridApiCreate.updateRowData({
                add: [selectedRule], addIndex: newRowIndex,
                selected: true
            });
            this.gridApiCreate.paginationGoToLastPage();
            this.gridApiCreate.forEachNode(function (node) {
                if (node.data.sourcingRuleKey === "") {
                    node.setSelected(true);
                }
            });

            //this.gridOptionsCreate.api.setRowData(this.rowDataCreate);
        }
    }

    onGridReadyCreate(params) {
        this.gridApiCreate = params.api;
        this.gridColumnApiCreate = params.columnApi;
        //  this.autoSizeAllFactory();
        //this.sizeToFit();
    }


    autoSizeAllFactory() {
        var allColumnIds = [];
        this.gridColumnApiCreate.getAllColumns().forEach(function (column) {
            allColumnIds.push(column.colId);
        });
        this.gridColumnApiCreate.autoSizeColumns(allColumnIds);
    }

    oncellEditingStartedCreate(event: any) {
        event.node.setSelected(true);
    }

    onSelectionChangedCreate(event: any) {
        console.log(event);
    }

    SaveClick(event: any) {
        this.ClearMessage();
        let selectedRow = this.rowDataCreate;
        var productID = this.selectedGMPProductCreateTreeData_label.split('(')[1].split(')')[0];
        this.ModifyProductChannelData(selectedRow);
        if (this.isCopyActiveEnable) {
            this._sourcingRuleService.GetSingleRuleLineItems(this.selectedChannelGeoCreate[0].id, productID)
                .subscribe(
                    (data: any) => {
                        if (data != null) {
                            this.sourcingRuleGridsCreate = data;
                            if (data.length > 0) {
                                this.DisplayMessage("Data for selected channel and product already exist, please delete it first.", false);
                            }
                            else {
                                this.Save(selectedRow);
                            }
                        }
                        else {
                            this.IsHideAddBlankRow = true;
                        }
                    },
                    err => console.log(err),
                );
        }
        else {
            this.Save(selectedRow);
        }
        
    }

    Save(selectedRow :any[]) {
        //this.ClearMessage();
        //let selectedRow = this.rowDataCreate; //  this.gridOptionsCreate.api.getSelectedRows();
        //var selectedRowAfterModification = JSON.parse(JSON.stringify(selectedRow));
        //selectedRowAfterModification = this.BeforeSave(selectedRowAfterModification);
        if (this.IsValidToSave(selectedRow)) {
            this._sourcingRuleService.SaveSourcingRule(selectedRow)
                .map((data: any) => data.json()).subscribe((data: any) => {
                    if (data != null && data.result != undefined) {

                        var message = data.result.message;
                        if (data.result.isSuccess) {
                            message = "Rule successfully saved.";

                            if (this.selectedGMPProductTreeData_label != "" && this.selectedChannelGeo.length > 0 ) {
                                this.GetMainGridData();
                            }
                            this.createClearClick();
                        }
                        this.DisplayMessage(message, data.result.isSuccess);
                    }
                },
                    err => console.log(err))
        }
    }

    ModifyProductChannelData(selectedRow: any[]) {
        var productID = this.selectedGMPProductCreateTreeData_label.split('(')[1].split(')')[0];
        this.selectedChannelGeoCreate[0].id;
        for (var i = 0; i < selectedRow.length; i++) {
            selectedRow[i].channel = this.selectedChannelGeoCreate[0].id;
            selectedRow[i].channelId = this.selectedChannelGeoCreate[0].id;
            selectedRow[i].channelName = this.selectedChannelGeoCreate[0].itemName;
            selectedRow[i].product = this.selectedGMPProductCreateTreeData_label;
            selectedRow[i].productID = this.selectedGMPProductCreateTreeData_label.split('(')[1].split(')')[0];
            selectedRow[i].productName = this.selectedGMPProductCreateTreeData_label.split('(')[1].split(')')[0];
        }
    }

    IsValidToSave(selectedRow: any[]) {
        for (var i = 0; i < selectedRow.length; i++) {
            if (!String(selectedRow[i].boxSplit).match(this.rulePriorityRegex) || selectedRow[i].boxSplit == "0") {
                this.DisplayMessage("KB Split percentage should be in between 1 to 100 !", false);
                return false;
            }
            else if (!String(selectedRow[i].mergeSplit).match(this.rulePriorityRegex) || selectedRow[i].mergeSplit == "0") {
                this.DisplayMessage("BM Split percentage should be in between 1 to 100 !", false);
                return false;
            }
        }
        return true;
    }

    AddNewRule() {
        this.ClearMessage();
        this.sourcingRuleGridsCreate = [];
        // this.addedIndex = -1;
        var productID = this.selectedGMPProductCreateTreeData_label.includes('(') ? this.selectedGMPProductCreateTreeData_label.split('(')[1].split(')')[0] : "575";
        if (this.rowDataCreate != undefined && this.rowDataCreate.length > 1) {
            this.AddRule();
        }
        else {
            this._sourcingRuleService.GetSingleRuleLineItems(this.selectedChannelGeoCreate[0].id, productID)
                .subscribe(
                    (data: any) => {
                        if (data != null) {
                            this.sourcingRuleGridsCreate = data;
                            if (data.length > 0) {
                                this.DisplayMessage("Data for selected channel and product already exist, please delete it first.", false);
                            }
                            else {
                                this.AddRule();

                            }
                        }
                        else {
                            this.IsHideAddBlankRow = true;
                        }
                    },
                    err => console.log(err),
                );
        }
    }

    AddRule() {
        this.ClearMessage();
        this.rowDataCreate = this.rowDataCreate === undefined ? [] : this.rowDataCreate;
        this.rowDataCreate.push(this.GetBlankSourcingRule());
        this.gridOptionsCreate.api.setRowData(this.rowDataCreate);
    }

    AddKitting() {
        this.ClearMessage();
        var addKitting = JSON.parse(JSON.stringify(this.gridOptionsCreate.api.getSelectedRows()));
        if (addKitting.length > 0) {

            addKitting[0].fromDateVisible = false;
            addKitting[0].toDateVisible = false;

            addKitting[0].kitSite = "";
            addKitting[0].kitSiteVisible = true;

            addKitting[0].boxSite = "";
            addKitting[0].boxSiteVisible = true;

            addKitting[0].boxSplit = "";
            addKitting[0].boxSplitVisible = true;

            addKitting[0].mergeSite = "";
            addKitting[0].mergeSiteVisible = true;

            addKitting[0].mergeSplit = "";
            addKitting[0].mergeSplitVisible = true;
            this.addedIndex += 1;
            addKitting[0].sourcingRuleKey = "Add" + this.addedIndex;

            this.rowDataCreate = this.rowDataCreate === undefined ? [] : this.rowDataCreate;
            this.rowDataCreate.push(addKitting[0]);
            this.gridOptionsCreate.api.setRowData(this.rowDataCreate);

        }
        else {
            this.DisplayMessage("Please select a rule where kitting site is to be added.", false);
        }
    }

    AddBoxing() {
        this.ClearMessage();
        var addKitting = JSON.parse(JSON.stringify(this.gridOptionsCreate.api.getSelectedRows()));

        if (addKitting.length > 0) {

            if (addKitting[0].kitSite === "" || addKitting[0].kitSite === undefined) {
                this.DisplayMessage("Please select a valid Kit site", false);
            }
            else {

                addKitting[0].fromDateVisible = false;
                addKitting[0].toDateVisible = false;
                // addKitting[0].kitSite = "";
                addKitting[0].kitSiteVisible = false;

                addKitting[0].boxSite = "";
                addKitting[0].boxSiteVisible = true;

                addKitting[0].boxSplit = "";
                addKitting[0].boxSplitVisible = true;

                addKitting[0].mergeSite = "";
                addKitting[0].mergeSiteVisible = true;

                addKitting[0].mergeSplit = "";
                addKitting[0].mergeSplitVisible = true;

                this.addedIndex += 1;
                addKitting[0].sourcingRuleKey = "Add" + this.addedIndex;

                this.rowDataCreate = this.rowDataCreate === undefined ? [] : this.rowDataCreate;
                this.rowDataCreate.push(addKitting[0]);
                this.gridOptionsCreate.api.setRowData(this.rowDataCreate);
            }
        }
        else {
            this.DisplayMessage("Please select a rule where kitting site is to be added.", false);
        }
    }

    AddMergeing() {
        this.ClearMessage();
        var addKitting = JSON.parse(JSON.stringify(this.gridOptionsCreate.api.getSelectedRows()));

        if (addKitting.length > 0) {

            if (addKitting[0].kitSite === "" || addKitting[0].kitSite === undefined) {
                this.DisplayMessage("Please select a valid Kit site", false);
            }
            else if (addKitting[0].boxSite === "" || addKitting[0].boxSite === undefined) {
                this.DisplayMessage("Please select a valid boxing site", false);
            }
            else {

                addKitting[0].fromDateVisible = false;
                addKitting[0].toDateVisible = false;
                //addKitting[0].kitSite = "";
                addKitting[0].kitSiteVisible = false;

                //addKitting[0].boxSite = "";
                addKitting[0].boxSiteVisible = false;

                //addKitting[0].boxSplit = "";
                addKitting[0].boxSplitVisible = false;

                addKitting[0].mergeSite = "";
                addKitting[0].mergeSiteVisible = true;

                addKitting[0].mergeSplit = "";
                addKitting[0].mergeSplitVisible = true;
                this.addedIndex += 1;
                addKitting[0].sourcingRuleKey = "Add" + this.addedIndex;

                this.rowDataCreate = this.rowDataCreate === undefined ? [] : this.rowDataCreate;
                this.rowDataCreate.push(addKitting[0]);
                this.gridOptionsCreate.api.setRowData(this.rowDataCreate);
            }


        }
        else {
            this.DisplayMessage("Please select a rule where Mergeing site is to be added.", false);
        }
    }

    GetBlankSourcingRule() {
        this.ClearMessage();
        this.addedIndex += 1;
        var prodID = this.selectedGMPProductCreateTreeData_label.includes('(') ? this.selectedGMPProductCreateTreeData_label.split('(')[1].split(')')[0] : "575";
        var prodName = this.selectedGMPProductCreateTreeData_label.includes('(') ? this.selectedGMPProductCreateTreeData_label.split('(')[0].split(')')[0] : this.selectedGMPProductCreateTreeData_label;
        var chanName = this.selectedChannelGeoCreate[0].itemName.includes('(') ? this.selectedChannelGeoCreate[0].itemName.split('(')[0].split(')')[0] : this.selectedChannelGeoCreate[0].itemName;
        var blankSouringData = {
            boxSite: "",
            boxSplit: "",
            channel: this.selectedChannelGeoCreate[0].itemName,
            channelName: chanName,
            channelId: this.selectedChannelGeoCreate[0].id,
            product: this.selectedGMPProductCreateTreeData_label,
            productID: prodID,
            //////productName: prodName,
           // fromDateStr: "1/1/0001",
            //toDateStr: "1/1/0001",
            //////kitSite: "",
            mergeSite: "",
           ////// mergeSplit: "",
            sourcingRuleKey: "Add" + this.addedIndex,
            fromDateVisible: true,
            toDateVisible: true,
            kitSiteVisible: true,
            boxSiteVisible: true,
            boxSplitVisible: true,
            mergeSiteVisible: true,
            mergeSplitVisible: true,
        };
        return blankSouringData;
    }

    //DeleteRuleCreate(event: any) {
    //    this.ClearMessage();
    //    var selectedRule = this.gridOptionsCreate.api.getSelectedNodes();
    //    var deleteRule = JSON.parse(JSON.stringify(event.rowData));
    //    var listOfSelectedRuleSourcingRuleKey = [];
    //    if (selectedRule.length > 0) {
    //        for (var i = 0; i < selectedRule.length; i++) {
    //            listOfSelectedRuleSourcingRuleKey.push(selectedRule[i].data.sourcingRuleKey);
    //        }
    //    }

    //    if (this.rowDataCreate != undefined) {
    //       // this.gridOptionsCreate.api.removeItems(deleteRule);
    //        this.rowDataCreate = this.rowDataCreate.filter(x => x.sourcingRuleKey != deleteRule.sourcingRuleKey);            
    //    }
    //    if (this.rowDataCreate.length > 1) {
    //        this.DisplayMessage("Only,one record can be deleted", false);
    //    }
    //    else {
    //        this.gridOptionsCreate.api.setRowData(this.rowDataCreate);
    //        this.gridApiCreate.paginationGoToLastPage();

    //        this.gridApiCreate.forEachNode(function (node) {
    //            if (listOfSelectedRuleSourcingRuleKey.includes(node.data.sourcingRuleKey)) {
    //                node.setSelected(true);
    //            }
    //        });
    //    }
    //}
    DeleteRuleCreate(event: any) {
        this.ClearMessage();
        var selectedRule = this.gridOptionsCreate.api.getSelectedNodes();
        var deleteRule = JSON.parse(JSON.stringify(event.rowData));
        //var listOfSelectedRuleSourcingRuleKey = [];
        //if (selectedRule.length > 0) {
        //    for (var i = 0; i < selectedRule.length; i++) {
        //        listOfSelectedRuleSourcingRuleKey.push(selectedRule[i].data.sourcingRuleKey);
        //    }
        //}
        if (selectedRule.length > 0) {
            if (selectedRule[0].data.sourcingRuleKey != deleteRule.sourcingRuleKey) {
                var alertMsg = "";
                alertMsg = 'Selected row is not same as row delete button clicked, are you sure you want to delete clicked row data ?';

                if (window.confirm(alertMsg)) {
                    this.DeleteSelectedRowCreate(deleteRule.sourcingRuleKey);
                }
            }
            else {
                this.DeleteSelectedRowCreate(deleteRule.sourcingRuleKey);
            }
        }
        else {
            this.DeleteSelectedRowCreate(deleteRule.sourcingRuleKey);
        }
    }

    DeleteSelectedRowCreate(sourcingRuleKey: any) {
        if (this.rowDataCreate != undefined) {
            this.rowDataCreate = this.rowDataCreate.filter(x => x.sourcingRuleKey != sourcingRuleKey);
        }

        this.gridOptionsCreate.api.setRowData(this.rowDataCreate);
        // this.gridApiCreate.paginationGoToLastPage();

        //this.gridApiCreate.forEachNode(function (node) {
        //    if (listOfSelectedRuleSourcingRuleKey.includes(node.data.sourcingRuleKey)) {
        //        node.setSelected(true);
        //    }
        //});
    }

    BeforeSave(data: any[]) {
        var ChannelProdCombination = "";
        var ChannelProdDateCombination = "";
        var ChannelProdDateKitCombination = "";
        var ChannelProdDateKitBoxCombination = "";
        var commandArgKey = "";
        for (var i = 0; i < data.length; i++) {

            let ChannelProd = data[i].ChannelId + "_" + data[i].ProductID;
            let ChannelProdDate = data[i].ChannelId + "_" + data[i].ProductID + "_" + data[i].FromDateCalId + "_" + data[i].ToDateCalId;
            let ChannelProdDateKit = data[i].ChannelId + "_" + data[i].ProductID + "_" + data[i].FromDateCalId + "_" + data[i].ToDateCalId + "_" + data[i].KitSite;
            let ChannelProdDateKitBox = data[i].ChannelId + "_" + data[i].ProductID + "_" + data[i].FromDateCalId + "_" + data[i].ToDateCalId + "_" + data[i].KitSite + "_" + data[i].BoxSite;
            commandArgKey = data[i].SourcingRuleKey;

            if (ChannelProd === ChannelProdCombination) {
                data[i].CopyButtonVisible = true;
                data[i].DeleteButtonVisible = true;

                data[i].EditButtonVisible = false;
            }
            if (ChannelProdDate === ChannelProdDateCombination) {
                data[i].ChannelVisible = false;
                data[i].ProductVisible = false;
                data[i].ToDateVisible = false;
                data[i].FromDateVisible = false;
                data[i].ToDateStr = "";
                data[i].FromDateStr = "";

                if (ChannelProdDateKit === ChannelProdDateKitCombination) {
                    //data[i].BoxSplitVisible = false;
                    //if (commandArgKey.Equals(ChannelProdDateCombination))
                    //{
                    //    data[i].BoxSplitVisible = true;
                    //    data[i].MergeSiteVisible = true;
                    //}
                    data[i].KitSiteVisible = false;
                    data[i].KitSite = "";
                    if (ChannelProdDateKitBox === ChannelProdDateKitBoxCombination) {
                        data[i].BoxSiteVisible = false;
                        data[i].BoxSite = "";
                    }
                }
            }
            ChannelProdCombination = ChannelProd;
            ChannelProdDateCombination = ChannelProdDate;
            ChannelProdDateKitCombination = ChannelProdDateKit;
            ChannelProdDateKitBoxCombination = ChannelProdDateKitBox;
        }
        return data;
    }

    onCreateItemSelect(event: any) {
        if (this.rowDataCreate != undefined && !this.isCopyActiveEnable) {
            this.rowDataCreate = [];
        }
    }

    OnCreateItemDeSelect(event: any) {
        if (this.rowDataCreate != undefined && !this.isCopyActiveEnable) { this.rowDataCreate = []; }
    }
    ///---- Create Section ends



    DisplayMessage(msg, isSuccess) {
        this.hideShowAlertMsg = true;
        this.successMsg = msg;
        this.colorCode = isSuccess == true ? "success" : "danger";
    }

    ClearMessage() {
        this.hideShowAlertMsg = false;
        this.successMsg = "";
        if (this.rowData != undefined && this.rowData.length > 0) {
            this.excelExportDisable = true;
        }
        else {
            this.excelExportDisable = false;
        }
    }

    //-- Main grid section

    MainGridDefination() {
        this.gridOptions = <GridOptions>{
            contextName: {
                componentParentName: this
            },
        };
        this.gridOptions.rowData = [];
        this.gridOptions.rowSelection = "multiple";
        this.gridOptions.paginationPageSize = 14;
        this.gridOptions.rowGroupPanelShow = "always";
        this.gridOptions.pivotPanelShow = "always";
        this.gridOptions.columnDefs = [
            {
                headerName: "Channel",
                field: "channel",
                tooltipField: "channel",
                width: 120,
                editable: false,
                //cellRenderer: function (params) {
                //    if (params.context.componentParent.regionSelected == "AMER") {
                //        var link = params.context.componentParent.myAmerNasUrl + params.value;
                //        return '<a href=' + link + '>' + params.value + '</a>'
                //    }
                //    else if (params.context.componentParent.regionSelected == "APJAP") {
                //        var link = params.context.componentParent.myApjNasUrl + params.value;
                //        return '<a href=' + link + '>' + params.value + '</a>'
                //    }
                //    else if (params.context.componentParent.regionSelected == "EMEAF") {
                //        var link = params.context.componentParent.myEmeaNasUrl + params.value;
                //        return '<a href=' + link + '>' + params.value + '</a>'
                //    }
                //}
            },
            {
                headerName: "Product",
                field: "product",
                tooltipField: "product",
                width: 150,
                editable: false,
            },
            {
                headerName: "From Date",
                field: "fromDateStr",
                tooltipField: "fromDateStr",
                width: 80,
                editable: false,
            },
            {
                headerName: "To Date",
                field: "toDateStr",
                tooltipField: "toDateStr",
                width: 80,
                editable: false
            },
            {
                headerName: "Kitting",
                field: "kitSite",
                tooltipField: "kitSite",
                width: 80,
                editable: false,
            },
            {
                headerName: "Boxing",
                field: "boxSite",
                tooltipField: "boxSite",
                width: 80,
                editable: false,
            },
            {
                headerName: "KB Split",
                field: "boxSplit",
                tooltipField: "boxSplit",
                width: 70,
                editable: false
            },
            {
                headerName: "Merging",
                field: "mergeSite",
                tooltipField: "mergeSite",
                width: 80,
                editable: false,
            },
            {
                headerName: "BM Split",
                field: "mergeSplit",
                tooltipField: "mergeSplit",
                width: 70,
                editable: false,
            },
            {
                headerName: "Last Modified By",
                field: "sysLastModifiedby",
                tooltipField: "sysLastModifiedby",
                width: 150,
                editable: false
            },
            {
                headerName: "Last Modified Date",
                field: "sysLastModifiedDate",
                tooltipField: "sysLastModifiedDate",
                width: 120,
                editable: false
            },
            {
                headerName: "Copy",
                cellRenderer: 'buttonRenderer',
                width: 80,
               // hide: true,
                //editable: function (Rownode) {
                //    return Rownode.node.data.fromDateVisible;
                //},
                cellRendererParams: {
                    onClick: this.CopyRule.bind(this),
                    label: 'Copy'
                }
            },
            {
                headerName: "Delete",
                cellRenderer: 'buttonRenderer',
                width: 80,
                //editable: function (Rownode) {
                //    return Rownode.context.componentParent.isUserHavingWriteAccess;
                //},
                cellRendererParams: {
                    onClick: this.DeleteRule.bind(this),
                    label: 'Delete'
                }
            },
        ];
        this.contextName = { componentParent: this };
        this.frameworkComponentsName = {
            buttonRenderer: ButtonRendererComponent,
        };
    }

    ViewResult() {
        this.ClearMessage();
        this.GetMainGridData();
    }

    GetMainGridData() {
        this.sourcingRuleGrids = [];
        //575 - means All product id
        var productID = this.selectedGMPProductTreeData_label.includes('(') ? this.selectedGMPProductTreeData_label.split('(')[1].split(')')[0] : "575";
        this.rowData = [];
        // this._sourcingRuleService.GetSourcingRuleLineItems("1865", "60825")
        this._sourcingRuleService.GetSourcingRuleLineItems(this.selectedChannelGeo[0].id, productID)
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        this.sourcingRuleGrids = data;
                        this.rowData = data;
                        this.excelExportDisable = true;
                        // this.SetRowData();

                        //this.gridColumnApi.setColumnVisible('Delete', true);
                        //this.gridColumnApi.setColumnVisible('Copy', false);
                    }
                },
                err => console.log(err),
            );
    }

    CopyRule(row: any) {
        this.ClearMessage();
        if (this.rowData != undefined && row.rowData != undefined) {
            var copiedRows = this.rowData.filter(x => x.channel === row.rowData.channel && x.product === row.rowData.product);  
            this._sourcingRuleService.GetCopiedProccessData(copiedRows)
                .map((data: any) => data.json()).subscribe((data: any) => {
                    if (data != null) {
                        this.selectedGMPProductCreateTreeData_label = data[0].product;
                        this.selectedChannelGeoCreate = [];
                        this.ModifySourcingRuleKeyForDelete(data);
                        this.selectedChannelGeoCreate.push({
                            id: data[0].channelId,
                            itemName: data[0].channelName + "(" + data[0].channelId + ")"
                        });
                        this.rowDataCreate = data;
                        this.gridOptionsCreate.api.setRowData(this.rowDataCreate);
                        this.isCopyActiveEnable = true;
                    }
                },
                    err => console.log(err))
        }

    }

    ModifySourcingRuleKeyForDelete(data: any[]) {
        this.addedIndex = 0;
        for (var i = 0; i < data.length; i++) {
            if (data[i].sourcingRuleKey != undefined) {
                data[i].sourcingRuleKey = "Add" + this.addedIndex;
                ++this.addedIndex;
            }
        }
        //sourcingRuleKey: "Add" + 
    }


    DeleteRule(row: any) {
        this.ClearMessage();
        if (this.rowData != undefined && row.rowData != undefined) {
            //var channelId = row.rowData.channel.split('(')[1].split(')')[0];
            //var productId = row.rowData.product.split('(')[1].split(')')[0];
            this.DeleteSouringRule(row);
            //var deletedRows = this.rowData.filter(x => x.channel === row.rowData.channel && x.product === row.rowData.product);
            //this.rowDataCreate = deletedRows;

            // this.gridOptionsCreate.api.setRowData(this.rowDataCreate);
        }
    }

    DeleteSouringRule(row :any ) {
        this.ClearMessage();
        //this.gridApi.stopEditing();
        var alertMsg = "";
        var channelId = row.rowData.channel.split('(')[1].split(')')[0];
        var productId = row.rowData.product.split('(')[1].split(')')[0];
        if (productId != '' && channelId != '') {
            alertMsg = 'Are sure you want to delete all rule for product = ' + row.rowData.product + ' and Channel = ' + row.rowData.channel + ' ?';
        }
        if (window.confirm(alertMsg)) {
            //put your delete method logic here

            this._sourcingRuleService.DeleteSourcingRuleCollection(productId, channelId)
                .subscribe(
                    (data: any) => {
                        console.log(data);
                        var returnMsg = data._body.split("~")[0];
                        if (returnMsg > 0) {
                            this.DisplayMessage("Data succesfully deleted.", true);
                            this.GetMainGridData();
                        }
                        else {
                            this.DisplayMessage("No record deleted.", false);
                        }
                        // this._factoryRoutingSource = [];
                        //  this.hideShowAlertMsg = true;
                        //if (returnMsg == "SUCCESS") {
                        //    this.gridOptions.api.deselectAllFiltered();
                        //    this.gridBtndeleteFactory = false;
                        //    this.hideShowAlertMsg = true;
                        //    this.DisplayMessage("Records successfully Deleted.", true);
                        //    this.gridOptionsFactory.api.updateRowData({ remove: selectedRow });
                        //    this.removeDeletedData(selectedRow);
                        //}
                        //else {
                        //    this.DisplayMessage("Error - check error grid for more details", false);
                        //    this.getErrorGrid('P_UPDATE_FSR_3');
                        //}
                    },
                    err => console.log(err), // error
                );

        }
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        //  this.autoSizeAllFactory();
        //this.sizeToFit();
    }

    onItemSelect(event: any) {
        if (this.rowData != undefined) { this.rowData = []; }
        this.excelExportDisable = false;
    }

    OnItemDeSelect(event: any) {
        if (this.rowData != undefined) { this.rowData = []; }
        this.excelExportDisable = false;
    }

    ExcelExport() {
        if (this.rowData === undefined) {
            this.DisplayMessage("Data in grid is blank", false);
        } else {
            this.GenerateExcelName();
        }
    }

    GenerateExcelName() {
        this.rowData
        this.ClearMessage();
        let param = [];
        for (var i = 0; i < this.rowData.length; i++) {
            param.push({
                ["Channel"]: this.rowData[i].channel,
                ["Product"]: this.rowData[i].product,
                ["FromDate"]: this.rowData[i].fromDateStr,
                ["ToDate"]: this.rowData[i].toDateStr,
                ["Kitting"]: this.rowData[i].kitSite,
                ["Boxing"]: this.rowData[i].boxSite,
                ["KB Split"]: this.rowData[i].boxSplit,
                ["Merging"]: this.rowData[i].mergeSite,
                ["BM Split"]: this.rowData[i].mergeSplit,
                ["Last Modified By"]: this.rowData[i].sysLastModifiedby,
                ["Last Modified Date"]: this.rowData[i].sysLastModifiedDate,

            })
        }
        //this.changedRows = [];
        this._sourcingRuleService.ExcelExport(param);
    }
    //-- Main grid section End
}

function getDatePicker() {
    function Datepicker() { }
    Datepicker.prototype.init = function (params) {
        this.eGui = document.createElement("input");
        this.eGui.value = params.value; //=== undefined ? "" : params.value;
        jQuery(this.eGui).datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true
        });
    };
    Datepicker.prototype.getGui = function () {
        return this.eGui;
    };
    Datepicker.prototype.afterGuiAttached = function () {
        this.eGui.focus();
        this.eGui.select();
    };
    Datepicker.prototype.getValue = function () {
        return this.eGui.value;
    };
    Datepicker.prototype.destroy = function () { };
    Datepicker.prototype.isPopup = function () {
        return false;
    };
    return Datepicker;
}

function singleSelectDDNamed() {
    function MultiSelectDropDown() { }

    MultiSelectDropDown.prototype.getGui = function () {
        return this.eGui;
    };
    MultiSelectDropDown.prototype.getValue = function () {
        var arr = [];
        for (var i = 0; i < this.eGui.firstChild.selectedOptions.length; i++) {

            var selectedItemValueGridDD = this.eGui.firstChild.selectedOptions[i].value;
            arr.push(selectedItemValueGridDD);
            var commaValues = arr.join(",");
        }
        this.value = commaValues;
        return this.value;
    };
    MultiSelectDropDown.prototype.isPopup = function () {
        return true;
    };
    MultiSelectDropDown.prototype.search = function () {
        console.log(this);
    }
    MultiSelectDropDown.prototype.afterGuiAttached = function () {
        var changedWidth = this.colWidth + "px";
        this.eGui.parentElement.style.width = changedWidth;
    };
    MultiSelectDropDown.prototype.init = function (params) {

        var multiDD: any = [];
        //  multiDD = params.context.componentParentCreate[params.column.colId];
        multiDD = params.context.componentParentCreate["Sites"];
        var myarr = params.value !=undefined ? params.value.split(",") : "";
        // var �colWidth;
        this.colWidth = params.column.actualWidth;

        var tempElement = document.createElement("div");
        tempElement.setAttribute('id', 'multiSel');
        tempElement.setAttribute('class', 'multiselect singleSelect');

        var tempElementDiv = document.createElement("select");
        tempElementDiv.setAttribute('class', 'multiselect-ui custom-select form-control');
        tempElementDiv.setAttribute('id', 'multiselect-ui');

        for (var i = 0; i < multiDD.length; i++) {
            var option = document.createElement("option");
            option.setAttribute("value", multiDD[i].itemName);
            option.text = multiDD[i].itemName;
            for (var j = 0; j < myarr.length; j++) {
                if (multiDD[i].itemName === myarr[j]) {
                    option.setAttribute("selected", "selected");
                }
            }
            tempElementDiv.appendChild(option);
            tempElement.appendChild(tempElementDiv);
        }
        loadJavaScript();
        this.eGui = tempElement;
    };
    return MultiSelectDropDown;
}

function loadJavaScript() {
    var url = '/dist/grid-multi-select.js';
    var s = document.createElement('script');
    s.setAttribute('src', url);
    document.head.appendChild(s);
}