﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { SourcingRuleComponent } from './sourcing-rule.component';

let component: SourcingRuleComponent;
let fixture: ComponentFixture<SourcingRuleComponent>;

describe('SourcingRule component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ SourcingRuleComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(SourcingRuleComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});