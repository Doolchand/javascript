﻿import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, Input } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { SessionStorageService } from 'ngx-webstorage';
import * as XLSX from 'xlsx';
import { UserRoles } from '../../Model/user-roles';
import { demandsplitService } from '../../services/demand-split.service';
import { UtilitiesService } from '../../services/utilities.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetErrorGridService } from '../../services/get-error-grid.service';
import { routingrulesService } from '../../services/routingrules.service';
import { FulfillmentSiteAssignmentService } from '../../services/fulfillment-siteassignment.service';
import { RoutingRulesSearchPara } from '../../Model/RoutingRulesSearch';
import { AddRouting } from '../../Model/AddRouting';
import { RoutingRulesUpdategrid } from '../../Model/RoutingRulesUpdategrid';


//import { treeViewData, node, children, FulFillmentSearchInput } from '../../Model/tree-view-data';
import { treeViewData } from '../../Model/treeview/treeViewData';
import { node } from '../../Model/treeview/node';
import { children } from '../../Model/treeview/children';
import { FulFillmentSearchInput } from '../../Model/treeview/FulFillmentSearchInput';



import { TreeViewComponent } from '../../components/tree-view/tree-view.component'
//import { Copyroutingrules } from '../../components/routingrulescopy/routingrulescopy.component'
import { CopyRouting } from '../../Model/CopyRouting';
import { DeleteRoutingRules } from '../../Model/DeleteRoutingRules';


import { from } from 'rxjs/observable/from';
import { forEach } from '@angular/router/src/utils/collection';
import { debounce } from 'rxjs/operator/debounce';
import { ReturnStatement } from '@angular/compiler';


declare var jQuery: any;

@Component({
    selector: 'app-routingrules',
    templateUrl: './routingrules.component.html',
    styleUrls: ['./routingrules.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class RoutingRulesComponent implements OnInit {


    @ViewChild('fakeClick') fakeClick: ElementRef;
    private gridOptionsName: GridOptions;

    private AddUpdateGrid: GridOptions;
    private DynamicGrid: GridOptions;

    private rowDataName;
    private DatarowDynamic=[];
    private AddupdaterowData=[];
    SelectedProductvalue = [];
    SelectedCopyProductvalue = [];
    singleSelectDropdownSettingsProduct = {};
    Productdata = [];

    SearchView: RoutingRulesSearchPara;
    SearchViewCopy: CopyRouting;
    SelectedProductvaluesecond = [];
    Productdatasecond = [];

    Selectedgeographyvalue = [];
    singleSelectDropdownSettingsgeography = {};
    geographyldata = [];
    FGAdata = [];
    SelectedFGA = [];
    singleSelectDropdownSettingsFGA = {};

    isProductTree: boolean = true;
    
    isProductSeconfTree: boolean = true;
    isProductTreeCopyRules: boolean = true;
    isFGACopyRules= false;

   
    selectedProductTreeData_label: string ="";
    selectedSecondProductTreeData_label: string;
    selectedProductTreeData: string;
    selectedProductTreeData_create: string;
    selectedProductTreeData_create_label: string="";
    CopyselectedProductTreeData_label: string;
    NoRoutingsConfigured = "";
    NoRoutingsConfiguredAddUpdate = "";
         
    private isAddDeletePanel = false;

    private isDyanamicGrid = false;
    
    @ViewChild('file') FileName: any;
    @ViewChild(TreeViewComponent) treeviewChild: TreeViewComponent;
  //  @ViewChild(Copyroutingrules) popupCopyRoutingChild: Copyroutingrules;

    //geo 
    geographyData = [];
    selectedGeogrphy = [];
    geogrphyDropdownSettings = {};



    AlternateRouting1 = [];
    SelectAlternateRouting1 = [];
    AlternateRouting1DropdownSettings = {};

    PrimaryRouting = [];
    selectedPrimaryRouting = [];
    PrimaryRoutingDropdownSettings = {};

    AlternateRouting2 = [];
    selectedAlternateRouting2 = [];
    AlternateRouting2DropdownSettings = {};



    ManufacturingSite = [];
    selectedManufacturingSite = [];
    ManufacturingSiteDropdownSettings = {};






    selectedFulfillmentSite = [];
    FulfillmentSite = [];
    FulfillmentSiteDropdownSettings = {};

    selectedMFGFiscalBeginDate = [];
    MFGFiscalBeginDate = [];
    MFGFiscalBeginDateDropdownSettings = {};

    selectedddlMFGFiscalEndDate = [];
    ddlMFGFiscalEndDate = [];
    ddlMFGFiscalEndDateDropdownSettings = {};





    selecteddEffectiveEndDate = [];
    EffectiveEndDate = [];
    EffectiveEndDateDropdownSettings = {};

    selectedEffectiveBeginDate = [];
    EffectiveBeginDate = [];
    EffectiveBeginDateDropdownSettings = {};


    isCountryDdlOpen: boolean = true;
    selectedGeogrphyTreeData: string;
    selectedGeogrphyTreeData_label: string;
    isGeographyTree: boolean = true;
    nodesGeography: node[];

    selectedCountry = [];
    countryData = [];
    countryDropdownSettings = {};
    private isupdatePanel = false;
    private isRoutingPanel = false;
    private isCopyRulesPanel = false;
    private isRoutingGrid = false;
    private issaveupdatedelete = false;

    private successmessage = "";
    private errormessage = "";
    private Fulfillerrormessage = "";
    private RequirefieldMessage = "";
    private PrimaryRoutingerro = "";
    private RoutingErrorMessage = "";
      
   // FulfillmentSiteDisable  = true;
    isddlProductSecond_edit: boolean = false;
    isddlProductSecond_edit_image: boolean = false;
    isddlFullfilment_edit: boolean = false;
    isddlMFGBeging_edit: boolean = false;
    isddlMFGEnd_edit: boolean = false;

    private IseSearchGridSection = false;
    AreRoutingDatesValid = "True";
    isddlProductSecond: boolean = false;


    RetriveRoutingRule_Edit: boolean = false;
    RetriveRoutingRule_Clear: boolean = false;
    DeleteAddUpdate_Delete: boolean = false;

    rowSelection_Main: string = 'single';
    rowSelection_Final: string = 'single';
   // rowSelection: string = 'single';
    rowSelection_rules: string = 'single';
    rowSelection_Update: string = 'single';
   // MfgSiteEffectiveBeginDate
    SelectedTreeLable = "";
    SelectedTreeLableSecond = "";
    CopySelectedTreeLable = "";

    routingId = "";
    

    //tree variables
    nodes: node[];
    treeViewNodesFilterdData: node[];
    treeViewDetailData: treeViewData = new treeViewData();
    nodesProducts: node[];
   // RoutingRulesGrid: RoutingRulesUpdategrid = new RoutingRulesUpdategrid();

    public RoutingRulesGrid: RoutingRulesUpdategrid[];

    public deleteRoutingRules: DeleteRoutingRules[];

    options: any;

    addrouting: AddRouting;
    private contextName;
    private componentsName;
    private frameworkComponentsName;
    private SelectedSplitPercentage;

    private gridApiNameDyanamic;
    private gridApiName1;
    private AddUpdateGridgridApiName;
    private gridColumnApiName;
    private gridColumnApiName1;

    private RetriveProductErrmsg = "";
    SavedMessage = "";
    selectProductMessage = "";
    fgaMessage = "";
    CopyfgaMessage = "";
    CopySuccessMessage = "";
    CopyruleSelectedRow = [];


    userRoles: UserRoles[];
    screenId = "2002";
    isUserHavingWriteAccess: boolean = false;
    isActiveScreen: boolean = true;
    userHavingWriteAccess: string[];
    listOfRoleHavingAccess: string[];
    userTenantTypeAccessList: string[];
    isUserHavingUserMaintenance: boolean = false;

    ManufactringDisplay = "";


    onselectedProductTreeDataChanged(selectedTreeValue: string): void {
        
        this.selectedProductTreeData = selectedTreeValue;
        this.selectedProductTreeData_label = this.selectedProductTreeData.split('__')[0];
       // this.selectedSecondProductTreeData_label = this.selectedProductTreeData.split('__')[0];

        
    }

    onselectedRoutingCreateProductTreeDataChanged(selectedTreeValue: string): void {
        
        this.selectedProductTreeData_create = selectedTreeValue;
        this.selectedProductTreeData_create_label = this.selectedProductTreeData_create.split('__')[0];
    }


    onselectedRoutingCopyProductTreeDataChanged(selectedTreeValue: string): void {
        
        this.selectedProductTreeData_create = selectedTreeValue;
        this.CopyselectedProductTreeData_label = this.selectedProductTreeData_create.split('__')[0];
    }

 

    constructor(private _router: Router, private _routingrulesService: routingrulesService, private _FulfillmentSiteAssignmentService: FulfillmentSiteAssignmentService, private _errorGridService: GetErrorGridService, private _sessionSt: SessionStorageService, private _utilitiesService: UtilitiesService) {


        this.Authorization();

        // this.RoutingRulesGrid = [];
        //Grid EMC Hierarchy Grid   
        this.gridOptionsName = <GridOptions>{
            contextName: {
                componentParentName: this
            },
          //  rowSelection: this.rowSelection,
        };
        this.gridOptionsName.columnDefs = [
            //{ headerName: "Select", field: "Select", suppressSizeToFit: false, tooltipField: "Product", editable: false, width: 40, },
           // { headerName: "Select", field: "Select",  suppressSizeToFit: false, tooltipField: "Select", editable: false, cellClass: "checkBox", cellRenderer: "checkboxRenderer",},

            {
                headerName: "",
                field: "id",
                width: 30,
                pinned: "left",
                tooltipField: "id",
                suppressSizeToFit: true,
                suppressFilter: true,
                
                hide: false,
                headerCheckboxSelectionFilteredOnly: true,
                editable: false,
                checkboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                },
                headerCheckboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                }
            },


            { headerName: "Product/Item", field: "productfga", suppressSizeToFit: false, tooltipField: "Channel", editable: false, width: 190, },
            //{ headerName: "Mfg Site Effective Begin Date", field: "geography_ID", suppressSizeToFit: false, tooltipField: "Geography", editable: false, width: 150, },
            { headerName: "Mfg Site Effective Begin Date", field: "mfG_EFFECTIVE_START_DATE", suppressSizeToFit: false, tooltipField: "Mfg_Site_Effective_Begin_Date", editable: false, width: 80, },
            { headerName: "Mfg Site Effective End Date", field: "mfG_EFFECTIVE_END_DATE", suppressSizeToFit: false, tooltipField: "Mfg_Site_Effective_End_Date", editable: false, width: 80, },
            { headerName: "Manufacturing Site", field: "manufacturinG_SITE_ID", suppressSizeToFit: false, tooltipField: "Manufacturing_Site", editable: false, width: 80, },
            { headerName: "Fulfillment Site", field: "fulfillmenT_SITE_ID", suppressSizeToFit: false, tooltipField: "Fulfillment Site", editable: false, width: 80, },
            { headerName: "Split Percentage", field: "mfG_SITE_SPLIT_PERCENT", suppressSizeToFit: false, tooltipField: "Split_Percentage", editable: false, width: 80, },
            { headerName: "Routing Effective Start Date", field: "routinG_EFFECTIVE_START_DATE", suppressSizeToFit: false, tooltipField: "Routing_Effective_Start_Date", editable: false, width: 80, },
            { headerName: "Routing Effective End Date", field: "routinG_EFFECTIVE_END_DATE", suppressSizeToFit: false, tooltipField: "Routing_Effective_End_Date", editable: false, width: 80, },
            { headerName: "Primary Routing", field: "primarY_ROUTING", suppressSizeToFit: false, tooltipField: "Primary_Routing", editable: false, width: 80, },
            { headerName: "Alternate Routing1", field: "alternatE_ROUTING_1", suppressSizeToFit: false, tooltipField: "Alternate_Routing1", editable: false, width: 100, },
            { headerName: "Alternate Routing2", field: "alternatE_ROUTING_2", suppressSizeToFit: false, tooltipField: "Alternate_Routing2", editable: false, width: 100, },
            { headerName: "Last Modified By", field: "syS_LAST_MODIFIED_BY", suppressSizeToFit: false, tooltipField: "Last_Modified_By", editable: false, width: 100, },

            { headerName: "Last Modified Date", field: "syS_LAST_MODIFIED_DATE", suppressSizeToFit: false, tooltipField: "Last_Modified_By", editable: false, width: 100, },

            { headerName: "routinG_ID", field: "routinG_ID", suppressSizeToFit: false, tooltipField: "Last_Modified_By", editable: false, width: 100, hide:true},


        ];
        this.gridOptionsName.headerHeight = 18;
        this.gridOptionsName.rowSelection = "multiple";
        this.gridOptionsName.paginationPageSize = 14;
        this.gridOptionsName.defaultColDef = {
            editable: true,
            enableValue: true
        };
        this.gridOptionsName.rowData = [];





        this.AddUpdateGrid = <GridOptions>{
            contextName: {
                componentParentName: this
            },
            rowSelection: this.rowSelection_Update,

        };
        this.AddUpdateGrid.columnDefs = [
         
            {
                headerName: "",
                field: "id",
                width: 30,
                pinned: "left",
                tooltipField: "id",
                suppressSizeToFit: true,
                suppressFilter: true,

                hide: false,
                headerCheckboxSelectionFilteredOnly: true,
                editable: false,
                checkboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                },
                headerCheckboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                }
            },

            
            { headerName: "Manufacturing Site", field: "manufacturinG_SITE_ID", suppressSizeToFit: false, tooltipField: "Manufacturing_Site", editable: false, width: 120, },
            { headerName: "Fulfillment Site", field: "fulfillmenT_SITE_ID", suppressSizeToFit: false, tooltipField: "Fulfillment Site", editable: false, width: 120, },
            { headerName: "Mfg Effective Start Date", field: "mfg_Site_Effective_Begin_Date", suppressSizeToFit: false, tooltipField: "mfg_Site_Effective_Begin_Date", editable: false, width: 120, },
            { headerName: "Mfg Effective End Date", field: "mfg_Site_Effective_End_Date", suppressSizeToFit: false, tooltipField: "mfg_Site_Effective_End_Date", editable: false, width: 120, },

            { headerName: "Split Percentage", field: "mfG_SITE_SPLIT_PERCENT", suppressSizeToFit: false, tooltipField: "Split_Percentage", editable: false, width: 120, },
            { headerName: "Routing Effective Start Date", field: "routing_Effective_Start_Date", suppressSizeToFit: false, tooltipField: "routing_Effective_Start_Date", editable: false, width: 120, },
            { headerName: "Routing Effective End Date", field: "routing_Effective_End_Date", suppressSizeToFit: false, tooltipField: "routing_Effective_End_Date", editable: false, width: 120, },

            { headerName: "Primary Routing", field: "primarY_ROUTING_DESC", suppressSizeToFit: false, tooltipField: "Primary_Routing", editable: false, width: 120, },
            { headerName: "Alternate Routing1", field: "alternate_Routing1", suppressSizeToFit: false, tooltipField: "Alternate_Routing1", editable: false, width: 120, hide: true },
            { headerName: "Alternate Routing2", field: "alternate_Routing2", suppressSizeToFit: false, tooltipField: "Alternate_Routing2", editable: false, width: 120, hide: true},


            { headerName: "Alternate Routing1", field: "alternate_Routing1_des", suppressSizeToFit: false, tooltipField: "alternate_Routing1_des", editable: false, width: 120, },
            { headerName: "Alternate Routing2", field: "alternate_Routing2_des", suppressSizeToFit: false, tooltipField: "alternate_Routing2_des", editable: false, width: 120, },



           //should be hide
            { headerName: "id", field: "id", suppressSizeToFit: false, tooltipField: "Primary_Routing", editable: false, width: 120, hide: true},
            { headerName: "item", field: "item", suppressSizeToFit: false, tooltipField: "Alternate_Routing1", editable: false, width: 120, hide: true},
            { headerName: "primarY_ROUTING", field: "primarY_ROUTING", suppressSizeToFit: false, tooltipField: "Alternate_Routing2", editable: false, width: 120, hide: true },
            { headerName: "routinG_ID", field: "routinG_ID", suppressSizeToFit: false, tooltipField: "Alternate_Routing1", editable: false, width: 120, hide: true  },
            { headerName: "type", field: "type", suppressSizeToFit: false, tooltipField: "Alternate_Routing2", editable: false, width: 120, hide:true },





        ];
        this.AddUpdateGrid.headerHeight = 18;
        this.AddUpdateGrid.rowSelection = "multiple";
        this.AddUpdateGrid.paginationPageSize = 14;
        this.AddUpdateGrid.defaultColDef = {
            editable: true,
            enableValue: true
        };
        this.AddUpdateGrid.rowData = [];





        this.DynamicGrid = <GridOptions>{
            contextName: {
                componentParentName: this
            },
            rowSelection: this.rowSelection_rules,
        };
        this.DynamicGrid.columnDefs = [

            {
                headerName: "",
                field: "id",
                width: 30,
                pinned: "left",
                tooltipField: "id",
                suppressSizeToFit: true,
                suppressFilter: true,

                hide: false,
                headerCheckboxSelectionFilteredOnly: true,
                editable: false,
                checkboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                },
                headerCheckboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                }
            },


            { headerName: "Site", field: "site", suppressSizeToFit: false, tooltipField: "Manufacturing_Site", editable: false, width: 170, },
            { headerName: "Split Percentage", field: "split_percentage", suppressSizeToFit: false, tooltipField: "Fulfillment Site", editable: false, width: 160, },
            { headerName: "Add Routing", field: "fullfilment_site", suppressSizeToFit: false, tooltipField: "Mfg_Site_Effective_Begin_Date", editable: false, width: 120,  hide:true},
           
           



        ];
      //  this.DynamicGrid.headerHeight = 18;
     //   this.DynamicGrid.rowSelection = "multiple";
       // this.DynamicGrid.paginationPageSize = 14;
        this.DynamicGrid.defaultColDef = {
            editable: true,
            enableValue: true
        };
        this.DynamicGrid.rowData = [];

        //this.contextName = { componentParent: this };
        //this.componentsName = {};
        //this.frameworkComponentsName = {
        //    ButtonRenderer: buttonreder,
        //};

    }

     
    ngOnInit() {

        this.singleSelectDropdownSettingsProduct = {
            singleSelection: true,
            text: "ALL",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };



        


        this.singleSelectDropdownSettingsFGA = {
            singleSelection: true,
            text: 'Select',
            selectAllText: 'Select',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };



        this.singleSelectDropdownSettingsgeography = {
            singleSelection: true,
            text: "ALL",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };


        this.AlternateRouting1DropdownSettings = {
            singleSelection: true,
            text: "Select",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };


        this.PrimaryRoutingDropdownSettings = {
            singleSelection: true,
            text: "Select",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };



        this.AlternateRouting2DropdownSettings = {
            singleSelection: true,
            text: "Select",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };


        this.FulfillmentSiteDropdownSettings = {
            singleSelection: true,
            text: "Select",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };


        this.MFGFiscalBeginDateDropdownSettings = {
            singleSelection: true,
            text: "",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };



        this.ddlMFGFiscalEndDateDropdownSettings = {
            singleSelection: true,
            text: "",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };

        this.EffectiveEndDateDropdownSettings = {
            singleSelection: true,
            text: "",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };


        this.EffectiveBeginDateDropdownSettings = {
            singleSelection: true,
            text: "",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };



        this.ManufacturingSiteDropdownSettings = {
            singleSelection: true,
            text: "Select",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };



        


        this.getProductData();
        this.getGeogrphyData();

        this.Getfulfillment();

        this.GetDate();
        this.GetEffectiveEndDate();

    }

    Authorization() {
        var rolesHavingWriteAccess = this._sessionSt.retrieve('screenAuthorization') != null ? this._sessionSt.retrieve('screenAuthorization') : [];
        this.userHavingWriteAccess = rolesHavingWriteAccess[rolesHavingWriteAccess.findIndex(x => x.functionId == this.screenId)]["roleId"].split(',');
        this.userTenantTypeAccessList = this._sessionSt.retrieve('userTenantTypeAccessList') != null ? this._sessionSt.retrieve('userTenantTypeAccessList') : [];

        if (this._sessionSt.retrieve('userRoles') === null) {
            this.isActiveScreen = false;
            this._router.navigateByUrl('/errorMessage');
            return;
        }
        else {
            this.userRoles = this._sessionSt.retrieve('userRoles');
            for (var i = 0; i < this.userRoles.length; i++) {
                if (this.userHavingWriteAccess.includes(this.userRoles[i].roleId.toString())) {
                    this.isUserHavingWriteAccess = true;
                    return;
                }
            }

            if (!(this.isUserHavingWriteAccess || this.userTenantTypeAccessList.includes("RP"))) {
                this.isActiveScreen = false;
                this._router.navigateByUrl('/errorMessage');
                return;
            }
        }
    }
 
    GetFGA() {
      
        if (this.FGAdata.length > 0) {


        }
        else {
            this._routingrulesService.GetFGA()
                .subscribe(
                    (data: any) => {

                        if (data != null) {
                            data = JSON.parse(data._body);
                            //this.FGAdata = [];

                            for (var i = 0; i < data.length; i++) {
                                this.FGAdata.push({
                                    id: data[i].itemTypeId,
                                    itemName: data[i].itemTypeName
                                })
                            }

                            this.SelectedFGA[0] = { id: "Select", itemName: "Select" };
                        }
                    },
                    err => console.log(err),
                );

        }
     

    }

    GetEffectiveEndDate() {
        this._routingrulesService.GetEffectiveEndDate()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                       

                        
                        this.EffectiveEndDate = [];
                        this.ddlMFGFiscalEndDate = [];

                        for (var i = 0; i < data.length; i++) {

                            this.EffectiveEndDate.push({
                                itemName: data[i].itemTypeName,
                                id: data[i].itemTypeId
                            })


                            this.ddlMFGFiscalEndDate.push({
                                itemName: data[i].itemTypeName,
                                id: data[i].itemTypeId
                            })

                          

                        }
                    }
                },
                err => console.log(err),
            );



    }

    Getfulfillment() {
        this._routingrulesService.GetFulfilment()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.FulfillmentSite = [];
                        
                        for (var i = 0; i < data.length; i++) {

                            this.FulfillmentSite.push({
                                itemName: data[i].itemTypeName,
                                id: data[i].itemTypeId
                            })


                        }
                    }
                    this.selectedFulfillmentSite[0] = { id: "Select", itemName: "Select" };
                     
                },
                err => console.log(err),
            );



    }

    GetDate() {
        this._routingrulesService.GetDate()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                       // this.ddlMFGFiscalEndDate = [];
                        this.MFGFiscalBeginDate = [];

                        this.EffectiveBeginDate = [];
                     //   this.EffectiveEndDate= [];
                        
                        for (var i = 0; i < data.length; i++) {

                            //this.ddlMFGFiscalEndDate.push({
                            //    itemName: data[i].itemTypeName,
                            //    id: data[i].itemTypeId
                            //})

                            this.MFGFiscalBeginDate.push({
                                itemName: data[i].itemTypeName,
                                id: data[i].itemTypeId
                            })



                            this.EffectiveBeginDate.push({
                                itemName: data[i].itemTypeName,
                                id: data[i].itemTypeId
                            })


                            //this.EffectiveEndDate.push({
                            //    itemName: data[i].itemTypeName,
                            //    id: data[i].itemTypeId
                            //})


                        }
                    }
                },
                err => console.log(err),
            );



    }

    getProductData() {

        this._FulfillmentSiteAssignmentService.getMasterDataData('PRODUCT')
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.Productdata = [];
                        for (var i = 0; i < data.length; i++) {

                            this.Productdata.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })


                            this.Productdatasecond.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })

                        }
                    }


                    this.SelectedProductvaluesecond[0] = { id: "ALL", itemName: "ALL" };
                    this.SelectedProductvalue[0] = { id: "ALL", itemName: "ALL" };
                    this.SelectedCopyProductvalue[0] = { id: "ALL", itemName: "ALL" };
                },
                err => console.log(err),
            );



    }

    getGeogrphyData() {
        this._routingrulesService.GetGeographyData()
            .subscribe(
            (data: any) => {
            
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.geographyldata = [];
                        for (var i = 0; i < data.length; i++) {

                            this.geographyldata.push({
                                id: data[i].itemTypeId,
                                itemName: data[i].itemTypeName
                            })
                        }
                        this.Selectedgeographyvalue[0] = { id: "ALL", itemName: "ALL" };
                    }
                },
                err => console.log(err),
            );
 
    }
  
    GetRoutingFilterViewResult() {

        
        this.SearchView

        var lblproduct = "";
        if (this.SelectedProductvalue[0].itemName == "FHC_TREE") {
            lblproduct = "ALL";
        }
       
        this.SearchView =
            {
           
            Product: this.SelectedProductvalue[0] == undefined ? "" : this.SelectedProductvalue[0].itemName,
            Geo: this.Selectedgeographyvalue[0] == undefined ? "" : this.Selectedgeographyvalue[0].id,
            Fhctree:  this.selectedProductTreeData_label,
            lblproduct: lblproduct,
            };

         
        if (this.SelectedProductvalue[0].itemName == "FGA" &&  this.selectedProductTreeData_label == "")  {
            this.fgaMessage = " Select a valid Product.";
        }
        else {
            this.fgaMessage = "";

             this._routingrulesService.GetRoutingRulesViewResult(this.SearchView)
                 .map((data: any) => data.json()).subscribe((data: any) => {
                     
                     
                     if (data.length > 0) {


                         this.rowDataName = data;
                         this.IseSearchGridSection = true;
                         this.NoRoutingsConfigured = "";
                     }
                     else {
                         this.rowDataName = [];
                         this.NoRoutingsConfigured = " No Routings Configured.";
                     } 

                 },
                     err => console.log(err), // error
                 )

        }
        
    }

    RetriveRoutingRule() {

        this.successmessage = "";
        this.errormessage = "";
        this.isDyanamicGrid = false;
        if (this.SelectedProductvaluesecond[0].itemName == "ALL") {

            this.RetriveProductErrmsg = "";
           
            this.RetiveFunction();
        }
        else {

            if (this.selectedProductTreeData_create_label != "")
            {
                
            this.RetiveFunction();
                this.RetriveProductErrmsg = "";
            }
            else if (this.SelectedProductvaluesecond[0].itemName == "FGA" && this.selectedProductTreeData_create_label == undefined)
            {
                this.RetriveProductErrmsg = " Select a valid Product.";
            }
            else {
                this.RetriveProductErrmsg = " Select a valid Product.";
            }
        }

    }

    RetiveFunction() {

        this.NoRoutingsConfigured = "";
        this.selectedEffectiveBeginDate[0] = { id: "", itemName: "" };
        this.selecteddEffectiveEndDate[0] = { id: "", itemName: "" };
        this.selectedFulfillmentSite[0] = { id: "Select", itemName: "Select" };
        
        this.isupdatePanel = true;
        this.SearchView

        
        var lblproduct = "";
        this.SearchView =
            {
                Product: this.SelectedProductvaluesecond[0] == undefined ? "" : this.SelectedProductvaluesecond[0].itemName,
                Geo: "",//this.Selectedgeographyvalue[0] == undefined ? "" : this.Selectedgeographyvalue[0].itemName,
            Fhctree: this.selectedProductTreeData_create_label,// this.selectedProductTreeData_label,
            lblproduct: lblproduct,

            };

        this._routingrulesService.RetriveRoutingRule(this.SearchView)
            .map((data: any) => data.json()).subscribe((data: any) => {

                

                if (data.length > 0) {
                    
                    this.AddupdaterowData = data;
                    this.isRoutingGrid = true;

                    this.RetriveRoutingRule_Edit = true;
                    this.NoRoutingsConfiguredAddUpdate = "";
                }
                else {
                    this.AddupdaterowData = [];
                    this.NoRoutingsConfiguredAddUpdate = "No Routings Configured.";
                }

                this.isddlProductSecond_edit = true;
                this.isddlProductSecond_edit_image = true;
                


            },
                err => console.log(err), // error
            )


        //this._routingrulesService.RetriveRoutingRule(this.SearchView)
        //    .subscribe(
        //        (data: any) => {
                    
        //            
        //            if (data.length > 0)
        //            {
        //                
        //             this.AddupdaterowData = data;
        //            this.isRoutingGrid = true;
        //            this.isddlProductSecond_edit = true;
        //            this.isddlProductSecond_edit_image = true;
        //            }
                   
        //        });





    }

    getProductTreeDetail() {
        
        this.nodesProducts = [];
        
        this.treeViewDetailData.selectedDPValue = this.SelectedProductvalue[0].itemName;
        this.treeViewDetailData.nodesData = this.nodesProducts;
        this.treeViewDetailData.header = "Product Tree Popup";
        this.treeViewDetailData.isSearchEnable = true;
       
        if (this.SelectedProductvalue[0].itemName == "PRODUCT_TREE") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All PRODUCT LOB Names";
            this.treeViewDetailData.headerMsg = "Please enter PRODUCT LOB Name!";
            this.treeViewDetailData.treeViewType = "PRODUCT_TREE";
        }
        if (this.SelectedProductvalue[0].itemName == "FHC_TREE") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All FHC Names";
            this.treeViewDetailData.headerMsg = "Please enter FHC ID!";
            this.treeViewDetailData.treeViewType = "PRODUCT_FHC";
            this.treeViewDetailData.header = "FHC Popup";
        }
        if (this.SelectedProductvalue[0].itemName == "FGA") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All FGA's";
            this.treeViewDetailData.headerMsg = "Please enter FGA ID!";
            this.treeViewDetailData.treeViewType = "PRODUCT_FGA";
            this.treeViewDetailData.header = "FGA Popup";
        }




        this.treeviewChild.bindTreeData();
    }

    getProductTreeDetails() {
        this.nodesProducts = [];
        
        this.treeViewDetailData.selectedDPValue = this.SelectedProductvaluesecond[0].itemName;
        this.treeViewDetailData.nodesData = this.nodesProducts;
        this.treeViewDetailData.header = "Product Tree Popup";
        this.treeViewDetailData.isSearchEnable = true;

        if (this.SelectedProductvaluesecond[0].itemName == "PRODUCT_TREE") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All PRODUCT LOB Names";
            this.treeViewDetailData.headerMsg = "Please enter PRODUCT LOB Name!";
            this.treeViewDetailData.treeViewType = "PRODUCT_TREE_create";
        }
        if (this.SelectedProductvaluesecond[0].itemName == "FHC_TREE") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All FHC Names";
            this.treeViewDetailData.headerMsg = "Please enter FHC ID!";
            this.treeViewDetailData.treeViewType = "PRODUCT_FHC_create";
            this.treeViewDetailData.header = "FHC Popup";
        }
        if (this.SelectedProductvaluesecond[0].itemName == "FGA") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All FGA's";
            this.treeViewDetailData.headerMsg = "Please enter FGA ID!";
            this.treeViewDetailData.treeViewType = "PRODUCT_FGA_create";
            this.treeViewDetailData.header = "FGA Popup";
        }




        this.treeviewChild.bindTreeData();
    }

    getProductCopyTreeDetail() {
        this.nodesProducts = [];

        this.treeViewDetailData.selectedDPValue = this.SelectedCopyProductvalue[0].itemName;
        this.treeViewDetailData.nodesData = this.nodesProducts;
        this.treeViewDetailData.header = "Product Tree Popup";
        this.treeViewDetailData.isSearchEnable = true;

        if (this.SelectedCopyProductvalue[0].itemName == "PRODUCT_TREE") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All PRODUCT LOB Names";
            this.treeViewDetailData.headerMsg = "Please enter PRODUCT LOB Name!";
            this.treeViewDetailData.treeViewType = "PRODUCT_TREE_copy";
        }
        if (this.SelectedCopyProductvalue[0].itemName == "FHC_TREE") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All FHC Names";
            this.treeViewDetailData.headerMsg = "Please enter FHC ID!";
            this.treeViewDetailData.treeViewType = "PRODUCT_FHC_copy";
            this.treeViewDetailData.header = "FHC Popup";
        }
        if (this.SelectedCopyProductvalue[0].itemName == "FGA") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All FGA's";
            this.treeViewDetailData.headerMsg = "Please enter FGA ID!";
            this.treeViewDetailData.treeViewType = "PRODUCT_FGA_copy";
            this.treeViewDetailData.header = "FGA Popup";
        }




        this.treeviewChild.bindTreeData();
    }


    onSelectedCopyProduct() {

        this.CopyfgaMessage = "";
        this.CopySuccessMessage = "";
        if (this.SelectedCopyProductvalue[0].itemName == "PRODUCT_TREE" || this.SelectedCopyProductvalue[0].itemName == "FHC_TREE") {
            this.isProductTreeCopyRules = false;
            this.isFGACopyRules = false;
        }
        else
        {
            //this.SelectedCopyProductvalue[0].itemName == "FGA"
            this.isProductTreeCopyRules = true;
            this.isFGACopyRules = true;

           
        }

    
        

    }


    onSelectedProduct() {

        if (this.SelectedProductvalue[0].itemName == "PRODUCT_TREE" || this.SelectedProductvalue[0].itemName == "FHC_TREE" || this.SelectedProductvalue[0].itemName == "FGA") {
            this.isProductTree = false;
            this.selectedProductTreeData_label = "";
            this.SelectedTreeLable = this.SelectedProductvalue[0].itemName;
        }
        else {
            this.isProductTree = true;
            this.selectedProductTreeData_label = "";
            this.SelectedTreeLable = "";
        }
        if (this.SelectedProductvalue[0].itemName == "FHC_TREE") {
           // this.selectedProductTreeData_label = "ALL";
            this.SelectedTreeLable = "";
        }
        if (this.SelectedProductvalue[0].itemName != "FGA") {
            this.fgaMessage = "";
        }

        this.NoRoutingsConfigured = "";
        //this.NoRoutingsConfiguredAddUpdate = "";
    }

    onSelectedProductSecond() {


        this.RetriveProductErrmsg = "";
        this.SavedMessage = "";
        if (this.SelectedProductvaluesecond[0].itemName == "PRODUCT_TREE" || this.SelectedProductvaluesecond[0].itemName == "FHC_TREE" || this.SelectedProductvaluesecond[0].itemName == "FGA") {
            this.isProductSeconfTree = false;
            this.isddlProductSecond_edit_image = false;
            this.SelectedTreeLableSecond = this.SelectedProductvaluesecond[0].itemName;
            //this.RetriveProductErrmsg = " Select a valid Product.";
        }
        else {
            this.isProductSeconfTree = true;
            this.SelectedTreeLableSecond = "";
            this.RetriveProductErrmsg="";
        }

        this.NoRoutingsConfigured = "";
        this.NoRoutingsConfiguredAddUpdate = "";
    }



    Fulfillmentonchange() {
        this.NoRoutingsConfiguredAddUpdate = "";
    }

    UpdateRoutingRules(event) {
        
        let rowsSelection = this.AddUpdateGrid.api.getSelectedRows();
        let strMFGSite = rowsSelection[0].manufacturinG_SITE_ID;
        let strSite = rowsSelection[0].fulfillmenT_SITE_ID;




        var AlternateRouting1 = rowsSelection[0].Alternate_Routing1;
        var PrimaryRouting = rowsSelection[0].primarY_ROUTING_DESC;
        var AlternateRouting2 = rowsSelection[0].Alternate_Routing2;
        var RoutingEffectiveStartDate = rowsSelection[0].mfg_Site_Effective_Begin_Date;
        var RoutingEffectiveEndDate = rowsSelection[0].mfg_Site_Effective_End_Date;

        if (rowsSelection.length > 0) {
            var outDeleteRoutingCount: any = 0;
            var outDeleteRoutingHeaderCount: any = 0;

            var Getvalue =
            {
                ItemTypeName: strMFGSite,
                ItemTypeId: strSite
            };

            this._routingrulesService.GetRoutingDropdownvalue(Getvalue)
                .subscribe(
                    (data: any) => {
                        
                        if (data != null) {
                            data = JSON.parse(data._body);
                            this.AlternateRouting1 = [];
                            this.PrimaryRouting = [];
                            this.AlternateRouting2 = [];
                            for (var i = 0; i < data.length; i++) {

                                this.AlternateRouting1.push({
                                    id: data[i].itemTypeId,
                                    itemName: data[i].itemTypeName
                                })

                                this.PrimaryRouting.push({
                                    id: data[i].itemTypeId,
                                    itemName: data[i].itemTypeName
                                })

                                this.AlternateRouting2.push({
                                    id: data[i].itemTypeId,
                                    itemName: data[i].itemTypeName
                                })
                               
                                

                               


                            }


                            this.SelectAlternateRouting1[0] = { id: "Select", itemName: "Select" };
                            this.selectedPrimaryRouting[0] = { id: "Select", itemName: "Select" };
                            this.selectedAlternateRouting2[0] = { id: "Select", itemName: "Select" };
                            this.isupdatePanel = true;
                            this.isRoutingPanel = true;

                        }
                    },
                    err => console.log(err),
                );


        }


    }

    SaveRoutingRules(event) {
        
        var rowsSelection = this.AddUpdateGrid.api.getSelectedRows();
        var fulfillmentid = this.selectedFulfillmentSite[0].id;
       // var product = "Abc";

        //this.RoutingRulesGrid = [];

        //var lstAddRouting = this.RoutingRulesGrid;

        //lstAddRouting.Routing_Effective_Start_Date= rowsSelection[0].Routing_Effective_Start_Date.toString(),
        //lstAddRouting.manufacturinG_SITE_ID= rowsSelection[0].manufacturinG_SITE_ID.toString(),
        //lstAddRouting.fulfillmenT_SITE_ID= rowsSelection[0].fulfillmenT_SITE_ID.toString(),
        //lstAddRouting.Mfg_Site_Effective_Begin_Date= rowsSelection[0].Mfg_Site_Effective_Begin_Date.toString(),
        //lstAddRouting.Mfg_Site_Effective_End_Date= rowsSelection[0].Mfg_Site_Effective_End_Date.toString(),
        //lstAddRouting.mfG_SITE_SPLIT_PERCENT= rowsSelection[0].mfG_SITE_SPLIT_PERCENT.toString(),
        //lstAddRouting.Routing_Effective_End_Date= rowsSelection[0].Routing_Effective_End_Date.toString(),
        //lstAddRouting.primarY_ROUTING_DESC= rowsSelection[0].primarY_ROUTING_DESC.toString(),
        //lstAddRouting.Alternate_Routing1= rowsSelection[0].Alternate_Routing1.toString(),
        //lstAddRouting.product= this.SelectedProductvaluesecond[0].itemName,
        //lstAddRouting.TreeValue = ""



        var strfhcType: any = '';
        
        


        if (this.SelectedProductvaluesecond[0].itemName == 'PRODUCT_TREE') {
            var strLevel = this.selectedProductTreeData_create_label.split('__')[1];
            var selectedProdId = this.selectedProductTreeData_create_label.split('__')[0];

            if (selectedProdId == '') {
                
                
            }
            else {
                if (selectedProdId != null && selectedProdId != '') {
                    //strProductId = selectedProdId.substring(0, strFgaId.indexOf("("));
                  //  strProductId = selectedProdId.split('(')[1].split(')')[0]
                }

            }
        }

        

        if (this.SelectedProductvaluesecond[0].itemName.toString() == 'FHC_TREE') {
            var strLevel = this.selectedProductTreeData_create_label.split('__')[1];
            var selectedProdId = this.selectedProductTreeData_create_label.split('__')[0];
            if (strLevel == '1') {
                strfhcType = 'FHC_ITEM';
            }
            if (strLevel == '2') {
                strfhcType = "FGA_ITEM";
            }
        }
        
       if(this.SelectedProductvaluesecond[0].itemName.toString() == 'FGA')
        {
            if (this.selectedProductTreeData_create_label != undefined) {
                var strLevel = this.selectedProductTreeData_create_label.split('__')[1];
                var selectedProdId = this.selectedProductTreeData_create_label.split('__')[0];
                strfhcType = "FGA_ITEM";
            }
        }

        
        this.RoutingRulesGrid = [];
        if (this.AddupdaterowData.length > 0) {
            this.RoutingErrorMessage = "";
            
            for (var i = 0; i < this.AddupdaterowData.length; i++) {
                this.RoutingRulesGrid.push({

                    routing_Effective_Start_Date: this.AddupdaterowData[i].routing_Effective_Start_Date == undefined ? "" : this.AddupdaterowData[i].routing_Effective_Start_Date.toString(),
                    manufacturinG_SITE_ID: this.AddupdaterowData[i].manufacturinG_SITE_ID == undefined  ? "" : this.AddupdaterowData[i].manufacturinG_SITE_ID.toString(),
                    fulfillmenT_SITE_ID: this.AddupdaterowData[i].fulfillmenT_SITE_ID == undefined  ? "" : this.AddupdaterowData[i].fulfillmenT_SITE_ID.toString(),
                    mfg_Site_Effective_Begin_Date: this.AddupdaterowData[i].mfg_Site_Effective_Begin_Date == undefined ? "" : this.AddupdaterowData[i].mfg_Site_Effective_Begin_Date.toString(),
                    mfg_Site_Effective_End_Date: this.AddupdaterowData[i].mfg_Site_Effective_End_Date == undefined ? "" : this.AddupdaterowData[i].mfg_Site_Effective_End_Date.toString(),
                    mfG_SITE_SPLIT_PERCENT: this.AddupdaterowData[i].mfG_SITE_SPLIT_PERCENT == undefined  ? "" : this.AddupdaterowData[i].mfG_SITE_SPLIT_PERCENT.toString(),

                    routing_Effective_End_Date: this.AddupdaterowData[i].routing_Effective_End_Date == undefined ? "" : this.AddupdaterowData[i].routing_Effective_End_Date.toString(),
                    primarY_ROUTING_DESC: this.AddupdaterowData[i].primarY_ROUTING_DESC == undefined  ? "" : this.AddupdaterowData[i].primarY_ROUTING_DESC.toString(),
                    alternate_Routing1: this.AddupdaterowData[i].alternate_Routing1 == undefined ? "" : this.AddupdaterowData[i].alternate_Routing1.toString(),
                    product: this.SelectedProductvaluesecond[0] == undefined  ? "" : this.SelectedProductvaluesecond[0].itemName.toString(),
                    TreeValue: this.selectedProductTreeData_create_label == undefined ? "" : this.selectedProductTreeData_create_label.toString(),
                    strMFGSiteName: "",
                    strMFGBeginDate: "",
                    strMFGEndDate: "",
                    strFFSite: "",
                    strNewRoutingBeginDate: "",
                    strNewRoutingEndDate: "",
                    id: this.AddupdaterowData[i] == undefined  ? "" : this.AddupdaterowData[i].id.toString(),
                    strStatus: "",
                    strRoutingIdRule: "",
                    strRoutingIDGrid: this.AddupdaterowData[i].routinG_ID == undefined  ? "" : this.AddupdaterowData[i].routinG_ID.toString(),
                    alternate_Routing2: this.AddupdaterowData[i].alternate_Routing2 == undefined ? "" : this.AddupdaterowData[i].alternate_Routing2.toString(), 
                    hdfTypeRetrieve: strfhcType.toString(),
                    primarY_ROUTING: this.AddupdaterowData[i].primarY_ROUTING == undefined ? "" : this.AddupdaterowData[i].primarY_ROUTING.toString(),
                    alternate_Routing1_des: this.AddupdaterowData[i].alternate_Routing1_des == undefined ? "" : this.AddupdaterowData[i].alternate_Routing1_des.toString(),
                    alternate_Routing2_des: this.AddupdaterowData[i].alternate_Routing2_des == undefined ? "" : this.AddupdaterowData[i].alternate_Routing2_des.toString(),

                });
            }


            
            

            this._routingrulesService.SaveRauting(this.RoutingRulesGrid)
                .subscribe(
                (data: any) => {
                    
                        if (data!=null) {
                            if (data._body.toString() == "1") {
                                this.SavedMessage = "Information Saved Successfully";
                                this.GetRoutingFilterViewResult();
                                this.isupdatePanel = false;
                                this.isRoutingPanel = false;
                                this.isRoutingGrid = false;
                                this.RetriveRoutingRule_Edit = false;
                                this.RetriveRoutingRule_Clear = false;
                                this.selectedProductTreeData_create_label = "";
                                this.isddlProductSecond_edit = false;
                                this.isddlProductSecond_edit_image = false;
                                this.SelectedProductvaluesecond[0] = { id: "ALL", itemName: "ALL" };
                                this.DatarowDynamic = [];
                                this.isProductSeconfTree = true;
                                 this.selectedEffectiveBeginDate[0] = { id: "", itemName: "" };
                                 this.selecteddEffectiveEndDate[0] = { id: "", itemName: "" };
                                this.selectedFulfillmentSite[0] = { id: "Select", itemName: "Select" };

                                this.NoRoutingsConfiguredAddUpdate = "";
                            }
                            else {

                                this.PrimaryRoutingerro = data._body.toString();
                            }
                        }
                        
                    });
        }
        else {
            this.RoutingErrorMessage = "No records is there" ;
        }


    }

    UpdateRules(event) {


        let rowsSelection = this.gridOptionsName.api.getSelectedRows();


        
        if (rowsSelection.length > 0) {
            let routinG_ID = rowsSelection[0].routinG_ID;
            let strSite = rowsSelection[0].fulfillmenT_SITE_ID;


            if (rowsSelection.length > 0) {
                var outDeleteRoutingCount: any = 0;
                var outDeleteRoutingHeaderCount: any = 0;

                var Getvalue =
                {
                    ItemTypeName: strSite,
                    ItemTypeId: routinG_ID
                };

                this._routingrulesService.GetRoutingRuladdupdateGrid(Getvalue)
                    .map((data: any) => data.json()).subscribe((data: any) => {


                        if (data.length > 0) {

                            
                            this.AddupdaterowData = data;
                            this.isRoutingGrid = true;
                            this.issaveupdatedelete = true;
                            

                        }


                    },
                        err => console.log(err), // error
                    )
            }

        }
        else {
            this.successmessage = "";
            this.errormessage = "No row is selected. Please select row ";

        }


    }

    DeleteRoutingRules(event) {
        

        let rowsSelection = this.gridOptionsName.api.getSelectedRows();
       


        this.deleteRoutingRules = [];
        


        
        if (rowsSelection.length > 0) {
 
            if (rowsSelection.length > 0) {
                var outDeleteRoutingCount: any = 0;
                var outDeleteRoutingHeaderCount: any = 0;
                this.errormessage = "";
               
                if (window.confirm("Are you sure to delete.")) {


                    for (var i = 0; i < rowsSelection.length; i++) {
                        this.deleteRoutingRules.push({

                            ItemTypeName: rowsSelection[i].fulfillmenT_SITE_ID,
                            ItemTypeId: rowsSelection[i].routinG_ID

                        });
                    }

                    this._routingrulesService.DeleteRoutingRules(this.deleteRoutingRules)
                        .map((data: any) => data.json()).subscribe((data: any) => {

                            if (data == true) {

                                this.successmessage = "Record Deleted Successfully.";
                                this.errormessage = "";
                                this.GetRoutingFilterViewResult();


                            }
                            else {

                                this.successmessage = "";
                            }


                        },
                            err => console.log(err), // error
                        )


                }
                
            }
        }
        else {
            this.successmessage = "";
            this.errormessage = "No row is selected. Please select row ";
            
        }


    }

    ExportExcelDemand(event) {
        this.successmessage = "";	
        this.errormessage = "";
        event.stopPropagation();
        // this._demandsplitService.exportAsExcelFile(this.rowDataName, "UnspecifiedDemandSplit");

        this.GenerateExcelName(this.rowDataName);
    }

    GenerateExcelName(rowData: any[]) {

        //this.ClearMessage();
        let param = [];
        for (var i = 0; i < rowData.length; i++) {
            param.push({
                ["Product/Item"]: rowData[i].productfga,
                ["Mfg Site Effective Begin Date"]: rowData[i].mfG_EFFECTIVE_START_DATE,
                ["Mfg Site Effective End Date"]: rowData[i].mfG_EFFECTIVE_END_DATE,
                ["Manufacturing Site"]: rowData[i].manufacturinG_SITE_ID,
                ["Fulfillment Site"]: rowData[i].fulfillmenT_SITE_ID ,

             
                ["Split Percentage"]: rowData[i].mfG_SITE_SPLIT_PERCENT,
                ["Routing Effective Start Date"]: rowData[i].routinG_EFFECTIVE_START_DATE,
                ["Routing Effective End Date"]: rowData[i].routinG_EFFECTIVE_END_DATE,


                ["Primary Routing"]: rowData[i].primarY_ROUTING,
                ["Alternate Routing1"]: rowData[i].alternatE_ROUTING_1,
                ["Alternate Routing2"]: rowData[i].alternatE_ROUTING_2,
 

                ["Last Modified By"]: rowData[i].syS_LAST_MODIFIED_BY,
                ["Last Modified Date"]: rowData[i].syS_LAST_MODIFIED_DATE,
                 





            })
        }
        this._routingrulesService.ExcelExport(param);
    }

    AddMFGInfo(event) {
        this.successmessage = "";
        this.errormessage = "";
        
        if (this.selectedFulfillmentSite[0].id != "Select") {
            this.isAddDeletePanel = true;
            this.Fulfillerrormessage = "";
            this.isddlFullfilment_edit = true;
            this.isddlMFGBeging_edit = true;
            this.isddlMFGEnd_edit = true;
            this.SelectedSplitPercentage = ""; 

           this.GetMFGDropdownlist();
        }
        else {
            this.isddlFullfilment_edit = false;
            this.isddlMFGBeging_edit = false;
            this.isddlMFGEnd_edit = false;
            this.Fulfillerrormessage = "Please select a Fulfillment site ";
        }
     


    }

    GetMFGDropdownlist() {



        // alert(this.selectedFulfillmentSite[0].id);
        var Fulfillment = this.selectedFulfillmentSite[0].id;



        

        this._routingrulesService.GetManufacturingSiteData(Fulfillment)
            .subscribe(
                (data: any) => {

                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.ManufacturingSite = [];
                        for (var i = 0; i < data.length; i++) {

                            this.ManufacturingSite.push({
                                id: data[i].itemTypeId,
                                itemName: data[i].itemTypeName
                            })
                        }
                    }
                },
                err => console.log(err),
            );

        this.selectedManufacturingSite[0] = { id: "Select", itemName: "Select" };




    }

    onGridReadyName_Dynaic(params) {
        this.gridApiNameDyanamic = params.api;
        this.gridColumnApiName = params.columnApi;
    }

    onGridReadyAddUpdate(params) {
        this.gridApiName1 = params.api;
        this.gridColumnApiName1 = params.columnApi;
    }

    Adddyanamiccolumn(event) {
        this.successmessage = "";	
        this.errormessage = "";
        
        if (this.selectedManufacturingSite[0].id == "Select" || this.SelectedSplitPercentage == undefined || this.SelectedSplitPercentage =="") {

            this.RequirefieldMessage = "Enter the Required fields.";
        }
        else {

            var SiteFlag = true;
            if (this.DatarowDynamic.length > 0) {

                for (var a = 0; a < this.DatarowDynamic.length; a++) {
                    if (this.selectedManufacturingSite[0].itemName == this.DatarowDynamic[a].site) {
                        SiteFlag = false;
                    }
                }

            } 
            

            if (SiteFlag == true) {
                
                this.isDyanamicGrid = true;

                this.DatarowDynamic.push({

                    site: this.selectedManufacturingSite[0].id,
                    split_percentage: this.SelectedSplitPercentage
                    //fullfilment_site: .this.selectedFulfillmentSite[0].id

                });
                 this.RequirefieldMessage = "";
                this.SelectedSplitPercentage = "";
                this.selectedManufacturingSite[0] = { id: "Select", itemName: "Select" };
                this.gridApiNameDyanamic.setRowData(this.DatarowDynamic);
               
            }
            else {
                //this.SelectedSplitPercentage = "Same Site Cannot Be Configured More Than Once for a Given Product/FGA";
                this.RequirefieldMessage = "Same Site Cannot Be Configured More Than Once for a Given Product/FGA";
            }
        }
        
    }

    DeleteDyanamirow(event) {
        this.RequirefieldMessage = "";
        
        let rowsSelection = this.DynamicGrid.api.getSelectedRows();
        if (rowsSelection.length >0) {
            var x = [];
            for (var i = 0; i < this.DatarowDynamic.length; i++) {
                if (!(rowsSelection.find(x => x.site == this.DatarowDynamic[i].site && x.split_percentage == this.DatarowDynamic[i].split_percentage))) {
                    x.push(this.DatarowDynamic[i]);
                }
            }

            if (rowsSelection.length > 0) {

                this.DatarowDynamic = x;
                this.gridApiNameDyanamic.setRowData(x);

            }
        }
        else {
         
            this.RequirefieldMessage = "No row is selected.Please select row";
        }

        if (this.DatarowDynamic.length > 0) {
            this.isDyanamicGrid = true;
        }
        else {

            this.isDyanamicGrid = false;
            this.isRoutingPanel = false;
            this.ManufactringDisplay = "";
                 
        }


    }

    DeleteAddUpdate(event) {
        //debugger
        //let rowsSelection = this.AddUpdateGrid.api.getSelectedRows();

        //if (rowsSelection.length > 0) {
        //    this.AddupdaterowData.splice(rowsSelection[0], 1);
        //    this.gridApiName1.setRowData(this.AddupdaterowData);
        //}
        //else {

        //    return false;
        //}

        
        let rowsSelection = this.AddUpdateGrid.api.getSelectedRows();


      
        if (rowsSelection.length > 0) {
           
            var x = [];
            for (var i = 0; i < this.AddupdaterowData.length; i++) {
                if (!(rowsSelection.find(x => x.manufacturinG_SITE_ID == this.AddupdaterowData[i].manufacturinG_SITE_ID
                    && x.fulfillmenT_SITE_ID == this.AddupdaterowData[i].fulfillmenT_SITE_ID
                    && x.mfg_Site_Effective_Begin_Date == this.AddupdaterowData[i].mfg_Site_Effective_Begin_Date
                    && x.mfg_Site_Effective_End_Date == this.AddupdaterowData[i].mfg_Site_Effective_End_Date
                    && x.mfG_SITE_SPLIT_PERCENT == this.AddupdaterowData[i].mfG_SITE_SPLIT_PERCENT
                    && x.routing_Effective_Start_Date == this.AddupdaterowData[i].routing_Effective_Start_Date
                    && x.routing_Effective_End_Date == this.AddupdaterowData[i].routing_Effective_End_Date
                    && x.primarY_ROUTING_DESC == this.AddupdaterowData[i].primarY_ROUTING_DESC
                    && x.alternate_Routing1 == this.AddupdaterowData[i].alternate_Routing1
                    && x.alternate_Routing2 == this.AddupdaterowData[i].alternate_Routing2))) {


                    x.push(this.AddupdaterowData[i]);
                }
            }

            if (rowsSelection.length > 0) {

                this.AddupdaterowData = x;
                this.gridApiName1.setRowData(x);

            }
        }
        else {

            this.RoutingErrorMessage = "No row is selected.Please select row";
        }

    }

    AddRoutingRules(event) {
        this.successmessage = "";
        this.errormessage = "";

        this.clearAddUpdate();
       
        let rowsSelection = this.DynamicGrid.api.getSelectedRows();

        this.ManufactringDisplay = rowsSelection[0].site;

        var sum = 0;
        if (this.DatarowDynamic.length > 0) {

            for (var a = 0; a < this.DatarowDynamic.length; a++) {
                
                sum += parseInt(this.DatarowDynamic[a].split_percentage);

               

            }

        } 
     

        
        if (sum == 100) {


              this.isddlFullfilment_edit = true;
            this.isddlMFGBeging_edit = true;
            this.isddlMFGEnd_edit = true;

               if (rowsSelection.length > 0) {
                    this.isRoutingPanel = true;
                    var Getvalue =
                    {
                        ItemTypeName: this.selectedFulfillmentSite[0].id,//strMFGSite,
                        ItemTypeId: rowsSelection[0].site//this.selectedManufacturingSite[0].id //strSite
                    };

                    this._routingrulesService.GetRoutingDropdownvalue(Getvalue)
                        .subscribe(
                            (data: any) => {
                                
                                if (data != null) {
                                    data = JSON.parse(data._body);
                                    this.AlternateRouting1 = [];
                                    this.PrimaryRouting = [];
                                    this.AlternateRouting2 = [];
                                    for (var i = 0; i < data.length; i++) {

                                        this.AlternateRouting1.push({
                                            id: data[i].itemTypeId,
                                            itemName: data[i].itemTypeName
                                        })

                                        this.PrimaryRouting.push({
                                            id: data[i].itemTypeId,
                                            itemName: data[i].itemTypeName
                                        })

                                        this.AlternateRouting2.push({
                                            id: data[i].itemTypeId,
                                            itemName: data[i].itemTypeName
                                        })
                                       
                                        




                                    }
                                    this.SelectAlternateRouting1[0] = { id: "Select", itemName: "Select" };
                                    this.selectedPrimaryRouting[0] = { id: "Select", itemName: "Select" };
                                    this.selectedAlternateRouting2[0] = { id: "Select", itemName: "Select" };

                                    this.isupdatePanel = true;
                                    this.isRoutingPanel = true;

                                }
                            },
                            err => console.log(err),
                        );
                    //  }
                    this.RequirefieldMessage = "";
                }
                else {

                    this.RequirefieldMessage = "No row is selected.Please select row ";
                }

            }
            else {

                this.RequirefieldMessage = "Split Percentage should be 100";
            }

        
    }

    AddUpdateRoutingRules(event) {
       // this.RoutingRulesGrid = [];
        
      //var abc =    this.selectedddlMFGFiscalEndDate[0].id;

     
            if (this.selectedPrimaryRouting[0].id != "Select") {
                if (this.selectedPrimaryRouting[0].id != this.SelectAlternateRouting1[0].id) {
                    if (this.selectedPrimaryRouting[0].id != this.selectedAlternateRouting2[0].id) {

                        this.isRoutingGrid = true;
                        let RowDynamicGridRows = this.DynamicGrid.api.getSelectedRows();

                       
                        this.RoutingRulesGrid = [];
                        if (this.AddupdaterowData.length >= 1) {
                            this.RoutingErrorMessage = "";

                            this.PrimaryRoutingerro = "";
                            //   var j = 0;
                            
                            for (var i = 0; i < this.AddupdaterowData.length; i++) {
                                this.RoutingRulesGrid.push({

                                    routing_Effective_Start_Date: this.AddupdaterowData[i].routing_Effective_Start_Date == undefined ? "" : this.AddupdaterowData[i].routing_Effective_Start_Date.toString(),
                                    manufacturinG_SITE_ID: this.AddupdaterowData[i].manufacturinG_SITE_ID == undefined ? "" : this.AddupdaterowData[i].manufacturinG_SITE_ID.toString(),
                                    fulfillmenT_SITE_ID: this.AddupdaterowData[i].fulfillmenT_SITE_ID == undefined ? "" : this.AddupdaterowData[i].fulfillmenT_SITE_ID.toString(),
                                    mfg_Site_Effective_Begin_Date: this.AddupdaterowData[i].mfg_Site_Effective_Begin_Date == undefined ? "" : this.AddupdaterowData[i].mfg_Site_Effective_Begin_Date.toString(),
                                    mfg_Site_Effective_End_Date: this.AddupdaterowData[i].mfg_Site_Effective_End_Date == undefined ? "" : this.AddupdaterowData[i].mfg_Site_Effective_End_Date.toString(),
                                    mfG_SITE_SPLIT_PERCENT: this.AddupdaterowData[i].mfG_SITE_SPLIT_PERCENT == undefined ? "" : this.AddupdaterowData[i].mfG_SITE_SPLIT_PERCENT.toString(),

                                    routing_Effective_End_Date: this.AddupdaterowData[i].routing_Effective_End_Date == undefined ? "" : this.AddupdaterowData[i].routing_Effective_End_Date.toString(),
                                    primarY_ROUTING_DESC: this.AddupdaterowData[i].primarY_ROUTING_DESC == undefined ? "" : this.AddupdaterowData[i].primarY_ROUTING_DESC.toString(),
                                    alternate_Routing1: this.AddupdaterowData[i].alternate_Routing1 == undefined ? "" : this.AddupdaterowData[i].alternate_Routing1.toString(),
                                    product: this.SelectedProductvaluesecond[0].itemName,
                                    TreeValue: "",
                                    strMFGSiteName: RowDynamicGridRows[0] == undefined ? "" : RowDynamicGridRows[0].site,
                                    strMFGBeginDate: this.selectedEffectiveBeginDate[0] == undefined ? "" : this.selectedEffectiveBeginDate[0].id,
                                    strMFGEndDate: this.selecteddEffectiveEndDate[0] == undefined ? "" : this.selecteddEffectiveEndDate[0].id,
                                    strFFSite: this.selectedFulfillmentSite[0] == undefined ? "" : this.selectedFulfillmentSite[0].id,
                                    strNewRoutingBeginDate: this.selectedMFGFiscalBeginDate[0] == undefined ? "" : this.selectedMFGFiscalBeginDate[0].id,
                                    strNewRoutingEndDate: this.selectedddlMFGFiscalEndDate[0] == undefined ? "" : this.selectedddlMFGFiscalEndDate[0].id,
                                    id: "",
                                    strStatus: "",
                                    strRoutingIdRule: "",
                                    strRoutingIDGrid: this.AddupdaterowData[i].routinG_ID == undefined ? "" : this.AddupdaterowData[i].routinG_ID.toString(),
                                    alternate_Routing2: this.AddupdaterowData[i].alternate_Routing2 == undefined ? "" : this.AddupdaterowData[i].alternate_Routing2.toString(),
                                    hdfTypeRetrieve: "",
                                    primarY_ROUTING: this.AddupdaterowData[i].primarY_ROUTING == undefined ? "" : this.AddupdaterowData[i].primarY_ROUTING.toString(),
                                    alternate_Routing1_des: this.AddupdaterowData[i].alternate_Routing1_des == undefined ? "" : this.AddupdaterowData[i].alternate_Routing1_des.toString(),
                                    alternate_Routing2_des: this.AddupdaterowData[i].alternate_Routing2_des == undefined ? "" : this.AddupdaterowData[i].alternate_Routing2_des.toString(),
                                });
                                //  j++;
                            }



                            // if (this.AddupdaterowData.length == j) {
                            this._routingrulesService.DateValidate(this.RoutingRulesGrid)
                                .subscribe(
                                    (data: any) => {
                                        if (data._body.toString() == "true") {
                                            this.AddNewrow(RowDynamicGridRows);
                                            this.RoutingErrorMessage = "";

                                            this.clearAddUpdate();

                                        }
                                        else {
                                            this.RoutingErrorMessage = "New Routing Dates are overlapping with the existing Dates";
                                        }
                                    });


                            // }
                        }




                        
                        if (this.AddupdaterowData.length == 0) {
                            this.AddupdaterowData.push({


                                mfg_Site_Effective_Begin_Date: this.selectedEffectiveBeginDate[0] == undefined ? "" : this.selectedEffectiveBeginDate[0].id, //this.selectedMFGFiscalBeginDate[0].id,
                                mfg_Site_Effective_End_Date: this.selecteddEffectiveEndDate[0] == undefined ? "" : this.selecteddEffectiveEndDate[0].id, // this.selectedddlMFGFiscalEndDate[0].id,

                                manufacturinG_SITE_ID: RowDynamicGridRows[0].site == undefined ? "" : RowDynamicGridRows[0].site,  //this.selectedManufacturingSite[0].id,
                                fulfillmenT_SITE_ID: this.selectedFulfillmentSite[0] == undefined ? "" : this.selectedFulfillmentSite[0].id,  //this.selectedFulfillmentSite[0].id,

                                mfG_SITE_SPLIT_PERCENT: RowDynamicGridRows[0] == undefined ? "" : RowDynamicGridRows[0].split_percentage,


                                routing_Effective_Start_Date: this.selectedMFGFiscalBeginDate[0] == undefined ? "" : this.selectedMFGFiscalBeginDate[0].id,// this.selectedddlMFGFiscalEndDate[0].id,
                                routing_Effective_End_Date: this.selectedddlMFGFiscalEndDate[0] == undefined ? "" : this.selectedddlMFGFiscalEndDate[0].id, //this.selectedddlMFGFiscalEndDate[0].id,



                                primarY_ROUTING_DESC: this.selectedPrimaryRouting[0].id == "Select" ? "" : this.selectedPrimaryRouting[0].itemName,//this.selectedPrimaryRouting[0].itemName,
                                alternate_Routing1: this.SelectAlternateRouting1[0].id == "Select" ? "" : this.SelectAlternateRouting1[0].id,//this.SelectAlternateRouting1[0].itemName,
                                alternate_Routing2: this.selectedAlternateRouting2[0].id == "Select" ? "" : this.selectedAlternateRouting2[0].id,//this.selectedAlternateRouting2[0].itemName

                                primarY_ROUTING: this.selectedPrimaryRouting[0].id == "Select" ? "" : this.selectedPrimaryRouting[0].id,
                                id: "",
                                routinG_ID: "",
                                alternate_Routing1_des: this.SelectAlternateRouting1[0].id == "Select" ? "" : this.SelectAlternateRouting1[0].itemName,
                                alternate_Routing2_des: this.selectedAlternateRouting2[0].id == "Select" ? "" : this.selectedAlternateRouting2[0].itemName,


                            });


                            this.SelectAlternateRouting1[0] = { id: "Select", itemName: "Select" };
                            this.selectedPrimaryRouting[0] = { id: "Select", itemName: "Select" };
                            this.selectedAlternateRouting2[0] = { id: "Select", itemName: "Select" };
                            this.selectedMFGFiscalBeginDate[0] = { id: "", itemName: "" };
                            this.selectedddlMFGFiscalEndDate[0] = { id: "", itemName: "" };

                            this.clearAddUpdate();
                            this.gridApiName1.setRowData(this.AddupdaterowData);

                            this.RoutingErrorMessage = "";
                            this.PrimaryRoutingerro = "";
                           
                        }

                        this.PrimaryRoutingerro = "";

                    }
                    else {
                        this.PrimaryRoutingerro = "";
                        //  this.RoutingErrorMessage = "Primary Routing and Alternate Routing 2 should not be same. ";
                        this.PrimaryRoutingerro = "Primary Routing and Alternate Routing 2 should not be same. ";
                    }
                }
                else {
                    this.PrimaryRoutingerro = "";
                    //  this.RoutingErrorMessage = "Primary Routing and Alternate Routing 1 should not be same. ";
                    this.PrimaryRoutingerro = "Primary Routing and Alternate Routing 1 should not be same. ";
                }
            }
            else {
                this.RoutingErrorMessage = "";
                this.PrimaryRoutingerro = "Select Primary Routing.";
            }
       

    }


    onSelectionChanged_Update(event: any) {

        let DeleteRow = this.AddUpdateGrid.api.getSelectedRows();
        if (DeleteRow.length > 0) {

            if (DeleteRow[0].routinG_ID != "") {
                this.DeleteAddUpdate_Delete = true;
                this.RequirefieldMessage = "";
            }
            else {
                this.DeleteAddUpdate_Delete = false;
               
            }

        }
    }
 
    AddNewrow(rowsSelection :any) {

        
        if (this.AreRoutingDatesValid == "True") {
            this.AddupdaterowData.push({

                
                mfg_Site_Effective_Begin_Date: this.selectedEffectiveBeginDate[0] == undefined ? "" : this.selectedEffectiveBeginDate[0].id, //this.selectedMFGFiscalBeginDate[0].id,
                mfg_Site_Effective_End_Date: this.selecteddEffectiveEndDate[0] == undefined ? "" : this.selecteddEffectiveEndDate[0].id, // this.selectedddlMFGFiscalEndDate[0].id,


                manufacturinG_SITE_ID: rowsSelection[0].site == undefined ? "" : rowsSelection[0].site,  //this.selectedManufacturingSite[0].id,
                fulfillmenT_SITE_ID: this.selectedFulfillmentSite[0] == undefined ? "" : this.selectedFulfillmentSite[0].id,  //this.selectedFulfillmentSite[0].id,
                mfG_SITE_SPLIT_PERCENT: rowsSelection[0] == undefined ? "" : rowsSelection[0].split_percentage,


                
                routing_Effective_Start_Date: this.selectedMFGFiscalBeginDate[0] == undefined ? "" : this.selectedMFGFiscalBeginDate[0].id,// this.selectedddlMFGFiscalEndDate[0].id,
                routing_Effective_End_Date: this.selectedddlMFGFiscalEndDate[0] == undefined ? "" : this.selectedddlMFGFiscalEndDate[0].id, //this.selectedddlMFGFiscalEndDate[0].id,


                primarY_ROUTING_DESC: this.selectedPrimaryRouting[0].id == "Select" ? "" : this.selectedPrimaryRouting[0].itemName,//this.selectedPrimaryRouting[0].itemName,
                alternate_Routing1: this.SelectAlternateRouting1[0].id == "Select" ? "" : this.SelectAlternateRouting1[0].id,//this.SelectAlternateRouting1[0].itemName,
                alternate_Routing2: this.selectedAlternateRouting2[0].id == "Select" ? "" : this.selectedAlternateRouting2[0].id,//this.selectedAlternateRouting2[0].itemName

                primarY_ROUTING: this.selectedPrimaryRouting[0].id == "Select" ? "" : this.selectedPrimaryRouting[0].id,
                id: "",
                routinG_ID: "",
                alternate_Routing1_des: this.SelectAlternateRouting1[0].id == "Select" ? "" : this.SelectAlternateRouting1[0].itemName,
                alternate_Routing2_des: this.selectedAlternateRouting2[0].id == "Select" ? "" : this.selectedAlternateRouting2[0].itemName,
               

            });

            this.gridApiName1.setRowData(this.AddupdaterowData);


            this.RoutingErrorMessage = "";
            this.PrimaryRoutingerro = "";

        }
        else {

            this.RoutingErrorMessage = "New Routing Dates are overlapping with the existing Dates";

        }

    }

    //ChekcDatevalidation( rowsSelection:any ) {
        
    //    this.RoutingRulesGrid = [];
    //    if (this.AddupdaterowData.length > 0) {
    //        this.RoutingErrorMessage = "";
    //        // let param = [];
    //        for (var i = 0; i < this.AddupdaterowData.length; i++) {
    //            this.RoutingRulesGrid.push({

    //                Routing_Effective_Start_Date: this.AddupdaterowData[0].Routing_Effective_Start_Date.toString(),
    //                manufacturinG_SITE_ID: this.AddupdaterowData[0].manufacturinG_SITE_ID.toString(),
    //                fulfillmenT_SITE_ID: this.AddupdaterowData[0].fulfillmenT_SITE_ID.toString(),
    //                Mfg_Site_Effective_Begin_Date: this.AddupdaterowData[0].Mfg_Site_Effective_Begin_Date.toString(),
    //                Mfg_Site_Effective_End_Date: this.AddupdaterowData[0].Mfg_Site_Effective_End_Date.toString(),
    //                mfG_SITE_SPLIT_PERCENT: this.AddupdaterowData[0].mfG_SITE_SPLIT_PERCENT.toString(),

    //                Routing_Effective_End_Date: this.AddupdaterowData[0].Routing_Effective_End_Date.toString(),
    //                primarY_ROUTING_DESC: this.AddupdaterowData[0].primarY_ROUTING_DESC.toString(),
    //                Alternate_Routing1: this.AddupdaterowData[0].Alternate_Routing1.toString(),
    //                product: this.SelectedProductvaluesecond[0].itemName,
    //                TreeValue: "",
    //                strMFGSiteName: rowsSelection[0] == undefined ? "" :rowsSelection[0].site,
    //                strMFGBeginDate: this.selectedEffectiveBeginDate[0]== undefined ? "": this.selectedEffectiveBeginDate[0].id,
    //                strMFGEndDate: this.selecteddEffectiveEndDate[0] == undefined ? "" : this.selecteddEffectiveEndDate[0].id,
    //                strFFSite: this.selectedFulfillmentSite[0] == undefined ? "" : this.selectedFulfillmentSite[0].id,
    //                strNewRoutingBeginDate: this.selectedMFGFiscalBeginDate[0] == undefined ? "" :  this.selectedMFGFiscalBeginDate[0].id,
    //                strNewRoutingEndDate: this.selectedddlMFGFiscalEndDate[0] == undefined ? "" : this.selectedddlMFGFiscalEndDate[0].id,
    //                id: "",
    //                strStatus: "",
    //                strRoutingIdRule :"",
    //                strRoutingIDGrid: "",
    //                Alternate_Routing2: this.AddupdaterowData[0].Alternate_Routing2.toString(), 
    //                hdfTypeRetrieve: "",
    //                primarY_ROUTING: this.AddupdaterowData[0] == undefined ? "" : this.AddupdaterowData[0].primarY_ROUTING.toString(),

    //            });
    //        }


            
    //        this._routingrulesService.DateValidate(this.RoutingRulesGrid)
    //            .map((data: any) => data.json()).subscribe((data: any) => {
                   
    //        return data;

    //            },
    //                err => console.log(err), // error
    //            )

    //    }
    //    else {
    //        this.RoutingErrorMessage = "No records is there";
    //    }


    //}
    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;

    }

    clearFilterData() {

        this.IseSearchGridSection = false;
        this.selectedProductTreeData_label = "";

        this.isProductTree = true;

        this.SelectedProductvalue[0] = { id: "ALL", itemName: "ALL" };
        this.Selectedgeographyvalue[0] = { id: "ALL", itemName: "ALL" };
        this.NoRoutingsConfigured = "";
        this.successmessage = "";
        this.errormessage = "";
        this.isCopyRulesPanel = false;
        
    }

    clearCreateData() {

        this.isupdatePanel = false;
        this.isRoutingPanel = false;


        this.isAddDeletePanel = false;
        this.isDyanamicGrid = false;

        this.SelectedProductvaluesecond[0] = { id: "ALL", itemName: "ALL" };
        this.SavedMessage = "";
        this.isRoutingGrid = false;
        this.AddupdaterowData = [];

        this.RetriveRoutingRule_Edit = false,
        this.isddlProductSecond_edit = false;
        this.isddlProductSecond_edit_image = true;
        this.SelectedTreeLableSecond = "";
        this.selectedProductTreeData_create_label = "";
      
        this.isProductSeconfTree = true;
        this.DatarowDynamic = [];
        this.PrimaryRoutingerro = "";
        this.NoRoutingsConfiguredAddUpdate = "";
        this.selectedFulfillmentSite[0] = { id: "Select", itemName: "Select" };

        this.isddlFullfilment_edit = false;
        this.isddlMFGBeging_edit = false;
        this.isddlMFGEnd_edit = false;
        this.successmessage = "";
        this.errormessage = "";
    }

    clearMFGinfo() {

        this.isAddDeletePanel = false;
        this.isRoutingPanel = false;
        this.isDyanamicGrid = false;
       // this.isAddDeletePanel = false;

        this.isddlFullfilment_edit = false;
        this.isddlMFGBeging_edit = false;
        this.isddlMFGEnd_edit = false;

        this.selectedEffectiveBeginDate[0] = { id: "", itemName: "" };
        this.selecteddEffectiveEndDate[0] = { id: "", itemName: "" };
        this.selectedFulfillmentSite[0] = { id: "Select", itemName: "Select" };

        this.DatarowDynamic = [];
        this.RequirefieldMessage = "";
        this.PrimaryRoutingerro = "";
        this.Fulfillerrormessage = "";
        this.successmessage = "";
        this.errormessage = "";
    }

    clearAddUpdate() {

        this.SelectAlternateRouting1[0] = { id: "Select", itemName: "Select" };
        this.selectedPrimaryRouting[0] = { id: "Select", itemName: "Select" };
        this.selectedAlternateRouting2[0] = { id: "Select", itemName: "Select" };

        this.selectedMFGFiscalBeginDate[0] = { id: "", itemName: "" };
        this.selectedddlMFGFiscalEndDate[0] = { id: "", itemName: "" };
        this.RoutingErrorMessage = "";
        this.PrimaryRoutingerro = "";
    }




    CopyRoutingRules(event) {
      //  this.GetFGA();
        this.successmessage = "";	
        this.errormessage = "";
        this.SelectedFGA = [];
            
        this.CopySuccessMessage = "";
        this.CopyfgaMessage = "";
        this.SelectedCopyProductvalue[0] = { id: "ALL", itemName: "ALL" };
        this.isProductTreeCopyRules = true;
        this.CopyselectedProductTreeData_label = "";
        this.isFGACopyRules = false;

        
        
       // var SelectedRow = this.gridOptionsName.api.getSelectedRows();
       // var routinG_ID = this.CopyruleSelectedRow[0].routinG_ID;

        let SelectedRow = this.gridOptionsName.api.getSelectedRows();

         
        if (SelectedRow.length > 0) {
            if (this.CopyruleSelectedRow.length == 1) {
                this.GetFGA();
                this.isCopyRulesPanel = true;
                this.routingId = this.CopyruleSelectedRow[0].routinG_ID; //SelectedRow[0].routinG_ID;
                this.errormessage = "";
            } else {
                this.errormessage = "";
                this.errormessage = "One Rule can be copy at a time.";
            }
        }
        else {
            this.errormessage = "";
            this.errormessage = "No row is selected. Please select row ";

        }
    }



    Close(event) {

        this.isCopyRulesPanel = false;
        this.isFGACopyRules = false;
    }

    SubmiteCopyRules(event) {

        
        
        var lblproduct = "";
        this.SearchViewCopy =
            {
                Product: this.SelectedCopyProductvalue[0] == undefined ? "" : this.SelectedCopyProductvalue[0].itemName,
                ddlFGA: this.SelectedFGA[0] == undefined ? "" : this.SelectedFGA[0].id,
                routingId: this.routingId,// this.SelectedCopyProductTree,// this.selectedProductTreeData_label,
                hfItemID: this.CopyselectedProductTreeData_label == undefined ? "" : this.CopyselectedProductTreeData_label,
                hfFHCType: "",

            };

        


        this._routingrulesService.SubmiteCopyRules(this.SearchViewCopy)
            .subscribe(
            (data: any) => {
                
                    if (data._body.length > 0) {

                        if (data._body == "1") {
                            this.CopySuccessMessage = "Copied rules successfully";
                            this.CopyfgaMessage = "";
                            this.GetRoutingFilterViewResult();

                            this.SelectedCopyProductvalue[0] = { id: "ALL", itemName: "ALL" };
                            this.isProductTreeCopyRules = true;
                            this.CopyselectedProductTreeData_label = "";
                            this.SelectedFGA[0] = { id: "Select", itemName: "Select" };
                            
                            
                        }
                        else {
                            this.CopyfgaMessage = data._body;
                            this.CopySuccessMessage = "";
                        }
 
                    }
                    
                });




    }



    onSelectionChanged_Main(event) {
         
        
        let rowsSelection = this.gridOptionsName.api.getSelectedRows();


        if (rowsSelection.length == 1) {
            this.CopyruleSelectedRow = rowsSelection;

        }
        else {
            this.CopyruleSelectedRow = [];
            this.isCopyRulesPanel = false;
        }

       // alert(rowsSelection);
    }


}
 





//To Enable bootstrap multi select inside grid for checkbox
function loadJavaScript() {
    var url = '/dist/grid-multi-select.js';
    var s = document.createElement('script');
    s.setAttribute('src', url);
    document.head.appendChild(s);
}
