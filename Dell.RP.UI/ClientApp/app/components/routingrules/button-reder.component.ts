﻿import { Component, AfterViewInit, ViewChild, ElementRef } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
@Component({
    selector: 'button-cell',
    //template: `<input type="button" value"add" (change)="onChange($event)">`,


    template:  '<button name = "btnaddupdate" id = "btnaddupdate" > Add / Update Routing < /button>'

})

export class buttonreder implements ICellRendererAngularComp {

    @ViewChild('.button') button: ElementRef;

    public params: any;
    private multiTier: boolean = false;


    agInit(params: any): void {
        

    }

    constructor() { }

    refresh(): boolean {
        return false;
    }

    public onChange(event: any) {

        this.params.data[this.params.colDef.field] = event.currentTarget.checked;

    }
}