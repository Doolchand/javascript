import { Component, OnInit, ViewEncapsulation, ViewChild, Input, ElementRef } from '@angular/core';
import { ColumnApi, GridApi, GridOptions } from "ag-grid/main";
import * as XLSX from 'xlsx';
import { dashCaseToCamelCase } from '@angular/compiler/src/util';
import { debugOutputAstAsTypeScript } from '@angular/compiler';
import { DebugContext } from '@angular/core/src/view';
import { debounce } from 'rxjs/operator/debounce';
import { UserRoles } from '../../Model/user-roles';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { UserMaintenanceService } from '../../services/user-maintenance.service';
import { UserDetails } from '../../Model/user-details';
import { CheckboxCellCommonComponent } from '../checkbox-renderer.component';
import { forEach } from '@angular/router/src/utils/collection';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UtilitiesService } from '../../services/utilities.service';

@Component({
    selector: 'app-user-maintenance',
    templateUrl: './user-maintenance.component.html',
    styleUrls: ['./user-maintenance.component.css'],
    
    encapsulation: ViewEncapsulation.None
})
export class UserMaintenanceComponent implements OnInit {

    debugger;

    @ViewChild('fakeClick') fakeClick: ElementRef;
    //Authorisation 
    userRoles: UserRoles[];
    isUserHavingWriteAccess: boolean = false;
    isActiveScreen: boolean = true;
    screenId = "2002";
    userHavingWriteAccess: string[];
    listOfRoleHavingAccess: string[]; //=  ['1002','1004','1005','1006','2013','2012','2008','2007','2002'];
    isUserHavingUserMaintenance: boolean = false;
    loggedInUserName: string;
    loggedInUserDomain: string;
    userTenantTypeAccessList: string[];
    ProductLobAdminId = "2002";
    uppTenant: string = "UPP";

    //Error/Success Message     
    colorCode: string;
    successMsg: string = "";
    public hideShowAlertMsg = false;

    //Add User 
    userDetails: UserDetails;
    singleSelectDropdownSettings = {};
    multiSelectDropdownSettings = {};
    selected_region = [];
    selectedUserGroups = [];
    selectedRoles = [];
    selectedTenant = [];
    region = [];
    userGroups = [];
    roles = [];
    LOBList = [];
    selectedLobs = [];
    TenantList = [];
    userName = "";
    emailId = "";
    domain = "";
    showEmailId = false;
    rolesListCommaSeprated: string = "";
    lobListCommaSeparated: string = "";
    showLobList: boolean = false;
    IsDisableRolesDD: boolean = false;

    //filter section
 //   filterDetails: FilterDetails;
    ActiveInactiveList = [];
    userNameList = [];
    selectedRegionFilter = [];
    selectedUserNameFilter = [];
    selectedUserGroupsFilter = [];
    selectedActiveInactiveFilter = [];
    selectedTenantFilter = [];

    //grid variable - FGA
    userDetailsList: UserDetails[];
    myOptions: any[] = [];
    private gridOptions: GridOptions;
    private rowData;
    private gridColumnApi;
    private gridApi;
    private context;
    private frameworkComponents;
    gridBtnsave: boolean = false;
    gridBtnClear: boolean = false;
    gridBtnExcelExport: boolean = false;
    gridBtnExcelExportFilter: boolean = false;
    private params: any;
    private getRowNodeId;
    private components;
    blankUserIds: string = "";
    OrignalRowData = [];
    isUppUserFilter = false;
    rolesGrid = [];

    constructor(private _router: Router, private _userMaintenanceService: UserMaintenanceService, private _sessionSt: SessionStorageService, private _utilitiesService: UtilitiesService) {
        console.log("Doolchand");
        debugger;
    }

    ngOnInit() {
       
    }

   

   
}

 

//To Enable bootstrap multi select inside grid for checkbox
function loadJavaScript() {
    var url = '/dist/grid-multi-select.js';
    var s = document.createElement('script');
    s.setAttribute('src', url);
    document.head.appendChild(s);
}

 