﻿import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, Input } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { SessionStorageService } from 'ngx-webstorage';
import * as XLSX from 'xlsx';
import { UserRoles } from '../../Model/user-roles';
import { demandsplitService } from '../../services/demand-split.service';
import { UtilitiesService } from '../../services/utilities.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetErrorGridService } from '../../services/get-error-grid.service';

import { DemandSplitSearch } from '../../Model/DemandSplitSearch';
 
import { from } from 'rxjs/observable/from';


declare var jQuery: any;

@Component({
    selector: 'app-demand-split',
    templateUrl: './demand-split.component.html',
    styleUrls: ['./demand-split.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class DemandsplitComponent implements OnInit {

  
    @ViewChild('fakeClick') fakeClick: ElementRef;
    private gridOptionsName: GridOptions;
    private errGridOptions: GridOptions;
    public _DemandSplitExport: DemandSplitExport[];
    SelectedProductvalue = [];
    singleSelectDropdownSettingsProduct = {};
    Productdata = [];

    Selectedchanelvalue = [];
    singleSelectDropdownSettingschannel = {};
    channeldata = [];

    Selectedgeographyvalue =[];
    singleSelectDropdownSettingsgeography = {};
    geographyldata = [];

    SearchView: DemandSplitSearch;

    private rowDataName;
    private errRowData;
    private rowDatatemplet: any;
    excelfile: any;
    @ViewChild('file') FileName: any;
    selectedfilename: string;
    private TotalRows ;
    private Recordsfailed;
    private RecordsCorrect;
    private TotalError;

    private ProductName;
    private ChannelName;
    private GeoName;
    private FilleerrorMessage;
    private fileuplaodName; 
    private IserrorGrid;
    private IsdemandGrid = true;
    private Totalsuberror;
    private Isexport = true;
    private productnamedisplay = true;

    private isvisibleerrodiv = false;
    private Sucessfullmessage;

    private errormessagedisplay = false;
    private errormessagedisplayscusses = false;

    private ErrorMessage;



    userRoles: UserRoles[];
    screenId = "2003";
    isUserHavingWriteAccess: boolean = false;
    isActiveScreen: boolean = true;
    userHavingWriteAccess: string[];
    listOfRoleHavingAccess: string[];
    userTenantTypeAccessList: string[];
    isUserHavingUserMaintenance: boolean = false;


    DemandSplitExcelHeader: string[] = ['PRODUCT_ID', 'CHANNEL_ID', 'GEOGRAPHY_ID', 'SSC', 'WK0', 'WK1', 'WK2', 'WK3', 'WK4',
        'WK5', 'WK6', 'WK7', 'WK8', 'WK9', 'WK10', 'WK11', 'WK12', 'WK13', 'WK14', 'WK15', 'WK16', 'WK17', 'WK18', 'WK19', 'WK20',
        'WK21', 'WK22', 'WK23', 'WK24', 'WK25', 'WK26', 'REST_OF_HORIZON', 'SYS_LAST_MODIFIED_BY', 'SYS_LAST_MODIFIED_DATE'
    ];
    constructor(private _router: Router, private _demandsplitService: demandsplitService, private _errorGridService: GetErrorGridService, private _sessionSt: SessionStorageService, private _utilitiesService: UtilitiesService) {

        this.Authorization();

        //Grid EMC Hierarchy Grid   
        this.gridOptionsName = <GridOptions>{
            contextName: {
                componentParentName: this
            },
        };
        this.gridOptionsName.columnDefs = [
            { headerName: "Product", field: "product_ID", suppressSizeToFit: false, tooltipField: "Product", editable: false, width: 190,  },
            { headerName: "Channel", field: "channel_ID", suppressSizeToFit: false, tooltipField: "Channel", editable: false, width: 150,},
            { headerName: "Geography", field: "geography_ID", suppressSizeToFit: false, tooltipField: "Geography", editable: false, width: 150, },
            { headerName: "SSC", field: "ssc", suppressSizeToFit: false, tooltipField: "SSC", editable: false, width: 45, },
            { headerName: "WK0", field: "wK0", suppressSizeToFit: false, tooltipField: "WK0", editable: false, width: 45, },
            { headerName: "WK1", field: "wK1", suppressSizeToFit: false, tooltipField: "WK1", editable: false, width: 45,},
            { headerName: "WK2", field: "wK2", suppressSizeToFit: false, tooltipField: "WK2", editable: false, width: 45,},
            { headerName: "WK3", field: "wK3", suppressSizeToFit: false, tooltipField: "WK3", editable: false, width: 45,},
            { headerName: "WK4", field: "wK4", suppressSizeToFit: false, tooltipField: "WK4", editable: false, width: 45,},
            { headerName: "WK5", field: "wK5", suppressSizeToFit: false, tooltipField: "WK5", editable: false, width: 45, },
            { headerName: "WK6", field: "wK6", suppressSizeToFit: false, tooltipField: "WK6", editable: false, width: 45,},
            { headerName: "WK7", field: "wK7", suppressSizeToFit: false, tooltipField: "WK7", editable: false, width: 45,},
            { headerName: "WK8", field: "wK8", suppressSizeToFit: false, tooltipField: "WK8", editable: false, width: 45,},
            { headerName: "WK9", field: "wK9", suppressSizeToFit: false, tooltipField: "WK9", editable: false, width: 45,},
            { headerName: "WK10", field: "wK10", suppressSizeToFit: false, tooltipField: "WK10", editable: false, width: 45, },
            { headerName: "WK11", field: "wK11", suppressSizeToFit: false, tooltipField: "WK11", editable: false, width: 45,},
            { headerName: "WK12", field: "wK12", suppressSizeToFit: false, tooltipField: "WK12", editable: false, width: 45,},
            { headerName: "WK13", field: "wK13", suppressSizeToFit: false, tooltipField: "WK13", editable: false, width: 45, },
            { headerName: "WK14", field: "wK14", suppressSizeToFit: false, tooltipField: "WK14", editable: false, width: 45, },
            { headerName: "WK15", field: "wK15", suppressSizeToFit: false, tooltipField: "WK15", editable: false, width: 45,},
            { headerName: "WK16", field: "wK16", suppressSizeToFit: false, tooltipField: "WK16", editable: false, width: 45,},
            { headerName: "WK17", field: "wK17", suppressSizeToFit: false, tooltipField: "WK17", editable: false, width: 45, },
            { headerName: "WK18", field: "wK18", suppressSizeToFit: false, tooltipField: "WK18", editable: false, width: 45,},
            { headerName: "WK19", field: "wK19", suppressSizeToFit: false, tooltipField: "WK19", editable: false, width: 45,},
            { headerName: "WK20", field: "wK20", suppressSizeToFit: false, tooltipField: "WK20", editable: false, width: 45,},

            { headerName: "WK21", field: "wK21", suppressSizeToFit: false, tooltipField: "WK21", editable: false, width: 45,},
            { headerName: "WK22", field: "wK22", suppressSizeToFit: false, tooltipField: "WK22", editable: false, width: 45,},
            { headerName: "WK23", field: "wK23", suppressSizeToFit: false, tooltipField: "WK023", editable: false, width: 45, },
            { headerName: "WK24", field: "wK24", suppressSizeToFit: false, tooltipField: "WK24", editable: false, width: 45, },
            { headerName: "WK25", field: "wK25", suppressSizeToFit: false, tooltipField: "WK25", editable: false, width: 45,},
            { headerName: "WK26", field: "wK26", suppressSizeToFit: false, tooltipField: "WK26", editable: false, width: 45,},
            { headerName: "Rest of Horizon", field: "rest_Of_Horizon", suppressSizeToFit: false, tooltipField: "Rest of Horizon", editable: false, width: 80,},
            { headerName: "LAST MODIFIED BY ", field: "sys_last_Modified_By", suppressSizeToFit: false, tooltipField: "LAST MODIFIED BY", editable: false, width: 80, },
            { headerName: "LAST MODIFIED DATE ", field: "sys_last_Modified_Date", suppressSizeToFit: false, tooltipField: "LAST MODIFIED DATE", editable: false, width: 80, },
           

        ];
        this.gridOptionsName.headerHeight = 18;
        this.gridOptionsName.rowSelection = "multiple";
        this.gridOptionsName.paginationPageSize = 14;
        this.gridOptionsName.defaultColDef = {
            editable: true,
            enableValue: true
        };
        this.gridOptionsName.rowData = [];
       
        //this.contextName = { componentParent: this };
        //this.componentsName = {

        //}


        this.errGridOptions = <GridOptions>{
            contextName: {
                componentParentName: this
            },
        };




        this.errGridOptions.columnDefs = [
            { headerName: "Row Number", field: "rowrumber", suppressSizeToFit: false, tooltipField: "Product", editable: false, width: 400, },
            { headerName: "Error Column", field: "errorcolumn", suppressSizeToFit: false, tooltipField: "Channel", editable: false, width: 400, },
            { headerName: "Error", field: "error", suppressSizeToFit: false, tooltipField: "Channel", editable: false, width: 400, },




        ];
        this.errGridOptions.headerHeight = 18;
        this.errGridOptions.rowSelection = "multiple";
        this.errGridOptions.paginationPageSize = 14;
        this.errGridOptions.defaultColDef = {
            editable: true,
            enableValue: true
        };
        this.errGridOptions.rowData = [];


      
    }

    //initilasying the dropdown members
    ngOnInit() {


        this.singleSelectDropdownSettingsProduct = {
            singleSelection: true,
            text: "ALL",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,  
            classes: "myclass custom-class singleSelect"
        };

        this.singleSelectDropdownSettingschannel = {
            singleSelection: true,
            text: "ALL",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };

        this.singleSelectDropdownSettingsgeography = {
            singleSelection: true,
            text: "ALL",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,  
            classes: "myclass custom-class singleSelect"
        };

        this.Selectedchanelvalue[0] = { id: "ALL", itemName: "ALL" };
        this.SelectedProductvalue[0] = { id: "ALL", itemName: "ALL" };
        this.Selectedgeographyvalue[0] = { id: "ALL", itemName: "ALL" };


        this.getProduct();

        this.getChennal();

        this.getGeography();

        this.GetEmcFilterViewResult();

    }


    Authorization() {
        var rolesHavingWriteAccess = this._sessionSt.retrieve('screenAuthorization') != null ? this._sessionSt.retrieve('screenAuthorization') : [];
        this.userHavingWriteAccess = rolesHavingWriteAccess[rolesHavingWriteAccess.findIndex(x => x.functionId == this.screenId)]["roleId"].split(',');
        this.userTenantTypeAccessList = this._sessionSt.retrieve('userTenantTypeAccessList') != null ? this._sessionSt.retrieve('userTenantTypeAccessList') : [];

        if (this._sessionSt.retrieve('userRoles') === null) {
            this.isActiveScreen = false;
            this._router.navigateByUrl('/errorMessage');
            return;
        }
        else {
            this.userRoles = this._sessionSt.retrieve('userRoles');
          
            for (var i = 0; i < this.userRoles.length; i++) {
                if (this.userHavingWriteAccess.includes(this.userRoles[i].roleId.toString())) {
                    this.isUserHavingWriteAccess = true;
                    return;
                }
            }

            if (!(this.isUserHavingWriteAccess || this.userTenantTypeAccessList.includes("GMP"))) {
                this.isActiveScreen = false;
                this._router.navigateByUrl('/errorMessage');
                return;
            }
        }
    }

    getChennal() {

        this._demandsplitService.getChennal()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        this.channeldata = [];
                        for (var i = 0; i < data.length; i++) {

                            this.channeldata.push({
                                id: data[i].itemTypeId,
                                itemName: data[i].itemTypeName
                            })
 
                        }
                    }
                },
                err => console.log(err),
            );
    }

    getProduct() {

        this._demandsplitService.getProduct()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        this.Productdata = [];
                        for (var i = 0; i < data.length; i++) {

                            this.Productdata.push({
                                id: data[i].itemTypeId,
                                itemName: data[i].itemTypeName
                            })

                        }
                    }
                },
                err => console.log(err),
            );
    }

    getGeography() {

        this._demandsplitService.getGeography()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        this.geographyldata = [];
                        for (var i = 0; i < data.length; i++) {

                            this.geographyldata.push({
                                id: data[i].itemTypeId,
                                itemName: data[i].itemTypeName
                            })

                      

                            //this.productTypeListCommaSeperated += data[i].id + ",";
                            //this.productTypeListCommaSeperated.substring(0, this.productTypeListCommaSeperated.length - 1);
                        }
                    }
                },
                err => console.log(err),
            );
    }

    DownloadTemplate(event) {
        
        event.stopPropagation();
        this.getHeaderData();
        this._demandsplitService.exportAsExcelFile(this.rowDatatemplet, "UnspecifiedDemandSplit");
    }

    getHeaderData() {
        this.rowDatatemplet = [{
            "PRODUCT_ID": "",
            "CHANNEL_ID": "",
            "GEOGRAPHY_ID": "",
            "SSC": "",
            "WK0": "",
            "WK1": "",
            "WK2": "",
            "WK3": "",
            "WK4": "",
            "WK5": "",
            "WK6": "",
            "WK7": "",
            "WK8": "",
            "WK9": "",
            "WK10": "",
            "WK11": "",
            "WK12": "",
            "WK13": "",
            "WK14": "",
            "WK15": "",
            "WK16": "",
            "WK17": "",
            "WK18": "",
            "WK19": "",
            "WK20": "",
            "WK21": "",
            "WK22": "",
            "WK23": "",
            "WK24": "",
            "WK25": "",
            "WK26": "",
            "REST_OF_HORIZON": "",
            "SYS_LAST_MODIFIED_BY": "",
            "SYS_LAST_MODIFIED_DATE":"",
            
        }]
    }

    GetEmcFilterViewResult() {
        
        
        this.SearchView 

        this.ProductName = this.SelectedProductvalue[0].id;
        this.ChannelName = this.Selectedchanelvalue[0].id;
        this.GeoName = this.Selectedgeographyvalue[0].id;


        
        this.SearchView = 
            {
            product:  this.SelectedProductvalue[0] == undefined ? "" : this.SelectedProductvalue[0].id,
            chennal:   this.Selectedchanelvalue[0] == undefined ? "" : this.Selectedchanelvalue[0].id,
            geography:  this.Selectedgeographyvalue[0] == undefined ? "" : this.Selectedgeographyvalue[0].id,
    
            };

        
        
        this._demandsplitService.GetDemandSplitViewResult(this.SearchView)
            .map((data: any) => data.json()).subscribe((data: any) => {


                if (data.length > 0) {
                    
                    this.rowDataName = data;
                    this.isvisibleerrodiv = false;
                    this.Isexport = true;
                    this.IserrorGrid = false;
                    this.IsdemandGrid = true;
                    this.productnamedisplay = true;
                    this.FilleerrorMessage = "";
                }

                 
            },
                err => console.log(err), // error
            )
    }


    ExportExcelDemand(event) {
        
        event.stopPropagation();
       // this._demandsplitService.exportAsExcelFile(this.rowDataName, "UnspecifiedDemandSplit");

        this.GenerateExcelName(this.rowDataName);
    }


    GenerateExcelName(rowData: any[]) {
        
        //this.ClearMessage();
        let param = [];
        for (var i = 0; i < rowData.length; i++) {
            param.push({
                ["PRODUCT_ID"]: rowData[i].product_ID,
                ["CHANNEL_ID"]: rowData[i].channel_ID,
                ["GEOGRAPHY_ID"]: rowData[i].geography_ID,
                ["SSC"]: rowData[i].ssc,
                ["WK0"]: rowData[i].wK0,

                ["WK1"]: rowData[i].wK1,
                ["WK2"]: rowData[i].wK2,
                ["WK3"]: rowData[i].wK3,
                ["WK4"]: rowData[i].wK4,
                ["WK5"]: rowData[i].wK5,
                ["WK6"]: rowData[i].wK6,
                ["WK7"]: rowData[i].wK7,
                ["WK8"]: rowData[i].wK8,
                ["WK9"]: rowData[i].wK9,
                ["WK10"]: rowData[i].wK10,
                ["WK11"]: rowData[i].wK11,
                ["WK12"]: rowData[i].wK12,
                ["WK13"]: rowData[i].wK13,
                ["WK14"]: rowData[i].wK14,
                ["WK15"]: rowData[i].wK15,
                ["WK16"]: rowData[i].wK16,
                ["WK17"]: rowData[i].wK17,
                ["WK18"]: rowData[i].wK18,
                ["WK19"]: rowData[i].wK19,

                ["WK20"]: rowData[i].wK20,
                ["WK21"]: rowData[i].wK21,
                ["WK22"]: rowData[i].wK22,
                ["WK23"]: rowData[i].wK23,
                ["WK24"]: rowData[i].wK24,
                ["WK25"]: rowData[i].wK25,
                ["WK26"]: rowData[i].wK26,

                ["REST_OF_HORIZON"]: rowData[i].rest_Of_Horizon,
                ["SYS_LAST_MODIFIED_BY"]: rowData[i].sys_last_Modified_By,
                ["SYS_LAST_MODIFIED_DATE"]: rowData[i].sys_last_Modified_Date
            })
        }
        this._demandsplitService.ExcelExport(param);
    }


    FileValidation($event) {
        //this._fgaChannelHierarchy = [];
        //this._uppChannelHierarchy = [];
       // this.ClearMessage();
        if ($event.target.files && $event.target.files[0]) {
            this.excelfile = $event.target.files[0];
            this.selectedfilename = this.excelfile.name;
            var fileNameWithOutExtenstion = this.selectedfilename.substring(0, this.selectedfilename.lastIndexOf('.')).toUpperCase();
            var fileExt1 = this.selectedfilename.substring(this.selectedfilename.lastIndexOf('.') + 1).toUpperCase();
            if (fileExt1 != 'XLSX') {
                var msg = "\"" + fileExt1 + "\" - Incorrect file extension!!";
                
            }
            else {
              //  this.DisableUploadButton = false;
            }
        }
        else {
           // this.DisableUploadButton = true;
        }
    }


    ImportDemandDetails() {
        var DemandSplitImportFile: any = this._DemandSplitExport;
        
        //this._demandsplitService.importDemandDetails(DemandSplitImportFile).subscribe(
        //    (data: any) => {
        //        debugger
        //        var returnCode = data._body.split("~")[0];
        //        var returnMsg = data._body.split("~")[1];

        //        this.IserrorGrid = true;
        //        this.errRowData = data._body;
        //        //if (returnMsg == "SUCCESS") {
        //        //    this.rowDataError = [];
        //        //    this.DisableUploadButton = true;
        //        //    this.DisplayMessage("Records Successfully Imported.", true);
        //        //}
        //        //else {
        //        //    this.loadErrorGrid('P_IMPORT_CHANNEL');
        //        //    this.DisplayMessage(returnMsg, false);
        //        //}
        //    });



        this._demandsplitService.importDemandDetails(DemandSplitImportFile)
            .map((data: any) => data.json()).subscribe((data: any) => {
                

                if (data.successmsg == undefined) {
                    

                    this.IserrorGrid = true;
                    this.IsdemandGrid = false;
                    this.errRowData = data.demandSplitErrorgrids;
                    this.TotalRows = data.totalrow;
                    this.Recordsfailed = data.totalfailed;
                    this.RecordsCorrect = data.correctRecords;
                    this.FilleerrorMessage = data.successmsg;
                    this.Totalsuberror = data.totalsuberror;
                    this.ErrorMessage = data.errorMessage;
                    this.Isexport = false;
                    this.productnamedisplay = false;
                    this.isvisibleerrodiv = true;
                    this.errormessagedisplay = true;
                    this.errormessagedisplayscusses = false;
                    this.fileuplaodName = "";
                }

                else {

                    this.productnamedisplay = true;
                    this.Isexport = true;
                    this.IserrorGrid = false;
                    this.IsdemandGrid = true;
                    this.errRowData = data.demandSplitErrorgrids;
                    this.TotalRows = data.totalrow;
                    this.Recordsfailed = data.totalfailed;
                    this.RecordsCorrect = data.correctRecords;
                    this.Sucessfullmessage = data.successmsg;
                    this.isvisibleerrodiv = true;
                    this.errormessagedisplay = false;
                    this.errormessagedisplayscusses = true;
                    this.fileuplaodName = "";


                    this.Selectedchanelvalue[0] = { id: "ALL", itemName: "ALL" };
                    this.SelectedProductvalue[0] = { id: "ALL", itemName: "ALL" };
                    this.Selectedgeographyvalue[0] = { id: "ALL", itemName: "ALL" };

                    this.getProduct();

                    this.getChennal();

                    this.getGeography();

                    this.Filldatagrid();

                }


            },
                err => console.log(err), // error
            )

    }

    Filldatagrid() {
        

        this.SearchView

        this.ProductName = this.SelectedProductvalue[0].id;
        this.ChannelName = this.Selectedchanelvalue[0].id;
        this.GeoName = this.Selectedgeographyvalue[0].id;



        this.SearchView =
            {
                product: this.SelectedProductvalue[0] == undefined ? "" : this.SelectedProductvalue[0].id,
                chennal: this.Selectedchanelvalue[0] == undefined ? "" : this.Selectedchanelvalue[0].id,
                geography: this.Selectedgeographyvalue[0] == undefined ? "" : this.Selectedgeographyvalue[0].id,

            };

        

        this._demandsplitService.GetDemandSplitViewResult(this.SearchView)
            .map((data: any) => data.json()).subscribe((data: any) => {


                if (data.length > 0) {
                    
                    this.rowDataName = data;
                    this.isvisibleerrodiv = true;
                    this.Isexport = true;
                    this.IserrorGrid = false;
                    this.IsdemandGrid = true;
                    this.productnamedisplay = true;
                     
                }


            },
                err => console.log(err), // error
            )
    }    

    UploadFile() {
        debugger

        if (this.fileuplaodName != undefined && this.fileuplaodName != "" ) {
            var fgaRowData: any[] = [];
            var uppRowData: any[] = [];
            var rowDataError: any[] = [];
            var reader: FileReader = new FileReader();
            reader.onloadend = (e) => {
                var data = reader.result;
                var workbook = XLSX.read(data, { type: 'binary' });
                let excelHeader = [];
                var first_sheet_name = workbook.SheetNames[0];
                var headers = XLSX.utils.sheet_to_json(workbook.Sheets[first_sheet_name], { header: 1 })[0];
                var dataObjects: any = XLSX.utils.sheet_to_json(workbook.Sheets[first_sheet_name]);
                Object.values(headers).forEach(value => {
                    excelHeader.push(value)//value    
                });

                if (this.isValidExcelHeader(excelHeader)) {
                    if (dataObjects.length > 0) {
                        //Push excel data into object
                        this.getDemandlDataFromExcel(dataObjects);
                        //call service to update excel data 
                        this.ImportDemandDetails();
                    }
                    else {

                        this.FilleerrorMessage = "Empty File has been uploaded.";
                    }
                }

            }
            this.FileName.nativeElement.value = "";
            this.FilleerrorMessage = "";
            reader.readAsBinaryString(this.excelfile);
        }
        else {
            this.FilleerrorMessage = "Select an excel file to upload";
        }
    }

    getDemandlDataFromExcel(dataObjects) {
        
        this._DemandSplitExport = [];
        for (var i = 0; i < dataObjects.length; i++) {
            this._DemandSplitExport.push({
                Product_ID: dataObjects[i]["PRODUCT_ID"],
                Channel_ID: dataObjects[i]["CHANNEL_ID"],
                Geography_ID: dataObjects[i]["GEOGRAPHY_ID"],
                SSC: dataObjects[i]["SSC"],

                WK0: dataObjects[i]["WK0"],
                WK1: dataObjects[i]["WK1"],
                WK2: dataObjects[i]["WK2"],
                WK3: dataObjects[i]["WK3"],
                WK4: dataObjects[i]["WK4"],
                WK5: dataObjects[i]["WK5"],
                WK6: dataObjects[i]["WK6"],
                WK7: dataObjects[i]["WK7"],
                WK8: dataObjects[i]["WK8"],

                WK9: dataObjects[i]["WK9"],

                WK10: dataObjects[i]["WK10"],
                WK11: dataObjects[i]["WK11"],
                WK12: dataObjects[i]["WK12"],
                WK13: dataObjects[i]["WK13"],
                WK14: dataObjects[i]["WK14"],
                WK15: dataObjects[i]["WK15"],
                WK16: dataObjects[i]["WK16"],

                WK17: dataObjects[i]["WK17"],
                WK18: dataObjects[i]["WK18"],
                WK19: dataObjects[i]["WK19"],
                WK20: dataObjects[i]["WK20"],
                WK21: dataObjects[i]["WK21"],
                WK22: dataObjects[i]["WK22"],
                WK23: dataObjects[i]["WK23"],

                WK24: dataObjects[i]["WK24"],
                WK25: dataObjects[i]["WK25"],
                WK26: dataObjects[i]["WK26"],


                Rest_Of_Horizon: dataObjects[i]["REST_OF_HORIZON"],
               Sys_last_Modified_By: dataObjects[i]["SYS_LAST_MODIFIED_BY"],
                Sys_last_Modified_Date: dataObjects[i]["SYS_LAST_MODIFIED_DATE"],

              
                
            });
        }

    }

    isValidExcelHeader(excelHeader) {
        var isValidHeader = true;
        var expectedHeader = {};

        expectedHeader = this.DemandSplitExcelHeader;
        if (JSON.stringify(excelHeader).toUpperCase() != JSON.stringify(expectedHeader).toUpperCase()) {
            this.FilleerrorMessage = "Invalid File has been uploaded.";
            isValidHeader = false;
        }
        return isValidHeader;
    }

}

export interface DemandSplitExport {
    Product_ID: string;
    Channel_ID: string;
    Geography_ID: string;
    SSC: string;
    WK0: number;
    WK1: number;
    WK2: number;
    WK3: number;
    WK4: number;
    WK5: number;
    WK6: number;
    WK7: number;
    WK8: number;
    WK9: number;
    WK10: number;
    WK11: number;
    WK12: number;
    WK13: number;
    WK14: number;
    WK15: number;
    WK16: number;
    WK17: number;
    WK18: number;
    WK19: number;
    WK20: number;
    WK21: number;
    WK22: number;
    WK23: number;
    WK24: number;
    WK25: number;
    WK26: number;

    Rest_Of_Horizon: string;
   Sys_last_Modified_By: string;
    Sys_last_Modified_Date: string;
}


//To Enable bootstrap multi select inside grid for checkbox
function loadJavaScript() {
    var url = '/dist/grid-multi-select.js';
    var s = document.createElement('script');
    s.setAttribute('src', url);
    document.head.appendChild(s);
}
