﻿import { Component, OnInit, ViewEncapsulation, ViewChild, Input, ElementRef } from '@angular/core';
import { ColumnApi, GridApi, GridOptions } from "ag-grid/main";
import * as XLSX from 'xlsx';
import { dashCaseToCamelCase } from '@angular/compiler/src/util';
import { debugOutputAstAsTypeScript } from '@angular/compiler';
import { DebugContext } from '@angular/core/src/view';
import { debounce } from 'rxjs/operator/debounce';
import { UserRoles } from '../../Model/user-roles';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { UserMaintenanceService } from '../../services/user-maintenance.service';
import { UserDetails } from '../../Model/user-details';
import { CheckboxCellCommonComponent } from '../checkbox-renderer.component';
import { forEach } from '@angular/router/src/utils/collection';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UtilitiesService } from '../../services/utilities.service';

@Component({
    selector: 'app-maintenance',
    templateUrl: './maintenance.component.html',
    styleUrls: ['./maintenance.component.css']
})
/** maintenance component*/
export class MaintenanceComponent {
    /** maintenance ctor */

    @ViewChild('fakeClick') fakeClick: ElementRef;
    private gridOptions: GridOptions;


    selected_region = [];
    singleSelectDropdownSettings = {};
    region = [];

    TenantList = [];
    selectedTenant = [];
    //singleSelectDropdownSettings = {};

    userGroups = [];
    selectedUserGroups = [];

    roles = [];
    selectedRoles = [];
    multiSelectDropdownSettings = {};


    LOBList = [];
    selectedLobs = [];

    public hideShowAlertMsg = false;
    successMsg: string = "";
    showEmailId = false;
    emailId = "";
    domain = "";
    userName = "";
    userDetailsList = [];
    userHavingWriteAccess: string[];
    userTenantTypeAccessList: string[];
    listOfRoleHavingAccess: string[];
    userRoles: UserRoles[];
    loggedInUserName: string;
    loggedInUserDomain: string;
    isUserHavingUserMaintenance: boolean = false;
    isUserHavingWriteAccess: boolean = false;
    isActiveScreen: boolean = true;
    screenId = "2005";
    colorCode: string;
    private rowData;
    filterDetails: FilterDetails;
    userDetails: UserDetails;
    OrignalRowData = [];
    rolesListCommaSeprated: string = "";
    lobListCommaSeparated: string = "";
    rolesGrid = [];


    ActiveInactiveList = [];
    userNameList = [];
    selectedRegionFilter = [];
    selectedUserNameFilter = [];
    selectedUserGroupsFilter = [];
    selectedActiveInactiveFilter = [];
    selectedTenantFilter = [];

    private getRowNodeId;
    private components;
    uppTenant: string = "UPP";
    private gridApi;
    private gridColumnApi;
    blankUserIds: string = "";
    gridBtnsave: boolean = false;
    private Isexport = true;
    IsexportBtnDisable = true;




    constructor(private _router: Router, private _userMaintenanceService: UserMaintenanceService, private _sessionSt: SessionStorageService, private _utilitiesService: UtilitiesService) {

        

        var rolesHavingWriteAccess = this._sessionSt.retrieve('screenAuthorization') != null ? this._sessionSt.retrieve('screenAuthorization') : [];
        this.userHavingWriteAccess = rolesHavingWriteAccess[rolesHavingWriteAccess.findIndex(x => x.functionId == this.screenId)]["roleId"].split(',');
        this.userTenantTypeAccessList = this._sessionSt.retrieve('userTenantTypeAccessList') != null ? this._sessionSt.retrieve('userTenantTypeAccessList') : [];

        var sessionRolesCanAccessScreen = this._sessionSt.retrieve('RolesCanAccessScreen') != null ? this._sessionSt.retrieve('RolesCanAccessScreen') : [];
        this.listOfRoleHavingAccess = sessionRolesCanAccessScreen.find(x => x.screenId == this.screenId).roles.split(',');

        if (this._sessionSt.retrieve('userRoles') === null) {
            //to do
        }
        else {
            this.userRoles = this._sessionSt.retrieve('userRoles');
            this.loggedInUserDomain = this._sessionSt.retrieve('domainName');
            this.loggedInUserName = this._sessionSt.retrieve('userID');

            for (var i = 0; i < this.userRoles.length; i++) {
                if (this.listOfRoleHavingAccess.includes(this.userRoles[i].roleId.toString())) {
                    this.isUserHavingUserMaintenance = true;
                }
                if (this.userHavingWriteAccess.includes(this.userRoles[i].roleId.toString())) {
                    this.isUserHavingWriteAccess = true;
                }
            }

            if (!this.isUserHavingUserMaintenance) {
                this.isActiveScreen = false;
                this._router.navigateByUrl('/errorMessage');
                return;
            }

        }







        this.gridOptions = <GridOptions>{
            context: {
                componentParent: this
            },
        };
        this.gridOptions.columnDefs = [
            {
                headerName: "",
                field: "userId",
                width: 35,
                pinned: "left",
                suppressSizeToFit: true,
                suppressFilter: true,
                headerCheckboxSelectionFilteredOnly: true,
                editable: false,
                checkboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                },
                headerCheckboxSelection: function (params) {
                    return params.columnApi.getRowGroupColumns().length === 0;
                }
            },
            //  { headerName: "RECORD ID", field: "chanId", tooltipField: "chanId", suppressSizeToFit: false, editable: false },
            {
                headerName: "USER ID",
                field: "userId",
                tooltipField: "userId",
                suppressSizeToFit: false,
                editable: false,
                hide: true
                //width:50
            },
            {
                headerName: "USER NAME",
                field: "userName",
                tooltipField: "userName",
                suppressSizeToFit: false,
                editable: false,
                //width:180
            },
            {
                headerName: "USER DOMAIN",
                field: "userDomain",
                tooltipField: "userDomain",
                suppressSizeToFit: false,
                editable: false,
                //width:120
            },
            {
                headerName: "REGION",
                field: "region",
                tooltipField: "region",
                suppressSizeToFit: false,
                hide: true,
                editable: false
            },
            {
                headerName: "USER GROUP",
                field: "userGroup",
                tooltipField: "userGroup",
                suppressSizeToFit: false,
                editable: false,
                hide: true

            },
            {
                headerName: "USER EMAIL",
                field: "userEmail",
                tooltipField: "userEmail",
                suppressSizeToFit: false,
                editable: false
            },
            {
                headerName: "USER ROLE",
                field: "userRole",
                tooltipField: "userRole",
                suppressSizeToFit: false,
                //  editable: this.isUserHavingWriteAccess,
                cellClass: 'multi-select-grid',
                cellEditor: "roles",
                onCellClicked: this.oncellEditingStarted
            },
            {
                headerName: "LOB",
                field: "lobs",
                tooltipField: "lobs",
                // editable: this.isUserHavingWriteAccess,
                suppressSizeToFit: false,
                cellClass: 'multi-select-grid',
                cellEditor: "LOBList",
                hide: true,
                editable: function (Rownode) {
                    // if (this.isUserHavingWriteAccess) {
                    if (Rownode.node.data.userRole.includes("UPP_PRODUCT_LOB_ADMIN"))
                        return true
                    else
                        return false
                    // }
                }
            },
            {
                headerName: "ACTIVE",
                field: "isActive",
                tooltipField: "isActive",
                suppressSizeToFit: false,
                editable: false,
                cellClass: "checkBox",
                //  onCellClicked: this.oncellEditingStarted,
                //   cellRenderer: "checkboxRenderer",
                hide: true,
                //width:50
            },
            {
                headerName: "UPDATED BY",
                field: "updatedBy",
                tooltipField: "updatedBy",
                suppressSizeToFit: false,
                editable: false,
                //width:80
            },
            {
                headerName: "UPDATED DATE",
                field: "updatedDate",
                tooltipField: "updatedDate",
                suppressSizeToFit: true,
                editable: false,
                //width:100
            },
        ];

        this.gridOptions.rowSelection = "multiple";
        this.gridOptions.paginationPageSize = 14;
        this.gridOptions.defaultColDef = {
            editable: true,
            enableValue: true
        };
        // this.gridOptions.rowData = [];


        this.gridOptions.rowData = [];
        this.userDetailsList = [];
        //  this.context = { componentParent: this };
        //this.frameworkComponents = {
        //    checkboxRenderer: CheckboxCellCommonComponent,
        //};
        this.components = {
            roles: getRoles(),
            //   LOBList: getLobs()
        }
        this.getRowNodeId = function (data) {
            return data.userId;
        };



    }




    oncellEditingStarted(event: any) {

        event.node.setSelected(true);
        console.log('checkbox clicked!');
        event.context.componentParent.gridBtnClear = true;

        let nodeData = event.node.data;
        let selectedRow =
        {
            itemId: nodeData.itemId,
            commodityCode: nodeData.commodityCode,
            itemDescription: nodeData.itemDescription,
            multiTier: nodeData.multiTier,
            updatedBy: nodeData.updatedBy,
            updatedDate: nodeData.updatedDate
        };
        let index = event.context.componentParent.changedRows.findIndex(x => x.itemId == selectedRow.itemId);
        if (index == -1) {
            event.context.componentParent.changedRows.push(selectedRow);

        } else {
            event.context.componentParent.changedRows.splice(index, 1);
        }
    }



    DisplayMessage(msg, isSuccess) {
        this.hideShowAlertMsg = true;
        this.successMsg = msg;
        this.colorCode = isSuccess == true ? "success" : "danger";
    }






    ngOnInit() {

        this.multiSelectDropdownSettings = {
            singleSelection: false,
            text: "Select",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results'
            classes: "myclass custom-class",
            //badgeShowLimit: 100,
            badgeShowLimit: 1
        };
        this.singleSelectDropdownSettings = {
            singleSelection: true,
            text: "Select...",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false, //  remove the  filterSelectAllText: 'Select all filtered results'
            classes: "myclass custom-class singleSelect"
        };



        this.GetTenantList();

    }


    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        //this.autoSizeAll();
        this.gridOptions.api.sizeColumnsToFit();
    }


    AddUsers() {
        this.ClearMessage();
        this.GetAddUserData();
        this._userMaintenanceService.AddUsers(this.userDetails)
            .subscribe(
                (data: any) => {
                    var returnCode = data._body.split("~")[0];
                    var returnMsg = data._body.split("~")[1];
                    if (returnMsg == "SUCCESS") {
                        this.DisplayMessage("Records successfully Updated.", true);
                        this.GetUserNameList();
                    }
                    else {
                        this.DisplayMessage(returnMsg, false);
                    }
                },
                err => console.log(err), // error
            );
    }




    GetAddUserData() {
        var roles = this.GetCommaSeparatedDD(this.selectedRoles);
        var lobs = "";//this.GetCommaSeparatedLob(this.selectedLobs);
        this.userDetails = {
            userId: "",
            userName: this.userName.trim(),
            userDomain: this.domain,
            userType: this.selectedTenant[0].id,
            lobs: lobs,
            region: "region", //this.selected_region[0].id,
            userEmail: this.emailId,
            userRole: roles,
            userGroup: "1",// this.selectedUserGroups[0].id,
            isActive: true,
            updatedBy: "",
            updatedDate: "",
        }
    }



    GetCommaSeparatedDD(data) {
        var s = "";
        for (var i = 0; i < data.length; i++) {
            if (i != 0) {
                s += ",";
            }
            s += data[i].id;
        }

        return s;
    }

    GetUserNameList() {
        this.userNameList = [];
        var selectedRegionFilterData = this.selectedRegionFilter.length > 0 ? this.selectedRegionFilter : [];
        this._userMaintenanceService.GetUserNameList(this.selectedTenantFilter[0].id, selectedRegionFilterData)
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    for (var i = 0; i < data.length; i++) {
                        this.userNameList.push({
                            id: data[i].id,
                            itemName: data[i].itemName
                        })
                    }
                    this._utilitiesService.ReplaceSplCharFromStringList(this.userNameList, "itemName");
                },
                err => console.log(err),
            );
    }


    updateGridRow(event: any) {

        this.ClearMessage();
        this.gridApi.stopEditing();
        let rowsSelection = JSON.parse(JSON.stringify(this.gridOptions.api.getSelectedRows()));
        rowsSelection = this.ConvertRolesDescToId(rowsSelection);
        rowsSelection = this.ConvertLobsDescToId(rowsSelection);
        if (this.selectedTenantFilter[0].id === this.uppTenant) {
            rowsSelection = this.ProductLobAdminClearanceBeforeUpdate(rowsSelection)
        }

        if (rowsSelection.length > 0) {



            if (this.blankUserIds == "") {
                if (this.Validate(rowsSelection)) {

                    this._userMaintenanceService.UpdateUsers(rowsSelection)
                        .subscribe(
                            (data: any) => {

                                var returnCode = data._body.split('~')[0];
                                var returnMsg = data._body.split('~')[1];
                                this.userDetailsList = [];
                                this.hideShowAlertMsg = true;
                                if (returnMsg == "SUCCESS") {
                                    this.DisplayMessage("Records successfully Updated.", true);
                                    this.gridOptions.api.deselectAllFiltered();
                                     this.LoadUserGrid();
                                }
                                else {
                                    this.DisplayMessage(returnMsg, false);
                                }
                            },
                            err => console.log(err), // error
                        );
                }
            }
            else {
                this.DisplayMessage("Error : Atleast one role must be selected for User Id(s) - " + this.blankUserIds, false);
            }
        }
        else {

            this.DisplayMessage("Error : Atleast one role must be selected for User Id(s) - " + this.blankUserIds, false);
        }
    }



    ProductLobAdminClearanceBeforeUpdate(rowsSelection) {
        var row;
        this.blankUserIds = "";
        for (var i = 0; i < rowsSelection.length; i++) {

            //if (!rowsSelection[i].userRole.includes(this.ProductLobAdminId)) {
            //    rowsSelection[i].lobs = "";
            //}

        }
        return rowsSelection;
    }


    Validate(rowsSelection) {

        let globalAdminUserIds = "";
        var isVaild = true;
        var globalAdminId = 3001;
        if (this.userRoles.findIndex(x => x.roleId == globalAdminId) == -1) {
            for (var i = 0; i < rowsSelection.length; i++) {
                if (this.OrignalRowData.find(x => x.userId == rowsSelection[0].userId).userRole.includes(globalAdminId)) {
                    if (globalAdminUserIds != "") {
                        globalAdminUserIds += ",";
                    }
                    globalAdminUserIds += rowsSelection[i].userId;
                }
            }

            if (globalAdminUserIds != "") {
                this.DisplayMessage("You are not authorised to update global Admin's data : " + globalAdminUserIds, false);
                isVaild = false;
            }
        }
        return isVaild;
    }


    ConvertRolesDescToId(rowsSelection) {
        var row;
        this.blankUserIds = "";
        for (var i = 0; i < rowsSelection.length; i++) {
            if (rowsSelection[i].userRole != "") {
                row = rowsSelection[i].userRole.split(',');
                let id = "";
                for (var j = 0; j < row.length; j++) {
                    if (j != 0) {
                        id += ",";
                    }
                    id += this.rolesGrid.find(x => x.itemName == row[j]).id
                }

                rowsSelection[i].userRole = id;
            }
            else {
                if (this.blankUserIds != "") {
                    this.blankUserIds += ", ";
                }
                this.blankUserIds += rowsSelection[i].userId;
            }

        }
        return rowsSelection;
    }

    ConvertLobsDescToId(rowsSelection) {
        var row;
        for (var i = 0; i < rowsSelection.length; i++) {
            row = rowsSelection[i].lobs.split(',');
            let id = "";
            for (var j = 0; j < row.length; j++) {
                if (j != 0) {
                    id += ",";
                }
                if (this.LOBList.find(x => x.itemName == row[j]) != undefined) {
                    id += this.LOBList.find(x => x.itemName == row[j]).id
                }
            }

            rowsSelection[i].lobs = id;
        }
        return rowsSelection;
    }

    GetTenantList() {
        this.TenantList = [];
        this._userMaintenanceService.GetTenantList()
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    for (var i = 0; i < data.length; i++) {
                        this.TenantList.push({
                            id: data[i].id,
                            itemName: data[i].id
                        })

                    }

                    this.SelectTenantByDefault();
                },
                err => console.log(err),
            );
    }

    onItemSelectTenantFilter(event: any) {
        this.closeDropdown();
        this.OnSelcetionOfTenantFilter()
        this.gridOptions.api.sizeColumnsToFit();
    }

    closeDropdown() {
        let el: HTMLElement = this.fakeClick.nativeElement as HTMLElement;
        el.click();
    }

    SelectTenantByDefault() {
        if (this.TenantList.length === 1) {
            this.selectedTenant = [{
                id: this.TenantList[0].id,
                itemName: this.TenantList[0].itemName
            }];
            this.GetRoleList();

            this.selectedTenantFilter = [{
                id: this.TenantList[0].id,
                itemName: this.TenantList[0].itemName
            }];

            this.OnSelcetionOfTenantFilter();
        }
    }

    OnSelcetionOfTenantFilter() {
        this.rowData = [];
        this.GetUserNameList();
        this.GetRoleListGrid();
        this.selectedUserNameFilter = [];

        if (this.gridOptions.columnApi != undefined) {
            if (this.selectedTenantFilter[0].id === this.uppTenant) {
                this.gridOptions.columnApi.setColumnVisible('lobs', true);
            }
            else {
                this.gridOptions.columnApi.setColumnVisible('lobs', false);
            }
        }
    }


    GetRoleListGrid() {

        this.rolesGrid = [];
        this._userMaintenanceService.GetRoleList(this.selectedTenantFilter[0].id)
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    for (var i = 0; i < data.length; i++) {
                        //      this.IsDisableRolesDD = true;
                        this.rolesGrid.push({
                            id: data[i].id,
                            itemName: data[i].itemName
                        })
                        this.rolesListCommaSeprated += data[i].id;
                    }
                },
                err => console.log(err),
            );


    }

    onItemSelectTenant(event: any) {
        //   this.closeDropdown();
        this.GetRoleList();
    }


    GetRoleList() {
        this.roles = [];
        this.selectedRoles = [];
        this.selectedLobs = [];
        //   this.showLobList = false;
        this._userMaintenanceService.GetRoleList(this.selectedTenant[0].id)
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    for (var i = 0; i < data.length; i++) {
                        //      this.IsDisableRolesDD = true;
                        this.roles.push({
                            id: data[i].id,
                            itemName: data[i].itemName
                        })
                        //this.rolesListCommaSeprated += data[i].id;
                    }
                },
                err => console.log(err),
            );
    }


    ValidateUserName() {

        this._userMaintenanceService.ValidateUserName(this.userName.trim()).subscribe(
            (data: any) => {
                var returnMsg = data._body;
                if (returnMsg == "") {
                    this.DisplayMessage(this.userName.trim() + " - User Name Not Find.", false);
                    this.showEmailId = false;
                    this.emailId = "";
                    this.domain = "";
                }
                else {
                    this.emailId = returnMsg.split("+++")[0];
                    this.domain = returnMsg.split("+++")[1];
                    this.showEmailId = true;
                    this.DisplayMessage(this.userName.trim() + " - is Valid User.", true);
                }



            });
    }

    onSelectionChanged(event: any) {
        let rowsSelection = this.gridOptions.api.getSelectedRows();
        if (rowsSelection.length > 0) {
            this.gridBtnsave = true;
        }
        else {
            this.gridBtnsave = false;
        }
    }


    LoadUserGrid() {
        // this.ClearMessage();
        // this.rowData = [];
        this.gridBtnsave = false;

        this.GetFilterData();
        this._userMaintenanceService.GetUserDetails(this.filterDetails)
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {

                    this.OrignalRowData = JSON.parse(JSON.stringify(data));
                    data = this.ConvertRolesIDtoDesc(data);
                    // data = this.ConvertLobsIDtoDesc(data);
                    data = this.replaceCharacters(data);
                    this.rowData = [];
                    this.rowData = data;
                   // this.Isexport = true;
                    this.IsexportBtnDisable = false;
                },
                err => console.log(err), // error
            );
    }


    GetFilterData() {
        this.filterDetails = {
            region: [],
            userName: [],
            userGroup: [],
            userType: this.selectedTenantFilter[0].id,
            isActive: "Y"
        };
    }


    replaceCharacters(data) {
        this._utilitiesService.ReplaceSplCharFromStringList(data, "updatedBy");
        this._utilitiesService.ReplaceSplCharFromStringList(data, "userName");
        this._utilitiesService.ReplaceSplCharFromStringList(data, "userEmail");
        return data;
    }
    ConvertRolesIDtoDesc(data) {

        for (var i = 0; i < data.length; i++) {
            var splittedDataRoles = data[i].userRole.split(',');
            var splittedDataLobs = data[i].lobs.split(',');
            var roleDescriptions = "";
            var lobDescriptions = "";

            //Roles
            for (var j = 0; j < splittedDataRoles.length; j++) {
                if (this.rolesListCommaSeprated.includes(splittedDataRoles[j])) {
                    if (splittedDataRoles[j] != "") {
                        if (j != 0) {
                            roleDescriptions += ",";
                        }

                        if (this.rolesGrid.find(x => x.id == splittedDataRoles[j]) != undefined) {
                            roleDescriptions += this.rolesGrid.find(x => x.id == splittedDataRoles[j]).itemName;
                        }
                    }
                }
            }
            data[i].userRole = roleDescriptions;
        }
        return data;
    }

    ConvertLobsIDtoDesc(data) {
        for (var i = 0; i < data.length; i++) {
            var splittedDataLobs = data[i].lobs.split(',');
            var lobDescriptions = "";

            //Roles
            for (var j = 0; j < splittedDataLobs.length; j++) {
                if (this.lobListCommaSeparated.includes(splittedDataLobs[j])) {
                    if (splittedDataLobs[j] != "") {
                        if (j != 0) {
                            lobDescriptions += ",";
                        }
                        lobDescriptions += this.LOBList.find(x => x.id == splittedDataLobs[j]).itemName;
                    }
                }
            }
            data[i].lobs = lobDescriptions;
        }
        return data;
    }


    ClearMessage() {
        this.hideShowAlertMsg = false;
        this.successMsg = "";
    }



    ClearAddUserData() {
        this.userName = "";
        this.emailId = "";
        this.domain = "";
        this.showEmailId = false;
        this.selected_region = [];
        this.selectedUserGroups = [];
        this.selectedRoles = [];
        this.selectedTenant = [];

        this.selectedLobs = [];

        this.ClearMessage();
        this.SelectTenantByDefault();
     //   this.rowData = [];
     //   this.IsexportBtnDisable = true;
    }



    ClearSearchCriteria() {
        this.ClearMessage();
        this.selectedRegionFilter = [];
        this.selectedUserNameFilter = [];
        this.selectedUserGroupsFilter = [];
        this.selectedActiveInactiveFilter = [];
        this.selectedTenantFilter = [];
        this.userNameList = [];
        this.rowData = [];
        this.SelectTenantByDefault();
        this.IsexportBtnDisable = true;
        this.rowData = [];
        
    }



    ExportExcelDemand(event) {

        event.stopPropagation();
        // this._demandsplitService.exportAsExcelFile(this.rowDataName, "UnspecifiedDemandSplit");

        this.GenerateExcelName(this.rowData);
    }


    GenerateExcelName(rowData: any[]) {

        //this.ClearMessage();
        let param = [];
        for (var i = 0; i < rowData.length; i++) {
            param.push({
                ["USER NAME"]: rowData[i].userName,
                ["USER DOMAIN"]: rowData[i].userDomain,
            //    ["REGION"]: rowData[i].region,
         //       ["USER GROUP"]: rowData[i].userGroup,
                ["USER EMAIL"]: rowData[i].userEmail,
                ["USER ROLE"]: rowData[i].userRole,
                ["UPDATED BY"]: rowData[i].updatedBy,
                ["UPDATED DATE"]: rowData[i].updatedDate
                
            })
        }
        this._userMaintenanceService.ExcelExport(param);
    }


}




function getRoles() {


    function MultiSelectDropDown() { }

    MultiSelectDropDown.prototype.getGui = function () {
        return this.eGui;
    };
    MultiSelectDropDown.prototype.getValue = function () {
        var arr = [];
        if (this.eGui.firstChild != null) {
            if (this.eGui.firstChild.selectedOptions.length > 0) {
                for (var i = 0; i < this.eGui.firstChild.selectedOptions.length; i++) {
                    var selectedItemValueGridDD = this.eGui.firstChild.selectedOptions[i].value;
                    arr.push(selectedItemValueGridDD);
                    var commaValues = arr.join(",");
                }
                this.value = commaValues;
                return this.value;
            }
            else {
                return this.value = "";
            }
        }
        else {
            return this.value = "";
        }
    };
    MultiSelectDropDown.prototype.isPopup = function () {
        return true;
    };
    MultiSelectDropDown.prototype.search = function () {
        console.log(this);
    }
    MultiSelectDropDown.prototype.afterGuiAttached = function () {
        var changedWidth = this.myDDWid + "px";
        this.eGui.parentElement.style.width = changedWidth;
    };
    MultiSelectDropDown.prototype.init = function (params) {

        var multiDD: any = [];
        //var multiDD = params.context.componentParent.demandGeoFunction(params);
        multiDD = params.context.componentParent.rolesGrid;
        var myarr = params.value.split(",");

        this.myDDWid = params.column.actualWidth;

        var tempElement = document.createElement("div");
        tempElement.setAttribute('id', 'multiSel');
        tempElement.setAttribute('class', 'multiselect');

        var tempElementDiv = document.createElement("select");
        tempElementDiv.setAttribute('class', 'multiselect-ui custom-select form-control');
        tempElementDiv.setAttribute('id', 'multiselect-ui');
        tempElementDiv.setAttribute('multiple', 'multiple');

        for (var i = 0; i < multiDD.length; i++) {
            var option = document.createElement("option");
            option.setAttribute("value", multiDD[i].itemName);
            // option.setAttribute("itemName", multiDD[i].itemName);
            option.text = multiDD[i].itemName;
            for (var j = 0; j < myarr.length; j++) {
                if (multiDD[i].itemName === myarr[j]) {
                    option.setAttribute("selected", "selected");
                }
            }
            tempElementDiv.appendChild(option);
            tempElement.appendChild(tempElementDiv);
        }
        loadJavaScript();
        this.eGui = tempElement;
    };
    return MultiSelectDropDown;
}

//To Enable bootstrap multi select inside grid for checkbox
function loadJavaScript() {
    var url = '/dist/grid-multi-select.js';
    var s = document.createElement('script');
    s.setAttribute('src', url);
    document.head.appendChild(s);
}

export class FilterDetails {
    region: Dropdown[];
    userName: Dropdown[];
    userGroup: Dropdown[];
    userType: string;
    isActive: string;
}

export class Dropdown {
    id: string;
    itemName: string;
}