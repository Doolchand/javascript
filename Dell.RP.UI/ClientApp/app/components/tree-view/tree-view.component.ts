﻿import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { SessionStorageService } from 'ngx-webstorage';
import * as XLSX from 'xlsx';
import { UserRoles } from '../../Model/user-roles';
import { FulfillmentSiteAssignmentService } from '../../services/fulfillment-siteassignment.service';
import { UtilitiesService } from '../../services/utilities.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetErrorGridService } from '../../services/get-error-grid.service';

import { TreeModel, TreeNode } from 'angular-tree-component';
import { Binary } from '@angular/compiler';
//import { treeViewData, children, node } from '../../Model/tree-view-data'
import { treeViewData } from '../../Model/treeview/treeViewData';
import { node } from '../../Model/treeview/node';
import { children } from '../../Model/treeview/children';


import { FulfillmentSiteAssignmentComponent }from '../../components/fulfillment-siteassignment/fulfillment-siteassignment.component'
import { SourcingRuleService } from '../../services/sourcing-rule.service';
 
declare var jQuery: any;

@Component({
    selector: 'app-tree-view',
    templateUrl: './tree-view.component.html',
    styleUrls: ['./tree-view.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class TreeViewComponent implements OnInit {

    
    @Input() treeViewDetail: treeViewData; 
    @Input() selectedGeogrphyTreeData: string;
    //@Output() valueUpdate = new EventEmitter();
    @Output() selectedGeogrphyTreeDataChanged = new EventEmitter<string>();

    @Input() selectedProductTreeData: string;
    @Output() selectedProductTreeDataChanged = new EventEmitter<string>();

    @Input() selectedChennelTreeData: string;
    @Output() selectedChennelTreeDataChanged = new EventEmitter<string>();
    errMsg: string;
    filterString: string;

    @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;
 

    treeData: node[];
    popupHeader: string;

    
    //nodes: node[];
    treeViewNodesFilterdData: node[];
    options: any;

    nodeData: node[];

    
    @ViewChild('fakeClick') fakeClick: ElementRef;
   // @ViewChild(FulfillmentSiteAssignmentComponent) fulfillmentChild: FulfillmentSiteAssignmentComponent;


    //Edit section - start
    @Input() selectedGeogrphyTreeData_edit: string;
    @Output() selectedGeogrphyTreeDataChanged_edit = new EventEmitter<string>();

    @Input() selectedProductTreeData_edit: string;
    @Output() selectedProductTreeDataChanged_edit = new EventEmitter<string>();

    @Input() selectedChennelTreeData_edit: string;
    @Output() selectedChennelTreeDataChanged_edit = new EventEmitter<string>();
    //Edit section - start 

    //GMP Section STart
    @Input() selectedGMPChennelGeoTreeData: string;
    @Output() selectedGMPChennelGeoTreeDataChanged = new EventEmitter<string>();

    @Input() selectedGMPProductTreeData: string;
    @Output() selectedGMPProductTreeDataChanged = new EventEmitter<string>();

    @Input() selectedGMPProductCreateTreeData: string;
    @Output() selectedGMPProductCreateTreeDataChanged = new EventEmitter<string>();
   


    @Input() selectedRoutingCreateProductTreeData: string;
    @Output() selectedRoutingCreateProductTreeDataChanged = new EventEmitter<string>();
    @Output() selectedRoutingCopyProductTreeDataChanged = new EventEmitter<string>();
    @Output() selectedProductTreeCopyDataChanged = new EventEmitter<string>();

      //GMP Section End

    constructor(private _fulfillmentSiteAssignmentServiceService: FulfillmentSiteAssignmentService,private  _sourcingRuleService:SourcingRuleService ) {
        
        //if (this.treeViewDetail != undefined) {
        //    this.treeData = this.treeViewDetail.nodesData;
        //    this.popupHeader = this.treeViewDetail.selectedDPValue;
        //}

    }



    //initilasying the dropdown members
    ngOnInit() {
        
    }

    bindTreeData() {
       
        this.nodeData = [];
        //RP Section
        if (this.treeViewDetail.treeViewType == "GEO") {
           this.getGeoTreeData();
        }


        if (this.treeViewDetail.treeViewType == "CHANNEL") {
            this.getChannelTreeData();
        }
        //Edit section - start
        if (this.treeViewDetail.treeViewType == "GEO_edit") {
            this.getGeoTreeData();
        }

        if (this.treeViewDetail.treeViewType == "CHANNEL_edit") {
            this.getChannelTreeData();
        }
       
        //GMP Section
        if (this.treeViewDetail.treeViewType == "GMP_SR_ChannelGeo") {
            this.getGMPChannelGeoTreeData("");
        }

        if (this.treeViewDetail.treeViewType == "GMP_SR_Product") {
           // this.getProductBTOTreeData("");
        }
        if (this.treeViewDetail.treeViewType == "GMP_SR_Product_Create") {
          //  this.getProductBTOTreeData("");
        }
        //Edit section - end

        // this.nodeData = [{ "id": "1__0", "name": "TOTAL GEO", "level": "0", "children": [{ "id": "APJAP__1", "name": "APJAP ~APJ", "level": "1", "children": [{ "id": "APJSA__2", "name": "APJSA ~SOUTH ASIA", "level": "2", "children": [{ "id": "AFGHA__30", "name": "AFGHA ~AFGANISTAN", "level": "3" }, { "id": "BANGL__30", "name": "BANGL ~BANGLADESH", "level": "3" }, { "id": "INDON__30", "name": "INDON ~INDONESIA", "level": "3" }] }, { "id": "INDIA__2", "name": "INDIA ~INDIA", "level": "2", "children": [{ "id": "INDIA__30", "name": "INDIA ~INDIA", "level": "3" }] }] }, { "id": "AMER__1", "name": "AMER ~AMERICAS", "level": "1", "children": [{ "id": "AMSUP__2", "name": "AMSUP ~AMERICAS SUPPORT", "level": "2", "children": [{ "id": "AMSUP__30", "name": "AMSUP ~AMERICAS SUPPORT", "level": "3" }] }, { "id": "AMINT__2", "name": "AMINT ~LATIN AMERICA", "level": "2", "children": [{ "id": "ARGEN__30", "name": "ARGEN ~ARGENTINA", "level": "3" }, { "id": "BRAZI__30", "name": "BRAZI ~BRAZIL", "level": "3" }] }] }, { "id": "EMEAF__1", "name": "EMEAF ~EMEA", "level": "1", "children": [{ "id": "EPEMG__2", "name": "EPEMG ~EMERGING", "level": "2", "children": [{ "id": "EGYPT__30", "name": "EGYPT ~EGYPT", "level": "3" }, { "id": "HUNGA__30", "name": "HUNGA ~HUNGARY", "level": "3" }, { "id": "ISRAE__30", "name": "ISRAE ~ISRAEL", "level": "3" }] }, { "id": "EPWEE__2", "name": "EPWEE ~WESTERN EUROPE", "level": "2", "children": [{ "id": "FINLA__30", "name": "FINLA ~FINLAND", "level": "3" }] }, { "id": "EPLAC__2", "name": "EPLAC ~LARGE COUNTRIES", "level": "2", "children": [{ "id": "FRANC__30", "name": "FRANC ~FRANCE", "level": "3" }, { "id": "GERMA__30", "name": "GERMA ~GERMANY", "level": "3" }] }] }] }];
    }

    getGeoTreeData() {
       
        this.nodeData = [];
        this._fulfillmentSiteAssignmentServiceService.getGeoTreeData()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        
                        this.nodeData = data;
                    }
                },
                err => console.log(err),
            );

    }

    getProductBTOTreeData(strSearch: string) {
        
        this.nodeData = [];
        this._fulfillmentSiteAssignmentServiceService.getProductTreeData(strSearch)
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        
                        this.nodeData = data;
                    }
                },
                err => console.log(err),
            );
    }

    getProductFHCTreeData(strSearch: string) {
        this.nodeData = [];
        this._fulfillmentSiteAssignmentServiceService.getProductFHCTreeData(strSearch)
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.nodeData = [];
                        this.nodeData = data;
                    }
                },
                err => console.log(err),
            );
    }

    getProductFGATreeData(strSearch: string) {
        this.nodeData = [];
        this._fulfillmentSiteAssignmentServiceService.getProductFGATreeData(strSearch)
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.nodeData = [];
                        this.nodeData = data;
                    }
                },
                err => console.log(err),
            );
    }

    getChannelTreeData() {
        this.nodeData = [];
        this._fulfillmentSiteAssignmentServiceService.getChannelTreeData()
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        //this.nodeData = [];
                        this.nodeData = data;
                    }
                },
                err => console.log(err),
            );
    }


    //GMP Section
    getGMPChannelGeoTreeData(filterString) {
        this.nodeData = [];
        this._sourcingRuleService.getGMPChannelGeoTreeData(filterString)
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.nodeData = [];
                        this.nodeData = data;
                    }
                },
                err => console.log(err),
            );
    }

    getGMPProductTreeData(filterString:string) {
        this.nodeData = [];
        this._sourcingRuleService.getGMPProductTreeData(filterString)
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.nodeData = data;
                    }
                },
                err => console.log(err),
            );
    }

    //getProductBTOTreeData(strSearch: string) {
    //    this.nodeData = [];
    //    this._fulfillmentSiteAssignmentServiceService.getProductTreeData(strSearch)
    //        .subscribe(
    //            (data: any) => {
    //                if (data != null) {
    //                    data = JSON.parse(data._body);

    //                    this.nodeData = data;
    //                }
    //            },
    //            err => console.log(err),
    //        );
    //}
    //GMP Section End


    onDeSelectTreeNode(event: any) {
       // alert('deactivate');
    }

    onSelectTreeNode(event: any) {

        //alert('double in select tree');
        //if (event.node.isActive) {
        //    alert('a');
        //}
        //else {
        //    alert('n a');
        //}

       

        if (event.node.isActive) {
            var strLevel: any = null;

            if (event.node.data.name != '' && event.node.data.id != '') {
                strLevel = event.node.data.id.split('__')[1];
            }


            if (this.treeViewDetail.treeViewType == "GEO") {
                this.selectedGeogrphyTreeData = event.node.data.name;

                this.selectedGeogrphyTreeDataChanged.emit(this.selectedGeogrphyTreeData + '__' + strLevel);
            }

            if (this.treeViewDetail.treeViewType == "PRODUCT_TREE" || this.treeViewDetail.treeViewType == "PRODUCT_FHC" || this.treeViewDetail.treeViewType == "PRODUCT_FGA") {

                if ((this.treeViewDetail.treeViewType == "PRODUCT_FHC" && event.node.data.name == "ALL FHC") || (this.treeViewDetail.treeViewType == "PRODUCT_FGA" && event.node.data.name == "FGA")) {
                    return false;
                }
                else {
                    this.selectedProductTreeData = event.node.data.name;

                    this.selectedProductTreeDataChanged.emit(this.selectedProductTreeData + '__' + strLevel);
                }
            }

            if (this.treeViewDetail.treeViewType == "CHANNEL") {
                this.selectedChennelTreeData = event.node.data.name;
                this.selectedChennelTreeDataChanged.emit(this.selectedChennelTreeData + '__' + strLevel);
            }

            //Edit section - start
            if (this.treeViewDetail.treeViewType == "GEO_edit") {
                this.selectedGeogrphyTreeData_edit = event.node.data.name;

                this.selectedGeogrphyTreeDataChanged_edit.emit(this.selectedGeogrphyTreeData_edit + '__' + strLevel);
            }

            if (this.treeViewDetail.treeViewType == "PRODUCT_TREE_edit" || this.treeViewDetail.treeViewType == "PRODUCT_FHC_edit" || this.treeViewDetail.treeViewType == "PRODUCT_FGA_edit") {

                if ((this.treeViewDetail.treeViewType == "PRODUCT_FHC_edit" && event.node.data.name == "ALL FHC") || (this.treeViewDetail.treeViewType == "PRODUCT_FGA_edit" && event.node.data.name == "FGA")) {
                    return false;
                }
                else {
                    this.selectedProductTreeData_edit = event.node.data.name;

                    this.selectedProductTreeDataChanged_edit.emit(this.selectedProductTreeData_edit + '__' + strLevel);
                }
            }

            if (this.treeViewDetail.treeViewType == "CHANNEL_edit") {
                this.selectedChennelTreeData_edit = event.node.data.name;
                this.selectedChennelTreeDataChanged_edit.emit(this.selectedChennelTreeData_edit + '__' + strLevel);
            }

            //Edit section - end

            //GMP Section

            if (this.treeViewDetail.treeViewType == "GMP_SR_ChannelGeo") {
                this.selectedGMPChennelGeoTreeData = event.node.data.name;
                this.selectedGMPChennelGeoTreeDataChanged.emit(this.selectedGMPChennelGeoTreeData + '__' + strLevel);
            }

            if (this.treeViewDetail.treeViewType == "GMP_SR_Product") {
                this.selectedGMPProductTreeData = event.node.data.name;
                this.selectedGMPProductTreeDataChanged.emit(this.selectedGMPProductTreeData + '__' + strLevel);
            }
            if (this.treeViewDetail.treeViewType == "GMP_SR_Product_Create") {
                this.selectedGMPProductCreateTreeData = event.node.data.name;
                this.selectedGMPProductCreateTreeDataChanged.emit(this.selectedGMPProductCreateTreeData + '__' + strLevel);
            }


            //routing rule 
            if (this.treeViewDetail.treeViewType == "PRODUCT_TREE_create" || this.treeViewDetail.treeViewType == "PRODUCT_FHC_create" || this.treeViewDetail.treeViewType == "PRODUCT_FGA_create") {
                this.selectedRoutingCreateProductTreeData = event.node.data.name;
                this.selectedRoutingCreateProductTreeDataChanged.emit(this.selectedRoutingCreateProductTreeData + '__' + strLevel);
            }


            if (this.treeViewDetail.treeViewType == "PRODUCT_TREE_copy" || this.treeViewDetail.treeViewType == "PRODUCT_FHC_copy" || this.treeViewDetail.treeViewType == "PRODUCT_FGA_copy") {
                this.selectedRoutingCreateProductTreeData = event.node.data.name;
                this.selectedRoutingCopyProductTreeDataChanged.emit(this.selectedRoutingCreateProductTreeData + '__' + strLevel);
            }

            this.closeAddExpenseModal.nativeElement.click();

            return false;
        }

    }


    GetFilteredData() {

        
        if (this.filterString == 'undefined') {
            this.errMsg = "No Data Found";
        }
        else {
            if (this.treeViewDetail.treeViewType == "PRODUCT_TREE") {
                this.getProductBTOTreeData(this.filterString);
            }
            if (this.treeViewDetail.treeViewType == "PRODUCT_FHC") {
                this.getProductFHCTreeData(this.filterString);
            }
            if (this.treeViewDetail.treeViewType == "PRODUCT_FGA") {
                this.getProductFGATreeData(this.filterString);
            }
            //Edit section - start
            if (this.treeViewDetail.treeViewType == "PRODUCT_TREE_edit") {
                this.getProductBTOTreeData(this.filterString);
            }
            if (this.treeViewDetail.treeViewType == "PRODUCT_FHC_edit") {
                this.getProductFHCTreeData(this.filterString);
            }
            if (this.treeViewDetail.treeViewType == "PRODUCT_FGA_edit") {
                this.getProductFGATreeData(this.filterString);
            }
            //Edit section - end

            //GMP Section
            if (this.treeViewDetail.treeViewType == "GMP_SR_ChannelGeo") {
                this.getGMPChannelGeoTreeData(this.filterString);
            }

            if (this.treeViewDetail.treeViewType == "GMP_SR_Product") {
                this.getProductBTOTreeData(this.filterString);
            }
            if (this.treeViewDetail.treeViewType == "GMP_SR_Product_Create") {
                this.getProductBTOTreeData(this.filterString);
            }


            //reouting rules
            if (this.treeViewDetail.treeViewType == "PRODUCT_TREE_create") {
                this.getProductBTOTreeData(this.filterString);
            }
            if (this.treeViewDetail.treeViewType == "PRODUCT_FHC_create") {
                this.getProductFHCTreeData(this.filterString);
            }
            if (this.treeViewDetail.treeViewType == "PRODUCT_FGA_create") {
                this.getProductFGATreeData(this.filterString);
            }

            if (this.treeViewDetail.treeViewType == "PRODUCT_TREE_copy") {
                this.getProductBTOTreeData(this.filterString);
            }
            if (this.treeViewDetail.treeViewType == "PRODUCT_FHC_copy") {
                this.getProductFHCTreeData(this.filterString);
            }
            if (this.treeViewDetail.treeViewType == "PRODUCT_FGA_copy") {
                this.getProductFGATreeData(this.filterString);
            }

            this.filterString = '';

        }
    }
}

//To Enable bootstrap multi select inside grid for checkbox
function loadJavaScript() {
    var url = '/dist/grid-multi-select.js';
    var s = document.createElement('script');
    s.setAttribute('src', url);
    document.head.appendChild(s);
}

