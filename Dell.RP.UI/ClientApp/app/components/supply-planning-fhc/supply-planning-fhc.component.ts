﻿import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, Input } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { SessionStorageService } from 'ngx-webstorage';
import { SupplyPlanningFhcService } from '../../services/supply-planning-fhc.service';
import * as XLSX from 'xlsx';
import { UserRoles } from '../../Model/user-roles';
import { UtilitiesService } from '../../services/utilities.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetErrorGridService } from '../../services/get-error-grid.service';
import { from } from 'rxjs/observable/from';
//import { treeViewData, node, children, FulFillmentSearchInput } from '../../Model/tree-view-data';
import { treeViewData } from '../../Model/treeview/treeViewData';
import { node } from '../../Model/treeview/node';
import { children } from '../../Model/treeview/children';
import { FulFillmentSearchInput } from '../../Model/treeview/FulFillmentSearchInput';

import { TreeViewComponent } from '../../components/tree-view/tree-view.component'
import { expand } from 'rxjs/operator/expand';


declare var jQuery: any;

@Component({
    selector: 'app-supply-planning-fhc',
    templateUrl: './supply-planning-fhc.component.html',
    styleUrls: ['./supply-planning-fhc.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class SupplyPlanningFhcComponent implements OnInit {

    @ViewChild('fakeClick') fakeClick: ElementRef;
    @ViewChild('file') FileName: any;
    @ViewChild(TreeViewComponent) treeviewChild: TreeViewComponent;
    private gridOptionsName: GridOptions;
    excelfile: any;
    selectedfilename: string;

    //tree variables
    nodes: node[];
    treeViewNodesFilterdData: node[];
    treeViewDetailData: treeViewData = new treeViewData();
    options: any;

    //Family Tree Search
    //selectedFamilyParentTreeData: string;
    //selectedFamilyParentData_label: string;
    //isFamilyParentTreeHidden: boolean = true;
    //nodesFamilyParent: node[];
    //onSelectedFamilyParentTreeDataChanged(selectedTreeValue: string): void {
    //    this.selectedFamilyParentTreeData = selectedTreeValue;
    //    this.selectedFamilyParentData_label = this.selectedFamilyParentTreeData.split('__')[0];
    //}

    //Fhc_Id Tree Search
    //selectedFhcIdTreeData: string;
    //selectedFhcIdData_label: string;
    //isFhcIdTreeHidden: boolean = true;
    //nodesFhcId: node[];
    //onSelectedFhcIdTreeDataChanged(selectedTreeValue: string): void {
    //    debugger;
    //    this.selectedFhcIdTreeData = selectedTreeValue;
    //    this.selectedFhcIdData_label = this.selectedFhcIdTreeData.split('__')[0];
    //}

    singleSelectDropdownSettings = {};
    singleSelectDropdownSettingsReviewWeeks = {};
    singleSelectDropdownSettingsManSite = {};

    reviewWeekLimit = 52;
    reviewThroughWeekData = [];
    selectedWeek = [];

    //for fhc id dd
    fhcIdData = [];
    selectedFhcId = "";

    //for family parent dd
    familyParentData = [];
    selectedFamilyParent = [];

    //for manSite dd
    ManufacturingSiteData = [];
    selectedManSite = [];

    //for region dd
    regionData = [];
    selectedRegion = [];

    //for PO UI Lock
    POUILockData = [];

    isUploadDisabled: boolean = true;
    private fileuploadName;

    //for displaying error msg on top right corner
    colorCode: string;
    successMsg = '';
    hideShowAlertMsg = false;

    currentRPStatus = '';

    SupplyPlanFhcExcelHeader: string[] = ['ITEM_ID', 'FROM_SITE', 'DEMAND_GEO', 'QTY', 'SHIP_DATE', 'MODIFIED_BY'];
    public _supplyPlanFhcExcelData = [];

    //error grid
    private errGridOptions: GridOptions;
    private errRowData;
    private errGridColumnApi;
    hasError: boolean = false;
    errGridExportDisabled: boolean = true

    //Roles and Authorization
    userRoles: UserRoles[];
    screenId = "2004";
    isUserHavingWriteAccess: boolean = false;
    isActiveScreen: boolean = true;
    userHavingWriteAccess: string[];
    listOfRoleHavingAccess: string[];
    userTenantTypeAccessList: string[];
    isUserHavingUserMaintenance: boolean = false;

    constructor(private _router: Router, private _supplyPlanningFhcService: SupplyPlanningFhcService, private _sessionSt: SessionStorageService) {

        this.Authorization();

        this.errGridOptions = <GridOptions>{
            contextName: {
                componentParentName: this
            },
        };

        this.errGridOptions.columnDefs = [
            { headerName: "ITEM_ID", field: "itemID", suppressSizeToFit: false, tooltipField: "itemID", editable: false, width: 160, },
            { headerName: "FROM_SITE", field: "fromSite", suppressSizeToFit: false, tooltipField: "fromSite", editable: false, width: 160, },
            { headerName: "GEO", field: "geo", suppressSizeToFit: false, tooltipField: "geo", editable: false, width: 120, },
            { headerName: "QTY", field: "qty", suppressSizeToFit: false, tooltipField: "qty", editable: false, width: 120, },
            { headerName: "SHIP_DATE", field: "shipDate", suppressSizeToFit: false, tooltipField: "shipDate", editable: false, width: 160, },
            { headerName: "MODIFIED_BY", field: "modifiedBy", suppressSizeToFit: false, tooltipField: "modifiedBy", editable: false, width: 160, },
            { headerName: "ERROR_DESCRIPTION", field: "errDesc", suppressSizeToFit: false, tooltipField: "errDesc", editable: false, width: 349, },
        ];

        this.errGridOptions.headerHeight = 18;
        this.errGridOptions.rowSelection = "multiple";
        this.errGridOptions.paginationPageSize = 14;
        this.errGridOptions.defaultColDef = {
            editable: true,
            enableValue: true
        };
        this.errRowData = [];
    }

    ngOnInit() {

        this.singleSelectDropdownSettings = {
            singleSelection: true,
            text: "ALL",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: false,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };

        this.singleSelectDropdownSettingsReviewWeeks = {
            singleSelection: true,
            text: "",
            enableSearchFilter: false,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };

        this.singleSelectDropdownSettingsManSite = {
            singleSelection: true,
            text: "ALL",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };

        this.selectedWeek[0] = { id: "Wk 4", itemName: "Wk 4" }
        this.selectedFhcId = "ALL";
        this.selectedFamilyParent[0] = { id: "ALL", itemName: "ALL" };
        this.selectedManSite[0] = { id: "ALL", itemName: "ALL" };
        this.selectedRegion[0] = { id: "ALL", itemName: "ALL" };
        
        this.getRPStatus();
        this.bindPOUILock();
        this.getReviewThroughWeeks();
        this.getFamilyParent();
        this.getRegion();
        this.getManSite();

    }

    Authorization() {
        var rolesHavingWriteAccess = this._sessionSt.retrieve('screenAuthorization') != null ? this._sessionSt.retrieve('screenAuthorization') : [];
        this.userHavingWriteAccess = rolesHavingWriteAccess[rolesHavingWriteAccess.findIndex(x => x.functionId == this.screenId)]["roleId"].split(',');
        this.userTenantTypeAccessList = this._sessionSt.retrieve('userTenantTypeAccessList') != null ? this._sessionSt.retrieve('userTenantTypeAccessList') : [];

        if (this._sessionSt.retrieve('userRoles') === null) {
            this.isActiveScreen = false;
            this._router.navigateByUrl('/errorMessage');
            return;
        }
        else {
            this.userRoles = this._sessionSt.retrieve('userRoles');
            for (var i = 0; i < this.userRoles.length; i++) {
                if (this.userHavingWriteAccess.includes(this.userRoles[i].roleId.toString())) {
                    this.isUserHavingWriteAccess = true;
                    return;
                }
            }

            if (!(this.isUserHavingWriteAccess || this.userTenantTypeAccessList.includes("RP"))) {
                this.isActiveScreen = false;
                this._router.navigateByUrl('/errorMessage');
                return;
            }
        }
    }

    onErrGridReady(errparam) {
        this.errGridColumnApi = errparam.columnApi;
    }

    getReviewThroughWeeks() {
        for (var i = 0; i < this.reviewWeekLimit; i++) {
            this.reviewThroughWeekData.push({
                id: "Wk " + i,
                itemName: "Wk " + i
            });
        }
    }

    getFamilyParent() {
        this._supplyPlanningFhcService.getFamilyParentDD()
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        //data = JSON.parse(data._body);
                        this.familyParentData = [];
                        this.familyParentData.push({
                            id: "ALL",
                            itemName: "ALL"
                        });
                        for (var i = 0; i < data.length; i++) {
                            this.familyParentData.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })

                        }
                    }
                },
                err => console.log(err),
            );
    }

    getRegion() {
        this.regionData.push({
            id: "ALL",
            itemName: "ALL"
        });
        this.regionData.push({
            id: "AMER",
            itemName: "AMER"
        });
        this.regionData.push({
            id: "APJAP",
            itemName: "APJAP"
        });
        this.regionData.push({
            id: "EMEAF",
            itemName: "EMEAF"
        });
    }

    getManSite() {
        this._supplyPlanningFhcService.getManSite()
            .map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        //data = JSON.parse(data._body);
                        this.ManufacturingSiteData = [];
                        this.ManufacturingSiteData.push({
                            id: "ALL",
                            itemName: "ALL"
                        });
                        for (var i = 0; i < data.length; i++) {
                            this.ManufacturingSiteData.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })

                        }
                    }
                },
                err => console.log(err),
            );
    }

    getRPStatus() {
        this._supplyPlanningFhcService.getRPStatus()
            .subscribe(
            (data: any) => {
                    if (data != null) {
                        this.currentRPStatus = "";
                        if (data._body.toLowerCase() == "state_pre_rp_run") {
                            this.currentRPStatus = "PRE RP VIEW (Saturday – Wednesday).";
                        }
                        else if (data._body.toLowerCase() == "state_rp_running") {
                            this.currentRPStatus = "RP RUN IN PROGRESS (No Edits/Updates Allowed).";
                        }
                        else if (data._body.toLowerCase() == "state_rp_run_complete") {
                            this.currentRPStatus = "POST RP VIEW (Thursday – Friday).";
                            this.isUploadDisabled = false;
                        }
                        else if (data._body.toLowerCase() == "state_post_rp_run") {
                            this.currentRPStatus = "POST RP RUN PROCESSING (No Edits/Updates Allowed).";
                        }
                        else if (data._body.toLowerCase() == "po_process_running") {
                            this.currentRPStatus = "PO PROCESS IS RUNNING (No Edits/Updates Allowed).";
                        }
                    }
                },
                err => console.log(err),
            );
    }

    bindPOUILock() {
        this._supplyPlanningFhcService.bindPOUILock()
            .map((data: any) => data.json())
            .subscribe(
            (data: any) => {
                    if (data != null) {
                        this.POUILockData = data;
                        if (data.find(x => x.id === "APJ_Status").itemName != "SUCCESS") {
                            this.isUploadDisabled = true;
                        }
                        else if (data.find(x => x.id === "EMEA_Status").itemName != "SUCCESS") {
                            this.isUploadDisabled = true;
                        }
                        else if (data.find(x => x.id === "AMER_Status").itemName != "SUCCESS") {
                            this.isUploadDisabled = true;
                        }
                    }
                },
                err => console.log(err),
            );
    }

    //onSelectFamilyParent() {
    //    if (this.selectedFamilyParent[0].itemName == "ALL") {
    //        this.isFamilyParentTreeHidden = true;
    //        this.selectedFamilyParentTreeData = "";
    //        this.selectedFamilyParentData_label = "";
    //    }
    //    else {
    //        this.isFamilyParentTreeHidden = false;
    //    }
    //}

    //getFamilyParentTreeDetail() {
    //    this.nodesFamilyParent = [];
    //    this.treeViewDetailData.selectedDPValue = this.selectedFamilyParent[0].itemName;
    //    this.treeViewDetailData.nodesData = this.nodesFamilyParent;
    //    this.treeViewDetailData.header = "Family Parent Tree Popup";
    //    this.treeViewDetailData.isSearchEnable = false;
    //    this.treeViewDetailData.msgNote = "";
    //    this.treeViewDetailData.headerMsg = "";
    //    this.treeViewDetailData.treeViewType = "RP_SP_FHC_FamilyParent";

    //    this.treeviewChild.bindTreeData();
    //}

    //onSelectFhcId() {
    //    if (this.selectedFhcId[0].itemName == "ALL") {
    //        this.isFhcIdTreeHidden = true;
    //        this.selectedFhcIdTreeData = "";
    //        this.selectedFhcIdData_label = "";
    //    }
    //    else {
    //        this.isFhcIdTreeHidden = false;
    //    }
    //}

    //getFhcIdTreeDetail() {
    //    this.nodesFhcId = [];
    //    this.treeViewDetailData.selectedDPValue = this.selectedFhcId[0].itemName;
    //    this.treeViewDetailData.nodesData = this.nodesFhcId;
    //    this.treeViewDetailData.header = "FHC ID Tree Popup";
    //    this.treeViewDetailData.isSearchEnable = true;
    //    this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All FHC";
    //    this.treeViewDetailData.headerMsg = "Please enter FHC ID!";
    //    this.treeViewDetailData.treeViewType = "RP_SP_FHC_FhcId";

    //    this.treeviewChild.bindTreeData();
    //}

    DisplayMsg(msg: any, code: any) {
        this.hideShowAlertMsg = true;
        if (code == 'success') {
            this.successMsg = msg;
            this.colorCode = 'green';
        }
        else {
            this.successMsg = msg;
            this.colorCode = 'red';
        }
    }

    clearErrorMsg() {
        this.hideShowAlertMsg = false;
        this.successMsg = '';
    }

    clearFilterData() {
        this.clearErrorMsg();
        this.selectedWeek[0] = { id: "Wk 4", itemName: "Wk 4" }
        this.selectedFhcId = "ALL";
        this.selectedFamilyParent[0] = { id: "ALL", itemName: "ALL" };
        this.selectedManSite[0] = { id: "ALL", itemName: "ALL" };
        this.selectedRegion[0] = { id: "ALL", itemName: "ALL" };
        this.errRowData = [];
        this.hasError = false;
        this.FileName.nativeElement.value = "";
        //this.isUploadDisabled = true;
        //this.selectedFamilyParentTreeData = "";
        //this.selectedFamilyParentData_label = "";
        //this.isFamilyParentTreeHidden = true;
        //this.selectedFhcIdTreeData = "";
        //this.selectedFhcIdData_label = "";
        //this.isFhcIdTreeHidden = true; 
    }

    exportSupplyPlanningFhcData() {
        this.clearErrorMsg();

        var strReviewThroughWeek: string = '';
        var strFhcId: string = '';
        var strFamilyParent: string = '';
        var strManSite: string = '';
        var strRegion: string = '';
        var isError: Boolean = false;
        var errMsg: string = '';

        //Review through Weeks
        //strReviewThroughWeek = this.selectedWeek[0].itemName.replace(/\s/g, "");
        if (this.selectedWeek.length != 0) {
            strReviewThroughWeek = this.selectedWeek[0].itemName.substring(3);
        }
        else {
            isError = true;
            errMsg = "Please select a Week to review through."
        }
        //Fhc ID
        if (this.selectedFhcId == "ALL" || this.selectedFhcId == "") {
            strFhcId = null;
        }
        else {
            strFhcId = this.selectedFhcId;

        }
        
        //Family Parent
        if (this.selectedFamilyParent.length != 0) {
            if (this.selectedFamilyParent[0].itemName == "ALL") {
                strFamilyParent = null;
            }
            else {
                strFamilyParent = this.selectedFamilyParent[0].itemName;
            }
        }
        else {
            strFamilyParent = null;
        }

        //Manufacturing Site
        if (this.selectedManSite.length != 0) {
            if (this.selectedManSite[0].itemName == "ALL") {
                strManSite = null;
            }
            else {
                strManSite = this.selectedManSite[0].itemName;
            }
        }
        else {
            strManSite = null;
        }

        //Region
        if (this.selectedRegion.length != 0) {
            if (this.selectedRegion[0].itemName == "ALL") {
                strRegion = null;
            }
            else {
                strRegion = this.selectedRegion[0].itemName;
            }
        }
        else {
            strRegion = null;
        }

        var params = {
            ReviewWeeks: strReviewThroughWeek,
            FhcID: strFhcId,
            FamilyParent: strFamilyParent,
            ManSite: strManSite,
            Region: strRegion
        };
        
        if (!isError) {
            this._supplyPlanningFhcService.getExportData(params)
                .map((data: any) => data.json())
                .subscribe(
                    (data: any) => {
                        var expData = data;
                        let param = [];
                        if (expData == null) {
                            this.DisplayMsg("No Data Found!", "fail");
                        }
                        else {
                        for (var i = 0; i < expData.length; i++) {
                            param.push({
                                ["FHC_ID"]: expData[i].fhcId,
                                ["FAMPAR_CODE"]: expData[i].famParCode,
                                ["FAMILY_PARENT"]: expData[i].familyParent,
                                ["MANUFACTURING_SITE"]: expData[i].manSite,
                                ["DEMAND_GEO"]: expData[i].demandGeo,
                                ["REGION"]: expData[i].region,
                                ["SSC"]: expData[i].sscType,
                                ["SHIP_MODE"]: expData[i].shipMode,
                                ["SHIP_TO_SITE"]: expData[i].shipToSite,
                                ["SHIP_DATE"]: expData[i].shipDate,
                                ["DELIVERY_DATE"]: expData[i].deliveryDate,
                                ["ORIGINAL_QTY"]: expData[i].orginialQty,
                                ["UPDATED_QTY"]: expData[i].updatedQty,
                                ["UPDATED_BY"]: expData[i].updatedBy
                            });
                        }
                        this._supplyPlanningFhcService.exportAsExcelFile(param, "RP_FHC_Export_Detail_Extract");
                        }
                    },
                    err => console.log(err),
                );
        }
        else {
            this.DisplayMsg(errMsg, 'fail');
        }
    }

    DownloadTemplate(event) {
        let param = [];
        param.push({
            "ITEM_ID": "",
            "FROM_SITE": "",
            "DEMAND_GEO": "",
            "QTY": "",
            "SHIP_DATE": "",
            "MODIFIED_BY": ""
        });
        this._supplyPlanningFhcService.exportAsExcelFileUploadTemplate(param, "RP_Supply_Plan_FHC_Upload_Template");
    }

    FileValidation($event) {
        this._supplyPlanFhcExcelData = [];
        this.clearErrorMsg();
        if ($event.target.files && $event.target.files[0]) {
            this.excelfile = $event.target.files[0];
            this.selectedfilename = this.excelfile.name;
            var fileNameWithOutExtenstion = this.selectedfilename.substring(0, this.selectedfilename.lastIndexOf('.')).toUpperCase();
            var fileExt1 = this.selectedfilename.substring(this.selectedfilename.lastIndexOf('.') + 1).toUpperCase();
            if (fileExt1 != 'XLSX') {
                var msg = "\"" + fileExt1 + "\" - Incorrect file extension!!";
                this.DisplayMsg(msg, "fail");
                this.isUploadDisabled = true;
            }
            //else {
            //    this.isUploadDisabled = false;
            //}
        }
        else {
            this.isUploadDisabled = true;
        }
    }

    UploadFile() {
        this.clearErrorMsg();
        this.hasError = false;
        this.errRowData = [];
        this.errGridExportDisabled = true;
        var errGridData = [];
        //let flag = true;
        //this._supplyPlanningFhcService.bindPOUILock()
        //    .map((data: any) => data.json())
        //    .subscribe(
        //        (data: any) => {
        //            if (data != null) {
        //                this.POUILockData = data;
        //                if (data.find(x => x.id === "APJ_Status").itemName != "SUCCESS") {
        //                    this.isUploadDisabled = true;
        //                    flag = false;
        //                }
        //                else if (data.find(x => x.id === "EMEA_Status").itemName != "SUCCESS") {
        //                    this.isUploadDisabled = true;
        //                    flag = false
        //                }
        //                else if (data.find(x => x.id === "AMER_Status").itemName != "SUCCESS") {
        //                    this.isUploadDisabled = true;
        //                    flag = false;
        //                }
        //            }

        //if (!this.isUploadDisabled) {
        if (this.FileName.nativeElement.value != "") {
            var reader: FileReader = new FileReader();
            reader.onloadend = (e) => {
                var data = reader.result;
                var workbook = XLSX.read(data, { type: 'binary', cellDates: true, dateNF: 'mm/dd/yyyy;@' });
                let excelHeader = [];
                var first_sheet_name = workbook.SheetNames[0];
                var headers = XLSX.utils.sheet_to_json(workbook.Sheets[first_sheet_name], { header: 1 })[0];
                var dataObjects: any = XLSX.utils.sheet_to_json(workbook.Sheets[first_sheet_name]);
                if (JSON.stringify(headers) === JSON.stringify(this.SupplyPlanFhcExcelHeader)) {
                    if (dataObjects.length > 0) {
                        this._supplyPlanFhcExcelData = [];
                        for (var i = 0; i < dataObjects.length; i++) {
                            this._supplyPlanFhcExcelData.push({
                                item_id: typeof dataObjects[i]["ITEM_ID"] == "undefined" ? "" : dataObjects[i]["ITEM_ID"],
                                from_site: typeof dataObjects[i]["FROM_SITE"] == "undefined" ? "" : dataObjects[i]["FROM_SITE"],
                                geo: typeof dataObjects[i]["DEMAND_GEO"] == "undefined" ? "" : dataObjects[i]["DEMAND_GEO"],
                                updated_qty: typeof dataObjects[i]["QTY"] == "undefined" ? "" : dataObjects[i]["QTY"],
                                ship_date: typeof dataObjects[i]["SHIP_DATE"] == "undefined" ? "" : dataObjects[i]["SHIP_DATE"].split(" ")[0],
                                sys_last_modified_by: typeof dataObjects[i]["MODIFIED_BY"] == "undefined" ? "" : dataObjects[i]["MODIFIED_BY"],
                            });
                        }

                        this._supplyPlanningFhcService.getStatusForInsertFHC(this._supplyPlanFhcExcelData)
                            //.map((data: any) => data.json())
                            .subscribe(
                                (data: any) => {
                                    if (data._body == "PASSED") {
                                        this._supplyPlanningFhcService.getErrGridDetails()
                                            .map((data: any) => data.json())
                                            .subscribe(
                                                (data: any) => {
                                                    errGridData = data;
                                                    if (errGridData != null) {
                                                        this.DisplayMsg("Please enter Valid Records to proceed. For more Details, check Error Grid below", "fail");
                                                        this.hasError = true;
                                                        this.errRowData = errGridData;
                                                        this.errGridExportDisabled = false;
                                                        this.FileName.nativeElement.value = "";
                                                    }
                                                    else {
                                                        this.hasError = false;
                                                        var lockStatus = "";
                                                        this._supplyPlanningFhcService.getLockStatusFHC("LOCK")
                                                            //.map((data: any) => data.json())
                                                            .subscribe(
                                                                (data: any) => {
                                                                    lockStatus = data._body;
                                                                    if (lockStatus == "PASSED") {
                                                                        var uploadStatus = "";
                                                                        this._supplyPlanningFhcService.getValidRecStatusFHC()
                                                                            //.map((data: any) => data.json())
                                                                            .subscribe(
                                                                                (data: any) => {
                                                                                    uploadStatus = data._body;
                                                                                    if (uploadStatus == "UPLOAD_PROCESS_HAS_BEEN_COMPLETED_SUCCESSFULY") {
                                                                                        this.DisplayMsg("UPLOAD PROCESS HAS BEEN COMPLETED SUCCESSFULLY", "success");
                                                                                        this.FileName.nativeElement.value = "";
                                                                                        var num;
                                                                                        this._supplyPlanningFhcService.updateAutoUnlockUIFHC()
                                                                                            //.map((data: any) => data.json())
                                                                                            .subscribe(
                                                                                                (data: any) => {
                                                                                                    num = data._body;
                                                                                                });

                                                                                    }
                                                                                    else {
                                                                                        this.DisplayMsg(uploadStatus, "fail");
                                                                                    }
                                                                                });

                                                                    }
                                                                });
                                                    }
                                                });
                                    }
                                });
                    }
                    else {
                        this.DisplayMsg("Uploaded template is either blank or has no records. Please upload a template with valid records.", "fail");
                    }
                }
                else {
                    this.DisplayMsg("Uploaded template contains invalid columns, for reference please download the template.", "fail");
                }
            }
            reader.readAsBinaryString(this.excelfile);
        }
        else {
            this.hasError = false;
            this.DisplayMsg("Please select and Excel File to Upload!", "fail");
        }
                    //}
    }

    errGridExcelExport() {
        var param = [];
        for (var i = 0; i < this.errRowData.length; i++) {
            param.push({
                ["ITEM_ID"]: this.errRowData[i].itemID,
                ["FROM_SITE"]: this.errRowData[i].fromSite,
                ["GEO"]: this.errRowData[i].geo,
                ["QTY"]: this.errRowData[i].qty,
                ["SHIP_DATE"]: this.errRowData[i].shipDate,
                ["MODIFIED_BY"]: this.errRowData[i].modifiedBy,
                ["ERROR_DESCRIPTION"]: this.errRowData[i].errDesc
            });
        }
        this._supplyPlanningFhcService.exportAsExcelFile(param, "RP_FHC_Upload_Invalid_Records");
    }
    
}