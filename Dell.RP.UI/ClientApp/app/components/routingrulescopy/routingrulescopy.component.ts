﻿import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, Input } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { SessionStorageService } from 'ngx-webstorage';
import * as XLSX from 'xlsx';
import { UserRoles } from '../../Model/user-roles';
import { demandsplitService } from '../../services/demand-split.service';
import { UtilitiesService } from '../../services/utilities.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetErrorGridService } from '../../services/get-error-grid.service';
import { routingrulesService } from '../../services/routingrules.service';
import { FulfillmentSiteAssignmentService } from '../../services/fulfillment-siteassignment.service';
import { RoutingRulesSearchPara } from '../../Model/RoutingRulesSearch';
import { AddRouting } from '../../Model/AddRouting';
import { CopyRouting } from '../../Model/CopyRouting';


//import { treeViewData, node, children, FulFillmentSearchInput } from '../../Model/tree-view-data';
import { treeViewData } from '../../Model/treeview/treeViewData';
import { node } from '../../Model/treeview/node';
import { children } from '../../Model/treeview/children';
import { FulFillmentSearchInput } from '../../Model/treeview/FulFillmentSearchInput';
import { TreeViewComponent } from '../../components/tree-view/tree-view.component'
 


import { from } from 'rxjs/observable/from';
import { forEach } from '@angular/router/src/utils/collection';
import { debounce } from 'rxjs/operator/debounce';
import { ReturnStatement } from '@angular/compiler';

declare var jQuery: any;

@Component({
    selector: 'app-routingrulescopy',
    templateUrl: './routingrulescopy.component.html',
    styleUrls: ['./routingrulescopy.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class Copyroutingrules implements OnInit {


    //SearchView: RoutingRulesSearchPara;
    SearchView: CopyRouting;

    constructor(private _router: Router, private _routingrulesService: routingrulesService, private _FulfillmentSiteAssignmentService: FulfillmentSiteAssignmentService, private _sessionSt: SessionStorageService, private _utilitiesService: UtilitiesService) {



    }

    CopyProductdata = [];
    CopySelectedProductvalue = [];
    singleSelectDropdownSettingsProduct = {};
    SelectedCopyProductTree = "";
    selectedProductTreeData: string;
    selectedProductTreeData_label: string;

    nodes: node[];
    treeViewNodesFilterdData: node[];
    treeViewDetailData: treeViewData = new treeViewData();
    @ViewChild(TreeViewComponent) treeviewChild: TreeViewComponent;
    nodesProducts: node[];
    isProductTree: boolean = true;

  
    ngOnInit() {


        this.singleSelectDropdownSettingsProduct = {
            singleSelection: true,
            text: "ALL",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            enableFilterSelectAll: false,
            classes: "myclass custom-class singleSelect"
        };
        this.getProductData();


    }
    getProductData() {

        this._FulfillmentSiteAssignmentService.getMasterDataData('PRODUCT')
            .subscribe(
                (data: any) => {
                    if (data != null) {
                        data = JSON.parse(data._body);
                        this.CopyProductdata = [];
                        for (var i = 0; i < data.length; i++) {
                            this.CopyProductdata.push({
                                id: data[i].id,
                                itemName: data[i].itemName
                            })

                        }
                    }


                    this.CopySelectedProductvalue[0] = { id: "ALL", itemName: "ALL" };
                    
                },
                err => console.log(err),
            );



    }

    SubmiteCopyRules(event) {

       
        this.SearchView


        var lblproduct = "";
        this.SearchView =
            {
                 Product: this.CopySelectedProductvalue[0] == undefined ? "" : this.CopySelectedProductvalue[0].itemName,
                 ddlFGA : "",//this.Selectedgeographyvalue[0] == undefined ? "" : this.Selectedgeographyvalue[0].itemName,
                 routingId: "",// this.SelectedCopyProductTree,// this.selectedProductTreeData_label,
                 hfItemID: "",
                 hfFHCType :"",
                
            };

        this._routingrulesService.SubmiteCopyRules(this.SearchView)
            .map((data: any) => data.json()).subscribe((data: any) => {

                if (data.length > 0) {

                }

            },
                err => console.log(err), // error
            )



    }

    getProductTreeDetail() {
        this.nodesProducts = [];

        this.treeViewDetailData.selectedDPValue = this.CopySelectedProductvalue[0].itemName;
        this.treeViewDetailData.nodesData = this.nodesProducts;
        this.treeViewDetailData.header = "Product Tree Popup";
        this.treeViewDetailData.isSearchEnable = true;

        if (this.CopySelectedProductvalue[0].itemName == "PRODUCT_TREE") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All PRODUCT LOB Names";
            this.treeViewDetailData.headerMsg = "Please enter PRODUCT LOB Name!";
            this.treeViewDetailData.treeViewType = "PRODUCT_TREE";
        }
        if (this.CopySelectedProductvalue[0].itemName == "FHC_TREE") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All FHC Names";
            this.treeViewDetailData.headerMsg = "Please enter FHC ID!";
            this.treeViewDetailData.treeViewType = "PRODUCT_FHC";
        }
        if (this.CopySelectedProductvalue[0].itemName == "FGA") {
            this.treeViewDetailData.msgNote = "Note: Use % as wild card to search All FGA's";
            this.treeViewDetailData.headerMsg = "Please enter FGA ID!";
            this.treeViewDetailData.treeViewType = "PRODUCT_FGA";
        }

        this.treeviewChild.bindTreeData();
    }


    onCopyRulesSelectedProduct() {

        if (this.CopySelectedProductvalue[0].itemName == "PRODUCT_TREE" || this.CopySelectedProductvalue[0].itemName == "FHC_TREE" || this.CopySelectedProductvalue[0].itemName == "FGA") {
            this.isProductTree = false;
            
            
        }
        else {
            this.isProductTree = true;
           
        }


    }




    onselectedProductTreeDataChanged(selectedTreeValue: string): void {
        this.selectedProductTreeData = selectedTreeValue;
        this.selectedProductTreeData_label = this.selectedProductTreeData.split('__')[0];
        
    }
}
 


//To Enable bootstrap multi select inside grid for checkbox
function loadJavaScript() {
    var url = '/dist/grid-multi-select.js';
    var s = document.createElement('script');
    s.setAttribute('src', url);
    document.head.appendChild(s);
}
