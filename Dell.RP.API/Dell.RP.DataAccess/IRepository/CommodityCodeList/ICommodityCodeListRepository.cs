﻿using Dell.RP.API.Dell.RP.Common.Models.CommodityCodeListDetails;
using Dell.RP.API.Dell.RP.Common.Models.MultiTierBOM;
using Dell.RP.DataAccess.IRepository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.DataAccess.IRepository.CommodityCodeList
{
    public interface ICommodityCodeListRepository : IHelper
    {
        Task<List<CommodityCodeListDetails>> GetFilterViewResult(CommodityCodeListDetails param);
        Task<string> UpdateCommodityCodeListDetails(List<CommodityCodeListDetails> CommodityCodeListDetails);
    }
}
