﻿using System.Data;
using System.Threading.Tasks;

namespace Dell.RP.DataAccess.IRepository.ErrorLogging
{
    public interface IErrorLoggingRepository : IHelper
    {
        Task<DataTable> GetErrorList(string procName);
    }
}
