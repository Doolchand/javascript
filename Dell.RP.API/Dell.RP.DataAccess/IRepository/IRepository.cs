﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.DataAccess.IRepository
{
   
        public interface IRepository<TEntity> where TEntity :class
        {


        string UserID { get; set; }

        Task<TEntity> Get(int id);

        Task<IEnumerable<TEntity>> GetAll();

        Task<string> Insert(List<TEntity> entity, string fileName);

        Task<string> Update(TEntity entity);

        Task<string> Remove(TEntity entity);

        Task<string> RemoveRange(IEnumerable<TEntity> entities);
        
    }
}
