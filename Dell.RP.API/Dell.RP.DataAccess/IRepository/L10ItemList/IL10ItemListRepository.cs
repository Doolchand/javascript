﻿using Dell.RP.API.Dell.RP.Common.Models.MultiTierBOM;
using Dell.RP.DataAccess.IRepository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.Common.Models.L10ItemList;

namespace Dell.RP.API.Dell.RP.DataAccess.IRepository.L10ItemList
{
    public interface IL10ItemListRepository : IHelper
    {
        Task<List<l10Item>> Getl10FilterViewResult(l10Item param);
        Task<List<l10Item>> Getl10FilterViewResultExcel(l10Item param);
        
        Task<string> Updatel10ItemListDetails(List<l10Item> l10ItemList);
        Task<List<l10Item>> Getl10ItemList();
    }
}
