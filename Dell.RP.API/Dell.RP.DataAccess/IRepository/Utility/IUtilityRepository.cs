﻿using System;
using System.Data;
using System.Threading.Tasks;

namespace Dell.RP.DataAccess.IRepository.Utility
{
    public interface IUtilityRepository : IHelper
    {
        Task<DataTable> GetAttributeNames(Int32 dimension,string userId);
    }
}
