﻿using Dell.RP.API.Dell.RP.Common.Models.MultiTierBOM;
using Dell.RP.DataAccess.IRepository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.DataAccess.IRepository.MultiTierBOM
{
    public interface IMultiTierBOMRepository : IHelper
    {
        Task<List<MultiTierBOMDetails>> GetFilterViewResult(MultiTierBOMDetails param, int fromNumber, int count);
        Task<string> UpdateMultiTierBOMDetails(List<MultiTierBOMDetails> multiTierBOMDetails);
        Task<DataTable> GetCommodityCodeDDL();
        Task<List<MultiTierBOMDetails>> GetMultiTierViewResultExcel(MultiTierBOMDetails param);

    }
}
