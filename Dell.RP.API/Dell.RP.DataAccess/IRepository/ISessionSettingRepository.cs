﻿using Dell.RP.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.DataAccess.IRepository
{
    public interface ISessionSettingRepository
    {
        Task<string> GetSessionSettingsAsync(UserDetails user);
        Task<string> UpdateUserSessionAsync(updateUserSessionModel param);
    }
}
