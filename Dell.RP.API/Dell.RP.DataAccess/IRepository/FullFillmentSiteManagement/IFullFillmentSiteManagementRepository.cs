﻿using Dell.RP.API.Dell.RP.Common.Models;
using Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement;
using Dell.RP.DataAccess.IRepository;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.DataAccess.IRepository.FullFillmentSiteManagement
{
    public interface IFullFillmentSiteManagementRepository : IHelper
    {
        Task<List<DropDown>> GetMasterDataData(string key);
        Task<List<DropDown>> GetAccountNameFullfilment();
        Task<List<DropDown>> GetGeoCountries();
        Task<List<DropDown>> GetChannelSegments();
        Task<List<GeoTreeData>> GetGeoTreeData();
        Task<List<ChannelTreeData>> GetChannelTreeData();
        Task<List<ProdTreeData>> GetProductTreeData(string strString);
        Task<List<FhcTreeData>> GetProductFHCTreeData(string strString);
        Task<List<FgaTreeData>> GetProductFGATreeData(string strString);
        Task<DataSet> GetFulFillmentSearchResult(FulFillmentSearchInput searchInput);
        Task<int> DeleteRoutingDetails(string singleDeleteFlag, string ID);
        Task<int> DeleteRoutingDetailsHeader(string singleDeleteFlag, string ID);
        Task<DataSet> GetFulfillmentRetrive(FulFillmentSearchInput retrieveInput);
        Task<List<string>> GetChannelType(string channelid);
        string getRegionName(string country);
        List<string> GetNonVirtualSites(string regionCode);
        DataTable GetFSAFiscalCalendarInfo_BeginDate();
        DataTable GetFSAFiscalCalendarInfo_EndDate();
        bool IsVirtualSite(string siteName);
        DataTable retrieveRoutingDetailsByRoutingId(string routingID);
        DataTable retrieveRoutingDetails(FulFillmentSearchInput retrieveInput);
        bool InsertValidateEffectiveDates(FullFillmentRuleData FSAEntity);
        int AddRoutingdetails(FullFillmentRuleData Full);
        int AddFullFillmentsites(FullFillmentRuleData itFC);
        bool UpdateValidateEffectiveDates(UpdateValidateDateInputParams lstUpdateValidateParams);
        int updatefulfillment(FullFillmentRuleData updateFulfillment);
        int DeleteFulfillDetails(FullFillmentRuleData lstFulfillDeleteSite);
    }
}
