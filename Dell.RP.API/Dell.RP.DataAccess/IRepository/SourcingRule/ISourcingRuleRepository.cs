﻿using Dell.RP.API.Dell.RP.Common.Models;
using Dell.RP.API.Dell.RP.Common.Models.SourcingRule;
using Dell.RP.API.Dell.RP.Common.Models.SourcingRule1;
using Dell.RP.DataAccess.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.DataAccess.IRepository.SourcingRule1
{
    public interface ISourcingRuleRepository: IHelper
    {
        Task<List<DropDown>> GetChannelProducts();
        Task<List<DropDown>> GetChannelGeo();
        Task<List<ProductTree>> GetProductTreeData(string strString);
        Task<List<Site>> GetSites();
        Task<List<SourcingRuleGrid>> GetSourcingRuleDetails(SourcingRuleFilter sourcingRuleFilter);
        Task<Dictionary<string, SourcingRuleLineItem>> GetSourcingRuleLineItems(string startChannelId, string startProductId);
        Task<List<SourcingRule>> GetSourcingRules(string parentProductSearchFilter, string parentChannelSearchFilter);
        Task<Dictionary<string, SourcingRuleLineItem>> GetSingleRuleLineItems(string ChannelId, string ProductId);
        Task<List<SourcingRule>> GetAllSourcingRules();
        Task<int> DeleteSourcingRuleCollection(string productId, string channelId);

        Task<int> AddSourcingRuleCollection(SourcingRuleSaveClass ruleToSave);
    }
}
