﻿using Dell.RP.API.Dell.RP.Common.DemandSplit;
using Dell.RP.API.Dell.RP.Common.Models.DemandSplit;
using Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.Common.Models.RoutingRules;
using Dell.RP.DataAccess.IRepository;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.DataAccess.IRepository.RoutingRules
{
    public interface IRoutingRulesRepository : IHelper
    {
        // public string UserID { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }


        Task<List<ItemType>> GetGeographyData();
        Task<List<ItemType>> GetFulfilment();

        Task<List<ItemType>> GetDate();
        Task<List<ItemType>> GetFGA();
        Task<List<ItemType>> GetEffectiveEndDate();

        


        Task<List<ItemType>> GetManufacturingSiteData( string fulfillment);

        DataTable searchRoutingRules(string ID, string type, string strRegion);

        DataTable GetRoutingRuladdupdateGrid(string strRoutingId, string strFulfillmentSiteID);

        bool DeleteRoutingRules(List<ItemType> sitesvalue);


        Task<List<ItemType>> GetRoutingDropdownvalue( string ItemTypeId, string ItemTypeName);
        DataTable searchRoutings(string strProductId, string strFgaId, out string strType);


        bool ValidateRoutingDates(string oldRoutingStartDate, string oldRoutingEndDate, string newRoutingStartDate, string newRoutingEndDate);

        bool addRoutingRule(RoutingRule routingRule, string updatedBy);

        string CopyRoutingRule(string copyingRoutingId, string newRoutingId, string itemType, string itemId, string user);
    }
}
