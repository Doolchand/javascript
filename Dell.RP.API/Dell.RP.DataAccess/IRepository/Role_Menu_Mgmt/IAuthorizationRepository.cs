﻿using Dell.RP.Common.Models.Role_Menu_Mgmt;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Dell.RP.DataAccess.IRepository.Role_Menu_Mgmt
{
    public interface IAuthorizationRepository : IHelper
    {
      Task<DataTable> GetRoleList(string userType);
      Task<DataTable> GetUserGroupList();
      Task<List<string>> GetRegionList(); // Not find
      Task<DataTable> GetUserNameList(string tenant, List<AuthList> region);
      Task<DataTable> GetUserRoles();
      Task<DataTable> GetUserDetails(FilterDetails filters);
      Task<string> AddUsers(UserDetails user);
      Task<string> UpdateUsers(List<UserDetails> user);
      Task<DataTable> GetScreenAuthorizationList();
      Task<DataTable> GetUsertype();
      Task<DataTable> GetLOB();
      Task<DataTable> GetLoggedInUserRegionBasedOnRoleAsync(LoggedUserDetails user);
      Task<string> GetRoleBasedRegion(string screenId, string userTyp);
     Task<DataTable> GetRolesCanAccessScreenList();

    }
}
