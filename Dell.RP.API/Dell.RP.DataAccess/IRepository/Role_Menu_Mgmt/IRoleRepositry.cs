﻿using Dell.RP.Common.Models.Role_Menu_Mgmt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.DataAccess.IRepository.Role_Menu_Mgmt
{
    public interface IRoleRepositry : IHelper
    {
        Task<List<Role>> GetRoles();
    }
}
