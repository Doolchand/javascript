﻿using Dell.RP.API.Dell.RP.Common.Models;
using Dell.RP.API.Dell.RP.Common.Models.SupplyPlanningFhc;
using Dell.RP.DataAccess.IRepository;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.DataAccess.IRepository.SupplyPlanningFhc
{
    public interface ISupplyPlanningFhcRepository : IHelper
    {
        Task<List<DropDown>> GetManSite();

        string GetRPStatus();

        List<DropDown> BindPOUILock();

        Task<List<Get_SupplyPlanningFhc_Export_Details>> GetRPSupplyPlanningFhcExportDetails(Export_Parameters exportParams);

        Task<List<DropDown>> GetFamilyParentDD();

        string InsertFHC(List<RPSupplyPlan_Import_Params> importData);

        List<Error_Grid_Details> getErrGridDetails();

        string GetLockStatusFHC(string flag);

        string GetValidRecStatusFHC();

        int UpdateAutoUnlockUIFHC();
    }
}
