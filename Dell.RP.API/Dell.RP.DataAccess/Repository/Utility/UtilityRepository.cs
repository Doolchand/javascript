﻿using Dell.RP.DataAccess.IRepository.Utility;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.DataAccess.Common;

namespace Dell.RP.DataAccess.Repository.Utility
{
    public class UtilityRepository : IUtilityRepository
    {
        private readonly string connenctionString;
        private readonly string schemaName;

        public UtilityRepository(IOptions<AppSettings> appSettings, ICommonData commonData)
        {
            connenctionString = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringSCDH, appSettings.Value.ConnectionKey);
            schemaName = appSettings.Value.SchemaName;
        }
        public string UserID { get; set; }

        //public string UserID
        //{
        //    get
        //    {
        //        throw new NotImplementedException();
        //    }

        //    set
        //    {
        //        throw new NotImplementedException();
        //    }
        //}

        public async Task<DataTable> GetAttributeNames(Int32 dimention,string userId)
        {
            //     PROCEDURE P_GET_HIERARCHY_ATTRIBUTES (
            //P_I_NT_USER_V             IN    VARCHAR2,
            //P_I_DIMENTION_N           IN    NUMBER,
            //P_O_ATTRIBUTES_C          OUT SYS_REFCURSOR,
            //P_O_ERROR_CODE_V         OUT VARCHAR2,
            //P_O_ERROR_MESSAGE_V      OUT VARCHAR2) 

            DataTable dtAttributeNames;
            return await Task.Run(() =>
            {
               
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();
                        var query = schemaName + ".E2OPEN_ATTRIBUTE_MNT_UPP_PKG.P_GET_HIERARCHY_ATTRIBUTES_UPP";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam1 = new OracleParameter();
                        objparam1.ParameterName = "P_I_NT_USER_V";
                        objparam1.Value = userId;
                        objparam1.OracleDbType = OracleDbType.Varchar2;
                        objparam1.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "P_I_DIMENTION_N";
                        objparam2.Value = dimention;
                        objparam2.OracleDbType = OracleDbType.Int32;
                        objparam2.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam2);

                        OracleParameter objparam = new OracleParameter();
                        objparam.ParameterName = "P_O_ATTRIBUTES_C";
                        objparam.OracleDbType = OracleDbType.RefCursor;
                        objparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam);

                        OracleParameter objparam3 = new OracleParameter();
                        objparam3.ParameterName = "P_O_ERROR_CODE_V";
                        objparam3.OracleDbType = OracleDbType.Varchar2;
                        objparam3.Size = 30000;
                        objparam3.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam3);

                        OracleParameter objparam4 = new OracleParameter();
                        objparam4.ParameterName = "P_O_ERROR_MESSAGE_V";
                        objparam4.OracleDbType = OracleDbType.Varchar2;
                        objparam4.Size = 30000;
                        objparam4.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam4);                        

                        OracleDataReader readerAttributeNames = cmd.ExecuteReader();
                        var adtprAttributeNames = new OracleDataAdapter(cmd);
                        dtAttributeNames = new DataTable();
                        adtprAttributeNames.Fill(dtAttributeNames);
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return dtAttributeNames;
            });






            //return await Task.Run(() =>
            //{
            //    List<Attributes> lstAttribute = new List<Attributes>();
            //    try
            //    {
            //        Attributes attribute = new Attributes();
            //        attribute.AttributeCode = "ATT-Code1";
            //        attribute.AttributeName = "ATT-Name1";
            //        lstAttribute.Add(attribute);

            //        Attributes attribute1 = new Attributes();
            //        attribute1.AttributeCode = "ATT-Code2";
            //        attribute1.AttributeName = "ATT-Name2";
            //        lstAttribute.Add(attribute1);

            //        Attributes attribute2 = new Attributes();
            //        attribute2.AttributeCode = "ATT-Code3";
            //        attribute2.AttributeName = "ATT-Name3";
            //        lstAttribute.Add(attribute2);
            //    }
            //    catch (Exception ex)
            //    {
            //        var errorMsg = ex.Message;
            //        throw ex;
            //    }

            //    return lstAttribute;
            //});
        }
    }
}
