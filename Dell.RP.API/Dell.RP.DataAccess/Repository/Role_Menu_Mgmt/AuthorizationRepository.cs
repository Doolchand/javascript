﻿using Dell.RP.Common.Models.Role_Menu_Mgmt;
using Dell.RP.DataAccess.IRepository.Role_Menu_Mgmt;
using Microsoft.Extensions.Options;
//using System.Web.Script.Serialization;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.DataAccess.Common;

namespace Dell.RP.DataAccess.Repository.Role_Menu_Mgmt
{
    public class AuthorizationRepository : IAuthorizationRepository
    {
        private readonly string connenctionString;
        private readonly string schemaName;

        public AuthorizationRepository(IOptions<AppSettings> appSettings, ICommonData commonData)
        {
            connenctionString = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringRP, appSettings.Value.ConnectionKey);
            schemaName = appSettings.Value.SchemaName;
        }
        public string UserID { get; set; }

        public async Task<DataTable> GetRoleList(string userType)
        {
            DataTable dtRoleList;
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        var query = schemaName + ".RP_SECURITY_PKG.P_GET_ROLE_LIST";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam3 = new OracleParameter();
                        objparam3.ParameterName = "p_i_nt_user";
                        objparam3.Value = this.UserID;
                        objparam3.OracleDbType = OracleDbType.Varchar2;
                        objparam3.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam3);

                        OracleParameter objparam8 = new OracleParameter();
                        objparam8.ParameterName = "p_i_user_type";
                        objparam8.Value = userType;
                        objparam8.OracleDbType = OracleDbType.Varchar2;
                        objparam8.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam8);

                        OracleParameter objparam = new OracleParameter();
                        objparam.ParameterName = "p_o_cur_roles";
                        objparam.OracleDbType = OracleDbType.RefCursor;
                        objparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam);

                        OracleParameter objparam1 = new OracleParameter();
                        objparam1.ParameterName = "p_o_err_code";
                        objparam1.OracleDbType = OracleDbType.Varchar2;
                        objparam1.Size = 30000;
                        objparam1.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "p_o_err_msg";
                        objparam2.OracleDbType = OracleDbType.Varchar2;
                        objparam2.Size = 30000;
                        objparam2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam2);

                        OracleDataReader readerDimension = cmd.ExecuteReader();
                        var adtprRoleList = new OracleDataAdapter(cmd);
                        dtRoleList = new DataTable();
                        adtprRoleList.Fill(dtRoleList);
                        connection.Close();

                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return dtRoleList;
            });
        }
        public async Task<DataTable> GetUserGroupList()
        {
            DataTable dtUserGroupList;
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        var query = schemaName + ".RP_SECURITY_PKG.P_GET_USER_GROUP_LIST";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam3 = new OracleParameter();
                        objparam3.ParameterName = "p_i_nt_user";
                        objparam3.Value = this.UserID;
                        objparam3.OracleDbType = OracleDbType.Varchar2;
                        objparam3.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam3);



                        OracleParameter objparam = new OracleParameter();
                        objparam.ParameterName = "p_o_cur_user_grp";
                        objparam.OracleDbType = OracleDbType.RefCursor;
                        objparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam);

                        OracleParameter objparam1 = new OracleParameter();
                        objparam1.ParameterName = "p_o_err_code";
                        objparam1.OracleDbType = OracleDbType.Varchar2;
                        objparam1.Size = 30000;
                        objparam1.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "p_o_err_msg";
                        objparam2.OracleDbType = OracleDbType.Varchar2;
                        objparam2.Size = 30000;
                        objparam2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam2);

                        /*
                        OracleDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            list.Add(new AuthList { id = reader[0].ToString(), itemName = reader[1].ToString() });
                        }
                        connection.Close();
                        */
                        OracleDataReader readerUserGroupList = cmd.ExecuteReader();
                        var adtprUserGroupList = new OracleDataAdapter(cmd);
                        dtUserGroupList = new DataTable();
                        adtprUserGroupList.Fill(dtUserGroupList);
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return dtUserGroupList;
            });
        }
        public async Task<List<string>> GetRegionList()
        {
            List<string> list = new List<string>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        var query = schemaName + ".RP_SECURITY_PKG.P_GET_REGION_LIST";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam3 = new OracleParameter();
                        objparam3.ParameterName = "p_i_nt_user";
                        objparam3.Value = this.UserID;
                        objparam3.OracleDbType = OracleDbType.Varchar2;
                        objparam3.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam3);



                        OracleParameter objparam = new OracleParameter();
                        objparam.ParameterName = "p_cur_region";
                        objparam.OracleDbType = OracleDbType.RefCursor;
                        objparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam);

                        OracleParameter objparam1 = new OracleParameter();
                        objparam1.ParameterName = "p_o_err_code";
                        objparam1.OracleDbType = OracleDbType.Varchar2;
                        objparam1.Size = 30000;
                        objparam1.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "p_o_err_msg";
                        objparam2.OracleDbType = OracleDbType.Varchar2;
                        objparam2.Size = 30000;
                        objparam2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam2);

                        OracleDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            list.Add(reader[0].ToString());
                        }
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return list;
            });
        }
        public async Task<DataTable> GetUserNameList(string tenant, List<AuthList> region)
        {
            DataTable dtUserNameList;
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        var query = schemaName + ".RP_SECURITY_PKG.P_GET_USER_NAME_LIST";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam3 = new OracleParameter();
                        objparam3.ParameterName = "p_i_nt_user";
                        objparam3.Value = this.UserID;
                        objparam3.OracleDbType = OracleDbType.Varchar2;
                        objparam3.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam3);

                        OracleParameter ONArray = new OracleParameter("p_i_region", OracleDbType.Varchar2, ParameterDirection.Input);
                        ONArray.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                        ONArray.Value = region != null && region.Count > 0 ? region.Select(x => x.id).ToArray() : new string[1] { "" };
                        cmd.Parameters.Add(ONArray);

                        OracleParameter objparam5 = new OracleParameter();
                        objparam5.ParameterName = "p_i_user_type";
                        objparam5.Value = tenant;
                        objparam5.OracleDbType = OracleDbType.Varchar2;
                        objparam5.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam5);

                        OracleParameter objparam = new OracleParameter();
                        objparam.ParameterName = "p_cur_username";
                        objparam.OracleDbType = OracleDbType.RefCursor;
                        objparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam);

                        OracleParameter objparam1 = new OracleParameter();
                        objparam1.ParameterName = "p_o_err_code";
                        objparam1.OracleDbType = OracleDbType.Varchar2;
                        objparam1.Size = 30000;
                        objparam1.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "p_o_err_msg";
                        objparam2.OracleDbType = OracleDbType.Varchar2;
                        objparam2.Size = 30000;
                        objparam2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam2);

                        /*
                        OracleDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            list.Add(new AuthList { id = reader["USER_ID"].ToString(), itemName = reader["USER_NAME"].ToString() });
                        }
                        connection.Close();
                        */

                        OracleDataReader readerUserNameList = cmd.ExecuteReader();
                        var adtprUserNameList = new OracleDataAdapter(cmd);
                        dtUserNameList = new DataTable();
                        adtprUserNameList.Fill(dtUserNameList);
                        connection.Close();
                        
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return dtUserNameList;
            });
        }
        public async Task<DataTable> GetUserRoles()
        {
            DataTable dtUserRoles;
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        var query = schemaName + ".RP_SECURITY_PKG.P_GET_USER_ROLES ";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam3 = new OracleParameter();
                        objparam3.ParameterName = "p_i_nt_user";
                        objparam3.Value = this.UserID;
                        objparam3.OracleDbType = OracleDbType.Varchar2;
                        objparam3.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam3);

                        OracleParameter objparam = new OracleParameter();
                        objparam.ParameterName = "p_cur_user_roles";
                        objparam.OracleDbType = OracleDbType.RefCursor;
                        objparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam);

                        OracleParameter objparam1 = new OracleParameter();
                        objparam1.ParameterName = "p_o_err_code";
                        objparam1.OracleDbType = OracleDbType.Varchar2;
                        objparam1.Size = 30000;
                        objparam1.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "p_o_err_msg";
                        objparam2.OracleDbType = OracleDbType.Varchar2;
                        objparam2.Size = 30000;
                        objparam2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam2);

                        OracleDataReader readerUserRoles = cmd.ExecuteReader();
                        var adtprUserRoles = new OracleDataAdapter(cmd);
                        dtUserRoles = new DataTable();
                        adtprUserRoles.Fill(dtUserRoles);
                        connection.Close();
                        
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return dtUserRoles;
            });
        }
        public async Task<DataTable> GetUserDetails(FilterDetails filters)
        {
            DataTable dtUserDetails;
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        var query = schemaName + ".RP_SECURITY_PKG.P_GET_USER_DETAILS";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam3 = new OracleParameter();
                        objparam3.ParameterName = "p_i_nt_user";
                        objparam3.Value = this.UserID;
                        objparam3.OracleDbType = OracleDbType.Varchar2;
                        objparam3.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam3);

                        OracleParameter objparam4 = new OracleParameter("p_i_region", OracleDbType.Varchar2, ParameterDirection.Input);
                        objparam4.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                        objparam4.Value = filters.Region != null && filters.Region.Count > 0 ? filters.Region.Select(x => x.id).ToArray() : new string[1] { "" };
                        cmd.Parameters.Add(objparam4);

                        OracleParameter ONArray = new OracleParameter("p_i_user_name", OracleDbType.Varchar2, ParameterDirection.Input);
                        ONArray.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                        ONArray.Value = filters.UserName != null && filters.UserName.Count > 0 ? filters.UserName.Select(x => x.itemName).ToArray() : new string[1] { "" };
                        cmd.Parameters.Add(ONArray);

                        OracleParameter objparam5 = new OracleParameter("p_i_user_group", OracleDbType.Varchar2, ParameterDirection.Input);
                        objparam5.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                        objparam5.Value = filters.UserGroup != null && filters.UserGroup.Count > 0 ? filters.UserGroup.Select(x => x.itemName).ToArray() : new string[1] { "" };
                        cmd.Parameters.Add(objparam5);

                        OracleParameter objparam6 = new OracleParameter("p_i_is_active", OracleDbType.Varchar2, ParameterDirection.Input);
                        objparam6.Value = filters.IsActive;
                        cmd.Parameters.Add(objparam6);

                        OracleParameter objparam7 = new OracleParameter("p_i_user_type", OracleDbType.Varchar2, ParameterDirection.Input);
                        objparam7.Value = filters.UserType;
                        cmd.Parameters.Add(objparam7);

                        OracleParameter objparam = new OracleParameter();
                        objparam.ParameterName = "p_cur_user_details";
                        objparam.OracleDbType = OracleDbType.RefCursor;
                        objparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam);

                        OracleParameter objparam1 = new OracleParameter();
                        objparam1.ParameterName = "p_o_err_code";
                        objparam1.OracleDbType = OracleDbType.Varchar2;
                        objparam1.Size = 30000;
                        objparam1.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "p_o_err_msg";
                        objparam2.OracleDbType = OracleDbType.Varchar2;
                        objparam2.Size = 30000;
                        objparam2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam2);

                        OracleDataReader readerUserDetails = cmd.ExecuteReader();
                        var adtprUserDetails = new OracleDataAdapter(cmd);
                        dtUserDetails = new DataTable();
                        adtprUserDetails.Fill(dtUserDetails);
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return dtUserDetails;
            });
        }

        public async Task<string> AddUsers(UserDetails user)
        {
            string returnMessage = "";
            return await Task.Run(() =>
            {
                try
                {
                    var query = schemaName + ".RP_SECURITY_PKG.P_ADD_USERS";
                    var connection = new OracleConnection(connenctionString);
                    connection.Open();
                    OracleCommand cmd = new OracleCommand(query, connection);

                    cmd.CommandType = CommandType.StoredProcedure;

                    OracleParameter objparam = new OracleParameter("p_i_nt_user", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam.Value = this.UserID;
                    objparam.Size = 300;
                    cmd.Parameters.Add(objparam);

                    OracleParameter objparam1 = new OracleParameter("p_i_user_name", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam1.Value = user.UserName;
                    objparam1.Size = 300;
                    cmd.Parameters.Add(objparam1);

                    OracleParameter objparam2 = new OracleParameter("p_i_region", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam2.Value = user.Region;
                    objparam2.Size = 300;
                    cmd.Parameters.Add(objparam2);

                    OracleParameter objparam3 = new OracleParameter("p_i_user_domain", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam3.Value = user.UserDomain;
                    objparam3.Size = 300;
                    cmd.Parameters.Add(objparam3);


                    OracleParameter objparam7 = new OracleParameter("p_i_user_email", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam7.Value = user.UserEmail;
                    objparam7.Size = 300;
                    cmd.Parameters.Add(objparam7);
                    
                    OracleParameter objparam4 = new OracleParameter("p_i_user_group", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam4.Value = user.UserGroup;
                    objparam4.Size = 300;
                    cmd.Parameters.Add(objparam4);

                   
                    OracleParameter UserRole = new OracleParameter("p_i_user_role_array", OracleDbType.Varchar2, ParameterDirection.Input);
                    UserRole.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                    UserRole.Value = user != null ? user.UserRole.Split(',').ToArray() : new string [1] { "" };
                    UserRole.Size = 300;
                    cmd.Parameters.Add(UserRole);

                    OracleParameter objparam5 = new OracleParameter("p_i_active_array", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam5.Value = user.IsActive ? "Y" : "N";
                    objparam5.Size = 300;
                    cmd.Parameters.Add(objparam5);

                    OracleParameter objparam6 = new OracleParameter("p_i_user_type", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam6.Value = user.UserType;
                    objparam6.Size = 300;
                    cmd.Parameters.Add(objparam6);

                    OracleParameter lob = new OracleParameter("p_i_lob", OracleDbType.Varchar2, ParameterDirection.Input);
                    lob.Value = user.Lobs;
                    lob.Size = 300;
                    cmd.Parameters.Add(lob);

                    OracleParameter errorNum = new OracleParameter("p_o_err_code", OracleDbType.Varchar2, ParameterDirection.Output);
                    errorNum.Size = 5000;
                    cmd.Parameters.Add(errorNum);

                    OracleParameter errorMsg = new OracleParameter("p_o_err_msg", OracleDbType.Varchar2, ParameterDirection.Output);
                    errorMsg.Size = 5000;
                    cmd.Parameters.Add(errorMsg);

                    

                    cmd.ExecuteNonQuery();
                    returnMessage = cmd.Parameters["p_o_err_code"].Value.ToString() + "~" + cmd.Parameters["p_o_err_msg"].Value.ToString();
                    connection.Close();

                }
                catch (OracleException ex)
                {
                    Console.WriteLine("oracle exception message");
                    Console.WriteLine("exception message: " + ex.Message);
                    Console.WriteLine("exception source: " + ex.Source);
                }

                catch (Exception ex)
                {
                    var errormsg = ex.Message;
                    throw ex;
                }
                return returnMessage;
            });
        }
        public async Task<string> UpdateUsers(List<UserDetails> user)
        {
            string returnMessage = "";
            return await Task.Run(() =>
            {
                try
                {
                    var query = schemaName + ".RP_SECURITY_PKG.P_UPDATE_USERS";
                    var connection = new OracleConnection(connenctionString);
                    connection.Open();
                    OracleCommand cmd = new OracleCommand(query, connection);

                    cmd.CommandType = CommandType.StoredProcedure;

                    OracleParameter objparam = new OracleParameter("p_i_nt_user", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam.Value = this.UserID;
                    objparam.Size = 300;
                    cmd.Parameters.Add(objparam);

                    OracleParameter objparam0 = new OracleParameter("p_i_user_id", OracleDbType.Int32, ParameterDirection.Input);
                    objparam0.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                    objparam0.Value = user != null && user.Count > 0 ? user.Select(x => int.Parse( x.UserId)).ToArray() : new int[1] { -1 };
                    objparam0.Size = 300;
                    cmd.Parameters.Add(objparam0);


                    OracleParameter objparam1 = new OracleParameter("p_i_user_email", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam1.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                    objparam1.Value = user != null && user.Count > 0 ? user.Select(x => x.UserEmail).ToArray() : new string[1] { "" };
                    objparam1.Size = 300;
                    cmd.Parameters.Add(objparam1);

                    OracleParameter objparam2 = new OracleParameter("p_i_user_role_array", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam2.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                    objparam2.Value = user != null && user.Count > 0 ? user.Select(x => x.UserRole.ToString()).ToArray() : new string[1] { "" };
                    objparam2.Size = 300;
                    cmd.Parameters.Add(objparam2);

                    OracleParameter isActive = new OracleParameter("p_i_active_array", OracleDbType.Varchar2, ParameterDirection.Input);
                    isActive.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                    isActive.Value = user != null && user.Count > 0 ? user.Select(x => x.IsActive ? "Y" : "N").ToArray() : new string[1] { "" };
                    isActive.Size = 300;
                    cmd.Parameters.Add(isActive);

                    OracleParameter userType = new OracleParameter("p_i_user_type", OracleDbType.Varchar2, ParameterDirection.Input);
                    userType.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                    userType.Value = user != null && user.Count > 0 ? user.Select(x => x.UserType.ToString()).ToArray() : new string[1] { "" };
                    userType.Size = 300;
                    cmd.Parameters.Add(userType);

                    OracleParameter lob = new OracleParameter("p_i_lob", OracleDbType.Varchar2, ParameterDirection.Input);
                    lob.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                    lob.Value = user != null && user.Count > 0 ? user.Select(x => x.Lobs.ToString()).ToArray() : new string[1] { "" };
                    lob.Size = 300;
                    cmd.Parameters.Add(lob);

                    OracleParameter errorNum = new OracleParameter("p_o_err_code", OracleDbType.Varchar2, ParameterDirection.Output);
                    errorNum.Size = 5000;
                    cmd.Parameters.Add(errorNum);

                    OracleParameter errorMsg = new OracleParameter("p_o_err_msg", OracleDbType.Varchar2, ParameterDirection.Output);
                    errorMsg.Size = 5000;
                    cmd.Parameters.Add(errorMsg);

                    cmd.ExecuteNonQuery();
                    returnMessage = cmd.Parameters["p_o_err_code"].Value.ToString() + "~" + cmd.Parameters["p_o_err_msg"].Value.ToString();
                    connection.Close();

                }
                catch (OracleException ex)
                {
                    Console.WriteLine("oracle exception message");
                    Console.WriteLine("exception message: " + ex.Message);
                    Console.WriteLine("exception source: " + ex.Source);
                }

                catch (Exception ex)
                {
                    var errormsg = ex.Message;
                    throw ex;
                }
                return returnMessage;
            });
        }

        public async Task<DataTable> GetScreenAuthorizationList()
        {
            DataTable dtScreenList;
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        var query = schemaName + ".RP_SECURITY_PKG.P_GET_ROLE_FUNCTION";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam3 = new OracleParameter();
                        objparam3.ParameterName = "p_i_nt_user";
                        objparam3.Value = this.UserID;
                        objparam3.OracleDbType = OracleDbType.Varchar2;
                        objparam3.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam3);


                        OracleParameter objparam = new OracleParameter();
                        objparam.ParameterName = "p_cur_role_functions";
                        objparam.OracleDbType = OracleDbType.RefCursor;
                        objparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam);

                        OracleParameter objparam1 = new OracleParameter();
                        objparam1.ParameterName = "p_o_err_code";
                        objparam1.OracleDbType = OracleDbType.Varchar2;
                        objparam1.Size = 30000;
                        objparam1.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "p_o_err_msg";
                        objparam2.OracleDbType = OracleDbType.Varchar2;
                        objparam2.Size = 30000;
                        objparam2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam2);

                        OracleDataReader readerScreenList = cmd.ExecuteReader();
                        var adtprScreenList = new OracleDataAdapter(cmd);
                        dtScreenList = new DataTable();
                        adtprScreenList.Fill(dtScreenList);
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return dtScreenList;
            });
        }      
        public async Task<DataTable> GetLoggedInUserRegionBasedOnRoleAsync(LoggedUserDetails user)
        {
            DataTable dtRegionBasedOnRole = new DataTable(); 
            return await Task.Run(() =>
            {
                try
                {
                    var query = schemaName + ".RP_SECURITY_PKG.P_GET_ROLE_BASED_REGION";
                    var connection = new OracleConnection(connenctionString);
                    connection.Open();
                    OracleCommand cmd = new OracleCommand(query, connection);

                    cmd.CommandType = CommandType.StoredProcedure;

                    OracleParameter objparam1 = new OracleParameter("p_i_nt_user", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam1.Value = user.UserId;
                    objparam1.Size = 300;
                    cmd.Parameters.Add(objparam1);

                    OracleParameter objparam = new OracleParameter();
                    objparam.ParameterName = "p_o_region";
                    objparam.OracleDbType = OracleDbType.RefCursor;
                    objparam.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(objparam);


                    OracleParameter errorNum = new OracleParameter("p_o_err_code", OracleDbType.Varchar2, ParameterDirection.Output);
                    errorNum.Size = 5000;
                    cmd.Parameters.Add(errorNum);

                    OracleParameter errorMsg = new OracleParameter("p_o_err_msg", OracleDbType.Varchar2, ParameterDirection.Output);
                    errorMsg.Size = 5000;
                    cmd.Parameters.Add(errorMsg);

                    OracleDataReader readerRegionBasedOnRole = cmd.ExecuteReader();
                    var adtprRegionBasedOnRole = new OracleDataAdapter(cmd);                    
                    adtprRegionBasedOnRole.Fill(dtRegionBasedOnRole);
                    connection.Close();
                }
                catch (OracleException ex)
                {
                    Console.WriteLine("oracle exception message");
                    Console.WriteLine("exception message: " + ex.Message);
                    Console.WriteLine("exception source: " + ex.Source);
                }

                catch (Exception ex)
                {
                    // todo: need to log the exception.
                    var errormsg = ex.Message;
                    throw ex;

                }
                return dtRegionBasedOnRole;
            });

        }
        public async Task<DataTable> GetUsertype()
        {
            DataTable dtUsertype;
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        var query = schemaName + ".RP_SECURITY_PKG.P_GET_TENANT_DETAILS";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam3 = new OracleParameter();
                        objparam3.ParameterName = "p_i_nt_user";
                        objparam3.Value = this.UserID;
                        objparam3.OracleDbType = OracleDbType.Varchar2;
                        objparam3.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam3);



                        OracleParameter objparam = new OracleParameter();
                        objparam.ParameterName = "p_o_tenant";
                        objparam.OracleDbType = OracleDbType.RefCursor;
                        objparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam);

                        OracleParameter objparam1 = new OracleParameter();
                        objparam1.ParameterName = "p_o_err_code";
                        objparam1.OracleDbType = OracleDbType.Varchar2;
                        objparam1.Size = 30000;
                        objparam1.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "p_o_err_msg";
                        objparam2.OracleDbType = OracleDbType.Varchar2;
                        objparam2.Size = 30000;
                        objparam2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam2);

                        OracleDataReader readerUsertype = cmd.ExecuteReader();
                        var adtprUsertype = new OracleDataAdapter(cmd);
                        dtUsertype = new DataTable();
                        adtprUsertype.Fill(dtUsertype);
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return dtUsertype;
            });
        }

        public async Task<DataTable> GetLOB()
        {
            DataTable dtLOB;
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        var query = schemaName + ".RP_LOB_Mnt_Pkg.P_GET_LOB";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam3 = new OracleParameter();
                        objparam3.ParameterName = "p_i_nt_user";
                        objparam3.Value = this.UserID;
                        objparam3.OracleDbType = OracleDbType.Varchar2;
                        objparam3.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam3);

                        OracleParameter objparam = new OracleParameter();
                        objparam.ParameterName = "p_o_lob";
                        objparam.OracleDbType = OracleDbType.RefCursor;
                        objparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam);

                        OracleParameter objparam1 = new OracleParameter();
                        objparam1.ParameterName = "p_o_err_code";
                        objparam1.OracleDbType = OracleDbType.Varchar2;
                        objparam1.Size = 30000;
                        objparam1.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "p_o_err_msg";
                        objparam2.OracleDbType = OracleDbType.Varchar2;
                        objparam2.Size = 30000;
                        objparam2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam2);

                        OracleDataReader readerLOB = cmd.ExecuteReader();
                        var adtprLOB = new OracleDataAdapter(cmd);
                        dtLOB = new DataTable();
                        adtprLOB.Fill(dtLOB);
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return dtLOB;
            });
        }
        public async Task<string> GetRoleBasedRegion(string screenId, string userType)
        {
            string returnMessage = "";
            return await Task.Run(() =>
            {
                try
                {
                    var query = schemaName + ".RP_SECURITY_PKG.P_GET_REGION_ACCESS";
                    var connection = new OracleConnection(connenctionString);
                    connection.Open();
                    OracleCommand cmd = new OracleCommand(query, connection);

                    cmd.CommandType = CommandType.StoredProcedure;

                    OracleParameter objparam1 = new OracleParameter("p_i_nt_user_id", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam1.Value = this.UserID;
                    objparam1.Size = 300;
                    cmd.Parameters.Add(objparam1);

                    OracleParameter objparam2 = new OracleParameter("p_screen_id", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam2.Value = screenId;
                    objparam2.Size = 300;
                    cmd.Parameters.Add(objparam2);

                    OracleParameter outregion = new OracleParameter("p_region_id", OracleDbType.Varchar2, ParameterDirection.Output);
                    outregion.Size = 5000;
                    cmd.Parameters.Add(outregion);

                    OracleParameter errorNum = new OracleParameter("p_o_err_code", OracleDbType.Varchar2, ParameterDirection.Output);
                    errorNum.Size = 5000;
                    cmd.Parameters.Add(errorNum);

                    OracleParameter errorMsg = new OracleParameter("p_o_err_msg", OracleDbType.Varchar2, ParameterDirection.Output);
                    errorMsg.Size = 5000;
                    cmd.Parameters.Add(errorMsg);

                    OracleParameter objparam3 = new OracleParameter("P_USER_TYPE", OracleDbType.Varchar2, ParameterDirection.Input);
                    objparam3.Value = userType;
                    objparam3.Size = 300;
                    cmd.Parameters.Add(objparam3);

                    cmd.ExecuteNonQuery();
                    returnMessage = cmd.Parameters["p_region_id"].Value.ToString();
                    
                  
                    connection.Close();

                }
                catch (OracleException ex)
                {
                    Console.WriteLine("oracle exception message");
                    Console.WriteLine("exception message: " + ex.Message);
                    Console.WriteLine("exception source: " + ex.Source);
                }

                catch (Exception ex)
                {
                    // todo: need to log the exception.
                    var errormsg = ex.Message;
                    throw ex;

                }
                return returnMessage;
            });
        }

        /// <summary>
        /// Get Roles can acess screen
        /// </summary>
        /// <returns></returns>
        public async Task<DataTable> GetRolesCanAccessScreenList()
        {
            DataTable dtAccessScreenList;
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        var query = schemaName + ".RP_SECURITY_PKG.P_GET_SCREEN_ACCESS";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam3 = new OracleParameter();
                        objparam3.ParameterName = "p_i_nt_user";
                        objparam3.Value = this.UserID;
                        objparam3.OracleDbType = OracleDbType.Varchar2;
                        objparam3.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam3);



                        OracleParameter objparam = new OracleParameter();
                        objparam.ParameterName = "p_o_screen_id";
                        objparam.OracleDbType = OracleDbType.RefCursor;
                        objparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam);

                        OracleParameter objparam1 = new OracleParameter();
                        objparam1.ParameterName = "p_o_err_code";
                        objparam1.OracleDbType = OracleDbType.Varchar2;
                        objparam1.Size = 30000;
                        objparam1.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "p_o_err_msg";
                        objparam2.OracleDbType = OracleDbType.Varchar2;
                        objparam2.Size = 30000;
                        objparam2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam2);

                        OracleDataReader readerAccessScreenList = cmd.ExecuteReader();
                        var adtprAccessScreenList = new OracleDataAdapter(cmd);
                        dtAccessScreenList = new DataTable();
                        adtprAccessScreenList.Fill(dtAccessScreenList);
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return dtAccessScreenList;
            });
        }

    }
}

