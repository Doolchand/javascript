﻿using Dell.RP.DataAccess.IRepository.Role_Menu_Mgmt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dell.RP.Common.Models.Role_Menu_Mgmt;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dell.RP.API.Dell.RP.DataAccess.Common;

namespace Dell.RP.DataAccess.Repository.Role_Menu_Mgmt
{
    public class MenuRepositry : IMenuRepositry
    {
        public string UserID { get; set; }
        private readonly string connenctionString;
        private readonly string schemaName;

        public MenuRepositry(IOptions<AppSettings> appSettings, ICommonData commonData)
        {
            connenctionString = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringSCDH, appSettings.Value.ConnectionKey);
            schemaName = appSettings.Value.SchemaName;
        }


        public async Task<List<Menu>> GetMenus()
        {
            return await Task.Run(() =>
            {
                List<Menu> menus = new List<Menu>();
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        var query = schemaName + ".E2OPEN_FRAMEWORK_PKG.P_GET_MENU";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam3 = new OracleParameter();
                        objparam3.ParameterName = "p_i_nt_user";
                        objparam3.Value = "VELU";//this.UserID;
                        objparam3.OracleDbType = OracleDbType.Varchar2;
                        objparam3.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam3);



                        OracleParameter objparam = new OracleParameter();
                        objparam.ParameterName = "p_o_main_menus";
                        objparam.OracleDbType = OracleDbType.RefCursor;
                        objparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam);

                        OracleParameter objparam1 = new OracleParameter();
                        objparam1.ParameterName = "p_o_err_code";
                        objparam1.OracleDbType = OracleDbType.Varchar2;
                        objparam1.Size = 30000;
                        objparam1.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "p_o_err_msg";
                        objparam2.OracleDbType = OracleDbType.Varchar2;
                        objparam2.Size = 30000;
                        objparam2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam2);

                        //depotDescriptions.Add(new DepotDescription { Depot = "ANVCHA", Description = "ANVCHA AVNET for Compellant" });
                        //depotDescriptions.Add(new DepotDescription { Depot = "APCC", Description = "APCC  Dell Penang Manufacturing site" });
                        //depotDescriptions.Add(new DepotDescription { Depot = "ARC", Description = "ARC  ARC" });
                        //depotDescriptions.Add(new DepotDescription { Depot = "AUGJUA", Description = "Augmentix Juarez Mexico" });
                        OracleDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            menus.Add(new Menu {
                               
                                ParentMenuItem = Int32.Parse(reader[0].ToString()),
                                MenuItem = Int32.Parse(reader[1].ToString()),
                                MenuItemDesc = reader[2].ToString(),
                                MenuItemName = reader[3].ToString(),
                                //  data = new  List<string> { "hshs", "djddj","dkdkd" }
                               //IsAccessAllow = Int32.Parse( reader[4].ToString())
                            });
                        }
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return menus;
            });
        }
    }
}
