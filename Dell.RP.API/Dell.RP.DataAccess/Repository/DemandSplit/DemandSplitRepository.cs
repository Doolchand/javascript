﻿using Dell.RP.DataAccess;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

using Dell.RP.API.Dell.RP.DataAccess.IRepository.DemandSplit;
using Dell.RP.API.Dell.RP.DataAccess.Common;
using Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.Common.Models.DemandSplit;
using Dell.RP.API.Dell.RP.Common.DemandSplit;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Dell.RP.API.Dell.RP.DataAccess.Repository.DemandSplit
{
    public class DemandSplitRepository : IDemandSplitRepository
    {


        private readonly string connenctionString;
        public string UserID { get; set; }

        private readonly string schemaName;

        public DemandSplitRepository(IOptions<AppSettings> appSettings, ICommonData commonData)
        {
            connenctionString = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringSCDH, appSettings.Value.ConnectionKey);
            schemaName = appSettings.Value.SchemaName;
        }



        public async Task<List<ItemType>> getProduct()
        {
            List<ItemType> itemType = new List<ItemType>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();


                        string sql = "select distinct  product_id, nvl( (select name || '(' || product_id || ')' from MST_PRODUCT_MASTER_VW where product_id = D.PRODUCT_ID  ) , "
                             + " (select name || '(' || product_id || ')' from mst_item_vw where item_id = D.PRODUCT_ID)) PRODUCT_NAME "
                            + " from demand_split D order by PRODUCT_NAME";

                        // var query = schemaName + ".E2OPEN_UPP_EMC_PRODUCT_PKG.P_Get_Item_Attribute";
                        OracleCommand cmd = new OracleCommand(sql, connection);
                       // cmd.CommandType = CommandType.StoredProcedure;

                       

                        OracleDataReader reader = cmd.ExecuteReader();
                        itemType.Add(new ItemType { ItemTypeId = "ALL", ItemTypeName = "ALL" });
                        while (reader.Read())
                        {
                            itemType.Add(new ItemType { ItemTypeId = reader[0].ToString(), ItemTypeName = reader[1].ToString() });
                        }

                        
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return itemType;
            });
        }

       public async Task<List<ItemType>> getChennal()
        {
            List<ItemType> itemType = new List<ItemType>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();


                        string sql = "select distinct channel_id,(select distinct CHANNEL_ID || '(' || NAME || ')' from FIN_CHANNEL_MASTER_VW  "
                                        + "where CHANNEL_ID=D.CHANNEL_ID) CHANNEL_NAME from demand_split D order by CHANNEL_NAME";

                        // var query = schemaName + ".E2OPEN_UPP_EMC_PRODUCT_PKG.P_Get_Item_Attribute";
                        OracleCommand cmd = new OracleCommand(sql, connection);
                        // cmd.CommandType = CommandType.StoredProcedure;
 

                        OracleDataReader reader = cmd.ExecuteReader();
                        itemType.Add(new ItemType { ItemTypeId = "ALL", ItemTypeName = "ALL" });
                        while (reader.Read())
                        {
                            itemType.Add(new ItemType { ItemTypeId = reader[0].ToString(), ItemTypeName = reader[1].ToString() });
                        }
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return itemType;
            });
        }




        


         public async Task<List<ItemType>> getGeography()
        {
            List<ItemType> itemType = new List<ItemType>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        string sql = "select distinct geography_id, (select distinct GEO_ID || '(' || NAME || ')' from MST_GEOGRAPHY_VW where GEO_ID=D.GEOGRAPHY_ID) GEOGRAPHY_NAME from demand_split D order by GEOGRAPHY_NAME";
                        OracleCommand cmd = new OracleCommand(sql, connection);
                       
                        OracleDataReader reader = cmd.ExecuteReader();
                        itemType.Add(new ItemType { ItemTypeId = "ALL", ItemTypeName = "ALL" });
                        while (reader.Read())
                        {
                            itemType.Add(new ItemType { ItemTypeId = reader[0].ToString(), ItemTypeName = reader[1].ToString() });
                        }
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return itemType;
            });
        }



        //Task<List<SearchQuery>> GetDemandSplitViewResult(DemandSearch selectedSearch)


        public async Task<List<SearchQuery>> GetDemandSplitViewResult(DemandSearch selectedSearch)
        {
            //List<SearchQuery> itemType = new List<SearchQuery>();


            return await Task.Run(() =>
            {
                try
                {
                    var searchResult = new List<SearchQuery>();

               
                    using (var connection = new OracleConnection(connenctionString))
                    {


                        connection.Open();

                        string strSql = string.Format("SELECT SSC,WK0,WK1,WK2,WK3,WK4,WK5,WK6,WK7,WK8,WK9,WK10,WK11,WK12,WK13,WK14,WK15,WK16,WK17,WK18,WK19,WK20,WK21,WK22,WK23,WK24,WK25,WK26, "

                + " REST_OF_HORIZON, SYS_LAST_MODIFIED_DATE,SYS_LAST_MODIFIED_BY,GEOGRAPHY_ID, "
                + " (select distinct GEO_ID || '(' || NAME || ')' from MST_GEOGRAPHY_VW where GEO_ID=D.GEOGRAPHY_ID) GEOGRAPHY_NAME ,   "
                + " CHANNEL_ID, (select distinct CHANNEL_ID || '(' || NAME || ')' from FIN_CHANNEL_MASTER_VW   "
                + " where CHANNEL_ID=D.CHANNEL_ID) CHANNEL_NAME, PRODUCT_ID,   "
                + " nvl((select name || '(' || product_id || ')' from MST_PRODUCT_MASTER_VW where product_id = D.PRODUCT_ID   "
                + " ),(select name || '(' || product_id || ')' from mst_item_vw where item_id = D.PRODUCT_ID)) PRODUCT_NAME FROM DEMAND_SPLIT D where D.SPLIT_ID = D.SPLIT_ID ");




                        if (selectedSearch.Chennal != "ALL")
                            {
                                strSql += " AND CHANNEL_ID = '" + selectedSearch.Chennal + "'";
                            }

                            if (selectedSearch.Product != "ALL")
                            {

                                strSql += " AND PRODUCT_ID = '" + selectedSearch.Product + "'";
                            }
                            if (selectedSearch.Geography != "ALL")
                            {

                                strSql += " AND GEOGRAPHY_ID = '" + selectedSearch.Geography + "'";
                            }

                        OracleCommand command = new OracleCommand(strSql, connection);
                        var adapter = new OracleDataAdapter(command);
                            var dt = new DataTable();
                            adapter.Fill(dt);

                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    if (dr["GEOGRAPHY_ID"].ToString() == "ALL_GEO")
                                    {
                                        dr["GEOGRAPHY_NAME"] = "ALL_GEO";
                                    }
                                    if (dr["CHANNEL_ID"].ToString() == "ALL_CHANNEL")
                                    {
                                        dr["CHANNEL_NAME"] = "ALL_CHANNEL";
                                    }
                                    if (dr["PRODUCT_ID"].ToString() == "ALL_PRODUCT")
                                    {
                                        dr["PRODUCT_NAME"] = "ALL_PRODUCT";
                                    }
                                }
                            }

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                var output = new SearchQuery();
                                output.Product_ID = String.Empty;
                                if (dt.Rows[i]["PRODUCT_NAME"] != System.DBNull.Value)
                                {
                                    output.Product_ID = Convert.ToString(dt.Rows[i]["PRODUCT_NAME"]);
                                }


                                output.Channel_ID = String.Empty;
                                if (dt.Rows[i]["CHANNEL_NAME"] != System.DBNull.Value)
                                {
                                    output.Channel_ID = Convert.ToString(dt.Rows[i]["CHANNEL_NAME"]);
                                }


                                output.Geography_ID = String.Empty;
                                if (dt.Rows[i]["GEOGRAPHY_NAME"] != System.DBNull.Value)
                                {
                                    output.Geography_ID = Convert.ToString(dt.Rows[i]["GEOGRAPHY_NAME"]);
                                }


                                output.SSC = String.Empty;
                                if (dt.Rows[i]["SSC"] != System.DBNull.Value)
                                {
                                    output.SSC = Convert.ToString(dt.Rows[i]["SSC"]);
                                }

                                if (dt.Rows[i]["WK0"] != System.DBNull.Value)
                                {
                                output.WK0 = dt.Rows[i]["WK0"].ToString();
                                }


                                if (dt.Rows[i]["WK1"] != System.DBNull.Value)
                                {
                                    output.WK1 = dt.Rows[i]["WK1"].ToString();
                            }
                                if (dt.Rows[i]["WK2"] != System.DBNull.Value)
                                {
                                    output.WK2 = dt.Rows[i]["WK2"].ToString();
                            }
                                if (dt.Rows[i]["WK3"] != System.DBNull.Value)
                                {
                                    output.WK3 = dt.Rows[i]["WK3"].ToString();
                            }


                                if (dt.Rows[i]["WK4"] != System.DBNull.Value)
                                {
                                    output.WK4 = dt.Rows[i]["WK4"].ToString();
                            }

                                if (dt.Rows[i]["WK5"] != System.DBNull.Value)
                                {
                                    output.WK5 = dt.Rows[i]["WK5"].ToString();
                            }


                                if (dt.Rows[i]["WK6"] != System.DBNull.Value)
                                {
                                    output.WK6 = dt.Rows[i]["WK6"].ToString();
                            }


                                if (dt.Rows[i]["WK7"] != System.DBNull.Value)
                                {
                                    output.WK7 = dt.Rows[i]["WK7"].ToString();
                            }


                                if (dt.Rows[i]["WK8"] != System.DBNull.Value)
                                {
                                    output.WK8 = dt.Rows[i]["WK8"].ToString();
                            }


                                if (dt.Rows[i]["WK9"] != System.DBNull.Value)
                                {
                                    output.WK9 = dt.Rows[i]["WK9"].ToString();
                            }


                                if (dt.Rows[i]["WK10"] != System.DBNull.Value)
                                {
                                    output.WK10 = dt.Rows[i]["WK10"].ToString();
                            }


                                if (dt.Rows[i]["WK11"] != System.DBNull.Value)
                                {
                                    output.WK11 = dt.Rows[i]["WK11"].ToString();
                            }


                                if (dt.Rows[i]["WK12"] != System.DBNull.Value)
                                {
                                    output.WK12 = dt.Rows[i]["WK12"].ToString();
                            }


                                if (dt.Rows[i]["WK13"] != System.DBNull.Value)
                                {
                                    output.WK13 = dt.Rows[i]["WK13"].ToString();
                            }


                                if (dt.Rows[i]["WK14"] != System.DBNull.Value)
                                {
                                    output.WK14 = dt.Rows[i]["WK14"].ToString();
                            }


                                if (dt.Rows[i]["WK15"] != System.DBNull.Value)
                                {
                                    output.WK15 = dt.Rows[i]["WK15"].ToString();
                            }


                                if (dt.Rows[i]["WK16"] != System.DBNull.Value)
                                {
                                    output.WK16 = dt.Rows[i]["WK16"].ToString();
                            }


                                if (dt.Rows[i]["WK17"] != System.DBNull.Value)
                                {
                                    output.WK17 = dt.Rows[i]["WK17"].ToString();
                            }


                                if (dt.Rows[i]["WK18"] != System.DBNull.Value)
                                {
                                    output.WK18 = dt.Rows[i]["WK18"].ToString();
                            }


                                if (dt.Rows[i]["WK19"] != System.DBNull.Value)
                                {
                                    output.WK19 = dt.Rows[i]["WK19"].ToString();
                            }


                                if (dt.Rows[i]["WK20"] != System.DBNull.Value)
                                {
                                    output.WK20 = dt.Rows[i]["WK20"].ToString();
                            }


                                if (dt.Rows[i]["WK21"] != System.DBNull.Value)
                                {
                                    output.WK21 = dt.Rows[i]["WK21"].ToString();
                            }


                                if (dt.Rows[i]["WK22"] != System.DBNull.Value)
                                {
                                    output.WK22 = dt.Rows[i]["WK22"].ToString();
                            }
                                if (dt.Rows[i]["WK23"] != System.DBNull.Value)
                                {
                                    output.WK23 = dt.Rows[i]["WK23"].ToString();
                            }
                                if (dt.Rows[i]["WK24"] != System.DBNull.Value)
                                {
                                    output.WK24 = dt.Rows[i]["WK24"].ToString();
                            }
                                if (dt.Rows[i]["WK25"] != System.DBNull.Value)
                                {
                                    output.WK25 = dt.Rows[i]["WK25"].ToString();
                            }
                                if (dt.Rows[i]["WK26"] != System.DBNull.Value)
                                {
                                    output.WK26 = dt.Rows[i]["WK26"].ToString();
                            }
                            //    if (dt.Rows[i]["WK26"] != System.DBNull.Value)
                            //    {
                            //        output.WK26 = dt.Rows[i]["WK0"].ToString();
                            //}


                                if (dt.Rows[i]["REST_OF_HORIZON"] != System.DBNull.Value)
                                {
                                    output.Rest_Of_Horizon = Convert.ToString(dt.Rows[i]["REST_OF_HORIZON"]);
                                }


                                output.Sys_last_Modified_By = String.Empty;
                                if (dt.Rows[i]["SYS_LAST_MODIFIED_BY"] != System.DBNull.Value)
                                {
                                    output.Sys_last_Modified_By = Convert.ToString(dt.Rows[i]["SYS_LAST_MODIFIED_BY"]);
                                }


                                output.Sys_last_Modified_Date = String.Empty;
                                if (dt.Rows[i]["SYS_LAST_MODIFIED_DATE"] != System.DBNull.Value)
                                {
                                    output.Sys_last_Modified_Date = Convert.ToString(dt.Rows[i]["SYS_LAST_MODIFIED_DATE"]);
                                }



                                searchResult.Add(output);
                            }
                        connection.Close();

                    }
                    return searchResult;
                }
                catch (Exception ex)
                {
                     
                    throw ex;
                }
            });




        }




        [HttpPost]
        [Route("uploadDemandData")]
        public async Task<ObjectResult> uploadDemandData([FromBody] List<SearchQuery> chanHierarchyDetails)
        {
            ObjectResult response;

            try
            {
                var fileNameList = "";// await iChanHeirarachyService.UpdateChanData(chanHierarchyDetails, methodType);
                if (fileNameList != null)
                {
                    //response = new ObjectResult(fileNameList);
                    //response.StatusCode = (int)HttpStatusCode.OK;
                    return null;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    //response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
              //  response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }




     public async Task< Dictionary<string,string>> GetChannelIdSplit()
        {
            Dictionary<string, string> lstChannel = new Dictionary<string, string>();
            OracleConnection conn = null;

            try
            {
                using (conn = new OracleConnection(connenctionString))
                {
                    //string sql = "SELECT DISTINCT CHANNEL_ID FROM MST_CHANNEL_MASTER_VW WHERE SYS_SOURCE ='" + SYS_SOURCE + "' ORDER BY CHANNEL_ID";
                    string sql = "SELECT DISTINCT CHANNEL_ID FROM FIN_CHANNEL_MASTER_VW ORDER BY CHANNEL_ID";
                    using (OracleCommand cmd = new OracleCommand(sql, conn))
                    {
                        conn.Open();
                        OracleDataReader oracleDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                        while (oracleDataReader.Read())
                        {
                            lstChannel.Add(oracleDataReader["CHANNEL_ID"].ToString(), oracleDataReader["CHANNEL_ID"].ToString());
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return lstChannel;

        }
     public async Task<Dictionary<string, string>> GetGeographyIdSplit()
        {
            Dictionary<string, string> lstChannel = new Dictionary<string, string>();
            OracleConnection conn = null;

            try
            {
                using (conn = new OracleConnection(connenctionString))
                {
                    //string sql = "SELECT DISTINCT CHANNEL_ID FROM MST_CHANNEL_MASTER_VW WHERE SYS_SOURCE ='" + SYS_SOURCE + "' ORDER BY CHANNEL_ID";
                    string sql = "SELECT DISTINCT GEO_ID FROM MST_GEOGRAPHY_VW WHERE SYS_SOURCE ='LIBERTY' ORDER BY GEO_ID";
                    using (OracleCommand cmd = new OracleCommand(sql, conn))
                    {
                        conn.Open();
                        OracleDataReader oracleDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                        while (oracleDataReader.Read())
                        {
                            lstChannel.Add(oracleDataReader["GEO_ID"].ToString(), oracleDataReader["GEO_ID"].ToString());
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return lstChannel;

        }
     public async Task<Dictionary<string, string>> GetProductIdSplit()
        {
            Dictionary<string, string> lstChannel = new Dictionary<string, string>();
            OracleConnection conn = null;

            try
            {
                using (conn = new OracleConnection(connenctionString))
                {
                    //string sql = "SELECT DISTINCT CHANNEL_ID FROM MST_CHANNEL_MASTER_VW WHERE SYS_SOURCE ='" + SYS_SOURCE + "' ORDER BY CHANNEL_ID";
                    string sql = "select fhc_id PRODUCT_ID from dell_cm_fhc_ref_vw union SELECT PRODUCT_ID FROM MST_PRODUCT_MASTER_VW WHERE PRODUCT_LEVEL NOT IN ('106') ORDER BY PRODUCT_ID";
                    using (OracleCommand cmd = new OracleCommand(sql, conn))
                    {
                        conn.Open();
                        OracleDataReader oracleDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                        while (oracleDataReader.Read())
                        {
                            lstChannel.Add(oracleDataReader["PRODUCT_ID"].ToString(), oracleDataReader["PRODUCT_ID"].ToString());
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return lstChannel;

        }

     public async Task<Dictionary<string, string>> GetSupplychainSplit()
        {
            Dictionary<string, string> lstChannel = new Dictionary<string, string>();
            OracleConnection conn = null;

            try
            {
                using (conn = new OracleConnection(connenctionString))
                {
                    //string sql = "SELECT DISTINCT CHANNEL_ID FROM MST_CHANNEL_MASTER_VW WHERE SYS_SOURCE ='" + SYS_SOURCE + "' ORDER BY CHANNEL_ID";
                    string sql = "SELECT SSC_NAME ssc from SEGMENT_SUPPLY_CHAIN_VW ORDER BY SSC_NAME";
                    using (OracleCommand cmd = new OracleCommand(sql, conn))
                    {
                        conn.Open();
                        OracleDataReader oracleDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                        while (oracleDataReader.Read())
                        {
                            lstChannel.Add(oracleDataReader["SSC"].ToString(), oracleDataReader["SSC"].ToString());
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return lstChannel;

        }



        public int DemandSplitBulkInsert(DataTable dt, string extra_ID, string userName)
       {
            OracleTransaction transaction = null;
            OracleCommand oracleCommand = new OracleCommand();
            OracleConnection oracleConnection = new OracleConnection();

            int intNumberOfRows = 0, intRowsAffected = 0;
            try
            {

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (String.IsNullOrEmpty(dt.Rows[i][0].ToString()) == true)
                    {
                        break;
                    }
                    else
                    {
                        intNumberOfRows = i + 1;
                    }
                }

                //object[] SPLIT_IDArr = new object[intNumberOfRows];
                object[] PRODUCT_IDArr = new object[intNumberOfRows];
                object[] CHANNEL_IDArr = new object[intNumberOfRows];
                object[] GEOGRAPHY_IDArr = new object[intNumberOfRows];
                object[] SSCArr = new object[intNumberOfRows];
                object[] WK0Arr = new object[intNumberOfRows];
                object[] WK1Arr = new object[intNumberOfRows];
                object[] WK2Arr = new object[intNumberOfRows];
                object[] WK3Arr = new object[intNumberOfRows];
                object[] WK4Arr = new object[intNumberOfRows];
                object[] WK5Arr = new object[intNumberOfRows];
                object[] WK6Arr = new object[intNumberOfRows];
                object[] WK7Arr = new object[intNumberOfRows];
                object[] WK8Arr = new object[intNumberOfRows];
                object[] WK9Arr = new object[intNumberOfRows];
                object[] WK10Arr = new object[intNumberOfRows];
                object[] WK11Arr = new object[intNumberOfRows];
                object[] WK12Arr = new object[intNumberOfRows];
                object[] WK13Arr = new object[intNumberOfRows];
                object[] WK14Arr = new object[intNumberOfRows];
                object[] WK15Arr = new object[intNumberOfRows];
                object[] WK16Arr = new object[intNumberOfRows];
                object[] WK17Arr = new object[intNumberOfRows];
                object[] WK18Arr = new object[intNumberOfRows];
                object[] WK19Arr = new object[intNumberOfRows];
                object[] WK20Arr = new object[intNumberOfRows];
                object[] WK21Arr = new object[intNumberOfRows];
                object[] WK22Arr = new object[intNumberOfRows];
                object[] WK23Arr = new object[intNumberOfRows];
                object[] WK24Arr = new object[intNumberOfRows];
                object[] WK25Arr = new object[intNumberOfRows];
                object[] WK26Arr = new object[intNumberOfRows];
                object[] REST_OF_HORIZONArr = new object[intNumberOfRows];
                object[] SOURCE_TYPEArr = new object[intNumberOfRows];
                object[] SYS_SOURCEArr = new object[intNumberOfRows];
                object[] SYS_CREATED_BYArr = new object[intNumberOfRows];
                object[] SYS_LAST_MODIFIED_BYArr = new object[intNumberOfRows];
                object[] SYS_ENT_STATEArr = new object[intNumberOfRows];

                for (int i = 0; i < intNumberOfRows; i++)
                {
                    //SPLIT_IDArr[i] = extract_ID;
                    PRODUCT_IDArr[i] = dt.Rows[i]["PRODUCTIDVAL"].ToString();
                    CHANNEL_IDArr[i] = dt.Rows[i]["CHANNELIDVAL"].ToString();
                    GEOGRAPHY_IDArr[i] = dt.Rows[i]["GEOIDVAL"].ToString();
                    SSCArr[i] = dt.Rows[i][3].ToString();
                    WK0Arr[i] = Convert.ToDecimal(dt.Rows[i][4]);
                    WK1Arr[i] = Convert.ToDecimal(dt.Rows[i][5]);
                    WK2Arr[i] = Convert.ToDecimal(dt.Rows[i][6]);
                    WK3Arr[i] = Convert.ToDecimal(dt.Rows[i][7]);
                    WK4Arr[i] = Convert.ToDecimal(dt.Rows[i][8]);
                    WK5Arr[i] = Convert.ToDecimal(dt.Rows[i][9]);
                    WK6Arr[i] = Convert.ToDecimal(dt.Rows[i][10]);
                    WK7Arr[i] = Convert.ToDecimal(dt.Rows[i][11]);
                    WK8Arr[i] = Convert.ToDecimal(dt.Rows[i][12]);
                    WK9Arr[i] = Convert.ToDecimal(dt.Rows[i][13]);
                    WK10Arr[i] = Convert.ToDecimal(dt.Rows[i][14]);
                    WK11Arr[i] = Convert.ToDecimal(dt.Rows[i][15]);
                    WK12Arr[i] = Convert.ToDecimal(dt.Rows[i][16]);
                    WK13Arr[i] = Convert.ToDecimal(dt.Rows[i][17]);
                    WK14Arr[i] = Convert.ToDecimal(dt.Rows[i][18]);
                    WK15Arr[i] = Convert.ToDecimal(dt.Rows[i][19]);
                    WK16Arr[i] = Convert.ToDecimal(dt.Rows[i][20]);
                    WK17Arr[i] = Convert.ToDecimal(dt.Rows[i][21]);
                    WK18Arr[i] = Convert.ToDecimal(dt.Rows[i][22]);
                    WK19Arr[i] = Convert.ToDecimal(dt.Rows[i][23]);
                    WK20Arr[i] = Convert.ToDecimal(dt.Rows[i][24]);
                    WK21Arr[i] = Convert.ToDecimal(dt.Rows[i][25]);
                    WK22Arr[i] = Convert.ToDecimal(dt.Rows[i][26]);
                    WK23Arr[i] = Convert.ToDecimal(dt.Rows[i][27]);
                    WK24Arr[i] = Convert.ToDecimal(dt.Rows[i][28]);
                    WK25Arr[i] = Convert.ToDecimal(dt.Rows[i][29]);
                    WK26Arr[i] = Convert.ToDecimal(dt.Rows[i][30]);
                    REST_OF_HORIZONArr[i] = Convert.ToDecimal(dt.Rows[i][31]);
                    SOURCE_TYPEArr[i] = null;
                    SYS_SOURCEArr[i] = "MOSSUI";// ConfigurationManager.AppSettings["MOSSUI"].ToString();
                    SYS_CREATED_BYArr[i] = userName;
                    SYS_LAST_MODIFIED_BYArr[i] = userName;
                    SYS_ENT_STATEArr[i] = "ACTIVE"; // ConfigurationManager.AppSettings["SYS_ENT_STATE"].ToString();
                }


                string insertSql = "Insert into  DEMAND_SPLIT(SPLIT_ID,PRODUCT_ID,CHANNEL_ID,GEOGRAPHY_ID,SSC,WK0,WK1,WK2,WK3,WK4,WK5,"
                                    + "WK6,WK7,WK8,WK9,WK10,WK11,WK12,WK13,WK14,WK15,WK16,WK17,WK18,WK19,WK20,WK21,"
                                    + "WK22,WK23,WK24,WK25,WK26,REST_OF_HORIZON,SOURCE_TYPE,SYS_SOURCE,SYS_CREATED_BY,SYS_CREATION_DATE,SYS_LAST_MODIFIED_BY,SYS_LAST_MODIFIED_DATE,SYS_ENT_STATE)"
                                    + "values(SEQ_TRANSACTION_ID.NEXTVAL,:PRODUCT_ID,:CHANNEL_ID,:GEOGRAPHY_ID,:SSC,:WK0,:WK1,:WK2,:WK3,:WK4,:WK5,"
                                    + ":WK6,:WK7,:WK8,:WK9,:WK10,:WK11,:WK12,:WK13,:WK14,:WK15,:WK16,:WK17,:WK18,:WK19,:WK20,:WK21,"
                                    + ":WK22,:WK23,:WK24,:WK25,:WK26,:REST_OF_HORIZON,:SOURCE_TYPE,:SYS_SOURCE,:SYS_CREATED_BY,sysdate,:SYS_LAST_MODIFIED_BY,sysdate,:SYS_ENT_STATE)";
                //"values('2','Bangalore', 'fga', '001', 'Babul','13-JAN-1998', '1','13-JAN-1998','1','13-JAN-1998','1',10,10,'channel','geo','ship','sou','babul','13-JAN-1998' )

                //this code is required to purse data



                 oracleConnection = new OracleConnection(connenctionString);

                String deletStr = "DELETE FROM DEMAND_SPLIT";

                using (oracleConnection)
                {

                    OracleCommand tempCmd = new OracleCommand(deletStr, oracleConnection);
                    oracleConnection.Open();
                    transaction = oracleConnection.BeginTransaction();
                    int count = tempCmd.ExecuteNonQuery();

                    using (oracleCommand = oracleConnection.CreateCommand())
                    {
                        oracleCommand.CommandText = insertSql;

                        //OracleParameter SPLIT_ID = null;
                        OracleParameter PRODUCT_ID = null;
                        OracleParameter CHANNEL_ID = null;
                        OracleParameter GEOGRAPHY_ID = null;
                        OracleParameter SSC = null;
                        OracleParameter WK0 = null;
                        OracleParameter WK1 = null;
                        OracleParameter WK2 = null;
                        OracleParameter WK3 = null;
                        OracleParameter WK4 = null;
                        OracleParameter WK5 = null;
                        OracleParameter WK6 = null;
                        OracleParameter WK7 = null;
                        OracleParameter WK8 = null;
                        OracleParameter WK9 = null;
                        OracleParameter WK10 = null;
                        OracleParameter WK11 = null;
                        OracleParameter WK12 = null;
                        OracleParameter WK13 = null;
                        OracleParameter WK14 = null;
                        OracleParameter WK15 = null;
                        OracleParameter WK16 = null;
                        OracleParameter WK17 = null;
                        OracleParameter WK18 = null;
                        OracleParameter WK19 = null;
                        OracleParameter WK20 = null;
                        OracleParameter WK21 = null;
                        OracleParameter WK22 = null;
                        OracleParameter WK23 = null;
                        OracleParameter WK24 = null;
                        OracleParameter WK25 = null;
                        OracleParameter WK26 = null;
                        OracleParameter REST_OF_HORIZON = null;
                        OracleParameter SOURCE_TYPE = null;
                        OracleParameter SYS_SOURCE = null;
                        OracleParameter SYS_CREATED_BY = null;
                        OracleParameter SYS_LAST_MODIFIED_BY = null;
                        OracleParameter SYS_ENT_STATE = null;



                        if (intNumberOfRows > 0)
                        {
                            //SPLIT_ID = new OracleParameter("SPLIT_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            PRODUCT_ID = new OracleParameter("PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            CHANNEL_ID = new OracleParameter("CHANNEL_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            GEOGRAPHY_ID = new OracleParameter("GEOGRAPHY_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            SSC = new OracleParameter("SSC", OracleDbType.Varchar2, ParameterDirection.Input);
                            WK0 = new OracleParameter("WK0", OracleDbType.Decimal, ParameterDirection.Input);
                            WK1 = new OracleParameter("WK1", OracleDbType.Decimal, ParameterDirection.Input);
                            WK2 = new OracleParameter("WK2", OracleDbType.Decimal, ParameterDirection.Input);
                            WK3 = new OracleParameter("WK3", OracleDbType.Decimal, ParameterDirection.Input);
                            WK4 = new OracleParameter("WK4", OracleDbType.Decimal, ParameterDirection.Input);
                            WK5 = new OracleParameter("WK5", OracleDbType.Decimal, ParameterDirection.Input);
                            WK6 = new OracleParameter("WK6", OracleDbType.Decimal, ParameterDirection.Input);
                            WK7 = new OracleParameter("WK7", OracleDbType.Decimal, ParameterDirection.Input);
                            WK8 = new OracleParameter("WK8", OracleDbType.Decimal, ParameterDirection.Input);
                            WK9 = new OracleParameter("WK9", OracleDbType.Decimal, ParameterDirection.Input);
                            WK10 = new OracleParameter("WK10", OracleDbType.Decimal, ParameterDirection.Input);
                            WK11 = new OracleParameter("WK11", OracleDbType.Decimal, ParameterDirection.Input);
                            WK12 = new OracleParameter("WK12", OracleDbType.Decimal, ParameterDirection.Input);
                            WK13 = new OracleParameter("WK13", OracleDbType.Decimal, ParameterDirection.Input);
                            WK14 = new OracleParameter("WK14", OracleDbType.Decimal, ParameterDirection.Input);
                            WK15 = new OracleParameter("WK15", OracleDbType.Decimal, ParameterDirection.Input);
                            WK16 = new OracleParameter("WK16", OracleDbType.Decimal, ParameterDirection.Input);
                            WK17 = new OracleParameter("WK17", OracleDbType.Decimal, ParameterDirection.Input);
                            WK18 = new OracleParameter("WK18", OracleDbType.Decimal, ParameterDirection.Input);
                            WK19 = new OracleParameter("WK19", OracleDbType.Decimal, ParameterDirection.Input);
                            WK20 = new OracleParameter("WK20", OracleDbType.Decimal, ParameterDirection.Input);
                            WK21 = new OracleParameter("WK21", OracleDbType.Decimal, ParameterDirection.Input);
                            WK22 = new OracleParameter("WK22", OracleDbType.Decimal, ParameterDirection.Input);
                            WK23 = new OracleParameter("WK23", OracleDbType.Decimal, ParameterDirection.Input);
                            WK24 = new OracleParameter("WK24", OracleDbType.Decimal, ParameterDirection.Input);
                            WK25 = new OracleParameter("WK25", OracleDbType.Decimal, ParameterDirection.Input);
                            WK26 = new OracleParameter("WK26", OracleDbType.Decimal, ParameterDirection.Input);
                            REST_OF_HORIZON = new OracleParameter("REST_OF_HORIZON", OracleDbType.Decimal, ParameterDirection.Input);
                            SOURCE_TYPE = new OracleParameter("SYS_SOURCE", OracleDbType.Varchar2, ParameterDirection.Input);
                            SYS_SOURCE = new OracleParameter("MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                            SYS_CREATED_BY = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                            SYS_LAST_MODIFIED_BY = new OracleParameter("SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                            SYS_ENT_STATE = new OracleParameter("SYS_ENT_STATE", OracleDbType.Varchar2, ParameterDirection.Input);


                            // SPLIT_ID.Value = SPLIT_IDArr;
                            PRODUCT_ID.Value = PRODUCT_IDArr;
                            CHANNEL_ID.Value = CHANNEL_IDArr;
                            GEOGRAPHY_ID.Value = GEOGRAPHY_IDArr;
                            SSC.Value = SSCArr;
                            WK0.Value = WK0Arr;
                            WK1.Value = WK1Arr;
                            WK2.Value = WK2Arr;
                            WK3.Value = WK3Arr;
                            WK4.Value = WK4Arr;
                            WK5.Value = WK5Arr;
                            WK6.Value = WK6Arr;
                            WK7.Value = WK7Arr;
                            WK8.Value = WK8Arr;
                            WK9.Value = WK9Arr;
                            WK10.Value = WK10Arr;
                            WK11.Value = WK11Arr;
                            WK12.Value = WK12Arr;
                            WK13.Value = WK13Arr;
                            WK14.Value = WK14Arr;
                            WK15.Value = WK15Arr;
                            WK16.Value = WK16Arr;
                            WK17.Value = WK17Arr;
                            WK18.Value = WK18Arr;
                            WK19.Value = WK19Arr;
                            WK20.Value = WK20Arr;
                            WK21.Value = WK21Arr;
                            WK22.Value = WK22Arr;
                            WK23.Value = WK23Arr;
                            WK24.Value = WK24Arr;
                            WK25.Value = WK25Arr;
                            WK26.Value = WK25Arr;
                            REST_OF_HORIZON.Value = REST_OF_HORIZONArr;
                            SOURCE_TYPE.Value = SOURCE_TYPEArr;
                            SYS_SOURCE.Value = SYS_SOURCEArr;
                            SYS_CREATED_BY.Value = SYS_CREATED_BYArr;
                            SYS_LAST_MODIFIED_BY.Value = SYS_LAST_MODIFIED_BYArr;
                            SYS_ENT_STATE.Value = SYS_ENT_STATEArr;


                            //oracleCommand.Parameters.Add(SPLIT_ID);
                            oracleCommand.Parameters.Add(PRODUCT_ID);
                            oracleCommand.Parameters.Add(CHANNEL_ID);
                            oracleCommand.Parameters.Add(GEOGRAPHY_ID);
                            oracleCommand.Parameters.Add(SSC);
                            oracleCommand.Parameters.Add(WK0);
                            oracleCommand.Parameters.Add(WK1);
                            oracleCommand.Parameters.Add(WK2);
                            oracleCommand.Parameters.Add(WK3);
                            oracleCommand.Parameters.Add(WK4);
                            oracleCommand.Parameters.Add(WK5);
                            oracleCommand.Parameters.Add(WK6);
                            oracleCommand.Parameters.Add(WK7);
                            oracleCommand.Parameters.Add(WK8);
                            oracleCommand.Parameters.Add(WK9);
                            oracleCommand.Parameters.Add(WK10);
                            oracleCommand.Parameters.Add(WK11);
                            oracleCommand.Parameters.Add(WK12);
                            oracleCommand.Parameters.Add(WK13);
                            oracleCommand.Parameters.Add(WK14);
                            oracleCommand.Parameters.Add(WK15);
                            oracleCommand.Parameters.Add(WK16);
                            oracleCommand.Parameters.Add(WK17);
                            oracleCommand.Parameters.Add(WK18);
                            oracleCommand.Parameters.Add(WK19);
                            oracleCommand.Parameters.Add(WK20);
                            oracleCommand.Parameters.Add(WK21);
                            oracleCommand.Parameters.Add(WK22);
                            oracleCommand.Parameters.Add(WK23);
                            oracleCommand.Parameters.Add(WK24);
                            oracleCommand.Parameters.Add(WK25);
                            oracleCommand.Parameters.Add(WK26);
                            oracleCommand.Parameters.Add(REST_OF_HORIZON);
                            oracleCommand.Parameters.Add(SOURCE_TYPE);
                            oracleCommand.Parameters.Add(SYS_SOURCE);
                            oracleCommand.Parameters.Add(SYS_CREATED_BY);
                            oracleCommand.Parameters.Add(SYS_LAST_MODIFIED_BY);
                            oracleCommand.Parameters.Add(SYS_ENT_STATE);

                            oracleCommand.ArrayBindCount = intNumberOfRows;
                            Stopwatch sw = new Stopwatch();
                            sw.Start();
                            intRowsAffected = oracleCommand.ExecuteNonQuery();
                            sw.Stop();
                        }
                    }
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw ex;
            }
            finally
            {
                if (oracleConnection != null)
                {
                    oracleConnection.Close();
                    oracleConnection.Dispose();
                }
            }
            return intRowsAffected;


        }





    }
}
