﻿using Dell.RP.DataAccess;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.DataAccess.Common;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.SupplyPlanningFhc;
using Dell.RP.API.Dell.RP.Common.Models.SupplyPlanningFhc;
using Dell.RP.API.Dell.RP.Common.Models;
using System.Text;

namespace Dell.RP.API.Dell.RP.DataAccess.Repository.SupplyPlanningFhc
{
    public class SupplyPlanningFhcRepository :ISupplyPlanningFhcRepository
    {
        private readonly string connectionString;
        public string UserID { get; set; }

        private readonly string schemaName;

        public SupplyPlanningFhcRepository(IOptions<AppSettings> appSettings, ICommonData commonData)
        {
            connectionString = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringRP, appSettings.Value.ConnectionKey);
            schemaName = appSettings.Value.SchemaName;
        }

        public async Task<List<DropDown>> GetManSite()
        {
            List<DropDown> ManufacturingSites = new List<DropDown>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connectionString))
                    {
                        connection.Open();

                        string sql = "select distinct SITE_ID from snop_site where site_type = 'MFG' and VIRTUAL_FLAG = 'N' and sys_ent_state = 'ACTIVE' order by SITE_ID";

                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            ManufacturingSites.Add(new DropDown { Id = reader["SITE_ID"].ToString(), ItemName = reader["SITE_ID"].ToString() });
                        }
                        connection.Close();
                    }

                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return ManufacturingSites;
            });
        }

        public string GetRPStatus()
        {
            string currentRPStatus = "";
            
                try
                {
                    using (var connection = new OracleConnection(connectionString))
                    {
                        connection.Open();

                        string sql = "SELECT SNOP_RP_PRE_POST_PKG.fun_get_rp_run_state FROM DUAL";

                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            currentRPStatus = reader["FUN_GET_RP_RUN_STATE"].ToString();
                        }
                        connection.Close();
                    }

                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return currentRPStatus;
        }

        public List<DropDown> BindPOUILock()
        {
            List<DropDown> diffRegionStatus = new List<DropDown>();
            try
            {
                using (var connection = new OracleConnection(connectionString))
                {
                    connection.Open();

                    //for APJ
                    string sqlAPJStatus = "SELECT a.* FROM (SELECT status, log_id FROM snop_job_monitor WHERE     master_job_id = (SELECT PROPVALUE FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_APJ_JOB') " +
                                              "AND step_no = (SELECT MAX (step_no) FROM snop_job_monitor WHERE master_job_id = (SELECT PROPVALUE FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_APJ_JOB')) " +
                                              "AND START_TIME IN (SELECT MAX (start_time) FROM snop_job_monitor WHERE master_job_id = (SELECT PROPVALUE FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_APJ_JOB') " +
                                              "AND step_no = (SELECT MAX (step_no) FROM snop_job_monitor WHERE master_job_id = (SELECT PROPVALUE FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_APJ_JOB')))) " +
                                              "a WHERE a.log_id > (SELECT log_id FROM snop_job_monitor WHERE START_TIME IN (SELECT MAX (start_time) FROM snop_job_monitor WHERE master_job_id = (SELECT PROPVALUE " +
                                              " FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_APJ_JOB') AND step_no = (SELECT MIN (step_no) FROM snop_job_monitor WHERE master_job_id =  (SELECT PROPVALUE  FROM SNOP_SYS_PROPS  WHERE PROPNAME = 'REGIONAL_APJ_JOB'))))";

                    using (OracleCommand cmd = new OracleCommand(sqlAPJStatus, connection))
                    { 
                        OracleDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows) { 
                            while (reader.Read())
                            {
                                diffRegionStatus.Add(new DropDown { Id = "APJ_Status", ItemName = reader["STATUS"].ToString() });
                            }
                        }
                        else
                        {
                            diffRegionStatus.Add(new DropDown { Id = "APJ_Status", ItemName = string.Empty });
                        }
                    }

                    //for EMEA
                    string sqlEMEAStatus = "SELECT a.* FROM (SELECT status, log_id FROM snop_job_monitor WHERE     master_job_id = (SELECT PROPVALUE FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_EMEA_JOB') " +
                                              "AND step_no = (SELECT MAX (step_no) FROM snop_job_monitor WHERE master_job_id = (SELECT PROPVALUE FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_EMEA_JOB')) " +
                                              "AND START_TIME IN (SELECT MAX (start_time) FROM snop_job_monitor WHERE master_job_id = (SELECT PROPVALUE FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_EMEA_JOB') " +
                                              "AND step_no = (SELECT MAX (step_no) FROM snop_job_monitor WHERE master_job_id = (SELECT PROPVALUE FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_EMEA_JOB')))) " +
                                              "a WHERE a.log_id > (SELECT log_id FROM snop_job_monitor WHERE START_TIME IN (SELECT MAX (start_time) FROM snop_job_monitor WHERE master_job_id = (SELECT PROPVALUE " +
                                              " FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_EMEA_JOB') AND step_no = (SELECT MIN (step_no) FROM snop_job_monitor WHERE master_job_id =  (SELECT PROPVALUE  FROM SNOP_SYS_PROPS  WHERE PROPNAME = 'REGIONAL_EMEA_JOB'))))";

                    using (OracleCommand cmd = new OracleCommand(sqlEMEAStatus, connection))
                    { 

                        OracleDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                diffRegionStatus.Add(new DropDown { Id = "EMEA_Status", ItemName = reader["STATUS"].ToString() });
                            }
                        }
                        else
                        {
                            diffRegionStatus.Add(new DropDown { Id = "EMEA_Status", ItemName = string.Empty });
                        }
                    }


                    //for AMER
                    string sqlAMERStatus = "SELECT a.* FROM (SELECT status, log_id FROM snop_job_monitor WHERE     master_job_id = (SELECT PROPVALUE FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_AMER_JOB') " +
                                              "AND step_no = (SELECT MAX (step_no) FROM snop_job_monitor WHERE master_job_id = (SELECT PROPVALUE FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_AMER_JOB')) " +
                                              "AND START_TIME IN (SELECT MAX (start_time) FROM snop_job_monitor WHERE master_job_id = (SELECT PROPVALUE FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_AMER_JOB') " +
                                              "AND step_no = (SELECT MAX (step_no) FROM snop_job_monitor WHERE master_job_id = (SELECT PROPVALUE FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_AMER_JOB')))) " +
                                              "a WHERE a.log_id > (SELECT log_id FROM snop_job_monitor WHERE START_TIME IN (SELECT MAX (start_time) FROM snop_job_monitor WHERE master_job_id = (SELECT PROPVALUE " +
                                              " FROM SNOP_SYS_PROPS WHERE PROPNAME = 'REGIONAL_AMER_JOB') AND step_no = (SELECT MIN (step_no) FROM snop_job_monitor WHERE master_job_id =  (SELECT PROPVALUE  FROM SNOP_SYS_PROPS  WHERE PROPNAME = 'REGIONAL_AMER_JOB'))))";

                    using (OracleCommand cmd = new OracleCommand(sqlAMERStatus, connection))
                    {

                        OracleDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                diffRegionStatus.Add(new DropDown { Id = "AMER_Status", ItemName = reader["STATUS"].ToString() });
                            }
                        }
                        else
                        {
                            diffRegionStatus.Add(new DropDown { Id = "AMER_Status", ItemName = string.Empty });
                        }
                    }
                    

                    connection.Close();
                }

            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                throw ex;
            }

            return diffRegionStatus;
        }

        public async Task<List<Get_SupplyPlanningFhc_Export_Details>> GetRPSupplyPlanningFhcExportDetails(Export_Parameters exportParams)
        {
            return await Task.Run(() =>
            {
                List<Get_SupplyPlanningFhc_Export_Details> exportDetailsList = new List<Get_SupplyPlanningFhc_Export_Details>();
                try
                {
                    using (var connection = new OracleConnection(connectionString))
                    {
                        connection.Open();
                        var query = "SNOP_RP_MASS_UPLOAD_FHC_PKG.PRC_SNOP_RP_FHC_DTL_EXTRACT";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "p_in_wk_no";
                        objparam2.Value = exportParams.ReviewWeeks;
                        objparam2.OracleDbType = OracleDbType.Varchar2;
                        objparam2.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam2);

                        OracleParameter objparam3 = new OracleParameter();
                        objparam3.ParameterName = "p_in_from_site";
                        objparam3.Value = exportParams.ManSite;
                        objparam3.OracleDbType = OracleDbType.Varchar2;
                        objparam3.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam3);

                        OracleParameter objparam4 = new OracleParameter();
                        objparam4.ParameterName = "p_in_region";
                        objparam4.Value = exportParams.Region;
                        objparam4.OracleDbType = OracleDbType.Varchar2;
                        objparam4.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam4);

                        OracleParameter objparam5 = new OracleParameter();
                        objparam5.ParameterName = "p_in_itemid";
                        objparam5.Value = exportParams.FhcID;
                        objparam5.OracleDbType = OracleDbType.Varchar2;
                        objparam5.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam5);

                        OracleParameter objparam6 = new OracleParameter();
                        objparam6.ParameterName = "p_in_product";
                        objparam6.Value = exportParams.FamilyParent;
                        objparam6.OracleDbType = OracleDbType.Varchar2;
                        objparam6.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam6);


                        OracleParameter objparam = new OracleParameter();
                        objparam.ParameterName = "p_out_ret_cursor";
                        objparam.OracleDbType = OracleDbType.RefCursor;
                        objparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam);

                        OracleDataReader reader = cmd.ExecuteReader();
                        reader.FetchSize = reader.RowSize * 1000;
                        while (reader.Read())
                        {
                            exportDetailsList.Add(new Get_SupplyPlanningFhc_Export_Details
                            {
                                FhcId = reader["FHC_ID"].ToString(),
                                FamParCode = reader["FAMPAR_CODE"].ToString(),
                                FamilyParent = reader["FAMILY_PARENT"].ToString(),
                                ManSite = reader["MANUFACTURING_SITE"].ToString(),
                                DemandGeo = reader["DEMAND_GEO"].ToString(),
                                Region = reader["REGION"].ToString(),
                                SscType = reader["SSC"].ToString(),
                                ShipMode = reader["SHIP_MODE"].ToString(),
                                ShipToSite = reader["SHIP_TO_SITE"].ToString(),
                                ShipDate = reader["SHIP_DATE"].ToString(),
                                DeliveryDate = reader["DELIVERY_DATE"].ToString(),
                                OrginialQty = reader["ORIGINAL_QTY"].ToString(),
                                UpdatedQty = reader["UPDATED_QTY"].ToString(),
                                UpdatedBy = reader["UPDATED_BY"].ToString()

                            });
                        }
                        connection.Close();

                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return exportDetailsList;
            });
        }

        public async Task<List<DropDown>> GetFamilyParentDD()
        {
            List<DropDown> famPar = new List<DropDown>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connectionString))
                    {
                        connection.Open();

                        string sql = "SELECT DISTINCT b.fampar_name || '(' || a.fampar_code || ')' AS Family_Parent,b.fampar_desc FROM snop_fga_ref a, snop_product_flat b WHERE a.FAMPAR_CODE = b.fampar_code ORDER BY family_parent";

                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            famPar.Add(new DropDown { Id = reader["FAMILY_PARENT"].ToString(), ItemName = reader["FAMILY_PARENT"].ToString() });
                        }
                        connection.Close();
                    }

                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return famPar;
            });
        }

        public string InsertFHC(List<RPSupplyPlan_Import_Params> importData)
        {
            string str = "";
            foreach (var item in importData)
            {
                item.sys_last_modified_by = UserID;
            }
            try
            {
                using (var connection = new OracleConnection(connectionString))
                {
                    //OracleCommand cmd = new OracleCommand();
                    //cmd.Connection = connection;
                    var query = "SNOP_RP_MASS_UPLOAD_FHC_PKG.PRC_RP_LOAD_FHC_IN_TMP_TBL";
                    connection.Open();
                    OracleCommand cmd = new OracleCommand(query, connection);
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.CommandText = "SNOP_RP_MASS_UPLOAD_FHC_PKG.PRC_RP_LOAD_FHC_IN_TMP_TBL";

                    //OracleParameter oracleParameter = new OracleParameter();
                    //oracleParameter.ParameterName = "p1_in_xl_upload";
                    //oracleParameter.UdtTypeName = "SNOP_STG_SOURCE.TB_TY_RP_FHC_UPLD_ARRY";
                    //oracleParameter.Value = importData.ToArray();
                    //oracleParameter.OracleDbType = OracleDbType.RefCursor;
                    //oracleParameter.Direction = ParameterDirection.Input;
                    //cmd.Parameters.Add(oracleParameter);

                    OracleParameter objArrayparam1 = new OracleParameter("P_IN_ITEM_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                    objArrayparam1.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                    objArrayparam1.Value = importData.Select(x => x.item_id).ToArray();
                    objArrayparam1.Size = 5000000;
                    cmd.Parameters.Add(objArrayparam1);

                    OracleParameter objArrayparam2 = new OracleParameter("P_IN_FROM_SITE", OracleDbType.Varchar2, ParameterDirection.Input);
                    objArrayparam2.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                    objArrayparam2.Value = importData.Select(x => x.from_site).ToArray();
                    objArrayparam2.Size = 5000000;
                    cmd.Parameters.Add(objArrayparam2);

                    OracleParameter objArrayparam3 = new OracleParameter("P_IN_GEO", OracleDbType.Varchar2, ParameterDirection.Input);
                    objArrayparam3.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                    objArrayparam3.Value = importData.Select(x => x.geo).ToArray();
                    objArrayparam3.Size = 5000000;
                    cmd.Parameters.Add(objArrayparam3);

                    OracleParameter objArrayparam4 = new OracleParameter("P_IN_UPDATED_QTY", OracleDbType.Varchar2, ParameterDirection.Input);
                    objArrayparam4.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                    objArrayparam4.Value = importData.Select(x => x.updated_qty).ToArray();
                    objArrayparam4.Size = 5000000;
                    cmd.Parameters.Add(objArrayparam4);

                    OracleParameter objArrayparam5 = new OracleParameter("P_IN_SHIP_DATE", OracleDbType.Varchar2, ParameterDirection.Input);
                    objArrayparam5.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                    objArrayparam5.Value = importData.Select(x => x.ship_date).ToArray();
                    objArrayparam5.Size = 5000000;
                    cmd.Parameters.Add(objArrayparam5);

                    OracleParameter objArrayparam6 = new OracleParameter("P_IN_SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                    objArrayparam6.CollectionType = OracleCollectionType.PLSQLAssociativeArray;
                    objArrayparam6.Value = importData.Select(x => x.sys_last_modified_by).ToArray();
                    objArrayparam6.Size = 5000000;
                    cmd.Parameters.Add(objArrayparam6);

                    OracleParameter oracleParameter7 = new OracleParameter();
                    oracleParameter7.ParameterName = "p_out_err_mesg";
                    oracleParameter7.OracleDbType = OracleDbType.Varchar2;
                    oracleParameter7.Direction = ParameterDirection.Output;
                    oracleParameter7.Size = 50000;
                    cmd.Parameters.Add(oracleParameter7);

                    cmd.ExecuteNonQuery();
                    str = cmd.Parameters["p_out_err_mesg"].Value.ToString();
                    connection.Close();
                }

            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                throw ex;
            }

            return str;
        }

        public List<Error_Grid_Details> getErrGridDetails()
        {
            List<Error_Grid_Details> errData = new List<Error_Grid_Details>();

            try
            {
                using (var connection = new OracleConnection(connectionString))
                {
                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = connection;
                    connection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SNOP_RP_MASS_UPLOAD_FHC_PKG.PRC_RP_FHC_GET_UPLD_REJ_RECS";

                    OracleParameter oracleParameter2 = new OracleParameter();
                    oracleParameter2.ParameterName = "p_out_ret_rej";
                    oracleParameter2.OracleDbType = OracleDbType.RefCursor;
                    oracleParameter2.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(oracleParameter2);

                    OracleDataReader reader = cmd.ExecuteReader();
                    reader.FetchSize = reader.RowSize * 1000;
                    while (reader.Read())
                    {
                        errData.Add(new Error_Grid_Details
                        {
                            ItemID = reader[0].ToString(),
                            FromSite = reader[1].ToString(),
                            Geo = reader[2].ToString(),
                            Qty = reader[3].ToString(),
                            ShipDate = reader[4].ToString(),
                            ModifiedBy = reader[5].ToString(),
                            ErrDesc = reader[6].ToString()
                        });
                    }
                    connection.Close();
                }

            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                throw ex;
            }

            return errData;
        }

        public string GetLockStatusFHC(string flag)
        {
            string statusMsg = "";

            try
            {
                using (var connection = new OracleConnection(connectionString))
                {
                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = connection;
                    connection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SNOP_RP_PRE_POST_PKG.PRC_LOCK_UNLOCK_UI_FHC_UPLD";

                    OracleParameter oracleParameter = new OracleParameter();
                    oracleParameter.ParameterName = "p1_in_lock_type";
                    oracleParameter.Value = "LOCK_XL_UPLOAD";
                    oracleParameter.OracleDbType = OracleDbType.Varchar2;
                    oracleParameter.Direction = ParameterDirection.Input;
                    oracleParameter.Size = 5000000;
                    cmd.Parameters.Add(oracleParameter);

                    OracleParameter oracleParameter2 = new OracleParameter();
                    oracleParameter2.ParameterName = "p2_in_lock_flag";
                    oracleParameter2.Value = flag;
                    oracleParameter2.OracleDbType = OracleDbType.Varchar2;
                    oracleParameter2.Direction = ParameterDirection.Input;
                    oracleParameter2.Size = 5000000;
                    cmd.Parameters.Add(oracleParameter2);

                    OracleParameter oracleParameter3 = new OracleParameter();
                    oracleParameter3.ParameterName = "p_out_err_msg";
                    oracleParameter3.OracleDbType = OracleDbType.Varchar2;
                    oracleParameter3.Direction = ParameterDirection.Output;
                    oracleParameter3.Size = 50000;
                    cmd.Parameters.Add(oracleParameter3);

                    cmd.ExecuteNonQuery();
                    statusMsg = cmd.Parameters["p_out_err_msg"].Value.ToString();
                    connection.Close();
                }

            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                throw ex;
            }

            return statusMsg;
        }

        public string GetValidRecStatusFHC()
        {
            string statusMsg = "";

            try
            {
                using (var connection = new OracleConnection(connectionString))
                {
                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = connection;
                    connection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SNOP_RP_MASS_UPLOAD_FHC_PKG.PRC_RP_PROCES_FHC_VALID_REC";

                    OracleParameter oracleParameter3 = new OracleParameter();
                    oracleParameter3.ParameterName = "p1_out_response_msg";
                    oracleParameter3.OracleDbType = OracleDbType.Varchar2;
                    oracleParameter3.Direction = ParameterDirection.Output;
                    oracleParameter3.Size = 50000;
                    cmd.Parameters.Add(oracleParameter3);

                    cmd.ExecuteNonQuery();
                    statusMsg = cmd.Parameters["p1_out_response_msg"].Value.ToString();
                    connection.Close();
                }

            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                throw ex;
            }

            return statusMsg;
        }

        public int UpdateAutoUnlockUIFHC()
        {

            int count = 0;
            OracleConnection oracleConnection = null;
            OracleCommand oracleCommand = null;
            try
            {
                using (oracleConnection = new OracleConnection(connectionString))
                {
                    
                        string sqlUpdate = string.Format("UPDATE SNOP_STG.SNOP_SYS_PROPS SET PROPVALUE=:propvalue,SYS_LAST_MODIFIED_DATE=TO_DATE('" + DateTime.Now.ToString("yyyyMMddHHmmss") + "', 'YYYYMMDDHH24MISS')  WHERE PROPNAME='RP_UPLOAD_FHC_STATUS'");
                        oracleConnection.Open();
                        using (oracleCommand = new OracleCommand(sqlUpdate, oracleConnection))
                        {
                            OracleParameter p_propvalue = new OracleParameter("PROPVALUE", OracleDbType.Varchar2, ParameterDirection.Input);
                            p_propvalue.Value = "COMPLETED";
                            p_propvalue.ParameterName = "propvalue";
                            oracleCommand.Parameters.Add(p_propvalue);

                            count = oracleCommand.ExecuteNonQuery();
                        }
                }
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                throw ex;
            }
            finally
            {
                oracleCommand = null;
                if (oracleConnection != null && oracleConnection.State != ConnectionState.Closed)
                {
                    oracleConnection.Close();
                }
                oracleConnection = null;
            }

            return count;
        }


    }
}
