﻿using Dell.RP.DataAccess;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.DataAccess.Common;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.Common.Models;
using System.Text;

namespace Dell.RP.API.Dell.RP.DataAccess.Repository.FullFillmentSiteManagement
{
    public class FullFillmentSiteManagementRepository : IFullFillmentSiteManagementRepository
    {
        private readonly string connenctionString;
        public string UserID { get; set; }

        private readonly string schemaName;

        public FullFillmentSiteManagementRepository(IOptions<AppSettings> appSettings, ICommonData commonData)
        {
            connenctionString = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringSCDH, appSettings.Value.ConnectionKey);
            schemaName = appSettings.Value.SchemaName;
        }

        public async Task<List<DropDown>> GetMasterDataData(string key)
        {
            List<DropDown> searchData = new List<DropDown>();
            return await Task.Run(() =>
            {
                try
                {
                    List<MasterData> mData = new List<MasterData>();
                    mData.Add(new MasterData { Id = "1", ItemName = "ALL", Key = "GEO" });
                    mData.Add(new MasterData { Id = "2", ItemName = "GEOGRAPHY_TREE", Key = "GEO" });
                    mData.Add(new MasterData { Id = "3", ItemName = "COUNTRY", Key = "GEO" });


                    mData.Add(new MasterData { Id = "1", ItemName = "ALL", Key = "CHANNEL" });
                    mData.Add(new MasterData { Id = "2", ItemName = "CHANNEL_TREE", Key = "CHANNEL" });
                    mData.Add(new MasterData { Id = "3", ItemName = "SEGMENT", Key = "CHANNEL" });

                    mData.Add(new MasterData { Id = "1", ItemName = "ALL", Key = "PRODUCT" });
                    mData.Add(new MasterData { Id = "2", ItemName = "PRODUCT_TREE", Key = "PRODUCT" });
                    mData.Add(new MasterData { Id = "3", ItemName = "FHC_TREE", Key = "PRODUCT" });
                    mData.Add(new MasterData { Id = "4", ItemName = "FGA", Key = "PRODUCT" });

                    mData.Add(new MasterData { Id = "1", ItemName = "ALL", Key = "SUPPLYCHAINTYPE" });
                    mData.Add(new MasterData { Id = "2", ItemName = "BTO", Key = "SUPPLYCHAINTYPE" });
                    mData.Add(new MasterData { Id = "3", ItemName = "BTP", Key = "SUPPLYCHAINTYPE" });
                    mData.Add(new MasterData { Id = "4", ItemName = "BTS", Key = "SUPPLYCHAINTYPE" });
                    mData.Add(new MasterData { Id = "5", ItemName = "CTO", Key = "SUPPLYCHAINTYPE" });

                    mData.Add(new MasterData { Id = "1", ItemName = "BTO", Key = "SUPPLYCHAINTYPE_edit" });
                    mData.Add(new MasterData { Id = "2", ItemName = "BTP", Key = "SUPPLYCHAINTYPE_edit" });
                    mData.Add(new MasterData { Id = "3", ItemName = "BTS", Key = "SUPPLYCHAINTYPE_edit" });
                    mData.Add(new MasterData { Id = "4", ItemName = "CTO", Key = "SUPPLYCHAINTYPE_edit" });

                    List<MasterData> filteredData = mData.Where(a => a.Key.ToString() == key.ToString()).ToList();

                    foreach (MasterData m in filteredData)
                    {
                        searchData.Add(new DropDown { Id = m.Id, ItemName = m.ItemName });
                    }

                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }
                return searchData;
            });
        }

        public async Task<List<string>> GetChannelType(string channelid)
        {
            List<string> channelType = new List<string>();
            OracleConnection conn = null;
            return await Task.Run(() =>
            {
                try
                {
                        string sql = "select channel_type from FIN_channel_master_vw where channel_id='" + channelid + "'";
                        using (OracleCommand cmd = new OracleCommand(sql, conn))
                        {
                            conn.Open();
                            OracleDataReader oracleDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                            while (oracleDataReader.Read())
                            {
                                channelType.Add(oracleDataReader["channel_type"].ToString());
                            }
                        }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return channelType;
            });
        }

        public async Task<List<DropDown>> GetAccountNameFullfilment()
        {
            List<string> accountname = new List<string>();
            List<DropDown> lstAccountName = new List<DropDown>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();
                        string sql = "select distinct account_name,ACCOUNT_ID from account_vw ORDER BY account_name";
                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            if (!string.IsNullOrEmpty(reader["ACCOUNT_NAME"].ToString()) && !string.IsNullOrEmpty(reader["ACCOUNT_ID"].ToString()))
                                accountname.Add(reader["ACCOUNT_NAME"].ToString() + "(" + reader["ACCOUNT_ID"].ToString() + ")");
                        }
                        connection.Close();


                        for (int i = 1; i < accountname.Count; i++)
                        {
                            lstAccountName.Add(new DropDown { Id = i.ToString(), ItemName = accountname[i].ToString() });
                        }

                        lstAccountName.Add(new DropDown { Id = "0", ItemName = "--SELECT--" });
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }
                return lstAccountName;
            });
        }

        public async Task<List<DropDown>> GetGeoCountries()
        {
            List<DropDown> lstGeoCountries = new List<DropDown>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();
                        string sql = "select GEO_ID, GEO_ID||'('||NAME||')' GEO_VALUE from mst_geography_vw where sys_source ='LIBERTY' and geo_type ='COUNTRY' order by GEO_ID||'('||NAME||')'";
                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            lstGeoCountries.Add(new DropDown { Id = reader["GEO_ID"].ToString(), ItemName = reader["GEO_VALUE"].ToString() });
                        }
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }
                return lstGeoCountries;
            });
        }

        public async Task<List<DropDown>> GetChannelSegments()
        {
            List<DropDown> lstChannelSegments = new List<DropDown>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();
                        string sql = "SELECT CHANNEL_ID, CHANNEL_ID ||'('|| NAME ||')' Name from FIN_CHANNEL_MASTER_VW WHERE CHANNEL_TYPE = 'SEGMENT'  ORDER BY CHANNEL_ID";
                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            lstChannelSegments.Add(new DropDown { Id = reader["CHANNEL_ID"].ToString(), ItemName = reader["Name"].ToString() });
                        }
                        connection.Close();
                    }

                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }
                return lstChannelSegments;
            });
        }

        public async Task<List<GeoTreeData>> GetGeoTreeData()
        {
            List<GeoTreeData> lstGeoTreeData = new List<GeoTreeData>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();
                        string sql = "select distinct RGN_DESC,RGN_CODE,SUB_RGN_DESC,SUB_RGN_CODE,AREA_DESC,AREA_CODE,CTRY_DESC,CTRY_CODE from mst_geography_flat_vw";
                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            lstGeoTreeData.Add(new GeoTreeData {
                                AllRegion = "ALL_GEO",
                                AllRegionId = "0",
                                Region = Convert.ToString(reader["RGN_DESC"]),
                                RegionId = Convert.ToString(reader["RGN_CODE"]),
                                SubRegion = Convert.ToString(reader["SUB_RGN_DESC"]),
                                SubRegionId = Convert.ToString(reader["SUB_RGN_CODE"]),
                                Area = Convert.ToString(reader["AREA_DESC"]),
                                AreaId = Convert.ToString(reader["AREA_CODE"]),
                                Country = Convert.ToString(reader["CTRY_DESC"]),
                                CountryId = Convert.ToString(reader["CTRY_CODE"])
                            });
                        }
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }
                return lstGeoTreeData;
            });
        }

        public async Task<List<ProdTreeData>> GetProductTreeData(string strString)
        {
            List<ProdTreeData> lstProdTreeData = new List<ProdTreeData>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();
                        //  string sql = "select distinct LOB_NAME,LOB_CODE,FMLY_PARNT_NAME,FMLY_PARNT_CODE,BASE_PROD_DESC,BASE_PROD_CODE from plan_product_Flat_Vw where LOB_CODE like '%" + strString.ToUpper() + "%' or LOB_NAME like '%" + strString.ToUpper() + "%' AND BASE_PROD_DESC IS not NULL";
                        string sql = "select distinct LOB_NAME,LOB_CODE,FMLY_PARNT_NAME,FMLY_PARNT_CODE,BASE_PROD_DESC,BASE_PROD_CODE from plan_product_Flat_Vw where base_prod_Desc is not null  and (LOB_CODE like '%" + strString.ToUpper() + "%'  or LOB_NAME like '%" + strString.ToUpper() + "%') ";



                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            lstProdTreeData.Add(new ProdTreeData
                            {
                                AllProduct = "ALL_PRODUCT",
                                AllProductId = "0",
                                LOBProd = Convert.ToString(reader["LOB_NAME"]),
                                LOBProdId = Convert.ToString(reader["LOB_CODE"]),
                                FamilyParent = Convert.ToString(reader["FMLY_PARNT_NAME"]),
                                FamilyParentId = Convert.ToString(reader["FMLY_PARNT_CODE"]),
                                BaseProd = Convert.ToString(reader["BASE_PROD_DESC"]),
                                BaseProdId = Convert.ToString(reader["BASE_PROD_CODE"])
                            });
                        }
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }


                return lstProdTreeData;
            });
        }

        public async Task<List<FhcTreeData>> GetProductFHCTreeData(string strString)
        {
            List<FhcTreeData> lstFHCTreeData = new List<FhcTreeData>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();
                        string sql = "select distinct a.fhc_id, a.fhc_id||'('||NAME||')' FHC, b.FGA_ID,b.FGA_ID ||'('|| SUBSTR(NAME,1,30)||')' FGA, A.SSC_ID from DELL_CM_FP_FHC_FGA_MAP_VW A, DELL_CM_FP_FHC_FGA_MAP_VW B, MST_ITEM_VW c where a.fhc_id = b.fhc_id and a.fhc_id = c.item_id and a.FHC_ID like '%" + strString.ToUpper() + "%'";
                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            lstFHCTreeData.Add(new FhcTreeData
                            {
                                AllFHC = "ALL FHC",
                                AllFHCId = "0",
                                FHC = Convert.ToString(reader["FHC"]),
                                FHCId = Convert.ToString(reader["fhc_id"]),
                                Fga = Convert.ToString(reader["FGA"]),
                                FgaId = Convert.ToString(reader["FGA_ID"]),
                            });
                        }
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return lstFHCTreeData;
            });
        }

        public async Task<List<FgaTreeData>> GetProductFGATreeData(string strString)
        {
            List<FgaTreeData> lstFgaTreeData = new List<FgaTreeData>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();
                        string sql = "SELECT distinct fga_id, fga_id ||'('|| SUBSTR(NAME,1,30)||')' FGA FROM DELL_CM_FP_FHC_FGA_MAP_VW A, MST_ITEM_VW B where a.FGA_ID is not null and a.ssc_id in ('BTS', 'BTP') and a.fga_id = b.item_id(+) and a.fga_id like '%" + strString.ToUpper() + "%' order by FGA_ID";
                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            lstFgaTreeData.Add(new FgaTreeData
                            {
                                AllFga = "FGA",
                                AllFgaId = "0",
                                Fga = Convert.ToString(reader["FGA"]),
                                FgaId = Convert.ToString(reader["fga_id"]),
                            });
                        }
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return lstFgaTreeData;
            });
        }

        public async Task<List<ChannelTreeData>> GetChannelTreeData()
        {
            List<ChannelTreeData> lstchannelTreeData = new List<ChannelTreeData>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();
                        string sql = "SELECT distinct SBU_ABBR, SBU_CODE, SBU_ABBR|| '(' || SBU_CODE || ')' sbu_display, seg_grp_code, SEG_GRP_ABBR,SEG_GRP_ABBR || '(' || seg_grp_code || ')' seg_grp_display,seg_code,seg_abbr,seg_abbr || '(' || seg_code || ')' seg_display FROM FIN_channel_flat_vw ORDER BY 3";

                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            lstchannelTreeData.Add(new ChannelTreeData
                            {
                                AllChannel = "SACHA(TOTSLS)-ALL_CHANNEL",
                                AllChannelId = "SACHA",
                                GroupId = Convert.ToString(reader["SBU_CODE"]),
                                GroupName = Convert.ToString(reader["SBU_ABBR"]),
                                ChannelId = Convert.ToString(reader["SEG_GRP_CODE"]),
                                ChannelName = Convert.ToString(reader["SEG_GRP_ABBR"]),
                                BaselId = Convert.ToString(reader["SEG_CODE"]),
                                BaseName = Convert.ToString(reader["SEG_ABBR"]),
                            });
                        }
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return lstchannelTreeData;
            });
        }



        public async Task<DataSet> GetFulFillmentSearchResult(FulFillmentSearchInput searchInput)
        {
            List<FulfillmentDetail> lstFulfillmentDetails = new List<FulfillmentDetail>();
            DataSet ds = new DataSet();
            string productType = null;
            string geoType1 = string.Empty;
            string channelType1 = string.Empty;
            string lobType = "LOB";
            string fpType = "FAMPAR";
            string baseType = "BASE";
            DataSet dtResult = new DataSet();

            //set model parameters value 
            string channelid = string.Empty;
            string channelType = string.Empty;
            string geographyid = string.Empty;
            string geoType = string.Empty;
            string productid = string.Empty;
            string lookupproduct = string.Empty;
            string ssc = string.Empty;
            bool useShipVia = false;
            string shipVia = string.Empty;
            string accountID = string.Empty;
            string item = string.Empty;
            string itemType = string.Empty;

            
            channelType = searchInput.ChannelType;
            if(searchInput.Channelid.Contains("(") && searchInput.ChannelType.ToUpper() == "SEGMENT")
            {
                channelid = searchInput.Channelid.Split('(')[0];
            }
            else
            {
                channelid = searchInput.Channelid;
            }
            if(searchInput.Geoid.Contains("(") && searchInput.GeoType.ToUpper() == "COUNTRY")
            {
                geographyid = searchInput.Geoid.Split('(')[0];
            }
            else
            {
                geographyid = searchInput.Geoid;
            }
            
            geoType = searchInput.GeoType;
            productid = searchInput.StrProductId;
            lookupproduct = searchInput.Lookupproduct;
            ssc = searchInput.Ssc;
            useShipVia = searchInput.UseShipVia;
            shipVia = searchInput.ShipVia;
            accountID = searchInput.StrsearcAccountID;
            item = searchInput.Item;
            itemType = searchInput.ItemType;

            return await Task.Run(() =>
            {
                try
                {
                    using (var conn = new OracleConnection(connenctionString))
                    {
                        conn.Open();

                        if (channelid != "ALL")
                        {
                            StringBuilder sqlQuery3 = new StringBuilder("select channel_group_type from FIN_channel_group_detail_vw where channel_group_id =  '");
                            sqlQuery3.Append(channelid);
                            sqlQuery3.Append("'");
                            OracleCommand oracleCommand2 = new OracleCommand(sqlQuery3.ToString(), conn);
                            Object temp2 = oracleCommand2.ExecuteScalar();
                            channelType1 = temp2.ToString();
                        }

                        string sqlselect = " select DISTINCT RH.ROUTING_ID, RL.FULFILLMENT_SITE_ID, RL.SPLIT from  ROUTING_HEADER_VW RH,ROUTING_LINES_VW RL WHERE RH.ROUTING_ID=RL.ROUTING_ID ";

                        string sqlselect1 = " SELECT DISTINCT  ROUTING_ID, PRODUCT_ID, ITEM,TO_CHAR(EFFECTIVE_START_DATE, 'MM/DD/YYYY') EFFECTIVE_START_DATE,TO_CHAR(EFFECTIVE_END_DATE, 'MM/DD/YYYY') EFFECTIVE_END_DATE, "
                                          + " DECODE(ITEM,NULL, (SELECT NAME || '(' || product_id || ')' from MST_PRODUCT_MASTER_VW where product_id = a.PRODUCT_ID),PRODUCT_ID,NULL,ITEM||'('||(SELECT NAME FROM MST_ITEM_VW WHERE ITEM = ITEM_ID) ||')') PRODUCTFGA,"
                                          + " a.channel_id CHANNEL_ID, "
                                          + "  CHANNEL_ID || '(' || ( SELECT distinct NAME FROM FIN_CHANNEL_MASTER_VW WHERE channel_id = a.channel_id ) || ')' CHANNEL_NAME, GEOGRAPHY_ID, "
                                          + "  GEOGRAPHY_ID || '(' || ( SELECT DISTINCT NAME FROM MST_GEOGRAPHY_VW  WHERE GEO_ID = a.GEOGRAPHY_ID and sys_source='LIBERTY' and rownum < 2 ) || ')' GEOGRAPHY_NAME, "
                                          + "  SSC, SHIP_VIA,ACCOUNT_NAME,ACCOUNT_ID,   sys_last_modified_by, sys_last_modified_date,CHANNEL_LEVEL,GEOGRAPHY_LEVEL,ID FROM(  SELECT ROUTING_ID,   PRODUCT_ID || ITEM ProductFGA,"
                                          + "  decode(PRODUCT_ID, pre_PRODUCT_ID, '1','0') FLAG, PRODUCT_ID, ITEM,  type, CHANNEL_ID, GEOGRAPHY_ID, SSC, SHIP_VIA,ACCOUNT_NAME,ACCOUNT_ID, geography_level, channel_level, sys_last_modified_by, "
                                          + "  sys_last_modified_date,effective_start_date,effective_end_date,ID from (  select  a.ROUTING_ID, a.PRODUCT_ID || a.ITEM ProductFga, lag(a.PRODUCT_ID, 1,0) over(order by a.PRODUCT_ID) as pre_PRODUCT_ID, a.PRODUCT_ID,  "
                                          + "  a.ITEM, a.type, a.channel_level, a.geography_level, a.channel_id, "
                                          + "  a.geography_id, a.ship_via,a.account_name,a.account_id, a.ssc, a.sys_last_modified_by, a.sys_last_modified_date,a.effective_start_date,a.effective_end_date,a.ID from routing_header_vw a ";

                        if (channelType == "CHANNEL_TREE" && channelType != "ALL_CHANNEL" && channelType1 != "SEGMENT" && channelid != "ALL")
                        {


                            sqlselect1 += " ,("
                                                            + "  select decode(channel_group_id, channel_id, null, channel_group_id) channel_group_id, channel_id,"
                                                            + "   decode(channel_group_type, type, null, channel_group_type) channel_group_type , type"
                                                            + "            from FIN_channel_group_detail_vw"
                                                            + "      connect by NOCYCLE prior channel_id = channel_group_id"
                                                            + "      start with channel_group_id = '" + channelid + "' and channel_group_type = '" + channelType1 + "'"
                                                            + "    ) d";
                        }



                        if (!string.IsNullOrEmpty(geoType) && geoType != "ALL_GEO" && geoType != "COUNTRY")
                        {


                            sqlselect1 += "  , (  select  substr(geo_group_id_type,0,instr(geo_group_id_type,'_')-1) geo_group_id, "
                                            + " substr(geo_group_id_type,instr(geo_group_id_type,'_')+1) geo_group_type, "
                                            + " substr(geo_id_type,0,instr(geo_id_type,'_')-1) geo_id, "
                                            + " substr(geo_id_type,instr(geo_id_type,'_')+1) geo_type,  "
                                            + " geo_group_id_type,geo_id_type from "
                                            + "  ( select decode(geo_group_id||'_'||geo_group_type, geo_id||'_'||geo_type, null, geo_group_id||'_'||geo_group_type) geo_group_id_type, "
                                            + "   geo_id||'_'||geo_type geo_id_type "
                                            + "  from mst_geo_group_detail_Vw where sys_source='LIBERTY'  ) "
                                            + "    connect by NOCYCLE prior geo_id_type    = geo_group_id_type "
                                            + "   start with geo_group_id_type = '" + geographyid + "_" + geoType + "'  )  "
                                            + "  e ";

                        }
                        sqlselect1 += " where ROUTING_ID=ROUTING_ID";
                        if (!string.IsNullOrEmpty(channelid) && channelid != "ALL" && channelType1 == "SEGMENT")
                        {

                            sqlselect1 += " And a.CHANNEL_ID = '" + channelid + "'";
                            sqlselect += " And RH.CHANNEL_ID = '" + channelid + "'";
                        }
                        if (channelid == "ALL" && channelType == "SEGMENT")
                        {
                            sqlselect1 += " And CHANNEL_LEVEL='" + channelType + "'";
                            sqlselect += " And RH.CHANNEL_LEVEL='" + channelType + "'";
                        }
                        if (!string.IsNullOrEmpty(geographyid) && geographyid != "ALL" && geoType == "COUNTRY")
                        {

                            sqlselect1 += " And  GEOGRAPHY_ID = '" + geographyid + "' and GEOGRAPHY_LEVEL='" + geoType + "'";
                            sqlselect += " And  RH.GEOGRAPHY_ID = '" + geographyid + "' and RH.GEOGRAPHY_LEVEL='" + geoType + "'";
                        }
                        if (geographyid == "ALL" && geoType == "COUNTRY")
                        {
                            sqlselect1 += " And GEOGRAPHY_LEVEL='" + geoType + "'";
                            sqlselect += " And RH.GEOGRAPHY_LEVEL='" + geoType + "'";
                        }

                        if (channelType == "CHANNEL_TREE" && channelType != "ALL" && channelType1 != "SEGMENT")
                        {
                            sqlselect1 += " and a.channel_id = d.channel_group_id and channel_level = d.channel_group_type";
                        }

                        if (!string.IsNullOrEmpty(geoType) && geoType != "ALL_GEO" && geoType != "COUNTRY")
                        {
                            sqlselect1 += " and geography_id = e.geo_group_id";  //ToDO:and geography_level = e.geo_group_type ";
                        }

                        // Look up Product Queries starts here.

                        if (!string.IsNullOrEmpty(lookupproduct) && lookupproduct != "ALL")
                        {
                            if (!string.IsNullOrEmpty(productid) && productid != "ALL_PRODUCT")
                            {
                                if (productid != "ALL_PRODUCT")
                                {
                                    StringBuilder sqlQuery = new StringBuilder("select product_group_type from mst_product_group_vw where product_group_id='");
                                    sqlQuery.Append(productid);
                                    sqlQuery.Append("'");
                                    OracleCommand oracleCommand = new OracleCommand(sqlQuery.ToString(), conn);
                                    Object temp = oracleCommand.ExecuteScalar();

                                    if (temp == null || temp == "" || temp == string.Empty)
                                    {
                                        StringBuilder sqlQueryB = new StringBuilder("select PRODUCT_TYPE from MST_PRODUCT_GROUP_DETAIL_VW where product_id='");
                                        sqlQueryB.Append(productid);
                                        sqlQueryB.Append("'");
                                        OracleCommand oracleCommandB = new OracleCommand(sqlQueryB.ToString(), conn);
                                        //conn.Open();
                                        temp = oracleCommandB.ExecuteScalar();
                                    }

                                    productType = temp.ToString();

                                    //Base Code Query 
                                    if (productType.Equals(baseType))
                                    {
                                        string strQueryB = "  select distinct rr.routing_id, rr.product_id, rr.item,TO_CHAR(rr.effective_start_date, 'MM/DD/YYYY') effective_start_date,TO_CHAR(rr.effective_end_date,'MM/DD/YYYY') effective_end_date,rr.productfga, rr.channel_id, rr.channel_name, rr.geography_id, rr.geography_name,  "
                                                   + " rr.ssc, rr.ship_via,rr.account_name,rr.account_id, rr.sys_last_modified_by, rr.sys_last_modified_date, rr.channel_level, rr.geography_level,ID from  "
                                                  + " ( SELECT DISTINCT  ROUTING_ID, PRODUCT_ID, ITEM,EFFECTIVE_START_DATE,EFFECTIVE_END_DATE,ID,DECODE(ITEM,NULL, (SELECT NAME || '(' || product_id || ')'  from MST_PRODUCT_MASTER_VW  "
                                                  + "  where product_id = a.PRODUCT_ID),PRODUCT_ID,NULL,ITEM||'('||(SELECT NAME FROM MST_ITEM_VW WHERE ITEM = ITEM_ID) ||')') PRODUCTFGA, "
                                                   + "  a.channel_id CHANNEL_ID,  CHANNEL_ID || '(' || ( SELECT distinct NAME FROM FIN_CHANNEL_MASTER_VW WHERE channel_id = a.channel_id ) || ')' CHANNEL_NAME, "
                                                    + " GEOGRAPHY_ID, GEOGRAPHY_ID || '(' || ( SELECT DISTINCT NAME FROM MST_GEOGRAPHY_VW   WHERE GEO_ID = a.GEOGRAPHY_ID and sys_source='LIBERTY' and rownum < 2 ) || ')' GEOGRAPHY_NAME, "
                                                    + "   SSC, SHIP_VIA,ACCOUNT_NAME,ACCOUNT_ID,   sys_last_modified_by, sys_last_modified_date,CHANNEL_LEVEL,GEOGRAPHY_LEVEL FROM  (  SELECT ROUTING_ID,   PRODUCT_ID || ITEM ProductFGA, "
                                                       + " decode(PRODUCT_ID, pre_PRODUCT_ID, '1','0') FLAG, PRODUCT_ID, ITEM,  type, CHANNEL_ID, GEOGRAPHY_ID, SSC, SHIP_VIA,ACCOUNT_NAME,ACCOUNT_ID, geography_level, channel_level, sys_last_modified_by, "
                                                        + "  sys_last_modified_date,effective_start_date,effective_end_date,ID from ( "
                                                         + "   select  a.ROUTING_ID, a.PRODUCT_ID || a.ITEM ProductFga, lag(a.PRODUCT_ID, 1,0) over(order by a.PRODUCT_ID) as pre_PRODUCT_ID, a.PRODUCT_ID, "
                                                           + "  a.ITEM, a.type, a.channel_level, a.geography_level, a.channel_id,   a.geography_id, a.ship_via,a.account_name,a.account_id, a.ssc, a.sys_last_modified_by, a.sys_last_modified_date,a.effective_start_date,a.effective_end_date,ID from routing_header_vw a  "
                                                           + "   where a.product_id = '" + productid + "' )  ) A )  rr ";

                                        if (channelType == "CHANNEL_TREE" && channelType != "ALL" && channelType1 != "SEGMENT")
                                        {


                                            strQueryB += " ,("
                                                                 + "  select decode(channel_group_id, channel_id, null, channel_group_id) channel_group_id, channel_id,"
                                                                 + "   decode(channel_group_type, type, null, channel_group_type) channel_group_type , type"
                                                                 + "            from FIN_channel_group_detail_vw"
                                                                 + "      connect by NOCYCLE prior channel_id = channel_group_id"
                                                                 + "      start with channel_group_id = '" + channelid + "' and channel_group_type = '" + channelType1 + "'"
                                                                 + "    ) d";
                                        }

                                        if (!string.IsNullOrEmpty(geoType) && geoType != "ALL_GEO" && geoType != "COUNTRY")
                                        {

                                            strQueryB += " , (  select  substr(geo_group_id_type,0,instr(geo_group_id_type,'_')-1) geo_group_id, "
                                                + " substr(geo_group_id_type,instr(geo_group_id_type,'_')+1) geo_group_type, "
                                                + " substr(geo_id_type,0,instr(geo_id_type,'_')-1) geo_id, "
                                                + " substr(geo_id_type,instr(geo_id_type,'_')+1) geo_type,  "
                                                + " geo_group_id_type,geo_id_type from "
                                                + "  ( select decode(geo_group_id||'_'||geo_group_type, geo_id||'_'||geo_type, null, geo_group_id||'_'||geo_group_type) geo_group_id_type, "
                                                + "   geo_id||'_'||geo_type geo_id_type "
                                                + "  from mst_geo_group_detail_Vw where sys_source='LIBERTY'  ) "
                                                + "    connect by NOCYCLE prior geo_id_type    = geo_group_id_type "
                                                + "   start with geo_group_id_type = '" + geographyid + "_" + geoType + "' )  "
                                                + "  e ";
                                        }
                                        strQueryB += "  where rr.ROUTING_ID = rr.ROUTING_ID";
                                        if (channelType == "CHANNEL_TREE" && channelType != "ALL" && channelType1 != "SEGMENT")
                                        {
                                            strQueryB += " and rr.channel_id = d.channel_group_id and rr.channel_level = d.channel_group_type";
                                        }
                                        if (!string.IsNullOrEmpty(geoType) && geoType != "ALL_GEO" && geoType != "COUNTRY")
                                        {
                                            strQueryB += " and rr.geography_id = e.geo_group_id";  //ToDo:and rr.geography_level = e.geo_group_type ";
                                        }

                                        if (!string.IsNullOrEmpty(geographyid) && geographyid != "ALL" && geoType == "COUNTRY")
                                        {

                                            sqlselect1 += " And rr.GEOGRAPHY_ID = '" + geographyid + "' and rr.GEOGRAPHY_LEVEL='" + geoType + "'";
                                            sqlselect += " And  RH.GEOGRAPHY_ID = '" + geographyid + "' and RH.GEOGRAPHY_LEVEL='" + geoType + "'";
                                        }
                                        if (channelid != "ALL")
                                        {
                                            //strQueryB += " AND rr.CHANNEL_ID  = '" + channelid + "' ";
                                            sqlselect += " And RH.CHANNEL_ID = '" + channelid + "'  and RH.product_id ='" + productid + "' ";
                                        }
                                        if (geographyid == "ALL" && geoType == "COUNTRY")
                                        {
                                            sqlselect1 += " And rr.GEOGRAPHY_LEVEL='" + geoType + "'";
                                            sqlselect += " And RH.GEOGRAPHY_LEVEL='" + geoType + "'";
                                        }
                                        if (channelid == "ALL" && channelType == "SEGMENT")
                                        {
                                            sqlselect1 += " And rr.CHANNEL_LEVEL='" + channelType + "'";
                                            sqlselect += " And RH.CHANNEL_LEVEL='" + channelType + "'";
                                        }

                                        if (ssc != "ALL")
                                        {
                                            strQueryB += " AND rr.SSC = '" + ssc + "' ";
                                            sqlselect += " AND RH.SSC = '" + ssc + "'";
                                        }
                                        if (useShipVia)
                                        {
                                            if (ssc == "BTP")
                                            {
                                                if (!string.IsNullOrEmpty(shipVia))
                                                {
                                                    strQueryB += " AND rr.SHIP_VIA  = '" + shipVia + "' ";
                                                    sqlselect += " AND RH.SHIP_VIA = '" + shipVia + "' ";
                                                }
                                                else
                                                {
                                                    strQueryB += " AND rr.SHIP_VIA IS NULL ";
                                                    sqlselect += " AND RH.SHIP_VIA IS NULL ";
                                                }

                                            }
                                        }

                                        if (ssc == "BTP")
                                        {

                                            if (!string.IsNullOrEmpty(accountID))
                                            {
                                                strQueryB += " AND rr.ACCOUNT_ID  = '" + accountID + "' ";
                                                sqlselect += " AND RH.ACCOUNT_ID = '" + accountID + "' ";
                                            }
                                            else
                                            {
                                                strQueryB += " AND rr.ACCOUNT_ID IS NULL ";
                                                sqlselect += " AND RH.ACCOUNT_ID IS NULL ";
                                            }
                                        }

                                        strQueryB += " order by GEOGRAPHY_ID,CHANNEL_ID,PRODUCTFGA,SSC,nvl(to_date(effective_start_date,'mm/dd/yyyy'),to_date('1/1/1900','mm/dd/yyyy'))"; //Order by updated by Kanishka for the multi site.
                                        OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(strQueryB, conn);
                                        oracleDataAdapter.Fill(dtResult, "ROUTING_HEADER_VW");
                                        //Microsoft.Security.Application.AntiXss.HtmlEncode(dtResult.ToString());
                                        if (dtResult.Tables[0] != null)
                                        {
                                            foreach (DataRow dr in dtResult.Tables[0].Rows)
                                            {
                                                if (dr["GEOGRAPHY_ID"].ToString() == "ALL_GEO")
                                                {
                                                    dr["GEOGRAPHY_NAME"] = "ALL_GEO";
                                                }
                                                if (dr["CHANNEL_ID"].ToString() == "ALL_CHANNEL")
                                                {
                                                    dr["CHANNEL_NAME"] = "ALL_CHANNEL";
                                                }
                                                if (dr["PRODUCT_ID"].ToString() == "ALL_PRODUCT")
                                                {
                                                    dr["ProductFGA"] = "ALL_PRODUCT";
                                                }
                                            }
                                        }
                                        OracleDataAdapter da = new OracleDataAdapter(sqlselect, conn);
                                        da.Fill(dtResult, "ROUTING_LINES_VW");
                                        //Microsoft.Security.Application.AntiXss.HtmlEncode(dtResult.ToString());
                                        return dtResult;




                                    }
                                    else if (productType.Equals(fpType))
                                    {
                                        //string strQuery1 = "  select distinct rr.routing_id, rr.product_id, rr.item,TO_CHAR(rr.effective_start_date, 'MM/DD/YYYY') effective_start_date,TO_CHAR(rr.effective_end_date,'MM/DD/YYYY') effective_end_date,rr.productfga, rr.channel_id, rr.channel_name, rr.geography_id, rr.geography_name,  "
                                        //           + " rr.ssc, rr.ship_via,rr.account_name,rr.account_id, rr.sys_last_modified_by, rr.sys_last_modified_date, rr.channel_level, rr.geography_level,ID from  "
                                        //          + " ( SELECT DISTINCT  ROUTING_ID, PRODUCT_ID, ITEM,EFFECTIVE_START_DATE,EFFECTIVE_END_DATE,ID,DECODE(ITEM,NULL, (SELECT NAME || '(' || product_id || ')'  from MST_PRODUCT_MASTER_VW  "
                                        //          + "  where product_id = a.PRODUCT_ID),PRODUCT_ID,NULL,ITEM||'('||(SELECT NAME FROM MST_ITEM_VW WHERE ITEM = ITEM_ID) ||')') PRODUCTFGA, "
                                        //           + "  a.channel_id CHANNEL_ID,  CHANNEL_ID || '(' || ( SELECT distinct NAME FROM MST_CHANNEL_MASTER_VW WHERE channel_id = a.channel_id ) || ')' CHANNEL_NAME, "
                                        //            + " GEOGRAPHY_ID, GEOGRAPHY_ID || '(' || ( SELECT DISTINCT NAME FROM MST_GEOGRAPHY_VW   WHERE GEO_ID = a.GEOGRAPHY_ID and sys_source='LIBERTY' and rownum < 2 ) || ')' GEOGRAPHY_NAME, "
                                        //            + "   SSC, SHIP_VIA,ACCOUNT_NAME,ACCOUNT_ID,   sys_last_modified_by, sys_last_modified_date,CHANNEL_LEVEL,GEOGRAPHY_LEVEL FROM  (  SELECT ROUTING_ID,   PRODUCT_ID || ITEM ProductFGA, "
                                        //               + " decode(PRODUCT_ID, pre_PRODUCT_ID, '1','0') FLAG, PRODUCT_ID, ITEM,  type, CHANNEL_ID, GEOGRAPHY_ID, SSC, SHIP_VIA,ACCOUNT_NAME,ACCOUNT_ID, geography_level, channel_level, sys_last_modified_by, "
                                        //                + "  sys_last_modified_date,effective_start_date,effective_end_date,ID from ( "
                                        //                 + "   select  a.ROUTING_ID, a.PRODUCT_ID || a.ITEM ProductFga, lag(a.PRODUCT_ID, 1,0) over(order by a.PRODUCT_ID) as pre_PRODUCT_ID, a.PRODUCT_ID, "
                                        //                   + "  a.ITEM, a.type, a.channel_level, a.geography_level, a.channel_id,   a.geography_id, a.ship_via,a.account_name,a.account_id, a.ssc, a.sys_last_modified_by, a.sys_last_modified_date,a.effective_start_date,a.effective_end_date,ID from routing_header_vw a  "
                                        //                   + "   where a.product_id = '" + productid + "' or a.item in ( select fhc_id from dell_cm_fp_fhc_fga_map_vw where fmly_parnt_code = '" + productid + "')   or a.item in "
                                        //                    + "   ( select fga_id from dell_cm_fp_fhc_fga_map_vw where fmly_parnt_code = '" + productid + "') )  ) A )  rr ";

                                        //string strQuery1 = "  select distinct rr.ROUTING_ID, rr.pre_PRODUCT_ID,  rr.PRODUCT_ID, DECODE(ITEM,NULL, (SELECT NAME || '(' || product_id || ')'  from MST_PRODUCT_MASTER_VW  "
                                        //                       + " where product_id = rr.PRODUCT_ID),PRODUCT_ID,NULL,ITEM||'('||(SELECT NAME FROM MST_ITEM_VW WHERE ITEM = ITEM_ID) ||')') PRODUCTFGA,   rr.ITEM,TO_CHAR(rr.effective_start_date, 'MM/DD/YYYY') effective_start_date,TO_CHAR(rr.effective_end_date, 'MM/DD/YYYY') effective_end_date, rr.type, rr.channel_level, rr.geography_level, rr.channel_id, "
                                        //                      + " rr.CHANNEL_ID || '(' || ( SELECT distinct NAME FROM MST_CHANNEL_MASTER_VW WHERE channel_id = rr.channel_id ) || ')' CHANNEL_NAME, rr.geography_id, rr.GEOGRAPHY_ID || '(' || "
                                        //                         + " ( SELECT DISTINCT NAME FROM MST_GEOGRAPHY_VW   WHERE GEO_ID = rr.GEOGRAPHY_ID and sys_source='LIBERTY' and rownum < 2 ) || ')' GEOGRAPHY_NAME, rr.ship_via,rr.account_name,rr.account_id, rr.ssc, "
                                        //                       + " rr.sys_last_modified_by,  rr.sys_last_modified_date,rr.ID from  (  ( select a.ROUTING_ID, a.PRODUCT_ID || a.ITEM ProductFga, lag(a.PRODUCT_ID, 1,0) over(order by a.PRODUCT_ID) as "
                                        //                       + "  pre_PRODUCT_ID,  a.PRODUCT_ID,  a.ITEM, a.type, a.channel_level, a.geography_level, a.channel_id, a.geography_id,  "
                                        //                         + " a.ship_via,a.account_name,a.account_id, a.ssc,"
                                        //                          + "  a.sys_last_modified_by, a.sys_last_modified_date,a.effective_start_date,a.effective_end_date,a.ID  from routing_header_vw a,  ( select product_id, 'PRODUCT_TREE' type from (    select product_id, product_type  from (  "
                                        //                             + "     select product_type, product_group_type, product_group_id, decode(product_id, product_group_id, null, product_id) product_id    "
                                        //                              //+ "   from mst_product_group_detail_vw a where product_type not in ('BASE','FAMILY')    )     connect by prior product_id = product_group_id  start with product_id = '" + productid + "'   )   ) c "
                                        //                                + "   from mst_product_group_detail_vw a where product_type in ('LOB','FAMPAR','BASE','FAMILY')    )     connect by prior product_id = product_group_id  start with product_id = '" + productid + "'   )   ) c "
                                        //                              + "     where a.product_id = c.product_id and a.type=c.type  )    union                     select a.ROUTING_ID, a.PRODUCT_ID || a.ITEM ProductFga, lag(a.PRODUCT_ID, 1,0) "
                                        //                                 + "   over(order by a.PRODUCT_ID) as pre_PRODUCT_ID,   a.PRODUCT_ID,  a.ITEM, a.type, a.channel_level, a.geography_level, a.channel_id, a.geography_id, a.ship_via,a.account_name,a.account_id, a.ssc, "
                                        //                                  + "   a.sys_last_modified_by, a.sys_last_modified_date,a.effective_start_date,a.effective_end_date,a.ID  from routing_header_vw a where a.item in   (  select fga_Id from dell_cm_fp_fhc_fga_map_vw where fmly_parnt_code in "
                                        //                                  + "     (  select product_id from mst_product_group_detail_vw where product_group_id = '" + productid + "' and product_group_type ='FAMPAR'))   or a.item in "
                                        //                                    + "     (  select fhc_Id from dell_cm_fp_fhc_fga_map_vw where fmly_parnt_code in (      select product_id from mst_product_group_detail_vw where product_group_id = '" + productid + "' "
                                        //                                     + "    and product_group_type ='FAMPAR') ) ) rr ";


                                        string strQuery1 = "  select distinct rr.ROUTING_ID, rr.pre_PRODUCT_ID,  rr.PRODUCT_ID, DECODE(ITEM,NULL, (SELECT NAME || '(' || product_id || ')'  from MST_PRODUCT_MASTER_VW  "
                                                               + " where product_id = rr.PRODUCT_ID),PRODUCT_ID,NULL,ITEM||'('||(SELECT NAME FROM MST_ITEM_VW WHERE ITEM = ITEM_ID) ||')') PRODUCTFGA,   rr.ITEM,TO_CHAR(rr.effective_start_date, 'MM/DD/YYYY') effective_start_date,TO_CHAR(rr.effective_end_date, 'MM/DD/YYYY') effective_end_date, rr.type, rr.channel_level, rr.geography_level, rr.channel_id, "
                                                              + " rr.CHANNEL_ID || '(' || ( SELECT distinct NAME FROM FIN_CHANNEL_MASTER_VW WHERE channel_id = rr.channel_id ) || ')' CHANNEL_NAME, rr.geography_id, rr.GEOGRAPHY_ID || '(' || "
                                                                 + " ( SELECT DISTINCT NAME FROM MST_GEOGRAPHY_VW   WHERE GEO_ID = rr.GEOGRAPHY_ID and sys_source='LIBERTY' and rownum < 2 ) || ')' GEOGRAPHY_NAME, rr.ship_via,rr.account_name,rr.account_id, rr.ssc, "
                                                               + " rr.sys_last_modified_by,  rr.sys_last_modified_date,rr.ID from  (  ( select a.ROUTING_ID, a.PRODUCT_ID || a.ITEM ProductFga, lag(a.PRODUCT_ID, 1,0) over(order by a.PRODUCT_ID) as "
                                                               + "  pre_PRODUCT_ID,  a.PRODUCT_ID,  a.ITEM, a.type, a.channel_level, a.geography_level, a.channel_id, a.geography_id,  "
                                                                 + " a.ship_via,a.account_name,a.account_id, a.ssc,"
                                                                  + "  a.sys_last_modified_by, a.sys_last_modified_date,a.effective_start_date,a.effective_end_date,a.ID  from routing_header_vw a,  ( select product_id, 'PRODUCT_TREE' type from (    select product_id, product_type  from (  "
                                                                     + "     select product_type, product_group_type, product_group_id, decode(product_id, product_group_id, null, product_id) product_id    "
                                                                        //+ "   from mst_product_group_detail_vw a where product_type not in ('BASE','FAMILY')    )     connect by prior product_id = product_group_id  start with product_id = '" + productid + "'   )   ) c "
                                                                        + "   from mst_product_group_detail_vw a where product_type in ('LOB','FAMPAR','BASE','FAMILY')    )     connect by NOCYCLE prior product_id = product_group_id  start with product_id = '" + productid + "'   )   ) c "
                                                                      + "     where a.product_id = c.product_id and a.type=c.type  )    union                     select a.ROUTING_ID, a.PRODUCT_ID || a.ITEM ProductFga, lag(a.PRODUCT_ID, 1,0) "
                                                                         + "   over(order by a.PRODUCT_ID) as pre_PRODUCT_ID,   a.PRODUCT_ID,  a.ITEM, a.type, a.channel_level, a.geography_level, a.channel_id, a.geography_id, a.ship_via,a.account_name,a.account_id, a.ssc, "
                                                                           //+ "   a.sys_last_modified_by, a.sys_last_modified_date,a.effective_start_date,a.effective_end_date,a.ID  from routing_header_vw a where a.item in   (  select fga_Id from dell_cm_fp_fhc_fga_map_vw where fmly_parnt_code in "
                                                                           //+ "     ('" + productid + "')   or a.item in "
                                                                           // + "     (  select fhc_Id from dell_cm_fp_fhc_fga_map_vw where fmly_parnt_code = '" + productid + "') )) rr ";
                                                                           + "   a.sys_last_modified_by, a.sys_last_modified_date,a.effective_start_date,a.effective_end_date,a.ID  from routing_header_vw a where a.item in (SELECT fga_Id FROM dell_cm_fp_fhc_fga_map_vw WHERE fmly_parnt_code = '" + productid + "') OR a.item IN (SELECT fhc_Id FROM dell_cm_fp_fhc_fga_map_vw WHERE fmly_parnt_code = '" + productid + "')) rr";


                                        if (channelType == "CHANNEL_TREE" && channelType != "ALL" && channelType1 != "SEGMENT")
                                        {


                                            strQuery1 += " ,("
                                                                 + "  select decode(channel_group_id, channel_id, null, channel_group_id) channel_group_id, channel_id,"
                                                                 + "   decode(channel_group_type, type, null, channel_group_type) channel_group_type , type"
                                                                 + "            from FIN_channel_group_detail_vw"
                                                                 + "      connect by NOCYCLE prior channel_id = channel_group_id"
                                                                 + "      start with channel_group_id = '" + channelid + "' and channel_group_type = '" + channelType1 + "'"
                                                                 + "    ) d";
                                        }

                                        if (!string.IsNullOrEmpty(geoType) && geoType != "ALL_GEO" && geoType != "COUNTRY")
                                        {

                                            strQuery1 += " , (  select  substr(geo_group_id_type,0,instr(geo_group_id_type,'_')-1) geo_group_id, "
                                                + " substr(geo_group_id_type,instr(geo_group_id_type,'_')+1) geo_group_type, "
                                                + " substr(geo_id_type,0,instr(geo_id_type,'_')-1) geo_id, "
                                                + " substr(geo_id_type,instr(geo_id_type,'_')+1) geo_type,  "
                                                + " geo_group_id_type,geo_id_type from "
                                                + "  ( select decode(geo_group_id||'_'||geo_group_type, geo_id||'_'||geo_type, null, geo_group_id||'_'||geo_group_type) geo_group_id_type, "
                                                + "   geo_id||'_'||geo_type geo_id_type "
                                                + "  from mst_geo_group_detail_Vw where sys_source='LIBERTY'  ) "
                                                + "    connect by NOCYCLE prior geo_id_type    = geo_group_id_type "
                                                + "   start with geo_group_id_type = '" + geographyid + "_" + geoType + "' )  "
                                                + "  e ";
                                        }
                                        strQuery1 += "  where rr.ROUTING_ID = rr.ROUTING_ID";
                                        if (channelType == "CHANNEL_TREE" && channelType != "ALL" && channelType1 != "SEGMENT")
                                        {
                                            strQuery1 += " and rr.channel_id = d.channel_group_id and rr.channel_level = d.channel_group_type";
                                        }
                                        if (!string.IsNullOrEmpty(geoType) && geoType != "ALL_GEO" && geoType != "COUNTRY")
                                        {
                                            //TODO: make change for test 
                                            //strQuery1 += " and rr.geography_id = e.geo_group_id  and rr.geography_level = e.geo_group_type ";
                                            strQuery1 += " and rr.geography_id = e.geo_group_id"; //  and rr.geography_level = e.geo_group_type ";
                                        }

                                        if (!string.IsNullOrEmpty(geographyid) && geographyid != "ALL" && geoType == "COUNTRY")
                                        {

                                            sqlselect1 += " And rr.GEOGRAPHY_ID = '" + geographyid + "' and rr.GEOGRAPHY_LEVEL='" + geoType + "'";
                                            sqlselect += " And  RH.GEOGRAPHY_ID = '" + geographyid + "' and RH.GEOGRAPHY_LEVEL='" + geoType + "'";
                                        }
                                        if (channelid != "ALL")
                                        {
                                            //strQuery1 += " AND rr.CHANNEL_ID  = '" + channelid + "' ";
                                            sqlselect += " And RH.CHANNEL_ID = '" + channelid + "'  and RH.product_id ='" + productid + "' ";
                                        }
                                        if (geographyid == "ALL" && geoType == "COUNTRY")
                                        {
                                            sqlselect1 += " And rr.GEOGRAPHY_LEVEL='" + geoType + "'";
                                            sqlselect += " And RH.GEOGRAPHY_LEVEL='" + geoType + "'";
                                        }
                                        if (channelid == "ALL" && channelType == "SEGMENT")
                                        {
                                            sqlselect1 += " And rr.CHANNEL_LEVEL='" + channelType + "'";
                                            sqlselect += " And RH.CHANNEL_LEVEL='" + channelType + "'";
                                        }

                                        if (ssc != "ALL")
                                        {
                                            strQuery1 += " AND rr.SSC = '" + ssc + "' ";
                                            sqlselect += " AND RH.SSC = '" + ssc + "'";
                                        }
                                        if (useShipVia)
                                        {
                                            if (ssc == "BTP")
                                            {
                                                if (!string.IsNullOrEmpty(shipVia))
                                                {
                                                    strQuery1 += " AND rr.SHIP_VIA  = '" + shipVia + "' ";
                                                    sqlselect += " AND RH.SHIP_VIA = '" + shipVia + "' ";
                                                }
                                                else
                                                {
                                                    strQuery1 += " AND rr.SHIP_VIA IS NULL ";
                                                    sqlselect += " AND RH.SHIP_VIA IS NULL ";
                                                }

                                            }
                                        }

                                        if (ssc == "BTP")
                                        {

                                            if (!string.IsNullOrEmpty(accountID))
                                            {
                                                strQuery1 += " AND rr.ACCOUNT_ID  = '" + accountID + "' ";
                                                sqlselect += " AND RH.ACCOUNT_ID = '" + accountID + "' ";
                                            }
                                            else
                                            {
                                                strQuery1 += " AND rr.ACCOUNT_ID IS NULL ";
                                                sqlselect += " AND RH.ACCOUNT_ID IS NULL ";
                                            }
                                        }

                                        strQuery1 += " order by GEOGRAPHY_ID,CHANNEL_ID,PRODUCTFGA,SSC,nvl(to_date(effective_start_date,'mm/dd/yyyy'),to_date('1/1/1900','mm/dd/yyyy'))"; //Order by updated by Kanishka for the multi site.
                                        OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(strQuery1, conn);
                                        oracleDataAdapter.Fill(dtResult, "ROUTING_HEADER_VW");
                                        // Microsoft.Security.Application.AntiXss.HtmlEncode(dtResult.ToString());
                                        if (dtResult.Tables[0] != null)
                                        {
                                            foreach (DataRow dr in dtResult.Tables[0].Rows)
                                            {
                                                if (dr["GEOGRAPHY_ID"].ToString() == "ALL_GEO")
                                                {
                                                    dr["GEOGRAPHY_NAME"] = "ALL_GEO";
                                                }
                                                if (dr["CHANNEL_ID"].ToString() == "ALL_CHANNEL")
                                                {
                                                    dr["CHANNEL_NAME"] = "ALL_CHANNEL";
                                                }
                                                if (dr["PRODUCT_ID"].ToString() == "ALL_PRODUCT")
                                                {
                                                    dr["ProductFGA"] = "ALL_PRODUCT";
                                                }
                                            }
                                        }
                                        OracleDataAdapter da = new OracleDataAdapter(sqlselect, conn);
                                        da.Fill(dtResult, "ROUTING_LINES_VW");
                                        // Microsoft.Security.Application.AntiXss.HtmlEncode(dtResult.ToString());
                                        return dtResult;

                                    } //Family Parent Query ends here.

                                    else if (productType.Equals(lobType))
                                    {
                                        DataTable dtLobFampar = new DataTable();
                                        string strFilter = string.Empty;

                                        string sqlSelectQuery = "  select distinct rr.ROUTING_ID, rr.pre_PRODUCT_ID,  rr.PRODUCT_ID, DECODE(ITEM,NULL, (SELECT NAME || '(' || product_id || ')'  from MST_PRODUCT_MASTER_VW  "
                                                                + " where product_id = rr.PRODUCT_ID),PRODUCT_ID,NULL,ITEM||'('||(SELECT NAME FROM MST_ITEM_VW WHERE ITEM = ITEM_ID) ||')') PRODUCTFGA,   rr.ITEM,TO_CHAR(rr.effective_start_date, 'MM/DD/YYYY') effective_start_date,TO_CHAR(rr.effective_end_date, 'MM/DD/YYYY') effective_end_date, rr.type, rr.channel_level, rr.geography_level, rr.channel_id, "
                                                               + " rr.CHANNEL_ID || '(' || ( SELECT distinct NAME FROM FIN_CHANNEL_MASTER_VW WHERE channel_id = rr.channel_id ) || ')' CHANNEL_NAME, rr.geography_id, rr.GEOGRAPHY_ID || '(' || "
                                                                  + " ( SELECT DISTINCT NAME FROM MST_GEOGRAPHY_VW   WHERE GEO_ID = rr.GEOGRAPHY_ID and sys_source='LIBERTY' and rownum < 2 ) || ')' GEOGRAPHY_NAME, rr.ship_via,rr.account_name,rr.account_id, rr.ssc, "
                                                                + " rr.sys_last_modified_by,  rr.sys_last_modified_date,rr.ID from  (  ( select a.ROUTING_ID, a.PRODUCT_ID || a.ITEM ProductFga, lag(a.PRODUCT_ID, 1,0) over(order by a.PRODUCT_ID) as "
                                                                + "  pre_PRODUCT_ID,  a.PRODUCT_ID,  a.ITEM, a.type, a.channel_level, a.geography_level, a.channel_id, a.geography_id,  "
                                                                  + " a.ship_via,a.account_name,a.account_id, a.ssc,"
                                                                   + "  a.sys_last_modified_by, a.sys_last_modified_date,a.effective_start_date,a.effective_end_date,a.ID  from routing_header_vw a,  ( select product_id, 'PRODUCT_TREE' type from (    select product_id, product_type  from (  "
                                                                      + "     select product_type, product_group_type, product_group_id, decode(product_id, product_group_id, null, product_id) product_id    "
                                                                         //+ "   from mst_product_group_detail_vw a where product_type not in ('BASE','FAMILY')    )     connect by prior product_id = product_group_id  start with product_id = '" + productid + "'   )   ) c "
                                                                         + "   from mst_product_group_detail_vw a where product_type in ('LOB','FAMPAR','BASE','FAMILY')    )     connect by NOCYCLE prior product_id = product_group_id  start with product_id = '" + productid + "'   )   ) c "
                                                                       + "     where a.product_id = c.product_id and a.type=c.type  )    union                     select a.ROUTING_ID, a.PRODUCT_ID || a.ITEM ProductFga, lag(a.PRODUCT_ID, 1,0) "
                                                                          + "   over(order by a.PRODUCT_ID) as pre_PRODUCT_ID,   a.PRODUCT_ID,  a.ITEM, a.type, a.channel_level, a.geography_level, a.channel_id, a.geography_id, a.ship_via,a.account_name,a.account_id, a.ssc, "
                                                                           + "   a.sys_last_modified_by, a.sys_last_modified_date,a.effective_start_date,a.effective_end_date,a.ID  from routing_header_vw a where a.item in   (  select fga_Id from dell_cm_fp_fhc_fga_map_vw where fmly_parnt_code in "
                                                                           + "     (  select product_id from mst_product_group_detail_vw where product_group_id = '" + productid + "' and product_group_type ='LOB'))   or a.item in "
                                                                             + "     (  select fhc_Id from dell_cm_fp_fhc_fga_map_vw where fmly_parnt_code in (      select product_id from mst_product_group_detail_vw where product_group_id = '" + productid + "' "
                                                                              + "    and product_group_type ='LOB') ) ) rr ";


                                        if (channelType == "CHANNEL_TREE" && channelType != "ALL" && channelType1 != "SEGMENT")
                                        {
                                            sqlSelectQuery += " ,(  select decode(channel_group_id, channel_id, null, channel_group_id) channel_group_id, channel_id,  "
                                                                                  + "    decode(channel_group_type, type, null, channel_group_type) channel_group_type , type   from FIN_channel_group_detail_vw"
                                                                                   + "     connect by NOCYCLE prior channel_id = channel_group_id   start with channel_group_id = '" + channelid + "' and channel_group_type = '" + channelType1 + "'    ) d";
                                        }

                                        if (!string.IsNullOrEmpty(geoType) && geoType != "ALL_GEO" && geoType != "COUNTRY")
                                        {
                                            sqlSelectQuery += "   , (  select  substr(geo_group_id_type,0, "
                                                                  + "    instr(geo_group_id_type,'_')-1) geo_group_id,   substr(geo_group_id_type,instr(geo_group_id_type,'_')+1) geo_group_type,   substr(geo_id_type,0,instr(geo_id_type,'_')-1) geo_id, "
                                                                   + "   substr(geo_id_type,instr(geo_id_type,'_')+1) geo_type,   geo_group_id_type,geo_id_type from    ( select decode(geo_group_id||'_'||geo_group_type, geo_id||'_'||geo_type, null, "
                                                                    + "   geo_group_id||'_'||geo_group_type) geo_group_id_type,   geo_id||'_'||geo_type geo_id_type   from mst_geo_group_detail_Vw where sys_source='LIBERTY'  ) "
                                                                   + "     connect by NOCYCLE prior geo_id_type    = geo_group_id_type    start with geo_group_id_type = '" + geographyid + "_" + geoType + "' )    e";
                                        }
                                        sqlSelectQuery += " where rr.routing_id = rr.routing_id ";

                                        if (!string.IsNullOrEmpty(geoType) && geoType != "ALL_GEO" && geoType != "COUNTRY")
                                        {
                                            sqlSelectQuery += " and rr.geography_id = e.geo_group_id";    //ToDo:and rr.geography_level = e.geo_group_type ";
                                        }
                                        if (channelType == "CHANNEL_TREE" && channelType != "ALL" && channelType1 != "SEGMENT")
                                        {
                                            sqlSelectQuery += "  and rr.channel_id = d.channel_group_id  and rr.channel_level = d.channel_group_type ";
                                        }

                                        if (channelid != "ALL" && channelType1 == "SEGMENT")
                                        {
                                            //sqlSelectQuery += " AND rr.CHANNEL_ID  = '" + channelid + "' ";
                                            sqlselect += " And RH.CHANNEL_ID = '" + channelid + "'  and RH.product_id ='" + productid + "' ";
                                        }
                                        if (!string.IsNullOrEmpty(geographyid) && geographyid != "ALL" && geoType == "COUNTRY")
                                        {

                                            sqlSelectQuery += " And  rr.GEOGRAPHY_ID = '" + geographyid + "' and rr.GEOGRAPHY_LEVEL='" + geoType + "'";
                                            sqlselect += " And  RH.GEOGRAPHY_ID = '" + geographyid + "' and RH.GEOGRAPHY_LEVEL='" + geoType + "'";
                                        }
                                        if (geographyid == "ALL" && geoType == "COUNTRY")
                                        {
                                            sqlSelectQuery += " And rr.GEOGRAPHY_LEVEL='" + geoType + "'";
                                            sqlselect += " And RH.GEOGRAPHY_LEVEL='" + geoType + "'";
                                        }
                                        if (channelid == "ALL" && channelType == "SEGMENT")
                                        {
                                            sqlSelectQuery += " And rr.CHANNEL_LEVEL='" + channelType + "'";
                                            sqlselect += " And RH.CHANNEL_LEVEL='" + channelType + "'";
                                        }
                                        if (ssc != "ALL")
                                        {
                                            sqlSelectQuery += " AND rr.SSC = '" + ssc + "' ";
                                            sqlselect += " AND RH.SSC = '" + ssc + "'";
                                        }
                                        if (useShipVia)
                                        {
                                            if (ssc == "BTP")
                                            {
                                                if (!string.IsNullOrEmpty(shipVia))
                                                {
                                                    sqlSelectQuery += " AND rr.SHIP_VIA  = '" + shipVia + "' ";
                                                    sqlselect += " AND RH.SHIP_VIA = '" + shipVia + "'";
                                                }
                                                else
                                                {
                                                    sqlSelectQuery += " AND rr.SHIP_VIA IS NULL ";
                                                    sqlselect += " AND RH.SHIP_VIA IS NULL ";
                                                }

                                            }
                                        }
                                        if (ssc == "BTP")
                                        {

                                            if (!string.IsNullOrEmpty(accountID))
                                            {
                                                sqlSelectQuery += " AND rr.ACCOUNT_ID  = '" + accountID + "' ";
                                                sqlselect += " AND RH.ACCOUNT_ID = '" + accountID + "' ";
                                            }
                                            else
                                            {
                                                sqlSelectQuery += " AND rr.ACCOUNT_ID IS NULL ";
                                                sqlselect += " AND RH.ACCOUNT_ID IS NULL ";
                                            }
                                        }
                                        sqlSelectQuery += " order by GEOGRAPHY_ID,CHANNEL_ID,PRODUCTFGA,SSC,nvl(to_date(effective_start_date,'mm/dd/yyyy'),to_date('1/1/1900','mm/dd/yyyy'))";
                                        OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(sqlSelectQuery.ToString(), conn);
                                        oracleDataAdapter.Fill(dtResult, "ROUTING_HEADER_VW");
                                        // Microsoft.Security.Application.AntiXss.HtmlEncode(dtResult.ToString());
                                        if (dtResult.Tables[0] != null)
                                        {
                                            foreach (DataRow dr in dtResult.Tables[0].Rows)
                                            {
                                                if (dr["GEOGRAPHY_ID"].ToString() == "ALL_GEO")
                                                {
                                                    dr["GEOGRAPHY_NAME"] = "ALL_GEO";
                                                }
                                                if (dr["CHANNEL_ID"].ToString() == "ALL_CHANNEL")
                                                {
                                                    dr["CHANNEL_NAME"] = "ALL_CHANNEL";
                                                }
                                                if (dr["PRODUCT_ID"].ToString() == "ALL_PRODUCT")
                                                {
                                                    dr["ProductFGA"] = "ALL_PRODUCT";
                                                }
                                            }
                                        }
                                        OracleDataAdapter da = new OracleDataAdapter(sqlselect, conn);
                                        da.Fill(dtResult, "ROUTING_LINES_VW");

                                        return dtResult;
                                    } //LOB Query loop ends here.

                                } // productid != "ALL_PRODUCT" loop ends here.

                                else
                                {
                                    sqlselect1 += " AND PRODUCT_ID = '" + productid + "' ";
                                    sqlselect += " AND RH.PRODUCT_ID = '" + productid + "' ";
                                }

                            } //if not ALL_PRODUCT loop ends here.

                            else if (!string.IsNullOrEmpty(item))
                            {

                                if (item == "ALL")
                                {
                                    sqlselect1 += " AND TYPE='FGA_ITEM'";
                                    sqlselect += " AND RH.TYPE='FGA_ITEM'";
                                }
                                if (itemType == "FGA_ITEM" && item != "ALL")
                                {
                                    sqlselect1 += " AND ITEM = '" + item + "' ";
                                    sqlselect += " AND RH.ITEM = '" + item + "' ";
                                }
                                if (itemType == "FHC_ITEM")
                                {
                                    string strQueryFHC = "    select distinct rr.routing_id, rr.product_id, rr.item, TO_CHAR(rr.effective_start_date, 'MM/DD/YYYY') effective_start_date,TO_CHAR(rr.effective_end_date, 'MM/DD/YYYY') effective_end_date, rr.productfga, rr.channel_id, rr.channel_name, rr.geography_id, rr.geography_name,  "
                                                     + " rr.ssc, rr.ship_via,rr.account_name,rr.account_id, rr.sys_last_modified_by, rr.sys_last_modified_date, rr.channel_level, rr.geography_level,rr.ID from  "
                                                    + " ( SELECT DISTINCT  ROUTING_ID, PRODUCT_ID, ITEM,  DECODE(ITEM,NULL, (SELECT NAME || '(' || product_id || ')'  from MST_PRODUCT_MASTER_VW  "
                                                     + " where product_id = a.PRODUCT_ID),PRODUCT_ID,NULL,ITEM||'('||(SELECT NAME FROM MST_ITEM_VW WHERE ITEM = ITEM_ID) ||')') PRODUCTFGA, "
                                                      + " a.channel_id CHANNEL_ID,  CHANNEL_ID || '(' || ( SELECT distinct NAME FROM FIN_CHANNEL_MASTER_VW WHERE channel_id = a.channel_id ) || ')' CHANNEL_NAME, "
                                                      + " GEOGRAPHY_ID, GEOGRAPHY_ID || '(' || ( SELECT DISTINCT NAME FROM MST_GEOGRAPHY_VW   WHERE GEO_ID = a.GEOGRAPHY_ID and sys_source='LIBERTY' and rownum < 2 ) || ')' GEOGRAPHY_NAME, "
                                                       + "  SSC, SHIP_VIA,ACCOUNT_NAME,ACCOUNT_ID,   sys_last_modified_by, sys_last_modified_date,effective_start_date,effective_end_date,ID,CHANNEL_LEVEL,GEOGRAPHY_LEVEL FROM  (  SELECT ROUTING_ID,   PRODUCT_ID || ITEM ProductFGA, "
                                                        + " decode(PRODUCT_ID, pre_PRODUCT_ID, '1','0') FLAG, PRODUCT_ID, ITEM,  type, CHANNEL_ID, GEOGRAPHY_ID, SSC, SHIP_VIA,ACCOUNT_NAME,ACCOUNT_ID, geography_level, channel_level, sys_last_modified_by, "
                                                        + "    sys_last_modified_date,effective_start_date,effective_end_date,ID from ( "
                                                           + "   select  a.ROUTING_ID, a.PRODUCT_ID || a.ITEM ProductFga, lag(a.PRODUCT_ID, 1,0) over(order by a.PRODUCT_ID) as pre_PRODUCT_ID, a.PRODUCT_ID, "
                                                           + "    a.ITEM, a.type, a.channel_level, a.geography_level, a.channel_id,   a.geography_id, a.ship_via,a.account_name,a.account_id, a.ssc, a.sys_last_modified_by, a.sys_last_modified_date,a.effective_start_date,a.effective_end_date,a.ID from routing_header_vw a  "
                                                            + "    where a.item = '" + item + "' and  type='FHC_ITEM' or a.item in ( select fga_id from dell_cm_fp_fhc_fga_map_vw where fhc_id = '" + item + "')  "
                                                            + "  )  ) A )  rr ";


                                    if (channelType == "CHANNEL_TREE" && channelType != "ALL" && channelType1 != "SEGMENT")
                                    {


                                        strQueryFHC += " ,("
                                                             + "  select decode(channel_group_id, channel_id, null, channel_group_id) channel_group_id, channel_id,"
                                                             + "   decode(channel_group_type, type, null, channel_group_type) channel_group_type , type"
                                                             + "            from FIN_channel_group_detail_vw"
                                                             + "      connect by NOCYCLE prior channel_id = channel_group_id"
                                                             + "      start with channel_group_id = '" + channelid + "' and channel_group_type = '" + channelType1 + "'"
                                                             + "    ) d";
                                    }

                                    if (!string.IsNullOrEmpty(geoType) && geoType != "ALL_GEO" && geoType != "COUNTRY")
                                    {

                                        strQueryFHC += " , (  select  substr(geo_group_id_type,0,instr(geo_group_id_type,'_')-1) geo_group_id, "
                                            + " substr(geo_group_id_type,instr(geo_group_id_type,'_')+1) geo_group_type, "
                                            + " substr(geo_id_type,0,instr(geo_id_type,'_')-1) geo_id, "
                                            + " substr(geo_id_type,instr(geo_id_type,'_')+1) geo_type,  "
                                            + " geo_group_id_type,geo_id_type from "
                                            + "  ( select decode(geo_group_id||'_'||geo_group_type, geo_id||'_'||geo_type, null, geo_group_id||'_'||geo_group_type) geo_group_id_type, "
                                            + "   geo_id||'_'||geo_type geo_id_type "
                                            + "  from mst_geo_group_detail_Vw where sys_source='LIBERTY'  ) "
                                            + "    connect by NOCYCLE prior geo_id_type    = geo_group_id_type "
                                            + "   start with geo_group_id_type = '" + geographyid + "_" + geoType + "' )  "
                                            + "  e ";
                                    }
                                    strQueryFHC += "  where rr.ROUTING_ID = rr.ROUTING_ID";
                                    if (channelType == "CHANNEL_TREE" && channelType != "ALL" && channelType1 != "SEGMENT")
                                    {
                                        strQueryFHC += " and rr.channel_id = d.channel_group_id and rr.channel_level = d.channel_group_type";
                                    }
                                    if (!string.IsNullOrEmpty(geoType) && geoType != "ALL_GEO" && geoType != "COUNTRY")
                                    {
                                        strQueryFHC += " and rr.geography_id = e.geo_group_id";  //Todo:and rr.geography_level = e.geo_group_type ";
                                    }

                                    if (!string.IsNullOrEmpty(geographyid) && geographyid != "ALL" && geoType == "COUNTRY")
                                    {

                                        sqlselect1 += " And rr.GEOGRAPHY_ID = '" + geographyid + "' and rr.GEOGRAPHY_LEVEL='" + geoType + "'";
                                        sqlselect += " And  RH.GEOGRAPHY_ID = '" + geographyid + "' and RH.GEOGRAPHY_LEVEL='" + geoType + "'";
                                    }
                                    if (channelid != "ALL")
                                    {
                                        //strQueryFHC += " AND rr.CHANNEL_ID  = '" + channelid + "' ";
                                        sqlselect += " And RH.CHANNEL_ID = '" + channelid + "'  and RH.product_id ='" + productid + "' ";
                                    }
                                    if (geographyid == "ALL" && geoType == "COUNTRY")
                                    {
                                        sqlselect1 += " And rr.GEOGRAPHY_LEVEL='" + geoType + "'";
                                        sqlselect += " And RH.GEOGRAPHY_LEVEL='" + geoType + "'";
                                    }
                                    if (channelid == "ALL" && channelType == "SEGMENT")
                                    {
                                        sqlselect1 += " And rr.CHANNEL_LEVEL='" + channelType + "'";
                                        sqlselect += " And RH.CHANNEL_LEVEL='" + channelType + "'";
                                    }

                                    if (ssc != "ALL")
                                    {
                                        strQueryFHC += " AND rr.SSC = '" + ssc + "' ";
                                        sqlselect += " AND RH.SSC = '" + ssc + "'";
                                    }
                                    sqlselect += " AND RH.ITEM = '" + item + "' ";
                                    if (useShipVia)
                                    {
                                        if (ssc == "BTP")
                                        {
                                            if (!string.IsNullOrEmpty(shipVia))
                                            {
                                                strQueryFHC += " AND rr.SHIP_VIA  = '" + shipVia + "' ";
                                                sqlselect += " AND RH.SHIP_VIA = '" + shipVia + "' ";
                                            }
                                            else
                                            {
                                                strQueryFHC += " AND rr.SHIP_VIA IS NULL ";
                                                sqlselect += " AND RH.SHIP_VIA IS NULL ";
                                            }
                                        }
                                    }
                                    if (ssc == "BTP")
                                    {

                                        if (!string.IsNullOrEmpty(accountID))
                                        {
                                            strQueryFHC += " AND rr.ACCOUNT_ID  = '" + accountID + "' ";
                                            sqlselect += " AND RH.ACCOUNT_ID = '" + accountID + "' ";
                                        }
                                        else
                                        {
                                            strQueryFHC += " AND rr.ACCOUNT_ID IS NULL ";
                                            sqlselect += " AND RH.ACCOUNT_ID IS NULL ";
                                        }
                                    }

                                    //Order updated by Kanishka for multi site changes.
                                    strQueryFHC += " order by GEOGRAPHY_ID,CHANNEL_ID,PRODUCTFGA,SSC,nvl(to_date(effective_start_date,'mm/dd/yyyy'),to_date('1/1/1900','mm/dd/yyyy')) ";
                                    OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(strQueryFHC, conn);
                                    oracleDataAdapter.Fill(dtResult, "ROUTING_HEADER_VW");
                                    //Microsoft.Security.Application.AntiXss.HtmlEncode(dtResult.ToString());
                                    if (dtResult.Tables[0] != null)
                                    {
                                        foreach (DataRow dr in dtResult.Tables[0].Rows)
                                        {
                                            if (dr["GEOGRAPHY_ID"].ToString() == "ALL_GEO")
                                            {
                                                dr["GEOGRAPHY_NAME"] = "ALL_GEO";
                                            }
                                            if (dr["CHANNEL_ID"].ToString() == "ALL_CHANNEL")
                                            {
                                                dr["CHANNEL_NAME"] = "ALL_CHANNEL";
                                            }
                                            if (dr["PRODUCT_ID"].ToString() == "ALL_PRODUCT")
                                            {
                                                dr["ProductFGA"] = "ALL_PRODUCT";
                                            }
                                        }
                                    }
                                    OracleDataAdapter da = new OracleDataAdapter(sqlselect, conn);
                                    da.Fill(dtResult, "ROUTING_LINES_VW");
                                    //  Microsoft.Security.Application.AntiXss.HtmlEncode(dtResult.ToString());
                                    return dtResult;
                                } // Item type == FHC Query ends here.

                            } // Item != NULL loop ends here.

                        } //Product != ALL ends here.

                        // Look up Product Queries Ends here.

                        if (!string.IsNullOrEmpty(ssc) && ssc != "ALL")
                        {
                            sqlselect1 += " AND SSC = '" + ssc + "'";
                            sqlselect += " AND RH.SSC = '" + ssc + "'";
                        }
                        if (useShipVia)
                        {
                            if (ssc == "BTP")
                            {
                                if (!string.IsNullOrEmpty(shipVia))
                                {

                                    sqlselect1 += " AND SHIP_VIA = '" + shipVia + "'";
                                    sqlselect += " AND RH.SHIP_VIA = '" + shipVia + "'";
                                }
                                else
                                {
                                    sqlselect1 += " AND SHIP_VIA IS NULL ";
                                    sqlselect += " AND RH.SHIP_VIA IS NULL ";
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(accountID))
                        {
                            sqlselect1 += " AND ACCOUNT_ID  = '" + accountID + "' ";
                            sqlselect += " AND RH.ACCOUNT_ID = '" + accountID + "' ";
                        }

                        //Order changed by Kanishka.
                        sqlselect1 += " ) ) A order by GEOGRAPHY_ID,CHANNEL_ID,PRODUCTFGA,SSC,nvl(to_date(effective_start_date,'mm/dd/yyyy'),to_date('1/1/1900','mm/dd/yyyy'))  ";

                        //updating the transportation time
                        using (OracleCommand cmdselect = new OracleCommand(sqlselect1, conn))
                        {
                            OracleDataAdapter da1 = new OracleDataAdapter(cmdselect);
                            da1.Fill(ds, "ROUTING_HEADER_VW");

                            if (ds.Tables[0] != null)
                            {
                                foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    if (dr["GEOGRAPHY_ID"].ToString() == "ALL_GEO")
                                    {
                                        dr["GEOGRAPHY_NAME"] = "ALL_GEO";
                                    }
                                    if (dr["CHANNEL_ID"].ToString() == "ALL_CHANNEL")
                                    {
                                        dr["CHANNEL_NAME"] = "ALL_CHANNEL";
                                    }
                                    if (dr["PRODUCT_ID"].ToString() == "ALL_PRODUCT")
                                    {
                                        dr["ProductFGA"] = "ALL_PRODUCT";
                                    }
                                }
                            }
                            using (OracleCommand cmdselect1 = new OracleCommand(sqlselect, conn))
                            {
                                OracleDataAdapter da = new OracleDataAdapter(cmdselect1);
                                da.Fill(ds, "ROUTING_LINES_VW");
                            }
                            conn.Close();
                            return ds;
                        }

                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                // return lstFulfillmentDetails;
            });

        }



        public async Task<int> DeleteRoutingDetails(string singleDeleteFlag, string ID)
        {
            int count = 0;
            DataTable dt = new DataTable();
            string sqlselect = null;
            string sqlDelete = null;
            //OracleConnection conn = null;


            return await Task.Run(() =>
            {
                try
                {
                    using (var conn = new OracleConnection(connenctionString))
                    {
                        conn.Open();
                        if (singleDeleteFlag == "NO")
                        {
                            sqlselect = "Select * from ROUTING_LINES_VW where ROUTING_ID in (select ROUTING_ID from Routing_Header_vw where ID = '" + ID + "')";
                        }
                        if (singleDeleteFlag == "YES")
                        {
                            sqlselect = "Select * from ROUTING_LINES_VW where ROUTING_ID = '" + ID + "'";
                        }
                        OracleDataAdapter da = new OracleDataAdapter(sqlselect, conn);
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        //Microsoft.Security.Application.AntiXss.HtmlEncode(ds.ToString());
                        DataTable dt1 = ds.Tables[0];
                        foreach (DataRow row in dt1.Rows)
                        {
                            string var1 = row["ROUTING_ID"].ToString();
                            string var2 = row["FULFILLMENT_SITE_ID"].ToString();
                            string var3 = row["SPLIT"].ToString();
                            string syssource = row["SYS_SOURCE"].ToString();
                            string createdby = row["SYS_CREATED_BY"].ToString();
                            string createddate = row["SYS_CREATION_DATE"].ToString();
                            string lastmodi = row["SYS_LAST_MODIFIED_BY"].ToString();
                            string lastdate = row["SYS_LAST_MODIFIED_DATE"].ToString();
                            string entstate = row["SYS_ENT_STATE"].ToString();
                            string format = "mm/dd/yyyy hh:mi:ssam";

                            string sqlInsertAudit = " INSERT INTO AUD_ROUTING_LINES(TRANSACTION_ID, TRANSACTION_ACTION, ROUTING_ID_OV, ROUTING_ID_NV, FULFILLMENT_SITE_ID_OV, FULFILLMENT_SITE_ID_NV, SPLIT_OV, SPLIT_NV,"
                                               + " SYS_SOURCE, SYS_CREATED_BY, SYS_CREATION_DATE, SYS_LAST_MODIFIED_BY_OV, SYS_LAST_MODIFIED_BY_NV, SYS_LAST_MODIFIED_DATE_OV, SYS_LAST_MODIFIED_DATE_NV, SYS_ENT_STATE )"
                                               + " VALUES ( seq_transaction_id.nextval, :TRANSACTION_ACTION , :ROUTING_ID_OV, :ROUTING_ID_NV, :FULFILLMENT_SITE_ID_OV, :FULFILLMENT_SITE_ID_NV, "
                                               + ":SPLIT_OV, :SPLIT_NV, :SYS_SOURCE, :SYS_CREATED_BY, To_date('" + createddate + "','" + format + "' ), :SYS_LAST_MODIFIED_BY_OV, :SYS_LAST_MODIFIED_BY_NV, To_date('" + lastdate + "' , '" + format + "'), sysdate, :SYS_ENT_STATE )";
                            using (OracleCommand cmdInsertAudit = new OracleCommand(sqlInsertAudit, conn))
                            {

                                OracleParameter param1 = new OracleParameter("TRANSACTION_ACTION", OracleDbType.Varchar2, ParameterDirection.Input);
                                param1.Value = "DELETE";
                                cmdInsertAudit.Parameters.Add(param1);

                                OracleParameter param2 = new OracleParameter("ROUTING_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param2.Value = var1;
                                cmdInsertAudit.Parameters.Add(param2);

                                OracleParameter param3 = new OracleParameter("ROUTING_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param3.Value = DBNull.Value;
                                cmdInsertAudit.Parameters.Add(param3);

                                OracleParameter param4 = new OracleParameter("FULFILLMENT_SITE_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param4.Value = var2;
                                cmdInsertAudit.Parameters.Add(param4);

                                OracleParameter param5 = new OracleParameter("FULFILLMENT_SITE_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param5.Value = DBNull.Value;
                                cmdInsertAudit.Parameters.Add(param5);

                                OracleParameter param6 = new OracleParameter("SPLIT_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param6.Value = var3;
                                cmdInsertAudit.Parameters.Add(param6);

                                OracleParameter param7 = new OracleParameter("SPLIT_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param7.Value = DBNull.Value;
                                cmdInsertAudit.Parameters.Add(param7);

                                OracleParameter param8 = new OracleParameter("SYS_SOURCE", OracleDbType.Varchar2, ParameterDirection.Input);
                                param8.Value = syssource;
                                cmdInsertAudit.Parameters.Add(param8);

                                OracleParameter param9 = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                                param9.Value = createdby;
                                cmdInsertAudit.Parameters.Add(param9);

                                OracleParameter param10 = new OracleParameter("SYS_LAST_MODIFIED_BY_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param10.Value = lastmodi;
                                cmdInsertAudit.Parameters.Add(param10);

                                OracleParameter param11 = new OracleParameter("SYS_LAST_MODIFIED_BY_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param11.Value = lastmodi;
                                cmdInsertAudit.Parameters.Add(param11);

                                OracleParameter param13 = new OracleParameter("SYS_ENT_STATE", OracleDbType.Varchar2, ParameterDirection.Input);
                                param13.Value = "ACTIVE";
                                cmdInsertAudit.Parameters.Add(param13);

                                cmdInsertAudit.ExecuteNonQuery();
                            }

                        }

                        if (singleDeleteFlag == "NO")
                        {
                            sqlDelete = "DELETE SERVICESNOP.ROUTING_LINES"
                                               + " WHERE ROUTING_ID IN (SELECT ROUTING_ID FROM ROUTING_HEADER_VW WHERE ID = '" + ID + "')";
                        }
                        if (singleDeleteFlag == "YES")
                        {
                            sqlDelete = "DELETE SERVICESNOP.ROUTING_LINES"
                                               + " WHERE ROUTING_ID = '" + ID + "'";
                        }
                        using (OracleCommand cmdDelete = new OracleCommand(sqlDelete, conn))
                        {
                            count = (int)cmdDelete.ExecuteNonQuery();
                        }

                        conn.Clone();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return count;
            });
        }

        public async Task<int> DeleteRoutingDetailsHeader(string singleDeleteFlag, string ID)
        {
            int count = 0;
            DataTable dt = new DataTable();
            string sqlselect = null;
            string sqlDeleteHeader = null;
            //OracleConnection conn = null;


            return await Task.Run(() =>
            {
                try
                {
                    using (var conn = new OracleConnection(connenctionString))
                    {

                        conn.Open();
                        if (singleDeleteFlag == "NO")
                        {
                            sqlselect = "Select * from ROUTING_HEADER_VW where ID = '" + ID + "'";
                        }
                        if (singleDeleteFlag == "YES")
                        {
                            sqlselect = "Select * from ROUTING_HEADER_VW where ROUTING_ID = '" + ID + "'";
                        }
                        OracleDataAdapter da = new OracleDataAdapter(sqlselect, conn);
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        DataTable dt1 = ds.Tables[0];
                        foreach (DataRow row in dt1.Rows)
                        {
                            string routing = row["ROUTING_ID"].ToString();
                            string channel = row["CHANNEL_ID"].ToString();
                            string geo = row["GEOGRAPHY_ID"].ToString();
                            string product = row["PRODUCT_ID"].ToString();
                            string item = row["ITEM"].ToString();
                            string type = row["TYPE"].ToString();
                            string ssc = row["SSC"].ToString();
                            string shipvia = row["SHIP_VIA"].ToString();
                            string syssource = row["SYS_SOURCE"].ToString();
                            string createdby = row["SYS_CREATED_BY"].ToString();
                            string createddate = row["SYS_CREATION_DATE"].ToString();
                            string lastmodi = row["SYS_LAST_MODIFIED_BY"].ToString();
                            string lastdate = row["SYS_LAST_MODIFIED_DATE"].ToString();
                            string entstate = row["SYS_ENT_STATE"].ToString();
                            string geoLevel = row["GEOGRAPHY_LEVEL"].ToString();
                            string channelLevel = row["CHANNEL_LEVEL"].ToString();
                            string format = "mm/dd/yyyy hh:mi:ssam";
                            string accountId = row["ACCOUNT_ID"].ToString();
                            string accountName = row["ACCOUNT_NAME"].ToString();


                            string sqlinsertaudit = " INSERT INTO AUD_ROUTING_HEADER (TRANSACTION_ID, TRANSACTION_ACTION, ROUTING_ID_OV, ROUTING_ID_NV, CHANNEL_ID_OV, CHANNEL_ID_NV, GEOGRAPHY_ID_OV, GEOGRAPHY_ID_NV, " +
                                                    " PRODUCT_ID_OV, PRODUCT_ID_NV, ITEM_OV, ITEM_NV, TYPE_OV, TYPE_NV, SSC_OV, SSC_NV, SHIP_VIA_OV, SHIP_VIA_NV, SYS_SOURCE, SYS_CREATED_BY, SYS_CREATION_DATE, SYS_LAST_MODIFIED_BY_OV, " +
                                                     " SYS_LAST_MODIFIED_BY_NV,SYS_LAST_MODIFIED_DATE_OV ,SYS_LAST_MODIFIED_DATE_NV, SYS_ENT_STATE, CHANNEL_LEVEL_OV, CHANNEL_LEVEL_NV, GEOGRAPHY_LEVEL_OV, GEOGRAPHY_LEVEL_NV, ACCOUNT_ID_OV, ACCOUNT_ID_NV, ACCOUNT_NAME_OV, ACCOUNT_NAME_NV) VALUES (seq_transaction_id.nextval, :TRANSACTION_ACTION," +
                                                     " :ROUTING_ID_OV, :ROUTING_ID_NV, :CHANNEL_ID_OV, :CHANNEL_ID_NV, :GEOGRAPHY_ID_OV,  :GEOGRAPHY_ID_NV, :PRODUCT_ID_OV, :PRODUCT_ID_NV, :ITEM_OV, :ITEM_NV, :TYPE_OV, :TYPE_NV, " +
                                                     " :SSC_OV, :SSC_NV, :SHIP_VIA_OV, :SHIP_VIA_NV, :SYS_SOURCE, :SYS_CREATED_BY, To_date('" + createddate + "','" + format + "' ), :SYS_LAST_MODIFIED_BY_OV, :SYS_LAST_MODIFIED_BY_NV, To_date('" + lastdate + "' , '" + format + "'), sysdate,  :SYS_ENT_STATE, :CHANNEL_LEVEL_OV, :CHANNEL_LEVEL_NV, :GEOGRAPHY_LEVEL_OV, :GEOGRAPHY_LEVEL_NV, :ACCOUNT_ID_OV, :ACCOUNT_ID_NV, :ACCOUNT_NAME_OV, :ACCOUNT_NAME_NV ) ";

                            using (OracleCommand cmdInsertaudit = new OracleCommand(sqlinsertaudit, conn))
                            {
                                OracleParameter param1 = new OracleParameter("TRANSACTION_ACTION", OracleDbType.Varchar2, ParameterDirection.Input);
                                param1.Value = "DELETE";
                                cmdInsertaudit.Parameters.Add(param1);

                                OracleParameter param2 = new OracleParameter("ROUTING_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param2.Value = routing;
                                cmdInsertaudit.Parameters.Add(param2);

                                OracleParameter param3 = new OracleParameter("ROUTING_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param3.Value = DBNull.Value;
                                cmdInsertaudit.Parameters.Add(param3);

                                OracleParameter param4 = new OracleParameter("CHANNEL_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param4.Value = channel;
                                cmdInsertaudit.Parameters.Add(param4);

                                OracleParameter param5 = new OracleParameter("CHANNEL_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param5.Value = DBNull.Value;
                                cmdInsertaudit.Parameters.Add(param5);

                                OracleParameter param6 = new OracleParameter("GEOGRAPHY_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param6.Value = geo;
                                cmdInsertaudit.Parameters.Add(param6);

                                OracleParameter param7 = new OracleParameter("GEOGRAPHY_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param7.Value = DBNull.Value;
                                cmdInsertaudit.Parameters.Add(param7);

                                OracleParameter param8 = new OracleParameter("PRODUCT_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param8.Value = product;
                                cmdInsertaudit.Parameters.Add(param8);

                                OracleParameter param9 = new OracleParameter("PRODUCT_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param9.Value = DBNull.Value;
                                cmdInsertaudit.Parameters.Add(param9);

                                OracleParameter param10 = new OracleParameter("ITEM_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param10.Value = item;
                                cmdInsertaudit.Parameters.Add(param10);

                                OracleParameter param11 = new OracleParameter("ITEM_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param11.Value = DBNull.Value;
                                cmdInsertaudit.Parameters.Add(param11);

                                OracleParameter param12 = new OracleParameter("TYPE_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param12.Value = type;
                                cmdInsertaudit.Parameters.Add(param12);

                                OracleParameter param13 = new OracleParameter("TYPE_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param13.Value = DBNull.Value;
                                cmdInsertaudit.Parameters.Add(param13);

                                OracleParameter param14 = new OracleParameter("SSC_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param14.Value = ssc;
                                cmdInsertaudit.Parameters.Add(param14);

                                OracleParameter param15 = new OracleParameter("SSC_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param15.Value = DBNull.Value;
                                cmdInsertaudit.Parameters.Add(param15);

                                OracleParameter param16 = new OracleParameter("SHIP_VIA_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param16.Value = shipvia;
                                cmdInsertaudit.Parameters.Add(param16);

                                OracleParameter param17 = new OracleParameter("SHIP_VIA_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param17.Value = DBNull.Value;
                                cmdInsertaudit.Parameters.Add(param17);


                                OracleParameter param18 = new OracleParameter("SYS_SOURCE", OracleDbType.Varchar2, ParameterDirection.Input);
                                param18.Value = syssource;
                                cmdInsertaudit.Parameters.Add(param18);

                                OracleParameter param19 = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                                param19.Value = createdby;
                                cmdInsertaudit.Parameters.Add(param19);

                                OracleParameter param20 = new OracleParameter("SYS_LAST_MODIFIED_BY_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param20.Value = lastmodi;
                                cmdInsertaudit.Parameters.Add(param20);

                                OracleParameter param21 = new OracleParameter("SYS_LAST_MODIFIED_BY_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param21.Value = lastmodi;
                                cmdInsertaudit.Parameters.Add(param21);



                                OracleParameter param23 = new OracleParameter("SYS_ENT_STATE", OracleDbType.Varchar2, ParameterDirection.Input);
                                param23.Value = entstate;
                                cmdInsertaudit.Parameters.Add(param23);

                                OracleParameter param24 = new OracleParameter("CHANNEL_LEVEL_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param24.Value = channelLevel;
                                cmdInsertaudit.Parameters.Add(param24);

                                OracleParameter param25 = new OracleParameter("CHANNEL_LEVEL_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param25.Value = DBNull.Value;
                                cmdInsertaudit.Parameters.Add(param25);

                                OracleParameter param26 = new OracleParameter("GEOGRAPHY_LEVEL_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param26.Value = geoLevel;
                                cmdInsertaudit.Parameters.Add(param26);

                                OracleParameter param27 = new OracleParameter("GEOGRAPHY_LEVEL_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param27.Value = DBNull.Value;
                                cmdInsertaudit.Parameters.Add(param27);

                                OracleParameter param28 = new OracleParameter("ACCOUNT_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param28.Value = accountId;
                                cmdInsertaudit.Parameters.Add(param28);

                                OracleParameter param29 = new OracleParameter("ACCOUNT_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param29.Value = DBNull.Value; ;
                                cmdInsertaudit.Parameters.Add(param29);


                                OracleParameter param30 = new OracleParameter("ACCOUNT_NAME_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param30.Value = accountName;
                                cmdInsertaudit.Parameters.Add(param30);

                                OracleParameter param31 = new OracleParameter("ACCOUNT_NAME_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                                param31.Value = DBNull.Value; ;
                                cmdInsertaudit.Parameters.Add(param31);

                                cmdInsertaudit.ExecuteNonQuery();

                            }
                        }

                        if (singleDeleteFlag == "YES")
                        {
                            sqlDeleteHeader = "Delete SERVICESNOP.ROUTING_HEADER"
                                          + " WHERE ROUTING_ID = '" + ID + "'";
                        }
                        if (singleDeleteFlag == "NO")
                        {
                            sqlDeleteHeader = "Delete SERVICESNOP.ROUTING_HEADER"
                                        + " WHERE ID = '" + ID + "'";
                        }

                        //updating the transportation time
                        using (OracleCommand cmdDelete = new OracleCommand(sqlDeleteHeader, conn))
                        {
                            count = (int)cmdDelete.ExecuteNonQuery();
                        }

                        conn.Clone();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return count;
            });
        }

        public async Task<DataSet> GetFulfillmentRetrive(FulFillmentSearchInput retrieveInput)
        {
            DataSet ds = new DataSet();
            DataTable dtRetrieve = new DataTable();
            //string channelid = retrieveInput.Channelid;
            string channelid = retrieveInput.Channelid;
            string channelLevel = retrieveInput.ChannelType;
            string geographyid = retrieveInput.Geoid;
            string geoLevel = retrieveInput.GeoType;
            string lookupproduct = retrieveInput.Lookupproduct;
            string productid = retrieveInput.StrProductId;
            string item = retrieveInput.Item;
            string ssc = retrieveInput.Ssc;
            string shipVia = retrieveInput.ShipVia;
            bool useShipVia = retrieveInput.UseShipVia;
            string accountID = retrieveInput.StrsearcAccountID;

            return await Task.Run(() =>
            {
                try
                {
                    using (var conn = new OracleConnection(connenctionString))
                    {
                        conn.Open();

                        string sqlselect = "select DISTINCT RH.ROUTING_ID, RL.FULFILLMENT_SITE_ID,RL.SPLIT,(SELECT BEGIN_FISCAL_WEEK FROM SNOP_RP_FISCAL_WK_LIST_VW WHERE BEGIN_CAL_DATE = TO_CHAR(RH.EFFECTIVE_START_DATE, 'MM/DD/YYYY')) as EFFECTIVE_START_FISCAL_WEEK,TO_CHAR(RH.EFFECTIVE_START_DATE, 'MM/DD/YYYY') EFFECTIVE_START_DATE,(SELECT END_FISCAL_WEEK FROM SNOP_RP_FISCAL_WK_LIST_VW WHERE END_CAL_DATE = TO_CHAR(RH.EFFECTIVE_END_DATE, 'MM/DD/YYYY')) as EFFECTIVE_END_FISCAL_WEEK,TO_CHAR(RH.EFFECTIVE_END_DATE, 'MM/DD/YYYY') EFFECTIVE_END_DATE,RH.ID from  ROUTING_HEADER_VW RH,ROUTING_LINES_VW RL WHERE RH.ROUTING_ID=RL.ROUTING_ID";

                        string sqlselect1 = " SELECT D.ROUTING_ID, D.GEOGRAPHY_ID, D.PRODUCT_ID, D.ITEM,  D.CHANNEL_ID,ITEM||'('||(SELECT NAME FROM MST_ITEM_VW WHERE ITEM = ITEM_ID) ||')' PRODUCTFGA,  "
                             + "  CHANNEL_ID || '(' || ( SELECT distinct NAME FROM FIN_CHANNEL_MASTER_VW WHERE channel_id = D.channel_id ) || ')' CHANNEL_NAME, GEOGRAPHY_ID, "
                                          + "  GEOGRAPHY_ID || '(' || ( SELECT DISTINCT NAME FROM MST_GEOGRAPHY_VW  WHERE GEO_ID = D.GEOGRAPHY_ID and sys_source='LIBERTY' and rownum < 2 ) || ')' GEOGRAPHY_NAME, "
                             + " D.SSC, D.SHIP_VIA,D.ACCOUNT_NAME,D.ACCOUNT_ID, d.sys_last_modified_by, d.sys_last_modified_date "
                             + " FROM ROUTING_HEADER_VW D  WHERE D.ROUTING_ID = D.ROUTING_ID ";


                        if (!string.IsNullOrEmpty(channelid))
                        {
                            sqlselect1 += " And d.CHANNEL_ID = '" + channelid + "' AND d.CHANNEL_LEVEL='" + channelLevel + "' ";
                            sqlselect += " And RH.CHANNEL_ID = '" + channelid + "' AND RH.CHANNEL_LEVEL='" + channelLevel + "' ";
                        }
                        if (!string.IsNullOrEmpty(geographyid))
                        {
                            sqlselect1 += " And  d.GEOGRAPHY_ID = '" + geographyid + "' AND d.GEOGRAPHY_LEVEL='" + geoLevel + "' ";
                            sqlselect += " And  RH.GEOGRAPHY_ID = '" + geographyid + "' AND RH.GEOGRAPHY_LEVEL='" + geoLevel + "' ";
                        }
                        if (!string.IsNullOrEmpty(lookupproduct))
                        {
                            if (!string.IsNullOrEmpty(productid))
                            {

                                sqlselect1 += " AND D.PRODUCT_ID = '" + productid + "' ";
                                sqlselect += " AND RH.PRODUCT_ID = '" + productid + "' ";

                            }
                            else if (!string.IsNullOrEmpty(item))
                            {
                                sqlselect1 += " AND D.ITEM = '" + item + "' ";
                                sqlselect += " AND RH.ITEM = '" + item + "' ";
                            }
                        }
                        if (!string.IsNullOrEmpty(ssc) && ssc != "ALL")
                        {
                            sqlselect1 += " AND D.SSC = '" + ssc + "'";
                            sqlselect += " AND RH.SSC = '" + ssc + "'";
                        }
                        if (useShipVia)
                        {
                            if (ssc == "BTP")
                            {
                                if (!string.IsNullOrEmpty(shipVia))
                                {

                                    sqlselect1 += " AND D.SHIP_VIA = '" + shipVia + "'";
                                    sqlselect += " AND RH.SHIP_VIA = '" + shipVia + "'";
                                }
                                else
                                {
                                    sqlselect1 += " AND D.SHIP_VIA IS NULL ";
                                    sqlselect += " AND RH.SHIP_VIA IS NULL ";
                                }
                            }
                        }
                        if (ssc == "BTP")
                        {

                            if (!string.IsNullOrEmpty(accountID))
                            {
                                sqlselect1 += " AND D.ACCOUNT_ID  = '" + accountID + "' ";
                                sqlselect += " AND RH.ACCOUNT_ID = '" + accountID + "' ";
                            }
                            else
                            {
                                sqlselect1 += " AND D.ACCOUNT_ID IS NULL ";
                                sqlselect += " AND RH.ACCOUNT_ID IS NULL ";
                            }
                        }
                        sqlselect1 += " order by routing_id ";
                        sqlselect += " order by RH.routing_id ";

                        //updating the transportation time
                        using (OracleCommand cmdselect = new OracleCommand(sqlselect1, conn))
                        {
                            OracleDataAdapter da1 = new OracleDataAdapter(cmdselect);
                            da1.Fill(ds, "ROUTING_HEADER_VW");
                            //Microsoft.Security.Application.AntiXss.HtmlEncode(da1.ToString());
                            if (ds.Tables[0] != null)
                            {
                                foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    if (dr["GEOGRAPHY_ID"].ToString() == "ALL_GEO")
                                    {
                                        dr["GEOGRAPHY_NAME"] = "ALL_GEO";
                                    }
                                    if (dr["CHANNEL_ID"].ToString() == "ALL_CHANNEL")
                                    {
                                        dr["CHANNEL_NAME"] = "ALL_CHANNEL";
                                    }
                                    if (dr["PRODUCT_ID"].ToString() == "ALL_PRODUCT")
                                    {
                                        dr["ProductFGA"] = "ALL_PRODUCT";
                                    }
                                }
                            }
                            using (OracleCommand cmdselect1 = new OracleCommand(sqlselect, conn))
                            {
                                OracleDataAdapter da = new OracleDataAdapter(cmdselect1);
                                da.Fill(ds, "ROUTING_LINES_VW");
                                //Microsoft.Security.Application.AntiXss.HtmlEncode(ds.ToString());
                            }
                            conn.Close();
                        }

                    }

                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return ds;
            });


        }


        public string getRegionName(string country)
        {
            string countryname = country;
            string strRegion = string.Empty;
            DataSet ds = new DataSet();
            OracleConnection conn = null;

            try
            {
                using (conn = new OracleConnection(connenctionString))
                {
                    string sql = "SELECT substr(geogroupid,0,instr(geogroupid,'-')-1) REGION  FROM "
                                 + " ( select geo_id||'-'||geo_type groupId, geo_group_id||'-'||geo_group_type geoGroupId from MST_GEO_GROUP_DETAIL_VW ) "
                                  + "  where substr(geogroupid,instr(geogroupid,'-')+1) = 'REGION'"
                                  + "  connect by NOCYCLE prior geoGroupId = groupId "
                                  + " start with groupId = '" + countryname + "' ";
                    using (OracleCommand cmd = new OracleCommand(sql, conn))
                    {
                        conn.Open();
                        OracleDataAdapter da = new OracleDataAdapter(cmd);
                        da.Fill(ds);
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            strRegion = ds.Tables[0].Rows[0][0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return strRegion;
        }

        public List<string> GetNonVirtualSites(string regionCode)
        {
            List<string> lstsite = new List<string>();
            // string strREGION_CODE = ConfigurationManager.AppSettings["REGION_CODE"].ToString();
            string strFF_TYPE_NAME = "MERGE"; //ConfigurationManager.AppSettings["FF_TYPE_NAME"].ToString();
            string sys_ent_state = "ACTIVE";//ConfigurationManager.AppSettings["sys_ent_state"].ToString();
            string enableSysEntState = "FALSE";
            OracleConnection conn = null;

            try
            {
                using (conn = new OracleConnection(connenctionString))
                {
                    //string sql = " SELECT DISTINCT C.SITE_NAME FROM  FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B, "
                    //           + " FF_SUB_REGION_SITE_XREF_VW C, SITE_VW D WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE "
                    //           + " AND A.REGION_CODE = B.REGION_CODE  "
                    //           + " and D.virtual_site_flag = 'N'  and C.SITE_NAME = D.SITE_NAME ";
                    string sql = " SELECT DISTINCT C.SITE_NAME FROM  FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B, "
                               + " FF_SUB_REGION_SITE_XREF_VW C, SITE_VW D WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE "
                               + " AND B.FF_TYPE_NAME = C.FF_TYPE_NAME AND A.FF_TYPE_NAME  = 'RP_NODES' AND A.REGION_CODE = B.REGION_CODE  "
                               + " and D.virtual_site_flag = 'N'  and C.SITE_NAME = D.SITE_NAME  AND NVL(A.SYS_ENT_STATE,'ACTIVE') = 'ACTIVE' AND NVL(B.SYS_ENT_STATE,'ACTIVE') = 'ACTIVE' AND NVL(C.SYS_ENT_STATE,'ACTIVE') = 'ACTIVE'";
                    //and  A.FF_TYPE_NAME = '" + strFF_TYPE_NAME + "'
                    if (enableSysEntState != null && enableSysEntState == "TRUE")
                    {
                        sql += " and a.sys_ent_state = '" + sys_ent_state + "' ";
                        sql += " and b.sys_ent_state = '" + sys_ent_state + "' ";
                    }
                    if (regionCode != "ALL_GEO")
                    {
                        sql += " AND A.REGION_CODE='" + regionCode + "'";
                    }
                    else
                    {
                        sql += " AND A.REGION_CODE IN ( select distinct a.geo_group_id from mst_geo_group_detail_Vw a, mst_geography_vw b where a.sys_source='LIBERTY' and b.sys_source='LIBERTY' and geo_group_type = 'REGION' AND NVL(a.SYS_ENT_STATE,'ACTIVE') = 'ACTIVE' AND NVL(b.SYS_ENT_STATE,'ACTIVE') = 'ACTIVE'and a.geo_group_id = b.geo_id )";
                    }
                    sql += " order by C.SITE_NAME  ";
                    using (OracleCommand cmd = new OracleCommand(sql, conn))
                    {
                        conn.Open();
                        OracleDataReader oracleDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                        while (oracleDataReader.Read())
                        {
                            lstsite.Add(oracleDataReader["SITE_NAME"].ToString());
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return lstsite;
        }

        public DataTable GetFSAFiscalCalendarInfo_BeginDate()
        {
            //Method added by Kanishka, for multi-site changes.

            //Datatable that holds the fiscal calendar information.
            DataTable dtFiscalInfo = new DataTable();
            OracleConnection conn = null;
            try
            {
                using (conn = new OracleConnection(connenctionString))
                {

                    string sql = "select BEGIN_FISCAL_WEEK,BEGIN_CAL_DATE,END_FISCAL_WEEK,END_CAL_DATE from SNOP_RP_FISCAL_WK_LIST_VW";
                    using (OracleCommand cmd = new OracleCommand(sql, conn))
                    {
                        conn.Open();
                        OracleDataAdapter da = new OracleDataAdapter(cmd);
                        da.Fill(dtFiscalInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return dtFiscalInfo;

        }

        public DataTable GetFSAFiscalCalendarInfo_EndDate()
        {
            //Method added by Kanishka, for multi-site changes.

            //Datatable that holds the fiscal calendar information.
            DataTable dtFiscalInfo = new DataTable();
            OracleConnection conn = null;
            try
            {
                using (conn = new OracleConnection(connenctionString))
                {

                    string sql = "select BEGIN_FISCAL_WEEK,BEGIN_CAL_DATE,END_FISCAL_WEEK,END_CAL_DATE from SNOP_RP_FISCAL_WK_LIST_VW";
                    using (OracleCommand cmd = new OracleCommand(sql, conn))
                    {
                        conn.Open();
                        OracleDataAdapter da = new OracleDataAdapter(cmd);
                        da.Fill(dtFiscalInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return dtFiscalInfo;

        }

        public bool IsVirtualSite(string siteName)
        {
            bool isVirutal = false;
            OracleConnection conn = null;

            try
            {
                using (conn = new OracleConnection(connenctionString))
                {
                    string sql = "select * from site_vw where virtual_site_flag = 'Y' and site_name = '" + siteName + "' ";
                    using (OracleCommand cmd = new OracleCommand(sql, conn))
                    {
                        conn.Open();
                        OracleDataReader oracleDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                        if (oracleDataReader.Read())
                        {
                            isVirutal = true;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return isVirutal;
        }

        public DataTable retrieveRoutingDetailsByRoutingId(string routingID)
        {
            DataTable dtRoutingLines = new DataTable();
            OracleConnection oracleConnection = null;
            OracleCommand oracleCommand = null;
            OracleDataAdapter oracleDataAdapter = null;
            string sqlRoutingLinesQuery = null;
            try
            {
                using (oracleConnection = new OracleConnection(connenctionString))
                {
                    if (!string.IsNullOrEmpty(routingID))
                    {
                        sqlRoutingLinesQuery = "SELECT ROUTING_ID, FULFILLMENT_SITE_ID FROM ROUTING_LINES WHERE ROUTING_ID='" + routingID + "'";
                        oracleCommand = new OracleCommand(sqlRoutingLinesQuery.ToString(), oracleConnection);
                        oracleDataAdapter = new OracleDataAdapter(oracleCommand);
                        oracleDataAdapter.Fill(dtRoutingLines);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (oracleConnection != null)
                {
                    oracleConnection.Close();
                    oracleConnection.Dispose();
                }
            }
            return dtRoutingLines;
        }

        public DataTable retrieveRoutingDetails(FulFillmentSearchInput retrieveInput)
        {

            string channelid="";
            string geographyid = "";
            string geoLevel = "";
            string channelLevel = "";
            string productid = "";
            string item = "";
            string ssc = "";
            string shipVia = "";
            string strAccountID = "";
            string effectiveStartDate = "";
            string effectiveEndDate = "";


            channelid = retrieveInput.Channelid;
            geographyid = retrieveInput.Geoid;
            geoLevel = retrieveInput.GeoType;
            channelLevel = retrieveInput.ChannelType;
            productid = retrieveInput.StrProductId;
            item = retrieveInput.Item;
            ssc = retrieveInput.Ssc;
            shipVia = retrieveInput.ShipVia;
            strAccountID = retrieveInput.StrsearcAccountID;
            effectiveStartDate = retrieveInput.effectiveStartDate;
            effectiveEndDate = retrieveInput.effectiveEndDate;

            DataTable dtRoutingLines = new DataTable();
            OracleConnection oracleConnection = null;
            OracleCommand oracleCommand = null;
            OracleDataAdapter oracleDataAdapter = null;
            string sqlQuery = null;
            string routingID = null;
            string sqlRoutingLinesQuery = null;
            bool check = false;
            try
            {
                //In the Query, table has been replaced with view by Kanishka, on July 3, 2012
                sqlQuery = "SELECT ROUTING_ID FROM ROUTING_HEADER_VW WHERE CHANNEL_ID='" + channelid + "' AND GEOGRAPHY_ID ='" + geographyid + "' AND SSC='" + ssc + "' AND GEOGRAPHY_LEVEL='" + geoLevel + "' AND CHANNEL_LEVEL='" + channelLevel + "' ";
                if (!string.IsNullOrEmpty(productid))
                {
                    sqlQuery = sqlQuery + " AND PRODUCT_ID ='" + productid + "' ";
                }
                if (!string.IsNullOrEmpty(item))
                {
                    sqlQuery = sqlQuery + " AND ITEM ='" + item + "' ";
                }
                if (ssc.Equals("BTP"))
                {
                    if (!string.IsNullOrEmpty(shipVia))
                    {
                        sqlQuery = sqlQuery + " AND SHIP_VIA ='" + shipVia + "' ";
                    }
                    else
                    {
                        sqlQuery = sqlQuery + " AND SHIP_VIA IS NULL ";
                    }
                    if (!string.IsNullOrEmpty(strAccountID))
                    {
                        sqlQuery = sqlQuery + " AND account_ID ='" + strAccountID + "' ";
                    }
                    else
                    {
                        sqlQuery = sqlQuery + " AND account_ID IS NULL ";
                    }
                }

                if (effectiveStartDate == " " && effectiveEndDate == " ")
                {
                    sqlQuery = sqlQuery + "AND EFFECTIVE_START_DATE = null  AND EFFECTIVE_END_DATE = null";
                }

                //Append Effective dates here.TO_CHAR(RH.EFFECTIVE_START_DATE, 'MM/DD/YYYY')
                if (effectiveStartDate != null && effectiveEndDate != null && effectiveStartDate != " " && effectiveStartDate != string.Empty && effectiveEndDate != " " && effectiveEndDate != string.Empty)
                {
                    sqlQuery = sqlQuery + " AND TO_CHAR(EFFECTIVE_START_DATE, 'MM/DD/YYYY') ='" + effectiveStartDate + "' AND TO_CHAR(EFFECTIVE_END_DATE, 'MM/DD/YYYY') = '" + effectiveEndDate + "' ";
                    check = true;
                }

                if (effectiveStartDate != null && effectiveStartDate != " " && effectiveStartDate != string.Empty && check == false)
                {
                    sqlQuery = sqlQuery + " AND TO_CHAR(EFFECTIVE_START_DATE, 'MM/DD/YYYY') ='" + effectiveStartDate + "'";
                }
                if (effectiveEndDate != null && effectiveEndDate != " " && effectiveEndDate != string.Empty && check == false)
                {
                    sqlQuery = sqlQuery + " AND TO_CHAR(EFFECTIVE_END_DATE, 'MM/DD/YYYY') ='" + effectiveEndDate + "'";
                }



                using (oracleConnection = new OracleConnection(connenctionString))
                {
                    oracleCommand = new OracleCommand(sqlQuery.ToString(), oracleConnection);
                    oracleConnection.Open();
                    routingID = (string)oracleCommand.ExecuteScalar();
                    if (!string.IsNullOrEmpty(routingID))
                    {
                        sqlRoutingLinesQuery = "SELECT ROUTING_ID, FULFILLMENT_SITE_ID FROM ROUTING_LINES WHERE ROUTING_ID='" + routingID + "'";
                        oracleCommand = new OracleCommand(sqlRoutingLinesQuery.ToString(), oracleConnection);
                        oracleDataAdapter = new OracleDataAdapter(oracleCommand);
                        oracleDataAdapter.Fill(dtRoutingLines);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (oracleConnection != null)
                {
                    oracleConnection.Close();
                    oracleConnection.Dispose();
                }
            }
            return dtRoutingLines;
        }


        public bool InsertValidateEffectiveDates(FullFillmentRuleData FSAEntity)
        {
            OracleConnection conn = null;
            string sqlText = string.Empty;
            string retValue = string.Empty;

            if (FSAEntity.effectiveBeginDate == null || FSAEntity.effectiveBeginDate == " " || FSAEntity.effectiveBeginDate == string.Empty)
                FSAEntity.effectiveBeginDate = null;

            if (FSAEntity.effectiveEndDate == null || FSAEntity.effectiveEndDate == " " || FSAEntity.effectiveEndDate == string.Empty)
                FSAEntity.effectiveEndDate = null;

            try
            {
                using (conn = new OracleConnection(connenctionString))
                {
                    sqlText = "select SNOP_RP_UI_UTIL_PKG.FN_FSA_ADD_WEEK_VALIDATION('" + FSAEntity.Geography + "','" + FSAEntity.geoLevel + "','" + FSAEntity.Channel + "','" + FSAEntity.channelLevel + "','" + FSAEntity.item1 + "','" + FSAEntity.product_id + "','" + FSAEntity.SupplyChainType + "','" + FSAEntity.ship_via + "', TO_DATE('" + FSAEntity.effectiveBeginDate + "','mm/dd/yyyy')," + "TO_DATE('" + FSAEntity.effectiveEndDate + "','mm/dd/yyyy')," + "'" + FSAEntity.accountId + "') MESSAGE from dual";

                    using (OracleCommand cmdID = new OracleCommand(sqlText, conn))
                    {
                        conn.Open();
                        OracleDataReader oracleDataReader = cmdID.ExecuteReader(CommandBehavior.CloseConnection);

                        while (oracleDataReader.Read())
                        {
                            retValue = oracleDataReader["MESSAGE"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            if (retValue == "SUCCESS")
            {
                return true;
            }
            else
                return false;

        }

        public int AddRoutingdetails(FullFillmentRuleData Full)
        {

            //set default value for sys_active_State

            Full.sys_Ent_State = "ACTIVE";

            string routingid = string.Empty;
            string uniqueID = string.Empty;
            int count = 0;
            OracleConnection conn = null;
            //return await Task.Run(() =>
            //{
                try
                {

                    StringBuilder sqlInsertQuery = new StringBuilder();

                    using (conn = new OracleConnection(connenctionString))
                    {

                        //Get the ID Value.
                        string sqlID = "SELECT NVL((SELECT ID FROM ROUTING_HEADER WHERE CHANNEL_ID = :CHANNEL_ID AND GEOGRAPHY_ID=:GEOGRAPHY_ID AND NVL(PRODUCT_ID,ITEM)=NVL(:PRODUCT_ID,:ITEM) AND SSC=:SSC AND ROWNUM=1), (SELECT NVL(MAX(ID),0)+1 FROM ROUTING_HEADER)) ID FROM DUAL";

                        using (OracleCommand cmdID = new OracleCommand(sqlID, conn))
                        {
                            conn.Open();

                            OracleParameter param1 = new OracleParameter("CHANNEL_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            param1.Value = Full.Channel;
                            cmdID.Parameters.Add(param1);

                            OracleParameter param2 = new OracleParameter("GEOGRAPHY_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            param2.Value = Full.Geography;
                            cmdID.Parameters.Add(param2);

                            OracleParameter param3 = new OracleParameter("PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            param3.Value = Full.product_id;
                            cmdID.Parameters.Add(param3);

                            OracleParameter param4 = new OracleParameter("ITEM", OracleDbType.Varchar2, ParameterDirection.Input);
                            param4.Value = Full.item1;
                            cmdID.Parameters.Add(param4);

                            OracleParameter param5 = new OracleParameter("SSC", OracleDbType.Varchar2, ParameterDirection.Input);
                            param5.Value = Full.SupplyChainType;
                            cmdID.Parameters.Add(param5);

                            OracleDataReader oracleDataReader = cmdID.ExecuteReader(CommandBehavior.CloseConnection);

                            while (oracleDataReader.Read())
                            {
                                uniqueID = oracleDataReader["ID"].ToString();
                            }
                        }

                        string sqlInsert = "INSERT INTO ROUTING_HEADER(ID,CHANNEL_ID, GEOGRAPHY_ID, PRODUCT_ID, ITEM, TYPE, SSC,SHIP_VIA,EFFECTIVE_START_DATE,EFFECTIVE_END_DATE, ROUTING_ID,GEOGRAPHY_LEVEL, CHANNEL_LEVEL, SYS_SOURCE,SYS_CREATED_BY,SYS_CREATION_DATE,SYS_LAST_MODIFIED_BY,SYS_LAST_MODIFIED_DATE,SYS_ENT_STATE,ACCOUNT_ID,ACCOUNT_NAME) "
                                          + "VALUES (:ID,:CHANNEL_ID, :GEOGRAPHY_ID, :PRODUCT_ID, :ITEM, :TYPE, :SSC, :SHIP_VIA,:EFFECTIVE_START_DATE,:EFFECTIVE_END_DATE,:ROUTING_ID,:GEOGRAPHY_LEVEL,:CHANNEL_LEVEL,:SYS_SOURCE,:SYS_CREATED_BY,SYSDATE,:SYS_LAST_MODIFIED_BY,SYSDATE,:SYS_ENT_STATE,:ACCOUNT_ID,:ACCOUNT_NAME)";


                        string sqlinsertaudit = " INSERT INTO AUD_ROUTING_HEADER (TRANSACTION_ID, TRANSACTION_ACTION, ROUTING_ID_OV, ROUTING_ID_NV, CHANNEL_ID_OV, CHANNEL_ID_NV, GEOGRAPHY_ID_OV, GEOGRAPHY_ID_NV, " +
                                                " PRODUCT_ID_OV, PRODUCT_ID_NV, ITEM_OV, ITEM_NV, TYPE_OV, TYPE_NV, SSC_OV, SSC_NV, SHIP_VIA_OV, SHIP_VIA_NV, SYS_SOURCE, SYS_CREATED_BY, SYS_CREATION_DATE, SYS_LAST_MODIFIED_BY_OV, " +
                                                 " SYS_LAST_MODIFIED_BY_NV,SYS_LAST_MODIFIED_DATE_OV ,SYS_LAST_MODIFIED_DATE_NV, SYS_ENT_STATE, CHANNEL_LEVEL_OV, CHANNEL_LEVEL_NV,GEOGRAPHY_LEVEL_OV, GEOGRAPHY_LEVEL_NV, ACCOUNT_ID_OV, ACCOUNT_ID_NV, ACCOUNT_NAME_OV, ACCOUNT_NAME_NV ) VALUES (seq_transaction_id.nextval, :TRANSACTION_ACTION," +
                                                 " :ROUTING_ID_OV, :ROUTING_ID_NV, :CHANNEL_ID_OV, :CHANNEL_ID_NV, :GEOGRAPHY_ID_OV,  :GEOGRAPHY_ID_NV, :PRODUCT_ID_OV, :PRODUCT_ID_NV, :ITEM_OV, :ITEM_NV, :TYPE_OV, :TYPE_NV, " +
                                                 " :SSC_OV, :SSC_NV, :SHIP_VIA_OV, :SHIP_VIA_NV, :SYS_SOURCE, :SYS_CREATED_BY, SYSDATE, :SYS_LAST_MODIFIED_BY_OV, :SYS_LAST_MODIFIED_BY_NV, SYSDATE, SYSDATE, :SYS_ENT_STATE, :CHANNEL_LEVEL_OV,:CHANNEL_LEVEL_NV,:GEOGRAPHY_LEVEL_OV,:GEOGRAPHY_LEVEL_NV, :ACCOUNT_ID_OV, :ACCOUNT_ID_NV, :ACCOUNT_NAME_OV, :ACCOUNT_NAME_NV ) ";

                        using (OracleCommand cmdInsert = new OracleCommand(sqlInsert, conn))
                        {

                            OracleParameter param0 = new OracleParameter("ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            param0.Value = uniqueID;
                            cmdInsert.Parameters.Add(param0);


                            OracleParameter param1 = new OracleParameter("CHANNEL_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            param1.Value = Full.Channel;
                            cmdInsert.Parameters.Add(param1);


                            OracleParameter param2 = new OracleParameter("GEOGRAPHY_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            param2.Value = Full.Geography;
                            cmdInsert.Parameters.Add(param2);

                            OracleParameter param3 = new OracleParameter("PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            param3.Value = Full.product_id;
                            cmdInsert.Parameters.Add(param3);

                            OracleParameter param4 = new OracleParameter("ITEM", OracleDbType.Varchar2, ParameterDirection.Input);
                            param4.Value = Full.item1;
                            cmdInsert.Parameters.Add(param4);

                            OracleParameter param5 = new OracleParameter("TYPE", OracleDbType.Varchar2, ParameterDirection.Input);
                            param5.Value = Full.producttype;
                            cmdInsert.Parameters.Add(param5);

                            OracleParameter param6 = new OracleParameter("SSC", OracleDbType.Varchar2, ParameterDirection.Input);
                            param6.Value = Full.SupplyChainType;
                            cmdInsert.Parameters.Add(param6);

                            OracleParameter param7 = new OracleParameter("SHIP_VIA", OracleDbType.Varchar2, ParameterDirection.Input);
                            if (!string.IsNullOrEmpty(Full.ship_via))
                            {
                                param7.Value = Full.ship_via;
                            }
                            else
                            {
                                param7.Value = DBNull.Value;
                            }
                            cmdInsert.Parameters.Add(param7);

                            //Date Ranges added by Kanishka for multi site changes.
                            OracleParameter param8 = new OracleParameter("EFFECTIVE_START_DATE", OracleDbType.Date, ParameterDirection.Input);

                            if (Full.effectiveBeginDate != null && Full.effectiveBeginDate != string.Empty && Full.effectiveBeginDate != " ")
                            {
                                param8.Value = DateTime.Parse(Full.effectiveBeginDate);
                            }
                            cmdInsert.Parameters.Add(param8);

                            OracleParameter param9 = new OracleParameter("EFFECTIVE_END_DATE", OracleDbType.Date, ParameterDirection.Input);

                            if (Full.effectiveEndDate != null && Full.effectiveEndDate != string.Empty && Full.effectiveEndDate != " ")
                            {
                                param9.Value = DateTime.Parse(Full.effectiveEndDate);
                            }
                            cmdInsert.Parameters.Add(param9);

                            //End of code change for multi sites.

                            OracleParameter param10 = new OracleParameter("ROUTING_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            param10.Value = Full.Routing;
                            cmdInsert.Parameters.Add(param10);

                            OracleParameter param11 = new OracleParameter("GEOGRAPHY_LEVEL", OracleDbType.Varchar2, ParameterDirection.Input);
                            param11.Value = Full.geoLevel;
                            cmdInsert.Parameters.Add(param11);

                            OracleParameter param12 = new OracleParameter("CHANNEL_LEVEL", OracleDbType.Varchar2, ParameterDirection.Input);
                            param12.Value = Full.channelLevel;
                            cmdInsert.Parameters.Add(param12);

                            OracleParameter param13 = new OracleParameter("SYS_SOURCE", OracleDbType.Varchar2, ParameterDirection.Input);
                            param13.Value = Full.sys_source;
                            cmdInsert.Parameters.Add(param13);

                            OracleParameter param14 = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                            param14.Value = this.UserID;
                            cmdInsert.Parameters.Add(param14);

                            OracleParameter param15 = new OracleParameter("SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                            param15.Value = this.UserID;
                            cmdInsert.Parameters.Add(param15);

                            OracleParameter param16 = new OracleParameter("SYS_ENT_STATE", OracleDbType.Varchar2, ParameterDirection.Input);
                            param16.Value = Full.sys_Ent_State;
                            cmdInsert.Parameters.Add(param16);

                            OracleParameter param17 = new OracleParameter("ACCOUNT_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            if (!string.IsNullOrEmpty(Full.accountId))
                            {
                                param17.Value = Full.accountId;
                            }
                            else
                            {
                                param17.Value = DBNull.Value;
                            }
                            cmdInsert.Parameters.Add(param17);

                            OracleParameter param18 = new OracleParameter("ACCOUNT_NAME", OracleDbType.Varchar2, ParameterDirection.Input);
                            if (!string.IsNullOrEmpty(Full.accountName))
                            {
                                param18.Value = Full.accountName;
                            }
                            else
                            {
                                param18.Value = DBNull.Value;
                            }
                            cmdInsert.Parameters.Add(param18);

                            count = (int)cmdInsert.ExecuteNonQuery();


                        }

                        using (OracleCommand cmdInsertaudit = new OracleCommand(sqlinsertaudit, conn))
                        {
                            OracleParameter param1 = new OracleParameter("TRANSACTION_ACTION", OracleDbType.Varchar2, ParameterDirection.Input);
                            param1.Value = "INSERT";
                            cmdInsertaudit.Parameters.Add(param1);

                            OracleParameter param2 = new OracleParameter("ROUTING_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param2.Value = null;
                            cmdInsertaudit.Parameters.Add(param2);

                            OracleParameter param3 = new OracleParameter("ROUTING_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param3.Value = Full.Routing;
                            cmdInsertaudit.Parameters.Add(param3);

                            OracleParameter param4 = new OracleParameter("CHANNEL_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param4.Value = null;
                            cmdInsertaudit.Parameters.Add(param4);

                            OracleParameter param5 = new OracleParameter("CHANNEL_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param5.Value = Full.Channel;
                            cmdInsertaudit.Parameters.Add(param5);

                            OracleParameter param6 = new OracleParameter("GEOGRAPHY_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param6.Value = null;
                            cmdInsertaudit.Parameters.Add(param6);

                            OracleParameter param7 = new OracleParameter("GEOGRAPHY_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param7.Value = Full.Geography;
                            cmdInsertaudit.Parameters.Add(param7);

                            OracleParameter param8 = new OracleParameter("PRODUCT_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param8.Value = null;
                            cmdInsertaudit.Parameters.Add(param8);

                            OracleParameter param9 = new OracleParameter("PRODUCT_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param9.Value = Full.product_id;
                            cmdInsertaudit.Parameters.Add(param9);

                            OracleParameter param10 = new OracleParameter("ITEM_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param10.Value = null;
                            cmdInsertaudit.Parameters.Add(param10);

                            OracleParameter param11 = new OracleParameter("ITEM_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param11.Value = Full.item1;
                            cmdInsertaudit.Parameters.Add(param11);

                            OracleParameter param12 = new OracleParameter("TYPE_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param12.Value = null;
                            cmdInsertaudit.Parameters.Add(param12);

                            OracleParameter param13 = new OracleParameter("TYPE_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param13.Value = Full.producttype;
                            cmdInsertaudit.Parameters.Add(param13);

                            OracleParameter param14 = new OracleParameter("SSC_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param14.Value = null;
                            cmdInsertaudit.Parameters.Add(param14);

                            OracleParameter param15 = new OracleParameter("SSC_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param15.Value = Full.SupplyChainType;
                            cmdInsertaudit.Parameters.Add(param15);

                            OracleParameter param16 = new OracleParameter("SHIP_VIA_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param16.Value = null;
                            cmdInsertaudit.Parameters.Add(param16);

                            OracleParameter param17 = new OracleParameter("SHIP_VIA_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param17.Value = Full.ship_via;
                            cmdInsertaudit.Parameters.Add(param17);


                            OracleParameter param18 = new OracleParameter("SYS_SOURCE", OracleDbType.Varchar2, ParameterDirection.Input);
                            param18.Value = Full.sys_source;
                            cmdInsertaudit.Parameters.Add(param18);

                            OracleParameter param19 = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                            param19.Value = this.UserID;
                            cmdInsertaudit.Parameters.Add(param19);

                            OracleParameter param20 = new OracleParameter("SYS_LAST_MODIFIED_BY_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param20.Value = this.UserID;
                            cmdInsertaudit.Parameters.Add(param20);

                            OracleParameter param21 = new OracleParameter("SYS_LAST_MODIFIED_BY_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param21.Value = this.UserID;
                            cmdInsertaudit.Parameters.Add(param21);

                            OracleParameter param22 = new OracleParameter("SYS_ENT_STATE", OracleDbType.Varchar2, ParameterDirection.Input);
                            param22.Value = Full.sys_Ent_State;
                            cmdInsertaudit.Parameters.Add(param22);

                            OracleParameter param23 = new OracleParameter("CHANNEL_LEVEL_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param23.Value = null;
                            cmdInsertaudit.Parameters.Add(param23);

                            OracleParameter param24 = new OracleParameter("CHANNEL_LEVEL_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param24.Value = Full.channelLevel;
                            cmdInsertaudit.Parameters.Add(param24);

                            OracleParameter param25 = new OracleParameter("GEOGRAPHY_LEVEL_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param25.Value = null;
                            cmdInsertaudit.Parameters.Add(param25);

                            OracleParameter param26 = new OracleParameter("GEOGRAPHY_LEVEL_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param26.Value = Full.geoLevel;
                            cmdInsertaudit.Parameters.Add(param26);

                            OracleParameter param27 = new OracleParameter("ACCOUNT_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param27.Value = DBNull.Value;
                            cmdInsertaudit.Parameters.Add(param27);

                            OracleParameter param28 = new OracleParameter("ACCOUNT_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param28.Value = Full.accountId;
                            cmdInsertaudit.Parameters.Add(param28);

                            OracleParameter param29 = new OracleParameter("ACCOUNT_NAME_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param29.Value = DBNull.Value;
                            cmdInsertaudit.Parameters.Add(param29);

                            OracleParameter param30 = new OracleParameter("ACCOUNT_NAME_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param30.Value = Full.accountName;
                            cmdInsertaudit.Parameters.Add(param30);

                            cmdInsertaudit.ExecuteNonQuery();
                        }
                    }

                }

                catch (Exception ex)
                {
                    throw;
                }

                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                        conn.Dispose();
                    }
                }

                return count;
            //});
        }

        public int AddFullFillmentsites(FullFillmentRuleData itFC)
        {
            int count = 0;
            OracleConnection conn = null;
            try
            {
                string sqlInsert = "INSERT INTO ROUTING_LINES(ROUTING_ID, FULFILLMENT_SITE_ID, SPLIT,SYS_SOURCE,SYS_CREATED_BY,SYS_CREATION_DATE, SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE, SYS_ENT_STATE) "
                                    + "VALUES (:ROUTING_ID,:FULFILLMENT_SITE_ID,:SPLIT,:SYS_SOURCE,:SYS_CREATED_BY,SYSDATE,:SYS_LAST_MODIFIED_BY,SYSDATE,:SYS_ENT_STATE) ";

                string sqlInsertAudit = " INSERT INTO AUD_ROUTING_LINES(TRANSACTION_ID, TRANSACTION_ACTION, ROUTING_ID_OV, ROUTING_ID_NV, FULFILLMENT_SITE_ID_OV, FULFILLMENT_SITE_ID_NV, SPLIT_OV, SPLIT_NV,"
                                         + " SYS_SOURCE, SYS_CREATED_BY, SYS_CREATION_DATE, SYS_LAST_MODIFIED_BY_OV, SYS_LAST_MODIFIED_BY_NV, SYS_LAST_MODIFIED_DATE_OV, SYS_LAST_MODIFIED_DATE_NV, SYS_ENT_STATE  )"
                                         + " VALUES ( seq_transaction_id.nextval, :TRANSACTION_ACTION , :ROUTING_ID_OV, :ROUTING_ID_NV, :FULFILLMENT_SITE_ID_OV, :FULFILLMENT_SITE_ID_NV, "
                                         + ":SPLIT_OV, :SPLIT_NV, :SYS_SOURCE, :SYS_CREATED_BY, SYSDATE, :SYS_LAST_MODIFIED_BY_OV, :SYS_LAST_MODIFIED_BY_NV, SYSDATE, SYSDATE, :SYS_ENT_STATE )";


                using (conn = new OracleConnection(connenctionString))
                {
                    conn.Open();

                    //updating the transportation time
                    using (OracleCommand cmdInsert = new OracleCommand(sqlInsert, conn))
                    {
                        OracleParameter param1 = new OracleParameter("ROUTING_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                        param1.Value = itFC.Routing;
                        cmdInsert.Parameters.Add(param1);


                        OracleParameter param2 = new OracleParameter("FULFILLMENT_SITE_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                        param2.Value = itFC.sites;
                        cmdInsert.Parameters.Add(param2);

                        OracleParameter param3 = new OracleParameter("SPLIT", OracleDbType.Varchar2, ParameterDirection.Input);
                        param3.Value = itFC.Split;
                        cmdInsert.Parameters.Add(param3);

                        OracleParameter param4 = new OracleParameter("SYS_SOURCE", OracleDbType.Varchar2, ParameterDirection.Input);
                        param4.Value = itFC.sys_source;
                        cmdInsert.Parameters.Add(param4);

                        OracleParameter param5 = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                        param5.Value = this.UserID;
                        cmdInsert.Parameters.Add(param5);

                        OracleParameter param6 = new OracleParameter("SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                        param6.Value = this.UserID;
                        cmdInsert.Parameters.Add(param6);

                        OracleParameter param7 = new OracleParameter("SYS_ENT_STATE", OracleDbType.Varchar2, ParameterDirection.Input);
                        param7.Value = itFC.sys_Ent_State;
                        cmdInsert.Parameters.Add(param7);

                        count = (int)cmdInsert.ExecuteNonQuery();


                    }
                    using (OracleCommand cmdInsertAudit = new OracleCommand(sqlInsertAudit, conn))
                    {

                        OracleParameter param1 = new OracleParameter("TRANSACTION_ACTION", OracleDbType.Varchar2, ParameterDirection.Input);
                        param1.Value = "INSERT";
                        cmdInsertAudit.Parameters.Add(param1);

                        OracleParameter param2 = new OracleParameter("ROUTING_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                        param2.Value = DBNull.Value;
                        cmdInsertAudit.Parameters.Add(param2);

                        OracleParameter param3 = new OracleParameter("ROUTING_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                        param3.Value = itFC.Routing;
                        cmdInsertAudit.Parameters.Add(param3);

                        OracleParameter param4 = new OracleParameter("FULFILLMENT_SITE_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                        param4.Value = DBNull.Value;
                        cmdInsertAudit.Parameters.Add(param4);

                        OracleParameter param5 = new OracleParameter("FULFILLMENT_SITE_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                        param5.Value = itFC.sites;
                        cmdInsertAudit.Parameters.Add(param5);

                        OracleParameter param6 = new OracleParameter("SPLIT_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                        param6.Value = DBNull.Value;
                        cmdInsertAudit.Parameters.Add(param6);

                        OracleParameter param7 = new OracleParameter("SPLIT_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                        param7.Value = itFC.Split;
                        cmdInsertAudit.Parameters.Add(param7);

                        OracleParameter param8 = new OracleParameter("SYS_SOURCE", OracleDbType.Varchar2, ParameterDirection.Input);
                        param8.Value = itFC.sys_source;
                        cmdInsertAudit.Parameters.Add(param8);

                        OracleParameter param9 = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                        param9.Value = this.UserID;
                        cmdInsertAudit.Parameters.Add(param9);

                        OracleParameter param10 = new OracleParameter("SYS_LAST_MODIFIED_BY_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                        param10.Value = this.UserID;
                        cmdInsertAudit.Parameters.Add(param10);

                        OracleParameter param11 = new OracleParameter("SYS_LAST_MODIFIED_BY_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                        param11.Value = itFC.last_modified_by;
                        cmdInsertAudit.Parameters.Add(param11);

                        OracleParameter param12 = new OracleParameter("SYS_ENT_STATE", OracleDbType.Varchar2, ParameterDirection.Input);
                        param12.Value = itFC.sys_Ent_State;
                        cmdInsertAudit.Parameters.Add(param12);

                        cmdInsertAudit.ExecuteNonQuery();

                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }

            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return count = 0;
        }

        public bool UpdateValidateEffectiveDates(UpdateValidateDateInputParams lstUpdateValidateParams)
        {

            string sqlText = string.Empty;
            string retValue = string.Empty;
            OracleConnection conn = null;

            if (lstUpdateValidateParams.strEffectiveBeginDate == null || lstUpdateValidateParams.strEffectiveBeginDate == " " || lstUpdateValidateParams.strEffectiveBeginDate == string.Empty)
                lstUpdateValidateParams.strEffectiveBeginDate = null;

            if (lstUpdateValidateParams.strEffectiveEndDate == null || lstUpdateValidateParams.strEffectiveEndDate == " " || lstUpdateValidateParams.strEffectiveEndDate == string.Empty)
                lstUpdateValidateParams.strEffectiveEndDate = null;
            try
            {
                using (conn = new OracleConnection(connenctionString))
                {
                    sqlText = "select SNOP_RP_UI_UTIL_PKG.FN_FSA_UPDATE_WEEK_VALIDATION('" + lstUpdateValidateParams.routing + "'," + "TO_DATE('" + lstUpdateValidateParams.strEffectiveBeginDate + "','mm/dd/yyyy')," + "TO_DATE('" + lstUpdateValidateParams.strEffectiveEndDate + "','mm/dd/yyyy')) MESSAGE from dual";

                    using (OracleCommand cmdID = new OracleCommand(sqlText, conn))
                    {
                        conn.Open();
                        OracleDataReader oracleDataReader = cmdID.ExecuteReader(CommandBehavior.CloseConnection);

                        while (oracleDataReader.Read())
                        {
                            retValue = oracleDataReader["MESSAGE"].ToString();
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw;
            }

            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            if (retValue == "SUCCESS")
            {
                return true;
            }
            else
                return false;
        }

        public int updatefulfillment(FullFillmentRuleData updateFulfillment)
        {
            //Declarations.
            DataSet ds = new DataSet();
            string dateFormat = "mm/dd/yyyy";
            int count;
            count = 0;
            string sqlQueryUpdate = null;
            OracleConnection conn = null;

            try
            {
                //Query to update the Routing Lines
                string sqlQuery = "Update ROUTING_LINES SET SPLIT='" + updateFulfillment.Split + "' where FULFILLMENT_SITE_ID ='" + updateFulfillment.sites + "' AND ROUTING_ID='" +    updateFulfillment.Routing + "'";

                //Append Effective dates here.As per the multi site requirement.By Kanishka
                if (updateFulfillment.effectiveBeginDate != null && updateFulfillment.effectiveBeginDate != " " && updateFulfillment.effectiveBeginDate != string.Empty)
                {
                    sqlQueryUpdate = "Update ROUTING_HEADER SET SYS_LAST_MODIFIED_DATE = SYSDATE ,EFFECTIVE_START_DATE = to_date('" + updateFulfillment.effectiveBeginDate + "','" + dateFormat + "'),EFFECTIVE_END_DATE = null,SYS_LAST_MODIFIED_BY='" + updateFulfillment.sys_created_by + "' WHERE ROUTING_ID='" + updateFulfillment.Routing + "'";
                }

                if (updateFulfillment.effectiveEndDate != null && updateFulfillment.effectiveEndDate != " " && updateFulfillment.effectiveEndDate != string.Empty)
                {
                    sqlQueryUpdate = "Update ROUTING_HEADER SET SYS_LAST_MODIFIED_DATE = SYSDATE ,EFFECTIVE_START_DATE = null,EFFECTIVE_END_DATE = to_date('" + updateFulfillment.effectiveEndDate + "','" + dateFormat + "'), SYS_LAST_MODIFIED_BY='" + updateFulfillment.sys_created_by + "' WHERE ROUTING_ID='" + updateFulfillment.Routing + "'";
                }

                if (updateFulfillment.effectiveBeginDate != null && updateFulfillment.effectiveEndDate != null && updateFulfillment.effectiveBeginDate != " " && updateFulfillment.effectiveBeginDate != string.Empty && updateFulfillment.effectiveEndDate != " " && updateFulfillment.effectiveEndDate != string.Empty)
                {
                    sqlQueryUpdate = "Update ROUTING_HEADER SET SYS_LAST_MODIFIED_DATE = SYSDATE ,EFFECTIVE_START_DATE = to_date('" + updateFulfillment.effectiveBeginDate + "','" + dateFormat + "') ,EFFECTIVE_END_DATE = to_date('" + updateFulfillment.effectiveEndDate + "','" + dateFormat + "'), SYS_LAST_MODIFIED_BY='" + updateFulfillment.sys_created_by + "' WHERE ROUTING_ID='" + updateFulfillment.Routing + "'";
                }

                if (sqlQueryUpdate == null)
                {
                    sqlQueryUpdate = "Update ROUTING_HEADER SET SYS_LAST_MODIFIED_DATE = SYSDATE ,EFFECTIVE_START_DATE = null,EFFECTIVE_END_DATE = null, SYS_LAST_MODIFIED_BY='" + updateFulfillment.sys_created_by + "' WHERE ROUTING_ID='" + updateFulfillment.Routing + "'";
                }


                using (conn = new OracleConnection(connenctionString))
                {
                    conn.Open();

                    string sqlselect = "Select * from ROUTING_LINES_VW where ROUTING_ID = :ROUTING_ID AND FULFILLMENT_SITE_ID = :FULFILLMENT";

                    using (OracleDataAdapter da = new OracleDataAdapter(sqlselect, conn))
                    {
                        OracleParameter param1 = new OracleParameter("ROUTING_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                        param1.Value = updateFulfillment.Routing;
                        da.SelectCommand.Parameters.Add(param1);

                        OracleParameter param2 = new OracleParameter("FULFILLMENT", OracleDbType.Varchar2, ParameterDirection.Input);
                        param2.Value = updateFulfillment.sites;
                        da.SelectCommand.Parameters.Add(param2);

                        da.Fill(ds);
                    }

                    DataTable dt1 = ds.Tables[0];

                    foreach (DataRow row in dt1.Rows)
                    {
                        string var1 = row["ROUTING_ID"].ToString();
                        string var2 = row["FULFILLMENT_SITE_ID"].ToString();
                        string var3 = row["SPLIT"].ToString();
                        string syssource = row["SYS_SOURCE"].ToString();
                        string createdby = row["SYS_CREATED_BY"].ToString();
                        string createddate = row["SYS_CREATION_DATE"].ToString();
                        string lastmodi = row["SYS_LAST_MODIFIED_BY"].ToString();
                        string lastdate = row["SYS_LAST_MODIFIED_DATE"].ToString();
                        string entstate = row["SYS_ENT_STATE"].ToString();
                        string format = "mm/dd/yyyy hh:mi:ssam";
                        //Query to insert the record in the Audit table.
                        string sqlInsertAudit = " INSERT INTO AUD_ROUTING_LINES(TRANSACTION_ID, TRANSACTION_ACTION, ROUTING_ID_OV, ROUTING_ID_NV, FULFILLMENT_SITE_ID_OV, FULFILLMENT_SITE_ID_NV, SPLIT_OV, SPLIT_NV,"
                                           + " SYS_SOURCE, SYS_CREATED_BY, SYS_CREATION_DATE, SYS_LAST_MODIFIED_BY_OV, SYS_LAST_MODIFIED_BY_NV, SYS_LAST_MODIFIED_DATE_OV, SYS_LAST_MODIFIED_DATE_NV, SYS_ENT_STATE  )"
                                           + " VALUES ( seq_transaction_id.nextval, :TRANSACTION_ACTION , :ROUTING_ID_OV, :ROUTING_ID_NV, :FULFILLMENT_SITE_ID_OV, :FULFILLMENT_SITE_ID_NV, "
                                           + ":SPLIT_OV, :SPLIT_NV, :SYS_SOURCE, :SYS_CREATED_BY, to_date('" + createddate + "','" + format + "'), :SYS_LAST_MODIFIED_BY_OV, :SYS_LAST_MODIFIED_BY_NV, to_date('" + lastdate + "' , '" + format + "'), SYSDATE, :SYS_ENT_STATE )";
                        using (OracleCommand cmdInsertAudit = new OracleCommand(sqlInsertAudit, conn))
                        {

                            OracleParameter param1 = new OracleParameter("TRANSACTION_ACTION", OracleDbType.Varchar2, ParameterDirection.Input);
                            param1.Value = "UPDATE";
                            cmdInsertAudit.Parameters.Add(param1);

                            OracleParameter param2 = new OracleParameter("ROUTING_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param2.Value = var1;
                            cmdInsertAudit.Parameters.Add(param2);

                            OracleParameter param3 = new OracleParameter("ROUTING_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param3.Value = var1;
                            cmdInsertAudit.Parameters.Add(param3);

                            OracleParameter param4 = new OracleParameter("FULFILLMENT_SITE_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param4.Value = var2;
                            cmdInsertAudit.Parameters.Add(param4);

                            OracleParameter param5 = new OracleParameter("FULFILLMENT_SITE_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param5.Value = updateFulfillment.sites;
                            cmdInsertAudit.Parameters.Add(param5);

                            OracleParameter param6 = new OracleParameter("SPLIT_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param6.Value = var3;
                            cmdInsertAudit.Parameters.Add(param6);

                            OracleParameter param7 = new OracleParameter("SPLIT_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param7.Value = updateFulfillment.Split;
                            cmdInsertAudit.Parameters.Add(param7);

                            OracleParameter param8 = new OracleParameter("SYS_SOURCE", OracleDbType.Varchar2, ParameterDirection.Input);
                            param8.Value = syssource;
                            cmdInsertAudit.Parameters.Add(param8);

                            OracleParameter param9 = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                            param9.Value = createdby;
                            cmdInsertAudit.Parameters.Add(param9);

                            OracleParameter param10 = new OracleParameter("SYS_LAST_MODIFIED_BY_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param10.Value = lastmodi;
                            cmdInsertAudit.Parameters.Add(param10);

                            OracleParameter param11 = new OracleParameter("SYS_LAST_MODIFIED_BY_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param11.Value = updateFulfillment.sys_created_by;
                            cmdInsertAudit.Parameters.Add(param11);


                            OracleParameter param12 = new OracleParameter("SYS_ENT_STATE", OracleDbType.Varchar2, ParameterDirection.Input);
                            param12.Value = "ACTIVE";
                            cmdInsertAudit.Parameters.Add(param12);

                            cmdInsertAudit.ExecuteNonQuery(); // Inserts Record in the Audit table.
                        }

                        using (OracleCommand com = new OracleCommand(sqlQuery, conn))
                        {
                            count = (int)com.ExecuteNonQuery(); //Updated the record in the Routing Lines table.
                        }

                        using (OracleCommand cmdUpdte = new OracleCommand(sqlQueryUpdate, conn))
                        {
                            count = (int)cmdUpdte.ExecuteNonQuery(); // Updates the record in the Routing Header table.
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return count;
        }

        public int DeleteFulfillDetails(FullFillmentRuleData lstFulfillDeleteSite)
        {
            int count = 0;
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            OracleConnection conn = null;

            try
            {
                using (conn = new OracleConnection(connenctionString))
                {
                    conn.Open();
                    string sqlselect = "Select * from ROUTING_LINES_VW where ROUTING_ID = + :ROUTING_ID AND FULFILLMENT_SITE_ID=  :FULFILLMENT ";
                    using (OracleDataAdapter da = new OracleDataAdapter(sqlselect, conn))
                    {
                        OracleParameter param1 = new OracleParameter("ROUTING_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                        param1.Value = lstFulfillDeleteSite.Routing;
                        da.SelectCommand.Parameters.Add(param1);

                        OracleParameter param2 = new OracleParameter("FULFILLMENT", OracleDbType.Varchar2, ParameterDirection.Input);
                        param2.Value = lstFulfillDeleteSite.sites;
                        da.SelectCommand.Parameters.Add(param2);
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        dt1 = ds.Tables[0];
                    }
                    //Microsoft.Security.Application.AntiXss.HtmlEncode(dt1.ToString());
                    foreach (DataRow row in dt1.Rows)
                    {
                        string var1 = row["ROUTING_ID"].ToString();
                        string var2 = row["FULFILLMENT_SITE_ID"].ToString();
                        string var3 = row["SPLIT"].ToString();
                        string syssource = row["SYS_SOURCE"].ToString();
                        string createdby = row["SYS_CREATED_BY"].ToString();
                        string createddate = row["SYS_CREATION_DATE"].ToString();
                        string lastmodi = row["SYS_LAST_MODIFIED_BY"].ToString();
                        string lastdate = row["SYS_LAST_MODIFIED_DATE"].ToString();
                        string entstate = row["SYS_ENT_STATE"].ToString();
                        string format = "mm/dd/yyyy hh:mi:ssam";
                        string sqlInsertAudit = " INSERT INTO AUD_ROUTING_LINES(TRANSACTION_ID, TRANSACTION_ACTION, ROUTING_ID_OV, ROUTING_ID_NV, FULFILLMENT_SITE_ID_OV, FULFILLMENT_SITE_ID_NV, SPLIT_OV, SPLIT_NV,"
                                           + " SYS_SOURCE, SYS_CREATED_BY, SYS_CREATION_DATE, SYS_LAST_MODIFIED_BY_OV, SYS_LAST_MODIFIED_BY_NV, SYS_LAST_MODIFIED_DATE_OV, SYS_LAST_MODIFIED_DATE_NV, SYS_ENT_STATE  )"
                                           + " VALUES ( seq_transaction_id.nextval, :TRANSACTION_ACTION , :ROUTING_ID_OV, :ROUTING_ID_NV, :FULFILLMENT_SITE_ID_OV, :FULFILLMENT_SITE_ID_NV, "
                                           + ":SPLIT_OV, :SPLIT_NV, :SYS_SOURCE, :SYS_CREATED_BY, To_date('" + createddate + "', '" + format + "'), :SYS_LAST_MODIFIED_BY_OV, :SYS_LAST_MODIFIED_BY_NV, To_date('" + lastdate + "', '" + format + "'), sysdate, :SYS_ENT_STATE )";
                        using (OracleCommand cmdInsertAudit = new OracleCommand(sqlInsertAudit, conn))
                        {

                            OracleParameter param1 = new OracleParameter("TRANSACTION_ACTION", OracleDbType.Varchar2, ParameterDirection.Input);
                            param1.Value = "DELETE";
                            cmdInsertAudit.Parameters.Add(param1);

                            OracleParameter param2 = new OracleParameter("ROUTING_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param2.Value = var1;
                            cmdInsertAudit.Parameters.Add(param2);

                            OracleParameter param3 = new OracleParameter("ROUTING_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param3.Value = DBNull.Value;
                            cmdInsertAudit.Parameters.Add(param3);

                            OracleParameter param4 = new OracleParameter("FULFILLMENT_SITE_ID_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param4.Value = var2;
                            cmdInsertAudit.Parameters.Add(param4);

                            OracleParameter param5 = new OracleParameter("FULFILLMENT_SITE_ID_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param5.Value = DBNull.Value;
                            cmdInsertAudit.Parameters.Add(param5);

                            OracleParameter param6 = new OracleParameter("SPLIT_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param6.Value = var3;
                            cmdInsertAudit.Parameters.Add(param6);

                            OracleParameter param7 = new OracleParameter("SPLIT_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param7.Value = DBNull.Value;
                            cmdInsertAudit.Parameters.Add(param7);

                            OracleParameter param8 = new OracleParameter("SYS_SOURCE", OracleDbType.Varchar2, ParameterDirection.Input);
                            param8.Value = syssource;
                            cmdInsertAudit.Parameters.Add(param8);

                            OracleParameter param9 = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                            param9.Value = createdby;
                            cmdInsertAudit.Parameters.Add(param9);

                            OracleParameter param10 = new OracleParameter("SYS_LAST_MODIFIED_BY_OV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param10.Value = lastmodi;
                            cmdInsertAudit.Parameters.Add(param10);

                            OracleParameter param11 = new OracleParameter("SYS_LAST_MODIFIED_BY_NV", OracleDbType.Varchar2, ParameterDirection.Input);
                            param11.Value = lstFulfillDeleteSite.sys_created_by;
                            cmdInsertAudit.Parameters.Add(param11);


                            OracleParameter param13 = new OracleParameter("SYS_ENT_STATE", OracleDbType.Varchar2, ParameterDirection.Input);
                            param13.Value = "ACTIVE";
                            cmdInsertAudit.Parameters.Add(param13);

                            cmdInsertAudit.ExecuteNonQuery();

                        }
                    }
                    string sqlDelete = "Delete ROUTING_LINES"
                                        + " WHERE ROUTING_ID = :ROUTING_ID AND FULFILLMENT_SITE_ID= :FULFILLMENT";



                    using (OracleCommand cmdDelete = new OracleCommand(sqlDelete, conn))
                    {

                        OracleParameter param1 = new OracleParameter("ROUTING_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                        param1.Value = lstFulfillDeleteSite.Routing;
                        cmdDelete.Parameters.Add(param1);

                        OracleParameter param2 = new OracleParameter("FULFILLMENT", OracleDbType.Varchar2, ParameterDirection.Input);
                        param2.Value = lstFulfillDeleteSite.sites;
                        cmdDelete.Parameters.Add(param2);

                        count = (int)cmdDelete.ExecuteNonQuery();


                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return count;
        }
    }
}
