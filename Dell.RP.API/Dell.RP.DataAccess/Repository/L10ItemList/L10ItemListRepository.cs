﻿using Dell.RP.DataAccess;
using Microsoft.Extensions.Options;
using Dell.RP.API.Dell.RP.DataAccess.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.Common.Models.MultiTierBOM;
using System.Data;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.L10ItemList;
using Dell.RP.API.Dell.RP.Common.Models.L10ItemList;

namespace Dell.RP.API.Dell.RP.DataAccess.Repository.L10ItemList
{
    public class L10ItemListRepository: IL10ItemListRepository
    {
        private readonly string connenctionString;
        public string UserID { get; set; }

        private readonly string schemaName;

        public L10ItemListRepository(IOptions<AppSettings> appSettings, ICommonData commonData)
        {
            connenctionString = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringGMP, appSettings.Value.ConnectionKey);
            schemaName = appSettings.Value.SchemaName;
        }        

        private string filterCrieteria(l10Item param)
        {
            string filter = "";
            
            if (!string.IsNullOrEmpty(param.id.Trim()))
            {
                filter += "AND I.ITEM_ID LIKE '" + param.id.ToUpper().Trim() + "%' ";
            }
            if (!string.IsNullOrEmpty(param.description.Trim()))
            {
                filter += "AND DESCRIPTION LIKE '%" + param.description.Trim() + "%' ";
            }
            return filter;
        }                      


        public async Task<List<l10Item>> Getl10ItemList()
        {
            List<l10Item> l10ItemList = new List<l10Item>();
            DataTable dt = new DataTable();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        //string sql = " SELECT ITEM_ID, DESCRIPTION, ITEM_CLASS, SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE  from MST_ITEM WHERE item_type = 'PART'";
                        string sql = "SELECT ITEM_ID, DESCRIPTION, ITEM_CLASS, SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE  from MST_ITEM WHERE ROWNUM <= 500";

                        using (OracleCommand cmd = new OracleCommand(sql, connection))
                        {
                            connection.Open();
                            OracleDataAdapter da = new OracleDataAdapter(cmd);
                            da.Fill(dt);
                        }

                        //return dt;
                        foreach(DataRow dr in dt.Rows)
                        {
                            l10ItemList.Add(new l10Item
                            {
                                id = dr["ITEM_ID"].ToString(),
                                description = dr["DESCRIPTION"].ToString(),
                                l10Class = dr["ITEM_CLASS"].ToString(),
                                lastModifiedBy = dr["SYS_LAST_MODIFIED_BY"].ToString(),
                                lastModifiedDate = dr["SYS_LAST_MODIFIED_DATE"].ToString()
                            });
                        }
                        return l10ItemList;
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }
            });
        }

        public string addl10ListItem(l10Item l10NewItem)
        {
            int count = 0;
            string returnMessage = "";
            try
            {

                //string sqlUpdate = "UPDATE MST_ITEM SET ITEM_CLASS='L10_MOD', "
                //+ "SYS_LAST_MODIFIED_BY=:SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE=SYSDATE "
                //+ "WHERE ITEM_ID=:ITEM_ID ";
                //string sqlUpdate = "UPDATE MST_ITEM SET ITEM_CLASS=:ITEM_CLASS, "
                                    //+ "SYS_LAST_MODIFIED_BY=:SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE=SYSDATE "
                                    //+ "WHERE ITEM_ID=:ITEM_ID";
                string sqlUpdate1 = "UPDATE MST_ITEM SET ITEM_CLASS='L10_MOD', "
                                    + "SYS_LAST_MODIFIED_BY=:SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE=SYSDATE "
                                    + "WHERE ITEM_ID=:ITEM_ID";
                string sqlUpdate2 = "UPDATE MST_ITEM SET ITEM_CLASS='', "
                                    + "SYS_LAST_MODIFIED_BY=:SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE=SYSDATE "
                                    + "WHERE ITEM_ID=:ITEM_ID";

                using (OracleConnection conn = new OracleConnection(this.connenctionString))
                {
                    conn.Open();
                    if (count == 0)
                    {
                        //setting up multi-tier for the first time
                        if (l10NewItem.l10Class == "L10_MOD")
                        {
                            using (OracleCommand cmdUpd = new OracleCommand(sqlUpdate1, conn))
                            {
                                OracleParameter param1 = new OracleParameter("SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                                param1.Value = UserID;
                                cmdUpd.Parameters.Add(param1);

                                OracleParameter param2 = new OracleParameter("ITEM_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                param2.Value = l10NewItem.id;
                                cmdUpd.Parameters.Add(param2);

                                //OracleParameter param3 = new OracleParameter("ITEM_CLASS", OracleDbType.Varchar2, ParameterDirection.Input);
                                //param3.Value = l10NewItem.Class == "" ? string.Empty : "L10_MOD";
                                //cmdUpd.Parameters.Add(param3);

                                count = (int)cmdUpd.ExecuteNonQuery();
                            }
                        }
                        else if(l10NewItem.l10Class == "")
                        {
                            using (OracleCommand cmdUpd = new OracleCommand(sqlUpdate2, conn))
                            {
                                OracleParameter param1 = new OracleParameter("SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                                param1.Value = UserID;
                                cmdUpd.Parameters.Add(param1);

                                OracleParameter param2 = new OracleParameter("ITEM_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                param2.Value = l10NewItem.id;
                                cmdUpd.Parameters.Add(param2);

                                count = (int)cmdUpd.ExecuteNonQuery();
                            }
                        }
                        returnMessage = 0 + "~" + "SUCCESS";
                    }
                }

            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                returnMessage = 1 + "~" + "Records could not be saved";
                throw ex;
            }
            return returnMessage;
        }

        public async Task<string> Updatel10ItemListDetails(List<l10Item> l10ItemList)
        {
            string returnMessage = "";
            return await Task.Run(() =>
            {
                try
                {
                    foreach (l10Item item in l10ItemList)
                    {
                        returnMessage = addl10ListItem(item);
                        if (returnMessage.Split("~")[0] == "1")
                        {
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    returnMessage = 1 + "~" + "Records could not be saved";
                }
                return returnMessage;
            });
        }

        public async Task<List<l10Item>> Getl10FilterViewResult(l10Item item)
        {            
            List<l10Item> l10ItemList = new List<l10Item>();
            string sql = string.Empty;
            return await Task.Run(() =>
            {
                try
                {
                    using (OracleConnection conn = new OracleConnection(this.connenctionString))
                    {
                        //if (!string.IsNullOrEmpty(filterCrieteria(item)))
                        //{
                            sql = string.Format("select * from ( "
                                                                            + " select rownum as rnum, L.* from ( "
                                                                            + " SELECT ITEM_ID, DESCRIPTION, ITEM_CLASS, SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE  from MST_ITEM I "
                                                                            + " WHERE item_type='PART' "
                                                                            + " {0} "
                                                                            //+ " order by B.IS_ENABLED DESC NULLS LAST, I.ITEM_ID ASC "
                                                                            + " ) L "
                                                                            + ") ", filterCrieteria(item))
                                                                            + " WHERE ROWNUM <= 10001";
                            //+ "where rnum > {1} * {2} and rnum <= ({1} + 1) * {2} ", filterCrieteria(item), pageIndex, pageSize);
                        //}
                        //else
                        //{
                        //    sql = "SELECT ITEM_ID, DESCRIPTION, ITEM_CLASS, SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE from MST_ITEM WHERE ROWNUM <= 5000";
                        //}
                                                                        
                        using (OracleCommand cmd = new OracleCommand(sql, conn))
                        {
                            conn.Open();
                            OracleDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                reader.FetchSize = reader.RowSize * 1000;
                            }
                            while (reader.Read())
                            {
                                l10ItemList.Add(new l10Item
                                {
                                    id = reader["ITEM_ID"].ToString(),                                    
                                    description = reader["DESCRIPTION"].ToString(),
                                    //Class = "Select...",
                                    l10Class = reader["ITEM_CLASS"].ToString() == string.Empty? "Select...": "L10_MOD",
                                    lastModifiedBy = reader["SYS_LAST_MODIFIED_BY"].ToString(),
                                    lastModifiedDate = reader["SYS_LAST_MODIFIED_DATE"].ToString()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return l10ItemList;
            });
        }

        public async Task<List<l10Item>> Getl10FilterViewResultExcel(l10Item item)
        {
            List<l10Item> l10ItemList = new List<l10Item>();
            string sql = string.Empty;
            return await Task.Run(() =>
            {
                try
                {
                    using (OracleConnection conn = new OracleConnection(this.connenctionString))
                    {
                        sql = string.Format("select * from ( SELECT ITEM_ID, DESCRIPTION, ITEM_CLASS, SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE  from MST_ITEM I WHERE item_type='PART'  and ITEM_CLASS = 'L10_MOD') WHERE ROWNUM <= 10001");
                        
                        using (OracleCommand cmd = new OracleCommand(sql, conn))
                        {
                            conn.Open();
                            OracleDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                reader.FetchSize = reader.RowSize * 1000;
                            }
                            while (reader.Read())
                            {
                                l10ItemList.Add(new l10Item
                                {
                                    id = reader["ITEM_ID"].ToString(),
                                    description = reader["DESCRIPTION"].ToString(),
                                    //Class = "Select...",
                                    l10Class = reader["ITEM_CLASS"].ToString() == string.Empty ? "Select..." : "L10_MOD",
                                    lastModifiedBy = reader["SYS_LAST_MODIFIED_BY"].ToString(),
                                    lastModifiedDate = reader["SYS_LAST_MODIFIED_DATE"].ToString()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return l10ItemList;
            });
        }

    }
}
