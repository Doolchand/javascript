﻿using Dell.RP.API.Dell.RP.DataAccess.IRepository.MultiTierBOM;
using Dell.RP.DataAccess;
using Microsoft.Extensions.Options;
using Dell.RP.API.Dell.RP.DataAccess.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.Common.Models.MultiTierBOM;
using System.Data;

namespace Dell.RP.API.Dell.RP.DataAccess.Repository.MultiTierBOM
{
    public class MultiTierBOMRepository: IMultiTierBOMRepository
    {
        private readonly string connenctionString;
        public string UserID { get; set; }

        private readonly string schemaName;

        public MultiTierBOMRepository(IOptions<AppSettings> appSettings, ICommonData commonData)
        {
            connenctionString = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringGMP, appSettings.Value.ConnectionKey);
            schemaName = appSettings.Value.SchemaName;
        }

        public async Task<List<MultiTierBOMDetails>> GetFilterViewResult(MultiTierBOMDetails param, int fromNumber, int count)
        {
            //DataTable dt = new DataTable();
            List<MultiTierBOMDetails> multiTierBOMDetails = new List<MultiTierBOMDetails>();
            return await Task.Run(() =>
            {
                try
                {
                    using (OracleConnection conn = new OracleConnection(this.connenctionString))
                    {
                        string sql = string.Format("select * from ( "
                                                    + " select rownum as rnum, L.* from ( "
                                                    + " select I.ITEM_ID, DESCRIPTION, C.COMMODITY_NAME, B.IS_ENABLED, B.SYS_LAST_MODIFIED_BY, B.SYS_LAST_MODIFIED_DATE "
                                                    + " from MST_ITEM I, MST_COMMODITY_MASTER C, MST_GMP_MULTI_TIER_BOM B  "
                                                    + " WHERE I.COMMODITY_ID=C.COMMODITY_ID "
                                                    +  filterCrieteria(param)
                                                    + " AND I.ITEM_ID=B.ITEM_ID(+) "
                                                    //+ " order by B.IS_ENABLED DESC NULLS LAST, I.ITEM_ID ASC "
                                                    + " ) L "
                                                    + ") "
                                                    + " where rnum <= 10001"
                                                     //+ "where rnum > {1} * {2} and rnum <= ({1} + 1) * {2} ", new object[] { filterCrieteria, pageIndex, pageSize });
                                                     //+ "where rnum >" + fromNumber + " and rnum <=  " + (fromNumber+count)
                                                     );                        

                        using (OracleCommand cmd = new OracleCommand(sql, conn))
                        {
                            conn.Open();
                            //OracleDataAdapter da = new OracleDataAdapter(cmd);
                            //da.Fill(dt);
                            OracleDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                reader.FetchSize = reader.RowSize * 30000;
                            }
                            while (reader.Read())
                            {
                                multiTierBOMDetails.Add(new MultiTierBOMDetails
                                {
                                    itemId = reader["ITEM_ID"].ToString(),
                                    itemDescription = reader["DESCRIPTION"].ToString(),
                                    commodityCode = reader["COMMODITY_NAME"].ToString(),
                                    multiTier = ((reader["IS_ENABLED"].ToString() == "Y") ? true : false),
                                    updatedBy = reader["SYS_LAST_MODIFIED_BY"].ToString(),
                                    updatedDate = reader["SYS_LAST_MODIFIED_DATE"].ToString(),
                                });
                            }
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return multiTierBOMDetails;
            });
        }

        private string filterCrieteria(MultiTierBOMDetails param)
        {
            string filter = "";

            if (param.itemId != "" && param.itemDescription != "" && param.commodityCode != "" && param.commodityCode != "ALL")
            {
                filter = " and upper(I.ITEM_ID) LIKE  '" + param.itemId +"'"+ " and upper(DESCRIPTION) LIKE '" + param.itemDescription + "'" + "   and C.COMMODITY_NAME='" + param.commodityCode + "'";     
            }
            else if (param.itemId != "" && param.itemDescription != "")
            {
                filter = " and upper(I.ITEM_ID) LIKE  '" + param.itemId + "'" + " and upper(DESCRIPTION) LIKE '" + param.itemDescription + "'";
            }
            else if (param.itemDescription != "" && param.commodityCode != "" && param.commodityCode != "ALL")
            {
                filter = " and upper(DESCRIPTION) LIKE '" + param.itemDescription + "'" + " and C.COMMODITY_NAME='" + param.commodityCode + "'";
            }
            else if (param.itemId != "" && param.commodityCode != "" && param.commodityCode != "ALL")
            {
                filter = " and upper(I.ITEM_ID) LIKE  '" + param.itemId + "'" + " and C.COMMODITY_NAME='" + param.commodityCode + "'";
            }
            else if (param.itemId != "")
            {
                filter = " and upper(I.ITEM_ID) LIKE  '" + param.itemId + "'";
            }
            else if (param.itemDescription != "")
            {
                filter = " and upper(DESCRIPTION) LIKE '" + param.itemDescription + "'";
            }
            else if (param.commodityCode != "" && param.commodityCode != "ALL")
            {
                filter = " and C.COMMODITY_NAME='" + param.commodityCode + "'";
            } 
            else
            {
                filter = "";
            }
            return filter;
        }

        public async Task<DataTable> GetCommodityCodeDDL()
        {
            DataTable dt = new DataTable();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {

                        string sql = "select commodity_id, commodity_name from MST_COMMODITY_MASTER ORDER BY commodity_name ASC";

                        using (OracleCommand cmd = new OracleCommand(sql, connection))
                        {
                            connection.Open();
                            OracleDataAdapter da = new OracleDataAdapter(cmd);
                            da.Fill(dt);
                        }

                        return dt;
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }
            });
        }

        


        public async Task<string> UpdateMultiTierBOMDetails(List<MultiTierBOMDetails> multiTierBOMDetails)
        {
            string returnMessage = "";
            return await Task.Run(() =>
            {
                try
                {
                    foreach(MultiTierBOMDetails multiTierBOMDetail in multiTierBOMDetails)
                    {
                        returnMessage = addMultiTierItem(multiTierBOMDetail);
                        if (returnMessage.Split("~")[0] == "1")
                        {
                            break;
                        }
                    }

                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    returnMessage = 1 + "~" + "Records could not be saved";
                    
                }

                return returnMessage;
            });
        }

        public string addMultiTierItem(MultiTierBOMDetails multiTierBOMDetail)
        {
            int count = 0;
            string returnMessage = "";
            try
            {
                string sqlInsert = "INSERT WHEN (item_count = 0) THEN INTO MST_GMP_MULTI_TIER_BOM "
                                    + "(SCENARIO_ID, ITEM_ID, IS_ENABLED, SYS_SOURCE, SYS_CREATED_BY, SYS_CREATION_DATE, "
                                    + "SYS_ENT_STATE, SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE) "
                                    + "VALUES (0, :ITEM_ID, :IS_ENABLED, 'UI', :SYS_CREATED_BY, SYSDATE, 'ACTIVE', "
                                    + ":SYS_LAST_MODIFIED_BY, SYSDATE) "
                                    + "SELECT count(*) item_count FROM MST_GMP_MULTI_TIER_BOM WHERE ITEM_ID=:ITEM_ID ";

                string sqlUpdate = "UPDATE MST_GMP_MULTI_TIER_BOM SET IS_ENABLED=:IS_ENABLED, "
                                    + "SYS_LAST_MODIFIED_BY=:SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE=SYSDATE "
                                    + "WHERE ITEM_ID=:ITEM_ID ";

                using (OracleConnection conn = new OracleConnection(this.connenctionString))
                {
                    conn.Open();

                    using (OracleCommand cmdIns = new OracleCommand(sqlInsert, conn))
                    {
                        OracleParameter param1 = new OracleParameter("ITEM_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                        param1.Value = multiTierBOMDetail.itemId;
                        cmdIns.Parameters.Add(param1);

                        OracleParameter param2 = new OracleParameter("IS_ENABLED", OracleDbType.Varchar2, ParameterDirection.Input);
                        param2.Value = multiTierBOMDetail.multiTier ? "Y" : "N";
                        cmdIns.Parameters.Add(param2);

                        OracleParameter param3 = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                        param3.Value = this.UserID;
                        cmdIns.Parameters.Add(param3);

                        OracleParameter param4 = new OracleParameter("SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                        //param3.Value = multiTierBOMDetail.updatedBy;
                        param4.Value = this.UserID;
                        cmdIns.Parameters.Add(param4);

                        

                        count = (int)cmdIns.ExecuteNonQuery();
                    }
                    returnMessage = 0 + "~" + "SUCCESS";

                    if (count == 0)
                    {
                        //setting up multi-tier for the first time
                        using (OracleCommand cmdUpd = new OracleCommand(sqlUpdate, conn))
                        {
                            OracleParameter param1 = new OracleParameter("IS_ENABLED", OracleDbType.Varchar2, ParameterDirection.Input);
                            param1.Value = multiTierBOMDetail.multiTier ? "Y" : "N";
                            cmdUpd.Parameters.Add(param1);

                            OracleParameter param2 = new OracleParameter("SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                            //param1.Value = multiTierBOMDetail.updatedBy;
                            param2.Value = this.UserID;
                            cmdUpd.Parameters.Add(param2);

                            OracleParameter param3 = new OracleParameter("ITEM_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            param3.Value = multiTierBOMDetail.itemId;
                            cmdUpd.Parameters.Add(param3);

                            

                            count = (int)cmdUpd.ExecuteNonQuery();
                        }
                        returnMessage = 0 + "~" + "SUCCESS";
                    }

                }

            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                returnMessage = 1 + "~" + "Records could not be saved";
                throw ex;
            }
            return returnMessage;
        }

        public async Task<List<MultiTierBOMDetails>> GetMultiTierViewResultExcel(MultiTierBOMDetails item)
        {
            List<MultiTierBOMDetails> multiTierList = new List<MultiTierBOMDetails>();
            string sql = string.Empty;
            return await Task.Run(() =>
            {
                try
                {
                    using (OracleConnection conn = new OracleConnection(this.connenctionString))
                    {
                        sql = string.Format("select * from("
                                                    + " select rownum as rnum, L.* from ( "
                                                    + " select I.ITEM_ID, DESCRIPTION, C.COMMODITY_NAME, B.IS_ENABLED, B.SYS_LAST_MODIFIED_BY, B.SYS_LAST_MODIFIED_DATE "
                                                    + " from MST_ITEM I, MST_COMMODITY_MASTER C, MST_GMP_MULTI_TIER_BOM B  "
                                                    + " WHERE I.COMMODITY_ID=C.COMMODITY_ID "
                                                    + " AND I.ITEM_ID=B.ITEM_ID(+) "
                                                    + " AND B.IS_ENABLED='Y' "
                                                    + " ) L "
                                                    + ") "
                                                    + " where rnum <= 10001"
                                                     );

                        using (OracleCommand cmd = new OracleCommand(sql, conn))
                        {
                            conn.Open();
                            OracleDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                reader.FetchSize = reader.RowSize * 1000;
                            }
                            while (reader.Read())
                            {
                                multiTierList.Add(new MultiTierBOMDetails
                                {
                                    itemId = reader["ITEM_ID"].ToString(),
                                    itemDescription = reader["DESCRIPTION"].ToString(),
                                    commodityCode = reader["COMMODITY_NAME"].ToString(),
                                    multiTier = ((reader["IS_ENABLED"].ToString() == "Y") ? true : false),
                                    updatedBy = reader["SYS_LAST_MODIFIED_BY"].ToString(),
                                    updatedDate = reader["SYS_LAST_MODIFIED_DATE"].ToString()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return multiTierList;
            });
        }

    }
}
