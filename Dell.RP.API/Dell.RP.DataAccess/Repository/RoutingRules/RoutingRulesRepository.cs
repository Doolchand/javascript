﻿using Dell.RP.DataAccess;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

using Dell.RP.API.Dell.RP.DataAccess.IRepository.DemandSplit;
using Dell.RP.API.Dell.RP.DataAccess.Common;
using Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.Common.Models.DemandSplit;
using Dell.RP.API.Dell.RP.Common.DemandSplit;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Text;
using Dell.RP.API.Dell.RP.Common.Models.RoutingRules;

namespace Dell.RP.API.Dell.RP.DataAccess.Repository.RoutingRules
{
    public class RoutingRulesRepository : IRepository.RoutingRules.IRoutingRulesRepository
    {
        private readonly string connenctionString;
        public string UserID { get; set; }

        private readonly string schemaName;

        OracleCommand oracleCommand = null;
        OracleDataAdapter oracleDataApt = null;
        OracleConnection oracleConnection = null;


        public RoutingRulesRepository(IOptions<AppSettings> appSettings, ICommonData commonData)
        {
            connenctionString = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringSCDH, appSettings.Value.ConnectionKey);
            schemaName = appSettings.Value.SchemaName;
        }



        public async Task<List<ItemType>> GetGeographyData()
        {
            List<ItemType> itemType = new List<ItemType>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();


                        string sql = "select distinct a.geo_group_id, a.geo_group_id || '(' || b.name ||  ')' geoname from mst_geo_group_detail_Vw a, mst_geography_vw b where geo_group_type = 'REGION'  and a.geo_group_id = b.geo_id";

                        // var query = schemaName + ".E2OPEN_UPP_EMC_PRODUCT_PKG.P_Get_Item_Attribute";
                        OracleCommand cmd = new OracleCommand(sql, connection);
                        // cmd.CommandType = CommandType.StoredProcedure;



                        OracleDataReader reader = cmd.ExecuteReader();
                        itemType.Add(new ItemType { ItemTypeId = "ALL", ItemTypeName = "ALL" });
                        while (reader.Read())
                        {
                            itemType.Add(new ItemType { ItemTypeId = reader[0].ToString(), ItemTypeName = reader[1].ToString() });
                        }


                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return itemType;
            });
        }





        




         public async Task<List<ItemType>> GetManufacturingSiteData( string strFFSite)
        {
            List<ItemType> itemType = new List<ItemType>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        string sql = "select DISTINCT FROM_SITE_NAME  from transportation_lead_time_vw where TO_SITE_NAME = '" + strFFSite + "' AND SYS_ENT_STATE = 'ACTIVE'" + " ORDER BY FROM_SITE_NAME";
                        // var query = schemaName + ".E2OPEN_UPP_EMC_PRODUCT_PKG.P_Get_Item_Attribute";
                        OracleCommand cmd = new OracleCommand(sql, connection);
                        // cmd.CommandType = CommandType.StoredProcedure;

                        OracleDataReader reader = cmd.ExecuteReader();
                       // itemType.Add(new ItemType { ItemTypeId = "Select", ItemTypeName = "Select" });
                        while (reader.Read())
                        {
                            itemType.Add(new ItemType { ItemTypeId = reader[0].ToString(), ItemTypeName = reader[0].ToString() });
                        }


                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return itemType;
            });
        }


        public async Task<List<ItemType>> GetFulfilment()

        {
            List<ItemType> itemType = new List<ItemType>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        string sql = "SELECT DISTINCT C.SITE_NAME FROM  FULFILLMENT_REGION_VW A, FULFILLMENT_SUB_REGION_VW B, FF_SUB_REGION_SITE_XREF_VW C WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME           AND B.FF_TYPE_NAME = C.FF_TYPE_NAME AND A.FF_TYPE_NAME  = 'RP_NODES' AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE " +
                                " AND A.REGION_CODE = B.REGION_CODE AND NVL(A.SYS_ENT_STATE,'ACTIVE') = 'ACTIVE' AND NVL(B.SYS_ENT_STATE,'ACTIVE') = 'ACTIVE'  AND NVL(C.SYS_ENT_STATE,'ACTIVE') = 'ACTIVE' AND A.REGION_CODE IN ( select distinct a.geo_group_id from mst_geo_group_detail_Vw a, mst_geography_vw b where a.sys_source='LIBERTY' and b.sys_source='LIBERTY' and geo_group_type = 'REGION'  AND NVL(a.SYS_ENT_STATE,'ACTIVE') = 'ACTIVE' AND NVL(b.SYS_ENT_STATE,'ACTIVE') = 'ACTIVE'and a.geo_group_id = b.geo_id ) order by C.SITE_NAME";

                        OracleCommand cmd = new OracleCommand(sql, connection);
                        // cmd.CommandType = CommandType.StoredProcedure;



                        OracleDataReader reader = cmd.ExecuteReader();
                        itemType.Add(new ItemType { ItemTypeId = "Select", ItemTypeName = "Select" });
                        while (reader.Read())
                        {
                            itemType.Add(new ItemType { ItemTypeId = reader[0].ToString(), ItemTypeName = reader[0].ToString() });
                        }


                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return itemType;
            });
        }

        // GetGeographyData(string ItemTypeId, string ItemTypeName);


        


      public async Task<List<ItemType>> GetDate()
        {
            List<ItemType> itemType = new List<ItemType>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        string sql = "select BEGIN_FISCAL_WEEK,BEGIN_CAL_DATE,END_FISCAL_WEEK,END_CAL_DATE from SNOP_RP_FISCAL_WK_LIST_VW";
                        OracleCommand cmd = new OracleCommand(sql, connection);
                       
                        OracleDataReader reader = cmd.ExecuteReader();
                        itemType.Add(new ItemType { ItemTypeId = "", ItemTypeName = "" });
                        while (reader.Read())
                        {
                            itemType.Add(new ItemType { ItemTypeId = reader[1].ToString(), ItemTypeName = reader[0].ToString() });
                        }


                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return itemType;
            });
        }




        



         public async Task<List<ItemType>> GetEffectiveEndDate()
        {
            List<ItemType> itemType = new List<ItemType>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        string sql = "select BEGIN_FISCAL_WEEK,BEGIN_CAL_DATE,END_FISCAL_WEEK,END_CAL_DATE from SNOP_RP_FISCAL_WK_LIST_VW";
                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();
                        itemType.Add(new ItemType { ItemTypeId = "", ItemTypeName = "" });
                        while (reader.Read())
                        {
                            itemType.Add(new ItemType { ItemTypeId = reader["END_CAL_DATE"].ToString(), ItemTypeName = reader["END_FISCAL_WEEK"].ToString() });
                        }


                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return itemType;
            });
        }



        public async Task<List<ItemType>> GetFGA()
        {
            List<ItemType> itemType = new List<ItemType>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        string sql = "SELECT distinct fga_id, fga_id ||'('|| SUBSTR(NAME,1,30)||')' FGA FROM DELL_CM_FP_FHC_FGA_MAP_VW A, MST_ITEM_VW B where a.FGA_ID is not null and a.ssc_id in ('BTS', 'BTP') and a.fga_id = b.item_id(+) order by FGA_ID ";

                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();
                        itemType.Add(new ItemType { ItemTypeId = "Select", ItemTypeName = "Select" });
                        while (reader.Read())
                        {
                            itemType.Add(new ItemType { ItemTypeId = reader[0].ToString(), ItemTypeName = reader[1].ToString() });
                        }


                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return itemType;
            });
        }






        public async Task<List<ItemType>> GetRoutingDropdownvalue(string ItemTypeId, string ItemTypeName)
        {
            List<ItemType> itemType = new List<ItemType>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();


                         string sql = "select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING,TRNSP_LEAD_TIME_ID  from transportation_lead_time_vw where TO_SITE_NAME='" + ItemTypeName + "' AND FROM_SITE_NAME='" +ItemTypeId+ "' AND SYS_ENT_STATE = 'ACTIVE' ORDER BY FROM_SITE_NAME";

                        // var query = schemaName + ".E2OPEN_UPP_EMC_PRODUCT_PKG.P_Get_Item_Attribute";
                        OracleCommand cmd = new OracleCommand(sql, connection);
                        // cmd.CommandType = CommandType.StoredProcedure;



                        OracleDataReader reader = cmd.ExecuteReader();
                        itemType.Add(new ItemType { ItemTypeId = "Select", ItemTypeName = "Select" });
                        while (reader.Read())
                        {
                            itemType.Add(new ItemType { ItemTypeName = reader[0].ToString(),ItemTypeId = reader[1].ToString() });
                        }


                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return itemType;
            });
        }




        public DataTable searchRoutingRules(string ID, string type, string strRegion)
        {
            string productType = null;
            string lobType = "LOB";
            string fpType = "FAMPAR";
            string baseCode = "BASE";
            DataTable dtResult = null;
            try
            {

                if (!String.IsNullOrEmpty(type))
                {

                    dtResult = new DataTable();
                    if (type == "All")
                    {
                        StringBuilder sqlSrting = new StringBuilder();

                        sqlSrting.Append(" select c.ROUTING_ID, c.product_id  ProductFga, decode(c.product_id, pre_product_id, '1','0') FLAG,  c.PRODUCT_ID, c.ITEM, c.type,  c.FULFILLMENT_SITE_ID, ");
                        sqlSrting.Append(" c.MANUFACTURING_SITE_ID, c.PRIMARY_ROUTING,c.ALTERNATE_ROUTING_1, c.ALTERNATE_ROUTING_2,   TO_CHAR(ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR (MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR (MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,MFG_SITE_SPLIT_PERCENT,c.SYS_LAST_MODIFIED_BY, c.SYS_LAST_MODIFIED_DATE  from (  ");
                        sqlSrting.Append("  select a.routing_id, a.PRODUCT_ID,lag(a.product_id,1,0) over (order by a.product_id) as pre_product_id,a.item, lag(a.item,1,0) over(order by a.item) as pre_item, a.type,   ");
                        sqlSrting.Append(" b.fulfillment_site_id, b.manufacturing_site_id,");
                        sqlSrting.Append("  (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                        sqlSrting.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                        sqlSrting.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                        sqlSrting.Append(" b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT,NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from  ");
                        sqlSrting.Append(" routing_rule_header a, routing_rule_lines b,   (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                        if (!string.IsNullOrEmpty(strRegion))
                        {
                            sqlSrting.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                        }
                        sqlSrting.Append(" ) R  where a.routing_id = b.routing_id(+)  and a.type = 'PRODUCT_TREE' AND R.site_name= B.FULFILLMENT_SITE_ID ");
                        sqlSrting.Append("  and a.PRODUCT_ID='ALL_PRODUCT' order by a.product_id, a.item, FULFILLMENT_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST, ROUTING_EFFECTIVE_END_DATE NULLS FIRST ) c");
                        sqlSrting.Append(" union all   ");
                        sqlSrting.Append(" select c.ROUTING_ID,d.NAME ||'('|| c.product_id ||')' ProductFga, decode(c.product_id, pre_product_id, '1','0') FLAG,  c.PRODUCT_ID, c.ITEM, c.type, ");
                        sqlSrting.Append("  c.FULFILLMENT_SITE_ID, c.MANUFACTURING_SITE_ID, c.PRIMARY_ROUTING,c.ALTERNATE_ROUTING_1, c.ALTERNATE_ROUTING_2,TO_CHAR(ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR (MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR (MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,MFG_SITE_SPLIT_PERCENT,c.SYS_LAST_MODIFIED_BY, c.SYS_LAST_MODIFIED_DATE ");
                        sqlSrting.Append(" from ( ");
                        sqlSrting.Append(" select a.routing_id, a.PRODUCT_ID,lag(a.product_id,1,0) over (order by a.product_id) as pre_product_id,a.item, lag(a.item,1,0) over(order by a.item) as pre_item, a.type, ");
                        sqlSrting.Append("  b.fulfillment_site_id, b.manufacturing_site_id,            ");
                        sqlSrting.Append("  (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                        sqlSrting.Append("   (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                        sqlSrting.Append("  (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                        sqlSrting.Append("  b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT,NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from ");
                        sqlSrting.Append("  routing_rule_header a, routing_rule_lines b,  (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                        if (!string.IsNullOrEmpty(strRegion))
                        {
                            sqlSrting.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                        }
                        sqlSrting.Append("  ) R  where a.routing_id = b.routing_id(+)  and a.type = 'PRODUCT_TREE' AND R.site_name= B.FULFILLMENT_SITE_ID ");
                        sqlSrting.Append("  ORDER BY  a.PRODUCT_ID,  FULFILLMENT_SITE_ID, ROUTING_EFFECTIVE_START_DATE  NULLS FIRST , ROUTING_EFFECTIVE_END_DATE NULLS FIRST  ");
                        sqlSrting.Append(" ) c , MST_PRODUCT_MASTER_VW d where c.PRODUCT_ID =d.PRODUCT_ID                  ");
                        sqlSrting.Append("  union all   ");
                        sqlSrting.Append(" select  c.ROUTING_ID,  c.item ||'('||d.NAME||')' ProductFga, decode(c.item, pre_item, '1','0')   ");
                        sqlSrting.Append(" FLAG,c.PRODUCT_ID,c.ITEM, c.type, c.FULFILLMENT_SITE_ID, c.MANUFACTURING_SITE_ID, c.PRIMARY_ROUTING,c.ALTERNATE_ROUTING_1, c.ALTERNATE_ROUTING_2,  TO_CHAR(ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR (MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR (MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,MFG_SITE_SPLIT_PERCENT, ");
                        sqlSrting.Append("   c.SYS_LAST_MODIFIED_BY, c.SYS_LAST_MODIFIED_DATE from (select a.routing_id, a.product_id,lag(a.product_id,1,0) over (order by a.product_id) as pre_product_id,  a.ITEM,  ");
                        sqlSrting.Append("  lag(a.item,1,0) over(order by a.item) as pre_item, a.type, b.fulfillment_site_id, b.manufacturing_site_id,");
                        sqlSrting.Append("    (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                        sqlSrting.Append("      (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                        sqlSrting.Append("    (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                        sqlSrting.Append("  b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT, ");
                        sqlSrting.Append("  NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from routing_rule_header a, routing_rule_lines b,  (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                        
                        if (!string.IsNullOrEmpty(strRegion))
                        {
                            sqlSrting.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                        }
                        sqlSrting.Append("  ) R where a.routing_id = b.routing_id(+)  AND R.site_name= B.FULFILLMENT_SITE_ID ");
                        sqlSrting.Append("  order by ");
                        sqlSrting.Append("  a.item, FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST, ROUTING_EFFECTIVE_END_DATE NULLS FIRST  ) c, MST_ITEM_VW d where  d.Item_ID =c.item order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST");

                         
                        using (oracleConnection = new OracleConnection(connenctionString))
                        {
                            oracleCommand = new OracleCommand(sqlSrting.ToString(), oracleConnection);
                            oracleDataApt = new OracleDataAdapter(oracleCommand);
                            dtResult = new DataTable();
                            oracleDataApt.Fill(dtResult);
                        }
                    }
                    else
                        if (type == "FGA")
                        {
                            using (oracleConnection = new OracleConnection(connenctionString))
                            {
                                StringBuilder sqlSrting = new StringBuilder();

                                if ((string.IsNullOrEmpty(ID)) || ID == "--ALL--")
                                {

                                    sqlSrting.Append(" select  c.ROUTING_ID,  c.item ||'('||d.NAME||')' ProductFga, decode(c.item, pre_item, '1','0')   ");
                                    sqlSrting.Append(" FLAG,c.PRODUCT_ID,c.ITEM, c.type, c.FULFILLMENT_SITE_ID, c.MANUFACTURING_SITE_ID, c.PRIMARY_ROUTING,c.ALTERNATE_ROUTING_1, c.ALTERNATE_ROUTING_2,  TO_CHAR(ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR (MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR (MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,MFG_SITE_SPLIT_PERCENT, ");
                                    sqlSrting.Append("   c.SYS_LAST_MODIFIED_BY, c.SYS_LAST_MODIFIED_DATE from (select a.routing_id, a.product_id,lag(a.product_id,1,0) over (order by a.product_id) as pre_product_id,  a.ITEM,  ");
                                    sqlSrting.Append("  lag(a.item,1,0) over(order by a.item) as pre_item, a.type, b.fulfillment_site_id, b.manufacturing_site_id,");
                                    sqlSrting.Append("    (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                    sqlSrting.Append("      (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                    sqlSrting.Append("    (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                    sqlSrting.Append("  b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT,  ");
                                    sqlSrting.Append("  NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from routing_rule_header a, routing_rule_lines b, (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                    if (!string.IsNullOrEmpty(strRegion))
                                    {
                                        sqlSrting.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                                    }
                                    sqlSrting.Append(" ) R where a.routing_id = b.routing_id(+) AND a.type='FGA_ITEM' AND R.site_name= B.FULFILLMENT_SITE_ID ");
                                    sqlSrting.Append(" order by ");
                                    sqlSrting.Append("  a.item, FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST , ROUTING_EFFECTIVE_END_DATE NULLS FIRST ) c, MST_ITEM_VW d where  d.Item_ID =c.item order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST");

                                }
                                else
                                {

                                    sqlSrting.Append(" select  c.ROUTING_ID,  c.item ||'('||d.NAME||')' ProductFga, decode(c.item, pre_item, '1','0')   ");
                                    sqlSrting.Append(" FLAG,c.PRODUCT_ID,c.ITEM, c.type, c.FULFILLMENT_SITE_ID, c.MANUFACTURING_SITE_ID, c.PRIMARY_ROUTING,c.ALTERNATE_ROUTING_1, c.ALTERNATE_ROUTING_2,  TO_CHAR(ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR (MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR (MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,MFG_SITE_SPLIT_PERCENT, ");
                                    sqlSrting.Append("   c.SYS_LAST_MODIFIED_BY, c.SYS_LAST_MODIFIED_DATE from (select a.routing_id, a.product_id,lag(a.product_id,1,0) over (order by a.product_id) as pre_product_id,  a.ITEM,  ");
                                    sqlSrting.Append("  lag(a.item,1,0) over(order by a.item) as pre_item, a.type, b.fulfillment_site_id, b.manufacturing_site_id,");
                                    sqlSrting.Append("    (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                    sqlSrting.Append("      (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                    sqlSrting.Append("    (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                    sqlSrting.Append("  b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT,  ");
                                    sqlSrting.Append("  NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from routing_rule_header a, routing_rule_lines b, (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                    if (!string.IsNullOrEmpty(strRegion))
                                    {
                                        sqlSrting.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                                    }
                                    sqlSrting.Append("  ) R where a.routing_id = b.routing_id(+) AND a.type='FGA_ITEM'  and a.item='" + ID + "' AND R.site_name= B.FULFILLMENT_SITE_ID ");

                                    sqlSrting.Append(" order by ");
                                    sqlSrting.Append("  a.item, FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST , ROUTING_EFFECTIVE_END_DATE NULLS FIRST ) c, MST_ITEM_VW d where  d.Item_ID =c.item order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST");

                                }
                                OracleDataAdapter oracleDataAdapterFga = new OracleDataAdapter(sqlSrting.ToString(), oracleConnection);
                                oracleDataAdapterFga.Fill(dtResult);
                            }
                        }
                        else
                            if (type == "FHC")
                            {
                                using (oracleConnection = new OracleConnection(connenctionString))
                                {
                                    StringBuilder sqlSrting = new StringBuilder();

                                    if ((string.IsNullOrEmpty(ID)) || ID == "ALL")
                                    {
                                        //all FGA/FHC
                                        sqlSrting.Append(" select  c.ROUTING_ID,  c.item ||'('||d.NAME||')' ProductFga, decode(c.item, pre_item, '1','0')   ");
                                        sqlSrting.Append(" FLAG,c.PRODUCT_ID,c.ITEM, c.type, c.FULFILLMENT_SITE_ID, c.MANUFACTURING_SITE_ID, c.PRIMARY_ROUTING,c.ALTERNATE_ROUTING_1, c.ALTERNATE_ROUTING_2,  TO_CHAR(ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR (MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR (MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,MFG_SITE_SPLIT_PERCENT, ");
                                        sqlSrting.Append("   c.SYS_LAST_MODIFIED_BY, c.SYS_LAST_MODIFIED_DATE from (select a.routing_id, a.product_id,lag(a.product_id,1,0) over (order by a.product_id) as pre_product_id,  a.ITEM,  ");
                                        sqlSrting.Append("  lag(a.item,1,0) over(order by a.item) as pre_item, a.type, b.fulfillment_site_id, b.manufacturing_site_id,");
                                        sqlSrting.Append("  (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                        sqlSrting.Append("  (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                        sqlSrting.Append("  (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                        sqlSrting.Append("  b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT, ");
                                        sqlSrting.Append("  NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from routing_rule_header a, routing_rule_lines b, (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                        if (!string.IsNullOrEmpty(strRegion))
                                        {
                                            sqlSrting.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                                        }
                                        sqlSrting.Append("  ) R where a.routing_id = b.routing_id(+) AND a.type in ('FGA_ITEM','FHC_ITEM') AND R.site_name= B.FULFILLMENT_SITE_ID ");

                                        sqlSrting.Append(" order by ");
                                        sqlSrting.Append("  a.item, FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST, MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST , ROUTING_EFFECTIVE_END_DATE NULLS FIRST  ) c, MST_ITEM_VW d where  d.Item_ID =c.item order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST");

                                    }
                                    else
                                    {
                                        //FHA
                                        sqlSrting.Append(" select  c.ROUTING_ID,  c.item ||'('||d.NAME||')' ProductFga, decode(c.item, pre_item, '1','0')   ");
                                        sqlSrting.Append(" FLAG,c.PRODUCT_ID,c.ITEM, c.type, c.FULFILLMENT_SITE_ID, c.MANUFACTURING_SITE_ID, c.PRIMARY_ROUTING,c.ALTERNATE_ROUTING_1, c.ALTERNATE_ROUTING_2,  TO_CHAR(ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR (MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR (MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,MFG_SITE_SPLIT_PERCENT, ");
                                        sqlSrting.Append("   c.SYS_LAST_MODIFIED_BY, c.SYS_LAST_MODIFIED_DATE from (select a.routing_id, a.product_id,lag(a.product_id,1,0) over (order by a.product_id) as pre_product_id,  a.ITEM,  ");
                                        sqlSrting.Append("  lag(a.item,1,0) over(order by a.item) as pre_item, a.type, b.fulfillment_site_id, b.manufacturing_site_id,");
                                        sqlSrting.Append("    (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                        sqlSrting.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                        sqlSrting.Append("    (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                        sqlSrting.Append("  b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT,  ");
                                        sqlSrting.Append("  NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from routing_rule_header a, routing_rule_lines b, (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                        if (!string.IsNullOrEmpty(strRegion))
                                        {
                                            sqlSrting.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                                        }
                                        sqlSrting.Append("  ) R where a.routing_id = b.routing_id(+) AND a.type='FHC_ITEM'  and a.item='" + ID + "' AND R.site_name= B.FULFILLMENT_SITE_ID ");

                                        sqlSrting.Append(" order by ");
                                        sqlSrting.Append("  a.item, FULFILLMENT_SITE_ID, ROUTING_EFFECTIVE_START_DATE  NULLS FIRST , ROUTING_EFFECTIVE_END_DATE NULLS FIRST ) c, MST_ITEM_VW d where  d.Item_ID =c.item      ");
                                        sqlSrting.Append(" union all   ");
                                        sqlSrting.Append(" select  c.ROUTING_ID,  c.item ||'('||d.NAME||')' ProductFga, decode(c.item, pre_item, '1','0')   ");
                                        sqlSrting.Append(" FLAG,c.PRODUCT_ID,c.ITEM, c.type, c.FULFILLMENT_SITE_ID, c.MANUFACTURING_SITE_ID, c.PRIMARY_ROUTING,c.ALTERNATE_ROUTING_1, c.ALTERNATE_ROUTING_2,  TO_CHAR(ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR (MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR (MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,MFG_SITE_SPLIT_PERCENT, ");
                                        sqlSrting.Append("   c.SYS_LAST_MODIFIED_BY, c.SYS_LAST_MODIFIED_DATE from (select a.routing_id, a.product_id,lag(a.product_id,1,0) over (order by a.product_id) as pre_product_id,  a.ITEM,  ");
                                        sqlSrting.Append("  lag(a.item,1,0) over(order by a.item) as pre_item, a.type, b.fulfillment_site_id, b.manufacturing_site_id,");
                                        sqlSrting.Append("  (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                        sqlSrting.Append("  (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                        sqlSrting.Append("    (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                        sqlSrting.Append("  b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT,  ");
                                        sqlSrting.Append("  NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from routing_rule_header a, routing_rule_lines b, (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                        if (!string.IsNullOrEmpty(strRegion))
                                        {
                                            sqlSrting.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                                        }
                                        sqlSrting.Append("  ) R where a.routing_id = b.routing_id(+) AND a.type='FGA_ITEM'  and a.item in ( SELECT FGA_ID FROM dell_cm_fp_fhc_fga_map_vw WHERE FHC_ID='" + ID + "') AND R.site_name= B.FULFILLMENT_SITE_ID ");

                                        sqlSrting.Append("  order by ");
                                        sqlSrting.Append("  a.item, FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST, MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST , ROUTING_EFFECTIVE_END_DATE NULLS FIRST  ) c, MST_ITEM_VW d where  d.Item_ID =c.item order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST");

                                    }
                                    OracleDataAdapter oracleDataAdapterFga = new OracleDataAdapter(sqlSrting.ToString(), oracleConnection);
                                    oracleDataAdapterFga.Fill(dtResult);
                                }
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(ID))
                                {
                                    Object temp = null;
                                    using (oracleConnection = new OracleConnection(connenctionString))
                                    {

                                        StringBuilder sqlQuery = new StringBuilder("select product_group_type from mst_product_group_vw where product_group_id='");
                                        sqlQuery.Append(ID);
                                        sqlQuery.Append("'");
                                        OracleCommand oracleCommand = new OracleCommand(sqlQuery.ToString(), oracleConnection);
                                        oracleConnection.Open();
                                        temp = oracleCommand.ExecuteScalar();

                                        if (temp == null || temp.ToString() == "" || temp.ToString() == string.Empty)
                                        {
                                            StringBuilder sqlQueryB = new StringBuilder("select PRODUCT_TYPE from MST_PRODUCT_GROUP_DETAIL_VW where product_id='");
                                            sqlQueryB.Append(ID);
                                            sqlQueryB.Append("'");
                                            OracleCommand oracleCommandB = new OracleCommand(sqlQueryB.ToString(), oracleConnection);
                                            //oracleConnection.Open();
                                            temp = oracleCommandB.ExecuteScalar();
                                        }

                                        productType = temp.ToString();

                                        if (productType.Equals(lobType))
                                        {
                                            //PRODUCT_TREE Query   (LOB, FAMPAR) 
                                            //over
                                            DataTable dtLobFampar = new DataTable();
                                            StringBuilder sqlSelectQuery = new StringBuilder("  ");
                                            sqlSelectQuery.Append(" select  d.ROUTING_ID, e.NAME ||'('|| d.product_id ||')' ProductFga, decode( d.PRODUCT_ID, pre_PRODUCT_ID, '1','0') FLAG,  d.PRODUCT_ID,  d.ITEM,   d.type,  d.FULFILLMENT_SITE_ID,  ");
                                            sqlSelectQuery.Append("  d.MANUFACTURING_SITE_ID,  d.PRIMARY_ROUTING,  d.ALTERNATE_ROUTING_1,  d.ALTERNATE_ROUTING_2, TO_CHAR(d.ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE, TO_CHAR(d.ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR(d.MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR(d.MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,d.MFG_SITE_SPLIT_PERCENT,d.SYS_LAST_MODIFIED_BY,  d.SYS_LAST_MODIFIED_DATE from(  ");
                                            sqlSelectQuery.Append(" select a.ROUTING_ID,a.PRODUCT_ID || ITEM ProductFga,  a.PRODUCT_ID,lag(a.product_id,1,0) over (order by a.product_id) as  ");
                                            sqlSelectQuery.Append("   pre_product_id, a.ITEM, a.type, b.FULFILLMENT_SITE_ID, b.MANUFACTURING_SITE_ID, ");
                                            sqlSelectQuery.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                            sqlSelectQuery.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                            sqlSelectQuery.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                            sqlSelectQuery.Append("  b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT,  ");
                                            sqlSelectQuery.Append("NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from routing_rule_header_vw a,   ( ");
                                            sqlSelectQuery.Append("   select product_id, 'PRODUCT_TREE' type from (select product_id, product_type from ( select product_type, product_group_type, product_group_id, ");
                                            //sqlSelectQuery.Append("  decode(product_id, product_group_id, null, product_id) product_id  from mst_product_group_detail_vw a where product_type not in ('BASE','FAMILY'))    ");
                                            sqlSelectQuery.Append("  decode(product_id, product_group_id, null, product_id) product_id  from mst_product_group_detail_vw a where product_type in ('LOB','FAMPAR','BASE','FAMILY'))    ");
                                            sqlSelectQuery.Append(" connect by NOCYCLE prior product_id = product_group_id  start with product_id =  '" + ID + "' ) ) c,   routing_rule_lines_vw b, (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                            if (!string.IsNullOrEmpty(strRegion))
                                            {
                                                sqlSelectQuery.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                                            }
                                            sqlSelectQuery.Append(" ) R where a.product_id = c.product_id and a.type = c.type    and a.routing_id = b.routing_id(+) AND R.site_name= B.FULFILLMENT_SITE_ID ");

                                            sqlSelectQuery.Append(" ORDER BY a.product_id, FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST , ROUTING_EFFECTIVE_END_DATE NULLS FIRST ) d,  MST_PRODUCT_MASTER_VW e ");
                                            sqlSelectQuery.Append("  where  d.PRODUCT_ID =e.PRODUCT_ID order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST");

                                            OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(sqlSelectQuery.ToString(), oracleConnection);
                                            oracleDataAdapter.Fill(dtResult);

                                            //FHC
                                            StringBuilder sqlSelectFHCQuery = new StringBuilder(" select d.ROUTING_ID, d.item ||'('|| e.NAME ||')' ProductFga, decode(d.PRODUCT_ID, pre_ITEM, '1','0') FLAG, d.PRODUCT_ID, d.ITEM, d.type, d.FULFILLMENT_SITE_ID, d.MANUFACTURING_SITE_ID, ");
                                            sqlSelectFHCQuery.Append("  d.PRIMARY_ROUTING, d.ALTERNATE_ROUTING_1, d.ALTERNATE_ROUTING_2, TO_CHAR(d.ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(d.ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR(d.MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR(d.MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,d.MFG_SITE_SPLIT_PERCENT, d.SYS_LAST_MODIFIED_DATE from(  ");
                                            sqlSelectFHCQuery.Append(" select a.ROUTING_ID,a.PRODUCT_ID || ITEM ProductFga, a.PRODUCT_ID ,lag(a.ITEM,1,0) over (order by a.ITEM) as pre_ITEM,  a.ITEM, a.type, ");
                                            sqlSelectFHCQuery.Append(" b.FULFILLMENT_SITE_ID, b.MANUFACTURING_SITE_ID, ");
                                            sqlSelectFHCQuery.Append("    (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                            sqlSelectFHCQuery.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                            sqlSelectFHCQuery.Append("    (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                            sqlSelectFHCQuery.Append(" b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT, ");
                                            sqlSelectFHCQuery.Append(" NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE  from routing_rule_header_vw a, (select fhc_id , 'FHC_ITEM' type from dell_cm_fp_fhc_fga_map_vw where fmly_parnt_code in ( ");
                                            sqlSelectFHCQuery.Append(" select product_id from ( select product_id, product_type from ( select product_type, product_group_type, product_group_id,  ");
                                            sqlSelectFHCQuery.Append(" decode(product_id, product_group_id, null, product_id) product_id from mst_product_group_detail_vw a where ");
                                            sqlSelectFHCQuery.Append(" product_type not in ('BASE','FAMILY')) connect by NOCYCLE prior product_id = product_group_id  start with product_id = '" + ID + "' ) where product_type = 'FAMPAR') ) c,  ");
                                            if (!string.IsNullOrEmpty(strRegion))
                                            {
                                                sqlSelectFHCQuery.Append(" routing_rule_lines_vw b , (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                                sqlSelectFHCQuery.Append(" AND A.REGION_CODE = '" + strRegion + "' ");
                                                sqlSelectFHCQuery.Append(" ) R  where a.item = c.fhc_id   and a.type = c.type    and a.routing_id = b.routing_id(+)  AND R.site_name= B.FULFILLMENT_SITE_ID ");
                                            }
                                            else
                                            {
                                                sqlSelectFHCQuery.Append(" routing_rule_lines_vw b ");
                                                sqlSelectFHCQuery.Append("  where a.item = c.fhc_id   and a.type = c.type    and a.routing_id = b.routing_id(+) ");
                                            }

                                            sqlSelectFHCQuery.Append(" ORDER BY a.item, FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST, ROUTING_EFFECTIVE_END_DATE NULLS FIRST  ) d, MST_ITEM_VW e  where   e.Item_ID =d.item order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST");
                                            DataTable dtFHC = new DataTable();
                                            OracleDataAdapter oracleDataAdapterFHC = new OracleDataAdapter(sqlSelectFHCQuery.ToString(), oracleConnection);
                                            oracleDataAdapterFHC.Fill(dtFHC);
                                            dtResult.Merge(dtFHC);

                                            //FGA Query
                                            StringBuilder sqlSelectFGAQuery = new StringBuilder(" select d.ROUTING_ID, d.item ||'('|| e.NAME ||')' ProductFga, decode(d.PRODUCT_ID, pre_ITEM, '1','0') FLAG, d.PRODUCT_ID, d.ITEM, d.type, d.FULFILLMENT_SITE_ID, d.MANUFACTURING_SITE_ID, ");
                                            sqlSelectFGAQuery.Append("  d.PRIMARY_ROUTING, d.ALTERNATE_ROUTING_1, d.ALTERNATE_ROUTING_2, TO_CHAR(d.ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(d.ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR(d.MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR(d.MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,d.MFG_SITE_SPLIT_PERCENT,d.SYS_LAST_MODIFIED_DATE from(  ");
                                            sqlSelectFGAQuery.Append(" select a.ROUTING_ID,a.PRODUCT_ID || ITEM ProductFga, a.PRODUCT_ID ,lag(a.ITEM,1,0) over (order by a.ITEM) as pre_ITEM,  a.ITEM, a.type, ");
                                            sqlSelectFGAQuery.Append(" b.FULFILLMENT_SITE_ID, b.MANUFACTURING_SITE_ID, ");
                                            sqlSelectFGAQuery.Append("    (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                            sqlSelectFGAQuery.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                            sqlSelectFGAQuery.Append("    (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                            sqlSelectFGAQuery.Append(" b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT, ");
                                            sqlSelectFGAQuery.Append(" NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE  from routing_rule_header_vw a, (select fga_id , 'FGA_ITEM' type from dell_cm_fp_fhc_fga_map_vw where fmly_parnt_code in ( ");
                                            sqlSelectFGAQuery.Append(" select product_id from ( select product_id, product_type from ( select product_type, product_group_type, product_group_id,  ");
                                            sqlSelectFGAQuery.Append(" decode(product_id, product_group_id, null, product_id) product_id from mst_product_group_detail_vw a where ");
                                            sqlSelectFGAQuery.Append(" product_type not in ('BASE','FAMILY')) connect by NOCYCLE prior product_id = product_group_id  start with product_id = '" + ID + "' ) where product_type = 'FAMPAR') ) c,  ");
                                            if (!string.IsNullOrEmpty(strRegion))
                                            {
                                                sqlSelectFGAQuery.Append(" routing_rule_lines_vw b , (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                                sqlSelectFGAQuery.Append(" AND A.REGION_CODE = '" + strRegion + "' ");
                                                sqlSelectFGAQuery.Append("  ) R  where a.item = c.fga_id   and a.type = c.type    and a.routing_id = b.routing_id(+)  AND R.site_name= B.FULFILLMENT_SITE_ID ");
                                            }
                                            else
                                            {
                                                sqlSelectFGAQuery.Append(" routing_rule_lines_vw b ");
                                                sqlSelectFGAQuery.Append("  where a.item = c.fga_id   and a.type = c.type    and a.routing_id = b.routing_id(+) ");
                                            }
                                            sqlSelectFGAQuery.Append("  ORDER BY a.item, FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST , ROUTING_EFFECTIVE_END_DATE NULLS FIRST ) d, MST_ITEM_VW e  where   e.Item_ID =d.item order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST");

                                            DataTable dtFGA = new DataTable();
                                            OracleDataAdapter oracleDataAdapterFga = new OracleDataAdapter(sqlSelectFGAQuery.ToString(), oracleConnection);
                                            oracleDataAdapterFga.Fill(dtFGA);
                                            dtResult.Merge(dtFGA);


                                        }
                                        else if (productType.Equals(fpType))
                                        {
                                            //Family Parent
                                            StringBuilder sqlQuery1 = new StringBuilder();
                                            //sqlQuery1.Append(" select c.ROUTING_ID, d.NAME ||'('|| c.product_id ||')' ProductFga, decode(c.item, pre_item, '1','0') FLAG, c.PRODUCT_ID, c.ITEM, c.type, c.FULFILLMENT_SITE_ID, c.MANUFACTURING_SITE_ID,  ");
                                            //sqlQuery1.Append(" c.PRIMARY_ROUTING,c.ALTERNATE_ROUTING_1, c.ALTERNATE_ROUTING_2, TO_CHAR(c.ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(c.ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR(c.MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR(c.MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,c.MFG_SITE_SPLIT_PERCENT,c.SYS_LAST_MODIFIED_BY, c.SYS_LAST_MODIFIED_DATE from (  ");
                                            //sqlQuery1.Append(" select a.ROUTING_ID,a.PRODUCT_ID || ITEM ProductFga, a.PRODUCT_ID, a.ITEM,lag(a.item,1,0) over(order by a.item) as pre_item,  a.type,  ");
                                            //sqlQuery1.Append(" b.FULFILLMENT_SITE_ID, b.MANUFACTURING_SITE_ID, ");
                                            //sqlQuery1.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                            //sqlQuery1.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                            //sqlQuery1.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                            //sqlQuery1.Append(" b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT,  ");
                                            //sqlQuery1.Append(" NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from routing_rule_header_vw a,  routing_rule_lines_vw b, (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                            //if (!string.IsNullOrEmpty(strRegion))
                                            //{
                                            //    sqlQuery1.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                                            //}
                                            //sqlQuery1.Append(" ) R  where a.routing_id= b.routing_id(+) AND R.site_name= B.FULFILLMENT_SITE_ID ");

                                            //sqlQuery1.Append(" and  ");
                                            //sqlQuery1.Append(" a.PRODUCT_ID='" + ID + "' ORDER BY a.product_id,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST, ROUTING_EFFECTIVE_END_DATE NULLS FIRST  ) c, MST_PRODUCT_MASTER_VW d where  c.PRODUCT_ID =d.PRODUCT_ID order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST");
                                            
                                            //Added by Kanishka for Config MPP changes.
                                            sqlQuery1.Append(" select  d.ROUTING_ID, e.NAME ||'('|| d.product_id ||')' ProductFga, decode( d.PRODUCT_ID, pre_PRODUCT_ID, '1','0') FLAG,  d.PRODUCT_ID,  d.ITEM,   d.type,  d.FULFILLMENT_SITE_ID,  ");
                                            sqlQuery1.Append("  d.MANUFACTURING_SITE_ID,  d.PRIMARY_ROUTING,  d.ALTERNATE_ROUTING_1,  d.ALTERNATE_ROUTING_2, TO_CHAR(d.ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE, TO_CHAR(d.ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR(d.MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR(d.MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,d.MFG_SITE_SPLIT_PERCENT,d.SYS_LAST_MODIFIED_BY,  d.SYS_LAST_MODIFIED_DATE from(  ");
                                            sqlQuery1.Append(" select a.ROUTING_ID,a.PRODUCT_ID || ITEM ProductFga,  a.PRODUCT_ID,lag(a.product_id,1,0) over (order by a.product_id) as  ");
                                            sqlQuery1.Append("   pre_product_id, a.ITEM, a.type, b.FULFILLMENT_SITE_ID, b.MANUFACTURING_SITE_ID, ");
                                            sqlQuery1.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                            sqlQuery1.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                            sqlQuery1.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                            sqlQuery1.Append("  b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT,  ");
                                            sqlQuery1.Append("NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from routing_rule_header_vw a,   ( ");
                                            sqlQuery1.Append("   select product_id, 'PRODUCT_TREE' type from (select product_id, product_type from ( select product_type, product_group_type, product_group_id, ");
                                            //sqlSelectQuery.Append("  decode(product_id, product_group_id, null, product_id) product_id  from mst_product_group_detail_vw a where product_type not in ('BASE','FAMILY'))    ");
                                            sqlQuery1.Append("  decode(product_id, product_group_id, null, product_id) product_id  from mst_product_group_detail_vw a where product_type in ('LOB','FAMPAR','BASE','FAMILY'))    ");
                                            sqlQuery1.Append(" connect by NOCYCLE prior product_id = product_group_id  start with product_id =  '" + ID + "' ) ) c,   routing_rule_lines_vw b, (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                            if (!string.IsNullOrEmpty(strRegion))
                                            {
                                                sqlQuery1.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                                            }
                                            sqlQuery1.Append(" ) R where a.product_id = c.product_id and a.type = c.type    and a.routing_id = b.routing_id(+) AND R.site_name= B.FULFILLMENT_SITE_ID ");

                                            sqlQuery1.Append(" ORDER BY a.product_id, FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST , ROUTING_EFFECTIVE_END_DATE NULLS FIRST ) d,  MST_PRODUCT_MASTER_VW e ");
                                            sqlQuery1.Append("  where  d.PRODUCT_ID =e.PRODUCT_ID order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST");



                                            DataTable dtFampar = new DataTable();
                                            OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(sqlQuery1.ToString(), oracleConnection);
                                            oracleDataAdapter.Fill(dtResult);

                                            //FHC
                                            StringBuilder sqlFHC = new StringBuilder();
                                            sqlFHC.Append(" select c.ROUTING_ID,c.item ||'('|| d.NAME ||')' ProductFga, decode(c.item, pre_item, '1','0') FLAG, c.PRODUCT_ID, c.ITEM, c.type, c.FULFILLMENT_SITE_ID, c.MANUFACTURING_SITE_ID,  ");
                                            sqlFHC.Append(" c.PRIMARY_ROUTING,c.ALTERNATE_ROUTING_1, c.ALTERNATE_ROUTING_2, TO_CHAR(c.ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(c.ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR(c.MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR(c.MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,c.MFG_SITE_SPLIT_PERCENT,c.SYS_LAST_MODIFIED_BY, c.SYS_LAST_MODIFIED_DATE from ( select a.ROUTING_ID, ");
                                            sqlFHC.Append(" a.PRODUCT_ID || ITEM ProductFga,a.PRODUCT_ID,  a.ITEM,lag(a.item,1,0) over(order by a.item)  as pre_item, a.type, b.FULFILLMENT_SITE_ID, ");
                                            sqlFHC.Append(" b.MANUFACTURING_SITE_ID, ");
                                            sqlFHC.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                            sqlFHC.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                            sqlFHC.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                            sqlFHC.Append(" b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT, NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from   ");
                                            sqlFHC.Append(" routing_rule_header_vw a,  routing_rule_lines_vw b, (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                            if (!string.IsNullOrEmpty(strRegion))
                                            {
                                                sqlFHC.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                                            }
                                            sqlFHC.Append(" ) R   where a.routing_id= b.routing_id(+) AND R.site_name= B.FULFILLMENT_SITE_ID ");
                                             
                                            sqlFHC.Append("  and  a.item in ");
                                            sqlFHC.Append(" ( select fhc_id from  DELL_CM_FP_FHC_FGA_MAP_VW  where  fmly_parnt_code = '" + ID + "' ) ORDER BY a.item, FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST , ROUTING_EFFECTIVE_END_DATE NULLS FIRST ) c, MST_ITEM_VW d where  d.Item_ID =c.item order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST ");

                                            DataTable dtFHC = new DataTable();
                                            OracleDataAdapter oracleDataAdapterFHC = new OracleDataAdapter(sqlFHC.ToString(), oracleConnection);
                                            oracleDataAdapterFHC.Fill(dtFHC);
                                            dtResult.Merge(dtFHC);


                                            StringBuilder sqlQuery2 = new StringBuilder();
                                            sqlQuery2.Append(" select c.ROUTING_ID,c.item ||'('|| d.NAME ||')' ProductFga, decode(c.item, pre_item, '1','0') FLAG, c.PRODUCT_ID, c.ITEM, c.type, c.FULFILLMENT_SITE_ID, c.MANUFACTURING_SITE_ID,  ");
                                            sqlQuery2.Append(" c.PRIMARY_ROUTING,c.ALTERNATE_ROUTING_1, c.ALTERNATE_ROUTING_2, TO_CHAR(c.ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(c.ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR(c.MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR(c.MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,c.MFG_SITE_SPLIT_PERCENT,c.SYS_LAST_MODIFIED_BY, c.SYS_LAST_MODIFIED_DATE from ( select a.ROUTING_ID, ");
                                            sqlQuery2.Append(" a.PRODUCT_ID || ITEM ProductFga,a.PRODUCT_ID,  a.ITEM,lag(a.item,1,0) over(order by a.item)  as pre_item, a.type, b.FULFILLMENT_SITE_ID, ");
                                            sqlQuery2.Append(" b.MANUFACTURING_SITE_ID, ");
                                            sqlQuery2.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                            sqlQuery2.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                            sqlQuery2.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                            sqlQuery2.Append(" b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT, NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from   ");
                                            sqlQuery2.Append(" routing_rule_header_vw a,  routing_rule_lines_vw b, (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                            if (!string.IsNullOrEmpty(strRegion))
                                            {
                                                sqlQuery2.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                                            }
                                            sqlQuery2.Append(" ) R   where a.routing_id= b.routing_id(+) AND R.site_name= B.FULFILLMENT_SITE_ID ");

                                            sqlQuery2.Append("  and  a.item in ");
                                            sqlQuery2.Append(" ( select fga_id from  DELL_CM_FP_FHC_FGA_MAP_VW  where  fmly_parnt_code = '" + ID + "' ) ORDER BY a.item, FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST , ROUTING_EFFECTIVE_END_DATE NULLS FIRST ) c, MST_ITEM_VW d where  d.Item_ID =c.item order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST ");

                                            DataTable dtFGA = new DataTable();
                                            OracleDataAdapter oracleDataAdapterFga = new OracleDataAdapter(sqlQuery2.ToString(), oracleConnection);
                                            oracleDataAdapterFga.Fill(dtFGA);
                                            dtResult.Merge(dtFGA);
                                        }

                                        else if (productType.Equals(baseCode))
                                        { 
                                            //BASE CODES.                                            
                                            //Added by Kanishka for Config MPP changes.

                                            StringBuilder sqlQueryBaseCode = new StringBuilder();
                                            sqlQueryBaseCode.Append(" select c.ROUTING_ID, d.NAME ||'('|| c.product_id ||')' ProductFga, decode(c.item, pre_item, '1','0') FLAG, c.PRODUCT_ID, c.ITEM, c.type, c.FULFILLMENT_SITE_ID, c.MANUFACTURING_SITE_ID,  ");
                                            sqlQueryBaseCode.Append(" c.PRIMARY_ROUTING,c.ALTERNATE_ROUTING_1, c.ALTERNATE_ROUTING_2, TO_CHAR(c.ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(c.ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR(c.MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR(c.MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,c.MFG_SITE_SPLIT_PERCENT,c.SYS_LAST_MODIFIED_BY, c.SYS_LAST_MODIFIED_DATE from (  ");
                                            sqlQueryBaseCode.Append(" select a.ROUTING_ID,a.PRODUCT_ID || ITEM ProductFga, a.PRODUCT_ID, a.ITEM,lag(a.item,1,0) over(order by a.item) as pre_item,  a.type,  ");
                                            sqlQueryBaseCode.Append(" b.FULFILLMENT_SITE_ID, b.MANUFACTURING_SITE_ID, ");
                                            sqlQueryBaseCode.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                            sqlQueryBaseCode.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                            sqlQueryBaseCode.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                            sqlQueryBaseCode.Append(" b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT,  ");
                                            sqlQueryBaseCode.Append(" NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from routing_rule_header_vw a,  routing_rule_lines_vw b, (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                            if (!string.IsNullOrEmpty(strRegion))
                                            {
                                                sqlQueryBaseCode.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                                            }
                                            sqlQueryBaseCode.Append(" ) R  where a.routing_id= b.routing_id(+) AND R.site_name= B.FULFILLMENT_SITE_ID ");

                                            sqlQueryBaseCode.Append(" and  ");
                                            sqlQueryBaseCode.Append(" a.PRODUCT_ID='" + ID + "' ORDER BY a.product_id,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST, ROUTING_EFFECTIVE_END_DATE NULLS FIRST  ) c, MST_PRODUCT_MASTER_VW d where  c.PRODUCT_ID =d.PRODUCT_ID order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST");

                                            
                                            DataTable dtFampar = new DataTable();
                                            OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(sqlQueryBaseCode.ToString(), oracleConnection);
                                            oracleDataAdapter.Fill(dtResult);

                                            //FHC
                                            //Analyze and remove the following query if not required.

                                            //StringBuilder sqlFHCBase = new StringBuilder();
                                            //sqlFHCBase.Append(" select c.ROUTING_ID,c.item ||'('|| d.NAME ||')' ProductFga, decode(c.item, pre_item, '1','0') FLAG, c.PRODUCT_ID, c.ITEM, c.type, c.FULFILLMENT_SITE_ID, c.MANUFACTURING_SITE_ID,  ");
                                            //sqlFHCBase.Append(" c.PRIMARY_ROUTING,c.ALTERNATE_ROUTING_1, c.ALTERNATE_ROUTING_2, TO_CHAR(c.ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(c.ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR(c.MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR(c.MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,c.MFG_SITE_SPLIT_PERCENT,c.SYS_LAST_MODIFIED_BY, c.SYS_LAST_MODIFIED_DATE from ( select a.ROUTING_ID, ");
                                            //sqlFHCBase.Append(" a.PRODUCT_ID || ITEM ProductFga,a.PRODUCT_ID,  a.ITEM,lag(a.item,1,0) over(order by a.item)  as pre_item, a.type, b.FULFILLMENT_SITE_ID, ");
                                            //sqlFHCBase.Append(" b.MANUFACTURING_SITE_ID, ");
                                            //sqlFHCBase.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                            //sqlFHCBase.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                            //sqlFHCBase.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                            //sqlFHCBase.Append(" b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT, NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from   ");
                                            //sqlFHCBase.Append(" routing_rule_header_vw a,  routing_rule_lines_vw b, (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                            //if (!string.IsNullOrEmpty(strRegion))
                                            //{
                                            //    sqlFHCBase.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                                            //}
                                            //sqlFHCBase.Append(" ) R   where a.routing_id= b.routing_id(+) AND R.site_name= B.FULFILLMENT_SITE_ID ");

                                            //sqlFHCBase.Append("  and  a.item in ");
                                            //sqlFHCBase.Append(" ( select fhc_id from  DELL_CM_FP_FHC_FGA_MAP_VW  where  fmly_parnt_code = '" + ID + "' ) ORDER BY a.item, FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST , ROUTING_EFFECTIVE_END_DATE NULLS FIRST ) c, MST_ITEM_VW d where  d.Item_ID =c.item order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST ");

                                            //DataTable dtFHC = new DataTable();
                                            //OracleDataAdapter oracleDataAdapterFHC = new OracleDataAdapter(sqlFHCBase.ToString(), oracleConnection);
                                            //oracleDataAdapterFHC.Fill(dtFHC);
                                            //dtResult.Merge(dtFHC);


                                            //StringBuilder sqlQuery2Base = new StringBuilder();
                                            //sqlQuery2Base.Append(" select c.ROUTING_ID,c.item ||'('|| d.NAME ||')' ProductFga, decode(c.item, pre_item, '1','0') FLAG, c.PRODUCT_ID, c.ITEM, c.type, c.FULFILLMENT_SITE_ID, c.MANUFACTURING_SITE_ID,  ");
                                            //sqlQuery2Base.Append(" c.PRIMARY_ROUTING,c.ALTERNATE_ROUTING_1, c.ALTERNATE_ROUTING_2, TO_CHAR(c.ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(c.ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR(c.MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR(c.MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,c.MFG_SITE_SPLIT_PERCENT,c.SYS_LAST_MODIFIED_BY, c.SYS_LAST_MODIFIED_DATE from ( select a.ROUTING_ID, ");
                                            //sqlQuery2Base.Append(" a.PRODUCT_ID || ITEM ProductFga,a.PRODUCT_ID,  a.ITEM,lag(a.item,1,0) over(order by a.item)  as pre_item, a.type, b.FULFILLMENT_SITE_ID, ");
                                            //sqlQuery2Base.Append(" b.MANUFACTURING_SITE_ID, ");
                                            //sqlQuery2Base.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS  from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.PRIMARY_ROUTING) PRIMARY_ROUTING,");
                                            //sqlQuery2Base.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1,");
                                            //sqlQuery2Base.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=B.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2,");
                                            //sqlQuery2Base.Append(" b.ROUTING_EFFECTIVE_START_DATE, b.ROUTING_EFFECTIVE_END_DATE,a.MFG_EFFECTIVE_START_DATE,a.MFG_EFFECTIVE_END_DATE,b.MFG_SITE_SPLIT_PERCENT, NVL(b.SYS_LAST_MODIFIED_BY, a.SYS_LAST_MODIFIED_BY) SYS_LAST_MODIFIED_BY, NVL(b.SYS_LAST_MODIFIED_DATE, a.SYS_LAST_MODIFIED_DATE) SYS_LAST_MODIFIED_DATE from   ");
                                            //sqlQuery2Base.Append(" routing_rule_header_vw a,  routing_rule_lines_vw b, (SELECT DISTINCT C.SITE_NAME FROM FULFILLMENT_REGION_VW A,  FULFILLMENT_SUB_REGION_VW B,  FF_SUB_REGION_SITE_XREF_VW C  WHERE A.FF_TYPE_NAME = B.FF_TYPE_NAME AND A.REGION_CODE = B.REGION_CODE AND B.SUB_FF_REGION_CODE = C.FF_SUB_REGION_CODE ");
                                            //if (!string.IsNullOrEmpty(strRegion))
                                            //{
                                            //    sqlQuery2Base.Append(" and A.REGION_CODE = '" + strRegion + "' ");
                                            //}
                                            //sqlQuery2Base.Append(" ) R   where a.routing_id= b.routing_id(+) AND R.site_name= B.FULFILLMENT_SITE_ID ");

                                            //sqlQuery2Base.Append("  and  a.item in ");
                                            //sqlQuery2Base.Append(" ( select fga_id from  DELL_CM_FP_FHC_FGA_MAP_VW  where  fmly_parnt_code = '" + ID + "' ) ORDER BY a.item, FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE  NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID,ROUTING_EFFECTIVE_START_DATE  NULLS FIRST , ROUTING_EFFECTIVE_END_DATE NULLS FIRST ) c, MST_ITEM_VW d where  d.Item_ID =c.item order by productFGA,FULFILLMENT_SITE_ID,MFG_EFFECTIVE_START_DATE NULLS FIRST,MFG_EFFECTIVE_END_DATE NULLS FIRST,MANUFACTURING_SITE_ID, ROUTING_EFFECTIVE_START_DATE NULLS FIRST,ROUTING_EFFECTIVE_END_DATE NULLS FIRST ");

                                            //DataTable dtFGA = new DataTable();
                                            //OracleDataAdapter oracleDataAdapterFga = new OracleDataAdapter(sqlQuery2Base.ToString(), oracleConnection);
                                            //oracleDataAdapterFga.Fill(dtFGA);
                                            //dtResult.Merge(dtFGA);

                                        }

                                    }

                                }

                            }

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                if (oracleConnection != null)
                {
                    oracleConnection.Close();
                    oracleConnection.Dispose();
                }
            }
            return dtResult;

        }




 

      public DataTable GetRoutingRuladdupdateGrid(string strRoutingId, string strFulfillmentSiteID)
        {
            string strSql;
            DataTable dtRoutingDetails = null;
            try
            {
                StringBuilder sbQuery = new StringBuilder();
                using (oracleConnection = new OracleConnection(connenctionString))
                {
                    sbQuery.Append("Select H.ROUTING_ID,H.PRODUCT_ID,TO_CHAR(H.MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY')MFG_EFFECTIVE_START_DATE,TO_CHAR(H.MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY')MFG_EFFECTIVE_END_DATE,L.MFG_SITE_SPLIT_PERCENT,H.ITEM, H.TYPE, L.ID, L.FULFILLMENT_SITE_ID, L.MANUFACTURING_SITE_ID,L.PRIMARY_ROUTING, L.ALTERNATE_ROUTING_1,L.ALTERNATE_ROUTING_2, ");
                    sbQuery.Append("(select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where TRNSP_LEAD_TIME_ID=L.PRIMARY_ROUTING AND SYS_ENT_STATE = 'ACTIVE') PRIMARY_ROUTING_DESC, ");
                    sbQuery.Append("(select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where TRNSP_LEAD_TIME_ID=L.ALTERNATE_ROUTING_1 AND SYS_ENT_STATE = 'ACTIVE') ALTERNATE_ROUTING_1_DESC, ");
                    sbQuery.Append("(select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where TRNSP_LEAD_TIME_ID=L.ALTERNATE_ROUTING_2 AND SYS_ENT_STATE = 'ACTIVE') ALTERNATE_ROUTING_2_DESC, ");
                    sbQuery.Append("TO_CHAR(L.ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(L.ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE ");
                    sbQuery.Append(" from ROUTING_RULE_HEADER H, ROUTING_RULE_LINES L where H.ROUTING_ID=L.ROUTING_ID and H.ROUTING_ID='" + strRoutingId + "' and L.FULFILLMENT_SITE_ID ='" + strFulfillmentSiteID + "' ORDER BY  L.FULFILLMENT_SITE_ID, L.ROUTING_EFFECTIVE_START_DATE NULLS FIRST, L.ROUTING_EFFECTIVE_END_DATE NULLS FIRST");
                    oracleCommand = new OracleCommand(sbQuery.ToString(), oracleConnection);
                    oracleDataApt = new OracleDataAdapter(oracleCommand);
                    dtRoutingDetails = new DataTable();
                    oracleDataApt.Fill(dtRoutingDetails);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (oracleConnection != null)
                {
                    oracleConnection.Close();
                    oracleConnection.Dispose();
                }
            }
            return dtRoutingDetails;

        }



       // bool DeleteRoutingRules(string strRoutingId, string strFulfillmentSiteID);

        public bool DeleteRoutingRules(List<ItemType> sitesvalue)
        {
            int delRow;
            int intInsertRow;
            string sqlDelete;
            string sqlDeleteLines;
            string sqlLinesCount;
            // string strAudRRLDelete = null;
            //string strAudRRHDelete = null;
            OracleTransaction transaction = null;
            try
            {

                for (int i = 0; i < sitesvalue.Count; i++)
                {

                    using (oracleConnection = new OracleConnection(connenctionString))
                    {
                        oracleConnection.Open();
                        transaction = oracleConnection.BeginTransaction();
                        sqlDeleteLines = "Delete from ROUTING_RULE_LINES where ROUTING_ID='" + sitesvalue[i].ItemTypeId + "' and FULFILLMENT_SITE_ID = '" + sitesvalue[i].ItemTypeName + "'";
                        //Audit Insert
                        StringBuilder sbAudRRLDelete = new StringBuilder();
                        sbAudRRLDelete.Append(" INSERT  INTO AUD_ROUTING_RULE_LINES ( TRANSACTION_ID ,  TRANSACTION_ACTION , ROUTING_ID_OV ,   FULFILLMENT_SITE_ID_OV ,   MANUFACTURING_SITE_ID_OV ,   PRIMARY_ROUTING_OV ,");
                        sbAudRRLDelete.Append("  ALTERNATE_ROUTING_1_OV ,   ALTERNATE_ROUTING_2_OV,   EFFECTIVE_START_DATE_OV ,   EFFECTIVE_END_DATE_OV,  SYS_LAST_MODIFIED_BY_OV, SYS_LAST_MODIFIED_DATE_OV   )");
                        sbAudRRLDelete.Append(" SELECT SEQ_TRANSACTION_ID.NEXTVAL ,'DELETE', ROUTING_ID,FULFILLMENT_SITE_ID,MANUFACTURING_SITE_ID,PRIMARY_ROUTING, ALTERNATE_ROUTING_1, ALTERNATE_ROUTING_2,");
                        sbAudRRLDelete.Append("  ROUTING_EFFECTIVE_START_DATE,ROUTING_EFFECTIVE_END_DATE,SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE from ROUTING_RULE_LINES ");
                        sbAudRRLDelete.Append("where ROUTING_ID='" + sitesvalue[i].ItemTypeId + "'");

                        using (OracleCommand cmdAudRRLInsert = new OracleCommand(sbAudRRLDelete.ToString(), oracleConnection))
                        {
                            intInsertRow = (int)cmdAudRRLInsert.ExecuteNonQuery();
                        }

                        //Actual data Delete
                        using (OracleCommand cmdDelete = new OracleCommand(sqlDeleteLines, oracleConnection))
                        {
                            delRow = (int)cmdDelete.ExecuteNonQuery();
                        }


                        //Routing Header starts here.
                        sqlLinesCount = "Select count(*) COUNT from ROUTING_RULE_LINES where ROUTING_ID='" + sitesvalue[i].ItemTypeId + "'";

                        using (OracleCommand cmdCount = new OracleCommand(sqlLinesCount, oracleConnection))
                        {
                            OracleDataReader oracleDataReader = cmdCount.ExecuteReader(CommandBehavior.CloseConnection);

                            while (oracleDataReader.Read())
                            {
                                sqlLinesCount = oracleDataReader["COUNT"].ToString();
                            }
                        }

                        int count = Convert.ToInt32(sqlLinesCount);

                        if (count == 0)
                        {
                            StringBuilder sbAudRRHDelete = new StringBuilder();
                            sbAudRRHDelete.Append("INSERT  INTO AUD_ROUTING_RULE_HEADER ( TRANSACTION_ID ,  TRANSACTION_ACTION , ROUTING_ID_OV ,   PRODUCT_ID_OV ,   ITEM_OV ,   TYPE_OV ,");
                            sbAudRRHDelete.Append(" EFFECTIVE_START_DATE_OV ,   EFFECTIVE_END_DATE_OV,  SYS_LAST_MODIFIED_BY_OV, SYS_LAST_MODIFIED_DATE_OV   )");
                            sbAudRRHDelete.Append("SELECT SEQ_TRANSACTION_ID.NEXTVAL ,'DELETE', ROUTING_ID,PRODUCT_ID,ITEM,TYPE, ");
                            sbAudRRHDelete.Append(" MFG_EFFECTIVE_START_DATE,MFG_EFFECTIVE_END_DATE,SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE from ROUTING_RULE_HEADER ");
                            sbAudRRHDelete.Append("where ROUTING_ID='" + sitesvalue[i].ItemTypeId + "'");

                            using (OracleCommand cmdAudRRHInsert = new OracleCommand(sbAudRRHDelete.ToString(), oracleConnection))
                            {
                                intInsertRow = (int)cmdAudRRHInsert.ExecuteNonQuery();
                            }


                            sqlDelete = "Delete from ROUTING_RULE_HEADER where ROUTING_ID='" + sitesvalue[i].ItemTypeId + "'";

                            using (OracleCommand cmdDelete = new OracleCommand(sqlDelete, oracleConnection))
                            {
                                delRow = (int)cmdDelete.ExecuteNonQuery();
                            }
                        }

                        transaction.Commit();
                    }
                }
              

            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
            finally
            {
                if (oracleConnection != null)
                {
                    oracleConnection.Close();
                    oracleConnection.Dispose();
                }
            }
            return true;

        }

      


        public DataTable searchRoutings(string strId, string itemType, out string strType)
        {
            string strSql = null;
            DataTable dt = null;
            strType = "LINES";
            StringBuilder sbQuery = new StringBuilder();
            try
            {

                using (oracleConnection = new OracleConnection(connenctionString))
                {

                    if (itemType.Equals("PRODUCT_TREE"))
                    {
                        sbQuery.Append(" Select L.ID, H.ROUTING_ID,H.PRODUCT_ID || H.ITEM ProductFga, L.FULFILLMENT_SITE_ID, L.MANUFACTURING_SITE_ID,L.PRIMARY_ROUTING , ");
                        sbQuery.Append(" L.ALTERNATE_ROUTING_1 ,L.ALTERNATE_ROUTING_2 , ");
                        sbQuery.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=L.PRIMARY_ROUTING) PRIMARY_ROUTING_DESC, ");
                        sbQuery.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=L.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1_DESC, ");
                        sbQuery.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=L.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2_DESC, ");
                        sbQuery.Append(" TO_CHAR(L.ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(L.ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR (H.MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR (H.MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,L.MFG_SITE_SPLIT_PERCENT ");
                        sbQuery.Append(" from ROUTING_RULE_HEADER H, ROUTING_RULE_LINES L where H.ROUTING_ID=L.ROUTING_ID and  H.PRODUCT_ID='" + strId + "' ORDER BY  L.FULFILLMENT_SITE_ID, L.ROUTING_EFFECTIVE_START_DATE NULLS FIRST, L.ROUTING_EFFECTIVE_END_DATE  NULLS FIRST");
                        // strSql = "Select L.ID, H.ROUTING_ID,H.PRODUCT_ID || H.ITEM ProductFga, L.FULFILLMENT_SITE_ID, L.MANUFACTURING_SITE_ID,L.PRIMARY_ROUTING PRIMARY_ROUTING_ID, L.ALTERNATE_ROUTING_1ALTERNATE_ROUTING_1_ID,L.ALTERNATE_ROUTING_2 ALTERNATE_ROUTING_2_ID, TO_CHAR(L.EFFECTIVE_START_DATE, 'MM/DD/YYYY') EFFECTIVE_START_DATE,  TO_CHAR(L.EFFECTIVE_END_DATE, 'MM/DD/YYYY') EFFECTIVE_END_DATE " +
                        //             "from ROUTING_RULE_HEADER H, ROUTING_RULE_LINES L where H.ROUTING_ID=L.ROUTING_ID and H.PRODUCT_ID='" + strId + "'";
                    }
                    else
                        if (itemType.Equals("FHC_ITEM") || itemType.Equals("FGA_ITEM"))
                    {
                        sbQuery.Append("  Select L.ID, H.ROUTING_ID,H.PRODUCT_ID || H.ITEM ProductFga, L.FULFILLMENT_SITE_ID, L.MANUFACTURING_SITE_ID,L.PRIMARY_ROUTING , ");
                        sbQuery.Append("L.ALTERNATE_ROUTING_1 ,L.ALTERNATE_ROUTING_2 , ");
                        sbQuery.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=L.PRIMARY_ROUTING) PRIMARY_ROUTING_DESC, ");
                        sbQuery.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=L.ALTERNATE_ROUTING_1) ALTERNATE_ROUTING_1_DESC, ");
                        sbQuery.Append(" (select FROM_SITE_NAME||'->'||TO_SITE_NAME||'->'||SHIP_METHOD_NAME||'->'||LEAD_TIME_IN_DAYS as ROUTING from transportation_lead_time_vw where  TRNSP_LEAD_TIME_ID=L.ALTERNATE_ROUTING_2) ALTERNATE_ROUTING_2_DESC, ");
                        sbQuery.Append(" TO_CHAR(L.ROUTING_EFFECTIVE_START_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_START_DATE,  TO_CHAR(L.ROUTING_EFFECTIVE_END_DATE, 'MM/DD/YYYY') ROUTING_EFFECTIVE_END_DATE,TO_CHAR (H.MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_START_DATE,TO_CHAR (H.MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY') MFG_EFFECTIVE_END_DATE,L.MFG_SITE_SPLIT_PERCENT ");
                        sbQuery.Append(" from ROUTING_RULE_HEADER H, ROUTING_RULE_LINES L where H.ROUTING_ID=L.ROUTING_ID and H.ITEM='" + strId + "' ORDER BY  L.FULFILLMENT_SITE_ID, L.ROUTING_EFFECTIVE_START_DATE NULLS FIRST, L.ROUTING_EFFECTIVE_END_DATE  NULLS FIRST");
                        //strSql = "Select L.ID, H.ROUTING_ID,H.PRODUCT_ID || H.ITEM ProductFga, L.FULFILLMENT_SITE_ID, L.MANUFACTURING_SITE_ID,L.PRIMARY_ROUTING, L.ALTERNATE_ROUTING_1,L.ALTERNATE_ROUTING_2,TO_CHAR(L.EFFECTIVE_START_DATE, 'MM/DD/YYYY') EFFECTIVE_START_DATE, TO_CHAR(L.EFFECTIVE_END_DATE, 'MM/DD/YYYY') EFFECTIVE_END_DATE " +
                        //                               "from ROUTING_RULE_HEADER H, ROUTING_RULE_LINES L where H.ROUTING_ID=L.ROUTING_ID and H.ITEM='" + strId + "'";
                    }
                    //else
                    //{
                    //    strSql = "Select L.ID, H.ROUTING_ID,H.PRODUCT_ID || H.ITEM ProductFga, L.FULFILLMENT_SITE_ID, L.MANUFACTURING_SITE_ID,L.PRIMARY_ROUTING, L.ALTERNATE_ROUTING_1,L.ALTERNATE_ROUTING_2, L.EFFECTIVE_START_DATE, L.EFFECTIVE_END_DATE " +
                    //                                                           "from ROUTING_RULE_HEADER H, ROUTING_RULE_LINES L where H.ROUTING_ID=L.ROUTING_ID";
                    //}
                    oracleCommand = new OracleCommand(sbQuery.ToString(), oracleConnection);
                    oracleDataApt = new OracleDataAdapter(oracleCommand);
                    dt = new DataTable();
                    oracleDataApt.Fill(dt);
                    if (dt == null || dt.Rows.Count <= 0)
                    {
                        if (itemType.Equals("PRODUCT_TREE"))
                        {
                            strSql = "Select  H.ROUTING_ID,H.PRODUCT_ID || H.ITEM ProductFga from ROUTING_RULE_HEADER H WHERE H.PRODUCT_ID='" + strId + "'";
                        }
                        else
                            if (itemType.Equals("FHC_ITEM") || itemType.Equals("FGA_ITEM"))
                        {
                            strSql = "Select  H.ROUTING_ID,H.PRODUCT_ID || H.ITEM ProductFga from ROUTING_RULE_HEADER H where H.ITEM='" + strId + "'";
                        }
                        oracleCommand = new OracleCommand(strSql, oracleConnection);
                        oracleDataApt = new OracleDataAdapter(oracleCommand);
                        dt = new DataTable();
                        oracleDataApt.Fill(dt);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            strType = "HEADER";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (oracleConnection != null)
                {
                    oracleConnection.Close();
                    oracleConnection.Dispose();
                }
            }
            return dt;

        }



        public bool ValidateRoutingDates(string oldRoutingStartDate, string oldRoutingEndDate, string newRoutingStartDate, string newRoutingEndDate)
        {
            string sqlText = string.Empty;
            string retValue = string.Empty;
            OracleConnection conn = null;

            if (oldRoutingStartDate == null || oldRoutingStartDate == " " || oldRoutingStartDate == string.Empty)
                oldRoutingStartDate = null;

            if (oldRoutingEndDate == null || oldRoutingEndDate == " " || oldRoutingEndDate == string.Empty)
                oldRoutingEndDate = null;

            if (newRoutingStartDate == null || newRoutingStartDate == " " || newRoutingStartDate == string.Empty)
                newRoutingStartDate = null;

            if (newRoutingEndDate == null || newRoutingEndDate == " " || newRoutingEndDate == string.Empty)
                newRoutingEndDate = null;

            try
            {
                using (conn = new OracleConnection(connenctionString))
                {

                    sqlText = "select SNOP_RP_UI_UTIL_PKG.FN_RR_ROUTING_DATES_VALIDATION(TO_DATE('" + oldRoutingStartDate + "','mm/dd/yyyy')," + "TO_DATE('" + oldRoutingEndDate + "','mm/dd/yyyy')," + "TO_DATE('" + newRoutingStartDate + "','mm/dd/yyyy')," + "TO_DATE('" + newRoutingEndDate + "','mm/dd/yyyy')" + ") MESSAGE from dual";

                    using (OracleCommand cmdID = new OracleCommand(sqlText, conn))
                    {
                        conn.Open();
                        OracleDataReader oracleDataReader = cmdID.ExecuteReader(CommandBehavior.CloseConnection);

                        while (oracleDataReader.Read())
                        {
                            retValue = oracleDataReader["MESSAGE"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            if (retValue == "SUCCESS")
            {
                return true;
            }
            else
                return false;
        }






        public bool addRoutingRule(RoutingRule routingRule, string updatedBy)
        {
            string strRoutingId = routingRule.RoutingId;
            string strProductId = routingRule.ProductId;
            string strFgaItem = routingRule.FgaItem;
            string strType = routingRule.Type;
            string strMFGStartDate = routingRule.Mfg_effective_startDate;
            string strMFGEndDate = routingRule.Mfg_effective_endDate;
            OracleParameter ROUTING_ID = null;
            OracleParameter FULFILLMENT_SITE_ID = null;
            OracleParameter MANUFACTURING_SITE_ID = null;
            OracleParameter PRIMARY_ROUTING = null;
            OracleParameter ALTERNATE_ROUTING_1 = null;
            OracleParameter ALTERNATE_ROUTING_2 = null;
            OracleParameter ROUTING_EFFECTIVE_START_DATE = null;
            OracleParameter ROUTING_EFFECTIVE_END_DATE = null;
            OracleParameter ID = null;
            OracleParameter MFG_EFFECTIVE_START_DATE = null; //Added as per the multi site change.
            OracleParameter MFG_EFFECTIVE_END_DATE = null;
            OracleParameter MFG_SPLIT_PERCENT = null;
            DataTable dt = null;
            DataTable dtSecondary = null;

            int intInsertedRows;
            string insertSql = null;

            int intNumberOfRows;
            string strSelect;
            string strSelectSecondary;

            OracleTransaction transaction = null;
            Dictionary<string, string> dictOldData = new Dictionary<string, string>();
            try
            {
                intNumberOfRows = routingRule.RoutingRuleLinesList.Count();

                using (oracleConnection = new OracleConnection(connenctionString))
                {
                    oracleConnection.Open();
                    transaction = oracleConnection.BeginTransaction();

                    //If dates are different, then consider as a new rule.
                    if (!string.IsNullOrEmpty(strProductId))
                    {
                        strSelect = "Select * from ROUTING_RULE_HEADER where PRODUCT_ID='" + strProductId + "' and NVL(TO_CHAR(MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY'),' ') ='" + strMFGStartDate + "' and NVL(TO_CHAR(MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY'),' ') ='" + strMFGEndDate + "'";
                    }
                    else
                    {
                        strSelect = "Select * from ROUTING_RULE_HEADER where ITEM='" + strFgaItem + "' and NVL(TO_CHAR(MFG_EFFECTIVE_START_DATE, 'MM/DD/YYYY'),' ') ='" + strMFGStartDate + "' and NVL(TO_CHAR(MFG_EFFECTIVE_END_DATE, 'MM/DD/YYYY'),' ') ='" + strMFGEndDate + "'";
                    }

                    using (OracleCommand oracleCommand = new OracleCommand(strSelect, oracleConnection))
                    {
                        oracleDataApt = new OracleDataAdapter(oracleCommand);
                        dt = new DataTable();
                        oracleDataApt.Fill(dt);
                    }

                    
                    //if header record not exist insert header and child
                    if (dt.Rows.Count < 1)
                    {
                        //insert header
                        insertSql = "INSERT INTO ROUTING_RULE_HEADER(ROUTING_ID, PRODUCT_ID, ITEM, TYPE,MFG_EFFECTIVE_START_DATE,MFG_EFFECTIVE_END_DATE,SYS_SOURCE,SYS_CREATED_BY,SYS_CREATION_DATE,SYS_LAST_MODIFIED_BY,SYS_LAST_MODIFIED_DATE,SYS_ENT_STATE) "
                                            + "VALUES (:ROUTING_ID, :PRODUCT_ID, :ITEM,:TYPE,:MFG_EFFECTIVE_START_DATE,:MFG_EFFECTIVE_END_DATE,:SYS_SOURCE,:SYS_CREATED_BY,SYSDATE,:SYS_LAST_MODIFIED_BY,SYSDATE,:SYS_ENT_STATE) ";

                        using (OracleCommand cmdInsert = new OracleCommand(insertSql, oracleConnection))
                        {
                            OracleParameter opRoutingId = new OracleParameter("ROUTING_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            opRoutingId.Value = strRoutingId;
                            cmdInsert.Parameters.Add(opRoutingId);

                            OracleParameter opProductId = new OracleParameter("PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            opProductId.Value = strProductId;
                            cmdInsert.Parameters.Add(opProductId);

                            OracleParameter opFgaItem = new OracleParameter("ITEM", OracleDbType.Varchar2, ParameterDirection.Input);
                            opFgaItem.Value = strFgaItem;
                            cmdInsert.Parameters.Add(opFgaItem);

                            OracleParameter opType = new OracleParameter("TYPE", OracleDbType.Varchar2, ParameterDirection.Input);
                            opType.Value = strType;
                            cmdInsert.Parameters.Add(opType);

                            //MFG Effective start date.
                            OracleParameter opMFGStartDate = new OracleParameter("MFG_EFFECTIVE_START_DATE", OracleDbType.Date, ParameterDirection.Input);
                            if (strMFGStartDate != null && strMFGStartDate != string.Empty && strMFGStartDate != " ")
                            {
                                opMFGStartDate.Value = DateTime.Parse(strMFGStartDate);
                            }
                            //else
                            //{
                            //    opMFGStartDate.Value = "";
                            //}
                            cmdInsert.Parameters.Add(opMFGStartDate);

                            //MFG Effective end date.
                            OracleParameter opMFGEndDate = new OracleParameter("MFG_EFFECTIVE_END_DATE", OracleDbType.Date, ParameterDirection.Input);
                            if (strMFGEndDate != null && strMFGEndDate != string.Empty && strMFGEndDate != " ")
                            {
                                opMFGEndDate.Value = DateTime.Parse(strMFGEndDate);
                            }
                          
                            cmdInsert.Parameters.Add(opMFGEndDate);


                            OracleParameter opSysSource = new OracleParameter("SYS_SOURCE", OracleDbType.Varchar2, ParameterDirection.Input);
                            opSysSource.Value = "MossUI";
                            cmdInsert.Parameters.Add(opSysSource);

                            OracleParameter opSysCreatedBy = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                            opSysCreatedBy.Value = updatedBy;
                            cmdInsert.Parameters.Add(opSysCreatedBy);

                            OracleParameter opSysLastModifiedBy = new OracleParameter("SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                            opSysLastModifiedBy.Value = updatedBy;
                            cmdInsert.Parameters.Add(opSysLastModifiedBy);

                            OracleParameter opSysEntState = new OracleParameter("SYS_ENT_STATE", OracleDbType.Varchar2, ParameterDirection.Input);
                            opSysEntState.Value = "ACTIVE";
                            cmdInsert.Parameters.Add(opSysEntState);
                            intInsertedRows = (int)cmdInsert.ExecuteNonQuery();
                        }

                        //Mfg dates are not added to audit tables.
                        StringBuilder sbAudRRHInsert = new StringBuilder();
                        sbAudRRHInsert.Append("INSERT  INTO AUD_ROUTING_RULE_HEADER ( TRANSACTION_ID ,  TRANSACTION_ACTION , ROUTING_ID_NV ,   PRODUCT_ID_NV ,   ITEM_NV ,   TYPE_NV ,");
                        sbAudRRHInsert.Append(" EFFECTIVE_START_DATE_NV ,   EFFECTIVE_END_DATE_NV,  SYS_LAST_MODIFIED_BY_NV, SYS_LAST_MODIFIED_DATE_NV   )");
                        sbAudRRHInsert.Append("SELECT SEQ_TRANSACTION_ID.NEXTVAL ,'INSERT', ROUTING_ID,PRODUCT_ID,ITEM,TYPE, ");
                        sbAudRRHInsert.Append("MFG_EFFECTIVE_START_DATE,MFG_EFFECTIVE_END_DATE,SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE from ROUTING_RULE_HEADER ");
                        sbAudRRHInsert.Append("where ROUTING_ID='" + strRoutingId + "'");
                        using (OracleCommand cmdAudRRHInsert = new OracleCommand(sbAudRRHInsert.ToString(), oracleConnection))
                        {
                            intInsertedRows = (int)cmdAudRRHInsert.ExecuteNonQuery();
                        }


                        //Added by Kanishka, to eliminate the duplicate insertions into the routing rule lines table.
                        //If dates are different, then consider as a new rule.

                        if (!string.IsNullOrEmpty(strProductId))
                        {
                            strSelectSecondary = "Select * from ROUTING_RULE_HEADER where PRODUCT_ID='" + strProductId + "'";
                        }
                        else
                        {
                            strSelectSecondary = "Select * from ROUTING_RULE_HEADER where ITEM='" + strFgaItem + "'";
                        }

                        using (OracleCommand oracleCommand = new OracleCommand(strSelectSecondary, oracleConnection))
                        {
                            oracleDataApt = new OracleDataAdapter(oracleCommand);
                            dtSecondary = new DataTable();
                            oracleDataApt.Fill(dtSecondary);
                        }

                        if (dtSecondary.Rows.Count > 0)
                        {
                            string strSelectRRL = "SELECT ID FROM ROUTING_RULE_LINES WHERE ROUTING_ID=:ROUTING_ID";

                            using (OracleCommand cmd = new OracleCommand(strSelectRRL.ToString(), oracleConnection))
                            {
                                OracleParameter opRoutingID = new OracleParameter("ROUTING_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                foreach (DataRow Dr in dtSecondary.Rows)
                                {
                                    opRoutingID.Value = Dr["ROUTING_ID"].ToString();
                                    cmd.Parameters.Add(opRoutingID);

                                    OracleDataReader oracleDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                                    while (oracleDataReader.Read())
                                    {
                                        dictOldData.Add(oracleDataReader["ID"].ToString(), oracleDataReader["ID"].ToString());
                                    }

                                    //opRoutingID.Value = null;
                                    cmd.Parameters.Remove(opRoutingID);
                                }
                            }

                            for (int i = 0; i < intNumberOfRows; i++)
                            {
                                bool isRecordExist = dictOldData.ContainsKey(routingRule.RoutingRuleLinesList[i].ID);

                                //If the ID already exists,then proceed accordingly based on the STATUS value.
                                if (isRecordExist)
                                {
                                    dictOldData.Remove(routingRule.RoutingRuleLinesList[i].ID);
                                    if (routingRule.RoutingRuleLinesList[i].Status == "UPDATE" || routingRule.RoutingRuleLinesList[i].Status == "INSERT")
                                    {
                                        string strSelectReord = "SELECT * FROM ROUTING_RULE_LINES WHERE ID='" + routingRule.RoutingRuleLinesList[i].ID + "'";
                                        DataTable dtData = new DataTable();
                                        OracleDataAdapter adapter = new OracleDataAdapter(strSelectReord, oracleConnection);
                                        adapter.Fill(dtData);

                                        StringBuilder sbAudRRLUpdate = new StringBuilder();
                                        sbAudRRLUpdate.Append(" INSERT  INTO AUD_ROUTING_RULE_LINES ( TRANSACTION_ID ,  TRANSACTION_ACTION , ROUTING_ID_OV,ROUTING_ID_NV ,   FULFILLMENT_SITE_ID_OV,FULFILLMENT_SITE_ID_NV ,   MANUFACTURING_SITE_ID_OV,MANUFACTURING_SITE_ID_NV ,   PRIMARY_ROUTING_OV,PRIMARY_ROUTING_NV ,");
                                        sbAudRRLUpdate.Append("  ALTERNATE_ROUTING_1_OV ,ALTERNATE_ROUTING_1_NV,   ALTERNATE_ROUTING_2_OV,ALTERNATE_ROUTING_2_NV,   EFFECTIVE_START_DATE_OV ,EFFECTIVE_START_DATE_NV,   EFFECTIVE_END_DATE_OV,EFFECTIVE_END_DATE_NV,  SYS_LAST_MODIFIED_BY_OV,SYS_LAST_MODIFIED_BY_NV, SYS_LAST_MODIFIED_DATE_OV,SYS_LAST_MODIFIED_DATE_NV   )");
                                        sbAudRRLUpdate.Append("values(");
                                        sbAudRRLUpdate.Append("SEQ_TRANSACTION_ID.NEXTVAL,");
                                        sbAudRRLUpdate.Append("'UPDATE',");
                                        sbAudRRLUpdate.Append("'" + dtData.Rows[0]["ROUTING_ID"].ToString() + "',");
                                        sbAudRRLUpdate.Append("'" + dtData.Rows[0]["ROUTING_ID"].ToString() + "',");
                                        sbAudRRLUpdate.Append("'" + dtData.Rows[0]["FULFILLMENT_SITE_ID"].ToString() + "',");
                                        sbAudRRLUpdate.Append("'" + routingRule.RoutingRuleLinesList[i].FulfillmentSite + "',");
                                        sbAudRRLUpdate.Append("'" + dtData.Rows[0]["MANUFACTURING_SITE_ID"].ToString() + "',");
                                        sbAudRRLUpdate.Append("'" + routingRule.RoutingRuleLinesList[i].ManufacturingSite + "',");
                                        sbAudRRLUpdate.Append("'" + dtData.Rows[0]["PRIMARY_ROUTING"].ToString() + "',");
                                        sbAudRRLUpdate.Append("'" + routingRule.RoutingRuleLinesList[i].PrimaryRouting + "',");
                                        sbAudRRLUpdate.Append("'" + dtData.Rows[0]["ALTERNATE_ROUTING_1"].ToString() + "',");
                                        sbAudRRLUpdate.Append("'" + routingRule.RoutingRuleLinesList[i].AlternateRouting1 + "',");
                                        sbAudRRLUpdate.Append("'" + dtData.Rows[0]["ALTERNATE_ROUTING_2"].ToString() + "',");
                                        sbAudRRLUpdate.Append("'" + routingRule.RoutingRuleLinesList[i].AlternateRouting2 + "',");

                                        //if (!string.IsNullOrEmpty(dtData.Rows[0]["ROUTING_EFFECTIVE_START_DATE"].ToString()))
                                        if (dtData.Rows[0]["ROUTING_EFFECTIVE_START_DATE"].ToString() != null && dtData.Rows[0]["ROUTING_EFFECTIVE_START_DATE"].ToString() != string.Empty && dtData.Rows[0]["ROUTING_EFFECTIVE_START_DATE"].ToString() != " ")
                                        {
                                            sbAudRRLUpdate.Append(" to_date('" + dtData.Rows[0]["ROUTING_EFFECTIVE_START_DATE"].ToString() + "','mm/dd/yyyy:hh:mi:ssam'), ");
                                        }
                                        else
                                        {
                                            sbAudRRLUpdate.Append(" null ,");
                                        }
                                        //if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_StartDate))
                                        if (routingRule.RoutingRuleLinesList[i].Routing_StartDate != null && routingRule.RoutingRuleLinesList[i].Routing_StartDate != string.Empty && routingRule.RoutingRuleLinesList[i].Routing_StartDate != " ")
                                        {
                                            sbAudRRLUpdate.Append(" to_date('" + routingRule.RoutingRuleLinesList[i].Routing_StartDate + "','mm/dd/yyyy:hh:mi:ssam'), ");
                                        }
                                        else
                                        {
                                            sbAudRRLUpdate.Append(" null ,");
                                        }
                                        //if (!string.IsNullOrEmpty(dtData.Rows[0]["ROUTING_EFFECTIVE_END_DATE"].ToString()))
                                        if (dtData.Rows[0]["ROUTING_EFFECTIVE_END_DATE"].ToString() != null && dtData.Rows[0]["ROUTING_EFFECTIVE_END_DATE"].ToString() != string.Empty && dtData.Rows[0]["ROUTING_EFFECTIVE_END_DATE"].ToString() != " ")
                                        {
                                            sbAudRRLUpdate.Append(" to_date('" + DateTime.Parse(dtData.Rows[0]["ROUTING_EFFECTIVE_END_DATE"].ToString()) + "','mm/dd/yyyy:hh:mi:ssam'), ");
                                        }
                                        else
                                        {
                                            sbAudRRLUpdate.Append(" null ,");
                                        }
                                        //if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_EndDate))
                                        if (routingRule.RoutingRuleLinesList[i].Routing_EndDate != null && routingRule.RoutingRuleLinesList[i].Routing_EndDate != string.Empty && routingRule.RoutingRuleLinesList[i].Routing_EndDate != " ")
                                        {
                                            sbAudRRLUpdate.Append(" to_date('" + routingRule.RoutingRuleLinesList[i].Routing_EndDate + "','mm/dd/yyyy:hh:mi:ssam'), ");
                                        }
                                        else
                                        {
                                            sbAudRRLUpdate.Append(" null ,");
                                        }
                                        sbAudRRLUpdate.Append("'" + dtData.Rows[0]["SYS_LAST_MODIFIED_BY"].ToString() + "',");
                                        sbAudRRLUpdate.Append("'" + updatedBy + "',");
                                        sbAudRRLUpdate.Append(" to_date('" + dtData.Rows[0]["SYS_LAST_MODIFIED_DATE"].ToString() + "','mm/dd/yyyy:hh:mi:ssam'), ");
                                        sbAudRRLUpdate.Append(" SYSDATE)");
                                        using (OracleCommand cmdAudRRLInsert = new OracleCommand(sbAudRRLUpdate.ToString(), oracleConnection))
                                        {
                                            intInsertedRows = (int)cmdAudRRLInsert.ExecuteNonQuery();
                                            // need to modify to update Effective date
                                        }

                                        //code is required to update 
                                        string sqlUpdate = "UPDATE ROUTING_RULE_LINES SET FULFILLMENT_SITE_ID=:FULFILLMENT_SITE_ID,MANUFACTURING_SITE_ID=:MANUFACTURING_SITE_ID,PRIMARY_ROUTING=:PRIMARY_ROUTING, " +
                                             "ALTERNATE_ROUTING_1=:ALTERNATE_ROUTING_1,ALTERNATE_ROUTING_2=:ALTERNATE_ROUTING_2, ROUTING_EFFECTIVE_START_DATE=:ROUTING_EFFECTIVE_START_DATE,ROUTING_EFFECTIVE_END_DATE=:ROUTING_EFFECTIVE_END_DATE, " +
                                             "SYS_LAST_MODIFIED_BY=:SYS_LAST_MODIFIED_BY,SYS_LAST_MODIFIED_DATE=SYSDATE,MFG_SITE_SPLIT_PERCENT=:MFG_SITE_SPLIT_PERCENT where ID=:ID";
                                        using (OracleCommand oracleCommand = new OracleCommand(sqlUpdate, oracleConnection))
                                        {
                                            FULFILLMENT_SITE_ID = new OracleParameter("FULFILLMENT_SITE_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                            MANUFACTURING_SITE_ID = new OracleParameter("MANUFACTURING_SITE_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                            PRIMARY_ROUTING = new OracleParameter("PRIMARY_ROUTING", OracleDbType.Varchar2, ParameterDirection.Input);
                                            ALTERNATE_ROUTING_1 = new OracleParameter("ALTERNATE_ROUTING_1", OracleDbType.Varchar2, ParameterDirection.Input);
                                            ALTERNATE_ROUTING_2 = new OracleParameter("ALTERNATE_ROUTING_2", OracleDbType.Varchar2, ParameterDirection.Input);
                                            ROUTING_EFFECTIVE_START_DATE = new OracleParameter("ROUTING_EFFECTIVE_START_DATE", OracleDbType.Date, ParameterDirection.Input);
                                            ROUTING_EFFECTIVE_END_DATE = new OracleParameter("ROUTING_EFFECTIVE_END_DATE", OracleDbType.Date, ParameterDirection.Input);
                                            MFG_SPLIT_PERCENT = new OracleParameter("MFG_SITE_SPLIT_PERCENT", OracleDbType.Varchar2, ParameterDirection.Input);
                                            ID = new OracleParameter("ID", OracleDbType.Varchar2, ParameterDirection.Input);

                                            FULFILLMENT_SITE_ID.Value = routingRule.RoutingRuleLinesList[i].FulfillmentSite;
                                            MANUFACTURING_SITE_ID.Value = routingRule.RoutingRuleLinesList[i].ManufacturingSite;
                                            PRIMARY_ROUTING.Value = routingRule.RoutingRuleLinesList[i].PrimaryRouting;
                                            ALTERNATE_ROUTING_1.Value = routingRule.RoutingRuleLinesList[i].AlternateRouting1;
                                            ALTERNATE_ROUTING_2.Value = routingRule.RoutingRuleLinesList[i].AlternateRouting2;

                                            //if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_StartDate))
                                            if (routingRule.RoutingRuleLinesList[i].Routing_StartDate != null && routingRule.RoutingRuleLinesList[i].Routing_StartDate != string.Empty && routingRule.RoutingRuleLinesList[i].Routing_StartDate != " ")
                                            {
                                                ROUTING_EFFECTIVE_START_DATE.Value = DateTime.Parse(routingRule.RoutingRuleLinesList[i].Routing_StartDate);
                                            }
                                            //if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_EndDate))
                                            if (routingRule.RoutingRuleLinesList[i].Routing_EndDate != null && routingRule.RoutingRuleLinesList[i].Routing_EndDate != string.Empty && routingRule.RoutingRuleLinesList[i].Routing_EndDate != " ")
                                            {
                                                ROUTING_EFFECTIVE_END_DATE.Value = DateTime.Parse(routingRule.RoutingRuleLinesList[i].Routing_EndDate);
                                            }

                                            MFG_SPLIT_PERCENT.Value = routingRule.RoutingRuleLinesList[i].MfgSplitPercent;

                                            ID.Value = routingRule.RoutingRuleLinesList[i].ID;

                                            oracleCommand.Parameters.Add(FULFILLMENT_SITE_ID);
                                            oracleCommand.Parameters.Add(MANUFACTURING_SITE_ID);
                                            oracleCommand.Parameters.Add(PRIMARY_ROUTING);
                                            oracleCommand.Parameters.Add(ALTERNATE_ROUTING_1);
                                            oracleCommand.Parameters.Add(ALTERNATE_ROUTING_2);
                                            oracleCommand.Parameters.Add(ROUTING_EFFECTIVE_START_DATE);
                                            oracleCommand.Parameters.Add(ROUTING_EFFECTIVE_END_DATE);


                                            OracleParameter opSysLastModifiedBy = new OracleParameter("SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                                            opSysLastModifiedBy.Value = updatedBy;
                                            oracleCommand.Parameters.Add(opSysLastModifiedBy);
                                            oracleCommand.Parameters.Add(MFG_SPLIT_PERCENT);
                                            oracleCommand.Parameters.Add(ID);
                                          
                                            intInsertedRows = (int)oracleCommand.ExecuteNonQuery();
                                        }
                                    }
                                }
                                else
                                {
                                    //Insert Child Record
                                    insertSql = "INSERT INTO ROUTING_RULE_LINES(ID, ROUTING_ID, FULFILLMENT_SITE_ID, MANUFACTURING_SITE_ID,PRIMARY_ROUTING,ALTERNATE_ROUTING_1, ALTERNATE_ROUTING_2, ROUTING_EFFECTIVE_START_DATE, ROUTING_EFFECTIVE_END_DATE, SYS_SOURCE,SYS_CREATED_BY,SYS_CREATION_DATE,SYS_LAST_MODIFIED_BY,SYS_LAST_MODIFIED_DATE,SYS_ENT_STATE,MFG_SITE_SPLIT_PERCENT) "
                                                                     + "VALUES (SEQ_TRANSACTION_ID.NEXTVAL,:ROUTING_ID, :FULFILLMENT_SITE_ID, :MANUFACTURING_SITE_ID,:PRIMARY_ROUTING,:ALTERNATE_ROUTING_1,:ALTERNATE_ROUTING_2,:ROUTING_EFFECTIVE_START_DATE,:ROUTING_EFFECTIVE_END_DATE,:SYS_SOURCE,:SYS_CREATED_BY,SYSDATE,:SYS_LAST_MODIFIED_BY,SYSDATE,:SYS_ENT_STATE,:MFG_SITE_SPLIT_PERCENT) ";
                                    //for (int i = 0; i < intNumberOfRows; i++)
                                    //{
                                    using (OracleCommand oracleCommand = new OracleCommand(insertSql, oracleConnection))
                                    {
                                        ROUTING_ID = new OracleParameter("ROUTING_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                        FULFILLMENT_SITE_ID = new OracleParameter("FULFILLMENT_SITE_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                        MANUFACTURING_SITE_ID = new OracleParameter("MANUFACTURING_SITE_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                        PRIMARY_ROUTING = new OracleParameter("PRIMARY_ROUTING", OracleDbType.Varchar2, ParameterDirection.Input);
                                        ALTERNATE_ROUTING_1 = new OracleParameter("ALTERNATE_ROUTING_1", OracleDbType.Varchar2, ParameterDirection.Input);
                                        ALTERNATE_ROUTING_2 = new OracleParameter("ALTERNATE_ROUTING_2", OracleDbType.Varchar2, ParameterDirection.Input);
                                        ROUTING_EFFECTIVE_START_DATE = new OracleParameter("ROUTING_EFFECTIVE_START_DATE", OracleDbType.Date, ParameterDirection.Input);
                                        ROUTING_EFFECTIVE_END_DATE = new OracleParameter("ROUTING_EFFECTIVE_END_DATE", OracleDbType.Date, ParameterDirection.Input);


                                        ROUTING_ID.Value = strRoutingId;
                                        FULFILLMENT_SITE_ID.Value = routingRule.RoutingRuleLinesList[i].FulfillmentSite;
                                        MANUFACTURING_SITE_ID.Value = routingRule.RoutingRuleLinesList[i].ManufacturingSite;
                                        PRIMARY_ROUTING.Value = routingRule.RoutingRuleLinesList[i].PrimaryRouting;
                                        ALTERNATE_ROUTING_1.Value = routingRule.RoutingRuleLinesList[i].AlternateRouting1;
                                        ALTERNATE_ROUTING_2.Value = routingRule.RoutingRuleLinesList[i].AlternateRouting2;

                                        //if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_StartDate))
                                        if (routingRule.RoutingRuleLinesList[i].Routing_StartDate != null && routingRule.RoutingRuleLinesList[i].Routing_StartDate != string.Empty && routingRule.RoutingRuleLinesList[i].Routing_StartDate != " ")
                                        {
                                            ROUTING_EFFECTIVE_START_DATE.Value = DateTime.Parse(routingRule.RoutingRuleLinesList[i].Routing_StartDate);
                                        }
                                        //if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_EndDate))
                                        if (routingRule.RoutingRuleLinesList[i].Routing_EndDate != null && routingRule.RoutingRuleLinesList[i].Routing_EndDate != string.Empty && routingRule.RoutingRuleLinesList[i].Routing_EndDate != " ")
                                        {
                                            ROUTING_EFFECTIVE_END_DATE.Value = DateTime.Parse(routingRule.RoutingRuleLinesList[i].Routing_EndDate);
                                        }


                                        oracleCommand.Parameters.Add(ROUTING_ID);
                                        oracleCommand.Parameters.Add(FULFILLMENT_SITE_ID);
                                        oracleCommand.Parameters.Add(MANUFACTURING_SITE_ID);
                                        oracleCommand.Parameters.Add(PRIMARY_ROUTING);
                                        oracleCommand.Parameters.Add(ALTERNATE_ROUTING_1);
                                        oracleCommand.Parameters.Add(ALTERNATE_ROUTING_2);
                                        oracleCommand.Parameters.Add(ROUTING_EFFECTIVE_START_DATE);
                                        oracleCommand.Parameters.Add(ROUTING_EFFECTIVE_END_DATE);


                                        OracleParameter opSysSource = new OracleParameter("SYS_SOURCE", OracleDbType.Varchar2, ParameterDirection.Input);
                                        opSysSource.Value = "MossUI";
                                        oracleCommand.Parameters.Add(opSysSource);

                                        OracleParameter opSysCreatedBy = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                                        opSysCreatedBy.Value = updatedBy;
                                        oracleCommand.Parameters.Add(opSysCreatedBy);

                                        OracleParameter opSysLastModifiedBy = new OracleParameter("SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                                        opSysLastModifiedBy.Value = updatedBy;
                                        oracleCommand.Parameters.Add(opSysLastModifiedBy);

                                        OracleParameter opSysEntState = new OracleParameter("SYS_ENT_STATE", OracleDbType.Varchar2, ParameterDirection.Input);
                                        opSysEntState.Value = "Active";
                                        oracleCommand.Parameters.Add(opSysEntState);

                                        MFG_SPLIT_PERCENT = new OracleParameter("MFG_SITE_SPLIT_PERCENT", OracleDbType.Varchar2, ParameterDirection.Input);
                                        MFG_SPLIT_PERCENT.Value = routingRule.RoutingRuleLinesList[i].MfgSplitPercent;
                                        oracleCommand.Parameters.Add(MFG_SPLIT_PERCENT);

                                        intInsertedRows = (int)oracleCommand.ExecuteNonQuery();

                                    }
                                    //}
                                }

                                //Split is not getting added to Audit as of now.
                                StringBuilder sbAudRRLDelete = new StringBuilder();
                                sbAudRRLDelete.Append(" INSERT  INTO AUD_ROUTING_RULE_LINES ( TRANSACTION_ID ,  TRANSACTION_ACTION , ROUTING_ID_NV ,   FULFILLMENT_SITE_ID_NV ,   MANUFACTURING_SITE_ID_NV ,   PRIMARY_ROUTING_NV ,");
                                sbAudRRLDelete.Append("  ALTERNATE_ROUTING_1_NV ,   ALTERNATE_ROUTING_2_NV,   EFFECTIVE_START_DATE_NV ,   EFFECTIVE_END_DATE_NV,  SYS_LAST_MODIFIED_BY_NV, SYS_LAST_MODIFIED_DATE_NV   )");
                                sbAudRRLDelete.Append(" SELECT SEQ_TRANSACTION_ID.NEXTVAL ,'INSERT', ROUTING_ID,FULFILLMENT_SITE_ID,MANUFACTURING_SITE_ID,PRIMARY_ROUTING, ALTERNATE_ROUTING_1, ALTERNATE_ROUTING_2,");
                                sbAudRRLDelete.Append("  ROUTING_EFFECTIVE_START_DATE,ROUTING_EFFECTIVE_END_DATE,SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE from ROUTING_RULE_LINES ");
                                sbAudRRLDelete.Append("where ROUTING_ID='" + strRoutingId + "'");
                                using (OracleCommand cmdAudRRLInsert = new OracleCommand(sbAudRRLDelete.ToString(), oracleConnection))
                                {
                                    intInsertedRows = (int)cmdAudRRLInsert.ExecuteNonQuery();
                                }
                            } //end of the for loop
                        }// end of dtSecondary table rows count check.
                        else
                        {
                            //Insert Child Record
                            //insertSql = "INSERT INTO ROUTING_RULE_LINES(ID, ROUTING_ID, FULFILLMENT_SITE_ID, MANUFACTURING_SITE_ID,PRIMARY_ROUTING,ALTERNATE_ROUTING_1, ALTERNATE_ROUTING_2, ROUTING_EFFECTIVE_START_DATE, ROUTING_EFFECTIVE_END_DATE, SYS_SOURCE,SYS_CREATED_BY,SYS_CREATION_DATE,SYS_LAST_MODIFIED_BY,SYS_LAST_MODIFIED_DATE,SYS_ENT_STATE,MFG_SITE_SPLIT_PERCENT) "
                            //                                 + "VALUES (SEQ_TRANSACTION_ID.NEXTVAL,:ROUTING_ID, :FULFILLMENT_SITE_ID, :MANUFACTURING_SITE_ID,:PRIMARY_ROUTING,:ALTERNATE_ROUTING_1,:ALTERNATE_ROUTING_2,:ROUTING_EFFECTIVE_START_DATE,:ROUTING_EFFECTIVE_END_DATE,:SYS_SOURCE,:SYS_CREATED_BY,SYSDATE,:SYS_LAST_MODIFIED_BY,SYSDATE,:SYS_ENT_STATE,:MFG_SITE_SPLIT_PERCENT) ";
                            insertSql = "INSERT INTO ROUTING_RULE_LINES(ID, ROUTING_ID, FULFILLMENT_SITE_ID, MANUFACTURING_SITE_ID,PRIMARY_ROUTING,ALTERNATE_ROUTING_1, ALTERNATE_ROUTING_2, ROUTING_EFFECTIVE_START_DATE, ROUTING_EFFECTIVE_END_DATE, SYS_SOURCE,SYS_CREATED_BY,SYS_CREATION_DATE,SYS_LAST_MODIFIED_BY,SYS_LAST_MODIFIED_DATE,SYS_ENT_STATE,MFG_SITE_SPLIT_PERCENT) "
                                                             + "VALUES (49477965,:ROUTING_ID, :FULFILLMENT_SITE_ID, :MANUFACTURING_SITE_ID,:PRIMARY_ROUTING,:ALTERNATE_ROUTING_1,:ALTERNATE_ROUTING_2,:ROUTING_EFFECTIVE_START_DATE,:ROUTING_EFFECTIVE_END_DATE,:SYS_SOURCE,:SYS_CREATED_BY,SYSDATE,:SYS_LAST_MODIFIED_BY,SYSDATE,:SYS_ENT_STATE,:MFG_SITE_SPLIT_PERCENT) ";
                            for (int i = 0; i < intNumberOfRows; i++)
                            {
                                using (OracleCommand oracleCommand = new OracleCommand(insertSql, oracleConnection))
                                {
                                    ROUTING_ID = new OracleParameter("ROUTING_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                    FULFILLMENT_SITE_ID = new OracleParameter("FULFILLMENT_SITE_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                    MANUFACTURING_SITE_ID = new OracleParameter("MANUFACTURING_SITE_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                    PRIMARY_ROUTING = new OracleParameter("PRIMARY_ROUTING", OracleDbType.Varchar2, ParameterDirection.Input);
                                    ALTERNATE_ROUTING_1 = new OracleParameter("ALTERNATE_ROUTING_1", OracleDbType.Varchar2, ParameterDirection.Input);
                                    ALTERNATE_ROUTING_2 = new OracleParameter("ALTERNATE_ROUTING_2", OracleDbType.Varchar2, ParameterDirection.Input);
                                    ROUTING_EFFECTIVE_START_DATE = new OracleParameter("ROUTING_EFFECTIVE_START_DATE", OracleDbType.Date, ParameterDirection.Input);
                                    ROUTING_EFFECTIVE_END_DATE = new OracleParameter("ROUTING_EFFECTIVE_END_DATE", OracleDbType.Date, ParameterDirection.Input);


                                    ROUTING_ID.Value = strRoutingId;
                                    FULFILLMENT_SITE_ID.Value = routingRule.RoutingRuleLinesList[i].FulfillmentSite;
                                    MANUFACTURING_SITE_ID.Value = routingRule.RoutingRuleLinesList[i].ManufacturingSite;
                                    PRIMARY_ROUTING.Value = routingRule.RoutingRuleLinesList[i].PrimaryRouting;
                                    ALTERNATE_ROUTING_1.Value = routingRule.RoutingRuleLinesList[i].AlternateRouting1;
                                    ALTERNATE_ROUTING_2.Value = routingRule.RoutingRuleLinesList[i].AlternateRouting2;
                                    if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_StartDate))
                                    {
                                        ROUTING_EFFECTIVE_START_DATE.Value = DateTime.Parse(routingRule.RoutingRuleLinesList[i].Routing_StartDate);
                                    }
                                    if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_EndDate))
                                    {
                                        ROUTING_EFFECTIVE_END_DATE.Value = DateTime.Parse(routingRule.RoutingRuleLinesList[i].Routing_EndDate);
                                    }


                                    ROUTING_ID.Value = strRoutingId;
                                    FULFILLMENT_SITE_ID.Value = routingRule.RoutingRuleLinesList[i].FulfillmentSite;
                                    MANUFACTURING_SITE_ID.Value = routingRule.RoutingRuleLinesList[i].ManufacturingSite;
                                    PRIMARY_ROUTING.Value = routingRule.RoutingRuleLinesList[i].PrimaryRouting;
                                    ALTERNATE_ROUTING_1.Value = routingRule.RoutingRuleLinesList[i].AlternateRouting1;
                                    ALTERNATE_ROUTING_2.Value = routingRule.RoutingRuleLinesList[i].AlternateRouting2;
                                    if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_StartDate))
                                    {
                                        ROUTING_EFFECTIVE_START_DATE.Value = DateTime.Parse(routingRule.RoutingRuleLinesList[i].Routing_StartDate);
                                    }
                                    if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_EndDate))
                                    {
                                        ROUTING_EFFECTIVE_END_DATE.Value = DateTime.Parse(routingRule.RoutingRuleLinesList[i].Routing_EndDate);
                                    }


                                    OracleParameter opSysSource = new OracleParameter("SYS_SOURCE", OracleDbType.Varchar2, ParameterDirection.Input);
                                    opSysSource.Value = "MossUI";
                                    oracleCommand.Parameters.Add(opSysSource);

                                    OracleParameter opSysCreatedBy = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                                    opSysCreatedBy.Value = updatedBy;
                                    oracleCommand.Parameters.Add(opSysCreatedBy);

                                    OracleParameter opSysLastModifiedBy = new OracleParameter("SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                                    opSysLastModifiedBy.Value = updatedBy;
                                    oracleCommand.Parameters.Add(opSysLastModifiedBy);

                                    OracleParameter opSysEntState = new OracleParameter("SYS_ENT_STATE", OracleDbType.Varchar2, ParameterDirection.Input);
                                    opSysEntState.Value = "Active";
                                    oracleCommand.Parameters.Add(opSysEntState);

                                    MFG_SPLIT_PERCENT = new OracleParameter("MFG_SITE_SPLIT_PERCENT", OracleDbType.Varchar2, ParameterDirection.Input);
                                    MFG_SPLIT_PERCENT.Value = routingRule.RoutingRuleLinesList[i].MfgSplitPercent;
                                    oracleCommand.Parameters.Add(MFG_SPLIT_PERCENT);

                                    intInsertedRows = (int)oracleCommand.ExecuteNonQuery();

                                }
                            }

                            //Split is not getting added to Audit as of now.
                            StringBuilder sbAudRRLDelete1 = new StringBuilder();
                            sbAudRRLDelete1.Append(" INSERT  INTO AUD_ROUTING_RULE_LINES ( TRANSACTION_ID ,  TRANSACTION_ACTION , ROUTING_ID_NV ,   FULFILLMENT_SITE_ID_NV ,   MANUFACTURING_SITE_ID_NV ,   PRIMARY_ROUTING_NV ,");
                            sbAudRRLDelete1.Append("  ALTERNATE_ROUTING_1_NV ,   ALTERNATE_ROUTING_2_NV,   EFFECTIVE_START_DATE_NV ,   EFFECTIVE_END_DATE_NV,  SYS_LAST_MODIFIED_BY_NV, SYS_LAST_MODIFIED_DATE_NV   )");
                            sbAudRRLDelete1.Append(" SELECT SEQ_TRANSACTION_ID.NEXTVAL ,'INSERT', ROUTING_ID,FULFILLMENT_SITE_ID,MANUFACTURING_SITE_ID,PRIMARY_ROUTING, ALTERNATE_ROUTING_1, ALTERNATE_ROUTING_2,");
                            sbAudRRLDelete1.Append("  ROUTING_EFFECTIVE_START_DATE,ROUTING_EFFECTIVE_END_DATE,SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE from ROUTING_RULE_LINES ");
                            sbAudRRLDelete1.Append("where ROUTING_ID='" + strRoutingId + "'");
                            using (OracleCommand cmdAudRRLInsert = new OracleCommand(sbAudRRLDelete1.ToString(), oracleConnection))
                            {
                                intInsertedRows = (int)cmdAudRRLInsert.ExecuteNonQuery();
                            }
                        } //Else ends here.
                        //}
                    }
                    //if header record exists...
                    else
                    {
                        string strSelectRRL = "SELECT ID FROM ROUTING_RULE_LINES WHERE ROUTING_ID=:ROUTING_ID";//'" + dt.Rows[0]["ROUTING_ID"].ToString() + "'";
                        using (OracleCommand cmd = new OracleCommand(strSelectRRL.ToString(), oracleConnection))
                        {
                            OracleParameter opRoutingID = new OracleParameter("ROUTING_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                            opRoutingID.Value = dt.Rows[0]["ROUTING_ID"].ToString();
                            cmd.Parameters.Add(opRoutingID);
                            OracleDataReader oracleDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                            while (oracleDataReader.Read())
                            {
                                dictOldData.Add(oracleDataReader["ID"].ToString(), oracleDataReader["ID"].ToString());
                            }
                        }
                        //if child record exist add/update/delete
                        //if (dictOldData.Count > 0)
                        //{
                        for (int i = 0; i < intNumberOfRows; i++)
                        {
                            bool isRecordExist = dictOldData.ContainsKey(routingRule.RoutingRuleLinesList[i].ID);
                            //If the ID already exists,then proceed accordingly based on the STATUS value.
                            if (isRecordExist)
                            {
                                dictOldData.Remove(routingRule.RoutingRuleLinesList[i].ID);
                                if (routingRule.RoutingRuleLinesList[i].Status == "UPDATE" || routingRule.RoutingRuleLinesList[i].Status == "INSERT")
                                {
                                    string strSelectReord = "SELECT * FROM ROUTING_RULE_LINES WHERE ID='" + routingRule.RoutingRuleLinesList[i].ID + "'";
                                    DataTable dtData = new DataTable();
                                    OracleDataAdapter adapter = new OracleDataAdapter(strSelectReord, oracleConnection);
                                    adapter.Fill(dtData);

                                    StringBuilder sbAudRRLUpdate = new StringBuilder();
                                    sbAudRRLUpdate.Append(" INSERT  INTO AUD_ROUTING_RULE_LINES ( TRANSACTION_ID ,  TRANSACTION_ACTION , ROUTING_ID_OV,ROUTING_ID_NV ,   FULFILLMENT_SITE_ID_OV,FULFILLMENT_SITE_ID_NV ,   MANUFACTURING_SITE_ID_OV,MANUFACTURING_SITE_ID_NV ,   PRIMARY_ROUTING_OV,PRIMARY_ROUTING_NV ,");
                                    sbAudRRLUpdate.Append("  ALTERNATE_ROUTING_1_OV ,ALTERNATE_ROUTING_1_NV,   ALTERNATE_ROUTING_2_OV,ALTERNATE_ROUTING_2_NV,   EFFECTIVE_START_DATE_OV ,EFFECTIVE_START_DATE_NV,   EFFECTIVE_END_DATE_OV,EFFECTIVE_END_DATE_NV,  SYS_LAST_MODIFIED_BY_OV,SYS_LAST_MODIFIED_BY_NV, SYS_LAST_MODIFIED_DATE_OV,SYS_LAST_MODIFIED_DATE_NV   )");
                                    sbAudRRLUpdate.Append("values(");
                                    sbAudRRLUpdate.Append("SEQ_TRANSACTION_ID.NEXTVAL,");
                                    sbAudRRLUpdate.Append("'UPDATE',");
                                    sbAudRRLUpdate.Append("'" + dtData.Rows[0]["ROUTING_ID"].ToString() + "',");
                                    sbAudRRLUpdate.Append("'" + dtData.Rows[0]["ROUTING_ID"].ToString() + "',");
                                    sbAudRRLUpdate.Append("'" + dtData.Rows[0]["FULFILLMENT_SITE_ID"].ToString() + "',");
                                    sbAudRRLUpdate.Append("'" + routingRule.RoutingRuleLinesList[i].FulfillmentSite + "',");
                                    sbAudRRLUpdate.Append("'" + dtData.Rows[0]["MANUFACTURING_SITE_ID"].ToString() + "',");
                                    sbAudRRLUpdate.Append("'" + routingRule.RoutingRuleLinesList[i].ManufacturingSite + "',");
                                    sbAudRRLUpdate.Append("'" + dtData.Rows[0]["PRIMARY_ROUTING"].ToString() + "',");
                                    sbAudRRLUpdate.Append("'" + routingRule.RoutingRuleLinesList[i].PrimaryRouting + "',");
                                    sbAudRRLUpdate.Append("'" + dtData.Rows[0]["ALTERNATE_ROUTING_1"].ToString() + "',");
                                    sbAudRRLUpdate.Append("'" + routingRule.RoutingRuleLinesList[i].AlternateRouting1 + "',");
                                    sbAudRRLUpdate.Append("'" + dtData.Rows[0]["ALTERNATE_ROUTING_2"].ToString() + "',");
                                    sbAudRRLUpdate.Append("'" + routingRule.RoutingRuleLinesList[i].AlternateRouting2 + "',");

                                    //if (!string.IsNullOrEmpty(dtData.Rows[0]["ROUTING_EFFECTIVE_START_DATE"].ToString()))
                                    if (dtData.Rows[0]["ROUTING_EFFECTIVE_START_DATE"].ToString() != null && dtData.Rows[0]["ROUTING_EFFECTIVE_START_DATE"].ToString() != string.Empty && dtData.Rows[0]["ROUTING_EFFECTIVE_START_DATE"].ToString() != " ")
                                    {
                                        sbAudRRLUpdate.Append(" to_date('" + dtData.Rows[0]["ROUTING_EFFECTIVE_START_DATE"].ToString() + "','mm/dd/yyyy:hh:mi:ssam'), ");
                                    }
                                    else
                                    {
                                        sbAudRRLUpdate.Append(" null ,");
                                    }
                                    //if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_StartDate))
                                    if (routingRule.RoutingRuleLinesList[i].Routing_StartDate != null && routingRule.RoutingRuleLinesList[i].Routing_StartDate != string.Empty && routingRule.RoutingRuleLinesList[i].Routing_StartDate != " ")
                                    {
                                        sbAudRRLUpdate.Append(" to_date('" + routingRule.RoutingRuleLinesList[i].Routing_StartDate + "','mm/dd/yyyy:hh:mi:ssam'), ");
                                    }
                                    else
                                    {
                                        sbAudRRLUpdate.Append(" null ,");
                                    }

                                    //if (!string.IsNullOrEmpty(dtData.Rows[0]["ROUTING_EFFECTIVE_END_DATE"].ToString()))
                                    if (dtData.Rows[0]["ROUTING_EFFECTIVE_END_DATE"].ToString() != null && dtData.Rows[0]["ROUTING_EFFECTIVE_END_DATE"].ToString() != string.Empty && dtData.Rows[0]["ROUTING_EFFECTIVE_END_DATE"].ToString() != " ")
                                    {
                                        sbAudRRLUpdate.Append(" to_date('" + DateTime.Parse(dtData.Rows[0]["ROUTING_EFFECTIVE_END_DATE"].ToString()) + "','mm/dd/yyyy:hh:mi:ssam'), ");
                                    }
                                    else
                                    {
                                        sbAudRRLUpdate.Append(" null ,");
                                    }

                                    //if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_EndDate))
                                    if (routingRule.RoutingRuleLinesList[i].Routing_EndDate != null && routingRule.RoutingRuleLinesList[i].Routing_EndDate != string.Empty && routingRule.RoutingRuleLinesList[i].Routing_EndDate != " ")
                                    {
                                        sbAudRRLUpdate.Append(" to_date('" + routingRule.RoutingRuleLinesList[i].Routing_EndDate + "','mm/dd/yyyy:hh:mi:ssam'), ");
                                    }
                                    else
                                    {
                                        sbAudRRLUpdate.Append(" null ,");
                                    }
                                    sbAudRRLUpdate.Append("'" + dtData.Rows[0]["SYS_LAST_MODIFIED_BY"].ToString() + "',");
                                    sbAudRRLUpdate.Append("'" + updatedBy + "',");
                                    sbAudRRLUpdate.Append(" to_date('" + dtData.Rows[0]["SYS_LAST_MODIFIED_DATE"].ToString() + "','mm/dd/yyyy:hh:mi:ssam'), ");
                                    sbAudRRLUpdate.Append(" SYSDATE)");
                                    using (OracleCommand cmdAudRRLInsert = new OracleCommand(sbAudRRLUpdate.ToString(), oracleConnection))
                                    {
                                        intInsertedRows = (int)cmdAudRRLInsert.ExecuteNonQuery();
                                        // need to modify to update Effective date
                                    }

                                    //code is required to update 
                                    string sqlUpdate = "UPDATE ROUTING_RULE_LINES SET FULFILLMENT_SITE_ID=:FULFILLMENT_SITE_ID,MANUFACTURING_SITE_ID=:MANUFACTURING_SITE_ID,PRIMARY_ROUTING=:PRIMARY_ROUTING, " +
                                         "ALTERNATE_ROUTING_1=:ALTERNATE_ROUTING_1,ALTERNATE_ROUTING_2=:ALTERNATE_ROUTING_2, ROUTING_EFFECTIVE_START_DATE=:ROUTING_EFFECTIVE_START_DATE,ROUTING_EFFECTIVE_END_DATE=:ROUTING_EFFECTIVE_END_DATE, " +
                                         "SYS_LAST_MODIFIED_BY=:SYS_LAST_MODIFIED_BY,SYS_LAST_MODIFIED_DATE=SYSDATE,MFG_SITE_SPLIT_PERCENT=:MFG_SITE_SPLIT_PERCENT where ID=:ID";
                                    using (OracleCommand oracleCommand = new OracleCommand(sqlUpdate, oracleConnection))
                                    {
                                        FULFILLMENT_SITE_ID = new OracleParameter("FULFILLMENT_SITE_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                        MANUFACTURING_SITE_ID = new OracleParameter("MANUFACTURING_SITE_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                        PRIMARY_ROUTING = new OracleParameter("PRIMARY_ROUTING", OracleDbType.Varchar2, ParameterDirection.Input);
                                        ALTERNATE_ROUTING_1 = new OracleParameter("ALTERNATE_ROUTING_1", OracleDbType.Varchar2, ParameterDirection.Input);
                                        ALTERNATE_ROUTING_2 = new OracleParameter("ALTERNATE_ROUTING_2", OracleDbType.Varchar2, ParameterDirection.Input);
                                        ROUTING_EFFECTIVE_START_DATE = new OracleParameter("ROUTING_EFFECTIVE_START_DATE", OracleDbType.Date, ParameterDirection.Input);
                                        ROUTING_EFFECTIVE_END_DATE = new OracleParameter("ROUTING_EFFECTIVE_END_DATE", OracleDbType.Date, ParameterDirection.Input);
                                        MFG_SPLIT_PERCENT = new OracleParameter("MFG_SITE_SPLIT_PERCENT", OracleDbType.Varchar2, ParameterDirection.Input);
                                        ID = new OracleParameter("ID", OracleDbType.Varchar2, ParameterDirection.Input);

                                        FULFILLMENT_SITE_ID.Value = routingRule.RoutingRuleLinesList[i].FulfillmentSite;
                                        MANUFACTURING_SITE_ID.Value = routingRule.RoutingRuleLinesList[i].ManufacturingSite;
                                        PRIMARY_ROUTING.Value = routingRule.RoutingRuleLinesList[i].PrimaryRouting;
                                        ALTERNATE_ROUTING_1.Value = routingRule.RoutingRuleLinesList[i].AlternateRouting1;
                                        ALTERNATE_ROUTING_2.Value = routingRule.RoutingRuleLinesList[i].AlternateRouting2;

                                        //if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_StartDate))
                                        if (routingRule.RoutingRuleLinesList[i].Routing_StartDate != null && routingRule.RoutingRuleLinesList[i].Routing_StartDate != string.Empty && routingRule.RoutingRuleLinesList[i].Routing_StartDate != " ")
                                        {
                                            ROUTING_EFFECTIVE_START_DATE.Value = DateTime.Parse(routingRule.RoutingRuleLinesList[i].Routing_StartDate);
                                        }
                                        //if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_EndDate))
                                        if (routingRule.RoutingRuleLinesList[i].Routing_EndDate != null && routingRule.RoutingRuleLinesList[i].Routing_EndDate != string.Empty && routingRule.RoutingRuleLinesList[i].Routing_EndDate != " ")
                                        {
                                            ROUTING_EFFECTIVE_END_DATE.Value = DateTime.Parse(routingRule.RoutingRuleLinesList[i].Routing_EndDate);
                                        }
                                        MFG_SPLIT_PERCENT.Value = routingRule.RoutingRuleLinesList[i].MfgSplitPercent;

                                        ID.Value = routingRule.RoutingRuleLinesList[i].ID;

                                        oracleCommand.Parameters.Add(FULFILLMENT_SITE_ID);
                                        oracleCommand.Parameters.Add(MANUFACTURING_SITE_ID);
                                        oracleCommand.Parameters.Add(PRIMARY_ROUTING);
                                        oracleCommand.Parameters.Add(ALTERNATE_ROUTING_1);
                                        oracleCommand.Parameters.Add(ALTERNATE_ROUTING_2);
                                        oracleCommand.Parameters.Add(ROUTING_EFFECTIVE_START_DATE);
                                        oracleCommand.Parameters.Add(ROUTING_EFFECTIVE_END_DATE);


                                        OracleParameter opSysLastModifiedBy = new OracleParameter("SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                                        opSysLastModifiedBy.Value = updatedBy;
                                        oracleCommand.Parameters.Add(opSysLastModifiedBy);
                                        oracleCommand.Parameters.Add(MFG_SPLIT_PERCENT);
                                        oracleCommand.Parameters.Add(ID);
                                       
                                        intInsertedRows = (int)oracleCommand.ExecuteNonQuery();
                                    }
                                }

                            }
                            else
                            {
                                //Insert only if the Routind ID matches.

                                if (dt.Rows[0]["ROUTING_ID"].ToString() == routingRule.RoutingRuleLinesList[i].RoutingId.ToString() || routingRule.RoutingRuleLinesList[i].RoutingId.ToString() == "")
                                {
                                    insertSql = "INSERT INTO ROUTING_RULE_LINES(ID, ROUTING_ID, FULFILLMENT_SITE_ID, MANUFACTURING_SITE_ID,PRIMARY_ROUTING,ALTERNATE_ROUTING_1, ALTERNATE_ROUTING_2, ROUTING_EFFECTIVE_START_DATE, ROUTING_EFFECTIVE_END_DATE, SYS_SOURCE,SYS_CREATED_BY,SYS_CREATION_DATE,SYS_LAST_MODIFIED_BY,SYS_LAST_MODIFIED_DATE,SYS_ENT_STATE,MFG_SITE_SPLIT_PERCENT) "
                                                            + "VALUES (SEQ_TRANSACTION_ID.NEXTVAL,:ROUTING_ID, :FULFILLMENT_SITE_ID, :MANUFACTURING_SITE_ID,:PRIMARY_ROUTING,:ALTERNATE_ROUTING_1,:ALTERNATE_ROUTING_2,  :ROUTING_EFFECTIVE_START_DATE, :ROUTING_EFFECTIVE_END_DATE, :SYS_SOURCE,:SYS_CREATED_BY,SYSDATE,:SYS_LAST_MODIFIED_BY,SYSDATE,:SYS_ENT_STATE,:MFG_SITE_SPLIT_PERCENT) ";

                                    using (OracleCommand oracleCommand = new OracleCommand(insertSql, oracleConnection))
                                    {
                                        ROUTING_ID = new OracleParameter("ROUTING_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                        FULFILLMENT_SITE_ID = new OracleParameter("FULFILLMENT_SITE_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                        MANUFACTURING_SITE_ID = new OracleParameter("MANUFACTURING_SITE_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                        PRIMARY_ROUTING = new OracleParameter("PRIMARY_ROUTING", OracleDbType.Varchar2, ParameterDirection.Input);
                                        ALTERNATE_ROUTING_1 = new OracleParameter("ALTERNATE_ROUTING_1", OracleDbType.Varchar2, ParameterDirection.Input);
                                        ALTERNATE_ROUTING_2 = new OracleParameter("ALTERNATE_ROUTING_2", OracleDbType.Varchar2, ParameterDirection.Input);
                                        ROUTING_EFFECTIVE_START_DATE = new OracleParameter("ROUTING_EFFECTIVE_START_DATE", OracleDbType.Date, ParameterDirection.Input);
                                        ROUTING_EFFECTIVE_END_DATE = new OracleParameter("ROUTING_EFFECTIVE_END_DATE", OracleDbType.Date, ParameterDirection.Input);

                                        ROUTING_ID.Value = dt.Rows[0]["ROUTING_ID"].ToString();
                                        FULFILLMENT_SITE_ID.Value = routingRule.RoutingRuleLinesList[i].FulfillmentSite;
                                        MANUFACTURING_SITE_ID.Value = routingRule.RoutingRuleLinesList[i].ManufacturingSite;
                                        PRIMARY_ROUTING.Value = routingRule.RoutingRuleLinesList[i].PrimaryRouting;
                                        ALTERNATE_ROUTING_1.Value = routingRule.RoutingRuleLinesList[i].AlternateRouting1;
                                        ALTERNATE_ROUTING_2.Value = routingRule.RoutingRuleLinesList[i].AlternateRouting2;

                                        //if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_StartDate))
                                        if (routingRule.RoutingRuleLinesList[i].Routing_StartDate != null && routingRule.RoutingRuleLinesList[i].Routing_StartDate != string.Empty && routingRule.RoutingRuleLinesList[i].Routing_StartDate != " ")
                                        {
                                            ROUTING_EFFECTIVE_START_DATE.Value = DateTime.Parse(routingRule.RoutingRuleLinesList[i].Routing_StartDate);
                                        }

                                        //if (!string.IsNullOrEmpty(routingRule.RoutingRuleLinesList[i].Routing_EndDate))
                                        if (routingRule.RoutingRuleLinesList[i].Routing_EndDate != null && routingRule.RoutingRuleLinesList[i].Routing_EndDate != string.Empty && routingRule.RoutingRuleLinesList[i].Routing_EndDate != " ")
                                        {
                                            ROUTING_EFFECTIVE_END_DATE.Value = DateTime.Parse(routingRule.RoutingRuleLinesList[i].Routing_EndDate);
                                        }

                                        oracleCommand.Parameters.Add(ROUTING_ID);
                                        oracleCommand.Parameters.Add(FULFILLMENT_SITE_ID);
                                        oracleCommand.Parameters.Add(MANUFACTURING_SITE_ID);
                                        oracleCommand.Parameters.Add(PRIMARY_ROUTING);
                                        oracleCommand.Parameters.Add(ALTERNATE_ROUTING_1);
                                        oracleCommand.Parameters.Add(ALTERNATE_ROUTING_2);
                                        oracleCommand.Parameters.Add(ROUTING_EFFECTIVE_START_DATE);
                                        oracleCommand.Parameters.Add(ROUTING_EFFECTIVE_END_DATE);


                                        OracleParameter opSysSource = new OracleParameter("SYS_SOURCE", OracleDbType.Varchar2, ParameterDirection.Input);
                                        opSysSource.Value = "MossUI";
                                        oracleCommand.Parameters.Add(opSysSource);

                                        OracleParameter opSysCreatedBy = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                                        opSysCreatedBy.Value = updatedBy;
                                        oracleCommand.Parameters.Add(opSysCreatedBy);

                                        OracleParameter opSysLastModifiedBy = new OracleParameter("SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                                        opSysLastModifiedBy.Value = updatedBy;
                                        oracleCommand.Parameters.Add(opSysLastModifiedBy);

                                        OracleParameter opSysEntState = new OracleParameter("SYS_ENT_STATE", OracleDbType.Varchar2, ParameterDirection.Input);
                                        opSysEntState.Value = "Active";
                                        oracleCommand.Parameters.Add(opSysEntState);

                                        MFG_SPLIT_PERCENT = new OracleParameter("MFG_SITE_SPLIT_PERCENT", OracleDbType.Varchar2, ParameterDirection.Input);
                                        MFG_SPLIT_PERCENT.Value = routingRule.RoutingRuleLinesList[i].MfgSplitPercent;
                                        oracleCommand.Parameters.Add(MFG_SPLIT_PERCENT);

                                        intInsertedRows = (int)oracleCommand.ExecuteNonQuery();

                                    }

                                    int intID = 0;
                                    string sqlMaxID = "SELECT max(ID) FROM ROUTING_RULE_LINES where ROUTING_ID=:ROUTING_ID AND FULFILLMENT_SITE_ID=:FULFILLMENT_SITE_ID";
                                    using (OracleCommand oracleCommand = new OracleCommand(sqlMaxID, oracleConnection))
                                    {
                                        OracleParameter opRoutingID = new OracleParameter("ROUTING_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                        opRoutingID.Value = dt.Rows[0]["ROUTING_ID"].ToString();
                                        oracleCommand.Parameters.Add(opRoutingID);
                                        OracleParameter opFulfillmentSite = new OracleParameter("FULFILLMENT_SITE_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                        opFulfillmentSite.Value = routingRule.RoutingRuleLinesList[i].FulfillmentSite;
                                        oracleCommand.Parameters.Add(opFulfillmentSite);
                                        intID = Convert.ToInt32(oracleCommand.ExecuteScalar());
                                    }

                                    StringBuilder sbAudRRLDelete = new StringBuilder();
                                    sbAudRRLDelete.Append(" INSERT  INTO AUD_ROUTING_RULE_LINES ( TRANSACTION_ID ,  TRANSACTION_ACTION , ROUTING_ID_NV ,   FULFILLMENT_SITE_ID_NV ,   MANUFACTURING_SITE_ID_NV ,   PRIMARY_ROUTING_NV ,");
                                    sbAudRRLDelete.Append("  ALTERNATE_ROUTING_1_NV ,   ALTERNATE_ROUTING_2_NV,   EFFECTIVE_START_DATE_NV ,   EFFECTIVE_END_DATE_NV,  SYS_LAST_MODIFIED_BY_NV, SYS_LAST_MODIFIED_DATE_NV   )");
                                    sbAudRRLDelete.Append(" SELECT SEQ_TRANSACTION_ID.NEXTVAL ,'INSERT', ROUTING_ID,FULFILLMENT_SITE_ID,MANUFACTURING_SITE_ID,PRIMARY_ROUTING, ALTERNATE_ROUTING_1, ALTERNATE_ROUTING_2,");
                                    sbAudRRLDelete.Append("  ROUTING_EFFECTIVE_START_DATE,ROUTING_EFFECTIVE_END_DATE,SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE from ROUTING_RULE_LINES ");
                                    sbAudRRLDelete.Append(" where ID=:ID");
                                    //sbAudRRLDelete.Append(" where ROUTING_ID='" + dt.Rows[0]["ROUTING_ID"].ToString() + "' AND FULFILLMENT_SITE_ID='" + routingRule.RoutingRuleLinesList[i].FulfillmentSite + "'");
                                    using (OracleCommand cmdAudRRLInsert = new OracleCommand(sbAudRRLDelete.ToString(), oracleConnection))
                                    {
                                        OracleParameter opID = new OracleParameter("ID", OracleDbType.Varchar2, ParameterDirection.Input);
                                        opID.Value = intID;
                                        cmdAudRRLInsert.Parameters.Add(opID);
                                        intInsertedRows = (int)cmdAudRRLInsert.ExecuteNonQuery();
                                    }

                                } //Routing Id if ends here.

                            } //Else ends here
                        }
 
                    }

                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
            finally
            {
                if (oracleConnection != null)
                {
                    oracleConnection.Close();
                    oracleConnection.Dispose();
                }
            }
            return true;
        }


        public string CopyRoutingRule(string copyingRoutingId, string newRoutingId, string itemType, string itemId, string user)
        {



            string result;
            string sql = "SNOP_RP_UI_UTIL_PKG.prc_rr_rule_copy";
            OracleConnection conn = null;

            try
            {
                using (conn = new OracleConnection(connenctionString))
                {
                    conn.Open();
                    using (OracleCommand cmd = new OracleCommand(sql, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new OracleParameter("p_existing_routing_id", OracleDbType.Varchar2, 1024, copyingRoutingId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("p_new_routing_id", OracleDbType.Varchar2, 1024, newRoutingId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("p_product_id", OracleDbType.Varchar2, 1024, itemId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("p_product_type", OracleDbType.Varchar2, 1024, itemType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("p_created_by", OracleDbType.Varchar2, 1024, user, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("p_result", OracleDbType.Varchar2, 2000, string.Empty, ParameterDirection.Output));

                        // result = cmd.ExecuteScalar().ToString();
                        cmd.ExecuteNonQuery().ToString();

                        result = cmd.Parameters["p_result"].Value.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return result;

        }




    }
}


