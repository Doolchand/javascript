﻿using Dell.RP.Common.Models;
using Dell.RP.DataAccess.IRepository.ErrorLogging;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.DataAccess.Common;

namespace Dell.RP.DataAccess.Repository.ErrorLogging
{
    public class ErrorLoggingRepository : IErrorLoggingRepository
    {
        private readonly string connenctionString;
        private readonly string schemaName;

        public ErrorLoggingRepository(IOptions<AppSettings> appSettings, ICommonData commonData)
        {
            connenctionString = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringSCDH, appSettings.Value.ConnectionKey);
            schemaName = appSettings.Value.SchemaName;
        }
        public string UserID { get; set; }
        public async Task<DataTable> GetErrorList(string procName)
        {
            DataTable dtErrorList;
            return await Task.Run(() =>
            {
                List<ErrorImport> errorData = new List<ErrorImport>();
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();

                        var query = schemaName + ".GET_ERROR_INFO";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam = new OracleParameter("P_I_NT_USER_V", OracleDbType.Varchar2, ParameterDirection.Input);
                        objparam.Value = this.UserID;
                        objparam.Size = 300;
                        cmd.Parameters.Add(objparam);

                        OracleParameter objparam1 = new OracleParameter("P_I_PROGRAM_V", OracleDbType.Varchar2, ParameterDirection.Input);
                        objparam1.Value = procName;// "P_SET_IMPORT_PROD";
                        objparam1.Size = 300;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "P_O_ERROR_DTLS_C";
                        objparam2.OracleDbType = OracleDbType.RefCursor;
                        objparam2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam2);

                        OracleParameter errorNum = new OracleParameter("P_O_ERR_CODE_V", OracleDbType.Varchar2, ParameterDirection.Output);
                        errorNum.Size = 5000;
                        cmd.Parameters.Add(errorNum);

                        OracleParameter errorMsg = new OracleParameter("P_O_ERR_MSG_V", OracleDbType.Varchar2, ParameterDirection.Output);
                        errorMsg.Size = 5000;
                        cmd.Parameters.Add(errorMsg);

                        OracleDataReader readerErrorList = cmd.ExecuteReader();
                        var adtprErrorList = new OracleDataAdapter(cmd);
                        dtErrorList = new DataTable();
                        adtprErrorList.Fill(dtErrorList);
                        connection.Close();
                        
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return dtErrorList;
            });
        }
    }
}
