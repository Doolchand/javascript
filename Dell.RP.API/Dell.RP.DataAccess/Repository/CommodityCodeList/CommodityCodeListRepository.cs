﻿using Dell.RP.DataAccess;
using Microsoft.Extensions.Options;
using Dell.RP.API.Dell.RP.DataAccess.Common;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Dell.RP.API.Dell.RP.Common.Models.CommodityCodeListDetails;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.CommodityCodeList;

namespace Dell.RP.API.Dell.RP.DataAccess.Repository.CommodityCodeList
{
    public class CommodityCodeListRepository: ICommodityCodeListRepository
    {
        private readonly string connenctionString;
        public string UserID { get; set; }

        private readonly string schemaName;

        public CommodityCodeListRepository(IOptions<AppSettings> appSettings, ICommonData commonData)
        {
            connenctionString = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringGMP, appSettings.Value.ConnectionKey);
            schemaName = appSettings.Value.SchemaName;
        }

        public async Task<List<CommodityCodeListDetails>> GetFilterViewResult(CommodityCodeListDetails param)
        {
            //DataTable dt = new DataTable();
            List<CommodityCodeListDetails> CommodityCodeListDetails = new List<CommodityCodeListDetails>();
            return await Task.Run(() =>
            {
                try
                {
                    using (OracleConnection conn = new OracleConnection(this.connenctionString))
                    {
                        string sql = string.Format("SELECT * FROM ( "
                                                 + " SELECT ROWNUM AS RNUM, L.* FROM ( "
                                                 + "  SELECT COMMODITY_ID, COMMODITY_NAME, IS_ENABLED, SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE FROM "
                                                 + "  MST_COMMODITY_MASTER WHERE 1=1 "
                                                 + filterCrieteria(param)
                                                 //+ " ORDER BY IS_ENABLED DESC, COMMODITY_NAME "
                                                 + " ORDER BY COMMODITY_NAME ASC"
                                                 + "   ) L "
                                                 + "  ) ");
                                                // + "  WHERE RNUM > 0 * 20 and rnum <= (0 + 1) * 20");
                                                 //, new object[] { pageIndex, pageSize, filterCriteria });

                        using (OracleCommand cmd = new OracleCommand(sql, conn))
                        {
                            conn.Open();
                            //OracleDataAdapter da = new OracleDataAdapter(cmd);
                            //da.Fill(dt);
                            OracleDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                reader.FetchSize = reader.RowSize * 1000;
                            }
                            while (reader.Read())
                            {
                                CommodityCodeListDetails.Add(new CommodityCodeListDetails
                                {
                                    commodityId = reader["COMMODITY_ID"].ToString(),
                                    commodityName = reader["COMMODITY_NAME"].ToString(),
                                    markFlag = ((reader["IS_ENABLED"].ToString() == "Y") ? true : false),
                                    updatedBy = reader["SYS_LAST_MODIFIED_BY"].ToString(),
                                    updatedDate = reader["SYS_LAST_MODIFIED_DATE"].ToString(),
                                });
                            }
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return CommodityCodeListDetails;
            });
        }

        private string filterCrieteria(CommodityCodeListDetails param)
        {
            string filter = "";

            if (param.commodityId != "" && param.commodityName != "")
            {
                filter = " and I.ITEM_ID='" + param.commodityId +"'"+ " and DESCRIPTION='" + param.commodityName + "'";     
            }
            else if (param.commodityId != "")
            {
                filter = " and I.ITEM_ID='" + param.commodityId + "'";
            }
            else if (param.commodityName != "")
            {
                filter = " and DESCRIPTION='" + param.commodityName + "'";
            }
            else
            {
                filter = "";
            }
            return filter;
        }

        public async Task<string> UpdateCommodityCodeListDetails(List<CommodityCodeListDetails> CommodityCodeListDetails)
        {
            string returnMessage = "";
            return await Task.Run(() =>
            {
                try
                {
                    foreach(CommodityCodeListDetails CommodityCodeListDetail in CommodityCodeListDetails)
                    {
                        returnMessage = addMultiTierItem(CommodityCodeListDetail);
                        if (returnMessage.Split("~")[0] == "1")
                        {
                            break;
                        }
                    }

                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    returnMessage = 1 + "~" + "Records could not be saved";
                    
                }

                return returnMessage;
            });
        }

        public string addMultiTierItem(CommodityCodeListDetails commodityCodeListDetail)
        {
            int count = 0;
            string returnMessage = "";
            try
            {
                string isEnable = commodityCodeListDetail.markFlag == true ? "Y" : "N";
                string sqlUpdate = "UPDATE MST_COMMODITY_MASTER SET IS_ENABLED='" + isEnable + "',SYS_LAST_MODIFIED_BY='" + this.UserID + "',SYS_LAST_MODIFIED_DATE=SYSDATE WHERE COMMODITY_ID=" + Convert.ToInt32(commodityCodeListDetail.commodityId);

                using (OracleConnection conn = new OracleConnection(this.connenctionString))
                {
                    conn.Open();

                    using (OracleCommand cmdIns = new OracleCommand(sqlUpdate, conn))
                    {
                        //OracleParameter param1 = new OracleParameter("SYS_LAST_MODIFIED_BY", OracleDbType.Varchar2, ParameterDirection.Input);
                        //param1.Value = commodityCodeListDetail.updatedBy;
                        //cmdIns.Parameters.Add(param1);

                        //OracleParameter param2 = new OracleParameter("COMMODITY_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                        //param2.Value = commodityCodeListDetail.commodityId;
                        //cmdIns.Parameters.Add(param2);

                        //OracleParameter param3 = new OracleParameter("IS_ENABLED", OracleDbType.Varchar2, ParameterDirection.Input);
                        //param3.Value = commodityCodeListDetail.markFlag == true ? "Y":"N";
                        //cmdIns.Parameters.Add(param3);

                        count = (int)cmdIns.ExecuteNonQuery();
                    }

                    returnMessage = 0 + "~" + "SUCCESS";

                }

            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                returnMessage = 1 + "~" + "Records could not be saved";
                throw ex;
            }
            return returnMessage;
        }
    }
}
