﻿using Dell.RP.API.Dell.RP.Common.Models;
using Dell.RP.API.Dell.RP.Common.Models.SourcingRule;
using Dell.RP.API.Dell.RP.Common.Models.SourcingRule1;
using Dell.RP.API.Dell.RP.DataAccess.Common;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.SourcingRule1;
using Dell.RP.DataAccess;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.DataAccess.Repository.SourcingRule1
{
    public class SourcingRuleRepository : ISourcingRuleRepository
    {

        #region Constructor & Local variables
        private readonly string connenctionString;
        private readonly string connectionStringRP;
        private readonly string connectionStringSCDH;
        private const string keyAppender = "_";

        public SourcingRuleRepository(IOptions<AppSettings> appSettings, ICommonData commonData)
        {
            connenctionString = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringGMP, appSettings.Value.ConnectionKey);
            connectionStringSCDH = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringSCDH, appSettings.Value.ConnectionKey);
            connectionStringRP = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringSCDH, appSettings.Value.ConnectionKey);
        }
        public string UserID { get; set; }

        #endregion

        #region Search

        public async Task<List<DropDown>> GetChannelProducts()
        {
            //List<string> accountname = new List<string>();
            List<DropDown> lstChannelProducts = new List<DropDown>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();
                        string sql = " select distinct PRODUCT_ID, product_name, CHANNEL_ID, channel_name from ("
                                    + " select sd.PRODUCT_ID, ( select pm.name from mst_product_master pm where pm.product_id = SD.product_id) product_name, sd.CHANNEL_ID, "
                                    + " ( select name from mst_gmp_dell_channels where channel_id = SD.channel_id) channel_name  "
                                    + " from MST_GMP_SOURCING_TEMPLATE ST, MST_GMP_SOURCING_DETAILS SD "
                                    + " where ST.route_id = SD.route_id  "
                                    + " and CHANNEL_ID not like '<%' "
                                    + " connect by prior ST.route_id = ST.parent_route_id  "
                                    + " start with ST.from_type='KITTING'"
                                    + ") order by channel_name, product_name";

                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();

                        lstChannelProducts.Add(new DropDown { Id = "0", ItemName = "--SELECT--" });
                        while (reader.Read())
                        {
                            lstChannelProducts.Add(new DropDown
                            {
                                ItemName = string.Format("{0}({1})->{2}({3})", new object[] { reader["channel_name"].ToString(), reader["CHANNEL_ID"].ToString(), reader["product_name"].ToString(), reader["PRODUCT_ID"].ToString() }),
                                Id = reader["CHANNEL_ID"].ToString() + ":" + reader["PRODUCT_ID"].ToString()
                            });
                        }
                        connection.Close();
                    }

                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return lstChannelProducts;
            });
        }

        public async Task<List<DropDown>> GetChannelGeo()
        {
            //List<string> accountname = new List<string>();
            List<DropDown> lstChannelGeo = new List<DropDown>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();
                        string sql = "select  channel_id, name, parent_id from mst_gmp_dell_channels where  is_active = '1' " +
                        "and (parent_id = '1864'  or   channel_id = '1864') order by parent_id desc, name";

                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            lstChannelGeo.Add(new DropDown
                            {
                                Id = reader["channel_id"].ToString(),
                                ItemName = reader["name"].ToString()
                            });
                        }
                        connection.Close();
                    }

                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return lstChannelGeo;
            });
        }

        public async Task<List<Site>> GetSites()
        {
            //List<string> accountname = new List<string>();
            List<Site> lstSite = new List<Site>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connectionStringSCDH))
                    {
                        connection.Open();
                        string sql = "select distinct b.location_group_id, c.name, substr(c.name,0,10) as site_id, b.location_group_type from mst_location_ccn a," +
                        " mst_location_group_detail b, mst_location c where b.location_id = a.facility and b.location_type = 'FACILITY' and a.ccn " +
                        "in ( select ccn from FDL_SNOP_SCDHUB.x_location_ccn_glovia_xref where forecast_instance_id in ('1','2','3','4')  ) " +
                        "and b.location_group_id = c.location_id  order by c.name";

                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            lstSite.Add(new Site
                            {
                                SiteId = reader["site_id"].ToString(),
                                SiteName = reader["name"].ToString(),
                                LocationGroupId = reader["location_group_id"].ToString(),
                                LocationGroupType = reader["location_group_type"].ToString(),
                            });
                        }
                        connection.Close();
                    }

                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return lstSite;
            });
        }

        public async Task<List<ProductTree>> GetProductTreeData(string strString)
        {
            List<ProductTree> lstProdTreeData = new List<ProductTree>();
            return await Task.Run(() =>
            {
                try
                {
                    using (var connection = new OracleConnection(connectionStringSCDH))
                    {
                        connection.Open();
                        string sql = "select distinct LOB_NAME,LOB_CODE,FMLY_PARNT_NAME,FMLY_PARNT_CODE,BASE_PROD_DESC,BASE_PROD_CODE from plan_product_Flat_Vw where LOB_CODE like '%" + strString + "%' or LOB_NAME like '%" + strString + "%'";
                        OracleCommand cmd = new OracleCommand(sql, connection);

                        OracleDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            lstProdTreeData.Add(new ProductTree
                            {
                                AllProduct = "ALL_PRODUCT",
                                AllProductId = "0",
                                LOBProd = Convert.ToString(reader["LOB_NAME"]),
                                LOBProdId = Convert.ToString(reader["LOB_CODE"]),
                                FamilyParent = Convert.ToString(reader["FMLY_PARNT_NAME"]),
                                FamilyParentId = Convert.ToString(reader["FMLY_PARNT_CODE"]),
                                BaseProd = Convert.ToString(reader["BASE_PROD_DESC"]),
                                BaseProdId = Convert.ToString(reader["BASE_PROD_CODE"])
                            });
                        }
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return lstProdTreeData;
            });
        }

        public async Task<List<SourcingRuleGrid>> GetSourcingRuleDetails(SourcingRuleFilter sourcingRuleFilter)
        {
            List<SourcingRuleGrid> sourcingRuleGrid = new List<SourcingRuleGrid>();
            return await Task.Run(() =>
            {
                try
                {
                    using (OracleConnection conn = new OracleConnection(this.connenctionString))
                    {
                        string sql = " select PRODUCT_ID, ( select name from mst_product_master where product_id = SD.product_id) product_name, CHANNEL_ID,  " +
                        "(select name from mst_gmp_dell_channels where channel_id = SD.channel_id) channel_name,  from_cal_id, " +
                        "(select START_DATE from MST_GMP_DELL_CALENDAR where CALENDAR_ID = SD.from_cal_id) fromDate,   to_cal_id, (select START_DATE from " +
                        "MST_GMP_DELL_CALENDAR where CALENDAR_ID = SD.to_cal_id) toDate, split,     connect_by_root ST.from_location_id parent_site, ST.route_id, " +
                        "ST.parent_route_id, ST.from_location_id, ST.from_type, ST.to_location_id, ST.to_type  from MST_GMP_SOURCING_TEMPLATE ST, " +
                        "MST_GMP_SOURCING_DETAILS SD where ST.route_id = SD.route_id and CHANNEL_ID not like '<%' " +
                        // "-- searchCriteria" +
                        " connect by prior ST.route_id" +
                        " = ST.parent_route_id  start with ST.from_type = 'KITTING'  order siblings by channel_id, product_id, ST.from_location_id";

                        using (OracleCommand cmd = new OracleCommand(sql, conn))
                        {
                            conn.Open();
                            OracleDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                reader.FetchSize = reader.RowSize * 30000;
                            }
                            while (reader.Read())
                            {
                                sourcingRuleGrid.Add(new SourcingRuleGrid
                                {
                                    //ProductId = reader["ITEM_ID"].ToString(),
                                    //ProductName = reader["ITEM_ID"].ToString(),
                                    //ChannelId = reader["ITEM_ID"].ToString(),
                                    //ChannelName = reader["ITEM_ID"].ToString(),
                                    //itemDescription = reader["DESCRIPTION"].ToString(),
                                    //commodityCode = reader["COMMODITY_NAME"].ToString(),
                                    //multiTier = ((reader["IS_ENABLED"].ToString() == "Y") ? true : false),
                                    //updatedBy = reader["SYS_LAST_MODIFIED_BY"].ToString(),
                                    //updatedDate = reader["SYS_LAST_MODIFIED_DATE"].ToString(),
                                });
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return sourcingRuleGrid;
            });

        }

        public async Task<List<SourcingRule>> GetAllSourcingRules()
        {
            List<SourcingRule> listSourcingRules = new List<SourcingRule>();
            DataSet ds = new DataSet();
            SourcingRule tempSourcingRule = null;
            RawRouting tempRawRouting = null;
            return await Task.Run(() =>
            {
                try
                {

                    using (OracleConnection oracleConnection = new OracleConnection(this.connenctionString))
                    {

                        string sqlSelect = string.Format(" select b.product_id, ( select distinct name from mst_product_master where product_id = b.product_id) product_name, b.channel_id, ( select distinct name from mst_gmp_dell_channels where channel_id = b.channel_id) channel_name, "
                   + "  b.from_cal_id, b.to_cal_id,  b.geography_id, a.route_id, nvl(a.parent_route_id,a.route_id) parent_route_id,  "
                   + "                 (select name from mst_location where location_id = a.from_location_id ) as from_location_id, "
                   + "         (select name from mst_location where location_id = a.to_location_id) as to_location_id, "
                   + "        a.from_type, a.to_type,  b.split "
                   + "                  from mst_gmp_sourcing_template a, "
                   + "                     mst_gmp_sourcing_details b "
                   + "                  where a.route_id = b.route_id "
                   + "                  and b.product_id in (select item_id from  "
                + " ( select product_id item_id, product_group_id item_group_id "
                + "   from ( select product_id, decode(product_group_id, product_id, null, product_group_id) product_group_id from mst_product_group_detail a) "
                + "   connect by prior product_id = product_group_id "
               + " start with product_id = '575')) "
               + "                      and b.channel_id in (select channel_id from ( "
               + "                           select channel_id, name, description, parent_id, channel_level_id, forecast_instance_id "
               + "                                  from  "
               + "                                  ( select a.channel_id, a.name, a.description, decode(a.parent_id, a.channel_id, null, a.parent_id) parent_id, a.channel_level_id, b.forecast_instance_id  "
               + "                                       from mst_gmp_dell_channels a, "
               + "                                          mst_gmp_dell_channel_levels b  "
               + "                                         where a.channel_level_id = b.channel_level_id "
               + "                                         and  a.is_active = '1'  "
               + "                                    ) "
               + "                                  connect by prior channel_id = parent_id "
               + "                                  start with channel_id =  '1864') "
               + "                           where channel_id = '1864' or forecast_instance_id in  ('1','2','3','4')) "
               //+ "                       and product_id ='27978'                         "
               + "                      order by product_id, channel_id asc, from_cal_id asc nulls first, to_cal_id asc nulls first,  "
               + "                      to_number(substr(a.route_id,6)) ", new Object());

                        // '1','2','3','4'

                        using (OracleCommand oracleCommand = new OracleCommand(sqlSelect, oracleConnection))
                        {

                            oracleConnection.Open();

                            //OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(oracleCommand);

                            //oracleDataAdapter.Fill(ds);

                            OracleDataReader oracleDataReader = oracleCommand.ExecuteReader(CommandBehavior.CloseConnection);

                            string currentRouteId = String.Empty;

                            string previousRouteId = String.Empty;

                            string currentSourcingRuleId = String.Empty;



                            string previousSourcingRuleId = String.Empty;

                            while (oracleDataReader.Read())
                            {

                                currentRouteId = oracleDataReader["PARENT_ROUTE_ID"].ToString();


                                currentSourcingRuleId = oracleDataReader["CHANNEL_ID"].ToString() + "$" + oracleDataReader["PRODUCT_ID"].ToString() + "$" + oracleDataReader["FROM_CAL_ID"].ToString() + "$" + oracleDataReader["TO_CAL_ID"].ToString();


                                //if (oracleDataReader["FROM_TYPE"].Equals("KITTING"))
                                //    currentSourcingRuleId += oracleDataReader["FROM_LOCATION_ID"].ToString();

                                //if (!string.IsNullOrEmpty(currentRouteId) && !currentRouteId.Equals(previousRouteId))
                                if (!string.IsNullOrEmpty(currentSourcingRuleId) && !currentSourcingRuleId.Equals(previousSourcingRuleId))
                                {
                                    int fromCal = 0;

                                    int toCal = 0;

                                    if (!string.IsNullOrEmpty(oracleDataReader["FROM_CAL_ID"].ToString()))
                                    {
                                        fromCal = Int32.Parse(oracleDataReader["FROM_CAL_ID"].ToString());
                                    }

                                    if (!string.IsNullOrEmpty(oracleDataReader["TO_CAL_ID"].ToString()))
                                    {
                                        toCal = Int32.Parse(oracleDataReader["TO_CAL_ID"].ToString());
                                    }

                                    tempSourcingRule = new SourcingRule(oracleDataReader["CHANNEL_ID"].ToString(), oracleDataReader["PRODUCT_ID"].ToString(), fromCal, toCal);

                                    tempSourcingRule.SourcingRuleId = currentSourcingRuleId;

                                    listSourcingRules.Add(tempSourcingRule);

                                }




                                tempSourcingRule.addRawRouting(currentRouteId, new RawRouting(
                                        oracleDataReader["FROM_LOCATION_ID"].ToString(),
                                        oracleDataReader["TO_LOCATION_ID"].ToString(),
                                        double.Parse(oracleDataReader["SPLIT"].ToString()),
                                        oracleDataReader["ROUTE_ID"].ToString(), oracleDataReader["FROM_TYPE"].ToString()));

                                previousRouteId = currentRouteId;

                                previousSourcingRuleId = currentSourcingRuleId;
                            }

                        }

                    }

                    /* Start of Code to process the raw routings of each Sourcing Rules and convert them into Final Routings. */


                    IEnumerator<SourcingRule> iEnumeratorTemp = listSourcingRules.GetEnumerator();

                    while (iEnumeratorTemp.MoveNext())
                    {
                        iEnumeratorTemp.Current.processRawRoutings();
                    }


                    /* End of Code to process the raw routings of each Sourcing Rules and convert them into Final Routings. */

                    return listSourcingRules;

                }
                catch (Exception ex)
                {
                    // log.Fatal("Failed to get information from the database.", ex);
                    throw;
                }

            });
        }

        public async Task<List<SourcingRule>> GetSourcingRules(string parentProductSearchFilter, string parentChannelSearchFilter)
        {
            List<SourcingRule> listSourcingRules = new List<SourcingRule>();
            DataSet ds = new DataSet();

            SourcingRule tempSourcingRule = null;

            RawRouting tempRawRouting = null;
            return await Task.Run(() =>
            {
                try
                {

                    using (OracleConnection oracleConnection = new OracleConnection(this.connenctionString))
                    {

                        string sqlSelect = string.Format(" select b.product_id, ( select distinct name from mst_product_master where product_id = b.product_id) product_name, b.channel_id, ( select distinct name from mst_gmp_dell_channels where channel_id = b.channel_id) channel_name, "
                   + "  b.from_cal_id, b.to_cal_id,  b.geography_id, a.route_id, nvl(a.parent_route_id,a.route_id) parent_route_id,  "
                   + "                 (select name from mst_location where location_id = a.from_location_id ) as from_location_id, "
                   + "         (select name from mst_location where location_id = a.to_location_id) as to_location_id, "
                   + "        a.from_type, a.to_type,  b.split "
                   + "                  from mst_gmp_sourcing_template a, "
                   + "                     mst_gmp_sourcing_details b "
                   + "                  where a.route_id = b.route_id "
                   + "                  and b.product_id in (select item_id from  "
                + " ( select product_id item_id, product_group_id item_group_id "
                + "   from ( select product_id, decode(product_group_id, product_id, null, product_group_id) product_group_id from mst_product_group_detail a) "
                + "   connect by prior product_id = product_group_id "
               //+ " start with product_id = '575')) "
               + " start with {0})) "
               + "                      and b.channel_id in (select channel_id from ( "
               + "                           select channel_id, name, description, parent_id, channel_level_id, forecast_instance_id "
               + "                                  from  "
               + "                                  ( select a.channel_id, a.name, a.description, decode(a.parent_id, a.channel_id, null, a.parent_id) parent_id, a.channel_level_id, b.forecast_instance_id  "
               + "                                       from mst_gmp_dell_channels a, "
               + "                                          mst_gmp_dell_channel_levels b  "
               + "                                         where a.channel_level_id = b.channel_level_id "
               + "                                         and  a.is_active = '1'  "
               + "                                    ) "
               + "                                  connect by prior channel_id = parent_id "
               //+ "                                  start with channel_id =  '1864') "
               + "                                  start with {1}) "
               //+ "                           where channel_id = '1864' or forecast_instance_id in  ('1','2','3','4')) "
               + "                           where {1} or forecast_instance_id in  ('1','2','3','4')) "
               //+ "                       and product_id ='27978'                         "
               + "                      order by product_id, channel_id asc, from_cal_id asc nulls first, to_cal_id asc nulls first,  "
               + "                      to_number(substr(a.route_id,6)) ", new Object[] { parentProductSearchFilter, parentChannelSearchFilter });

                        // '1','2','3','4'

                        using (OracleCommand oracleCommand = new OracleCommand(sqlSelect, oracleConnection))
                        {

                            oracleConnection.Open();

                            //OracleDataAdapter oracleDataAdapter = new OracleDataAdapter(oracleCommand);

                            //oracleDataAdapter.Fill(ds);

                            OracleDataReader oracleDataReader = oracleCommand.ExecuteReader(CommandBehavior.CloseConnection);

                            string currentRouteId = String.Empty;

                            string previousRouteId = String.Empty;

                            string currentSourcingRuleId = String.Empty;



                            string previousSourcingRuleId = String.Empty;

                            while (oracleDataReader.Read())
                            {

                                currentRouteId = oracleDataReader["PARENT_ROUTE_ID"].ToString();


                                currentSourcingRuleId = oracleDataReader["CHANNEL_ID"].ToString() + "$" + oracleDataReader["PRODUCT_ID"].ToString() + "$" + oracleDataReader["FROM_CAL_ID"].ToString() + "$" + oracleDataReader["TO_CAL_ID"].ToString();


                                //if (oracleDataReader["FROM_TYPE"].Equals("KITTING"))
                                //    currentSourcingRuleId += oracleDataReader["FROM_LOCATION_ID"].ToString();

                                //if (!string.IsNullOrEmpty(currentRouteId) && !currentRouteId.Equals(previousRouteId))
                                if (!string.IsNullOrEmpty(currentSourcingRuleId) && !currentSourcingRuleId.Equals(previousSourcingRuleId))
                                {
                                    int fromCal = 0;

                                    int toCal = 0;

                                    if (!string.IsNullOrEmpty(oracleDataReader["FROM_CAL_ID"].ToString()))
                                    {
                                        fromCal = Int32.Parse(oracleDataReader["FROM_CAL_ID"].ToString());
                                    }

                                    if (!string.IsNullOrEmpty(oracleDataReader["TO_CAL_ID"].ToString()))
                                    {
                                        toCal = Int32.Parse(oracleDataReader["TO_CAL_ID"].ToString());
                                    }

                                    tempSourcingRule = new SourcingRule(oracleDataReader["CHANNEL_ID"].ToString(), oracleDataReader["PRODUCT_ID"].ToString(), fromCal, toCal);

                                    tempSourcingRule.SourcingRuleId = currentSourcingRuleId;

                                    listSourcingRules.Add(tempSourcingRule);

                                }




                                tempSourcingRule.addRawRouting(currentRouteId, new RawRouting(
                                        oracleDataReader["FROM_LOCATION_ID"].ToString(),
                                        oracleDataReader["TO_LOCATION_ID"].ToString(),
                                        double.Parse(oracleDataReader["SPLIT"].ToString()),
                                        oracleDataReader["ROUTE_ID"].ToString(), oracleDataReader["FROM_TYPE"].ToString()));

                                previousRouteId = currentRouteId;

                                previousSourcingRuleId = currentSourcingRuleId;
                            }

                        }

                    }

                    /* Start of Code to process the raw routings of each Sourcing Rules and convert them into Final Routings. */


                    IEnumerator<SourcingRule> iEnumeratorTemp = listSourcingRules.GetEnumerator();

                    while (iEnumeratorTemp.MoveNext())
                    {
                        iEnumeratorTemp.Current.processRawRoutings();
                    }


                    /* End of Code to process the raw routings of each Sourcing Rules and convert them into Final Routings. */

                    return listSourcingRules;

                }
                catch (Exception ex)
                {
                    //log.Fatal("Failed to get information from the database.", ex);
                    throw;
                }
            });

        }

        #endregion

        public async Task<int> DeleteSourcingRuleCollection(string productId, string channelId)
        {
            int num = 0;
            return await Task.Run(() => { 
            try
            {
                string str = " select distinct route_id   from MST_GMP_SOURCING_DETAILS  where PRODUCT_ID=:PRODUCT_ID AND CHANNEL_ID=:CHANNEL_ID ";
                List<string> strs = new List<string>();
                using (OracleConnection connection = new OracleConnection(this.connenctionString))
                {
                    OracleCommand oracleCommand = new OracleCommand(str, connection);
                    try
                    {
                        connection.Open();
                        OracleParameter oracleParameter = new OracleParameter("PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input)
                        {
                            Value = productId
                        };
                        oracleCommand.Parameters.Add(oracleParameter);
                        OracleParameter oracleParameter1 = new OracleParameter("CHANNEL_ID", OracleDbType.Varchar2, ParameterDirection.Input)
                        {
                            Value = channelId
                        };
                        oracleCommand.Parameters.Add(oracleParameter1);
                        OracleDataReader oracleDataReader = oracleCommand.ExecuteReader();
                        while (oracleDataReader.Read())
                        {
                            strs.Add(oracleDataReader["route_id"].ToString());
                        }
                    }
                    finally
                    {
                        if (oracleCommand != null)
                        {
                            oracleCommand.Dispose();
                        }
                    }
                    foreach (string str1 in strs)
                    {
                        num += this.DeleteSourcingRule(str1);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
                return num;
            });
        }


        public int DeleteSourcingRule(string RouteId)
        {
            OracleParameter oracleParameter;
            int num = 0;
            //return await Task.Run(() =>
            //{
                try
                {
                    using (OracleConnection connection = new OracleConnection(this.connenctionString))
                    {
                        string str = "DELETE MST_GMP_SOURCING_TEMPLATE WHERE ROUTE_ID=:ROUTE_ID ";
                        OracleCommand oracleCommand = new OracleCommand("DELETE MST_GMP_SOURCING_DETAILS WHERE ROUTE_ID=:ROUTE_ID ", connection);
                        try
                        {
                        connection.Open();
                        oracleParameter = new OracleParameter("ROUTE_ID", OracleDbType.Varchar2, ParameterDirection.Input)
                            {
                                Value = RouteId
                            };
                            oracleCommand.Parameters.Add(oracleParameter);
                            num += oracleCommand.ExecuteNonQuery();
                            if (num == 0)
                            {
                                // SourcingRulesDataSource.log.Fatal(string.Concat("Failed to Delete Sourcing Rule with Route ID [", RouteId, "] to database table."));
                                throw new Exception(string.Concat("Failed to Delete Sourcing Rule with Route ID [", RouteId, "] to database table."));
                            }
                        }
                        finally
                        {
                            if (oracleCommand != null)
                            {
                                oracleCommand.Dispose();
                            }
                        }
                        OracleCommand oracleCommand1 = new OracleCommand(str, connection);
                        try
                        {
                            oracleParameter = new OracleParameter("ROUTE_ID", OracleDbType.Varchar2, ParameterDirection.Input)
                            {
                                Value = RouteId
                            };
                            oracleCommand1.Parameters.Add(oracleParameter);
                            num += oracleCommand1.ExecuteNonQuery();
                            if (num == 0)
                            {
                                //SourcingRulesDataSource.log.Fatal(string.Concat("Failed to Delete Sourcing Rule with Route ID [", RouteId, "] to database table."));
                                throw new Exception(string.Concat("Failed to Delete Sourcing Rule with Route ID [", RouteId, "] to database table."));
                            }
                        }
                        finally
                        {
                            if (oracleCommand1 != null)
                            {
                                oracleCommand1.Dispose();
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
                return num;
            //});
        }





        public async Task<Dictionary<string, SourcingRuleLineItem>> GetSourcingRuleLineItems(string startChannelId, string startProductId)
        {
            SourcingRuleLineItem sourcingRuleLineItem;
            Dictionary<string, SourcingRuleLineItem> strs;
            object[] count;
            string[] strArrays;
            Dictionary<string, SourcingRuleLineItem> strs1 = new Dictionary<string, SourcingRuleLineItem>();
            Dictionary<string, SourcingRuleLineItem> strs2 = new Dictionary<string, SourcingRuleLineItem>();
            //Stopwatch stopwatch = new Stopwatch();
            //SourcingRulesDataSource.log.Info("GetSourcingRuleLineItems : Begin...");
            return await Task.Run(() =>
            {
                try
                {
                    try
                    {
                        // ConfigurationManager.AppSettings["ChannelActiveValue"].ToString();
                        OracleConnection oracleConnection = new OracleConnection(this.connenctionString);
                        try
                        {
                            string str = " select b.product_id, ( select distinct name from mst_product_master where product_id = b.product_id) product_name, b.channel_id, " +
                                "( select distinct name from mst_gmp_dell_channels where channel_id = b.channel_id) channel_name,  b.geography_id, a.route_id, to_number(substr(a.route_id,6))" +
                                ", a.parent_route_id, a.parent_route_id,  (select name from mst_location where location_id = a.from_location_id ) as from_location_id,  " +
                                "(select name from mst_location where location_id = a.to_location_id) as to_location_id,  a.from_type, a.sys_last_modified_by, a.sys_last_modified_date," +
                                " a.to_type, b.from_cal_id, (select START_DATE from MST_GMP_DELL_CALENDAR where CALENDAR_ID=b.from_cal_id) fromDate, b.to_cal_id, (select START_DATE from " +
                                "MST_GMP_DELL_CALENDAR where CALENDAR_ID=b.to_cal_id) toDate, b.split        from mst_gmp_sourcing_template a,       mst_gmp_sourcing_details b " +
                                " where a.route_id = b.route_id  and b.product_id in (select item_id from   ( select product_id item_id, product_group_id item_group_id  from ( select " +
                                "product_id, decode(product_group_id, product_id, null, product_group_id) product_group_id from mst_product_group_detail a)  connect by prior " +
                                "product_id = product_group_id  start with product_id = :startProductId))  \t\t       and b.channel_id in (select channel_id from ( " +
                                " \t\t\t\tselect channel_id, name, description, parent_id, channel_level_id, forecast_instance_id  \t\t\t\t   from   \t\t\t   ( select a.channel_id, a.name," +
                                " a.description, decode(a.parent_id, a.channel_id, null, a.parent_id) parent_id, a.channel_level_id, b.forecast_instance_id   \t\t   \t from " +
                                "mst_gmp_dell_channels a,  \t\t \t  mst_gmp_dell_channel_levels b   \t\t  where a.channel_level_id = b.channel_level_id  \t  and  a.is_active = '1' " +
                                "  )  connect by prior channel_id = parent_id  start with channel_id =  :startChannelId)  where channel_id = '1864' or forecast_instance_id" +
                                " in  ('1','2','3','4'))  order by product_id, channel_id asc, from_type desc, to_number(substr(a.route_id,6)) ";
                            //stopwatch.Reset();
                            //stopwatch.Start();
                            string empty = string.Empty;
                            string empty1 = string.Empty;
                            string str1 = string.Empty;
                            string empty2 = string.Empty;
                            OracleCommand oracleCommand = new OracleCommand(str, oracleConnection);
                            try
                            {
                                OracleParameter oracleParameter = new OracleParameter("startProductId", OracleDbType.Varchar2, ParameterDirection.Input)
                                {
                                    Value = startProductId
                                };
                                oracleCommand.Parameters.Add(oracleParameter);
                                OracleParameter oracleParameter1 = new OracleParameter("startChannelId", OracleDbType.Varchar2, ParameterDirection.Input)
                                {
                                    Value = startChannelId
                                };
                                oracleCommand.Parameters.Add(oracleParameter1);
                                oracleConnection.Open();
                                OracleDataReader oracleDataReader = oracleCommand.ExecuteReader();
                                try
                                {
                                    while (oracleDataReader.Read())
                                    {
                                        string str2 = oracleDataReader["PRODUCT_ID"].ToString();
                                        string str3 = oracleDataReader["product_name"].ToString();
                                        string str4 = oracleDataReader["CHANNEL_ID"].ToString();
                                        string str5 = oracleDataReader["channel_name"].ToString();
                                        string empty3 = string.Empty;
                                        string str6 = oracleDataReader["SYS_LAST_MODIFIED_BY"].ToString();
                                        string str7 = oracleDataReader["SYS_LAST_MODIFIED_DATE"].ToString();
                                        if (!DBNull.Value.Equals(oracleDataReader["fromDate"]))
                                        {
                                            empty3 = oracleDataReader["from_cal_id"].ToString();
                                        }
                                        string empty4 = string.Empty;
                                        if (!DBNull.Value.Equals(oracleDataReader["toDate"]))
                                        {
                                            empty4 = oracleDataReader["to_cal_id"].ToString();
                                        }
                                        string str8 = oracleDataReader["route_id"].ToString();
                                        string str9 = oracleDataReader["parent_route_id"].ToString();
                                        int num = 0;
                                        if ((oracleDataReader["split"] == null ? false : !string.IsNullOrEmpty(oracleDataReader["split"].ToString())))
                                        {
                                            num = int.Parse(oracleDataReader["split"].ToString());
                                        }
                                        string str10 = oracleDataReader["from_location_id"].ToString();
                                        string str11 = oracleDataReader["from_type"].ToString();
                                        string str12 = oracleDataReader["to_location_id"].ToString();
                                        oracleDataReader["to_type"].ToString();
                                        string empty5 = string.Empty;
                                        string empty6 = string.Empty;
                                        if (str11.Equals("KITTING"))
                                        {
                                            empty5 = str8;
                                            sourcingRuleLineItem = new SourcingRuleLineItem(str2, str3, str4, str5);
                                            if (!DBNull.Value.Equals(oracleDataReader["fromDate"]))
                                            {
                                                sourcingRuleLineItem.FromDateCalId = empty3;
                                                sourcingRuleLineItem.FromDate = Convert.ToDateTime(oracleDataReader["fromDate"]);
                                                sourcingRuleLineItem.FromDateStr = sourcingRuleLineItem.FromDate.ToShortDateString();
                                            }
                                            if (!DBNull.Value.Equals(oracleDataReader["toDate"]))
                                            {
                                                sourcingRuleLineItem.ToDateCalId = empty4;
                                                sourcingRuleLineItem.ToDate = Convert.ToDateTime(oracleDataReader["toDate"]);
                                                sourcingRuleLineItem.ToDateStr = sourcingRuleLineItem.ToDate.ToShortDateString();
                                            }
                                            sourcingRuleLineItem.KitSite = str10;
                                            sourcingRuleLineItem.BoxSite = str12;
                                            sourcingRuleLineItem.BoxSplit = num;
                                            sourcingRuleLineItem.SysLastModifiedby = str6;
                                            sourcingRuleLineItem.SysLastModifiedDate = str7;
                                            strs1.Add(empty5, sourcingRuleLineItem);
                                        }
                                        else if (!str11.Equals("BOXING"))
                                        {
                                            strArrays = new string[] { str2, "_", str4, "_", empty3, "_", empty4, "__", str10, "_", str12, "_", str11 };
                                            empty5 = string.Concat(strArrays);
                                            //ILog log = SourcingRulesDataSource.log;
                                            count = new object[] { str11, empty5 };
                                            // log.InfoFormat("Invalid from type {0}. Ignored sourcing rule item {1}.", count);
                                            continue;
                                        }
                                        else
                                        {
                                            empty5 = str9;
                                            if (!strs1.ContainsKey(empty5))
                                            {
                                                strArrays = new string[] { str2, "_", str4, "_", empty3, "_", empty4, "__", str10, "_", str12, "_", str11 };
                                                empty5 = string.Concat(strArrays);
                                                // ILog log1 = SourcingRulesDataSource.log;
                                                count = new object[] { empty5 };
                                                //  log1.InfoFormat("Parent KITTING->BOXING rule not found. Ignored sourcing rule item {0}.", count);
                                                continue;
                                            }
                                            else
                                            {
                                                sourcingRuleLineItem = strs1[empty5];
                                                SourcingRuleLineItem sourcingRuleLineItem1 = new SourcingRuleLineItem(str2, str3, str4, str5)
                                                {
                                                    RouteID = str8,
                                                    ParentRouteID = str9,
                                                    FromDateCalId = sourcingRuleLineItem.FromDateCalId,
                                                    FromDate = sourcingRuleLineItem.FromDate,
                                                    FromDateStr = sourcingRuleLineItem.FromDateStr,
                                                    ToDateCalId = sourcingRuleLineItem.ToDateCalId,
                                                    ToDate = sourcingRuleLineItem.ToDate,
                                                    ToDateStr= sourcingRuleLineItem.ToDateStr,
                                                    KitSite = sourcingRuleLineItem.KitSite,
                                                    BoxSite = sourcingRuleLineItem.BoxSite,
                                                    BoxSplit = sourcingRuleLineItem.BoxSplit,
                                                    MergeSite = str12,
                                                    MergeSplit = num,
                                                    SysLastModifiedby = sourcingRuleLineItem.SysLastModifiedby,
                                                    SysLastModifiedDate = sourcingRuleLineItem.SysLastModifiedDate
                                                };
                                                empty6 = string.Concat(str8, "_", str9);
                                                if (strs2.ContainsKey(empty6))
                                                {
                                                    //                                                ILog log2 = SourcingRulesDataSource.log;
                                                    count = new object[] { empty6 };
                                                    // log2.InfoFormat("Duplicate SR Line Item Found {0}.", count);
                                                }
                                                else
                                                {
                                                    strs2.Add(empty6, sourcingRuleLineItem1);
                                                }
                                            }
                                        }
                                    }
                                }
                                finally
                                {
                                    if (oracleDataReader != null)
                                    {
                                        oracleDataReader.Dispose();
                                    }
                                }
                            }
                            finally
                            {
                                if (oracleCommand != null)
                                {
                                    oracleCommand.Dispose();
                                }
                            }
                            //stopwatch.Stop();
                            //ILog log3 = SourcingRulesDataSource.log;
                            count = new object[] { strs2.Count };
                            //  log3.InfoFormat("GetSourcingRuleLineItems: Loaded {0} Sourcing Rule Line Items.  In {1}.", count);
                        }
                        finally
                        {
                            if (oracleConnection != null)
                            {
                                oracleConnection.Dispose();
                            }
                        }
                        strs = strs2;
                    }
                    catch (Exception exception)
                    {
                        //SourcingRulesDataSource.log.Fatal("GetSourcingRuleLineItems : Failed to Get information from database table: ", exception);
                        throw;
                    }
                }
                finally
                {
                    // SourcingRulesDataSource.log.Info("GetSourcingRuleLineItems : End.");
                }
                return strs;
            });
        }

        public async Task<Dictionary<string, SourcingRuleLineItem>> GetSingleRuleLineItems(string ChannelId, string ProductId)
        {
            SourcingRuleLineItem sourcingRuleLineItem;
            Dictionary<string, SourcingRuleLineItem> strs;
            object[] count;
            string[] strArrays;
            Dictionary<string, SourcingRuleLineItem> strs1 = new Dictionary<string, SourcingRuleLineItem>();
            Dictionary<string, SourcingRuleLineItem> strs2 = new Dictionary<string, SourcingRuleLineItem>();
            return await Task.Run(() =>
            {
                try
                {
                    try
                    {
                        OracleConnection oracleConnection = new OracleConnection(this.connenctionString);
                        try
                        {
                            string str = " select b.product_id, ( select distinct name from mst_product_master where product_id = b.product_id) product_name, b.channel_id, ( select distinct name from mst_gmp_dell_channels where channel_id = b.channel_id) channel_name,  b.geography_id, a.route_id,a.sys_last_modified_by, a.sys_last_modified_date, to_number(substr(a.route_id,6)), a.parent_route_id, a.parent_route_id,  (select name from mst_location where location_id = a.from_location_id ) as from_location_id,  (select name from mst_location where location_id = a.to_location_id) as to_location_id,  a.from_type, a.to_type, b.from_cal_id, (select START_DATE from MST_GMP_DELL_CALENDAR where CALENDAR_ID=b.from_cal_id) fromDate, b.to_cal_id, (select START_DATE from MST_GMP_DELL_CALENDAR where CALENDAR_ID=b.to_cal_id) toDate, b.split        from mst_gmp_sourcing_template a,       mst_gmp_sourcing_details b  where a.route_id = b.route_id  and b.product_id = :startProductId  and b.channel_id =  :startChannelId  order by product_id, channel_id asc, from_type desc, to_number(substr(a.route_id,6)) ";
                            string empty = string.Empty;
                            string empty1 = string.Empty;
                            string str1 = string.Empty;
                            string empty2 = string.Empty;
                            OracleCommand oracleCommand = new OracleCommand(str, oracleConnection);
                            try
                            {
                                OracleParameter oracleParameter = new OracleParameter("startProductId", OracleDbType.Varchar2, ParameterDirection.Input)
                                {
                                    Value = ProductId
                                };
                                oracleCommand.Parameters.Add(oracleParameter);
                                OracleParameter oracleParameter1 = new OracleParameter("startChannelId", OracleDbType.Varchar2, ParameterDirection.Input)
                                {
                                    Value = ChannelId
                                };
                                oracleCommand.Parameters.Add(oracleParameter1);
                                oracleConnection.Open();
                                OracleDataReader oracleDataReader = oracleCommand.ExecuteReader();
                                try
                                {
                                    while (oracleDataReader.Read())
                                    {
                                        string str2 = oracleDataReader["PRODUCT_ID"].ToString();
                                        string str3 = oracleDataReader["product_name"].ToString();
                                        string str4 = oracleDataReader["CHANNEL_ID"].ToString();
                                        string str5 = oracleDataReader["channel_name"].ToString();
                                        string empty3 = string.Empty;
                                        string str6 = oracleDataReader["SYS_LAST_MODIFIED_DATE"].ToString();
                                        string str7 = oracleDataReader["SYS_LAST_MODIFIED_BY"].ToString();
                                        if (!DBNull.Value.Equals(oracleDataReader["fromDate"]))
                                        {
                                            empty3 = oracleDataReader["from_cal_id"].ToString();
                                        }
                                        string empty4 = string.Empty;
                                        if (!DBNull.Value.Equals(oracleDataReader["toDate"]))
                                        {
                                            empty4 = oracleDataReader["to_cal_id"].ToString();
                                        }
                                        string str8 = oracleDataReader["route_id"].ToString();
                                        string str9 = oracleDataReader["parent_route_id"].ToString();
                                        int num = 0;
                                        if ((oracleDataReader["split"] == null ? false : !string.IsNullOrEmpty(oracleDataReader["split"].ToString())))
                                        {
                                            num = int.Parse(oracleDataReader["split"].ToString());
                                        }
                                        string str10 = oracleDataReader["from_location_id"].ToString();
                                        string str11 = oracleDataReader["from_type"].ToString();
                                        string str12 = oracleDataReader["to_location_id"].ToString();
                                        oracleDataReader["to_type"].ToString();
                                        string empty5 = string.Empty;
                                        string empty6 = string.Empty;
                                        if (str11.Equals("KITTING"))
                                        {
                                            empty5 = str8;
                                            sourcingRuleLineItem = new SourcingRuleLineItem(str2, str3, str4, str5);
                                            if (!DBNull.Value.Equals(oracleDataReader["fromDate"]))
                                            {
                                                sourcingRuleLineItem.FromDateCalId = empty3;
                                                sourcingRuleLineItem.FromDate = Convert.ToDateTime(oracleDataReader["fromDate"]);
                                            }
                                            if (!DBNull.Value.Equals(oracleDataReader["toDate"]))
                                            {
                                                sourcingRuleLineItem.ToDateCalId = empty4;
                                                sourcingRuleLineItem.ToDate = Convert.ToDateTime(oracleDataReader["toDate"]);
                                            }
                                            sourcingRuleLineItem.KitSite = str10;
                                            sourcingRuleLineItem.BoxSite = str12;
                                            sourcingRuleLineItem.BoxSplit = num;
                                            sourcingRuleLineItem.SysLastModifiedby = str7;
                                            sourcingRuleLineItem.SysLastModifiedDate = str6;
                                            strs1.Add(empty5, sourcingRuleLineItem);
                                        }
                                        else if (!str11.Equals("BOXING"))
                                        {
                                            strArrays = new string[] { str2, "_", str4, "_", empty3, "_", empty4, "__", str10, "_", str12, "_", str11 };
                                            empty5 = string.Concat(strArrays);
                                            // log = SourcingRulesDataSource.log;
                                            count = new object[] { str11, empty5 };
                                            // log.InfoFormat("Invalid from type {0}. Ignored sourcing rule item {1}.", count);
                                            continue;
                                        }
                                        else
                                        {
                                            empty5 = str9;
                                            if (!strs1.ContainsKey(empty5))
                                            {
                                                strArrays = new string[] { str2, "_", str4, "_", empty3, "_", empty4, "__", str10, "_", str12, "_", str11 };
                                                empty5 = string.Concat(strArrays);
                                                // log1 = SourcingRulesDataSource.log;
                                                count = new object[] { empty5 };
                                                //log1.InfoFormat("Parent KITTING->BOXING rule not found. Ignored sourcing rule item {0}.", count);
                                                continue;
                                            }
                                            else
                                            {
                                                sourcingRuleLineItem = strs1[empty5];
                                                SourcingRuleLineItem sourcingRuleLineItem1 = new SourcingRuleLineItem(str2, str3, str4, str5)
                                                {
                                                    RouteID = str8,
                                                    ParentRouteID = str9,
                                                    FromDateCalId = sourcingRuleLineItem.FromDateCalId,
                                                    FromDate = sourcingRuleLineItem.FromDate,
                                                    ToDateCalId = sourcingRuleLineItem.ToDateCalId,
                                                    ToDate = sourcingRuleLineItem.ToDate,
                                                    KitSite = sourcingRuleLineItem.KitSite,
                                                    BoxSite = sourcingRuleLineItem.BoxSite,
                                                    BoxSplit = sourcingRuleLineItem.BoxSplit,
                                                    MergeSite = str12,
                                                    MergeSplit = num,
                                                    SysLastModifiedDate = sourcingRuleLineItem.SysLastModifiedDate,
                                                    SysLastModifiedby = sourcingRuleLineItem.SysLastModifiedby
                                                };
                                                empty6 = string.Concat(str8, "_", str9);
                                                if (strs2.ContainsKey(empty6))
                                                {
                                                    // log2 = SourcingRulesDataSource.log;
                                                    count = new object[] { empty6 };
                                                    //log2.InfoFormat("Duplicate SR Line Item Found {0}.", count);
                                                }
                                                else
                                                {
                                                    strs2.Add(empty6, sourcingRuleLineItem1);
                                                }
                                            }
                                        }
                                    }
                                }
                                finally
                                {
                                    if (oracleDataReader != null)
                                    {
                                        oracleDataReader.Dispose();
                                    }
                                }
                            }
                            finally
                            {
                                if (oracleCommand != null)
                                {
                                    oracleCommand.Dispose();
                                }
                            }
                            //  stopwatch.Stop();
                            // log3 = SourcingRulesDataSource.log;
                            count = new object[] { strs2.Count };
                            //log3.InfoFormat("GetSourcingRuleLineItems: Loaded {0} Sourcing Rule Line Items.  In {1}.", count);
                        }
                        finally
                        {
                            if (oracleConnection != null)
                            {
                                oracleConnection.Dispose();
                            }
                        }
                        strs = strs2;
                    }
                    catch (Exception exception)
                    {
                        // SourcingRulesDataSource.log.Fatal("GetSourcingRuleLineItems : Failed to Get information from database table: ", exception);
                        throw;
                    }
                }
                finally
                {
                    //SourcingRulesDataSource.log.Info("GetSourcingRuleLineItems : End.");
                }
                return strs;
            });
        }

        public DataSet GetSiteNames()
        {
            DataSet dataSet;
            DataSet dataSet1 = new DataSet();
            try
            {
                OracleConnection oracleConnection = new OracleConnection(this.connenctionString);
                try
                {
                    OracleCommand oracleCommand = new OracleCommand("select distinct b.location_group_id, c.name, substr(c.name,0,10) as site_id, b.location_group_type from mst_location_ccn a, mst_location_group_detail b, mst_location c where b.location_id = a.facility and b.location_type = 'FACILITY' and a.ccn in ( select ccn from x_location_ccn_glovia_xref where forecast_instance_id in ('1','2','3','4')  ) and b.location_group_id = c.location_id  order by c.name ", oracleConnection);
                    try
                    {
                        oracleConnection.Open();
                        (new OracleDataAdapter(oracleCommand)).Fill(dataSet1);
                    }
                    finally
                    {
                        if (oracleCommand != null)
                        {
                            oracleCommand.Dispose();
                        }
                    }
                }
                finally
                {
                    if (oracleConnection != null)
                    {
                        oracleConnection.Dispose();
                    }
                }
                dataSet = dataSet1;
            }
            catch (Exception exception)
            {
                throw;
            }
            return dataSet;
        }


        public async Task<int> AddSourcingRuleCollection(SourcingRuleSaveClass ruleToSave)
        {
            string updatedBy = UserID;
            OracleCommand oracleCommand;
            OracleParameter oracleParameter;
            DateTime dateTime;
            bool flag;
            bool flag1;
            int num = 0;
            int num1 = 0;
            OracleTransaction oracleTransaction = null;
            OracleConnection oracleConnection = null;
            return await Task.Run(() =>
            {
                try
                {
                    try
                    {
                        oracleConnection = new OracleConnection(this.connenctionString);
                        oracleConnection.Open();
                        oracleTransaction = oracleConnection.BeginTransaction();
                        string empty = string.Empty;
                        foreach (SourcingSaveLineItem itemsList in ruleToSave.ItemsList)
                        {
                            if (!string.IsNullOrEmpty(itemsList.FromDateCalId))
                            {
                                flag = true;
                            }
                            else
                            {
                                DateTime fromDate = itemsList.FromDate;
                                dateTime = new DateTime();
                                flag = !(fromDate != dateTime);
                            }
                            if (!flag)
                            {
                                oracleCommand = new OracleCommand("select CALENDAR_ID from MST_GMP_DELL_CALENDAR where START_DATE=:START_DATE", oracleConnection);
                                try
                                {
                                    oracleParameter = new OracleParameter("START_DATE", OracleDbType.Date, ParameterDirection.Input)
                                    {
                                        Value = itemsList.FromDate
                                    };
                                    oracleCommand.Parameters.Add(oracleParameter);
                                    itemsList.FromDateCalId = oracleCommand.ExecuteScalar().ToString();
                                }
                                finally
                                {
                                    if (oracleCommand != null)
                                    {
                                        oracleCommand.Dispose();
                                    }
                                }
                            }
                            if (!string.IsNullOrEmpty(itemsList.ToDateCalId))
                            {
                                flag1 = true;
                            }
                            else
                            {
                                DateTime toDate = itemsList.ToDate;
                                dateTime = new DateTime();
                                flag1 = !(toDate != dateTime);
                            }
                            if (!flag1)
                            {
                                oracleCommand = new OracleCommand("select CALENDAR_ID from MST_GMP_DELL_CALENDAR where START_DATE=:START_DATE", oracleConnection);
                                try
                                {
                                    oracleParameter = new OracleParameter("START_DATE", OracleDbType.Date, ParameterDirection.Input)
                                    {
                                        Value = itemsList.ToDate
                                    };
                                    oracleCommand.Parameters.Add(oracleParameter);
                                    itemsList.ToDateCalId = oracleCommand.ExecuteScalar().ToString();
                                }
                                finally
                                {
                                    if (oracleCommand != null)
                                    {
                                        oracleCommand.Dispose();
                                    }
                                }
                            }
                            string str = " SELECT count(*) FROM MST_GMP_SOURCING_TEMPLATE T, MST_GMP_SOURCING_DETAILS D WHERE T.FROM_LOCATION_ID=:FROM_LOCATION_ID AND T.FROM_TYPE='KITTING' AND T.TO_LOCATION_ID=:TO_LOCATION_ID AND T.TO_TYPE='BOXING'  AND T.ROUTE_ID=D.ROUTE_ID  AND D.PRODUCT_ID=:PRODUCT_ID AND D.CHANNEL_ID=:CHANNEL_ID AND D.SYS_ENT_STATE='ACTIVE' ";
                            str = (!string.IsNullOrEmpty(itemsList.FromDateCalId) ? string.Concat(str, " AND D.FROM_CAL_ID=:FROM_CAL_ID ") : string.Concat(str, " AND D.FROM_CAL_ID is null "));
                            str = (!string.IsNullOrEmpty(itemsList.ToDateCalId) ? string.Concat(str, " AND D.TO_CAL_ID=:TO_CAL_ID ") : string.Concat(str, " AND D.TO_CAL_ID is null "));
                            OracleCommand oracleCommand1 = new OracleCommand(str, oracleConnection);
                            try
                            {
                                OracleParameter oracleParameter1 = new OracleParameter("FROM_LOCATION_ID", OracleDbType.Varchar2, ParameterDirection.Input)
                                {
                                    Value = itemsList.KitSite
                                };
                                oracleCommand1.Parameters.Add(oracleParameter1);
                                OracleParameter oracleParameter2 = new OracleParameter("TO_LOCATION_ID", OracleDbType.Varchar2, ParameterDirection.Input)
                                {
                                    Value = itemsList.BoxSite
                                };
                                oracleCommand1.Parameters.Add(oracleParameter2);
                                OracleParameter oracleParameter3 = new OracleParameter("PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input)
                                {
                                    Value = itemsList.ProductID
                                };
                                oracleCommand1.Parameters.Add(oracleParameter3);
                                oracleParameter = new OracleParameter("CHANNEL_ID", OracleDbType.Varchar2, ParameterDirection.Input)
                                {
                                    Value = itemsList.ChannelId
                                };
                                oracleCommand1.Parameters.Add(oracleParameter);
                                if (!string.IsNullOrEmpty(itemsList.FromDateCalId))
                                {
                                    OracleParameter oracleParameter4 = new OracleParameter("FROM_CAL_ID", OracleDbType.Int32, ParameterDirection.Input)
                                    {
                                        Value = int.Parse(itemsList.FromDateCalId)
                                    };
                                    oracleCommand1.Parameters.Add(oracleParameter4);
                                }
                                if (!string.IsNullOrEmpty(itemsList.ToDateCalId))
                                {
                                    OracleParameter oracleParameter5 = new OracleParameter("TO_CAL_ID", OracleDbType.Int32, ParameterDirection.Input)
                                    {
                                        Value = int.Parse(itemsList.ToDateCalId)
                                    };
                                    oracleCommand1.Parameters.Add(oracleParameter5);
                                }
                                string str1 = oracleCommand1.ExecuteScalar().ToString().Trim();
                                if (!string.IsNullOrEmpty(str1))
                                {
                                    num1 = int.Parse(str1);
                                }
                            }
                            finally
                            {
                                if (oracleCommand1 != null)
                                {
                                    oracleCommand1.Dispose();
                                }
                            }
                            string empty1 = string.Empty;
                            if (num1 == 0)
                            {
                                num += this.InsertSourcingRule(itemsList, SourcingRuleType.KitToBox, updatedBy, oracleConnection, out empty1);
                                empty = empty1;
                            }
                            itemsList.ParentRouteID = empty;
                            num += this.InsertSourcingRule(itemsList, SourcingRuleType.BoxToMerge, updatedBy, oracleConnection, out empty1);
                        }
                        oracleTransaction.Commit();
                    }
                    catch (Exception exception1)
                    {
                        Exception exception = exception1;
                        if (oracleTransaction != null)
                        {
                            oracleTransaction.Rollback();
                        }
                        throw exception;
                    }
                }
                finally
                {
                    if (oracleTransaction != null)
                    {
                        oracleTransaction.Dispose();
                    }
                    if (oracleConnection != null)
                    {
                        oracleConnection.Close();
                        oracleConnection.Dispose();
                    }
                }
                return num;
            });
        }



        public int InsertSourcingRule(SourcingRuleLineItem item, SourcingRuleType type, string updatedBy, OracleConnection oracleConnection, out string routeId)
        {
            OracleParameter oracleParameter;
            OracleParameter parentRouteID;
            OracleParameter boxSite;
            OracleParameter oracleParameter1;
            OracleParameter mergeSite;
            OracleParameter value;
            OracleParameter mergeSplit;
            DateTime dateTime;
            bool flag;
            bool flag1;
            int num = 0;
            int num1 = 0;
            try
            {
                string str = "select max(to_number(substr(route_id,6))) from mst_gmp_sourcing_template";
                string str1 = "INSERT INTO MST_GMP_SOURCING_TEMPLATE (ROUTE_ID, PARENT_ROUTE_ID, FROM_LOCATION_ID, FROM_TYPE, TO_LOCATION_ID, TO_TYPE, SYS_SOURCE, SYS_CREATED_BY, SYS_CREATION_DATE, SYS_ENT_STATE, SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE)  VALUES (:ROUTE_ID, :PARENT_ROUTE_ID, :FROM_LOCATION_ID, :FROM_TYPE, :TO_LOCATION_ID, :TO_TYPE, 'UI', :SYS_CREATED_BY, SYSDATE, 'ACTIVE', :SYS_CREATED_BY, SYSDATE)";
                string str2 = "INSERT INTO MST_GMP_SOURCING_DETAILS(RULE_ID, PRODUCT_ID, CHANNEL_ID, ROUTE_ID, FROM_CAL_ID, TO_CAL_ID, SPLIT, SYS_SOURCE, SYS_CREATED_BY, SYS_CREATION_DATE, SYS_ENT_STATE, SYS_LAST_MODIFIED_BY, SYS_LAST_MODIFIED_DATE) VALUES (:RULE_ID, :PRODUCT_ID, :CHANNEL_ID, :ROUTE_ID, :FROM_CAL_ID, :TO_CAL_ID, :SPLIT, 'UI', :SYS_CREATED_BY, SYSDATE, 'ACTIVE', :SYS_CREATED_BY, SYSDATE) ";
                OracleCommand oracleCommand = new OracleCommand(str, oracleConnection);
                try
                {
                    string str3 = oracleCommand.ExecuteScalar().ToString().Trim();
                    num = (string.IsNullOrEmpty(str3) ? 0 : int.Parse(str3));
                }
                finally
                {
                    if (oracleCommand != null)
                    {
                        oracleCommand.Dispose();
                    }
                }
                if (!string.IsNullOrEmpty(item.FromDateCalId))
                {
                    flag = true;
                }
                else
                {
                    DateTime fromDate = item.FromDate;
                    dateTime = new DateTime();
                    flag = !(fromDate != dateTime);
                }
                if (!flag)
                {
                    oracleCommand = new OracleCommand("select CALENDAR_ID from MST_GMP_DELL_CALENDAR where START_DATE=:START_DATE", oracleConnection);
                    try
                    {
                        oracleParameter = new OracleParameter("START_DATE", OracleDbType.Date, ParameterDirection.Input)
                        {
                            Value = item.FromDate
                        };
                        oracleCommand.Parameters.Add(oracleParameter);
                        item.FromDateCalId = oracleCommand.ExecuteScalar().ToString();
                    }
                    finally
                    {
                        if (oracleCommand != null)
                        {
                            oracleCommand.Dispose();
                        }
                    }
                }
                if (!string.IsNullOrEmpty(item.ToDateCalId))
                {
                    flag1 = true;
                }
                else
                {
                    DateTime toDate = item.ToDate;
                    dateTime = new DateTime();
                    flag1 = !(toDate != dateTime);
                }
                if (!flag1)
                {
                    oracleCommand = new OracleCommand("select CALENDAR_ID from MST_GMP_DELL_CALENDAR where START_DATE=:START_DATE", oracleConnection);
                    try
                    {
                        oracleParameter = new OracleParameter("START_DATE", OracleDbType.Date, ParameterDirection.Input)
                        {
                            Value = item.ToDate
                        };
                        oracleCommand.Parameters.Add(oracleParameter);
                        item.ToDateCalId = oracleCommand.ExecuteScalar().ToString();
                    }
                    finally
                    {
                        if (oracleCommand != null)
                        {
                            oracleCommand.Dispose();
                        }
                    }
                }
                OracleCommand oracleCommand1 = new OracleCommand(str1, oracleConnection);
                try
                {
                    num++;
                    oracleParameter = new OracleParameter("ROUTE_ID", OracleDbType.Varchar2, ParameterDirection.Input)
                    {
                        Value = string.Concat("Route", num)
                    };
                    oracleCommand1.Parameters.Add(oracleParameter);
                    routeId = oracleParameter.Value.ToString();
                    parentRouteID = new OracleParameter("PARENT_ROUTE_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                    if (type != SourcingRuleType.KitToBox)
                    {
                        parentRouteID.Value = item.ParentRouteID;
                    }
                    else
                    {
                        parentRouteID.Value = DBNull.Value;
                    }
                    oracleCommand1.Parameters.Add(parentRouteID);
                    boxSite = new OracleParameter("FROM_LOCATION_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                    if (type != SourcingRuleType.KitToBox)
                    {
                        boxSite.Value = item.BoxSite;
                    }
                    else
                    {
                        boxSite.Value = item.KitSite;
                    }
                    oracleCommand1.Parameters.Add(boxSite);
                    oracleParameter1 = new OracleParameter("FROM_TYPE", OracleDbType.Varchar2, ParameterDirection.Input);
                    if (type != SourcingRuleType.KitToBox)
                    {
                        oracleParameter1.Value = "BOXING";
                    }
                    else
                    {
                        oracleParameter1.Value = "KITTING";
                    }
                    oracleCommand1.Parameters.Add(oracleParameter1);
                    mergeSite = new OracleParameter("TO_LOCATION_ID", OracleDbType.Varchar2, ParameterDirection.Input);
                    if (type != SourcingRuleType.KitToBox)
                    {
                        mergeSite.Value = item.MergeSite;
                    }
                    else
                    {
                        mergeSite.Value = item.BoxSite;
                    }
                    oracleCommand1.Parameters.Add(mergeSite);
                    value = new OracleParameter("TO_TYPE", OracleDbType.Varchar2, ParameterDirection.Input);
                    if (type != SourcingRuleType.KitToBox)
                    {
                        value.Value = "MERGING";
                    }
                    else
                    {
                        value.Value = "BOXING";
                    }
                    oracleCommand1.Parameters.Add(value);
                    mergeSplit = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input)
                    {
                        Value = updatedBy
                    };
                    oracleCommand1.Parameters.Add(mergeSplit);
                    num1 += oracleCommand1.ExecuteNonQuery();
                }
                finally
                {
                    if (oracleCommand1 != null)
                    {
                        oracleCommand1.Dispose();
                    }
                }
                oracleCommand1 = new OracleCommand(str2, oracleConnection);
                try
                {
                    oracleParameter = new OracleParameter("RULE_ID", OracleDbType.Varchar2, ParameterDirection.Input)
                    {
                        Value = string.Concat("Rule", num)
                    };
                    oracleCommand1.Parameters.Add(oracleParameter);
                    parentRouteID = new OracleParameter("PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input)
                    {
                        Value = item.ProductID
                    };
                    oracleCommand1.Parameters.Add(parentRouteID);
                    boxSite = new OracleParameter("CHANNEL_ID", OracleDbType.Varchar2, ParameterDirection.Input)
                    {
                        Value = item.ChannelId
                    };
                    oracleCommand1.Parameters.Add(boxSite);
                    oracleParameter1 = new OracleParameter("ROUTE_ID", OracleDbType.Varchar2, ParameterDirection.Input)
                    {
                        Value = string.Concat("Route", num)
                    };
                    oracleCommand1.Parameters.Add(oracleParameter1);
                    mergeSite = new OracleParameter("FROM_CAL_ID", OracleDbType.Int32, ParameterDirection.Input);
                    if (!string.IsNullOrEmpty(item.FromDateCalId))
                    {
                        mergeSite.Value = int.Parse(item.FromDateCalId);
                    }
                    else
                    {
                        mergeSite.Value = DBNull.Value;
                    }
                    oracleCommand1.Parameters.Add(mergeSite);
                    value = new OracleParameter("TO_CAL_ID", OracleDbType.Int32, ParameterDirection.Input);
                    if (!string.IsNullOrEmpty(item.ToDateCalId))
                    {
                        value.Value = int.Parse(item.ToDateCalId);
                    }
                    else
                    {
                        value.Value = DBNull.Value;
                    }
                    oracleCommand1.Parameters.Add(value);
                    mergeSplit = new OracleParameter("SPLIT", OracleDbType.Decimal, ParameterDirection.Input);
                    if (type != SourcingRuleType.KitToBox)
                    {
                        mergeSplit.Value = item.MergeSplit;
                    }
                    else
                    {
                        mergeSplit.Value = item.BoxSplit;
                    }
                    oracleCommand1.Parameters.Add(mergeSplit);
                    OracleParameter oracleParameter2 = new OracleParameter("SYS_CREATED_BY", OracleDbType.Varchar2, ParameterDirection.Input)
                    {
                        Value = updatedBy
                    };
                    oracleCommand1.Parameters.Add(oracleParameter2);
                    num1 += oracleCommand1.ExecuteNonQuery();
                }
                finally
                {
                    if (oracleCommand1 != null)
                    {
                        oracleCommand1.Dispose();
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return num1;
        }


        #region Commented Code
        //public async Task<Dictionary<string, SourcingRuleLineItem>> GetSourcingRuleLineItems(string searchCriteria)
        //{
        //    Dictionary<string, SourcingRuleLineItem> listSourcingRuleLineHdr = new Dictionary<string, SourcingRuleLineItem>();
        //    Dictionary<string, SourcingRuleLineItem> listSourcingRuleLineItems = new Dictionary<string, SourcingRuleLineItem>();
        //    return await Task.Run(() =>
        //    {
        //        try
        //        {
        //            using (OracleConnection conn = new OracleConnection(this.connenctionString))
        //            {
        //                //read the receipt history data
        //                string sql = "select PRODUCT_ID, ( select name from mst_product_master where product_id = SD.product_id) product_name, CHANNEL_ID, "
        //                                + " ( select name from mst_gmp_dell_channels where channel_id = SD.channel_id) channel_name,  "
        //                                + " from_cal_id, (select START_DATE from MST_GMP_DELL_CALENDAR where CALENDAR_ID=SD.from_cal_id) fromDate,  "
        //                                + " to_cal_id, (select START_DATE from MST_GMP_DELL_CALENDAR where CALENDAR_ID=SD.to_cal_id) toDate, split,  "
        //                                + " connect_by_root ST.from_location_id parent_site, ST.route_id, ST.parent_route_id, ST.from_location_id, ST.from_type, ST.to_location_id, ST.to_type  "
        //                                + " from MST_GMP_SOURCING_TEMPLATE ST, MST_GMP_SOURCING_DETAILS SD "
        //                                + " where ST.route_id = SD.route_id  "
        //                                + " and CHANNEL_ID not like '<%' "
        //                                + searchCriteria
        //                                + " connect by prior ST.route_id = ST.parent_route_id  "
        //                                + " start with ST.from_type='KITTING' order siblings by channel_id, product_id, ST.from_location_id";

        //                //sw.Reset();
        //                //sw.Start();

        //                string prevProductID = String.Empty;
        //                string prevChannelID = String.Empty;
        //                string prevFromDateCalID = String.Empty;
        //                string prevToDateCalID = String.Empty;



        //                using (OracleCommand cmd = new OracleCommand(sql, conn))
        //                {
        //                    conn.Open();

        //                    using (OracleDataReader rdr = cmd.ExecuteReader())
        //                    {
        //                        while (rdr.Read())
        //                        {
        //                            string productID = rdr["PRODUCT_ID"].ToString();
        //                            string productName = rdr["product_name"].ToString();
        //                            string channelID = rdr["CHANNEL_ID"].ToString();
        //                            string channelName = rdr["channel_name"].ToString();
        //                            string fromDateCalID = string.Empty;
        //                            //DateTime fromDate;
        //                            if (!DBNull.Value.Equals(rdr["fromDate"]))
        //                            {
        //                                fromDateCalID = rdr["from_cal_id"].ToString();
        //                                //fromDate = rdr.GetDateTime(5);
        //                            }
        //                            string toDateCalID = string.Empty;
        //                            //DateTime toDate;
        //                            if (!DBNull.Value.Equals(rdr["toDate"]))
        //                            {
        //                                toDateCalID = rdr["to_cal_id"].ToString();
        //                                //toDate = rdr.GetDateTime(7);
        //                            }

        //                            string routeID = rdr["route_id"].ToString();
        //                            string parentRouteID = rdr["parent_route_id"].ToString();

        //                            int splitPercentage = int.Parse(rdr["split"].ToString());
        //                            string parentSite = rdr["parent_site"].ToString();
        //                            string fromLocation = rdr["from_location_id"].ToString();
        //                            string fromType = rdr["from_type"].ToString();
        //                            string toLocation = rdr["to_location_id"].ToString();
        //                            string toType = rdr["to_type"].ToString();

        //                            string sourcingRuleHdrKey = string.Empty;
        //                            string sourcingRuleLineItemKey = string.Empty;
        //                            SourcingRuleLineItem ruleItemHdr;

        //                            if (fromType.Equals("KITTING"))
        //                            {
        //                                sourcingRuleHdrKey = routeID;
        //                                ruleItemHdr = new SourcingRuleLineItem(productID, productName, channelID, channelName);
        //                                //if (!DBNull.Value.Equals(rdr.GetDateTime(5)))
        //                                if (!DBNull.Value.Equals(rdr["fromDate"]))
        //                                {
        //                                    ruleItemHdr.FromDateCalId = fromDateCalID;
        //                                    ruleItemHdr.FromDate = rdr.GetDateTime(5);
        //                                }

        //                                //if (!DBNull.Value.Equals(rdr.GetDateTime(7)))
        //                                if (!DBNull.Value.Equals(rdr["toDate"]))
        //                                {
        //                                    ruleItemHdr.ToDateCalId = toDateCalID;
        //                                    ruleItemHdr.ToDate = rdr.GetDateTime(7);
        //                                }

        //                                ruleItemHdr.KitSite = fromLocation;
        //                                ruleItemHdr.BoxSite = toLocation;
        //                                ruleItemHdr.BoxSplit = splitPercentage;

        //                                listSourcingRuleLineHdr.Add(sourcingRuleHdrKey, ruleItemHdr);
        //                            }
        //                            else if (fromType.Equals("BOXING"))
        //                            {
        //                                //define the parent item key (KITTING->BOXING)
        //                                sourcingRuleHdrKey = parentRouteID;

        //                                if (listSourcingRuleLineHdr.ContainsKey(sourcingRuleHdrKey))
        //                                {
        //                                    ruleItemHdr = listSourcingRuleLineHdr[sourcingRuleHdrKey];

        //                                    SourcingRuleLineItem ruleItem = new SourcingRuleLineItem(productID, productName, channelID, channelName);
        //                                    //setup route Id for the parent and current rule
        //                                    ruleItem.RouteID = routeID;
        //                                    ruleItem.ParentRouteID = parentRouteID;

        //                                    ruleItem.FromDateCalId = ruleItemHdr.FromDateCalId;
        //                                    ruleItem.FromDate = ruleItemHdr.FromDate;

        //                                    ruleItem.ToDateCalId = ruleItemHdr.ToDateCalId;
        //                                    ruleItem.ToDate = ruleItemHdr.ToDate;

        //                                    ruleItem.KitSite = ruleItemHdr.KitSite;
        //                                    ruleItem.BoxSite = ruleItemHdr.BoxSite;
        //                                    ruleItem.BoxSplit = ruleItemHdr.BoxSplit;

        //                                    ruleItem.MergeSite = toLocation;
        //                                    ruleItem.MergeSplit = splitPercentage;

        //                                    //sourcingRuleLineItemKey = productID + keyAppender + channelID + keyAppender + fromDateCalID + keyAppender + toDateCalID + keyAppender
        //                                    //                    + ruleItem.KitSite + keyAppender + ruleItem.BoxSite + keyAppender + ruleItem.MergeSite
        //                                    //                    + keyAppender + ruleItem.BoxSplit + keyAppender + ruleItem.MergeSplit;
        //                                    sourcingRuleLineItemKey = routeID + keyAppender + parentRouteID;
        //                                    if (!listSourcingRuleLineItems.ContainsKey(sourcingRuleLineItemKey))
        //                                        listSourcingRuleLineItems.Add(sourcingRuleLineItemKey, ruleItem);
        //                                    //else
        //                                    //    log.InfoFormat("Duplicate SR Line Item Found {0}.", sourcingRuleLineItemKey);

        //                                }
        //                                else
        //                                {
        //                                    sourcingRuleHdrKey = productID + keyAppender + channelID + keyAppender + fromDateCalID + keyAppender + toDateCalID + keyAppender
        //                                                            + parentSite + keyAppender + fromLocation + keyAppender + toLocation + keyAppender
        //                                                            + fromType;
        //                                    //log.InfoFormat("Parent KITTING->BOXING rule not found. Ignored sourcing rule item {0}.", sourcingRuleHdrKey);
        //                                    continue;
        //                                }

        //                            }
        //                            else
        //                            {
        //                                sourcingRuleHdrKey = productID + keyAppender + channelID + keyAppender + fromDateCalID + keyAppender + toDateCalID + keyAppender
        //                                                        + parentSite + keyAppender + fromLocation + keyAppender + toLocation + keyAppender
        //                                                        + fromType;
        //                                //log.InfoFormat("Invalid from type {0}. Ignored sourcing rule item {1}.", new object[] { fromType, sourcingRuleHdrKey });
        //                                continue;
        //                            }


        //                            //prevProductID = productID;
        //                            //prevChannelID = channelID;
        //                            //prevFromDateCalID = fromDateCalID;
        //                            //prevToDateCalID = toDateCalID;

        //                        }
        //                    }

        //                }
        //                //sw.Stop();
        //                //log.InfoFormat("GetSourcingRuleLineItems: Loaded {0} Sourcing Rule Line Items.  In {1}.", new object[] { listSourcingRuleLineItems.Count, sw.Elapsed });

        //            }

        //            return listSourcingRuleLineItems;

        //        }
        //        catch (Exception ex)
        //        {
        //            //log.Fatal("GetSourcingRuleLineItems : Failed to Get information from database table: ", ex);
        //            throw;
        //        }
        //        finally
        //        {
        //            //log.Info("GetSourcingRuleLineItems : End.");
        //        }

        //    });
        //}


        #endregion



    }
}

