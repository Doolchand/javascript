﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dell.RP.DataAccess.IRepository;
using Dell.RP.Common.Models;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using Dell.RP.API.Dell.RP.DataAccess.Common;

namespace Dell.RP.DataAccess.Repository
{
    public class SessionSettingRepository : ISessionSettingRepository
    {
        private readonly string connenctionString;
        private readonly string schemaName;

        public SessionSettingRepository(IOptions<AppSettings> appSettings, ICommonData commonData)
        {
            connenctionString = commonData.DecriptedConnectionString(appSettings.Value.ConnectionStringRP, appSettings.Value.ConnectionKey);
            schemaName = appSettings.Value.SchemaName;

        }

        //Get Session Setting 
        public async Task<string> GetSessionSettingsAsync(UserDetails user)
        {
            return await Task.Run(() =>
            {
                var ddlList = "";
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();
                        var query = schemaName + ".RP_SESSION_PKG.GET_TIMINGS";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam3 = new OracleParameter();
                        objparam3.ParameterName = "P_I_NT_USER_V";
                        objparam3.Value = user.UserID;
                        objparam3.OracleDbType = OracleDbType.Varchar2;
                        objparam3.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(objparam3);

                        OracleParameter objparam = new OracleParameter();
                        objparam.ParameterName = "P_O_IDEAL_TIME";
                        objparam.OracleDbType = OracleDbType.Int16;
                        objparam.Size = 30000;
                        objparam.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam);

                        OracleParameter objparam1 = new OracleParameter();
                        objparam1.ParameterName = "P_O_TIMEOUT";
                        objparam1.OracleDbType = OracleDbType.Int16;
                        objparam1.Size = 30000;
                        objparam1.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter();
                        objparam2.ParameterName = "P_O_ERR_CODE_v";
                        objparam2.OracleDbType = OracleDbType.Varchar2;
                        objparam2.Size = 30000;
                        objparam2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam2);

                        OracleParameter objparam4 = new OracleParameter();
                        objparam4.ParameterName = "P_O_ERR_MSG_V";
                        objparam4.OracleDbType = OracleDbType.Varchar2;
                        objparam4.Size = 30000;
                        objparam4.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(objparam4);

                        cmd.ExecuteNonQuery();
                        ddlList = cmd.Parameters["P_O_IDEAL_TIME"].Value.ToString() + "~" + cmd.Parameters["P_O_TIMEOUT"].Value.ToString();
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return ddlList;
            });
        }

        //Update User Session token and state 
        public async Task<string> UpdateUserSessionAsync(updateUserSessionModel param)
        {
            return await Task.Run(() =>
            {
                var ddlList = "";
                try
                {
                    using (var connection = new OracleConnection(connenctionString))
                    {
                        connection.Open();
                        var query = schemaName + ".RP_SESSION_PKG.UPDATE_SESSION_LOG";
                        OracleCommand cmd = new OracleCommand(query, connection);
                        cmd.CommandType = CommandType.StoredProcedure;

                        OracleParameter objparam1 = new OracleParameter("P_I_NT_USER_V", OracleDbType.Varchar2, ParameterDirection.Input);
                        objparam1.Value = param.UserID;
                        objparam1.Size = 300;
                        cmd.Parameters.Add(objparam1);

                        OracleParameter objparam2 = new OracleParameter("P_I_SESSION_ID_V", OracleDbType.Varchar2, ParameterDirection.Input);
                        objparam2.Value = param.tokenID;
                        objparam2.Size = 300;
                        cmd.Parameters.Add(objparam2);

                        OracleParameter objparam3 = new OracleParameter("P_I_STATE_V", OracleDbType.Varchar2, ParameterDirection.Input);
                        objparam3.Value = param.state;
                        objparam3.Size = 300;
                        cmd.Parameters.Add(objparam3);

                        OracleParameter objparam4 = new OracleParameter("P_I_APPLICATION", OracleDbType.Varchar2, ParameterDirection.Input);
                        objparam4.Value = param.applicationUrl;
                        objparam4.Size = 300;
                        cmd.Parameters.Add(objparam4);

                        OracleParameter objparam5 = new OracleParameter("P_I_PAGE", OracleDbType.Varchar2, ParameterDirection.Input);
                        objparam5.Value = param.UserID;
                        objparam5.Size = 300;
                        cmd.Parameters.Add(objparam5);

                        OracleParameter objparam6 = new OracleParameter("P_O_ERR_CODE_v", OracleDbType.Varchar2, ParameterDirection.Output);
                        objparam6.Size = 300;
                        cmd.Parameters.Add(objparam6);

                        OracleParameter objparam7 = new OracleParameter("P_O_ERR_MSG_V", OracleDbType.Varchar2, ParameterDirection.Output);
                        objparam7.Size = 300;
                        cmd.Parameters.Add(objparam7);

                        cmd.ExecuteNonQuery();
                        ddlList = cmd.Parameters["P_O_ERR_CODE_v"].Value.ToString() + "~" + cmd.Parameters["P_O_ERR_MSG_V"].Value.ToString();
                        connection.Close();

                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    throw ex;
                }

                return ddlList;
            });
        }
    }
}
