﻿using Dell.Common.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.DataAccess.Common
{
   public class CommonData : ICommonData
    {
        public string DecriptedConnectionString(string connectionString, string connectionKey)
        {
            try
            {
                // string Key = default(string);
                // string CurrentEnvironment = default(string);
                // Call my Appsetting config file which will provide me all the config values.
                // CurrentEnvironment = AppSettingConfig.getEnvironemnt();
                // Key = AppSettingConfig.getDecryptionKey();

                if (!string.IsNullOrWhiteSpace(connectionString))
                {
                    connectionString = CryptoEngine.Decrypt(connectionString, connectionKey, true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

                return connectionString;
            }
    }
}
