﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.DataAccess.Common
{
    public interface ICommonData
    {
        string DecriptedConnectionString(string connectionString, string connectionKey);
    }
}
