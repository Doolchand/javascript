﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.DataAccess
{
   public class AppSettings
    {
        public String ConnectionStringSCDH { get; set; }

        public string ConnectionStringRP { get; set; }

        public String SchemaName { get; set; }

        public string ConnectionKey { get; set; }

        public string ConnectionStringGMP { get; set; }
    }
}
