﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace Dell.RP.API
{
    public class ClaimsTransformer : IClaimsTransformation
    {
        // Can consume services from DI as needed, including scoped DbContexts
        public ClaimsTransformer(IHttpContextAccessor httpAccessor) { }
        public Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            ((ClaimsIdentity)principal.Identity).AddClaim(new Claim("ExampleClaim", "true"));            
            return Task.FromResult(principal);
        }

        //public Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        //{
        //    ((ClaimsIdentity)principal.Identity).AddClaim(
        //        new Claim("ExampleClaim", "true"));
        //    return Task.FromResult(principal);
        //}
    }
}
