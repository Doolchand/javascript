﻿using Dell.RP.API.Controllers.HelperClasses;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.DemandSplit;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.RoutingRules;
using Dell.RP.API.Dell.RP.Common.DemandSplit;
using Dell.RP.API.Dell.RP.Common.Models.DemandSplit;
using Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.Common.Models.RoutingRules;
using Dell.RP.Common.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;

namespace Dell.RP.API.Controllers.RoutingRules
{


    [Route("api/[controller]")]
    [Authorize(Policy = "AllUsers")]

    public class RoutingRulesController : Controller
    {

        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IRoutingRules _IRoutingRules;
        private string userName = "";
        static DataTable dtSearch = null;
        string ReturnMessage = "";

        public RoutingRulesController(IRoutingRules _IRoutingRules, IHttpContextAccessor httpContextAccessor, IClaimsTransformation d, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            this._IRoutingRules = _IRoutingRules;
            var s = UserNameValidation.GetUser(httpContextAccessor);
            if (s.Result != null)
            {
                this.userName = s.Result;
            }
        }



        [HttpGet]
        [Route("GetGeographyData")]
        public async Task<ObjectResult> GetGeographyData()
        {
            ObjectResult response;
            // iEmcProductHierarchyMaintenance.UserID = this.userName;
            try
            {
                var Product = await _IRoutingRules.GetGeographyData();
                if (Product != null)
                {
                    response = new ObjectResult(Product);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }





        [HttpGet]
        [Route("GetManufacturingSiteData")]
        public async Task<ObjectResult> GetManufacturingSiteData( string key)
        {
            ObjectResult response;
            // iEmcProductHierarchyMaintenance.UserID = this.userName;
            try
            {
                var Product = await _IRoutingRules.GetManufacturingSiteData(key);
                if (Product != null)
                {
                    response = new ObjectResult(Product);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }





        [HttpGet]
        [Route("GetFulfilment")]
        public async Task<ObjectResult> GetFulfilment()
        {
            ObjectResult response;
            // iEmcProductHierarchyMaintenance.UserID = this.userName;
            try
            {
                var Product = await _IRoutingRules.GetFulfilment();
                if (Product != null)
                {
                    response = new ObjectResult(Product);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }




        



         [HttpGet]
        [Route("GetDate")]
        public async Task<ObjectResult> GetDate()
        {
            ObjectResult response;
            // iEmcProductHierarchyMaintenance.UserID = this.userName;
            try
            {
                var Product = await _IRoutingRules.GetDate();
                if (Product != null)
                {
                    response = new ObjectResult(Product);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        




        [HttpGet]
        [Route("GetEffectiveEndDate")]
        public async Task<ObjectResult> GetEffectiveEndDate()
        {
            ObjectResult response;
            // iEmcProductHierarchyMaintenance.UserID = this.userName;
            try
            {
                var Product = await _IRoutingRules.GetEffectiveEndDate();
                if (Product != null)
                {
                    response = new ObjectResult(Product);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }






        [HttpGet]
        [Route("GetFGA")]
        public async Task<ObjectResult> GetFGA()
        {
            ObjectResult response;
            // iEmcProductHierarchyMaintenance.UserID = this.userName;
            try
            {
                var Product = await _IRoutingRules.GetFGA();
                if (Product != null)
                {
                    response = new ObjectResult(Product);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }


        // 



        [HttpPost]
        [Route("SaveRauting")]
        public async Task<ObjectResult> SaveRauting([FromBody] List< RoutingRulesUpdategrid> Search)
        {
            ObjectResult response;
            //    iEmcProductHierarchyMaintenance.UserID = this.userName;


            try
            {
                var Searchresult = "";//addRoutingRule(Search);//await _IDemandSplit.GetDemandSplitViewResult(Search);

                //   DataTable Searchresult = searchRoutingRules(Search);
                SaveRoutingRulesData(Search);
                //if (fileNameList != null && fileNameList.Count > 0)
                if (ReturnMessage != null)
                {
                    response = new ObjectResult(ReturnMessage);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        void SaveRoutingRulesData(List<RoutingRulesUpdategrid> Search)
        {
            DataTable dtData = null;
            // SPWeb rootSite = null; Doolchand 
            //  SPSite siteColl = null;Doolchand
            DateTime date;
            //   SPUser spUser = null;Doolchand
            string strId;
            string strRoutingId;
            string strManufacturingSite;
            string strFulfillmentSite;
            string strRoutingStartDate = null;
            string strRoutingEndDate = null;
            string strPrimaryRouting;
            string strAlternateRouting1;
            string strAlternateRouting2;
            string strProduct = null;
            string strFga = null;
            string strUserName;
            string strType = null;
            string strStatus = null;
            string strItemID = null;
            string strMFGStartDate = null;
            string strMFGEndDate = null;
            string strSplitPercent = null;
            bool isSuccess = false;
          //  heirarchydatap = new HierarchyDataProvider();
            string strItemDescription = null;

            try
            {
                //if (ViewState["dtRoutingRule"] != null)
                //{
                  //  dtData = (DataTable)ViewState["dtRoutingRule"];

                    //siteColl = SPContext.Current.Site;
                    //rootSite = siteColl.RootWeb;
                    //spUser = rootSite.CurrentUser;
                    //strUserName = spUser.LoginName;
                    date = DateTime.Now;

                    if (Search[Search.Count - 1].product == "ALL")
                    {
                        strProduct = "ALL_PRODUCT";
                        strFga = null;
                        strType = "PRODUCT_TREE";
                    }
                    else
                        if (Search[Search.Count - 1].product == "PRODUCT_TREE")
                    {
                        strItemID = Search[Search.Count - 1].TreeValue;

                        //Check if this is required or not.
                        //strItemDescription = heirarchydatap.GetBaseCodeDescription(strItemID);

                        if (strItemID.Contains("("))
                        {
                            strProduct = strItemID.Substring(strItemID.IndexOf("(") + 1, strItemID.IndexOf(")") - strItemID.IndexOf("(") - 1);
                        }
                        else
                        {
                            strProduct = Search[Search.Count - 1].TreeValue;
                        }

                        strFga = null;
                        strType = "PRODUCT_TREE";
                    }
                    else
                     if (Search[Search.Count - 1].product == "PRODUCT_TREE")
                    {
                        strItemID = Search[Search.Count - 1].TreeValue;

                        if (strItemID.Contains("("))
                        {
                            strProduct = strItemID.Substring(strItemID.IndexOf("(") + 1, strItemID.IndexOf(")") - strItemID.IndexOf("(") - 1);
                        }
                        else
                        {
                            strProduct = Search[Search.Count - 1].TreeValue;
                        }
                        strFga = null;
                        strType = "PRODUCT_TREE";
                    }
                    else
                            if (Search[Search.Count - 1].product == "FHC")
                    {
                        strProduct = null;
                        strItemID = Search[Search.Count - 1].TreeValue;
                        if (strItemID.Contains("("))
                        {
                            strFga = strItemID.Substring(0, strItemID.IndexOf("("));
                        }
                        else
                        {
                            strFga = Search[Search.Count - 1].TreeValue;
                        }

                        if (Search[Search.Count - 1].hdfTypeRetrieve.Equals("FGA_ITEM"))
                        {
                            strType = "FGA_ITEM";
                        }
                        else
                            if (Search[Search.Count - 1].hdfTypeRetrieve.Equals("FHC_ITEM"))
                        {
                            strType = "FHC_ITEM";
                        }
                    }
                    else
                    {
                        /*---Modified By Sanjeeva---*/
                        //FGA
                        strProduct = null;
                        strFga = Search[Search.Count - 1].hdfTypeRetrieve;
                        strType = "FGA_ITEM";

                        strProduct = null;
                        strItemID = Search[Search.Count - 1].TreeValue;
                        if (strItemID.Contains("("))
                        {
                            strFga = strItemID.Substring(0, strItemID.IndexOf("("));
                        }
                        else
                        {
                            strFga = Search[Search.Count - 1].TreeValue;
                        }

                        if (Search[Search.Count - 1].hdfTypeRetrieve.Equals("FGA_ITEM"))
                        {
                            strType = "FGA_ITEM";
                        }


                    }
                    strRoutingId = date.Ticks.ToString();

                    //Fetch the MFG Dates.
                    strMFGStartDate = Search[Search.Count - 1].Mfg_Site_Effective_Begin_Date;// ddlMFGFiscalBeginDate.SelectedItem.Value;
                    strMFGEndDate = Search[Search.Count - 1].Mfg_Site_Effective_End_Date;//ddlMFGFiscalEndDate.SelectedItem.Value;

                    RoutingRuleLines routingRuleLines = null;
                   // int totalRows = grdCreate.Rows.Count; Doolchand

                    RoutingRule routingRule = new RoutingRule(strRoutingId, strProduct, strFga, strType, strMFGStartDate, strMFGEndDate);

                    string strRoutingIdRule;
                    double intermediateSplitTotal = 0;
                    string strRoutingIDGrid;
                    List<double> intermediateSplitTotalList = new List<double>();
                   // dtData.DefaultView.Sort = "MFG_EFFECTIVE_START_DATE,MANUFACTURING_SITE_ID,MFG_SITE_SPLIT_PERCENT" + " " + "ASC";
                    double interimResult = 0;
                    bool IsInterimGridValid = false;
                    bool AreDatesValid = false;

                    string mfgSite = "", ffSite = "";
                    string startMFGDate = "", endMFGDate = "";

                    //Split and MFG date Validation starts here.
                    //Calculate the split total in the intermediate grid also.
                    for (int i = 0; i < Search.Count; i++)
                    {
                        

                        if (Search[i].strRoutingIDGrid.ToString() == "")
                        {
                            if (!string.IsNullOrEmpty(Search[i].mfG_SITE_SPLIT_PERCENT.ToString()))
                            {
                                intermediateSplitTotal += Convert.ToDouble(Search[i].mfG_SITE_SPLIT_PERCENT.ToString());
                            }
                        }

                         
                    }

                    interimResult = intermediateSplitTotal % 100.0;

                    if (interimResult == 0.0)
                    {
                        IsInterimGridValid = true;
                    }

                    bool blNoChangeFlag = true;
                    //MFG Dates in the intermediate grid should be same.
                    //Validation part ends here.

                    if (IsInterimGridValid)
                    {
                        for (int i = 0; i < Search.Count; i++)
                        {
                            //strMFGStartDate = dtData.Rows[i]["ROUTING_EFFECTIVE_START_DATE"].ToString();
                            //strMFGEndDate = dtData.Rows[i]["ROUTING_EFFECTIVE_START_DATE"].ToString();


                            //RoutingRule routingRule = new RoutingRule(strRoutingId, strProduct, strFga, strType, strMFGStartDate, strMFGEndDate);

                            //strId = dtData.Rows[i]["ID"].ToString();
                            //strManufacturingSite = dtData.Rows[i]["MANUFACTURING_SITE_ID"].ToString();
                            //strSplitPercent = dtData.Rows[i]["MFG_SITE_SPLIT_PERCENT"].ToString();
                            //strFulfillmentSite = dtData.Rows[i]["FULFILLMENT_SITE_ID"].ToString();
                            //strRoutingStartDate = dtData.Rows[i]["ROUTING_EFFECTIVE_START_DATE"].ToString();
                            //strRoutingEndDate = dtData.Rows[i]["ROUTING_EFFECTIVE_END_DATE"].ToString();
                            //strPrimaryRouting = dtData.Rows[i]["PRIMARY_ROUTING"].ToString();
                            //strAlternateRouting1 = dtData.Rows[i]["ALTERNATE_ROUTING_1"].ToString();
                            //strAlternateRouting2 = dtData.Rows[i]["ALTERNATE_ROUTING_2"].ToString();
                            //strRoutingIdRule = dtData.Rows[i]["ID"].ToString();
                            //strRoutingIDGrid = dtData.Rows[i]["ROUTING_ID"].ToString();
                            //strStatus = dtData.Rows[i]["STATUS"].ToString();

                            strId = Search[i].id;
                            strManufacturingSite = Search[i].manufacturinG_SITE_ID;
                            strSplitPercent = Search[i].mfG_SITE_SPLIT_PERCENT;
                            strFulfillmentSite = Search[i].fulfillmenT_SITE_ID;
                            strRoutingStartDate = Search[i].Routing_Effective_Start_Date;
                            strRoutingEndDate = Search[i].Routing_Effective_End_Date;
                            strPrimaryRouting = Search[i].primarY_ROUTING;
                            strAlternateRouting1 = Search[i].Alternate_Routing1;
                            strAlternateRouting2 = Search[i].Alternate_Routing2;
                            strRoutingIdRule = Search[i].strRoutingIdRule;
                            strRoutingIDGrid = Search[i].strRoutingIDGrid;
                            strStatus = Search[i].strRoutingIDGrid;




                            routingRuleLines = new RoutingRuleLines(strId, strRoutingIDGrid, strFulfillmentSite, strManufacturingSite, strRoutingStartDate, strRoutingEndDate, strPrimaryRouting, strAlternateRouting1, strAlternateRouting2, strStatus, strSplitPercent);
                            routingRule.addRoutingRuleLines(routingRuleLines);

                            if (strStatus == "")
                            {
                                blNoChangeFlag = false;
                                if (InsertValidateDates(strProduct, strFga, strFulfillmentSite, strManufacturingSite, strMFGStartDate, strMFGEndDate, strRoutingStartDate, strRoutingEndDate))
                                {
                                    AreDatesValid = true;
                                }
                                else
                                {
                                    AreDatesValid = false;
                                    break;
                                }
                            }
                            else if (strRoutingIDGrid  != "")
                            {
                                blNoChangeFlag = false;
                                if (UpdateValidateDates(strRoutingIDGrid, strId, strManufacturingSite, strFulfillmentSite, strMFGStartDate, strMFGEndDate, strRoutingStartDate, strRoutingEndDate))
                                {
                                    AreDatesValid = true;
                                }
                                else
                                {
                                    AreDatesValid = false;
                                    break;
                                }
                            }
                            else if (strStatus == "NOCHANGE")
                            {
                                AreDatesValid = true;
                            }
                        }



                        if ((!blNoChangeFlag))
                        {
                           // routingRulesDataProvider = new RoutingRulesDataProvider();

                            if (AreDatesValid)
                            {
                                isSuccess = _IRoutingRules.addRoutingRule(routingRule, this.userName);

                                if (isSuccess)
                                {

                                  ReturnMessage = "1";
                                //divRetriveCreate.Enabled = false;
                                //ddlFulfillmentSite.Enabled = true;
                                //ddlFulfillmentSite.SelectedIndex = -1;
                                //ddlPrimaryRouting.SelectedIndex = -1;
                                //ddlAlternateRouting1.SelectedIndex = -1;
                                //ddlAlternateRouting2.SelectedIndex = -1;
                                ////ViewState["dtRoutingRule"] = null;
                                //ViewState["UpdateRowNo"] = null;
                                //lblMsg.Text = ConfigurationManager.AppSettings["msgSuccess"].ToString();
                                //lblMsg.ForeColor = System.Drawing.Color.Green;
                                //imgSuccess.Visible = true;
                                //btnUpdate.Enabled = true;
                                //btnDeleteRule.Enabled = true;
                                //ViewState["isParmanentData"] = "1";
                                //retrieve();
                                //clearAll();
                                //rulerAfterRetrieve.Visible = false;
                                //pnlRoutingRuleCreate.Visible = false;
                                //pnlMfgSitesInfo.Visible = false;
                                //Add clear all function call here.
                            }
                                else
                                {
                                    if (AreDatesValid)
                                    {
                                        //lblMsg.Text = ConfigurationManager.AppSettings["msgException"].ToString();
                                        //lblMsg.ForeColor = System.Drawing.Color.Red;
                                        //imgError.Visible = true;
                                    }
                                }

                               // lblNoRuleCongifureMsg.Visible = false;

                            }
                            else
                            {
                                //lblMsg.ForeColor = System.Drawing.Color.Red;
                                //imgError.Visible = true;
                            }
                        }
                        else
                        {
                            //divRetriveCreate.Enabled = false;
                            //ddlFulfillmentSite.Enabled = true;
                            //ddlFulfillmentSite.SelectedIndex = -1;
                            //ddlPrimaryRouting.SelectedIndex = -1;
                            //ddlAlternateRouting1.SelectedIndex = -1;
                            //ddlAlternateRouting2.SelectedIndex = -1;
                           
                            //ViewState["UpdateRowNo"] = null;
                            //lblMsg.Text = ConfigurationManager.AppSettings["msgSuccess"].ToString();
                            //lblMsg.ForeColor = System.Drawing.Color.Green;
                            //imgSuccess.Visible = true;
                            //btnUpdate.Enabled = true;
                            //btnDeleteRule.Enabled = true;
                            //ViewState["isParmanentData"] = "1";
                            //retrieve();
                            //clearAll();
                            //rulerAfterRetrieve.Visible = false;
                            //pnlRoutingRuleCreate.Visible = false;
                            //pnlMfgSitesInfo.Visible = false;

                        }

                    }//brace ends for split check.
                    else
                    {
                    ReturnMessage = "One of the Rule,Split Percentage is not 100.";
                    //lblMsg.Text = "One of the Rule,Split Percentage is not 100.";
                    //lblMsg.ForeColor = System.Drawing.Color.Red;
                    //imgError.Visible = true;
                }
             


              //  searchRoutingRules();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public bool InsertValidateDates(string productID, string item, string fulfillmentSite, string mfgSite, string mfgStartDate, string mfgEndDate, string routingStartDate, string routingEndDate)
        {
            //Convert the strings into standard date formats.

            //Basic validation for the MFG Dates.
            if (mfgStartDate != null && mfgEndDate != null && mfgStartDate != " " && mfgEndDate != " " && mfgStartDate != string.Empty && mfgEndDate != string.Empty)
            {
                if (DateTime.Parse(mfgStartDate) > DateTime.Parse(mfgEndDate))
                {
                    // lblMsg.Text = "Mfg Effective Start Date must be less than Mfg Effective End Date";

                    ReturnMessage = "Mfg Effective Start Date must be less than Mfg Effective End Date";
                    return false;
                }
            }
            //Basic validation for the RR Dates.
            if (routingStartDate != null && routingEndDate != null && routingStartDate != " " && routingEndDate != " " && routingStartDate != string.Empty && routingEndDate != string.Empty)
            {
                if (DateTime.Parse(routingStartDate) > DateTime.Parse(routingEndDate))
                {
                    //  lblMsg.Text = "Routing Effective Start Date must be less than Routing Effective End Date";
                    ReturnMessage = "Routing Effective Start Date must be less than Routing Effective End Date";
                    return false;
                }
            }

            //Call validation function by passing the dates.
            //routingRulesDataProvider = new RoutingRulesDataProvider();

            //if (routingRulesDataProvider.InsertValidateEffectiveDates(productID, item, fulfillmentSite, mfgSite, mfgStartDate, mfgEndDate, routingStartDate, routingEndDate))
            //{
            //    return true;
            //}
            //else
            //{
            //    lblMsg.Text = "Dates are overlapping with existing rules.";
            //    return false;
            //}

            return true;
        }

        public bool UpdateValidateDates(string strRoutingId, string ID, string strManufacturingSite, string strFulfillmentSiteID, string strMFGStartDate, string strMFGEndDate, string strRoutingStartDate, string strRoutingEndDate)
        { 
            if (strMFGStartDate != null && strMFGEndDate != null && strMFGStartDate != " " && strMFGEndDate != " " && strMFGStartDate != string.Empty && strMFGEndDate != string.Empty)
            {
                if (DateTime.Parse(strMFGStartDate) > DateTime.Parse(strMFGEndDate))
                {
                    // lblMsg.Text = "Mfg Effective Start Date must be less than Mfg Effective End Date";
                    ReturnMessage = "Mfg Effective Start Date must be less than Mfg Effective End Date";
                    return false;
                }
            }
            
            if (strRoutingStartDate != null && strRoutingEndDate != null && strRoutingStartDate != " " && strRoutingEndDate != " " && strRoutingStartDate != string.Empty && strRoutingEndDate != string.Empty)
            {
                if (DateTime.Parse(strRoutingStartDate) > DateTime.Parse(strRoutingEndDate))
                {
                    //lblMsg.Text = "Routing Effective Start Date must be less than Routing Effective End Date";
                    ReturnMessage = "Routing Effective Start Date must be less than Routing Effective End Date";
                    return false;
                }
            }

             
            //routingRulesDataProvider = new RoutingRulesDataProvider(); Doolchand 

            //if (routingRulesDataProvider.UpdateValidateEffectiveDates(strRoutingId, ID, strManufacturingSite, strFulfillmentSiteID, strMFGStartDate, strMFGEndDate, strRoutingStartDate, strRoutingEndDate))
            //{
            //    return true;
            //}
            //else
            //{
            //  //  lblMsg.Text = "Dates are overlapping with existing rules.";
            //    return false;
            //}

            return true;
        }


























        [HttpPost]
        [Route("DateValidate")]
        public ObjectResult DateValidate([FromBody] List<RoutingRulesUpdategrid> Search)
        {
            ObjectResult response;
            //    iEmcProductHierarchyMaintenance.UserID = this.userName;

            bool AreRoutingDatesValid;
            try
            {
                string Searchresult = "true";//addRoutingRule(Search);//await _IDemandSplit.GetDemandSplitViewResult(Search);


                for (int i = 0; i < Search.Count; i++)  
                {
                    //  if (i != updateRowNo)
                    //  {
                    string startMFGDate = Search[i].Mfg_Site_Effective_Begin_Date;//dtRoutingRule.Rows[i]["MFG_EFFECTIVE_START_DATE"].ToString();
                    string endMFGDate = Search[i].Mfg_Site_Effective_End_Date;// dtRoutingRule.Rows[i]["MFG_EFFECTIVE_END_DATE"].ToString();
                    string mfgSite = Search[i].manufacturinG_SITE_ID; //dtRoutingRule.Rows[i]["MANUFACTURING_SITE_ID"].ToString();
                    string ffSite = Search[i].fulfillmenT_SITE_ID;// dtRoutingRule.Rows[i]["FULFILLMENT_SITE_ID"].ToString();
                    string oldRoutingStartDate = Search[i].Routing_Effective_Start_Date;//dtRoutingRule.Rows[i]["ROUTING_EFFECTIVE_START_DATE"].ToString();
                    string oldRoutingEndDate = Search[i].Routing_Effective_End_Date;//dtRoutingRule.Rows[i]["ROUTING_EFFECTIVE_END_DATE"].ToString();


                    string strMFGSiteName = Search[0].strMFGSiteName;
                    string strMFGBeginDate = Search[0].strMFGBeginDate;
                    string strMFGEndDate = Search[0].strMFGEndDate;
                    string strFFSite = Search[0].strFFSite;

                    string strNewRoutingBeginDate = Search[0].strNewRoutingBeginDate;
                    string strNewRoutingEndDate = Search[0].strNewRoutingEndDate;

                    if (mfgSite == strMFGSiteName && startMFGDate == strMFGBeginDate && endMFGDate == strMFGEndDate && ffSite == strFFSite)
                        {
                        //Call the DB method and validate the date ranges.
                        AreRoutingDatesValid = _IRoutingRules.ValidateRoutingDates(oldRoutingStartDate, oldRoutingEndDate, strNewRoutingBeginDate, strNewRoutingEndDate);

                        if (!AreRoutingDatesValid)
                        {
                            Searchresult = "False";
                            break;// response;
                        }                       
                    }
                }

                if ( Searchresult!=null )
                {
                    response = new ObjectResult(Searchresult);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }





        //void  addRoutingRule(List<RoutingRulesUpdategrid> Search)
        //{

        //}








        [HttpPost]
        [Route("GetRoutingRulesViewResult")]
        public async Task<ObjectResult> GetRoutingRulesViewResult([FromBody] RoutingRulesSearchPara Search)
        {
            ObjectResult response;
            //    iEmcProductHierarchyMaintenance.UserID = this.userName;


            try
            {
                //var Searchresult = "";//await _IDemandSplit.GetDemandSplitViewResult(Search);
                List<RoutingRulesMainGrid> RetriveGrid = new List<RoutingRulesMainGrid>();
                DataTable Searchresult =  searchRoutingRules(Search);

                RetriveGrid = FillTable(Searchresult);
                //if (fileNameList != null && fileNameList.Count > 0)
                if (RetriveGrid != null)
                {
                    response = new ObjectResult(RetriveGrid);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }



        private List<RoutingRulesMainGrid> FillTable(DataTable dt)
        {
            List<RoutingRulesMainGrid> lstFulfillmentData = new List<RoutingRulesMainGrid>();


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var output = new RoutingRulesMainGrid();
                //output.Id = String.Empty;
                if (dt.Rows[i]["ROUTING_ID"] != System.DBNull.Value)
                {
                    output.ROUTING_ID = Convert.ToString(dt.Rows[i]["ROUTING_ID"]);
                }

                if (dt.Rows[i]["PRODUCTFGA"] != System.DBNull.Value)
                {
                    output.PRODUCTFGA = Convert.ToString(dt.Rows[i]["PRODUCTFGA"]);
                }

                if (dt.Rows[i]["FLAG"] != System.DBNull.Value)
                {
                    output.FLAG = Convert.ToString(dt.Rows[i]["FLAG"]);
                }

                if (dt.Rows[i]["PRODUCT_ID"] != System.DBNull.Value)
                {
                    output.PRODUCT_ID = Convert.ToString(dt.Rows[i]["PRODUCT_ID"]);
                }

                if (dt.Rows[i]["ITEM"] != System.DBNull.Value)
                {
                    output.ITEM = Convert.ToString(dt.Rows[i]["ITEM"]);
                }

                if (dt.Rows[i]["TYPE"] != System.DBNull.Value)
                {
                    output.TYPE = Convert.ToString(dt.Rows[i]["TYPE"]);
                }

                if (dt.Rows[i]["FULFILLMENT_SITE_ID"] != System.DBNull.Value)
                {
                    output.FULFILLMENT_SITE_ID = Convert.ToString(dt.Rows[i]["FULFILLMENT_SITE_ID"]);
                }

                if (dt.Rows[i]["MANUFACTURING_SITE_ID"] != System.DBNull.Value)
                {
                    output.MANUFACTURING_SITE_ID = Convert.ToString(dt.Rows[i]["MANUFACTURING_SITE_ID"]);
                }

                if (dt.Rows[i]["PRIMARY_ROUTING"] != System.DBNull.Value)
                {
                    output.PRIMARY_ROUTING = Convert.ToString(dt.Rows[i]["PRIMARY_ROUTING"]);
                }

                if (dt.Rows[i]["ALTERNATE_ROUTING_1"] != System.DBNull.Value)
                {
                    output.ALTERNATE_ROUTING_1 = Convert.ToString(dt.Rows[i]["ALTERNATE_ROUTING_1"]);
                }

                if (dt.Rows[i]["ALTERNATE_ROUTING_2"] != System.DBNull.Value)
                {
                    output.ALTERNATE_ROUTING_2 = Convert.ToString(dt.Rows[i]["ALTERNATE_ROUTING_2"]);
                }

                if (dt.Rows[i]["ROUTING_EFFECTIVE_START_DATE"] != System.DBNull.Value)
                {
                    output.ROUTING_EFFECTIVE_START_DATE = Convert.ToString(dt.Rows[i]["ROUTING_EFFECTIVE_START_DATE"]);
                }

                if (dt.Rows[i]["ROUTING_EFFECTIVE_END_DATE"] != System.DBNull.Value)
                {
                    output.ROUTING_EFFECTIVE_END_DATE = Convert.ToString(dt.Rows[i]["ROUTING_EFFECTIVE_END_DATE"]);
                }

                if (dt.Rows[i]["MFG_EFFECTIVE_START_DATE"] != System.DBNull.Value)
                {
                    output.MFG_EFFECTIVE_START_DATE = Convert.ToString(dt.Rows[i]["MFG_EFFECTIVE_START_DATE"]);
                }



                if (dt.Rows[i]["MFG_EFFECTIVE_END_DATE"] != System.DBNull.Value)
                {
                    output.MFG_EFFECTIVE_END_DATE = Convert.ToString(dt.Rows[i]["MFG_EFFECTIVE_END_DATE"]);
                }

                if (dt.Rows[i]["MFG_SITE_SPLIT_PERCENT"] != System.DBNull.Value)
                {
                    output.MFG_SITE_SPLIT_PERCENT = Convert.ToString(dt.Rows[i]["MFG_SITE_SPLIT_PERCENT"]);
                }



                if (dt.Rows[i]["SYS_LAST_MODIFIED_BY"] != System.DBNull.Value)
                {
                    output.SYS_LAST_MODIFIED_BY = Convert.ToString(dt.Rows[i]["SYS_LAST_MODIFIED_BY"]);
                }

                if (dt.Rows[i]["SYS_LAST_MODIFIED_DATE"] != System.DBNull.Value)
                {
                    output.SYS_LAST_MODIFIED_DATE = Convert.ToString(dt.Rows[i]["SYS_LAST_MODIFIED_DATE"]);
                }





                lstFulfillmentData.Add(output);
            }

            return lstFulfillmentData;
        }












        private DataTable searchRoutingRules(RoutingRulesSearchPara Search)
        {
            string strProductId = null;
            string strType = null;
            string strGeo = null;
            //DataTable dtSearch = null;

            if(Search.lblproduct=="ALL")
            {
                Search.Fhctree = "";
            }

            string strItemID = null;
            try
            {
                if (Search.Product == "ALL")
                {
                    strProductId = null;
                    strType = "All";
                }
                else
                    if (Search.Product == "PRODUCT_TREE")
                {
                    if (string.IsNullOrEmpty(Search.Fhctree) || Search.Fhctree == "ALL_PRODUCT")
                    {
                        strProductId = null;
                        strType = "All";
                    }
                    else
                    {
                        strItemID = Search.Fhctree;
                        strItemID = strItemID.Substring(strItemID.IndexOf("(") + 1, strItemID.IndexOf(")") - strItemID.IndexOf("(") - 1);                            
                        strProductId = strItemID;
                        //strProductId = Search.Fhctree;
                        strType = "ProductTree";
                    }
                }
                else
             if (Search.Product == "PRODUCT_TREE")
                {
                    if (string.IsNullOrEmpty(Search.Fhctree) || Search.Fhctree == "ALL_PRODUCT")
                    {
                        strProductId = null;
                        strType = "All";
                    }
                    else
                    {
                        //strItemID = hdfSearch.Value;
                        //strItemID = strItemID.Substring(strItemID.IndexOf("(") + 1, strItemID.IndexOf(")") - strItemID.IndexOf("(") - 1);
                        //strProductId = strItemID;
                        strProductId = Search.Fhctree;
                        strType = "ProductTree";
                    }
                }
                else
           if (Search.Product == "FHC_TREE")
                {
                    if (Search.lblproduct == "ALL")
                    {
                        strProductId = "ALL";
                        strType = "FGA";
                    }
                    if (Search.hdfTypeSearch == "FGA_ITEM")
                    {
                        if (!string.IsNullOrEmpty(Search.Fhctree))
                        {
                            strItemID = Search.Fhctree;
                            strItemID = strItemID.Substring(0, strItemID.IndexOf("("));
                            strProductId = strItemID;
                            //strProductId = hdfSearch.Value;
                            strType = "FGA";
                        }
                    }
                    else
                    if (!string.IsNullOrEmpty(Search.Fhctree))
                    {
                        strItemID = Search.Fhctree;
                        strItemID = strItemID.Substring(0, strItemID.IndexOf("("));
                        strProductId = strItemID;
                        //strProductId = hdfSearch.Value;
                        strType = "FHC";
                    }
                    else
                    {
                        strProductId = "ALL";
                        strType = "FHC";
                    }
                }
                else
                {
                    ///  strProductId = ddlFGAs.SelectedValue;
                    if (!string.IsNullOrEmpty(Search.Fhctree))
                    {
                        strProductId = Search.Fhctree; ///modified by Sanjeeva
                        strProductId = strProductId.Substring(0, strProductId.IndexOf("("));                               ///
                        strType = "FGA";
                    }
                }
                if (Search.Geo != "ALL")
                {
                    strGeo = Search.Geo;
                }


                //strProductId = "ALL";
                //strType = "FHC";
                //strGeo = Search.Geo;

                //  routingRulesDataProvider = new RoutingRulesDataProvider();
                dtSearch = _IRoutingRules.searchRoutingRules(strProductId, strType, strGeo);


                if (dtSearch != null && dtSearch.Rows.Count > 0)
                {
                    dtSearch.Rows[0]["FLAG"] = "0";
                    for (int rownum = 1; rownum < dtSearch.Rows.Count; rownum++)
                    {
                        //if (dtSearch.Rows[rownum]["ROUTING_ID"].ToString().Equals(dtSearch.Rows[rownum - 1]["ROUTING_ID"].ToString()))
                        if (dtSearch.Rows[rownum]["ROUTING_ID"].ToString().Equals(dtSearch.Rows[rownum - 1]["ROUTING_ID"].ToString()) && dtSearch.Rows[rownum]["MFG_EFFECTIVE_START_DATE"].ToString().Equals(dtSearch.Rows[rownum - 1]["MFG_EFFECTIVE_START_DATE"].ToString()) && dtSearch.Rows[rownum]["MFG_EFFECTIVE_END_DATE"].ToString().Equals(dtSearch.Rows[rownum - 1]["MFG_EFFECTIVE_END_DATE"].ToString()) && dtSearch.Rows[rownum]["FULFILLMENT_SITE_ID"].ToString().Equals(dtSearch.Rows[rownum - 1]["FULFILLMENT_SITE_ID"].ToString()))
                        {
                            dtSearch.Rows[rownum]["FLAG"] = "1";
                        }
                        else
                        {
                            dtSearch.Rows[rownum]["FLAG"] = "0";
                        }
                    }
                    dtSearch.AcceptChanges();
                  //  pnlRRules.Visible = true;
                    DataView dataView = dtSearch.DefaultView;
                    dataView.Sort = "ProductFga ASC";

                    return dtSearch;
                //    grdRoutingRules.DataSource = dataView;
                //    grdRoutingRules.DataBind();

                }
                else
                {
                   // pnlRRules.Visible = false;
                    dtSearch = new DataTable();
                  //  grdRoutingRules.DataSource = dtSearch;
                   // grdRoutingRules.DataBind();
                  //  lblNoRoutingRuleMsg.Visible = true;
                  //  lblNoRoutingRuleMsg.Text = ConfigurationManager.AppSettings["msgNoRulesConf"].ToString();

                }
                //ViewState["ProductFgaId"] = strProductId;
                //ViewState["Type"] = strType;
                //ViewState["Region"] = strGeo;


                return dtSearch;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        [HttpPost]
        [Route("GetRoutingDropdownvalue")]
        public async Task<ObjectResult> GetRoutingDropdownvalue([FromBody] ItemType sitesvalue)
        {
            ObjectResult response;
            response = null;
           // _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = await _IRoutingRules.GetRoutingDropdownvalue(sitesvalue.ItemTypeId, sitesvalue.ItemTypeName);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }





        [HttpPost]
        [Route("GetRoutingRuladdupdateGrid")]
        public async Task<ObjectResult> GetRoutingRuladdupdateGrid([FromBody] ItemType sitesvalue)
        {
            ObjectResult response;
            response = null;
            // _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = _IRoutingRules.GetRoutingRuladdupdateGrid(sitesvalue.ItemTypeId, sitesvalue.ItemTypeName);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }




        [HttpPost]
        [Route("DeleteRoutingRules")]
        public async Task<ObjectResult> DeleteRoutingRules([FromBody] List <ItemType> sitesvalue)
        {
            ObjectResult response;
            response = null;
            // _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {

                //string key = "GEO";
                bool isSuccess = _IRoutingRules.DeleteRoutingRules(sitesvalue);

                if (isSuccess == true)
                {
                    response = new ObjectResult(isSuccess);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }








        [HttpPost]
        [Route("RetriveRoutingRule")]
        public async Task<ObjectResult> RetriveRoutingRule([FromBody] RoutingRulesSearchPara Search)
        {
            ObjectResult response;
            //    iEmcProductHierarchyMaintenance.UserID = this.userName;

            List<RoutingRulesUpdategrid> RetriveGrid = new List<RoutingRulesUpdategrid>();

            
            DataTable Searchresult =   retrieve(Search);

            RetriveGrid = ConvertFulfillmentDTtoList(Searchresult);
         //  RetriveGrid = ConvertDataTable<RoutingRulesUpdategrid>(Searchresult);
            try
            {
              //  var  = "";//await _IDemandSplit.GetDemandSplitViewResult(Search);
                //if (fileNameList != null && fileNameList.Count > 0)
                if (RetriveGrid != null)
                {
                    response = new ObjectResult(RetriveGrid);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }





        private List<RoutingRulesUpdategrid> ConvertFulfillmentDTtoList(DataTable dt)
        {
            List<RoutingRulesUpdategrid> lstFulfillmentData = new List<RoutingRulesUpdategrid>();


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var output = new RoutingRulesUpdategrid();
                //output.Id = String.Empty;
                if (dt.Rows[i]["ID"] != System.DBNull.Value)
                {
                    output.id = Convert.ToString(dt.Rows[i]["ID"]);
                }

                if (dt.Rows[i]["ROUTING_ID"] != System.DBNull.Value)
                {
                    output.strRoutingIDGrid = Convert.ToString(dt.Rows[i]["ROUTING_ID"]);
                    output.ROUTING_ID = Convert.ToString(dt.Rows[i]["ROUTING_ID"]);
                }

                if (dt.Rows[i]["PRODUCTFGA"] != System.DBNull.Value)
                {
                    output.product = Convert.ToString(dt.Rows[i]["PRODUCTFGA"]);
                }

                if (dt.Rows[i]["FULFILLMENT_SITE_ID"] != System.DBNull.Value)
                {
                    output.fulfillmenT_SITE_ID = Convert.ToString(dt.Rows[i]["FULFILLMENT_SITE_ID"]);
                }

                if (dt.Rows[i]["MANUFACTURING_SITE_ID"] != System.DBNull.Value)
                {
                    output.manufacturinG_SITE_ID = Convert.ToString(dt.Rows[i]["MANUFACTURING_SITE_ID"]);
                }

                if (dt.Rows[i]["PRIMARY_ROUTING"] != System.DBNull.Value)
                {
                    output.primarY_ROUTING = Convert.ToString(dt.Rows[i]["PRIMARY_ROUTING"]);
                }

                if (dt.Rows[i]["ALTERNATE_ROUTING_1"] != System.DBNull.Value)
                {
                    output.Alternate_Routing1 = Convert.ToString(dt.Rows[i]["ALTERNATE_ROUTING_1"]);
                }

                if (dt.Rows[i]["ALTERNATE_ROUTING_2"] != System.DBNull.Value)
                {
                    output.Alternate_Routing2 = Convert.ToString(dt.Rows[i]["ALTERNATE_ROUTING_2"]);
                }

                if (dt.Rows[i]["PRIMARY_ROUTING_DESC"] != System.DBNull.Value)
                {
                    output.primarY_ROUTING_DESC = Convert.ToString(dt.Rows[i]["PRIMARY_ROUTING_DESC"]);
                }

                if (dt.Rows[i]["ROUTING_EFFECTIVE_START_DATE"] != System.DBNull.Value)
                {
                    output.Routing_Effective_Start_Date = Convert.ToString(dt.Rows[i]["ROUTING_EFFECTIVE_START_DATE"]);
                }

                if (dt.Rows[i]["ROUTING_EFFECTIVE_END_DATE"] != System.DBNull.Value)
                {
                    output.Routing_Effective_End_Date = Convert.ToString(dt.Rows[i]["ROUTING_EFFECTIVE_END_DATE"]);
                }
               
                if (dt.Rows[i]["MFG_EFFECTIVE_START_DATE"] != System.DBNull.Value)
                {
                    output.Mfg_Site_Effective_Begin_Date = Convert.ToString(dt.Rows[i]["MFG_EFFECTIVE_START_DATE"]);
                }
                                
                if (dt.Rows[i]["MFG_EFFECTIVE_END_DATE"] != System.DBNull.Value)
                {
                    output.Mfg_Site_Effective_End_Date = Convert.ToString(dt.Rows[i]["MFG_EFFECTIVE_END_DATE"]);
                }

                if (dt.Rows[i]["MFG_SITE_SPLIT_PERCENT"] != System.DBNull.Value)
                {
                    output.mfG_SITE_SPLIT_PERCENT = Convert.ToString(dt.Rows[i]["MFG_SITE_SPLIT_PERCENT"]);
                }

               

                if (dt.Rows[i]["ALTERNATE_ROUTING_1_DESC"] != System.DBNull.Value)
                {
                    output.Alternate_Routing1_des = Convert.ToString(dt.Rows[i]["ALTERNATE_ROUTING_1_DESC"]);
                }

                if (dt.Rows[i]["ALTERNATE_ROUTING_2_DESC"] != System.DBNull.Value)
                {
                    output.Alternate_Routing2_des = Convert.ToString(dt.Rows[i]["ALTERNATE_ROUTING_2_DESC"]);
                }



                //if (dt.Rows[i]["PRODUCTFGA"] != System.DBNull.Value)
                //{
                //    output.ProductFga = Convert.ToString(dt.Rows[i]["PRODUCTFGA"]);
                //}

                lstFulfillmentData.Add(output);
            }

            return lstFulfillmentData;
        }






        DataTable retrieve(RoutingRulesSearchPara Search)
        {
            string strId = null;
            string strType = "LINES";
            string itemType = null;
            string strItemID = null;
            string strItemDescription = null;

            try
            {
                if (Search.Product == "--SELECT--")
                {
                    //pnlMfgSitesInfo.Visible = false;
                    //pnlRoutingRuleCreate.Visible = false;
                    //rulerAfterRetrieve.Visible = false;

                    //lblMsg.Text = ConfigurationManager.AppSettings["msgNoFgaProdSelected"].ToString();
                    //lblMsg.ForeColor = System.Drawing.Color.Red;
                    //imgError.Visible = true;
                    //imgSuccess.Visible = false;
                    //return;
                }

                if (Search.Product == "BTXProductTree" && string.IsNullOrEmpty(Search.Fhctree))
                {
                    //pnlMfgSitesInfo.Visible = false;
                    //pnlRoutingRuleCreate.Visible = false;
                    //rulerAfterRetrieve.Visible = false;

                    //lblMsg.Text = ConfigurationManager.AppSettings["msgNoProdSelected"].ToString();
                    //lblMsg.ForeColor = System.Drawing.Color.Red;
                    //imgError.Visible = true;
                    //return;
                }
                if (Search.Product == "CTOProductTree" && string.IsNullOrEmpty(Search.Fhctree))
                {
                    //pnlMfgSitesInfo.Visible = false;
                    //pnlRoutingRuleCreate.Visible = false;
                    //rulerAfterRetrieve.Visible = false;

                    //lblMsg.Text = ConfigurationManager.AppSettings["msgNoProdSelected"].ToString();
                    //lblMsg.ForeColor = System.Drawing.Color.Red;
                    //imgError.Visible = true;
                    //return;
                }
                if (Search.Product == "FHC" && string.IsNullOrEmpty(Search.Fhctree))
                {
                    //pnlMfgSitesInfo.Visible = false;
                    //pnlRoutingRuleCreate.Visible = false;
                    //rulerAfterRetrieve.Visible = false;

                    //lblMsg.Text = "Select FGA/FHC.";//ConfigurationManager.AppSettings["msgNoProdSelected"].ToString();
                    //lblMsg.ForeColor = System.Drawing.Color.Red;
                    //imgError.Visible = true;
                    //return;
                }
                if (Search.Product == "FGA" && Search.Product == "--SELECT--")
                    if (Search.Product == "FGA" && string.IsNullOrEmpty(Search.Fhctree))  //modified by Sanjeeva
                    {
                        //pnlMfgSitesInfo.Visible = false;
                        //pnlRoutingRuleCreate.Visible = false;
                        //rulerAfterRetrieve.Visible = false;

                        //lblMsg.Text = ConfigurationManager.AppSettings["msgNoItemSelected"].ToString();
                        //lblMsg.ForeColor = System.Drawing.Color.Red;
                        //imgError.Visible = true;
                        //return;
                    }

                //divRetriveCreate.Enabled = true;
                //pnlRoutingRuleCreate.Enabled = true;
                //ddlFulfillmentSite.Enabled = true;
                if (Search.Product == "ALL")
                {
                    strId = "ALL_PRODUCT";
                    itemType = "PRODUCT_TREE";
                }
                else
                //if (Search.Product == "BTXProductTree")
                if (Search.Product == "PRODUCT_TREE")
                {
                    strId = Search.hdfRetrieve;
                    itemType = "PRODUCT_TREE";
                    if (string.IsNullOrEmpty(Search.Fhctree))
                    {
                        //lblMsg.Text = ConfigurationManager.AppSettings["msgNoProdSelected"].ToString();
                        //lblMsg.ForeColor = System.Drawing.Color.Red;
                        //imgError.Visible = true;
                        //divCreateGrid.Visible = false;
                        //pnlRoutingRuleCreate.Enabled = false;
                        //return;
                    }
                    else
                    {
                        ///if its ALL_PRODUCT, then dont do this.
                        if (Search.Fhctree.ToString() == "ALL_PRODUCT")
                        {
                           // lblProductCreate.Text = "ALL_PRODUCT";
                        }
                        else
                        {
                            
                            //heirarchydatap = new HierarchyDataProvider();
                            //strItemDescription = heirarchydatap.GetBaseCodeDescription(hdfRetrieve.Value);
                            //lblProductCreate.Text = hdfRetrieve.Value;
                            //lblProductCreate.Text = strItemDescription;
                        }
                    }
                    strItemID = Search.Fhctree;

                    if (strItemID.Contains("("))
                    {
                        strId = strItemID.Substring(strItemID.IndexOf("(") + 1, strItemID.IndexOf(")") - strItemID.IndexOf("(") - 1);
                    }
                    else
                        strId = Search.Fhctree;

                }
                else
                //if (Search.Product == "CTOProductTree")
                 if (Search.Product == "PRODUCT_TREE")
                {
                    strId = Search.Fhctree;
                    itemType = "PRODUCT_TREE";
                    if (string.IsNullOrEmpty(Search.Fhctree))
                    {
                        //lblMsg.Text = ConfigurationManager.AppSettings["msgNoProdSelected"].ToString();
                        //lblMsg.ForeColor = System.Drawing.Color.Red;
                        //imgError.Visible = true;
                        //divCreateGrid.Visible = false;
                        //pnlRoutingRuleCreate.Enabled = false;
                        //return;
                    }
                    else
                    {
                        
                        if (Search.Fhctree.ToString() == "ALL_PRODUCT")
                        {
                           // lblProductCreate.Text = "ALL_PRODUCT";
                        }
                        else
                        {
                           
                            //heirarchydatap = new HierarchyDataProvider();
                            //strItemDescription = heirarchydatap.GetBaseCodeDescription(hdfRetrieve.Value);

                            //lblProductCreate.Text = hdfRetrieve.Value;
                            //lblProductCreate.Text = strItemDescription;
                        }
                    }
                    strItemID = Search.Fhctree;
                    if (strItemID.Contains("("))
                    {
                        strId = strItemID.Substring(strItemID.IndexOf("(") + 1, strItemID.IndexOf(")") - strItemID.IndexOf("(") - 1);
                    }
                    else
                        strId = Search.Fhctree;

                }
                else
                 if (Search.Product == "FHC_TREE")
                {
                    strId = Search.Fhctree;

                    if (string.IsNullOrEmpty(Search.Fhctree) || string.IsNullOrEmpty(Search.Fhctree))
                    {
                        //lblMsg.Text = "Select FHC/FGA."; //ConfigurationManager.AppSettings["msgNoProdSelected"].ToString();
                        //lblMsg.ForeColor = System.Drawing.Color.Red;
                        //imgError.Visible = true;
                        //divCreateGrid.Visible = false;
                        //pnlRoutingRuleCreate.Enabled = false;
                        //return;
                    }
                    else
                    {
                       // lblProductCreate.Text = hdfRetrieve.Value;
                    }
                    strItemID = Search.Fhctree;
                    if (strItemID.Contains("("))
                    {
                        strId = strItemID.Substring(0, strItemID.IndexOf("("));
                    }
                    else
                        strId = Search.Fhctree;

                    if (Search.Fhctree.Equals("FGA_ITEM"))
                    {
                        itemType = "FGA_ITEM";
                    }
                    else
                        if (Search.Fhctree.Equals("FHC_ITEM"))
                    {
                        itemType = "FHC_ITEM";
                    }
                }
                else
                if (Search.Product== "FGA")
                {
                    strId = Search.Product;
                    if (string.IsNullOrEmpty(Search.Fhctree) || string.IsNullOrEmpty(Search.Fhctree))
                    {
                        //lblMsg.Text = "Select FHC/FGA."; //ConfigurationManager.AppSettings["msgNoProdSelected"].ToString();
                        //lblMsg.ForeColor = System.Drawing.Color.Red;
                        //imgError.Visible = true;
                        //divCreateGrid.Visible = false;
                        //pnlRoutingRuleCreate.Enabled = false;
                        //return;
                    }
                    else
                    {
                        strItemID = Search.Fhctree;
                        if (strItemID.Contains("("))
                        {
                            strId = strItemID.Substring(0, strItemID.IndexOf("("));
                        }
                        else
                            strId = Search.Fhctree;
                    }
                    itemType = "FGA_ITEM";
                }
                DataTable dtSearch;
               // routingRulesDataProvider = new RoutingRulesDataProvider();
                 dtSearch = _IRoutingRules.searchRoutings(strId, itemType, out strType);
             //   dtSearch = null;// Doolchand Added 

                return dtSearch;

                if (dtSearch != null && dtSearch.Rows.Count > 0 && strType != "HEADER")
                {
                    //grdCreate.DataSource = dtSearch;
                    //dtSearch.DefaultView.Sort = "MFG_EFFECTIVE_START_DATE,MANUFACTURING_SITE_ID,MFG_SITE_SPLIT_PERCENT" + " " + "ASC";

                    //grdCreate.DataBind();
                    //divCreateGrid.Visible = true;
                    dtSearch.Columns.Add("STATUS");
                    for (int i = 0; i < dtSearch.Rows.Count; i++)
                    {
                        dtSearch.Rows[i]["STATUS"] = "NOCHANGE";
                    }
                    dtSearch.AcceptChanges();

                    //if (grdCreate.Rows.Count > 0)
                    //{
                    //    grdCreate.Visible = true;
                    //    btnSave.Visible = btnUpdate.Visible = btnDeleteRule.Visible = true;
                    //}

                //    ddlMFGFiscalBeginDate.Enabled = ddlMFGFiscalEndDate.Enabled = btnAddMFG.Enabled = btnAddMFGClear.Enabled = true;

                    DataView Dv = dtSearch.DefaultView;
                    Dv.Sort = "MFG_EFFECTIVE_START_DATE,MANUFACTURING_SITE_ID,MFG_SITE_SPLIT_PERCENT" + " " + "ASC";
                    dtSearch = Dv.ToTable();

                    //ViewState["dtRoutingRule"] = dtSearch;
                    //btnUpdate.Enabled = true;
                    //btnDeleteRule.Enabled = true;
                    //ViewState["isParmanentData"] = "1";
                }
                else
                {
                    //dtSearch = new DataTable();
                    //grdCreate.DataSource = dtSearch;

                    //grdCreate.DataBind();
                    //ViewState["dtRoutingRule"] = null;
                    //lblNoRuleCongifureMsg.Visible = true;
                    //if (strType != "HEADER")
                    //{
                    //    lblNoRuleCongifureMsg.Text = ConfigurationManager.AppSettings["msgNoRulesConf"].ToString();
                    //}
                    //else
                    //{
                    //    lblNoRuleCongifureMsg.Text = ConfigurationManager.AppSettings["msgNoRule"].ToString();
                    //}
                    //divCreateGrid.Visible = false;
                    //ViewState["isParmanentData"] = null;
                }

                //lblFulfilmentSite.Visible = lblMfgEffectiveBeginDate.Visible = lblMfgEffectiveEndDate.Visible = true;
                //lblFulfilmentSite.Enabled = lblMfgEffectiveBeginDate.Enabled = lblMfgEffectiveEndDate.Enabled = true;
                //ddlFulfillmentSite.Visible = ddlMFGFiscalBeginDate.Visible = ddlMFGFiscalEndDate.Visible = btnAddMFG.Visible = btnAddMFGClear.Visible = true;
                //ddlFulfillmentSite.Enabled = ddlMFGFiscalBeginDate.Enabled = ddlMFGFiscalEndDate.Enabled = btnAddMFG.Enabled = btnAddMFGClear.Enabled = true;

                //bindFullfillmentsitesddl();

                
                //divRetriveCreate.Enabled = false;
                //btnClearAll.Enabled = true;
                //lblFGItems.Enabled = lblProdPlatform.Enabled = ddlProductCreate.Enabled = ImageButton1.Enabled = imgbtnFHCCreate.Enabled = lblProductCreate.Enabled = ddlProductPlatformCreate.Enabled = btnRetrieve.Enabled = false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }








        [HttpPost]
        [Route("SubmiteCopyRules")]
        public async Task<ObjectResult> SubmiteCopyRules([FromBody] CopyRoutingRules Search)
        {
            ObjectResult response;

            string RetriveGrid = SumbmitCopy(Search);

            try
            {
                
                if (RetriveGrid != null)
                {
                    response = new ObjectResult(RetriveGrid);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;


         

        }



       public string SumbmitCopy(CopyRoutingRules Search)
        {

            try
            {
                bool isInputValid = true;
                string result = string.Empty;
                string itemId = string.Empty;
               // string routingId = Request.QueryString["RoutingID"].ToString();

                string itemType = string.Empty;
                string newRoutingId = DateTime.Now.Ticks.ToString();
               // string loginUser = SPContext.Current.Site.RootWeb.CurrentUser.LoginName;

                //Validation
              //  switch (this.ddlRoutingItemType.SelectedValue)
                switch (Search.Product)
                {
                    case "PRODUCT_TREE":
                        itemId = Search.hfItemID;
                        itemType = "PRODUCT_TREE";

                        if (!string.IsNullOrEmpty(itemId) && itemId != "ALL")
                        {
                            if (itemId.Contains("("))
                            {
                                itemId = itemId.Substring(itemId.IndexOf("(") + 1, itemId.IndexOf(")") - itemId.IndexOf("(") - 1);
                            }
                            else
                                itemId = itemId.ToString();
                        }
                        else
                        {
                            isInputValid = false;
                            //this.lblErrorMessage.Text = ConfigurationManager.AppSettings["msgNoProdSelected"].ToString();
                        }
                        break;
                    
                    case "FHC_TREE":
                        itemId = Search.hfItemID;

                        if (string.IsNullOrEmpty(itemId))
                        {
                            isInputValid = false;
                          //  this.lblErrorMessage.Text = ConfigurationManager.AppSettings["msgNoItemSelected"].ToString();
                        }
                        if (itemId.IndexOf("(") > -1)
                        {
                            itemId = itemId.Remove(itemId.IndexOf("("));
                        }
                        itemType = "FHC_ITEM";//Search.hfFHCType;
                        break;
                    case "FGA":

                        itemId = Search.ddlFGA;
                        if (itemId == "Select")
                        {
                            isInputValid = false;
                            //  this.lblErrorMessage.Text = ConfigurationManager.AppSettings["`"].ToString();
                            ReturnMessage = "Please select Product/FHC/FGA";
                        }
                        itemType = "FGA_ITEM";
                        break;
                    default:
                        isInputValid = false;
                       // this.lblErrorMessage.Text = "Please select Product/FHC/FGA";
                        break;
                }

                if (!isInputValid)
                {
                    //this.lblErrorMessage.Visible = true;
                    //this.lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                    //this.imgError.Visible = true;
                    //this.lblSuccessMessage.Visible = false;
                    ReturnMessage = "Please select Product/FHC/FGA";
;                    return ReturnMessage;
                }

              //  IRoutingRulesDataProvider routingRuleDataProvider = new RoutingRulesDataProvider();
                result = _IRoutingRules.CopyRoutingRule(Search.routingId, newRoutingId, itemType, itemId,this.userName);

                if (result.Equals("SUCCESS"))
                {
                    //this.lblSuccessMessage.Visible = true;
                    //this.lblSuccessMessage.Text = "Successfully Copied";
                    //this.imgError.Visible = this.lblErrorMessage.Visible = false;
                    //this.btnSubmit.Enabled = false;

                    ReturnMessage = "1";
                    return ReturnMessage;

                }
                else
                {
                    //this.imgError.Visible = this.lblErrorMessage.Visible = true;
                    //this.lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                    //this.lblErrorMessage.Text = result;
                    //this.lblSuccessMessage.Visible = false;
                    ReturnMessage = result;
                    return ReturnMessage;
                }
            }
            catch (Exception ex)
            {
                //this.imgError.Visible = this.lblErrorMessage.Visible = true;
                //this.lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                //this.lblErrorMessage.Text = "Application has encountered technical issue. Please report to application administrator. <b>Error Details:</b> <br/> " + ex.Message;
                //this.lblSuccessMessage.Visible = false;
            }

            ReturnMessage = "not found";
            return ReturnMessage;

        }



    }
}
