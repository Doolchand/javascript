﻿using Dell.RP.API.Controllers.HelperClasses;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement;
using Dell.RP.Common.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Dell.RP.API.Controllers.FullFillmentSiteManagement
{
    [Route("api/[controller]")]
    [Authorize(Policy = "AllUsers")]
    public class FullFillmentSiteManagementController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IFullFillmentSiteManagement _iFullFillmentSiteManagement;


        #region Constructor & Local variables
        private string userName = "";

        public FullFillmentSiteManagementController(IFullFillmentSiteManagement iFullFillmentSiteManagement, IHttpContextAccessor httpContextAccessor, IClaimsTransformation d, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            this._iFullFillmentSiteManagement = iFullFillmentSiteManagement;
            var s = UserNameValidation.GetUser(httpContextAccessor);
            if (s.Result != null)
            {
                this.userName = s.Result;
            }
        }
        #endregion

        [HttpGet]
        [Route("GetMasterDataData")]
        public async Task<ObjectResult> GetMasterDataData(string key)
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                //string key = "GEO";
                var fileNameList = await _iFullFillmentSiteManagement.GetMasterDataData(key);
                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetAccountNameFullfilment")]
        public async Task<ObjectResult> GetAccountNameFullfilment()
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                //string key = "GEO";
                var fileNameList = await _iFullFillmentSiteManagement.GetAccountNameFullfilment();


                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetChannelType")]
        public async Task<ObjectResult> GetChannelType(string channelId)
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                //string key = "GEO";
                var fileNameList = await _iFullFillmentSiteManagement.GetChannelType(channelId);

                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList[0]);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }


        [HttpGet]
        [Route("GetGeoCountries")]
        public async Task<ObjectResult> GetGeoCountries()
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                //string key = "GEO";
                var fileNameList = await _iFullFillmentSiteManagement.GetGeoCountries();

                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }


        [HttpGet]
        [Route("GetFSAFiscalCalendarInfo_BeginDate")]
        public ObjectResult GetFSAFiscalCalendarInfo_BeginDate()
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                //string key = "GEO";
                var fileNameList = _iFullFillmentSiteManagement.GetFSAFiscalCalendarInfo_BeginDate();

                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetFSAFiscalCalendarInfo_EndDate")]
        public ObjectResult GetFSAFiscalCalendarInfo_EndDate()
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                //string key = "GEO";
                var fileNameList = _iFullFillmentSiteManagement.GetFSAFiscalCalendarInfo_EndDate();

                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }


        [HttpGet]
        [Route("GetChannelTreeData")]
        public async Task<ObjectResult> GetChannelTreeData()
        {
            ObjectResult response;
            response = null;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = await _iFullFillmentSiteManagement.GetChannelTreeData();

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }




        [HttpGet]
        [Route("GetChannelSegments")]
        public async Task<ObjectResult> GetChannelSegments()
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                //string key = "GEO";
                var fileNameList = await _iFullFillmentSiteManagement.GetChannelSegments();

                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }


        [HttpGet]
        [Route("GetGeoTreeData")]
        public async Task<ObjectResult> GetGeoTreeData()
        {
            ObjectResult response;
            response = null;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = await _iFullFillmentSiteManagement.GetGeoTreeData();

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetProductTreeData")]
        public async Task<ObjectResult> GetProductTreeData(string strString)
        {
            ObjectResult response;
            response = null;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = await _iFullFillmentSiteManagement.GetProductTreeData(strString);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetProductFHCTreeData")]
        public async Task<ObjectResult> GetProductFHCTreeData(string strString)
        {
            ObjectResult response;
            response = null;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = await _iFullFillmentSiteManagement.GetProductFHCTreeData(strString);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetProductFGATreeData")]
        public async Task<ObjectResult> GetProductFGATreeData(string strString)
        {
            ObjectResult response;
            response = null;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = await _iFullFillmentSiteManagement.GetProductFGATreeData(strString);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpPost]
        [Route("GetFulFillmentSearchResult")]
        public async Task<ObjectResult> GetFulFillmentSearchResult([FromBody] FulFillmentSearchInput strSearchInputParams)
        {
            ObjectResult response;
            response = null;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = await _iFullFillmentSiteManagement.GetFulFillmentSearchResult(strSearchInputParams);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }



        //[HttpPost]
        //[Route("DeleteRoutingDetails")]
        //public async Task<ObjectResult> DeleteRoutingDetails([FromBody] FulfillmentDeleteInputParams deleteParams)
        //{
        //    ObjectResult response;
        //    response = null;
        //    _iFullFillmentSiteManagement.UserID = this.userName;
        //    try
        //    {

        //        //string key = "GEO";
        //        var fileNameList = await _iFullFillmentSiteManagement.DeleteRoutingDetails(deleteParams.singleDeleteFlag, deleteParams.ID);

        //        if (fileNameList != null)
        //        {
        //            response = new ObjectResult(fileNameList);
        //            response.StatusCode = (int)HttpStatusCode.OK;
        //            return response;
        //        }
        //        else
        //        {
        //            response = new ObjectResult("No data found.");
        //            response.StatusCode = (int)HttpStatusCode.NoContent;
        //            return response;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        response = new ObjectResult(ex.Message);
        //        response.StatusCode = (int)HttpStatusCode.InternalServerError;
        //    }
        //    return response;

        //}


        [HttpPost]
        [Route("DeleteRoutingDetails_fulfillment")]
        public async Task<ObjectResult> DeleteRoutingDetails_fulfillment([FromBody] FulfillmentDelete deleteParams)
        {
            ObjectResult response;
            response = null;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = "";
                int result = -1;
                int result1 = -1;
                for (int i = 0; i < deleteParams.deleteParam.Count; i++)
                {
                    result = await _iFullFillmentSiteManagement.DeleteRoutingDetails(deleteParams.deleteParam[i].singleDeleteFlag, deleteParams.deleteParam[i].ID);
                    result1 = await _iFullFillmentSiteManagement.DeleteRoutingDetailsHeader(deleteParams.deleteParam[i].singleDeleteFlag, deleteParams.deleteParam[i].ID);
                }
                

                fileNameList = result + "," + result1;

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }


        [HttpPost]
        [Route("DeleteRoutingDetailsHeader")]
        public async Task<ObjectResult> DeleteRoutingDetailsHeader([FromBody] FulfillmentDeleteInputParams deleteParams)
        {
            ObjectResult response;
            response = null;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = await _iFullFillmentSiteManagement.DeleteRoutingDetailsHeader(deleteParams.singleDeleteFlag, deleteParams.ID);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }


        [HttpPost]
        [Route("GetFulfillmentRetrive")]
        public async Task<ObjectResult> GetFulfillmentRetrive([FromBody] FulFillmentSearchInput strRetriveInputParams)
        {
            ObjectResult response;
            response = null;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = await _iFullFillmentSiteManagement.GetFulfillmentRetrive(strRetriveInputParams);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }


        [HttpGet]
        [Route("getRegionName")]
        public ObjectResult getRegionName(string country)
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                //string key = "GEO";
                var fileNameList = _iFullFillmentSiteManagement.getRegionName(country);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }


        [HttpPost]
        [Route("AddUpdateRoutingRule_EditSection")]
        public ObjectResult AddUpdateRoutingRule_EditSection([FromBody] FulfillmentAddUpdateParams _fulfillmentAddUpdateParams)
        {

            _fulfillmentAddUpdateParams.strSysEnt = "ACTIVE";
            
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            bool isError = false;
            string errMsg = "";
            string errCode = "0";//success and 1 for fail
            int result = -1;
            string fulfillmentSiteValue = "";
            string splitPercentValue = "";
            bool validationFlag = true;



            //strEffectiveBeginDate = this.rowData_rule[k].beginDateValue //grdViewDates.Rows[k].Cells[4].Text.ToString().Replace("&nbsp;", " ");
            //strEffectiveEndDate = this.rowData_rule[k].endDateValue //grdViewDates.Rows[k].Cells[5].Text.ToString().Replace("&nbsp;", " ");

            List<RoutingDetail> routingRuleDt = new List<RoutingDetail>();

            if (_fulfillmentAddUpdateParams.objAddUpdateRuleData.Count > 0 && _fulfillmentAddUpdateParams.fulfillUpdateMode == true)
            {
                _fulfillmentAddUpdateParams.strIntermRoutingID = _fulfillmentAddUpdateParams.objselectedRow.hiddenRoutingId; //grdViewDates.Rows[k].Cells[6].Text.ToString().Replace("&nbsp;", "");;
                routingRuleDt = _iFullFillmentSiteManagement.retrieveRoutingDetailsByRoutingId(_fulfillmentAddUpdateParams.strIntermRoutingID);
            }
            else
            {
                routingRuleDt = _iFullFillmentSiteManagement.retrieveRoutingDetails(_fulfillmentAddUpdateParams.objAddUpdateParams);
            }

            if (routingRuleDt.Count == 0 || _fulfillmentAddUpdateParams.strIntermRoutingID == "")
            {
                FullFillmentRuleData lstFulfillmentRuleData = new FullFillmentRuleData();

                lstFulfillmentRuleData.Channel = _fulfillmentAddUpdateParams.objAddUpdateParams.Channelid;
                lstFulfillmentRuleData.Geography = _fulfillmentAddUpdateParams.objAddUpdateParams.Geoid;
                lstFulfillmentRuleData.producttype = _fulfillmentAddUpdateParams.typeValue;
                lstFulfillmentRuleData.product_id = _fulfillmentAddUpdateParams.objAddUpdateParams.StrProductId;
                lstFulfillmentRuleData.item1 = _fulfillmentAddUpdateParams.objAddUpdateParams.Item;
                lstFulfillmentRuleData.SupplyChainType = _fulfillmentAddUpdateParams.objAddUpdateParams.Ssc;
                lstFulfillmentRuleData.ship_via = _fulfillmentAddUpdateParams.objAddUpdateParams.ShipVia;
                lstFulfillmentRuleData.Routing = _fulfillmentAddUpdateParams.longtick;
                lstFulfillmentRuleData.geoLevel = _fulfillmentAddUpdateParams.objAddUpdateParams.GeoType;
                lstFulfillmentRuleData.channelLevel = _fulfillmentAddUpdateParams.objAddUpdateParams.ChannelType;
                lstFulfillmentRuleData.sys_source = _fulfillmentAddUpdateParams.sysSource;
                lstFulfillmentRuleData.sys_created_by = _fulfillmentAddUpdateParams.strUserName;

                lstFulfillmentRuleData.last_modified_by = _fulfillmentAddUpdateParams.strUserName;
                lstFulfillmentRuleData.accountName = _fulfillmentAddUpdateParams.objAddUpdateParams.StrsearcAccountID;
                lstFulfillmentRuleData.accountId = _fulfillmentAddUpdateParams.strAccountName;

                lstFulfillmentRuleData.effectiveBeginDate = _fulfillmentAddUpdateParams.objAddUpdateParams.effectiveStartDate;
                lstFulfillmentRuleData.effectiveEndDate = _fulfillmentAddUpdateParams.objAddUpdateParams.effectiveEndDate;

                //Check valid date for insert validations 
                string startDate = lstFulfillmentRuleData.effectiveBeginDate;
                string endDate = lstFulfillmentRuleData.effectiveEndDate;
                //Convert the strings into standard date formats.

                if (startDate != null && startDate != " " && startDate != string.Empty)
                {
                    if (DateTime.Parse(startDate).ToShortDateString() == "12/31/9999")
                    {
                        errMsg = "Invalid Start Date";
                        isError = true;
                        errCode = "1";
                        string res = errCode + "::" + isError + "::" + errMsg;

                        response = new ObjectResult(res);
                        response.StatusCode = (int)HttpStatusCode.OK;
                        return response;
                    }
                }
                if (endDate != null && endDate != " " && endDate != string.Empty)
                {
                    if (DateTime.Parse(endDate).ToShortDateString() == "12/31/9999")
                    {
                        errMsg = "Invalid End Date";
                        isError = true;
                        errCode = "1";
                        string res = errCode + "::" + isError + "::" + errMsg;

                        response = new ObjectResult(res);
                        response.StatusCode = (int)HttpStatusCode.OK;
                        return response;
                    }
                }

                //Validation: Start date should less than end date
                if (startDate != null && endDate != null && startDate != " " && startDate != string.Empty && endDate != " " && endDate != string.Empty)
                {
                    if (DateTime.Parse(startDate) > DateTime.Parse(endDate))
                    {
                        errMsg = "Effective Start Date must be less than Effective End Date";
                        isError = true;
                        errCode = "1";
                        string res = errCode + "::" + isError + "::" + errMsg;

                        response = new ObjectResult(res);
                        response.StatusCode = (int)HttpStatusCode.OK;
                        return response;
                    }
                }


                bool validInsertDates = _iFullFillmentSiteManagement.InsertValidateEffectiveDates(lstFulfillmentRuleData);

                if (validInsertDates)
                {
                    result = _iFullFillmentSiteManagement.AddRoutingdetails(lstFulfillmentRuleData);
                }
                else
                {
                    errMsg = "Dates are overlapping with existing rules.";
                    isError = true;
                    errCode = "1";
                    string res = errCode + "::" + isError + "::" + errMsg;
                    validationFlag = false;

                    response = new ObjectResult(res);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }

                if (result == 1)
                {
                    if (_fulfillmentAddUpdateParams.objselectedRow.fulfillmentSiteId.IndexOf(",") > -1) //Eg:WISKUN(60),COMCHE(40)
                    {
                        string[] FFandSplitInfo = _fulfillmentAddUpdateParams.objselectedRow.fulfillmentSiteId.ToString().Split(",");
                        for (var i = 0; i < FFandSplitInfo.Length; i++)
                        {
                            string[] intermArra = FFandSplitInfo[i].Split('(');
                            fulfillmentSiteValue = intermArra[0].ToString(); //Fulfillment site value
                            splitPercentValue = intermArra[1].Replace("(", "").Replace(")", "").ToString(); //Split %

                            FullFillmentRuleData lstFulfillSite = new FullFillmentRuleData();

                            lstFulfillSite.Routing = _fulfillmentAddUpdateParams.longtick;
                            lstFulfillSite.sites = fulfillmentSiteValue;
                            lstFulfillSite.Split = splitPercentValue;
                            lstFulfillSite.sys_source = _fulfillmentAddUpdateParams.sysSource;
                            lstFulfillSite.sys_created_by = _fulfillmentAddUpdateParams.strUserName;
                            lstFulfillSite.last_modified_by = _fulfillmentAddUpdateParams.strUserName;
                            lstFulfillSite.sys_Ent_State = _fulfillmentAddUpdateParams.strSysEnt;

                            _iFullFillmentSiteManagement.AddFullFillmentsites(lstFulfillSite);
                        }


                    }
                    else //Eg:WISKUN(60)
                    {
                        string[] intermArray = _fulfillmentAddUpdateParams.objselectedRow.fulfillmentSiteId.ToString().Split('(');
                        fulfillmentSiteValue = intermArray[0].ToString(); //Fulfillment site value
                        splitPercentValue = intermArray[1].Replace("(", "").Replace(")", "").ToString();

                        FullFillmentRuleData lstFulfillSite = new FullFillmentRuleData();

                        lstFulfillSite.Routing = _fulfillmentAddUpdateParams.longtick;
                        lstFulfillSite.sites = fulfillmentSiteValue;
                        lstFulfillSite.Split = splitPercentValue;
                        lstFulfillSite.sys_source = _fulfillmentAddUpdateParams.sysSource;
                        lstFulfillSite.sys_created_by = _fulfillmentAddUpdateParams.strUserName;
                        lstFulfillSite.last_modified_by = _fulfillmentAddUpdateParams.strUserName;
                        lstFulfillSite.sys_Ent_State = _fulfillmentAddUpdateParams.strSysEnt;

                        _iFullFillmentSiteManagement.AddFullFillmentsites(lstFulfillSite);
                    }
                }
            }
            else
            {
                string routing = "";
                if (routingRuleDt.Count != 0)
                {
                    routing = routingRuleDt[0].RoutingId;
                }
                else
                    routing = _fulfillmentAddUpdateParams.strIntermRoutingID.ToString();

                var dv = routingRuleDt;//todo:

                if (_fulfillmentAddUpdateParams.objselectedRow.fulfillmentSiteId.ToString().IndexOf(",") > -1) //Eg:WISKUN(60),COMCHE(40)
                {
                    string[] FFandSplitInfo = _fulfillmentAddUpdateParams.objselectedRow.fulfillmentSiteId.ToString().Split(',');
                    for (var i = 0; i < FFandSplitInfo.Length; i++)
                    {
                        string[] intermArra = FFandSplitInfo[i].Split('(');
                        fulfillmentSiteValue = intermArra[0].ToString(); 
                        splitPercentValue = intermArra[1].Replace("(", "").Replace(")", "").ToString(); 

                        UpdateValidateDateInputParams lstUpdateValidateParams = new UpdateValidateDateInputParams();

                        lstUpdateValidateParams.routing = routing;
                        lstUpdateValidateParams.strEffectiveBeginDate = _fulfillmentAddUpdateParams.objAddUpdateParams.effectiveStartDate;
                        lstUpdateValidateParams.strEffectiveEndDate = _fulfillmentAddUpdateParams.objAddUpdateParams.effectiveStartDate;

                        //validate update dates
                        string startDate = lstUpdateValidateParams.strEffectiveBeginDate;
                        string endDate = lstUpdateValidateParams.strEffectiveEndDate;
                        
                        //Convert the strings into standard date formats.

                        if (startDate != null && startDate != " " && startDate != string.Empty)
                        {
                            if (DateTime.Parse(startDate).ToShortDateString() == "12/31/9999")
                            {
                                errMsg = "Invalid Start Date";
                                isError = true;
                                errCode = "1";
                                string res = errCode + "::" + isError + "::" + errMsg;

                                response = new ObjectResult(res);
                                response.StatusCode = (int)HttpStatusCode.OK;
                                return response;
                            }
                        }
                        if (endDate != null && endDate != " " && endDate != string.Empty)
                        {
                            if (DateTime.Parse(endDate).ToShortDateString() == "12/31/9999")
                            {
                                errMsg = "Invalid End Date";
                                isError = true;
                                errCode = "1";
                                string res = errCode + "::" + isError + "::" + errMsg;

                                response = new ObjectResult(res);
                                response.StatusCode = (int)HttpStatusCode.OK;
                                return response;
                            }
                        }

                        //Validation: Start date should less than end date
                        if (startDate != null && endDate != null && startDate != " " && startDate != string.Empty && endDate != " " && endDate != string.Empty)
                        {
                            if (DateTime.Parse(startDate) > DateTime.Parse(endDate))
                            {
                                errMsg = "Effective Start Date must be less than Effective End Date";
                                isError = true;
                                errCode = "1";
                                string res = errCode + "::" + isError + "::" + errMsg;

                                response = new ObjectResult(res);
                                response.StatusCode = (int)HttpStatusCode.OK;
                                return response;
                            }
                        }

                        bool updateres = _iFullFillmentSiteManagement.UpdateValidateEffectiveDates(lstUpdateValidateParams);
                        if (updateres)
                        {
                            if (routingRuleDt.Count > 0)
                            {
                                FullFillmentRuleData lstFulfillUpdateSite = new FullFillmentRuleData();

                                lstFulfillUpdateSite.Split = splitPercentValue;
                                lstFulfillUpdateSite.sites = fulfillmentSiteValue;
                                lstFulfillUpdateSite.Routing = routing;
                                lstFulfillUpdateSite.sys_created_by = _fulfillmentAddUpdateParams.strUserName;
                                lstFulfillUpdateSite.effectiveBeginDate = _fulfillmentAddUpdateParams.objAddUpdateParams.effectiveStartDate;
                                lstFulfillUpdateSite.effectiveEndDate = _fulfillmentAddUpdateParams.objAddUpdateParams.effectiveEndDate;

                                _iFullFillmentSiteManagement.updatefulfillment(lstFulfillUpdateSite);

                                routingRuleDt.RemoveAt(0);

                            }
                            else if (routingRuleDt.Count == 0)
                            {
                                FullFillmentRuleData lstFulfillSite = new FullFillmentRuleData();

                                lstFulfillSite.Routing = routing;
                                lstFulfillSite.sites = fulfillmentSiteValue;
                                lstFulfillSite.Split = splitPercentValue;
                                lstFulfillSite.sys_source = _fulfillmentAddUpdateParams.sysSource;
                                lstFulfillSite.sys_created_by = _fulfillmentAddUpdateParams.strUserName;
                                lstFulfillSite.last_modified_by = _fulfillmentAddUpdateParams.strUserName;
                                lstFulfillSite.sys_Ent_State = _fulfillmentAddUpdateParams.strSysEnt;

                                _iFullFillmentSiteManagement.AddFullFillmentsites(lstFulfillSite);
                            }
                        } //update validation dates brace ends here.
                        else
                        {
                            errMsg = "Dates are overlapping with existing rules.";
                            isError = true;
                            errCode = "1";
                            string res = errCode + "::" + isError + "::" + errMsg;
                            validationFlag = false;

                            response = new ObjectResult(res);
                            response.StatusCode = (int)HttpStatusCode.OK;
                            return response;
                        }
                    }
                }
                else
                {
                    string[] intermArray = _fulfillmentAddUpdateParams.objselectedRow.fulfillmentSiteId.ToString().Split('(');
                    fulfillmentSiteValue = intermArray[0].ToString(); //Fulfillment site value
                    splitPercentValue = intermArray[1].Replace("(", "").Replace(")", "").ToString(); //Split %  

                    UpdateValidateDateInputParams lstUpdateValidateParams = new UpdateValidateDateInputParams();

                    lstUpdateValidateParams.routing = routing;
                    lstUpdateValidateParams.strEffectiveBeginDate = _fulfillmentAddUpdateParams.objAddUpdateParams.effectiveStartDate;
                    lstUpdateValidateParams.strEffectiveEndDate = _fulfillmentAddUpdateParams.objAddUpdateParams.effectiveStartDate;

                    //validate update dates
                    string startDate = lstUpdateValidateParams.strEffectiveBeginDate;
                    string endDate = lstUpdateValidateParams.strEffectiveEndDate;
                    //Convert the strings into standard date formats.

                    if (startDate != null && startDate != " " && startDate != string.Empty)
                    {
                        if (DateTime.Parse(startDate).ToShortDateString() == "12/31/9999")
                        {
                            errMsg = "Invalid Start Date";
                            isError = true;
                            errCode = "1";
                            string res = errCode + "::" + isError + "::" + errMsg;

                            response = new ObjectResult(res);
                            response.StatusCode = (int)HttpStatusCode.OK;
                            return response;
                        }
                    }
                    if (endDate != null && endDate != " " && endDate != string.Empty)
                    {
                        if (DateTime.Parse(endDate).ToShortDateString() == "12/31/9999")
                        {
                            errMsg = "Invalid End Date";
                            isError = true;
                            errCode = "1";
                            string res = errCode + "::" + isError + "::" + errMsg;

                            response = new ObjectResult(res);
                            response.StatusCode = (int)HttpStatusCode.OK;
                            return response;
                        }
                    }

                    //Validation: Start date should less than end date
                    if (startDate != null && endDate != null && startDate != " " && startDate != string.Empty && endDate != " " && endDate != string.Empty)
                    {
                        if (DateTime.Parse(startDate) > DateTime.Parse(endDate))
                        {
                            errMsg = "Effective Start Date must be less than Effective End Date";
                            isError = true;
                            errCode = "1";
                            string res = errCode + "::" + isError + "::" + errMsg;

                            response = new ObjectResult(res);
                            response.StatusCode = (int)HttpStatusCode.OK;
                            return response;
                        }
                    }

                    bool updateres = _iFullFillmentSiteManagement.UpdateValidateEffectiveDates(lstUpdateValidateParams);

                    if (updateres)
                    {
                        if (routingRuleDt.Count > 0)
                        {
                            FullFillmentRuleData lstFulfillUpdateSite = new FullFillmentRuleData();

                            lstFulfillUpdateSite.Split = splitPercentValue;
                            lstFulfillUpdateSite.sites = fulfillmentSiteValue;
                            lstFulfillUpdateSite.Routing = routing;
                            lstFulfillUpdateSite.sys_created_by = _fulfillmentAddUpdateParams.strUserName;
                            lstFulfillUpdateSite.effectiveBeginDate = _fulfillmentAddUpdateParams.objAddUpdateParams.effectiveStartDate;
                            lstFulfillUpdateSite.effectiveEndDate = _fulfillmentAddUpdateParams.objAddUpdateParams.effectiveEndDate;

                            _iFullFillmentSiteManagement.updatefulfillment(lstFulfillUpdateSite);

                            routingRuleDt.RemoveAt(0);
                        }
                        else if (routingRuleDt.Count == 0)
                        {
                            FullFillmentRuleData lstFulfillSite = new FullFillmentRuleData();

                            lstFulfillSite.Routing = routing;
                            lstFulfillSite.sites = fulfillmentSiteValue;
                            lstFulfillSite.Split = splitPercentValue;
                            lstFulfillSite.sys_source = _fulfillmentAddUpdateParams.sysSource;
                            lstFulfillSite.sys_created_by = _fulfillmentAddUpdateParams.strUserName;
                            lstFulfillSite.last_modified_by = _fulfillmentAddUpdateParams.strUserName;
                            lstFulfillSite.sys_Ent_State = _fulfillmentAddUpdateParams.strSysEnt;

                            _iFullFillmentSiteManagement.AddFullFillmentsites(lstFulfillSite);
                        }
                    } //update validation dates brace ends here.
                    else
                    {
                        errMsg = "Dates are overlapping with existing rules.";
                        isError = true;
                        errCode = "1";
                        string res = errCode + "::" + isError + "::" + errMsg;
                        validationFlag = false;

                        response = new ObjectResult(res);
                        response.StatusCode = (int)HttpStatusCode.OK;
                        return response;
                    }
                }

                if (validationFlag)
                {
                    for (var j = 0; j < routingRuleDt.Count; j++)
                    {
                        var fullfillmentsite = routingRuleDt[j].FulfillmentSiteId;

                        FullFillmentRuleData lstFulfillDeleteSite = new FullFillmentRuleData();

                        lstFulfillDeleteSite.Routing = routing;
                        lstFulfillDeleteSite.sites = fullfillmentsite;
                        lstFulfillDeleteSite.sys_created_by = _fulfillmentAddUpdateParams.strUserName;

                        this.DeleteFulfillDetails(lstFulfillDeleteSite);
                    }
                }
            }

            if(validationFlag)
            {
                errMsg = "success";
                isError = false;
                errCode = "0";
                string res = errCode + "::" + isError + "::" + errMsg;
                validationFlag = false;

                response = new ObjectResult(res);
                response.StatusCode = (int)HttpStatusCode.OK;
                return response;
            }
            else
            {
                errMsg = "Fail";
                isError = true;
                errCode = "1";
                string res = errCode + "::" + isError + "::" + errMsg;
                validationFlag = true;

                response = new ObjectResult(res);
                response.StatusCode = (int)HttpStatusCode.OK;
                return response;
            }
        }

        [HttpPost]
        [Route("GetNonVirtualSites")]
        public ObjectResult GetNonVirtualSites([FromBody] MainSiteDDLInputParams ddlMainSiteInputParams)
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                var regionCode = "";
                var strRegion = "";
                if (ddlMainSiteInputParams.selectedCountry != null && ddlMainSiteInputParams.selectedCountry.ItemName != "--SELECT--")
                {
                    regionCode = ddlMainSiteInputParams.selectedCountry.Id;
                    regionCode = regionCode + "-COUNTRY";
                }
                strRegion = _iFullFillmentSiteManagement.getRegionName(regionCode);

                /////get geolevel 
                var geoLevelforsite_edit = "";
                var geoId_ForSite = "";
                if (ddlMainSiteInputParams.selectedGeoTreeNodeValue != null && ddlMainSiteInputParams.selectedGeoTreeNodeValue != "")
                {
                    geoLevelforsite_edit = ddlMainSiteInputParams.selectedGeoTreeNodeValue.ToString().Split("__")[1];
                    geoId_ForSite = ddlMainSiteInputParams.selectedGeoTreeNodeValue.ToString().Split("__")[0];
                }

                var geoLevelVal_ForSite = "";

                if (geoLevelforsite_edit == "ALL_GEO")
                {
                    geoLevelVal_ForSite = "ALL_GEO";
                }
                if (geoLevelforsite_edit == "1")
                {
                    geoLevelVal_ForSite = "REGION";
                }
                if (geoLevelforsite_edit == "2")
                {
                    geoLevelVal_ForSite = "SUB_REGION";
                }
                if (geoLevelforsite_edit == "3")
                {
                    geoLevelVal_ForSite = "AREA";
                }
                if (geoLevelforsite_edit == "4")
                {
                    geoLevelVal_ForSite = "COUNTRY";
                }

                if (geoLevelVal_ForSite == "REGION")
                {
                    strRegion = geoId_ForSite;
                    if (strRegion != null && strRegion != "")
                    {
                        strRegion = strRegion.Substring(0, strRegion.IndexOf("("));
                    }
                }
                if (geoLevelVal_ForSite != "ALL_GEO")
                {
                    if (geoLevelVal_ForSite != null && geoLevelVal_ForSite != "" && geoLevelVal_ForSite != "REGION" && geoId_ForSite != null && geoId_ForSite != "")
                    {
                        regionCode = geoId_ForSite;
                        if (regionCode != null && regionCode != "")
                        {
                            regionCode = regionCode.Substring(0, regionCode.IndexOf("("));
                        }
                        regionCode = regionCode + "-" + geoLevelVal_ForSite;
                        strRegion = _iFullFillmentSiteManagement.getRegionName(regionCode);
                    }
                }
                if (ddlMainSiteInputParams.selectedGeoTreeNodeValue == "ALL_GEO")
                {
                    strRegion = "ALL_GEO";
                }

                if (ddlMainSiteInputParams.selectedGeography.ItemName == "ALL")
                {
                    strRegion = "ALL_GEO";
                }


                //string key = "GEO";
                var fileNameList = _iFullFillmentSiteManagement.GetNonVirtualSites(strRegion);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }

        [HttpGet]
        [Route("IsVirtualSite")]
        public ObjectResult IsVirtualSite(string siteName)
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                //string key = "GEO";
                var fileNameList = _iFullFillmentSiteManagement.IsVirtualSite(siteName);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }

        [HttpGet]
        [Route("retrieveRoutingDetailsByRoutingId")]
        public ObjectResult retrieveRoutingDetailsByRoutingId(string routingID)
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                //string key = "GEO";
                var fileNameList =  _iFullFillmentSiteManagement.retrieveRoutingDetailsByRoutingId(routingID);

                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }


        [HttpPost]
        [Route("retrieveRoutingDetails")]
        public ObjectResult retrieveRoutingDetails([FromBody] FulFillmentSearchInput strRetriveInputParams)
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                //string key = "GEO";
                var fileNameList = _iFullFillmentSiteManagement.retrieveRoutingDetails(strRetriveInputParams);

                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }

        [HttpPost]
        [Route("InsertValidateEffectiveDates")]
        public ObjectResult InsertValidateEffectiveDates([FromBody] FullFillmentRuleData _FullFillmentRuleData)
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                var fileNameList = _iFullFillmentSiteManagement.InsertValidateEffectiveDates(_FullFillmentRuleData);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }


        [HttpPost]
        [Route("AddRoutingdetails")]

        //public async Task<ObjectResult> GetMasterDataData(string key)
        public ObjectResult AddRoutingdetails([FromBody] FullFillmentRuleData _FullFillmentRuleData)
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                var fileNameList =  _iFullFillmentSiteManagement.AddRoutingdetails(_FullFillmentRuleData);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }

        [HttpPost]
        [Route("AddFullFillmentsites")]
        public ObjectResult AddFullFillmentsites([FromBody] FullFillmentRuleData _FullFillmentRuleData)
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                var fileNameList = _iFullFillmentSiteManagement.AddFullFillmentsites(_FullFillmentRuleData);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }


        [HttpPost]
        [Route("UpdateValidateEffectiveDates")]
        public ObjectResult UpdateValidateEffectiveDates([FromBody] UpdateValidateDateInputParams lstUpdateValidateParams)
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                var fileNameList = _iFullFillmentSiteManagement.UpdateValidateEffectiveDates(lstUpdateValidateParams);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }

        [HttpPost]
        [Route("updatefulfillment")]
        public ObjectResult updatefulfillment([FromBody] FullFillmentRuleData updateFulfillment)
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                var fileNameList = _iFullFillmentSiteManagement.updatefulfillment(updateFulfillment);
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }


        [HttpPost]
        [Route("DeleteFulfillDetails")]
        public ObjectResult DeleteFulfillDetails([FromBody] FullFillmentRuleData lstFulfillDeleteSite)
        {
            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                var fileNameList = _iFullFillmentSiteManagement.DeleteFulfillDetails(lstFulfillDeleteSite);
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }


        [HttpGet]
        [Route("DeleteRoutingDetailsAndHeaders")]
        public ObjectResult DeleteRoutingDetailsAndHeaders(string singleDeleteFlag, string id)
        {

            ObjectResult response;
            _iFullFillmentSiteManagement.UserID = this.userName;
            try
            {
                var result1 = _iFullFillmentSiteManagement.DeleteRoutingDetails(singleDeleteFlag,id);
                var result2 = _iFullFillmentSiteManagement.DeleteRoutingDetailsHeader(singleDeleteFlag, id);
                var fileNameList = result1.Id + "," +result2.Id;

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

    }
}