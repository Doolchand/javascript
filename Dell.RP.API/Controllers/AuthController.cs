﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using Dell.RP.BussinessServices.IServiceInterceptor;
using System.Net;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Hosting;
using Dell.RP.Common.Models;
using System.Text.RegularExpressions;
using Dell.RP.API.Controllers.HelperClasses;

namespace Dell.RP.API.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    [Authorize("AllUsers")]
    public class AuthController : Controller
    {
        #region Constructor & Local variables
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ISessionSettingService iSessionSettingService;
        private string userName = "";
        UserDetails user = new UserDetails();

        sessionSettingModel sessionSetting = new sessionSettingModel();

        public AuthController(ISessionSettingService iSessionSettingService, IHttpContextAccessor httpContextAccessor, IClaimsTransformation d, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            this.iSessionSettingService = iSessionSettingService;
            var username = UserNameValidation.GetUser(httpContextAccessor);
            if (username.Result != null)
            {
                this.userName = username.Result;
            }
        }
        #endregion

        [HttpGet]
        [Route("GetSessionSettings")]
        public async Task<ObjectResult> GetSessionSettingsAsync()
        {
            ObjectResult response;
            user.UserID = this.userName;
            try
            {
                var fileNameList = await iSessionSettingService.GetSessionSettingsAsync(user);
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpPost]
        [Route("updateUserSession")]
        public async Task<ObjectResult> UpdateUserSession([FromBody] updateUserSessionModel param)
        {
            ObjectResult response;
            param.UserID = this.userName;
            try
            {
                var fileNameList = await iSessionSettingService.UpdateUserSessionAsync(param);
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet]
        [Route("Login")]
        public IActionResult Login()
        {
            string userID = string.Empty;
            string domainName = string.Empty;
            try
            {
                // Added Windows Authentication
                string windowsID = Regex.Replace(User.Identity.Name, @"[']", "_");
                if (windowsID != null)
                {
                    string[] splitID = windowsID.Split('\\');
                    if (splitID.Count() > 0)
                    {
                        userID = splitID[1].ToString();
                        domainName = splitID[0].ToString();
                    }

                  // SetSession();

                    // If windows authentication not there get the Uderid and name from Ui and Validate to DB
                    // If the SSO implmented then SSO service Code to be implamented here
                    // Add authentication code here ex. User is having access to application
                    //if (!string.IsNullOrWhiteSpace(userID) || !string.IsNullOrWhiteSpace(domainName))
                    //{
                    //    // HttpContext.Session.SetString("LoggedInUser", JsonConvert.SerializeObject(userID));
                    //    // HttpContext.Session.SetString("LoggedInUser", JsonConvert.SerializeObject(domainName));
                    //}
                }
            }
            catch
            {
                
            }
            return Ok(new { userID, domainName });
        }

    }
}
