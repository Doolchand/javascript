﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.MultiTierBOM;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.L10ItemList;
using Dell.RP.API.Controllers.HelperClasses;
using Dell.RP.API.Dell.RP.Common.Models.MultiTierBOM;
using Dell.RP.API.Dell.RP.Common.Models.L10ItemList;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Net;


namespace Dell.RP.API.Controllers.MultiTierBOM
{
    [Route("api/[controller]")]
    [Authorize(Policy = "AllUsers")]
    public class L10ItemListController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IL10ItemList iIL10ItemListService;

        #region Constructor & Local variables
        private string userName = "";

        public L10ItemListController(IL10ItemList il10ItemListSvc, IHttpContextAccessor httpContextAccessor, IClaimsTransformation d, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            this.iIL10ItemListService = il10ItemListSvc;
            var s = UserNameValidation.GetUser(httpContextAccessor);
            if (s.Result != null)
            {
                this.userName = s.Result;
            }
        }
        #endregion

        [HttpPost]
        [Route("Getl10FilterViewResult")]
        public async Task<ObjectResult> Getl10FilterViewResult([FromBody] l10Item item)
        {
            ObjectResult response;
            iIL10ItemListService.UserID = this.userName;

            try
            {
                var gridData = await iIL10ItemListService.Getl10FilterViewResult(item);
                if (gridData != null && gridData.Count > 0)
                {
                    response = new ObjectResult(gridData);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }

        [HttpPost]
        [Route("Getl10FilterViewResultExcel")]
        public async Task<ObjectResult> Getl10FilterViewResultExcel([FromBody] l10Item item)
        {
            ObjectResult response;
            iIL10ItemListService.UserID = this.userName;

            try
            {
                var gridData = await iIL10ItemListService.Getl10FilterViewResultExcel(item);
                if (gridData != null && gridData.Count > 0)
                {
                    response = new ObjectResult(gridData);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }

        

        [HttpGet]
        [Route("Getl10ItemListDetails")]
        public async Task<ObjectResult> Getl10ItemListDetails()
        {
            ObjectResult response;
            iIL10ItemListService.UserID = this.userName;

            try
            {
                var gridData = await iIL10ItemListService.Getl10ItemList();
                if (gridData != null && gridData.Count >0)
                {
                    response = new ObjectResult(gridData);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpPost]
        [Route("Updatel10ItemListDetails")]
        public async Task<ObjectResult> Updatel10ItemListDetails([FromBody] List<l10Item> updateDetails)
        {
            ObjectResult response;
            iIL10ItemListService.UserID = this.userName;
            try
            {

                string result = await iIL10ItemListService.Updatel10ItemListDetails(updateDetails);
                if (result.Split('~')[1].Equals("SUCCESS"))
                {
                    response = new ObjectResult("Data has been Updated Successfully");
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }

        
    }
}