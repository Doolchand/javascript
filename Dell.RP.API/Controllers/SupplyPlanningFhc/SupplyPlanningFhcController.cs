﻿using Dell.RP.API.Controllers.HelperClasses;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.SupplyPlanningFhc;
using Dell.RP.API.Dell.RP.Common.Models.SupplyPlanningFhc;
using Dell.RP.Common.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Dell.RP.API.Controllers.SupplyPlanningFhc
{
    [Route("api/[controller]")]
    [Authorize(Policy = "AllUsers")]
    public class SupplyPlanningFhcController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ISupplyPlanningFhcService _iSupplyPlanningFhcService;


        #region Constructor & Local variables
        private string userName = "";

        public SupplyPlanningFhcController(ISupplyPlanningFhcService iSupplyPlanningFhcService, IHttpContextAccessor httpContextAccessor, IClaimsTransformation d, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            this._iSupplyPlanningFhcService = iSupplyPlanningFhcService;
            var s = UserNameValidation.GetUser(httpContextAccessor);
            if (s.Result != null)
            {
                this.userName = s.Result;
            }
        }
        #endregion

        [HttpGet]
        [Route("GetManSite")]
        public async Task<ObjectResult> GetManSite()
        {
            ObjectResult response;
            _iSupplyPlanningFhcService.UserID = this.userName;
            try
            {
                var fileNameList = await _iSupplyPlanningFhcService.GetManSite();
                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetRPStatus")]
        public ObjectResult GetRPStatus()
        {
            ObjectResult response;
            _iSupplyPlanningFhcService.UserID = this.userName;
            try
            {
                var fileNameList = _iSupplyPlanningFhcService.GetRPStatus();
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("BindPOUILock")]
        public ObjectResult BindPOUILock()
        {
            ObjectResult response;
            _iSupplyPlanningFhcService.UserID = this.userName;
            try
            {
                var fileNameList = _iSupplyPlanningFhcService.BindPOUILock();
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpPost]
        [Route("GetRPSupplyPlanningFhcExportDetails")]
        public async Task<ObjectResult> GetRPSupplyPlanningFhcExportDetails([FromBody] Export_Parameters exportParams)
        {
            ObjectResult response;
            _iSupplyPlanningFhcService.UserID = this.userName;
            try
            {
                var listResult = await _iSupplyPlanningFhcService.GetRPSupplyPlanningFhcExportDetails(exportParams);
                if (listResult != null && listResult.Count > 0)
                {
                    response = new ObjectResult(listResult);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetFamilyParentDD")]
        public async Task<ObjectResult> GetFamilyParentDD()
        {
            ObjectResult response;
            _iSupplyPlanningFhcService.UserID = this.userName;
            try
            {
                var fileNameList = await _iSupplyPlanningFhcService.GetFamilyParentDD();
                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpPost]
        [Route("InsertFhc")]
        public ObjectResult InsertFHC([FromBody] List<RPSupplyPlan_Import_Params> importData)
        {
            ObjectResult response;
            _iSupplyPlanningFhcService.UserID = this.userName;
            try
            {
                var fileNameList = _iSupplyPlanningFhcService.InsertFHC(importData);
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetErrGridDetails")]
        public ObjectResult GetErrGridDetails()
        {
            ObjectResult response;
            _iSupplyPlanningFhcService.UserID = this.userName;
            try
            {
                var listResult = _iSupplyPlanningFhcService.getErrGridDetails();
                if (listResult != null && listResult.Count > 0)
                {
                    response = new ObjectResult(listResult);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetLockStatusFHC")]
        public ObjectResult GetLockStatusFHC(string flag)
        {
            ObjectResult response;
            _iSupplyPlanningFhcService.UserID = this.userName;
            try
            {
                var fileNameList = _iSupplyPlanningFhcService.GetLockStatusFHC(flag);
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetValidRecStatusFHC")]
        public ObjectResult GetValidRecStatusFHC()
        {
            ObjectResult response;
            _iSupplyPlanningFhcService.UserID = this.userName;
            try
            {
                var fileNameList = _iSupplyPlanningFhcService.GetValidRecStatusFHC();
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("UpdateAutoUnlockUIFHC")]
        public ObjectResult UpdateAutoUnlockUIFHC()
        {
            ObjectResult response;
            _iSupplyPlanningFhcService.UserID = this.userName;
            try
            {
                var fileNameList = _iSupplyPlanningFhcService.UpdateAutoUnlockUIFHC();
                if (fileNameList.ToString() != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

    }
}
