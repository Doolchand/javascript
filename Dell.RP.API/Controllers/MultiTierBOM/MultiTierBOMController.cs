﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.MultiTierBOM;
using Dell.RP.API.Controllers.HelperClasses;
using Dell.RP.API.Dell.RP.Common.Models.MultiTierBOM;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Net;

namespace Dell.RP.API.Controllers.MultiTierBOM
{
    [Route("api/[controller]")]
    [Authorize(Policy = "AllUsers")]
    public class MultiTierBOMController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IMultiTierBOM iMultiTierBOMService;

        #region Constructor & Local variables
        private string userName = "";

        public MultiTierBOMController(IMultiTierBOM iMultiTierBOMService, IHttpContextAccessor httpContextAccessor, IClaimsTransformation d, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            this.iMultiTierBOMService = iMultiTierBOMService;
            var s = UserNameValidation.GetUser(httpContextAccessor);
            if (s.Result != null)
            {
                this.userName = s.Result;
            }
        }
        #endregion

        //[HttpPost]
        //[Route("GetFilterViewResultCount")]
        //public async Task<ObjectResult> GetFilterViewResultCount([FromBody] MultiTierBOMDetails multiTierBom)
        //{
        //    ObjectResult response;
        //    iMultiTierBOMService.UserID = this.userName;

        //    try
        //    {
        //        //var gridData = await iMultiTierBOMService.GetFilterViewResult(multiTierBom, 0 , 2108385);
        //        //if (gridData != null && gridData.Count > 0)
        //        //{
        //            response = new ObjectResult(2108385);
        //            response.StatusCode = (int)HttpStatusCode.OK;
        //            return response;
        //        //}
        //        //else
        //        //{
        //        //    response = new ObjectResult("No data found.");
        //        //    response.StatusCode = (int)HttpStatusCode.NoContent;
        //        //    return response;
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        response = new ObjectResult(ex.Message);
        //        response.StatusCode = (int)HttpStatusCode.InternalServerError;
        //    }
        //    return response;

        //}

        //[HttpPost]
        //[Route("GetFilterViewResult")]
        //public async Task<ObjectResult> GetFilterViewResult(int fromNumber, int count, [FromBody] MultiTierBOMDetails multiTierBom)
        //{
        //    ObjectResult response;
        //    iMultiTierBOMService.UserID = this.userName;

        //    try
        //    {
        //        var gridData = await iMultiTierBOMService.GetFilterViewResult(multiTierBom, fromNumber, count);
        //        if (gridData != null && gridData.Count > 0)
        //        {
        //            //gridData = gridData.GetRange(fromNumber, count);
        //            response = new ObjectResult(gridData);
        //            response.StatusCode = (int)HttpStatusCode.OK;
        //            return response;
        //        }
        //        else
        //        {
        //            response = new ObjectResult("No data found.");
        //            response.StatusCode = (int)HttpStatusCode.NoContent;
        //            return response;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response = new ObjectResult(ex.Message);
        //        response.StatusCode = (int)HttpStatusCode.InternalServerError;
        //    }
        //    return response;

        //}

        [HttpPost]
        [Route("GetFilterViewResult")]
        public async Task<ObjectResult> GetFilterViewResult(int fromNumber, int count, [FromBody] MultiTierBOMDetails multiTierBom)
        {
            ObjectResult response;
            iMultiTierBOMService.UserID = this.userName;

            try
            {
                var gridData = await iMultiTierBOMService.GetFilterViewResult(multiTierBom, fromNumber, count);
                if (gridData != null && gridData.Count > 0)
                {
                    //gridData = gridData.GetRange(fromNumber, count);
                    response = new ObjectResult(gridData);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetCommodityCodeDDL")]
        public async Task<ObjectResult> GetCommodityCodeDDL()
        {
            ObjectResult response;
            iMultiTierBOMService.UserID = this.userName;

            try
            {
                var gridData = await iMultiTierBOMService.GetCommodityCodeDDL();
                if (gridData != null && gridData.Count > 0)
                {
                    response = new ObjectResult(gridData);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpPost]
        [Route("UpdateMultiTierBOMDetails")]
        public async Task<ObjectResult> UpdateMultiTierBOMDetails([FromBody] List<MultiTierBOMDetails> updateDetails)
        {
            ObjectResult response;

            iMultiTierBOMService.UserID = this.userName;
            try
            {
                var fileNameList = await iMultiTierBOMService.UpdateMultiTierBOMDetails(updateDetails);
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpPost]
        [Route("GetMultiTierViewResultExcel")]
        public async Task<ObjectResult> GetMultiTierViewResultExcel([FromBody] MultiTierBOMDetails item)
        {
            ObjectResult response;
            iMultiTierBOMService.UserID = this.userName;

            try
            {
                var gridData = await iMultiTierBOMService.GetMultiTierViewResultExcelAsync(item);
                if (gridData != null && gridData.Count > 0)
                {
                    response = new ObjectResult(gridData);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }

    }
}