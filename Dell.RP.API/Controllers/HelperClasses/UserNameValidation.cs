﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Controllers.HelperClasses
{
    public class UserNameValidation
    {
        public static async Task<string> GetUser(IHttpContextAccessor httpContextAccessor)
        {
            return await Task.Run(() =>
            {
                return ValidateSpecialCharacter(httpContextAccessor.HttpContext.User?.Identity?.Name);
            });
        }

        public static string ValidateSpecialCharacter(string name)
        {
            return name.Replace("'", "!!");

        }

    }
}
