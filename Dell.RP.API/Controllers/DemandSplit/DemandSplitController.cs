﻿using Dell.RP.API.Controllers.HelperClasses;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.DemandSplit;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.Common.DemandSplit;
using Dell.RP.API.Dell.RP.Common.Models.DemandSplit;
using Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement;
using Dell.RP.Common.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;

namespace Dell.RP.API.Controllers.DemandSplit
{
    [Route("api/[controller]")]
    [Authorize(Policy = "AllUsers")]
    public class DemandSplitController: Controller
    {
        DataTable dtErrorDetails = null;

        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IDemandSplit _IDemandSplit;
        private string userName = "";
        private int intErrorRow = 0;


       


        public DemandSplitController(IDemandSplit _IDemandSplit, IHttpContextAccessor httpContextAccessor, IClaimsTransformation d, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            this._IDemandSplit = _IDemandSplit;
            var s = UserNameValidation.GetUser(httpContextAccessor);
            if (s.Result != null)
            {
                this.userName = s.Result;
            }
        }




        [HttpPost]
        [Route("GetDemandSplitViewResult")]
        public async Task<ObjectResult> GetDemandSplitViewResult([FromBody] DemandSearch Search)
        {
            ObjectResult response;
        //    iEmcProductHierarchyMaintenance.UserID = this.userName;

            try
            {
                var Searchresult = await _IDemandSplit.GetDemandSplitViewResult(Search);
                //if (fileNameList != null && fileNameList.Count > 0)
                 if (Searchresult != null )
                {
                    response = new ObjectResult(Searchresult);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }



      









        // Create Member Hierarchy Filter Section
        [HttpGet]
        [Route("getProduct")]
        public async Task<ObjectResult> getProduct()
        {
            ObjectResult response;
           // iEmcProductHierarchyMaintenance.UserID = this.userName;
            try
            {
                var Product = await _IDemandSplit.getProduct();
                if (Product != null)
                {
                    response = new ObjectResult(Product);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }



            [HttpGet]
            [Route("getChennal")]
            public async Task<ObjectResult> getChennal()
            {
                ObjectResult response;
                // iEmcProductHierarchyMaintenance.UserID = this.userName;
                try
                {
                    var fileNameList = await _IDemandSplit.getChennal();
                    if (fileNameList != null)
                    {
                        response = new ObjectResult(fileNameList);
                        response.StatusCode = (int)HttpStatusCode.OK;
                        return response;
                    }
                    else
                    {
                        response = new ObjectResult("No data found.");
                        response.StatusCode = (int)HttpStatusCode.NoContent;
                        return response;
                    }
                }
                catch (Exception ex)
                {
                    response = new ObjectResult(ex.Message);
                    response.StatusCode = (int)HttpStatusCode.InternalServerError;
                }
                return response;

            }

        [HttpGet]
        [Route("getGeography")]
        public async Task<ObjectResult> getGeography()
        {
            ObjectResult response;
            // iEmcProductHierarchyMaintenance.UserID = this.userName;
            try
            {
                var fileNameList = await _IDemandSplit.getGeography();
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }






        [HttpPost]
        [Route("uploadDemandData")]
        public async Task<ObjectResult> uploadDemandData([FromBody] List<SearchQuery> Details, string methodType)
        {
            ObjectResult response;
            Errocount Erromessage = new Errocount();

            List<DemandSplitErrorgrid> Errgrid = new List<DemandSplitErrorgrid>();
            try
            {
                // var fileNameList = "";//await iChanHeirarachyService.UpdateChanData(chanHierarchyDetails, methodType);
                // dtResult=   await UploadDemandSplitData(Details);
                Errocount c = await UploadDemandSplitData(Details);
                //Errgrid = ConvertDataTable <DemandSplitErrorgrid>(c.demandSplitErrorgrids);
                ErrocountUI errocountUI = new ErrocountUI();
                errocountUI.demandSplitErrorgrids= ConvertDataTable<DemandSplitErrorgrid>(c.demandSplitErrorgrids);

                errocountUI.CorrectRecords = c.CorrectRecords;
                errocountUI.Totalfailed = c.Totalfailed;
                errocountUI.Totalrow = c.Totalrow;
                errocountUI.successmsg = c.successmsg;
                errocountUI.Totalsuberror = c.Totalsuberror;
                errocountUI.ErrorMessage = c.ErrorMessage;
                //  var resrult = ConvertDataTable<DemandSplitErrorgrid>(dtResult);

                if (errocountUI != null)
                {
                    response = new ObjectResult(errocountUI);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }


        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }

        // Task<Dictionary>

        public async Task<Errocount> UploadDemandSplitData(List<SearchQuery> excelData)
        {


            string strFileLoc = null, userName = this.userName;
            DateTime date;
            int intNumberOfRows = 0, rowCount, intRowsEffected;
            this.intErrorRow = 0;
            long lngTick;
            DataTable dtErrorDetails = null;
            Errocount errorCount = new Errocount();
            DataTable  dtExcelData = null, dtUploadedData = null;
            //object[] rangeResult = null, objString = null;
            //object objTemp = null;
            //IEnumerator iEnumerator = null;
            //DataSet dsUploadedData = null;
            DataRow dr = null;
            string strUpladedFilePath;
            string channelid;
            string productid;
            string geoid;
            try
            {
                //Step1
                if (true)// if (fileUploadExcel.HasFile)
                {
                    date = DateTime.Now;
                    lngTick = date.Ticks;

                    //Get data object to data table
                    dtExcelData = ConvertToDataTable(excelData);
                    //

                    if (dtExcelData != null)
                    {
                        if (CheckColumns(dtExcelData))
                        {
                            if (dtExcelData.Rows.Count > 0)
                            {
                                dtExcelData.Columns.Add("PRODUCTIDVAL");
                                dtExcelData.Columns.Add("CHANNELIDVAL");
                                dtExcelData.Columns.Add("GEOIDVAL");
                                dtExcelData.Columns.Add("ChannelGeo");

                                //     Commits all the changes made to this table since the last time System.Data.DataTable.AcceptChanges
                                //     was called.
                                dtExcelData.AcceptChanges();

                                for (int count = 0; count < dtExcelData.Rows.Count; count++)
                                {
                                    productid = dtExcelData.Rows[count]["PRODUCT_ID"].ToString();
                                    channelid = dtExcelData.Rows[count]["CHANNEL_ID"].ToString();
                                    geoid = dtExcelData.Rows[count]["GEOGRAPHY_ID"].ToString();
                                    if (String.IsNullOrEmpty(dtExcelData.Rows[count][0].ToString()) == true)
                                    {
                                        break;
                                    }
                                    if (String.IsNullOrEmpty(dtExcelData.Rows[count][1].ToString()) == true)
                                    {
                                        break;
                                    }
                                    if (String.IsNullOrEmpty(dtExcelData.Rows[count][2].ToString()) == true)
                                    {
                                        break;
                                    }

                                    else
                                    {

                                        if (productid.Length > 0)
                                        {
                                            if (productid.Contains("(") && productid.Contains(")"))
                                            {
                                                int countofindex = Convert.ToInt32(productid.IndexOf("(").ToString());
                                                int endIndex = Convert.ToInt32(productid.IndexOf(")").ToString());
                                                int length = endIndex - countofindex;
                                                productid = productid.Substring(countofindex + 1, length - 1);
                                                dtExcelData.Rows[count]["PRODUCTIDVAL"] = productid;
                                            }
                                            else if (productid.Length > 0)
                                            {
                                                dtExcelData.Rows[count]["PRODUCTIDVAL"] = productid;
                                            }
                                            if ((channelid.Length > 0 && channelid.Contains("(") && channelid.Contains(")")))
                                            {
                                                int countofindex1 = Convert.ToInt32(channelid.IndexOf("(").ToString());
                                                int endIndex1 = Convert.ToInt32(channelid.IndexOf(")").ToString());
                                                int length1 = endIndex1 - countofindex1;
                                                channelid = channelid.Substring(0, countofindex1);
                                                dtExcelData.Rows[count]["CHANNELIDVAL"] = channelid;
                                            }
                                            else if (channelid.Length > 0)
                                            {
                                                dtExcelData.Rows[count]["CHANNELIDVAL"] = channelid;
                                            }
                                            if ((geoid.Length > 0 && geoid.Contains("(") && geoid.Contains(")")))
                                            {
                                                int countofindex2 = Convert.ToInt32(geoid.IndexOf("(").ToString());
                                                int endIndex2 = Convert.ToInt32(geoid.IndexOf(")").ToString());
                                                int length2 = endIndex2 - countofindex2;
                                                geoid = geoid.Substring(0, countofindex2);
                                                dtExcelData.Rows[count]["GEOIDVAL"] = geoid;
                                            }
                                            else if (geoid.Length > 0)
                                            {
                                                dtExcelData.Rows[count]["GEOIDVAL"] = geoid;
                                            }
                                            dtExcelData.Rows[count]["ChannelGeo"] = dtExcelData.Rows[count]["PRODUCTIDVAL"].ToString().Replace(" ", "") + dtExcelData.Rows[count]["CHANNELIDVAL"].ToString().Replace(" ", "") + dtExcelData.Rows[count]["GEOIDVAL"].ToString().Replace(" ", "");
                                            intNumberOfRows = count + 1;
                                        }

                                    }
                                }
                                rowCount = dtExcelData.Rows.Count;

                                dtErrorDetails = await demandSplitDataValidationAsync(dtExcelData);
                                //DataTable dtErrorDetails1 = await demandSplitDataValidationAsync(dtExcelData);
                                // pnlMsg.Visible = true;
                                if (dtErrorDetails.Rows.Count < 1)
                                {
                                    //pnlDisplayData.Visible = true;
                                    //pnlError.Visible = false;
                                    date = DateTime.Now;
                                    lngTick = date.Ticks;
                                    
                                    intRowsEffected =  _IDemandSplit.DemandSplitBulkInsert(dtExcelData, lngTick.ToString(), userName);
                                    if (intRowsEffected > 0)
                                    {
                                        //pnlDisplayData.Visible = true;
                                        //pnlPart.Visible = true;
                                        string channelidval = "ALL";
                                        string productidval = "ALL";
                                        string geoidval = "ALL";


                                      //  return dtErrorDetails;

                                        //To Show orignal data grid post successfull submission
                                        //  dtUploadedData = getDemandSplitdatawithnames(channelidval, productidval, geoidval);
                                        //gridUplaodDetails.DataSource = dtUploadedData;
                                        //gridUplaodDetails.DataBind();
                                        //lblFail.Visible = true;
                                        //lblCorrect.Visible = true;
                                        //lblTotalRows.Visible = true;
                                        //lblchannelmsg.Visible = true;
                                        //lblGeomsg.Visible = true;
                                        //lblproductmsg.Visible = true;
                                        //lblGeoval.Text = geoidval;
                                        //lblproductval.Text = productidval;
                                        //lblchannelval.Text = channelidval;
                                        //lblTotalRowsMsg.Text = dtExcelData.Rows.Count.ToString();
                                        //lblFailMsg.Text = 0.ToString();
                                        //lblCorrectMsg.Text = dtExcelData.Rows.Count.ToString();
                                        //string successMsg = ConfigurationManager.AppSettings["msgUploadSuccess"].ToString();
                                        //lblMsg.Text = successMsg;
                                        //lblMsg.ForeColor = System.Drawing.Color.Green;
                                        //imgSuccess.Visible = true;
                                        //pnlError.Visible = false;
                                        //bindDdl();
                                        //imgError.Visible = false;

                                        //btnExportToEcxel.Visible = true;
                                        //ViewState.Remove("channelid");
                                        //ViewState.Remove("productid");
                                        //ViewState.Remove("geoid");
                                        errorCount.Totalrow = dtExcelData.Rows.Count.ToString();
                                        errorCount.Totalfailed = intErrorRow;
                                        errorCount.CorrectRecords = dtExcelData.Rows.Count;
                                        errorCount.successmsg = "File successfully uploaded.";
                                        errorCount.Totalsuberror = dtErrorDetails.Rows.Count;

                                        errorCount.demandSplitErrorgrids = dtErrorDetails;
                                        return errorCount;

                                    }
                                    else
                                    {

                                        errorCount.Totalrow = dtExcelData.Rows.Count.ToString();
                                        errorCount.Totalfailed = Convert.ToInt32( intErrorRow);
                                        errorCount.CorrectRecords = Convert.ToInt32(dtExcelData.Rows.Count - intErrorRow);
                                        errorCount.successmsg = "";
                                        errorCount.Totalsuberror = dtErrorDetails.Rows.Count;
                                        errorCount.ErrorMessage = "There are " + dtErrorDetails.Rows.Count.ToString() + " errors in the uploaded file, Please correct the entries and re-upload the file.";


                                        errorCount.demandSplitErrorgrids = dtErrorDetails;
                                        return errorCount;

                                        //Display orignal grid with earlier search values
                                        // dtUploadedData = getDemandSplitdatawithnames("", "", "");
                                        //gridUplaodDetails.DataSource = dtUploadedData;
                                        //gridUplaodDetails.DataBind();
                                        //pnlMsg.Visible = false;
                                        //pnlError.Visible = false;
                                        //pnlDisplayData.Visible = true;
                                        //pnlPart.Visible = true;
                                        //string EmptyMsg = ConfigurationManager.AppSettings["msgEmptyFile"].ToString();
                                        //lblMsg.Text = EmptyMsg;
                                        //lblMsg.ForeColor = System.Drawing.Color.Red;
                                        //imgError.Visible = true;
                                        //lblFail.Visible = false;
                                        //lblCorrect.Visible = false;
                                        //lblTotalRows.Visible = false;
                                        //imgSuccess.Visible = false;
                                        //btnExportToEcxel.Visible = false;
                                        //lblchannelmsg.Visible = true;
                                        //lblGeomsg.Visible = true;
                                        //lblproductmsg.Visible = true;

                                    }
                                }
                                else
                                {
                                    // Show Error Grid
                                    // return dtErrorDetails;
                                    errorCount.Totalrow = dtExcelData.Rows.Count.ToString();
                                    errorCount.Totalfailed = intErrorRow;
                                    errorCount.CorrectRecords = dtExcelData.Rows.Count - intErrorRow;
                                    errorCount.Totalsuberror = dtErrorDetails.Rows.Count;
                                    errorCount.ErrorMessage= "There are " + dtErrorDetails.Rows.Count.ToString() + " errors in the uploaded file, Please correct the entries and re-upload the file.";
                                    errorCount.demandSplitErrorgrids = dtErrorDetails;
                                    return errorCount;
                                    //gridErrorDetails.DataSource = dtErrorDetails;

                                    //lblTotalRowsMsg.Text = dtExcelData.Rows.Count.ToString();

                                    //lblCorrectMsg.Text = (dtExcelData.Rows.Count - intErrorRow).ToString();
                                    //lblMsg.Text = "There are " + dtErrorDetails.Rows.Count.ToString() + " errors in the uploaded file, Please correct the entries and re-upload the file.";

                                }
                            }
                            else
                            {
                                //If Excel data is empty -- show orignal grid with already selected grid values + Message in red for empty file

                              //  dtUploadedData = getDemandSplitdatawithnames("", "", "");

                                //string EmptyMsg = ConfigurationManager.AppSettings["msgEmptyFile"].ToString();                                
                            }
                        }
                        else
                        { //IF Excel having incorrect no. of column 

                            // dtUploadedData = getDemandSplitdatawithnames("", "", "");

                            // string InvalidMsg = ConfigurationManager.AppSettings["msgInvalidFile"].ToString();



                            errorCount.Totalrow = dtExcelData.Rows.Count.ToString();
                            errorCount.Totalfailed = Convert.ToInt32(intErrorRow);
                            errorCount.CorrectRecords = Convert.ToInt32(dtExcelData.Rows.Count - intErrorRow);
                            errorCount.successmsg = "";
                            errorCount.Totalsuberror = dtErrorDetails.Rows.Count;
                            errorCount.ErrorMessage= "There are " + dtErrorDetails.Rows.Count.ToString() + " errors in the uploaded file, Please correct the entries and re-upload the file.";

                            errorCount.demandSplitErrorgrids = dtErrorDetails;
                            return errorCount;


                        }
                    }
                    else
                    {
                        //Show orignal grid with already search data 
                      //  dtUploadedData = getDemandSplitdatawithnames("", "", "");

                        //string formatmsg = ConfigurationManager.AppSettings["msgFileFormat"].ToString();

                    }
                }
                else
                {

                   // dtUploadedData = getDemandSplitdatawithnames("", "", "");

                    //  string selectmsg = ConfigurationManager.AppSettings["msgReqFile"].ToString();

                }
                errorCount.demandSplitErrorgrids = dtErrorDetails;
                return errorCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        bool CheckColumns(DataTable dt)
        {
            bool isCorrect = true;
            try
            {
                if (dt.Columns.Count >= 32)
                {
                    if (dt.Columns[0].ToString().ToUpper() != "PRODUCT_ID")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[1].ToString().ToUpper() != "CHANNEL_ID")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[2].ToString().ToUpper() != "GEOGRAPHY_ID")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[3].ToString().ToUpper() != "SSC")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[4].ToString() != "WK0")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[5].ToString() != "WK1")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[6].ToString() != "WK2")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[7].ToString() != "WK3")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[8].ToString() != "WK4")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[9].ToString() != "WK5")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[10].ToString() != "WK6")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[11].ToString() != "WK7")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[12].ToString() != "WK8")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[13].ToString() != "WK9")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[14].ToString() != "WK10")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[15].ToString() != "WK11")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[16].ToString() != "WK12")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[17].ToString() != "WK13")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[18].ToString() != "WK14")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[19].ToString() != "WK15")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[20].ToString() != "WK16")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[21].ToString() != "WK17")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[22].ToString() != "WK18")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[23].ToString() != "WK19")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[24].ToString() != "WK20")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[25].ToString() != "WK21")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[26].ToString() != "WK22")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[27].ToString() != "WK23")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[28].ToString() != "WK24")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[29].ToString() != "WK25")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[30].ToString() != "WK26")
                    {
                        isCorrect = false;
                    }
                    if (dt.Columns[31].ToString().ToUpper() != "REST_OF_HORIZON")
                    {
                        isCorrect = false;
                    }
                }
                else
                {
                    isCorrect = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isCorrect;
        }


        public  async Task<DataTable> demandSplitDataValidationAsync(DataTable dt)
        {
            //DataTable dt =   ConvertToDataTable(getDemandData());
            bool isAnyError = false;

            // DataTable dtErrorDetails = new DataTable();


            dtErrorDetails=  new System.Data.DataTable();
            dtErrorDetails.Columns.Add("rowrumber");
            dtErrorDetails.Columns.Add("errorcolumn");
            dtErrorDetails.Columns.Add("error");

            dtErrorDetails.Columns.Add("Totalrow ");
            dtErrorDetails.Columns.Add("Totalfailed ");
            dtErrorDetails.Columns.Add("CorrectRecords ");


            Dictionary<string, string> dictProductID = null;
            Dictionary<string, string> dictChannel = new Dictionary<string, string>();
            Dictionary<string, string> dictGeography = new Dictionary<string, string>();
            Dictionary<string, string> dictSSC = new Dictionary<string, string>();

            
            dictChannel = await  _IDemandSplit.GetChannelIdSplit();
            dictGeography = await _IDemandSplit.GetGeographyIdSplit();
            dictProductID = await _IDemandSplit.GetProductIdSplit();
            dictSSC = await _IDemandSplit.GetSupplychainSplit();
            bool isKeyPresent = false;

            DataRow drError = null; // Doolchand Suthar
           // DataRow drError;
            intErrorRow = 0;
            int sum = 0;
            string wk = null;
            int i = 1;
            bool isItemExist = true;
            Dictionary<string, string> dictUniqueChannelGeo = new Dictionary<string, string>();
            string strChannelGeo = null;
            float result;
            DataRow dr = null;
            string msginvalid = "is invalid";
            for (int count = 0; count < dt.Rows.Count; count++)
            {
                isAnyError = false;
                dr = dt.Rows[count];
                if (dr[0].ToString().Trim() != "ALL_PRODUCT")
                {
                    if (!string.IsNullOrEmpty(dr[0].ToString()))
                    {
                        isKeyPresent = dictProductID.ContainsKey(dr["PRODUCTIDVAL"].ToString());
                        if (!isKeyPresent)
                        {
                            drError = dtErrorDetails.NewRow();
                            drError[0] = count + 1;
                            drError[0] = count + 1;
                            drError[1] = "PRODUCT_ID";
                            drError[2] = msginvalid + " PRODUCT_ID";
                            dtErrorDetails.Rows.Add(drError);
                            isAnyError = true;
                        }
                    }
                    else
                    {
                        drError = dtErrorDetails.NewRow();
                        drError[0] = count + 1;
                        drError[1] = "PRODUCT_ID";
                        drError[2] = msginvalid + " PRODUCT_ID";
                        dtErrorDetails.Rows.Add(drError);
                        isAnyError = true;
                    }

                }
                if (dr[1].ToString().Trim() != "ALL_CHANNEL")
                {
                    if (!string.IsNullOrEmpty(dr[1].ToString()))
                    {
                        isKeyPresent = dictChannel.ContainsKey(dr["CHANNELIDVAL"].ToString());
                        if (!isKeyPresent)
                        {
                            drError = dtErrorDetails.NewRow();
                            drError[0] = count + 1;
                            drError[1] = "CHANNEL_ID";
                            drError[2] = msginvalid + " CHANNEL_ID";
                            dtErrorDetails.Rows.Add(drError);
                            isAnyError = true;
                        }

                    }
                    else
                    {
                        drError = dtErrorDetails.NewRow();
                        drError[0] = count + 1;
                        drError[1] = "CHANNEL_ID";
                        drError[2] = msginvalid + " CHANNEL_ID";
                        dtErrorDetails.Rows.Add(drError);
                        isAnyError = true;
                    }

                }
                if (dr[2].ToString().Trim() != "ALL_GEO")
                {
                    if (!string.IsNullOrEmpty(dr[2].ToString()))
                    {
                        isKeyPresent = dictGeography.ContainsKey(dr["GEOIDVAL"].ToString());
                        if (!isKeyPresent)
                        {
                            drError = dtErrorDetails.NewRow();
                            drError[0] = count + 1;
                            drError[1] = "GEOGRAPHY_ID";
                            drError[2] = msginvalid + " GEOGRAPHY_ID";
                            dtErrorDetails.Rows.Add(drError);
                            isAnyError = true;
                        }
                    }
                    else
                    {
                        drError = dtErrorDetails.NewRow();
                        drError[0] = count + 1;
                        drError[1] = "GEOGRAPHY_ID";
                        drError[2] = msginvalid + " GEOGRAPHY_ID";
                        dtErrorDetails.Rows.Add(drError);
                        isAnyError = true;
                    }
                }
                if (!string.IsNullOrEmpty(dr[31].ToString()))
                {
                    float res;
                    if (float.TryParse(dr[31].ToString(), out res))
                    {

                    }
                    else
                    {
                        drError = dtErrorDetails.NewRow();
                        drError[0] = count + 1;
                        drError[1] = "REST OF HORIZON";
                        drError[2] = msginvalid + " REST OF HORIZON";
                        dtErrorDetails.Rows.Add(drError);
                        isAnyError = true;
                    }
                }
                else
                {

                    drError = dtErrorDetails.NewRow();
                    drError[0] = count + 1;
                    drError[1] = "REST OF HORIZON";
                    drError[2] = msginvalid + " REST OF HORIZON";
                    dtErrorDetails.Rows.Add(drError);
                    isAnyError = true;
                }

                if (!string.IsNullOrEmpty(dr[3].ToString()))
                {
                    isKeyPresent = dictSSC.ContainsKey(dr[3].ToString());
                    if (!isKeyPresent)
                    {
                        drError = dtErrorDetails.NewRow();
                        drError[0] = count + 1;
                        drError[1] = "SSC";
                        drError[2] = msginvalid + " SSC";
                        dtErrorDetails.Rows.Add(drError);
                        isAnyError = true;
                    }
                }
                else
                {
                    drError = dtErrorDetails.NewRow();
                    drError[0] = count + 1;
                    drError[1] = "SSC";
                    drError[2] = msginvalid + " SSC";
                    dtErrorDetails.Rows.Add(drError);
                    isAnyError = true;
                }


                strChannelGeo = dt.Rows[count]["ChannelGeo"].ToString();
                isItemExist = dictUniqueChannelGeo.ContainsKey(strChannelGeo);
                if (!isItemExist)
                {
                    dictUniqueChannelGeo.Add(strChannelGeo, strChannelGeo);
                    for (int wkCount = 0; wkCount <= 26; wkCount++)
                    {
                        wk = "WK" + wkCount.ToString();

                        if (float.TryParse(dt.Rows[count][wk].ToString(), out result))
                        {
                            if (result < 0)
                            {
                                drError = dtErrorDetails.NewRow();
                                drError[0] = count + 1;
                                drError[1] = wk;
                                drError[2] = msginvalid + wk;
                                dtErrorDetails.Rows.Add(drError);
                                isAnyError = true;
                            }
                            string str = "ChannelGeo='" + dt.Rows[count]["ChannelGeo"].ToString() + "'";
                            double strSum = 0;

                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                if (dt.Rows[j]["ChannelGeo"].ToString() == dt.Rows[count]["ChannelGeo"].ToString())
                                {
                                    if (float.TryParse(dt.Rows[j][wk].ToString(), out result))
                                        strSum = strSum + Convert.ToDouble(result);
                                    else
                                    {
                                        drError = dtErrorDetails.NewRow();
                                        drError[0] = count + 1;
                                        drError[1] = wk;
                                        drError[2] = msginvalid + wk;
                                        dtErrorDetails.Rows.Add(drError);
                                        isAnyError = true;
                                    }
                                }

                            }




                            if (float.TryParse(strSum.ToString(), out result))
                            {
                                if (result != 100)
                                {
                                    drError = dtErrorDetails.NewRow();
                                    drError[0] = count + 1;
                                    drError[1] = wk;
                                    drError[2] = msginvalid + " sum of " + wk;
                                    dtErrorDetails.Rows.Add(drError);
                                    isAnyError = true;
                                }
                            }
                            else
                            {
                                drError = dtErrorDetails.NewRow();
                                drError[0] = count + 1;
                                drError[1] = wk;
                                drError[2] = msginvalid + " sum of " + wk;
                                dtErrorDetails.Rows.Add(drError);
                                isAnyError = true;
                            }

                        }


                        else
                        {
                            drError = dtErrorDetails.NewRow();
                            drError[0] = count + 1;
                            drError[1] = wk;
                            drError[2] = msginvalid + wk;
                            dtErrorDetails.Rows.Add(drError);
                            isAnyError = true;
                        }
                    }
                }


                if (isAnyError)
                {
                    intErrorRow = intErrorRow + 1;
                }
                i = i + 1;
            }

            // dtErrorDetails. ows[0].Columns["Totalrow"]
            //dtErrorDetails.Rows[0]["Totalrow"] = dt.Rows.Count;
            //dtErrorDetails.Rows[0]["Totalfailed"] = intErrorRow;
            //dtErrorDetails.Rows[0]["CorrectRecords "] = dt.Rows.Count - intErrorRow;

          return dtErrorDetails;
        }





    }
}
