﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Dell.RP.API.Controllers.HelperClasses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Net;
using Dell.RP.API.Dell.RP.Common.Models.CommodityCodeListDetails;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.CommodityCodeList;

namespace Dell.RP.API.Controllers.CommodityCodeList
{
    [Route("api/[controller]")]
    [Authorize(Policy = "AllUsers")]
    public class CommodityCodeListController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ICommodityCodeList iCommodityCodeListService;

        #region Constructor & Local variables
        private string userName = "";

        public CommodityCodeListController(ICommodityCodeList iCommodityCodeListService, IHttpContextAccessor httpContextAccessor, IClaimsTransformation d, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            this.iCommodityCodeListService = iCommodityCodeListService;
            var s = UserNameValidation.GetUser(httpContextAccessor);
            if (s.Result != null)
            {
                this.userName = s.Result;
            }
        }
        #endregion

        [HttpPost]
        [Route("GetFilterViewResult")]
        public async Task<ObjectResult> GetFilterViewResult([FromBody] CommodityCodeListDetails commodityCodeList)
        {
            ObjectResult response;
            iCommodityCodeListService.UserID = this.userName;

            try
            {
                var gridData = await iCommodityCodeListService.GetFilterViewResult(commodityCodeList);
                if (gridData != null && gridData.Count > 0)
                {
                    response = new ObjectResult(gridData);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpPost]
        [Route("UpdateCommodityCodeListDetails")]
        public async Task<ObjectResult> UpdateCommodityCodeListDetails([FromBody] List<CommodityCodeListDetails> updateDetails)
        {
            ObjectResult response;

            iCommodityCodeListService.UserID = this.userName;
            try
            {
                var fileNameList = await iCommodityCodeListService.UpdateCommodityCodeListDetails(updateDetails);
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }
    }
}