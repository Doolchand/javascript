﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dell.RP.BussinessServices.IServiceInterceptor.ErrorLogging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using System.Net;
//using System.Web.Script.Serialization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Microsoft.Net.Http.Headers;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Dell.RP.API.Controllers.ProductHeirarchy
{
    [Route("api/[controller]")]
    [Authorize(Policy = "AllUsers")]
    public class ErrorLoggingController : Controller
    {
        #region Constructor & Local variables
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IErrorLoggingService iErrorLoggingService;
        private string userName = "";
        public ErrorLoggingController(IErrorLoggingService iErrorLoggingService, IHttpContextAccessor httpContextAccessor, IClaimsTransformation d, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            this.iErrorLoggingService = iErrorLoggingService;
            var s = GetUser(httpContextAccessor);
            if (s.Result != null)
            {
                this.userName = s.Result;
            }
        }

        public async Task<string> GetUser(IHttpContextAccessor httpContextAccessor)
        {
            return await Task.Run(() =>
            {
                return httpContextAccessor.HttpContext.User?.Identity?.Name;
            });
        }
        #endregion

       

        [HttpGet]
        [Route("GetErrorList")]
        public async Task<JsonResult> GetErrorList(string procName)
        {
            JsonResult response;
            iErrorLoggingService.UserID = this.userName;
            try
            {
                var fileNameList = await iErrorLoggingService.GetErrorList(procName);
                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new JsonResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new JsonResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new JsonResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

    }
}
