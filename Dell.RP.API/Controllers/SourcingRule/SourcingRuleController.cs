﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Dell.RP.API.Controllers.HelperClasses;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.SourcingRule1;
using Dell.RP.API.Dell.RP.Common.Models;
using Dell.RP.API.Dell.RP.Common.Models.SourcingRule;
using Dell.RP.API.Dell.RP.Common.Models.SourcingRule1;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Dell.RP.API.Controllers.SourcingRule
{
    [Produces("application/json")]
    [Route("api/SourcingRule")]
    [Authorize(Policy = "AllUsers")]
    public class SourcingRuleController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ISourcingRuleService _iSourcingRuleService;


        #region Constructor & Local variables
        private string userName = "";

        public SourcingRuleController(ISourcingRuleService iSourcingRuleService, IHttpContextAccessor httpContextAccessor, IClaimsTransformation d, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            this._iSourcingRuleService = iSourcingRuleService;
            var s = UserNameValidation.GetUser(httpContextAccessor);
            if (s.Result != null)
            {
                this.userName = s.Result;
            }
        }

        #endregion


        [HttpGet]
        [Route("GetSites")]
        public async Task<ObjectResult> GetSites()
        {
            ObjectResult response;
            _iSourcingRuleService.UserID = this.userName;
            try
            {
                //string key = "GEO";
                var fileNameList = await _iSourcingRuleService.GetSites();

                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetSourcingRuleLineItems")]
        public async Task<ObjectResult> GetSourcingRuleLineItems(string startChannelId, string startProductId)
        {
            ObjectResult response;
            SourcingRuleFilter sourcingRuleFilter = new SourcingRuleFilter();
            response = null;
            _iSourcingRuleService.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var singleRuleLineItems = await _iSourcingRuleService.GetSourcingRuleLineItems(startChannelId, startProductId);

                singleRuleLineItems = (
                    from abc in singleRuleLineItems
                    orderby abc.Value.ChannelId, abc.Value.ProductID, abc.Value.FromDate, abc.Value.ToDate, abc.Value.KitSite, abc.Value.BoxSite, abc.Value.MergeSite
                    select abc).ToDictionary<KeyValuePair<string, SourcingRuleLineItem>, string, SourcingRuleLineItem>((KeyValuePair<string, SourcingRuleLineItem> n) => n.Key, (KeyValuePair<string, SourcingRuleLineItem> n) => n.Value);
                var sourcingRuleItemList = singleRuleLineItems.Values.ToList<SourcingRuleLineItem>();
              //  List<SourcingRuleLineItem> sourcingRuleLineItemsProcessed = GetProcessedSourcingRuleItemData(sourcingRuleItemList);

                //List<SourcingRuleGrid> sourcingRuleGrids = new List<SourcingRuleGrid>();
                //foreach (var item in fileNameList)
                //{
                //    sourcingRuleGrids.Add(new SourcingRuleGrid
                //    {
                //        Channel = item.Value.Channel,
                //        Product = item.Value.Product,
                //        FromDateStr = item.Value.FromDateStr,
                //        ToDateStr = item.Value.ToDateStr,
                //        KitSite = item.Value.KitSite,
                //        BoxSite = item.Value.BoxSite,
                //        BoxSplit = item.Value.BoxSplit.ToString(),
                //        MergeSite = item.Value.MergeSite,
                //        MergeSplit = item.Value.MergeSplit.ToString(),
                //        SourcingRuleKey = item.Value.SourcingRuleKey,
                //        SysLastModifiedby = item.Value.SysLastModifiedby,
                //        SysLastModifiedDate = item.Value.SysLastModifiedDate
                //    });
                //}

                if (sourcingRuleItemList != null)
                {
                    response = new ObjectResult(sourcingRuleItemList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetSingleRuleLineItems")]
        public async Task<ObjectResult> GetSingleRuleLineItems(string startChannelId, string startProductId)
        {
            ObjectResult response;
            SourcingRuleFilter sourcingRuleFilter = new SourcingRuleFilter();
            response = null;
            _iSourcingRuleService.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var singleRuleLineItems = await _iSourcingRuleService.GetSingleRuleLineItems(startChannelId, startProductId);

                singleRuleLineItems = (
                    from abc in singleRuleLineItems
                    orderby abc.Value.ChannelId, abc.Value.ProductID, abc.Value.FromDate, abc.Value.ToDate, abc.Value.KitSite, abc.Value.BoxSite, abc.Value.MergeSite
                    select abc).ToDictionary<KeyValuePair<string, SourcingRuleLineItem>, string, SourcingRuleLineItem>((KeyValuePair<string, SourcingRuleLineItem> n) => n.Key, (KeyValuePair<string, SourcingRuleLineItem> n) => n.Value);
                var sourcingRuleItemList = singleRuleLineItems.Values.ToList<SourcingRuleLineItem>();
                List<SourcingRuleLineItem> sourcingRuleLineItemsProcessed = GetProcessedSourcingRuleItemData(sourcingRuleItemList);


                //List<SourcingRuleGrid> sourcingRuleGrids = new List<SourcingRuleGrid>();
                //foreach (var item in fileNameList)
                //{
                //    sourcingRuleGrids.Add(new SourcingRuleGrid
                //    {
                //        Channel = item.Value.Channel,
                //        Product = item.Value.Product,
                //        FromDateStr = item.Value.FromDateStr,
                //        ToDateStr = item.Value.ToDateStr,
                //        KitSite = item.Value.KitSite,
                //        BoxSite = item.Value.BoxSite,
                //        BoxSplit = item.Value.BoxSplit.ToString(),
                //        MergeSite = item.Value.MergeSite,
                //        MergeSplit = item.Value.MergeSplit.ToString(),
                //        SourcingRuleKey = item.Value.SourcingRuleKey,
                //        SysLastModifiedby = item.Value.SysLastModifiedby,
                //        SysLastModifiedDate = item.Value.SysLastModifiedDate
                //    });
                //}

                if (sourcingRuleLineItemsProcessed != null)
                {
                    response = new ObjectResult(sourcingRuleLineItemsProcessed);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpPost]
        [Route("SaveSourcingRule")]
        public ObjectResult SaveSourcingRule([FromBody] List<SourcingRuleLineItem> sourcingRuleGrid)
        {
            ObjectResult response;

            _iSourcingRuleService.UserID = this.userName;
            try
            {
                var singleRuleLineItems = from abc in sourcingRuleGrid
                                           orderby abc.ChannelId, abc.ProductID, abc.FromDate, abc.ToDate, abc.KitSite, abc.BoxSite, abc.MergeSite
                                           select abc;              
                var fileNameList = SaveSourcingRule1(GetProcessedSourcingRuleItemData(singleRuleLineItems.ToList()));
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpPost]
        [Route("DeleteSourcingRuleCollection")]
        public async Task<ObjectResult> DeleteSourcingRuleCollection(string productId, string channelId)
        {
            ObjectResult response;

            _iSourcingRuleService.UserID = this.userName;
            try
            {
                var fileNameList = await _iSourcingRuleService.DeleteSourcingRuleCollection(productId, channelId);
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpPost]
        [Route("GetCopiedProccessData")]
        public ObjectResult GetCopiedProccessData([FromBody] List<SourcingRuleLineItem> singleRuleLineItems)
        {
            ObjectResult response;

            _iSourcingRuleService.UserID = this.userName;
            try
            {
                var singleRuleLineItemsa =  from abc in singleRuleLineItems
                                            orderby abc.ChannelId, abc.ProductID, abc.FromDate, abc.ToDate, abc.KitSite, abc.BoxSite, abc.MergeSite
                                            select abc;
                var fileNameList = GetProcessedSourcingRuleItemData(singleRuleLineItemsa.ToList());
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        public List<SourcingRuleLineItem> GetProcessedSourcingRuleItemData(List<SourcingRuleLineItem> rules)
        {
            string ChannelProdCombination = string.Empty;
            string ChannelProdDateCombination = string.Empty;
            string ChannelProdDateKitCombination = string.Empty;
            string ChannelProdDateKitBoxCombination = string.Empty;
            string commandArgKey = string.Empty;
            foreach (var item in rules)
            {
                //item.CopyButtonVisible = true;
                //item.DeleteButtonVisible = true;
                //item.EditButtonVisible = true;

                string ChannelProd = item.ChannelId + "_" + item.ProductID;
                string ChannelProdDate = item.ChannelId + "_" + item.ProductID + "_" + item.FromDateCalId + "_" + item.ToDateCalId;
                string ChannelProdDateKit = item.ChannelId + "_" + item.ProductID + "_" + item.FromDateCalId + "_" + item.ToDateCalId + "_" + item.KitSite;
                string ChannelProdDateKitBox = item.ChannelId + "_" + item.ProductID + "_" + item.FromDateCalId + "_" + item.ToDateCalId + "_" + item.KitSite + "_" + item.BoxSite;
                commandArgKey = item.SourcingRuleKey;

                if (ChannelProdDate.Equals(ChannelProdDateCombination))
                {
                    item.ToDateVisible = false;
                    item.FromDateVisible = false;
                    item.KitSiteVisible = true;
                    item.BoxSiteVisible = true;
                    item.BoxSplitVisible = true;
                    item.MergeSiteVisible = true;
                    item.MergeSplitVisible = true;

                    if (ChannelProdDateKit.Equals(ChannelProdDateKitCombination)) // Equivalent as Add boxing
                    {
                        item.KitSiteVisible = false;                        

                        if (ChannelProdDateKitBox.Equals(ChannelProdDateKitBoxCombination)) //Equivalent as add merging
                        {
                            item.BoxSiteVisible = false;
                            item.BoxSplitVisible = false;
                        }
                    }
                }
               
                ChannelProdCombination = ChannelProd;
                ChannelProdDateCombination = ChannelProdDate;
                ChannelProdDateKitCombination = ChannelProdDateKit;
                ChannelProdDateKitBoxCombination = ChannelProdDateKitBox;
            }
            return rules;
        }

        
        #region Not Used but need to check



        [HttpGet]
        [Route("GetProductTreeData")]
        public async Task<ObjectResult> GetProductTreeData(string strString)
        {
            ObjectResult response;
            response = null;
            _iSourcingRuleService.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = await _iSourcingRuleService.GetProductTreeData(strString.ToUpper());

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetChannelProducts")]
        public async Task<ObjectResult> GetChannelProducts()
        {
            ObjectResult response;
            _iSourcingRuleService.UserID = this.userName;
            try
            {
                //string key = "GEO";
                var fileNameList = await _iSourcingRuleService.GetChannelProducts();

                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetChannelGeo")]
        public async Task<ObjectResult> GetChannelGeo()
        {
            ObjectResult response;
            _iSourcingRuleService.UserID = this.userName;
            try
            {
                //string key = "GEO";
                var fileNameList = await _iSourcingRuleService.GetChannelGeo();

                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetSourcingRuleDetails")]
        public async Task<ObjectResult> GetSourcingRuleDetails()//[FromBody] SourcingRuleFilter sourcingRuleFilter)
        {
            ObjectResult response;
            SourcingRuleFilter sourcingRuleFilter = new SourcingRuleFilter();
            response = null;
            _iSourcingRuleService.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = await _iSourcingRuleService.GetSourcingRuleDetailsAsync(sourcingRuleFilter);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }



        //private object ConvertToSourcingRuleGrid(Dictionary<string, SourcingRuleLineItem> dictionary)
        //{
        //   SourcingRuleGrid sourcingGrid
        //}

        [HttpGet]
        [Route("GetAllSourcingRules")]
        public async Task<ObjectResult> GetAllSourcingRules()
        {
            ObjectResult response;
            response = null;
            _iSourcingRuleService.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = await _iSourcingRuleService.GetAllSourcingRules();

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }




        [HttpGet]
        [Route("GetSourcingRules")]
        public async Task<ObjectResult> GetSourcingRules(string parentProductSearchFilter, string parentChannelSearchFilter)
        {
            ObjectResult response;
            response = null;
            _iSourcingRuleService.UserID = this.userName;
            try
            {

                //string key = "GEO";
                var fileNameList = await _iSourcingRuleService.GetSourcingRules(parentProductSearchFilter, parentChannelSearchFilter);

                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }

            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }
                     
        protected async Task<SaveMessage> SaveSourcingRule1(List<SourcingRuleLineItem> sourcingRuleList)
        {
            string hfProduct = sourcingRuleList[0].ProductID;// Product.Split('(')[1].Split(')')[0];
            string hfChannel = sourcingRuleList[0].ChannelId;// Channel.Split('(')[1].Split(')')[0];
            string hfProductText = sourcingRuleList[0].Product;
            string hfChannelText = sourcingRuleList[0].Channel;
            SaveMessage saveMessage = new SaveMessage();
            int rowIndex;
            object[] selectedValue;
            DateTime selectedDate;
            bool flag;
            bool dayOfWeek;
            bool dayOfWeek1;
            object shortDateString;
            object obj;
            object shortDateString1;
            object obj1;
            object shortDateString2;
            object obj2;
            object shortDateString3;
            object obj3;
            object shortDateString4;
            object obj4;
            bool flag1;
            object shortDateString5;
            object obj5;
            object shortDateString6;
            object obj6;

            try
            {
                //Retrieve Click
                // Dictionary<string, SourcingRuleLineItem> sourcingRuleList = await _iSourcingRuleService.GetSingleRuleLineItems(hfChannel, hfProduct);
                if (sourcingRuleList.Count > 0)
                {
                    bool flag2 = true;
                    int num = 0;
                    int num1 = 0;
                    int num2 = 0;
                    string empty = string.Empty;
                    SourcingRuleSaveClass sourcingRuleSaveClass = new SourcingRuleSaveClass();
                    SourcingLineItemType sourcingLineItemType = SourcingLineItemType.KitSite;
                    if (string.IsNullOrEmpty(hfProduct))
                    {
                        flag2 = false;
                        empty = string.Concat(empty, string.Format(string.Concat("Please select product."), new object[0]));
                    }
                    if (string.IsNullOrEmpty(hfChannel))
                    {
                        flag2 = false;
                        empty = string.Concat(empty, string.Format(string.Concat("\n Please select channel."), new object[0]));
                    }

                    //    //Below Grid Click


                    //If edit mode is copy so error message --Already exist
                    //else 
                    //if (this.ViewState["Mode"] != null)
                    //{
                    //    str = this.ViewState["Mode"].ToString();
                    //}
                    //if ((str != "Copy" ? false : dictionary.Count > 0))  //ie.  edit or Add == False
                    //{
                    //    flag2 = false;
                    //    //empty = string.Concat(empty, string.Format(string.Concat("<BR>", ConfigurationManager.AppSettings["msgAlreadyExists"]), new object[0]));
                    //}
                    string str1 = hfProductText.Trim();
                    if (str1.IndexOf("(") >= 0)
                    {
                        str1 = str1.Substring(0, str1.IndexOf("(")).Trim();
                    }
                    string str2 = hfChannelText.Trim();
                    if (str2.IndexOf("(") >= 0)
                    {
                        str2 = str2.Substring(0, str2.IndexOf("(")).Trim();
                    }
                    DateTime date = new DateTime();
                    DateTime toDate = new DateTime();
                    bool flag3 = false;
                    bool flag4 = false;
                    bool flag5 = false;
                    ArrayList arrayLists = new ArrayList();
                    ArrayList arrayLists1 = new ArrayList();
                    ArrayList arrayLists2 = new ArrayList();
                    if (flag2)
                    {
                        //int index = 0;
                        for (int i = 0; i < sourcingRuleList.Count; i++)
                        {
                            sourcingLineItemType = SourcingLineItemType.KitSite;


                            string dateTimeControlStr = sourcingRuleList[i].FromDateStr;
                            string dateTimeControl1Str = sourcingRuleList[i].ToDateStr;

                            DateTime dateTimeControl;
                            DateTime.TryParse(sourcingRuleList[i].FromDateStr, out dateTimeControl);

                            DateTime dateTimeControl1;
                            DateTime.TryParse(sourcingRuleList[i].ToDateStr, out dateTimeControl1);

                            bool dateTimeControlVisible = sourcingRuleList[i].FromDateVisible;
                            bool dateTimeControl1Visible = sourcingRuleList[i].ToDateVisible;

                            bool dropDownListVisible = sourcingRuleList[i].KitSiteVisible;
                            string dropDownList = sourcingRuleList[i].KitSite;

                            bool selectedValue1Visible = sourcingRuleList[i].BoxSiteVisible;
                            string selectedValue1 = sourcingRuleList[i].BoxSite;

                            string textBox = sourcingRuleList[i].BoxSplit.ToString();
                            bool textBoxVisible = sourcingRuleList[i].BoxSplitVisible;

                            bool dropDownList1Visible = sourcingRuleList[i].MergeSiteVisible;
                            string dropDownList1 = sourcingRuleList[i].MergeSite;

                            bool textBox1Visible = sourcingRuleList[i].MergeSplitVisible;
                            string textBox1 = sourcingRuleList[i].MergeSplit.ToString();

                            if (string.IsNullOrEmpty(dropDownList))
                            {
                                flag2 = false;
                                empty = empty + "\n Please select Kitting site in row " + (i + 1) + ".";
                                break;
                            }
                            else if (string.IsNullOrEmpty(selectedValue1))
                            {
                                flag2 = false;
                                empty = empty + "\n Please select Boxing site in row " + (i + 1) + ".";
                                break;
                            }
                            else if (!string.IsNullOrEmpty(dropDownList1))
                            {
                                int num3 = 0;
                                float single = 0f;
                                if (textBoxVisible)
                                {
                                    if (float.TryParse(textBox.Trim(), out single))
                                    {
                                        int num4 = (int)single;
                                        num3 = num4;
                                        num2 = num4;
                                        if (!(num3 <= 0 ? false : single <= (float)num3))
                                        {
                                            flag2 = false;
                                            empty += "\n Please enter positive integer Boxing split for KIT Site " + dropDownList + " .";
                                            break;
                                        }
                                        else if (num3 > 100)
                                        {
                                            flag2 = false;
                                            string str3 = "Boxing split percentage must not exceed 100.";
                                            selectedValue = new object[] { dropDownList, selectedValue1 };
                                            empty = string.Concat(empty, string.Format(str3, selectedValue));
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        flag2 = false;
                                        empty = string.Concat(empty, "<br> Invalid Boxing split entered for KIT Site " + dropDownList + ".");
                                        break;
                                    }
                                }

                                int num5 = 0;
                                if (float.TryParse(textBox1.Trim(), out single))
                                {
                                    num5 = (int)single;
                                    if (!(num5 <= 0 ? false : single <= (float)num5))
                                    {
                                        flag2 = false;
                                        string str4 = string.Concat("<br> Please enter positive integer Merge split for KIT-> Box Split Rule {0} -> {1}.");
                                        selectedValue = new object[] { dropDownList, selectedValue1 };
                                        empty = string.Concat(empty, string.Format(str4, selectedValue));
                                        break;
                                    }
                                    else if (num5 <= 100)
                                    {
                                        if (dateTimeControlVisible)
                                        {
                                            arrayLists.Clear();
                                            arrayLists1.Clear();
                                            arrayLists2.Clear();
                                            if (dateTimeControlStr == string.Empty)
                                            {
                                                if (!flag3)
                                                {
                                                    flag3 = true;
                                                }
                                                else
                                                {
                                                    flag2 = false;
                                                    string str5 = empty;
                                                    string str6 = "<br> Period of rule {0}->{1}->{2}->{3} overlaps period of another rule.";
                                                    selectedValue = new object[] { "", null, null, null };
                                                    object[] objArray = selectedValue;
                                                    if (dateTimeControl1Str == string.Empty)
                                                    {
                                                        obj6 = "";
                                                    }
                                                    else
                                                    {
                                                        selectedDate = dateTimeControl1;
                                                        obj6 = selectedDate.ToShortDateString();
                                                    }
                                                    objArray[1] = obj6;
                                                    selectedValue[2] = dropDownList;
                                                    selectedValue[3] = selectedValue1;
                                                    empty = string.Concat(str5, string.Format(str6, selectedValue));
                                                    break;
                                                }
                                            }
                                            if (dateTimeControl1Str == string.Empty)
                                            {
                                                if (!flag4)
                                                {
                                                    flag4 = true;
                                                }
                                                else
                                                {
                                                    flag2 = false;
                                                    string str7 = empty;
                                                    string str8 = "<br> Period of rule {0}->{1}->{2}->{3} overlaps period of another rule.";
                                                    selectedValue = new object[4];
                                                    object[] objArray1 = selectedValue;
                                                    if (dateTimeControlStr == string.Empty)
                                                    {
                                                        shortDateString6 = "";
                                                    }
                                                    else
                                                    {
                                                        selectedDate = dateTimeControl;
                                                        selectedDate = selectedDate.Date;
                                                        shortDateString6 = selectedDate.ToShortDateString();
                                                    }
                                                    objArray1[0] = shortDateString6;
                                                    selectedValue[1] = "";
                                                    selectedValue[2] = dropDownList;
                                                    selectedValue[3] = selectedValue1;
                                                    empty = string.Concat(str7, string.Format(str8, selectedValue));
                                                    break;
                                                }
                                            }
                                        }

                                        if (dropDownListVisible)
                                        {
                                            if (!arrayLists.Contains(dropDownList))
                                            {
                                                arrayLists.Add(dropDownList);
                                                arrayLists1.Clear();
                                                arrayLists2.Clear();
                                            }
                                            else
                                            {
                                                flag5 = true;
                                                flag2 = false;
                                                string str9 = string.Concat("<br> Duplicate KIT site {0} entered.");
                                                selectedValue = new object[] { dropDownList };
                                                empty = string.Concat(empty, string.Format(str9, selectedValue));
                                                break;
                                            }
                                        }

                                        if (selectedValue1Visible)
                                        {
                                            if (!arrayLists1.Contains(selectedValue1))
                                            {
                                                arrayLists1.Add(selectedValue1);
                                                arrayLists2.Clear();
                                            }
                                            else
                                            {
                                                flag5 = true;
                                                flag2 = false;
                                                string str10 = string.Concat("<br> Duplicate Box site {1} entered for KIT site {0}.");
                                                selectedValue = new object[] { dropDownList, selectedValue1 };
                                                empty = string.Concat(empty, string.Format(str10, selectedValue));
                                                break;
                                            }

                                        }
                                        if (dropDownList1Visible)
                                        {
                                            if (!arrayLists2.Contains(dropDownList1))
                                            {
                                                arrayLists2.Add(dropDownList1);
                                            }
                                            else
                                            {
                                                flag5 = true;
                                                flag2 = false;
                                                string str11 = string.Concat("Duplicate Merge site {2} entered for KIT -> Box Split Rule {0}-> {1}.");
                                                selectedValue = new object[] { dropDownList, selectedValue1, dropDownList1 };
                                                empty = string.Concat(empty, string.Format(str11, selectedValue));
                                                break;
                                            }
                                        }
                                        if (i > 0)
                                        {
                                            DateTime dateTimeControl2;
                                            DateTime.TryParse(sourcingRuleList[i - 1].FromDateStr, out dateTimeControl2);

                                            DateTime dateTimeControl3;
                                            DateTime.TryParse(sourcingRuleList[i - 1].ToDateStr, out dateTimeControl3);

                                            //DateTime dateTimeControl2 = DateTime.Parse(sourcingRuleList[i - 1].FromDateStr);//[row.RowIndex - 1].FindControl("dtFromDate");
                                            //DateTime dateTimeControl3 = DateTime.Parse(sourcingRuleList[i - 1].ToDateStr);
                                            string dropDownList2 = sourcingRuleList[i - 1].KitSite;// (DropDownList)this.gvSourcingRule.Rows[row.RowIndex - 1].FindControl("ddlKitting");
                                            string dropDownList3 = sourcingRuleList[i - 1].BoxSite;//(DropDownList)this.gvSourcingRule.Rows[row.RowIndex - 1].FindControl("ddlBoxing");
                                            string dropDownList4 = sourcingRuleList[i - 1].MergeSite;//(DropDownList)this.gvSourcingRule.Rows[row.RowIndex - 1].FindControl("ddlMerging");
                                            if (!dropDownListVisible)
                                            {
                                                dropDownList = dropDownList2;
                                            }
                                            if (!selectedValue1Visible)
                                            {
                                                selectedValue1 = dropDownList3;
                                            }
                                            if (!dropDownList1Visible)
                                            {
                                                dropDownList1 = dropDownList4;
                                            }
                                            if (dateTimeControlVisible)
                                            {
                                                if (num < 100)
                                                {
                                                    flag2 = false;
                                                    empty = string.Concat(empty, "Sum of box split percentages entered for KIT Site " + dropDownList2 + " is less than 100.");
                                                    break;
                                                }
                                                else if (num1 >= 100)
                                                {
                                                    num = 0;
                                                    num1 = 0;
                                                    sourcingLineItemType = SourcingLineItemType.KitSite;
                                                }
                                                else
                                                {
                                                    flag2 = false;
                                                    string str12 = string.Concat("Sum of merge split percentages entered for KIT-> Box Split Rule {0}-> {1} is less than 100.");
                                                    selectedValue = new object[] { dropDownList2, dropDownList3 };
                                                    empty = string.Concat(empty, string.Format(str12, selectedValue));
                                                    break;
                                                }
                                            }
                                            else if (dropDownListVisible)
                                            {
                                                if (num < 100)
                                                {
                                                    flag2 = false;
                                                    empty = string.Concat(empty, "<br> Sum of box split percentages entered for KIT Site " + dropDownList2 + " is less than 100.");
                                                    break;
                                                }
                                                else if (num1 >= 100)
                                                {
                                                    num = 0;
                                                    num1 = 0;
                                                    sourcingLineItemType = SourcingLineItemType.KitSite;
                                                }
                                                else
                                                {
                                                    flag2 = false;
                                                    string str13 = string.Concat("Sum of merge split percentages entered for KIT->Box Split Rule {0}->{1} is less than 100.");
                                                    selectedValue = new object[] { dropDownList2, dropDownList3 };
                                                    empty = string.Concat(empty, string.Format(str13, selectedValue));
                                                    break;
                                                }
                                            }

                                            else if (!selectedValue1Visible)
                                            {
                                                sourcingLineItemType = SourcingLineItemType.MergeSite;
                                            }
                                            else if (num1 >= 100)
                                            {
                                                num1 = 0;
                                                sourcingLineItemType = SourcingLineItemType.BoxSite;
                                            }
                                            else
                                            {
                                                flag2 = false;
                                                string str14 = string.Concat("Sum of merge split percentages entered for KIT->Box Split Rule {0}->{1} is less than 100.");
                                                selectedValue = new object[] { dropDownList2, dropDownList3 };
                                                empty = string.Concat(empty, string.Format(str14, selectedValue));
                                                break;
                                            }
                                        }
                                        if (textBoxVisible)
                                        {
                                            num += num3;
                                        }
                                        if (num <= 100)
                                        {
                                            if (textBox1Visible)
                                            {
                                                num1 += num5;
                                            }
                                            if (num1 <= 100)
                                            {
                                                //Here Sandeep Change the logic since we are date string rather date control 
                                                if (dateTimeControlStr != string.Empty)
                                                {
                                                    selectedDate = dateTimeControl;
                                                    date = selectedDate.Date;

                                                    if (string.IsNullOrEmpty(dateTimeControl1Str))
                                                    {
                                                        toDate = new DateTime();
                                                    }
                                                    else
                                                    {
                                                        selectedDate = dateTimeControl1;
                                                        toDate = selectedDate.Date;
                                                    }
                                                }
                                                else
                                                {
                                                    date = new DateTime();
                                                }
                                                SourcingSaveLineItem sourcingSaveLineItem = new SourcingSaveLineItem(hfProduct, str1, hfChannel, str2)
                                                {
                                                    FromDate = date,
                                                    ToDate = toDate,
                                                    KitSite = dropDownList,
                                                    BoxSite = selectedValue1,
                                                    MergeSite = dropDownList1,
                                                    BoxSplit = num2,
                                                    MergeSplit = num5,
                                                    ItemType = sourcingLineItemType
                                                };
                                                sourcingRuleSaveClass.ItemsList.Add(sourcingSaveLineItem);
                                            }
                                            else
                                            {
                                                flag2 = false;
                                                string str15 = string.Concat("<br> Sum of merge split percentages entered for KIT->Box Split Rule {0}->{1} is more than 100.");
                                                selectedValue = new object[] { dropDownList, selectedValue1 };
                                                empty = string.Concat(empty, string.Format(str15, selectedValue));
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            flag2 = false;
                                            empty = string.Concat(empty, string.Format(string.Concat("Sum of box split percentages entered for KIT Site " + dropDownList + " is more than 100.")));
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        flag2 = false;
                                        string str16 = string.Concat("Merge split percentage must not exceed 100.");
                                        selectedValue = new object[] { dropDownList, selectedValue1 };
                                        empty = string.Concat(empty, string.Format(str16, selectedValue));
                                        break;
                                    }
                                }
                                else
                                {
                                    flag2 = false;
                                    string str17 = string.Concat("Invalid Merge split entered for  KIT->Box Split Rule {0}->{1}.");
                                    selectedValue = new object[] { dropDownList, selectedValue1 };
                                    empty = string.Concat(empty, string.Format(str17, selectedValue));
                                    break;
                                }
                            }
                            else
                            {
                                flag2 = false;
                                rowIndex = i + 1;
                                empty = string.Concat(empty, string.Format(string.Concat("Please select Merging site in row " + rowIndex.ToString() + ".")));
                                break;
                            }
                        }

                    }
                    if (flag2)
                    {
                        if (num < 100)
                        {
                            flag2 = false;
                            empty = string.Concat(empty, string.Format(string.Concat("<br> Sum of box split percentages entered for KIT Site " + sourcingRuleSaveClass.ItemsList[sourcingRuleSaveClass.ItemsList.Count - 1].KitSite)) + " is less than 100.");
                        }
                        if (num1 < 100)
                        {
                            flag2 = false;
                            string str18 = string.Concat("Sum of merge split percentages entered for KIT-> Box Split Rule {0} -> {1} is less than 100.");
                            selectedValue = new object[] { sourcingRuleSaveClass.ItemsList[sourcingRuleSaveClass.ItemsList.Count - 1].KitSite, sourcingRuleSaveClass.ItemsList[sourcingRuleSaveClass.ItemsList.Count - 1].BoxSite };
                            empty = string.Concat(empty, string.Format(str18, selectedValue));
                        }
                        sourcingRuleSaveClass.ItemsList = (
                            from abc in sourcingRuleSaveClass.ItemsList
                            orderby abc.ChannelId, abc.ProductID, abc.FromDate, abc.ToDate, abc.KitSite, abc.BoxSite, abc.MergeSite
                            select abc).ToList<SourcingSaveLineItem>();
                        date = new DateTime();
                        toDate = new DateTime();
                        flag3 = false;
                        flag4 = false;
                        int num6 = 0;
                        while (num6 < sourcingRuleSaveClass.ItemsList.Count)
                        {
                            SourcingSaveLineItem item = sourcingRuleSaveClass.ItemsList[num6];
                            if (num6 != 0)
                            {
                                if (num6 == sourcingRuleSaveClass.ItemsList.Count - 1)
                                {
                                    DateTime dateTime = item.ToDate;
                                    selectedDate = new DateTime();
                                    if (dateTime == selectedDate)
                                    {
                                        goto Label1;
                                    }
                                    DateTime toDate1 = item.ToDate;
                                    flag = 1 == 0;
                                    goto Label0;
                                }
                                Label1:
                                flag = true;
                                Label0:
                                if (!flag)
                                {
                                    flag2 = false;
                                    string str19 = empty;
                                    string str20 = string.Concat("To Date value of last rule {0}->{1}->{2}->{3} must be blank.");
                                    selectedValue = new object[4];
                                    object[] objArray2 = selectedValue;
                                    DateTime fromDate = item.FromDate;
                                    selectedDate = new DateTime();
                                    if (fromDate == selectedDate)
                                    {
                                        shortDateString4 = "";
                                    }
                                    else
                                    {
                                        selectedDate = item.FromDate;
                                        shortDateString4 = selectedDate.ToShortDateString();
                                    }
                                    objArray2[0] = shortDateString4;
                                    object[] objArray3 = selectedValue;
                                    DateTime dateTime1 = item.ToDate;
                                    selectedDate = new DateTime();
                                    if (dateTime1 == selectedDate)
                                    {
                                        obj4 = "";
                                    }
                                    else
                                    {
                                        selectedDate = item.ToDate;
                                        obj4 = selectedDate.ToShortDateString();
                                    }
                                    objArray3[1] = obj4;
                                    selectedValue[2] = item.KitSite;
                                    selectedValue[3] = item.BoxSite;
                                    empty = string.Concat(str19, string.Format(str20, selectedValue));
                                    break;
                                }
                            }
                            else
                            {
                                DateTime fromDate1 = item.FromDate;
                                selectedDate = new DateTime();
                                if (fromDate1 == selectedDate)
                                {
                                    flag1 = true;
                                }
                                else
                                {
                                    DateTime fromDate2 = item.FromDate;
                                    flag1 = 1 == 0;
                                }
                                if (!flag1)
                                {
                                    flag2 = false;
                                    string str21 = empty;
                                    string str22 = string.Concat("<br> From Date value of first rule {0}->{1}->{2}->{3} must be blank.");
                                    selectedValue = new object[4];
                                    object[] objArray4 = selectedValue;
                                    DateTime dateTime2 = item.FromDate;
                                    selectedDate = new DateTime();
                                    if (dateTime2 == selectedDate)
                                    {
                                        shortDateString5 = "";
                                    }
                                    else
                                    {
                                        selectedDate = item.FromDate;
                                        shortDateString5 = selectedDate.ToShortDateString();
                                    }
                                    objArray4[0] = shortDateString5;
                                    object[] objArray5 = selectedValue;
                                    DateTime toDate2 = item.ToDate;
                                    selectedDate = new DateTime();
                                    if (toDate2 == selectedDate)
                                    {
                                        obj5 = "";
                                    }
                                    else
                                    {
                                        selectedDate = item.ToDate;
                                        obj5 = selectedDate.ToShortDateString();
                                    }
                                    objArray5[1] = obj5;
                                    selectedValue[2] = item.KitSite;
                                    selectedValue[3] = item.BoxSite;
                                    empty = string.Concat(str21, string.Format(str22, selectedValue));
                                    break;
                                }
                            }
                            DateTime fromDate3 = item.FromDate;
                            selectedDate = new DateTime();
                            if ((fromDate3 == selectedDate ? true : !(item.FromDate == item.ToDate)))
                            {
                                DateTime dateTime3 = item.FromDate;
                                selectedDate = new DateTime();
                                if (dateTime3 == selectedDate)
                                {
                                    dayOfWeek = true;
                                }
                                else
                                {
                                    selectedDate = item.FromDate;
                                    dayOfWeek = selectedDate.DayOfWeek == DayOfWeek.Saturday;
                                }
                                if (!dayOfWeek)
                                {
                                    flag2 = false;
                                    string str23 = empty;
                                    string str24 = string.Concat("<br> From Date value of rule {0}->{1}->{2}->{3} must be saturday.");
                                    selectedValue = new object[4];
                                    object[] objArray6 = selectedValue;
                                    DateTime fromDate4 = item.FromDate;
                                    selectedDate = new DateTime();
                                    if (fromDate4 == selectedDate)
                                    {
                                        shortDateString2 = "";
                                    }
                                    else
                                    {
                                        selectedDate = item.FromDate;
                                        shortDateString2 = selectedDate.ToShortDateString();
                                    }
                                    objArray6[0] = shortDateString2;
                                    object[] objArray7 = selectedValue;
                                    DateTime toDate3 = item.ToDate;
                                    selectedDate = new DateTime();
                                    if (toDate3 == selectedDate)
                                    {
                                        obj2 = "";
                                    }
                                    else
                                    {
                                        selectedDate = item.ToDate;
                                        obj2 = selectedDate.ToShortDateString();
                                    }
                                    objArray7[1] = obj2;
                                    selectedValue[2] = item.KitSite;
                                    selectedValue[3] = item.BoxSite;
                                    empty = string.Concat(str23, string.Format(str24, selectedValue));
                                    break;
                                }
                                else if ((num6 <= 0 || !(date != item.FromDate) && !(toDate != item.ToDate) ? true : !(toDate != item.FromDate)))
                                {
                                    DateTime toDate4 = item.ToDate;
                                    selectedDate = new DateTime();
                                    if (toDate4 == selectedDate)
                                    {
                                        dayOfWeek1 = true;
                                    }
                                    else
                                    {
                                        selectedDate = item.ToDate;
                                        dayOfWeek1 = selectedDate.DayOfWeek == DayOfWeek.Saturday;
                                    }
                                    if (dayOfWeek1)
                                    {
                                        date = item.FromDate;
                                        toDate = item.ToDate;
                                        num6++;
                                    }
                                    else
                                    {
                                        flag2 = false;
                                        string str25 = empty;
                                        string str26 = string.Concat("<br> To Date value of rule {0}->{1}->{2}->{3} must be saturday.");
                                        selectedValue = new object[4];
                                        object[] objArray8 = selectedValue;
                                        DateTime dateTime4 = item.FromDate;
                                        selectedDate = new DateTime();
                                        if (dateTime4 == selectedDate)
                                        {
                                            shortDateString = "";
                                        }
                                        else
                                        {
                                            selectedDate = item.FromDate;
                                            shortDateString = selectedDate.ToShortDateString();
                                        }
                                        objArray8[0] = shortDateString;
                                        object[] objArray9 = selectedValue;
                                        DateTime toDate5 = item.ToDate;
                                        selectedDate = new DateTime();
                                        if (toDate5 == selectedDate)
                                        {
                                            obj = "";
                                        }
                                        else
                                        {
                                            selectedDate = item.ToDate;
                                            obj = selectedDate.ToShortDateString();
                                        }
                                        objArray9[1] = obj;
                                        selectedValue[2] = item.KitSite;
                                        selectedValue[3] = item.BoxSite;
                                        empty = string.Concat(str25, string.Format(str26, selectedValue));
                                        break;
                                    }
                                }
                                else
                                {
                                    flag2 = false;
                                    string str27 = empty;
                                    string str28 = string.Concat("<br> From Date value of rule {0}->{1}->{2}->{3} must be equal to To Date value of previous rule.");
                                    selectedValue = new object[4];
                                    object[] objArray10 = selectedValue;
                                    DateTime fromDate5 = item.FromDate;
                                    selectedDate = new DateTime();
                                    if (fromDate5 == selectedDate)
                                    {
                                        shortDateString1 = "";
                                    }
                                    else
                                    {
                                        selectedDate = item.FromDate;
                                        shortDateString1 = selectedDate.ToShortDateString();
                                    }
                                    objArray10[0] = shortDateString1;
                                    object[] objArray11 = selectedValue;
                                    DateTime dateTime5 = item.ToDate;
                                    selectedDate = new DateTime();
                                    if (dateTime5 == selectedDate)
                                    {
                                        obj1 = "";
                                    }
                                    else
                                    {
                                        selectedDate = item.ToDate;
                                        obj1 = selectedDate.ToShortDateString();
                                    }
                                    objArray11[1] = obj1;
                                    selectedValue[2] = item.KitSite;
                                    selectedValue[3] = item.BoxSite;
                                    empty = string.Concat(str27, string.Format(str28, selectedValue));
                                    break;
                                }
                            }
                            else
                            {
                                flag2 = false;
                                string str29 = empty;
                                string str30 = string.Concat("<br> From Date and To Date values can not be same. Rule {0}->{1}->{2}->{3}.");
                                selectedValue = new object[4];
                                object[] objArray12 = selectedValue;
                                DateTime fromDate6 = item.FromDate;
                                selectedDate = new DateTime();
                                if (fromDate6 == selectedDate)
                                {
                                    shortDateString3 = "";
                                }
                                else
                                {
                                    selectedDate = item.FromDate;
                                    shortDateString3 = selectedDate.ToShortDateString();
                                }
                                objArray12[0] = shortDateString3;
                                object[] objArray13 = selectedValue;
                                DateTime toDate6 = item.ToDate;
                                selectedDate = new DateTime();
                                if (toDate6 == selectedDate)
                                {
                                    obj3 = "";
                                }
                                else
                                {
                                    selectedDate = item.ToDate;
                                    obj3 = selectedDate.ToShortDateString();
                                }
                                objArray13[1] = obj3;
                                selectedValue[2] = item.KitSite;
                                selectedValue[3] = item.BoxSite;
                                empty = string.Concat(str29, string.Format(str30, selectedValue));
                                break;
                            }
                        }
                    }
                    if (!flag2)
                    {

                        empty = string.Concat("Update Unsuccessful!Please review errors given below: <br>", empty);
                        saveMessage.Message = empty;
                        saveMessage.IsSuccess = false;
                    }
                    else
                    {
                        _iSourcingRuleService.UserID = this.userName;
                        saveMessage.NumberOfRowImpacted = await _iSourcingRuleService.AddSourcingRuleCollection(sourcingRuleSaveClass);
                        saveMessage.Message = "Success";
                        saveMessage.IsSuccess = true;
                        ////if (dictionary.Count != 0)
                        ////{
                        ////    this.srDS.UpdateSourcingRuleCollection(sourcingRuleSaveClass, HttpContext.Current.User.Identity.Name);
                        ////}
                        ////else
                        ////{
                        //this.srDS.AddSourcingRuleCollection(sourcingRuleSaveClass, HttpContext.Current.User.Identity.Name);
                        ////}
                        //this.lblsuccess.Text = "Sourcing rule saved successfully.";
                        //this.successimage.Visible = true;
                        //HiddenField hiddenField = this.hdnSelectedChannel;
                        //string value = this.hfChannelSearch.Value;
                        //string str31 = value;
                        //hiddenField.Value = value;
                        //this.searchChannelId = str31;
                        //HiddenField hiddenField1 = this.hdnSelectedProduct;
                        //string value1 = this.hfProductSearch.Value;
                        //str31 = value1;
                        //hiddenField1.Value = value1;
                        //this.searchProductId = str31;
                        //this.ViewState["SearchMode"] = "Multiple";
                        //this.LoadFilters();
                        //this.BindingData();
                        //this.BindEditGrid();
                    }
                }
            }
            catch (Exception exception)
            {
                throw;
            }

            return saveMessage;
        }
        #endregion
    }
}