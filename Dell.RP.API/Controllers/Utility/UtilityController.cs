﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Dell.RP.BussinessServices.IServiceInterceptor.Utility;
using System.Net;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Dell.RP.API.Controllers.Utility
{
    [Route("api/[controller]")]
    [Authorize(Policy = "AllUsers")]
    public class UtilityController : Controller
    {
        IUtilityService _iUtilityService;
        private string userName = "";
        public UtilityController(IUtilityService iUtilityService, IHttpContextAccessor httpContextAccessor)
        {
            _iUtilityService = iUtilityService;
            var username = GetUser(httpContextAccessor);
            if (username.Result != null)
            {
                this.userName = username.Result;
            }
        }

        public async Task<string> GetUser(IHttpContextAccessor httpContextAccessor)
        {
            return await Task.Run(() =>
            {
                return httpContextAccessor.HttpContext.User?.Identity?.Name;
            });
        }
        //get product attribute by attribute code
        [HttpGet]
        [Route("GetProductAttributeByAttributeCode")]
        public async Task<ObjectResult> GetProductAttributeByAttributeCodeAsync(Int32 dimension)
        {
            ObjectResult response;
            //user.UserID = this.userName;
            try
            {
                var listResult = await _iUtilityService.GetAttributeNames(dimension,this.userName);
                if (listResult != null && listResult.Count > 0)
                {
                    response = new ObjectResult(listResult);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
            
        }
    }
}
