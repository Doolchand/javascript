﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Dell.RP.BussinessServices.IServiceInterceptor.Role_Menu_Mgmt;
using System.Net;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Dell.RP.API.Controllers.Role_Menu_Mgmt
{
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class MenuController : Controller
    {
        private readonly IMenuService _iMenuService;
        public MenuController(IMenuService iMenuService)
        {
            this._iMenuService = iMenuService;
        }

        [Route("GetMenus")]
        public async Task<ObjectResult> GetMenus()
        {
            ObjectResult response;
            this._iMenuService.UserID = "Test";
            try
            {
                var fileNameList = await this._iMenuService.GetMenus();
                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }


    }
}
