﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dell.RP.BussinessServices.IServiceInterceptor.Role_Menu_Mgmt;
using Dell.RP.Common.Models.Role_Menu_Mgmt;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using System.Net;
//using System.Web.Script.Serialization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Microsoft.Net.Http.Headers;
using System.DirectoryServices;
using Dell.RP.API.Controllers.HelperClasses;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Dell.RP.API.Controllers.Role_Menu_Mgmt
{
    [Route("api/[controller]")]
    [Authorize(Policy = "AllUsers")]
    public class AuthorizationController : Controller
    {
        #region Constructor & Local variables
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly BussinessServices.IServiceInterceptor.Role_Menu_Mgmt.IAuthorizationService iAuthorizationService;
        private string userName = "";
        LoggedUserDetails objUser = new LoggedUserDetails();
        public AuthorizationController(BussinessServices.IServiceInterceptor.Role_Menu_Mgmt.IAuthorizationService iAuthorizationService, IHttpContextAccessor httpContextAccessor, IClaimsTransformation d, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            this.iAuthorizationService = iAuthorizationService;
            var s = UserNameValidation.GetUser(httpContextAccessor);
            if (s.Result != null)
            {
                this.userName = s.Result;
            }
        }

      
        #endregion

        [HttpGet]
        [Route("GetRoleList")]
        public async Task<ObjectResult> GetRoleList(string userType)
        {
            ObjectResult response;
            iAuthorizationService.UserID = this.userName;
            try
            {
                var fileNameList = await iAuthorizationService.GetRoleList(userType);
                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetUserGroupList")]
        public async Task<ObjectResult> GetUserGroupList()
        {
            ObjectResult response;            
            iAuthorizationService.UserID = this.userName;
            try
            {
                var fileNameList = await iAuthorizationService.GetUserGroupList();
                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetRegionList")]
        public async Task<ObjectResult> GetRegionList()
        {
            ObjectResult response;
            iAuthorizationService.UserID = this.userName;
            try
            {
                var fileNameList = await iAuthorizationService.GetRegionList();
                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpPost]
        [Route("GetUserNameList")]
        public async Task<ObjectResult> GetUserNameList(string tenant, [FromBody] List<AuthList> region)
        {
            //if (region == null)
            //{
            //    List<AuthList> list = new List<AuthList>()
            //    {
            //        new AuthList {id="101", itemName="ANSHUMAN_PRADHAN" },
            //        new AuthList {id="102", itemName="SANDEEP_RASTOGI" }
            //    };
            //    return list;
            //}
            //else
            //{
            //    List<AuthList> list = new List<AuthList>()
            //    {
            //        new AuthList {id="101", itemName="ANSHUMAN_PRADHAN" },
            //    };
            //    return list;
            //}
            ObjectResult response;
            iAuthorizationService.UserID = this.userName;
            try
            {
                var fileNameList = await iAuthorizationService.GetUserNameList(tenant,region);
                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }
        [HttpGet]
        [Route("GetUserRoles")]
        public async Task<ObjectResult> GetUserRoles()
        {
            ObjectResult response;
            iAuthorizationService.UserID = this.userName;
            try
            {
                var fileNameList = await iAuthorizationService.GetUserRoles();
                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpPost]
        [Route("GetUserDetails")]
        public async Task<ObjectResult> GetUserDetails([FromBody] FilterDetails filters)
        {
            ObjectResult response;
            iAuthorizationService.UserID = this.userName;
          //  filters = HandleSpecialCharactersFilter(filters);
            try
            {
                var fileNameList = await iAuthorizationService.GetUserDetails(filters);
                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        private FilterDetails HandleSpecialCharactersFilter(FilterDetails filters)
        {
            foreach (var item in filters.UserName)
            {
                item.itemName = UserNameValidation.ValidateSpecialCharacter(item.itemName);
            }
            return filters;
        }

        [HttpPost]
        [Route("AddUsers")]
        public async Task<ObjectResult> AddUsers([FromBody] UserDetails user)
        {
            ObjectResult response;
            iAuthorizationService.UserID = this.userName;
            user.UserName = UserNameValidation.ValidateSpecialCharacter(user.UserName);
            user.UserEmail = UserNameValidation.ValidateSpecialCharacter(user.UserEmail);
            try
            {
                var fileNameList = await iAuthorizationService.AddUsers(user);
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpPost]
        [Route("UpdateUsers")]
        public async Task<ObjectResult> UpdateUsers([FromBody] List<UserDetails> user)
        {
            ObjectResult response;
            iAuthorizationService.UserID = this.userName;
            user = HandleSpecialCharacters(user);
            try
            {
                var fileNameList = await iAuthorizationService.UpdateUsers(user);
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        private List<UserDetails> HandleSpecialCharacters(List<UserDetails> user)
        {
            foreach (var item in user)
            {
                item.UserName = UserNameValidation.ValidateSpecialCharacter(item.UserName);
                item.UserEmail = UserNameValidation.ValidateSpecialCharacter(item.UserEmail);
            }
            return user;
        }

        [HttpGet]
        [Route("GetScreenAuthorizationList")]
        public async Task<ObjectResult> GetScreenAuthorizationList()
        {
            ObjectResult response;
            iAuthorizationService.UserID = this.userName;
            try
            {
                var fileNameList = await iAuthorizationService.GetScreenAuthorizationList();
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetUsertype")]
        public async Task<ObjectResult> GetUsertype()
        {
            ObjectResult response;
            //List<AuthList> list = new List<AuthList>()
            //{
            //    new AuthList {id="101", itemName="FGA" },
            //    new AuthList {id="102", itemName="UPP" }
            //};
            //return list;

            iAuthorizationService.UserID = this.userName;
            try
            {
                var fileNameList = await iAuthorizationService.GetUsertype();
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        [HttpGet]
        [Route("GetLOB")]
        public async Task<ObjectResult> GetLOB()
        {
            ObjectResult response;
            //List<AuthList> list = new List<AuthList>()
            //{
            //    new AuthList {id="101", itemName="LATITUDE" },
            //    new AuthList {id="102", itemName="LOB2" }
            //};
            //return list;

            iAuthorizationService.UserID = this.userName;
            try
            {
                var fileNameList = await iAuthorizationService.GetLOB();
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

        /// <summary>
        /// Get users LDAP details by Name
        /// </summary>
        /// <param name="name">name of the user</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUsersLdapDetails")]
        public async Task<string> GetUsersLdapDetails(string name)
        {
            string email = "";
            string domain = "";
            string returnValue = "";
            Dictionary<string, string> domainList = GetDomainList();
            try
            {
                System.DirectoryServices.DirectoryEntry dENtry = new DirectoryEntry("LDAP://ausdcamer.amer.dell.com:3268/dc=dell,dc=com");
                DirectorySearcher deSearch = new DirectorySearcher();
                deSearch.SearchRoot = dENtry;
                deSearch.Filter = "(cn=" + name + ")";
                Dictionary<string, string> d = new Dictionary<string, string>();
                SearchResult dsresult = deSearch.FindOne();

                if (dsresult != null)
                {
                    if (dsresult.Properties["sAMAccountName"][0] != null && dsresult.Properties["userAccountControl"][0] != null)
                    {
                        foreach (var item in domainList)
                        {
                            if (dsresult.Properties["distinguishedname"][0].ToString().Contains(item.Key))
                            {
                                domain = domainList[item.Key];
                            }
                        }

                        returnValue = dsresult.Properties["mail"][0].ToString() + "+++" + domain;
                    }
                }

            }
            catch (Exception ex)
            {
                //Logging.Log(ErrorType.Error, ex.Message, _config);
            }
            return returnValue;
        }

        private static Dictionary<string, string> GetDomainList()
        {
            Dictionary<string, string> domainList = new Dictionary<string, string>();
            domainList.Add("apac", "ASIA-PACIFIC");
            domainList.Add("emea", "EUROPE");
            domainList.Add("amer", "AMERICAS");
            domainList.Add("japn", "ASIA-PACIFIC"); //Japan also in Asia Pacific added
            return domainList;
        }

        [HttpGet]
        [Route("GetLoggedInUserRegionBasedOnRole")]
        public async Task<ObjectResult> GetLoggedInUserRegionBasedOnRoleAsync(string UserId)
        {
            ObjectResult response;
            objUser.UserId = UserId;          
            try
            {
                var listResult = await iAuthorizationService.GetLoggedInUserRegionBasedOnRoleAsync(objUser);
                if (listResult != null)
                {
                    response = new ObjectResult(listResult);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }

        [HttpGet]
        [Route("GetRoleBasedRegion")]
        public async Task<ObjectResult> GetRoleBasedRegion(string screenId, string userType)
        {

            //return "AMER,APJ";
            ObjectResult response;
            iAuthorizationService.UserID = this.userName;
            try
            {
                var listResult = await iAuthorizationService.GetRoleBasedRegion(screenId, userType);
                if (listResult != null)
                {
                    response = new ObjectResult(listResult);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;
        }


        [HttpGet]
        [Route("GetRolesCanAccessScreenList")]
        public async Task<ObjectResult> GetRolesCanAccessScreenList()
        {
            ObjectResult response;
            iAuthorizationService.UserID = this.userName;
            try
            {
                var fileNameList = await iAuthorizationService.GetRolesCanAccessScreenList();
                if (fileNameList != null)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }

    }
}

