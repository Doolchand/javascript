﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Dell.RP.BussinessServices.IServiceInterceptor.Role_Menu_Mgmt;
using System.Net;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Dell.RP.API.Controllers.Role_Menu_Mgmt
{
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class RoleController : Controller
    {
        private readonly IRoleService _iRoleService;
        public RoleController(IRoleService iRoleService)
        {
            this._iRoleService = iRoleService;
        }

        [Route("GetRoles")]
        public async Task<ObjectResult> GetMenus()
        {
            ObjectResult response;
            this._iRoleService.UserID = "";
            try
            {
                var fileNameList = await this._iRoleService.GetRoles();
                if (fileNameList != null && fileNameList.Count > 0)
                {
                    response = new ObjectResult(fileNameList);
                    response.StatusCode = (int)HttpStatusCode.OK;
                    return response;
                }
                else
                {
                    response = new ObjectResult("No data found.");
                    response.StatusCode = (int)HttpStatusCode.NoContent;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response = new ObjectResult(ex.Message);
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            return response;

        }


    }
}
