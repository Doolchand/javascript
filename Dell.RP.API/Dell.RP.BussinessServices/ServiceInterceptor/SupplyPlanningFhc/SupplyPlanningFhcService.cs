﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.SupplyPlanningFhc;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.SupplyPlanningFhc;
using Dell.RP.API.Dell.RP.Common.Models.SupplyPlanningFhc;
using Dell.RP.API.Dell.RP.Common.Models;
using System.Linq;
using Newtonsoft.Json;
using System.Data;
using System;
namespace Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.SupplyPlanningFhc
{
    public class SupplyPlanningFhcService : ISupplyPlanningFhcService
    {
        private readonly ISupplyPlanningFhcRepository _iSupplyPlanningFhcRepository;

        public SupplyPlanningFhcService(ISupplyPlanningFhcRepository iSupplyPlanningFhcRepository)
        {
            this._iSupplyPlanningFhcRepository = iSupplyPlanningFhcRepository;
        }

        public string UserID { get; set; }

        public async Task<List<DropDown>> GetManSite()
        {
            this._iSupplyPlanningFhcRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._iSupplyPlanningFhcRepository.GetManSite();
            });
        }

        public string GetRPStatus()
        {
            this._iSupplyPlanningFhcRepository.UserID = this.UserID;
            return this._iSupplyPlanningFhcRepository.GetRPStatus();
            
        }

        public List<DropDown> BindPOUILock()
        {
            this._iSupplyPlanningFhcRepository.UserID = this.UserID;
            return this._iSupplyPlanningFhcRepository.BindPOUILock();
        }

        public async Task<List<Get_SupplyPlanningFhc_Export_Details>> GetRPSupplyPlanningFhcExportDetails(Export_Parameters exportParams)
        {
            this._iSupplyPlanningFhcRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._iSupplyPlanningFhcRepository.GetRPSupplyPlanningFhcExportDetails(exportParams);
            });
        }

        public async Task<List<DropDown>> GetFamilyParentDD()
        {
            this._iSupplyPlanningFhcRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._iSupplyPlanningFhcRepository.GetFamilyParentDD();
            });
        }

        public string InsertFHC(List<RPSupplyPlan_Import_Params> importData)
        {
            this._iSupplyPlanningFhcRepository.UserID = this.UserID;
            return this._iSupplyPlanningFhcRepository.InsertFHC(importData);
            
        }

        public List<Error_Grid_Details> getErrGridDetails()
        {
            this._iSupplyPlanningFhcRepository.UserID = this.UserID;
            return this._iSupplyPlanningFhcRepository.getErrGridDetails();
        }

        public string GetLockStatusFHC(string flag)
        {
            this._iSupplyPlanningFhcRepository.UserID = this.UserID;
            return this._iSupplyPlanningFhcRepository.GetLockStatusFHC(flag);

        }

        public string GetValidRecStatusFHC()
        {
            this._iSupplyPlanningFhcRepository.UserID = this.UserID;
            return this._iSupplyPlanningFhcRepository.GetValidRecStatusFHC();

        }

        public int UpdateAutoUnlockUIFHC()
        {
            this._iSupplyPlanningFhcRepository.UserID = this.UserID;
            return this._iSupplyPlanningFhcRepository.UpdateAutoUnlockUIFHC();

        }
    }
}
