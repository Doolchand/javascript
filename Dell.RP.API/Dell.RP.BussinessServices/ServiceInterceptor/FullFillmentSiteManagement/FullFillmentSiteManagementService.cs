﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.Common.Models;
using System.Linq;
using Newtonsoft.Json;
using System.Data;
using System;

namespace Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.FullFillmentSiteManagement
{
    public class FullFillmentSiteManagementService : IFullFillmentSiteManagement
    {
        private readonly IFullFillmentSiteManagementRepository _iFullFillmentSiteManagementRepository;

        public FullFillmentSiteManagementService(IFullFillmentSiteManagementRepository iFullFillmentSiteManagementRepository)
        {
            this._iFullFillmentSiteManagementRepository = iFullFillmentSiteManagementRepository;
        }

        public string UserID { get; set; }

        public async Task<List<DropDown>> GetAccountNameFullfilment()
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._iFullFillmentSiteManagementRepository.GetAccountNameFullfilment();
            });
        }

        public async Task<List<string>> GetChannelType(string channelId)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._iFullFillmentSiteManagementRepository.GetChannelType(channelId);
            });
        }

        public async Task<List<DropDown>> GetGeoCountries()
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._iFullFillmentSiteManagementRepository.GetGeoCountries();
            });
        }


        public async Task<List<DropDown>> GetMasterDataData(string key)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._iFullFillmentSiteManagementRepository.GetMasterDataData(key);
            });
        }

        public async Task<List<DropDown>> GetChannelSegments()
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._iFullFillmentSiteManagementRepository.GetChannelSegments();
            });
        }

        public async Task<string> GetGeoTreeData()
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                List<GeoTreeData> geoDatas = new List<GeoTreeData>();
                geoDatas = this._iFullFillmentSiteManagementRepository.GetGeoTreeData().Result;
                return GetGeoJsonDataWithoutChidName(geoDatas);
            });
        }
        public string GetGeoJsonDataWithoutChidName(List<GeoTreeData> geoDatas)
        {
            var GeoHierarchy = from l1 in geoDatas
                                   //group l1 by new { id = l1.AllRegionId, name = l1.AllRegion, level = "0" } into group1
                               group l1 by new  { id = l1.AllRegionId, name = l1.AllRegion, level = "0" } into group1 orderby group1.Key.name
                               select new
                               {
                                   id = group1.Key.id + "__" + group1.Key.level,
                                   name =  group1.Key.name,
                                   children = from l2 in group1
                                              group l2 by new { id = l2.RegionId, name = l2.RegionId + "(" + l2.Region + ")", level = "1" } into group2 orderby group2.Key.name
                                              select new
                                              {
                                                  id = group2.Key.id + "__" + group2.Key.level,
                                                  name = group2.Key.name,
                                                  children = from l3 in group2
                                                             group l3 by new { id = l3.SubRegionId, name = l3.SubRegionId + "(" + l3.SubRegion + ")", level = "2" } into group3 orderby group3.Key.name
                                                             select new
                                                             {
                                                                 id = group3.Key.id + "__" + group3.Key.level,
                                                                 name = group3.Key.name,
                                                                 children = from l4 in group3
                                                                            group l4 by new { id = l4.AreaId, name = l4.AreaId + "(" + l4.Area + ")", level = "3" } into group4 orderby group4.Key.name
                                                                            select new
                                                                            {
                                                                                id = group4.Key.id + "__" + group4.Key.level,
                                                                                name = group4.Key.name,
                                                                                children = from l5 in group4
                                                                                               //select new { id = l5.CountryId + "__4", name = l5.CountryId + "(" + l5.Country + ")"  }
                                                                                           group l5 by new { id = l5.CountryId, name = l5.CountryId + "(" + l5.Country + ")", level="4" } into group5 orderby group5.Key.name
                                                                                           select new
                                                                                           {
                                                                                               id = group5.Key.id + "__" + group5.Key.level,
                                                                                               name = group5.Key.name,
                                                                                           }
                                                                            } 
                                                             }
                                              }
                               };
            return JsonConvert.SerializeObject(GeoHierarchy);

        }

        public async Task<string> GetProductTreeData(string strString)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                List<ProdTreeData> prodDatas = new List<ProdTreeData>();
                prodDatas = this._iFullFillmentSiteManagementRepository.GetProductTreeData(strString).Result;
                return GetProductJsonDataWithoutChidName(prodDatas);
            });
        }

        public async Task<string> GetProductFHCTreeData(string strString)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                List<FhcTreeData> FHCDatas = new List<FhcTreeData>();
                FHCDatas = this._iFullFillmentSiteManagementRepository.GetProductFHCTreeData(strString).Result;
                return GetProductJsonDataWithoutChidName_FHC(FHCDatas);
            });
        }

        public async Task<string> GetProductFGATreeData(string strString)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                List<FgaTreeData> prodDatas = new List<FgaTreeData>();
                prodDatas = this._iFullFillmentSiteManagementRepository.GetProductFGATreeData(strString).Result;
                return GetProductJsonDataWithoutChidName_FGA(prodDatas);
            });
        }

        public string GetProductJsonDataWithoutChidName(List<ProdTreeData> prodDatas)
        {
            var ProductHierarchy = from l1 in prodDatas
                                   group l1 by new { id = l1.AllProductId, name = l1.AllProduct, level = "0" } into group1 orderby group1.Key.name
                                   select new
                                   {
                                       id = group1.Key.id + "__" + group1.Key.level,
                                       name = group1.Key.name,
                                       children = from l2 in group1
                                                  group l2 by new { id = l2.LOBProdId, name = l2.LOBProd + "(" + l2.LOBProdId + ")", level = "1" } into group2 orderby group2.Key.name
                                                  select new
                                                  {
                                                      id = group2.Key.id + "__" + group2.Key.level,
                                                      name = group2.Key.name,
                                                      children = from l3 in group2
                                                                 group l3 by new { id = l3.FamilyParentId, name = l3.FamilyParent + "(" + l3.FamilyParentId + ")", level = "2" } into group3 orderby group3.Key.name
                                                                 select new
                                                                 {
                                                                     id = group3.Key.id + "__" + group3.Key.level,
                                                                     name = group3.Key.name,
                                                                     children = from l4 in group3
                                                                                group l4 by new { id = l4.BaseProdId, name = l4.BaseProd + "(" + l4.BaseProdId + ")", level = "3" } into group4 orderby group4.Key.name
                                                                                select new
                                                                                {
                                                                                    id = group4.Key.id + "__" + group4.Key.level,
                                                                                    name = group4.Key.name

                                                                                    //children = from l5 in group3
                                                                                    //           select new { id = l5.CountryId + "__4", name = l5.Country + "(" + l5.CountryId + ")" }
                                                                                }
                                                                 }
                                                  }
                                   };
            return JsonConvert.SerializeObject(ProductHierarchy);

        }

        public string GetProductJsonDataWithoutChidName_FHC(List<FhcTreeData> fhcDatas)
        {
            var fhcHierarchy = from l1 in fhcDatas
                               group l1 by new { id = l1.AllFHCId, name = l1.AllFHC, level = "0" } into group1 orderby group1.Key.name
                               select new
                               {
                                   id = group1.Key.id + "__" + group1.Key.level,
                                   name = group1.Key.name,
                                   children = from l2 in group1
                                              group l2 by new { id = l2.FHCId, name = l2.FHC + "(" + l2.FHCId + ")", level = "1" } into group2 orderby group2.Key.name
                                              select new
                                              {
                                                  id = group2.Key.id + "__" + group2.Key.level,
                                                  name = group2.Key.name,
                                                  children = from l3 in group2
                                                             group l3 by new { id = l3.FgaId, name = l3.Fga + "(" + l3.FgaId + ")", level = "2" } into group3 orderby group3.Key.name
                                                             select new
                                                             {
                                                                 id = group3.Key.id + "__" + group3.Key.level,
                                                                 name = group3.Key.name,
                                                                 //children = from l4 in group3
                                                                 //           group l4 by new { id = l4.BaseProdId, name = l4.BaseProd + "(" + l4.BaseProdId + ")", level = "3" } into group4
                                                                 //           select new
                                                                 //           {
                                                                 //               id = group4.Key.id + "__" + group4.Key.level,
                                                                 //               name = group4.Key.name

                                                                 //               //children = from l5 in group3
                                                                 //               //           select new { id = l5.CountryId + "__4", name = l5.Country + "(" + l5.CountryId + ")" }
                                                                 //           }
                                                             }
                                              }
                               };
            return JsonConvert.SerializeObject(fhcHierarchy);
        }

        public string GetProductJsonDataWithoutChidName_FGA(List<FgaTreeData> prodDatas)
        {
            var ProductHierarchy = from l1 in prodDatas
                                   group l1 by new { id = l1.AllFgaId, name = l1.AllFga, level = "0" } into group1 orderby group1.Key.name
                                   select new
                                   {
                                       id = group1.Key.id + "__" + group1.Key.level,
                                       name = group1.Key.name,
                                       children = from l2 in group1
                                                  group l2 by new { id = l2.FgaId, name = l2.Fga + "(" + l2.FgaId + ")", level = "1" } into group2 orderby group2.Key.name
                                                  select new
                                                  {
                                                      id = group2.Key.id + "__" + group2.Key.level,
                                                      name = group2.Key.name
                                                      //children = from l3 in group2
                                                      //group l3 by new { id = l3.FamilyParentId, name = l3.FamilyParent + "(" + l3.FamilyParentId + ")", level = "2" } into group3
                                                      //select new
                                                      //{
                                                      //    id = group3.Key.id + "__" + group3.Key.level,
                                                      //    name = group3.Key.name,
                                                      //    children = from l4 in group3
                                                      //               group l4 by new { id = l4.BaseProdId, name = l4.BaseProd + "(" + l4.BaseProdId + ")", level = "3" } into group4
                                                      //               select new
                                                      //               {
                                                      //                   id = group4.Key.id + "__" + group4.Key.level,
                                                      //                   name = group4.Key.name

                                                      //                   //children = from l5 in group3
                                                      //                   //           select new { id = l5.CountryId + "__4", name = l5.Country + "(" + l5.CountryId + ")" }
                                                      //               }
                                                      //}
                                                  }
                                   };
            return JsonConvert.SerializeObject(ProductHierarchy);
        }

        public async Task<List<FulfillmentDetail>> GetFulFillmentSearchResult(FulFillmentSearchInput searchInput)
        {
            List<FulfillmentDetail> lstFulfillmentDetails = new List<FulfillmentDetail>();
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                DataSet ds = new DataSet();
                ds = this._iFullFillmentSiteManagementRepository.GetFulFillmentSearchResult(searchInput).Result;
                DataTable RoutingHeaderTable = new DataTable();
                DataTable RoutingLinesTable = new DataTable();

                RoutingHeaderTable = ds.Tables["ROUTING_HEADER_VW"];
                RoutingLinesTable = ds.Tables["ROUTING_LINES_VW"];

                string concatfga = null;
                if (RoutingHeaderTable.Rows.Count > 0)
                {
                    RoutingHeaderTable.Columns.Add("FLAG");
                    RoutingHeaderTable.Rows[0]["FLAG"] = "0";

                    for (int rownum = 1; rownum < RoutingHeaderTable.Rows.Count; rownum++)
                    {
                        if (RoutingHeaderTable.Rows[rownum]["ID"].ToString().Equals(RoutingHeaderTable.Rows[rownum - 1]["ID"].ToString()))
                        {
                            RoutingHeaderTable.Rows[rownum]["FLAG"] = "1";
                        }
                        else
                        {
                            RoutingHeaderTable.Rows[rownum]["FLAG"] = "0";
                        }
                    }
                
                    RoutingHeaderTable.Columns.Add("PS");
                    RoutingHeaderTable.Columns.Add("ProductFGA");
                    foreach (DataRow row in RoutingHeaderTable.Rows)
                    {
                        string concat = string.Empty;

                        DataView dv = RoutingLinesTable.DefaultView;
                        dv.RowFilter = "ROUTING_ID = '" + row["ROUTING_ID"] + "'";
                        foreach (DataRowView drv in dv)
                        {

                            if (concat.Length > 0)
                            {
                                concat += "," + drv["FULFILLMENT_SITE_ID"].ToString() + "(" + drv["SPLIT"].ToString() + ")";

                            }
                            else
                            {
                                concat += drv["FULFILLMENT_SITE_ID"].ToString() + "(" + drv["SPLIT"].ToString() + ")";
                            }
                        }
                        string fga = row["ITEM"].ToString();
                        string product = row["PRODUCT_ID"].ToString();
                        concatfga = product + fga;
                        row["ProductFGA"] = concatfga;
                        row["PS"] = concat;
                    }

                    lstFulfillmentDetails = ConvertFulfillmentDTtoList(RoutingHeaderTable);
                }
                else
                {

                    //ToDO: error msg
                    //norules.Text = ConfigurationManager.AppSettings["msgNoRule"];
                    //DataTable dt = new DataTable();
                    //GridView1.DataSource = dt;
                    //GridView1.DataBind();
                }
                return lstFulfillmentDetails;
            });


        }


        private List<FulfillmentDetail> ConvertFulfillmentDTtoList(DataTable dt)
        {
            List<FulfillmentDetail> lstFulfillmentData = new List<FulfillmentDetail>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var output = new FulfillmentDetail();
                if (dt.Rows[i]["ROUTING_ID"] != System.DBNull.Value)
                {
                    output.RoutingId = Convert.ToString(dt.Rows[i]["ROUTING_ID"]);
                }

                if (dt.Rows[i]["PRODUCT_ID"] != System.DBNull.Value)
                {
                    output.ProductId = Convert.ToString(dt.Rows[i]["PRODUCT_ID"]);
                }

                if (dt.Rows[i]["ITEM"] != System.DBNull.Value)
                {
                    output.Item = Convert.ToString(dt.Rows[i]["ITEM"]);
                }

                if (dt.Rows[i]["EFFECTIVE_START_DATE"] != System.DBNull.Value)
                {
                    output.EffectiveStartDate = Convert.ToString(dt.Rows[i]["EFFECTIVE_START_DATE"]);
                }

                if (dt.Rows[i]["EFFECTIVE_END_DATE"] != System.DBNull.Value)
                {
                    output.EffectiveEndDate = Convert.ToString(dt.Rows[i]["EFFECTIVE_END_DATE"]);
                }

                if (dt.Rows[i]["PRODUCTFGA"] != System.DBNull.Value)
                {
                    output.ProductFga = Convert.ToString(dt.Rows[i]["PRODUCTFGA"]);
                }

                if (dt.Rows[i]["CHANNEL_ID"] != System.DBNull.Value)
                {
                    output.ChannelId = Convert.ToString(dt.Rows[i]["CHANNEL_ID"]);
                }

                if (dt.Rows[i]["CHANNEL_NAME"] != System.DBNull.Value)
                {
                    output.ChannelName = Convert.ToString(dt.Rows[i]["CHANNEL_NAME"]);
                }

                if (dt.Rows[i]["GEOGRAPHY_ID"] != System.DBNull.Value)
                {
                    output.GeographyId = Convert.ToString(dt.Rows[i]["GEOGRAPHY_ID"]);
                }

                if (dt.Rows[i]["GEOGRAPHY_NAME"] != System.DBNull.Value)
                {
                    output.GeographyName = Convert.ToString(dt.Rows[i]["GEOGRAPHY_NAME"]);
                }

                if (dt.Rows[i]["SSC"] != System.DBNull.Value)
                {
                    output.Ssc = Convert.ToString(dt.Rows[i]["SSC"]);
                }

                if (dt.Rows[i]["SHIP_VIA"] != System.DBNull.Value)
                {
                    output.ShipVia = Convert.ToString(dt.Rows[i]["SHIP_VIA"]);
                }

                if (dt.Rows[i]["ACCOUNT_NAME"] != System.DBNull.Value)
                {
                    output.AccountName = Convert.ToString(dt.Rows[i]["ACCOUNT_NAME"]);
                }

                if (dt.Rows[i]["ACCOUNT_ID"] != System.DBNull.Value)
                {
                    output.AccountId = Convert.ToString(dt.Rows[i]["ACCOUNT_ID"]);
                }

                if (dt.Rows[i]["SYS_LAST_MODIFIED_BY"] != System.DBNull.Value)
                {
                    output.SysLastModifiedBy = Convert.ToString(dt.Rows[i]["SYS_LAST_MODIFIED_BY"]);
                }

                if (dt.Rows[i]["SYS_LAST_MODIFIED_DATE"] != System.DBNull.Value)
                {
                    output.SysLastModifiedDate = Convert.ToString(dt.Rows[i]["SYS_LAST_MODIFIED_DATE"]);
                }

                if (dt.Rows[i]["CHANNEL_LEVEL"] != System.DBNull.Value)
                {
                    output.ChannelLevel = Convert.ToString(dt.Rows[i]["CHANNEL_LEVEL"]);
                }

                if (dt.Rows[i]["GEOGRAPHY_LEVEL"] != System.DBNull.Value)
                {
                    output.GeographyLevel = Convert.ToString(dt.Rows[i]["GEOGRAPHY_LEVEL"]);
                }

                if (dt.Rows[i]["ID"] != System.DBNull.Value)
                {
                    output.Id = Convert.ToString(dt.Rows[i]["ID"]);
                }

                if (dt.Rows[i]["FLAG"] != System.DBNull.Value)
                {
                    output.Flag = Convert.ToString(dt.Rows[i]["FLAG"]);
                }

                if (dt.Rows[i]["PS"] != System.DBNull.Value)
                {
                    output.Ps = Convert.ToString(dt.Rows[i]["PS"]);
                }

                lstFulfillmentData.Add(output);
            }

            return lstFulfillmentData;
        }

        public async Task<string> GetChannelTreeData()
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                List<ChannelTreeData> channelDatas = new List<ChannelTreeData>();
                channelDatas = this._iFullFillmentSiteManagementRepository.GetChannelTreeData().Result;
                return GetGeoJsonDataWithoutChidName_Channel(channelDatas);
            });
        }

        public string GetGeoJsonDataWithoutChidName_Channel(List<ChannelTreeData> channelDatas)
        {
            var ProductHierarchy = from l1 in channelDatas
                                   group l1 by new { id = l1.AllChannelId, name = l1.AllChannel, level = "0" } into group1 orderby group1.Key.name
                                   select new
                                   {
                                       id = group1.Key.id + "__" + group1.Key.level,
                                       name = group1.Key.name,
                                       children = from l2 in group1
                                                  group l2 by new { id = l2.GroupId, name = l2.GroupId + "(" + l2.GroupName + ")", level = "1" } into group2 orderby group2.Key.name
                                                  select new
                                                  {
                                                      id = group2.Key.id + "__" + group2.Key.level,
                                                      name = group2.Key.name,
                                                      children = from l3 in group2
                                                                 group l3 by new { id = l3.ChannelId, name = l3.ChannelId + "(" + l3.ChannelName + ")", level = "2" } into group3 orderby group3.Key.name
                                                                 select new
                                                                 {
                                                                     id = group3.Key.id + "__" + group3.Key.level,
                                                                     name = group3.Key.name,
                                                                     children = from l4 in group3
                                                                                group l4 by new { id = l4.BaselId, name = l4.BaselId + "(" + l4.BaseName + ")", level = "3" } into group4 orderby group4.Key.name
                                                                                select new
                                                                                {
                                                                                    id = group4.Key.id + "__" + group4.Key.level,
                                                                                    name = group4.Key.name

                                                                                    //                   //children = from l5 in group3
                                                                                    //                   //           select new { id = l5.CountryId + "__4", name = l5.Country + "(" + l5.CountryId + ")" }
                                                                                                   }
                                                                                }
                                                                 }
                                   };
            return JsonConvert.SerializeObject(ProductHierarchy);
        }

        public async Task<int> DeleteRoutingDetails(string singleDeleteFlag, string ID)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._iFullFillmentSiteManagementRepository.DeleteRoutingDetails(singleDeleteFlag, ID);
            });
        }

        public async Task<int> DeleteRoutingDetailsHeader(string singleDeleteFlag, string ID)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._iFullFillmentSiteManagementRepository.DeleteRoutingDetailsHeader(singleDeleteFlag, ID);
            });
        }


        public async Task<List<FulfillmentRuleDetails>> GetFulfillmentRetrive(FulFillmentSearchInput retrieveInput)
        {
            List<FulfillmentRuleDetails> lstFulfillmentDetails = new List<FulfillmentRuleDetails>();
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            DataTable dtRetrieve = new DataTable();
            return await Task.Run(() =>
            {
                DataSet ds = new DataSet();
                ds = this._iFullFillmentSiteManagementRepository.GetFulfillmentRetrive(retrieveInput).Result;

                DataTable dtData = new DataTable();
                dtData = ds.Tables[1];

                dtRetrieve = getTempDataWithDates(dtData);

                if(dtRetrieve != null && dtRetrieve.Rows.Count>0)
                {
                    for(int i=0;i<dtRetrieve.Rows.Count;i++)
                    {

                        var output = new FulfillmentRuleDetails();
                        //output.Id = String.Empty;
                        if (dtRetrieve.Rows[i]["FULFILLMENT_SITE_ID"] != System.DBNull.Value)
                        {
                            output.fulfillmentSiteId = Convert.ToString(dtRetrieve.Rows[i]["FULFILLMENT_SITE_ID"]);
                        }

                        if (dtRetrieve.Rows[i]["EFFECTIVE_BEGIN_DATE"] != System.DBNull.Value)
                        {
                            output.effectiveBeginDate = Convert.ToString(dtRetrieve.Rows[i]["EFFECTIVE_BEGIN_DATE"]);
                        }

                        if (dtRetrieve.Rows[i]["EFFECTIVE_END_DATE"] != System.DBNull.Value)
                        {
                            output.effectiveEndDate = Convert.ToString(dtRetrieve.Rows[i]["EFFECTIVE_END_DATE"]);
                        }

                        if (dtRetrieve.Rows[i]["Begin_Date_Value"] != System.DBNull.Value)
                        {
                            output.beginDateValue = Convert.ToString(dtRetrieve.Rows[i]["Begin_Date_Value"]);
                        }

                        if (dtRetrieve.Rows[i]["End_Date_Value"] != System.DBNull.Value)
                        {
                            output.endDateValue = Convert.ToString(dtRetrieve.Rows[i]["End_Date_Value"]);
                        }

                        if (dtRetrieve.Rows[i]["Hidden_Routing_ID"] != System.DBNull.Value)
                        {
                            output.hiddenRoutingId = Convert.ToString(dtRetrieve.Rows[i]["Hidden_Routing_ID"]);
                        }

                        if (dtRetrieve.Rows[i]["Hidden"] != System.DBNull.Value)
                        {
                            output.hiddlenFlag = Convert.ToString(dtRetrieve.Rows[i]["Hidden"]);
                        }

                        lstFulfillmentDetails.Add(output);
                    }
                }
            
                return lstFulfillmentDetails;
            });
        }

        public DataTable getTempDataWithDates(DataTable data)
        {
            DataRow dtRow = null;
            DataTable dtFulfilmentDates = new DataTable();
            string intermediateRoutingID = "RoutingID";

            dtFulfilmentDates.Columns.Add("FULFILLMENT_SITE_ID", typeof(string));
            dtFulfilmentDates.Columns.Add("EFFECTIVE_BEGIN_DATE", typeof(string));
            dtFulfilmentDates.Columns.Add("EFFECTIVE_END_DATE", typeof(string));
            dtFulfilmentDates.Columns.Add("Begin_Date_Value", typeof(string)); 
            dtFulfilmentDates.Columns.Add("End_Date_Value", typeof(string)); 
            dtFulfilmentDates.Columns.Add("Hidden_Routing_ID", typeof(string));      
            dtFulfilmentDates.Columns.Add("Hidden", typeof(string)); 

            foreach (DataRow row in data.Rows)
            {

                DataView dv = data.DefaultView;
                dv.RowFilter = "ROUTING_ID = '" + row["ROUTING_ID"] + "'";

                if (intermediateRoutingID != row["ROUTING_ID"].ToString())
                {
                    dtRow = dtFulfilmentDates.NewRow();
                    string concat = string.Empty;

                    foreach (DataRowView drv in dv)
                    {
                        if (concat.Length > 0)
                        {
                            concat += "," + drv["FULFILLMENT_SITE_ID"].ToString() + "(" + drv["SPLIT"].ToString() + ")";
                        }
                        else
                        {
                            concat += drv["FULFILLMENT_SITE_ID"].ToString() + "(" + drv["SPLIT"].ToString() + ")";
                        }
                    }

                    dtRow["FULFILLMENT_SITE_ID"] = concat;
                    dtRow["EFFECTIVE_BEGIN_DATE"] = row["EFFECTIVE_START_FISCAL_WEEK"].ToString();
                    dtRow["EFFECTIVE_END_DATE"] = row["EFFECTIVE_END_FISCAL_WEEK"].ToString();
                    dtRow["Begin_Date_Value"] = row["EFFECTIVE_START_DATE"].ToString(); ;
                    dtRow["End_Date_Value"] = row["EFFECTIVE_END_DATE"].ToString(); ;
                    dtRow["Hidden_Routing_ID"] = row["ROUTING_ID"].ToString(); ;
                    dtRow["Hidden"] = "Read";
                    dtFulfilmentDates.Rows.Add(dtRow);
                }

                intermediateRoutingID = row["ROUTING_ID"].ToString();
            }

            dtFulfilmentDates.AcceptChanges();
            return dtFulfilmentDates;
        }

        public string getRegionName(string country)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return this._iFullFillmentSiteManagementRepository.getRegionName(country);
        }

        public List<string> GetNonVirtualSites(string regionCode)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return this._iFullFillmentSiteManagementRepository.GetNonVirtualSites(regionCode);
        }

        public List<DropDown> GetFSAFiscalCalendarInfo_BeginDate()
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;

            List<DropDown> lstStartDate = new List<DropDown>();
            DataTable dtFiscalInfo = new DataTable();
            dtFiscalInfo = this._iFullFillmentSiteManagementRepository.GetFSAFiscalCalendarInfo_BeginDate();

            if (dtFiscalInfo != null && dtFiscalInfo.Rows.Count > 0)
            {
                foreach (DataRow dr in dtFiscalInfo.Rows)
                {
                    lstStartDate.Add(new DropDown { Id = dr["BEGIN_CAL_DATE"].ToString(), ItemName = dr["BEGIN_FISCAL_WEEK"].ToString() });
                }
            }
            return lstStartDate;
        }

        public List<DropDown> GetFSAFiscalCalendarInfo_EndDate()
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;

            List<DropDown> lstEndDate = new List<DropDown>();
            DataTable dtFiscalInfo = new DataTable();
            dtFiscalInfo = this._iFullFillmentSiteManagementRepository.GetFSAFiscalCalendarInfo_BeginDate();

            if (dtFiscalInfo != null && dtFiscalInfo.Rows.Count > 0)
            {
                foreach (DataRow dr in dtFiscalInfo.Rows)
                {
                    lstEndDate.Add(new DropDown { Id = dr["END_CAL_DATE"].ToString(), ItemName = dr["END_FISCAL_WEEK"].ToString() });
                }
            }
            return lstEndDate;
        }

        public bool IsVirtualSite(string siteName)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return this._iFullFillmentSiteManagementRepository.IsVirtualSite(siteName);
        }

        public List<RoutingDetail> retrieveRoutingDetailsByRoutingId(string routingID)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            DataTable dt = new DataTable();
            dt = this._iFullFillmentSiteManagementRepository.retrieveRoutingDetailsByRoutingId(routingID);
            return ConvertRoutingDTtoList(dt);
        }

        public List<RoutingDetail> retrieveRoutingDetails(FulFillmentSearchInput retrieveInput)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            DataTable dt = new DataTable();
            dt = this._iFullFillmentSiteManagementRepository.retrieveRoutingDetails(retrieveInput);
            return ConvertRoutingDTtoList(dt);
        }

        private List<RoutingDetail> ConvertRoutingDTtoList(DataTable dt)
        {
            List<RoutingDetail> lstRoutingData = new List<RoutingDetail>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var output = new RoutingDetail();
                if (dt.Rows[i]["ROUTING_ID"] != System.DBNull.Value)
                {
                    output.RoutingId = Convert.ToString(dt.Rows[i]["ROUTING_ID"]);
                }

                if (dt.Rows[i]["FULFILLMENT_SITE_ID"] != System.DBNull.Value)
                {
                    output.FulfillmentSiteId = Convert.ToString(dt.Rows[i]["FULFILLMENT_SITE_ID"]);
                }
                lstRoutingData.Add(output);
            }
            return lstRoutingData;
        }

        public bool InsertValidateEffectiveDates(FullFillmentRuleData FSAEntity)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return this._iFullFillmentSiteManagementRepository.InsertValidateEffectiveDates(FSAEntity);
        }

        public int AddRoutingdetails(FullFillmentRuleData Full)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            Full.last_modified_by = this.UserID;
            return this._iFullFillmentSiteManagementRepository.AddRoutingdetails(Full);
        }

        public int AddFullFillmentsites(FullFillmentRuleData itFC)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return this._iFullFillmentSiteManagementRepository.AddFullFillmentsites(itFC);
        }

        public bool UpdateValidateEffectiveDates(UpdateValidateDateInputParams lstUpdateValidateParams)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return this._iFullFillmentSiteManagementRepository.UpdateValidateEffectiveDates(lstUpdateValidateParams);
        }

        public int updatefulfillment(FullFillmentRuleData updateFulfillment)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return this._iFullFillmentSiteManagementRepository.updatefulfillment(updateFulfillment);
        }

        public int DeleteFulfillDetails(FullFillmentRuleData lstFulfillDeleteSite)
        {
            this._iFullFillmentSiteManagementRepository.UserID = this.UserID;
            return this._iFullFillmentSiteManagementRepository.DeleteFulfillDetails(lstFulfillDeleteSite);
        }
    }
}
