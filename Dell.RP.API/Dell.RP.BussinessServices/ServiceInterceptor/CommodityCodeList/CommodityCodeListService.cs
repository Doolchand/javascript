﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.CommodityCodeList;
using Dell.RP.API.Dell.RP.Common.Models.CommodityCodeListDetails;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.CommodityCodeList;


namespace Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.CommodityCodeList
{
    public class CommodityCodeListService: ICommodityCodeList
    {
        public string UserID { get; set; }

        private readonly ICommodityCodeListRepository _iCommodityCodeListRepository;

        public CommodityCodeListService(ICommodityCodeListRepository iCommodityCodeListRepository)
        {
            this._iCommodityCodeListRepository = iCommodityCodeListRepository;
        }

        public async Task<List<CommodityCodeListDetails>> GetFilterViewResult(CommodityCodeListDetails param)
        {
            List<CommodityCodeListDetails> CommodityCodeListDetails = new List<CommodityCodeListDetails>();

            this._iCommodityCodeListRepository.UserID = this.UserID;

            return await Task.Run(() =>
            {
                return this._iCommodityCodeListRepository.GetFilterViewResult(param);
            });

            //return await Task.Run( async () =>
            //{
            //    DataTable dt = await this._iCommodityCodeListRepository.GetFilterViewResult(param);
            //    foreach (DataRow row in dt.Rows)
            //    {
            //        CommodityCodeListDetails.Add(new CommodityCodeListDetails
            //        {
            //            itemId = row["ITEM_ID"].ToString(),
            //            itemDescription = row["DESCRIPTION"].ToString(),
            //            commodityCode = row["COMMODITY_NAME"].ToString(),
            //            multiTier = ((row["IS_ENABLED"].ToString() == "Y") ? true : false),
            //            updatedBy = row["SYS_LAST_MODIFIED_BY"].ToString(),
            //            updatedDate = row["SYS_LAST_MODIFIED_DATE"].ToString(),
            //        });
            //    }
            //    return CommodityCodeListDetails;
            //});

        }

        public async Task<string> UpdateCommodityCodeListDetails(List<CommodityCodeListDetails> commodityCodeListDetails)

        {
            this._iCommodityCodeListRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._iCommodityCodeListRepository.UpdateCommodityCodeListDetails(commodityCodeListDetails);
            });
        }
    }
}
