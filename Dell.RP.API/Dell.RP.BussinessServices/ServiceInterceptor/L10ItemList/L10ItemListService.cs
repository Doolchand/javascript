﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.L10ItemList;
using Dell.RP.API.Dell.RP.Common.Models.MultiTierBOM;
using Dell.RP.API.Dell.RP.Common.Models.L10ItemList;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.L10ItemList;

namespace Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.L10ItemList
{
    public class L10ItemListService : IL10ItemList
    {
        public string UserID { get; set; }

        private readonly IL10ItemListRepository _il10ItemListRepository;

        public L10ItemListService(IL10ItemListRepository iL10ItemRepository)
        {
            this._il10ItemListRepository = iL10ItemRepository;
        }

        public async Task<List<l10Item>> Getl10FilterViewResult(l10Item item)
        {            
            this._il10ItemListRepository.UserID = this.UserID;

            return await Task.Run(() =>
            {
                return this._il10ItemListRepository.Getl10FilterViewResult(item);
            });
        }

        public async Task<List<l10Item>> Getl10FilterViewResultExcel(l10Item item)
        {
            this._il10ItemListRepository.UserID = this.UserID;

            return await Task.Run(() =>
            {
                return this._il10ItemListRepository.Getl10FilterViewResultExcel(item);
            });
        }

        public async Task<List<l10Item>> Getl10ItemList()
        {
            List<l10Item> l10ItemList = new List<l10Item>();
            this._il10ItemListRepository.UserID = this.UserID;
            return await Task.Run(async () =>
            {
                l10ItemList = await _il10ItemListRepository.Getl10ItemList();
                return l10ItemList;
            });
        }

        public async Task<string> Updatel10ItemListDetails(List<l10Item> l10ItemList)
        {
            this._il10ItemListRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._il10ItemListRepository.Updatel10ItemListDetails(l10ItemList);
            });
        }

        public Task<List<l10Item>> Getl10ItemListDetailsExcel()
        {
            throw new NotImplementedException();
        }
    }
}
