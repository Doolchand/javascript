﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.DemandSplit;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.RoutingRules;
using Dell.RP.API.Dell.RP.Common.DemandSplit;
using Dell.RP.API.Dell.RP.Common.Models.DemandSplit;
using Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.Common.Models.RoutingRules;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.DemandSplit;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.RoutingRules;
using Dell.RP.BussinessServices.IServiceInterceptor;

namespace Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.RoutingRules
{
    public class RoutingRulesService: IRoutingRules
    {

        private readonly IRoutingRulesRepository _IRoutingRulesRepository;

        public RoutingRulesService(IRoutingRulesRepository iRoutingRulesRepository)
        {
            this._IRoutingRulesRepository = iRoutingRulesRepository;
        }

        string IServiceHelper.UserID { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public async Task<List<ItemType>> GetGeographyData()
        {

            return await Task.Run(() =>
            {
                return this._IRoutingRulesRepository.GetGeographyData();
            });
        }

        public async Task<List<ItemType>> GetManufacturingSiteData(string fulfillment)
        {
            return await Task.Run(() =>
            {
                return this._IRoutingRulesRepository.GetManufacturingSiteData(fulfillment);
            });
        }

       

        public async Task<List<ItemType>> GetFulfilment()
        {

            return await Task.Run(() =>
            {
                return this._IRoutingRulesRepository.GetFulfilment();
            });
        }
        public async Task<List<ItemType>> GetDate()
        {

            return await Task.Run(() =>
            {
                return this._IRoutingRulesRepository.GetDate();
            });
        }


        public async Task<List<ItemType>> GetFGA()
        {

            return await Task.Run(() =>
            {
                return this._IRoutingRulesRepository.GetFGA();
            });
        }

        


        public async Task<List<ItemType>> GetEffectiveEndDate()
        {

            return await Task.Run(() =>
            {
                return this._IRoutingRulesRepository.GetEffectiveEndDate();
            });
        }



        public async Task<List<ItemType>> GetRoutingDropdownvalue(string ItemTypeId, string ItemTypeName)
        {

            return await Task.Run(() =>
            {
                return this._IRoutingRulesRepository.GetRoutingDropdownvalue(ItemTypeId, ItemTypeName);
            });
        }




        public  DataTable searchRoutingRules(string ID, string type, string strRegion)
        {
                return this._IRoutingRulesRepository.searchRoutingRules(ID, type, strRegion);
        }

 

        public DataTable GetRoutingRuladdupdateGrid(string strRoutingId, string strFulfillmentSiteID)
        {
            return this._IRoutingRulesRepository.GetRoutingRuladdupdateGrid(strRoutingId, strFulfillmentSiteID);
        }



        public bool DeleteRoutingRules(List<ItemType> sitesvalue)
        {
            return _IRoutingRulesRepository.DeleteRoutingRules(sitesvalue);
        }





        public DataTable searchRoutings(string strProductId, string strFgaId, out string strType)
        {
            return this._IRoutingRulesRepository.searchRoutings(strProductId, strFgaId, out strType);
        }



      //  bool ValidateRoutingDates(string oldRoutingStartDate, string oldRoutingEndDate, string newRoutingStartDate, string newRoutingEndDate);


        public bool ValidateRoutingDates(string oldRoutingStartDate, string oldRoutingEndDate, string newRoutingStartDate, string newRoutingEndDate)
        {
            return this._IRoutingRulesRepository.ValidateRoutingDates(oldRoutingStartDate, oldRoutingEndDate, newRoutingStartDate, newRoutingEndDate);
        }

        public bool addRoutingRule(RoutingRule routingRule, string updatedBy)
        {
            return _IRoutingRulesRepository.addRoutingRule(routingRule, updatedBy);
        }



        public string CopyRoutingRule(string copyingRoutingId, string newRoutingId, string itemType, string itemId, string user)
        {
            return _IRoutingRulesRepository.CopyRoutingRule(copyingRoutingId, newRoutingId, itemType, itemId, user);
        }



    }
}
