﻿using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.SourcingRule1;
using Dell.RP.API.Dell.RP.Common.Models;
using Dell.RP.API.Dell.RP.Common.Models.SourcingRule;
using Dell.RP.API.Dell.RP.Common.Models.SourcingRule1;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.SourcingRule1;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.SourcingRule1
{
    public class SourcingRuleService : ISourcingRuleService
    {
        #region Constructor and Local Variables
        private readonly ISourcingRuleRepository iSourcingRuleRepository;
        public string UserID { get; set; }

        public SourcingRuleService(ISourcingRuleRepository iSourcingRule)
        {
            this.iSourcingRuleRepository = iSourcingRule;
            this.iSourcingRuleRepository.UserID = this.UserID;
        }
        #endregion

        public async Task<List<DropDown>> GetChannelProducts()
        {
            this.iSourcingRuleRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this.iSourcingRuleRepository.GetChannelProducts();
            });
        }

        public async Task<List<Site>> GetSites()
        {
            this.iSourcingRuleRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this.iSourcingRuleRepository.GetSites();
            });
        }

        public async Task<List<DropDown>> GetChannelGeo()
        {
            this.iSourcingRuleRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this.iSourcingRuleRepository.GetChannelGeo();
            });
        }

        public async Task<string> GetProductTreeData(string strString)
        {
            this.iSourcingRuleRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                List<ProductTree> prodDatas = new List<ProductTree>();
                prodDatas = this.iSourcingRuleRepository.GetProductTreeData(strString).Result;
                return GetProductJsonDataWithoutChidName(prodDatas);
            });
        }

        public string GetProductJsonDataWithoutChidName(List<ProductTree> prodDatas)
        {
            //List<GeoData> geoDatas = GetGeoData();

            var ProductHierarchy = from l1 in prodDatas
                                   group l1 by new { id = l1.AllProductId, name = l1.AllProduct, level = "0" } into group1
                                   select new
                                   {
                                       id = group1.Key.id + "__" + group1.Key.level,
                                       name = group1.Key.name,
                                       children = from l2 in group1
                                                  group l2 by new { id = l2.LOBProdId, name = l2.LOBProd + "(" + l2.LOBProdId + ")", level = "1" } into group2
                                                  select new
                                                  {
                                                      id = group2.Key.id + "__" + group2.Key.level,
                                                      name = group2.Key.name,
                                                      children = from l3 in group2
                                                                 group l3 by new { id = l3.FamilyParentId, name = l3.FamilyParent + "(" + l3.FamilyParentId + ")", level = "2" } into group3
                                                                 select new
                                                                 {
                                                                     id = group3.Key.id + "__" + group3.Key.level,
                                                                     name = group3.Key.name,
                                                                     children = from l4 in group3
                                                                                group l4 by new { id = l4.BaseProdId, name = l4.BaseProd + "(" + l4.BaseProdId + ")", level = "3" } into group4
                                                                                select new
                                                                                {
                                                                                    id = group4.Key.id + "__" + group4.Key.level,
                                                                                    name = group4.Key.name

                                                                                    //children = from l5 in group3
                                                                                    //           select new { id = l5.CountryId + "__4", name = l5.Country + "(" + l5.CountryId + ")" }
                                                                                }
                                                                 }
                                                  }
                                   };
            return JsonConvert.SerializeObject(ProductHierarchy);

        }
        public async Task<List<SourcingRuleGrid>> GetSourcingRuleDetailsAsync(SourcingRuleFilter sourcingRuleFilter)
        {
            //this.iSourcingRuleRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this.iSourcingRuleRepository.GetSourcingRuleDetails(sourcingRuleFilter);
            });
        }

        public async Task<Dictionary<string, SourcingRuleLineItem>> GetSourcingRuleLineItems(string startChannelId, string startProductId)
        {
            return await Task.Run(() =>
            {
                return this.iSourcingRuleRepository.GetSourcingRuleLineItems(startChannelId, startProductId);
            });
        }

        public async Task<List<SourcingRule>> GetSourcingRules(string parentProductSearchFilter, string parentChannelSearchFilter)
        {
            return await Task.Run(() =>
            {
                return this.iSourcingRuleRepository.GetSourcingRules( parentProductSearchFilter,  parentChannelSearchFilter);
            });
        }
        public async Task<List<SourcingRule>> GetAllSourcingRules()
        {
            return await Task.Run(() =>
            {
                return this.iSourcingRuleRepository.GetAllSourcingRules();
            });
        }

        public async Task<Dictionary<string, SourcingRuleLineItem>> GetSingleRuleLineItems(string ChannelId, string ProductId)
        {
            return await Task.Run(() =>
            {
                return this.iSourcingRuleRepository.GetSingleRuleLineItems(ChannelId, ProductId);
            });
        }

        public async Task<int> DeleteSourcingRuleCollection(string productId, string channelId)
        {
            return await Task.Run(() =>
            {
                return this.iSourcingRuleRepository.DeleteSourcingRuleCollection(productId, channelId);
            });
        }

        public async Task<int> AddSourcingRuleCollection(SourcingRuleSaveClass ruleToSave)
        {
            iSourcingRuleRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this.iSourcingRuleRepository.AddSourcingRuleCollection(ruleToSave);
            });
        }

    }
}
