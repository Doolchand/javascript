﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.MultiTierBOM;
using Dell.RP.API.Dell.RP.Common.Models.MultiTierBOM;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.MultiTierBOM;

namespace Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.MultiTierBOM
{
    public class MultiTierBOMService: IMultiTierBOM
    {
        public string UserID { get; set; }

        private readonly IMultiTierBOMRepository _iMultiTierBOMRepository;

        public MultiTierBOMService(IMultiTierBOMRepository iMultiTierBOMRepository)
        {
            this._iMultiTierBOMRepository = iMultiTierBOMRepository;
            this.UserID = "Loona_Ishmita";
        }

        public async Task<List<MultiTierBOMDetails>> GetFilterViewResult(MultiTierBOMDetails param, int fromNumber, int count)
        {
            List<MultiTierBOMDetails> multiTierBOMDetails = new List<MultiTierBOMDetails>();

            this._iMultiTierBOMRepository.UserID = this.UserID;

            return await Task.Run(() =>
            {
                return this._iMultiTierBOMRepository.GetFilterViewResult(param, fromNumber, count);
            });

            //return await Task.Run( async () =>
            //{
            //    DataTable dt = await this._iMultiTierBOMRepository.GetFilterViewResult(param);
            //    foreach (DataRow row in dt.Rows)
            //    {
            //        multiTierBOMDetails.Add(new MultiTierBOMDetails
            //        {
            //            itemId = row["ITEM_ID"].ToString(),
            //            itemDescription = row["DESCRIPTION"].ToString(),
            //            commodityCode = row["COMMODITY_NAME"].ToString(),
            //            multiTier = ((row["IS_ENABLED"].ToString() == "Y") ? true : false),
            //            updatedBy = row["SYS_LAST_MODIFIED_BY"].ToString(),
            //            updatedDate = row["SYS_LAST_MODIFIED_DATE"].ToString(),
            //        });
            //    }
            //    return multiTierBOMDetails;
            //});

        }

        public async Task<List<CommodityCodeItem>> GetCommodityCodeDDL()
        {
            List<CommodityCodeItem> commodityCodeList = new List<CommodityCodeItem>();
            this._iMultiTierBOMRepository.UserID = this.UserID;
            return await Task.Run(async () =>
            {
                DataTable dtCommodityCode = await _iMultiTierBOMRepository.GetCommodityCodeDDL();
                foreach (DataRow rowCommodityCode in dtCommodityCode.Rows)
                {
                    commodityCodeList.Add(new CommodityCodeItem { id = rowCommodityCode[0].ToString(), itemName = rowCommodityCode[1].ToString() });
                }
                return commodityCodeList;
            });
        }

        public async Task<string> UpdateMultiTierBOMDetails(List<MultiTierBOMDetails> multiTierDetails)
        {
            this._iMultiTierBOMRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._iMultiTierBOMRepository.UpdateMultiTierBOMDetails(multiTierDetails);
            });
        }
        

        public async Task<List<MultiTierBOMDetails>> GetMultiTierViewResultExcelAsync(MultiTierBOMDetails item)
        {
            this._iMultiTierBOMRepository.UserID = this.UserID;

            return await Task.Run(() =>
            {
                return this._iMultiTierBOMRepository.GetMultiTierViewResultExcel(item);
            });
        }

    }
}
