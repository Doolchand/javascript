﻿using Dell.RP.BussinessServices.IServiceInterceptor.Role_Menu_Mgmt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dell.RP.Common.Models.Role_Menu_Mgmt;
using Dell.RP.DataAccess.IRepository.Role_Menu_Mgmt;

namespace Dell.RP.BussinessServices.ServiceInterceptor.Role_Menu_Mgmt
{
   public class RoleService : IRoleService
    {
        private readonly IRoleRepositry _iRoleRepositry;
        public string UserID { get; set; }

        public RoleService(IRoleRepositry iRoleRepositry)
        {
            this._iRoleRepositry = iRoleRepositry; 
        }

        public async Task<List<Role>> GetRoles()
        {
            this._iRoleRepositry.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._iRoleRepositry.GetRoles();
            });
        }

    }
}
