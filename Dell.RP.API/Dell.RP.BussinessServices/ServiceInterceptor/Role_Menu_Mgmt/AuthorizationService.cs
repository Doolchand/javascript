﻿using Dell.RP.Common.Models.Role_Menu_Mgmt;
using Dell.RP.DataAccess.IRepository.Role_Menu_Mgmt;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Dell.RP.BussinessServices.IServiceInterceptor.Role_Menu_Mgmt
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly IAuthorizationRepository iAuthorizationRepository;
        public AuthorizationService(IAuthorizationRepository iAuthorizationRepository)
        {
            this.iAuthorizationRepository = iAuthorizationRepository;
        }
        public string UserID { get; set; }

        public async Task<List<AuthList>> GetRoleList(string userType)
        {
            this.iAuthorizationRepository.UserID = this.UserID;
            List<AuthList> listRoleList = new List<AuthList>();
            return await Task.Run(async () =>
            {            
                DataTable dtRoleList = await this.iAuthorizationRepository.GetRoleList(userType);
                foreach (DataRow rowrl in dtRoleList.Rows)
                {
                    listRoleList.Add(new AuthList { id = rowrl["ROLE_ID"].ToString(), itemName = rowrl["ROLE_NAME"].ToString() });
                }
                return listRoleList;

            });
        }
        public async Task<List<AuthList>> GetUserGroupList()
        {
            this.iAuthorizationRepository.UserID = this.UserID;
            List<AuthList> listUserGroupList = new List<AuthList>();
            return await Task.Run(async () =>
            {
                DataTable dtUserGroupList = await this.iAuthorizationRepository.GetUserGroupList();
                foreach (DataRow rowugl in dtUserGroupList.Rows)
                {
                    listUserGroupList.Add(new AuthList { id = rowugl[0].ToString(), itemName = rowugl[1].ToString() });
                }
                return listUserGroupList;
            });
        }
        public async Task<List<AuthList>> GetUserNameList(string tenant, List<AuthList> region)
        {
            this.iAuthorizationRepository.UserID = this.UserID;
            List<AuthList> listUserNameList = new List<AuthList>();
            return await Task.Run(async () =>
            {
                DataTable dtUserNameList = await this.iAuthorizationRepository.GetUserNameList(tenant, region);
                foreach (DataRow rownl in dtUserNameList.Rows)
                {
                    listUserNameList.Add(new AuthList { id = rownl["USER_ID"].ToString(), itemName = rownl["USER_NAME"].ToString() });
                }
                return listUserNameList;
            });
        }
        public async Task<List<string>> GetRegionList()
        {
            this.iAuthorizationRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this.iAuthorizationRepository.GetRegionList();
            });
        }
        public async Task<List<Role>> GetUserRoles()
        {
            this.iAuthorizationRepository.UserID = this.UserID;
            List<Role> listUserRoles = new List<Role>();
            return await Task.Run(async () =>
            {
               
                DataTable dtUserRoles = await this.iAuthorizationRepository.GetUserRoles();
                foreach (DataRow rowur in dtUserRoles.Rows)
                {
                    listUserRoles.Add(new Role { RoleId = int.Parse(rowur["ROLE_ID"].ToString()), RoleName = rowur["ROLE_NAME"].ToString(), UserType = rowur["user_type"].ToString() });
                }
                return listUserRoles;
            });
        }
        public async Task<List<UserDetails>> GetUserDetails(FilterDetails filters)
        {
            this.iAuthorizationRepository.UserID = this.UserID;
            List<UserDetails> listUserDetails = new List<UserDetails>();
            return await Task.Run(async () =>
            {
                DataTable dtUserDetails = await this.iAuthorizationRepository.GetUserDetails(filters);
                foreach (DataRow rowud in dtUserDetails.Rows)
                {
                    listUserDetails.Add(new UserDetails
                    {
                        UserId = rowud["USER_ID"].ToString(),
                        UserName = rowud["USER_NAME"].ToString(),
                        UserDomain = rowud["USER_DOMAIN"].ToString(),
                        Region = rowud["REGION"].ToString(),
                        UserEmail = rowud["USER_EMAIL"].ToString(),
                        UserRole = rowud["ROLE_NAME"].ToString(),
                        UserGroup = rowud["GROUP_NAME"].ToString(),
                        UserType = rowud["USER_TYPE"].ToString(),
                        Lobs = rowud["LOB"].ToString(),
                        IsActive = rowud["IS_ACTIVE"].ToString() == "Y" ? true : false,
                        UpdatedBy = rowud["SYS_LAST_MODIFIED_BY"].ToString(),
                        UpdatedDate = rowud["SYS_LAST_MODIFIED_DATE"].ToString(),

                    });
                }
                return listUserDetails;
            });
        }
        public async Task<string> AddUsers(UserDetails user)
        {
            this.iAuthorizationRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this.iAuthorizationRepository.AddUsers(user);
            });
        }
        public async Task<string> UpdateUsers(List<UserDetails> user)
        {
            this.iAuthorizationRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this.iAuthorizationRepository.UpdateUsers(user);
            });
        }
        public async Task<List<ScreenAuthorization>> GetScreenAuthorizationList()
        {
            this.iAuthorizationRepository.UserID = this.UserID;
            List<ScreenAuthorization> listScreen = new List<ScreenAuthorization>();
            return await Task.Run(async () =>
            {               
                DataTable dtScreen = await this.iAuthorizationRepository.GetScreenAuthorizationList();
                foreach (DataRow rowsrn in dtScreen.Rows)
                {
                    listScreen.Add(new ScreenAuthorization { FunctionId = rowsrn["FUNCTION_ID"].ToString(), FunctionName = rowsrn["FUNCTION_NAME"].ToString(), RoleId = rowsrn["ROLE_ID"].ToString() });
                }
                return listScreen;
            });
        }
        public async Task<List<AuthList>> GetUsertype()
        {
            this.iAuthorizationRepository.UserID = this.UserID;
            List<AuthList> listUsertype = new List<AuthList>();
            return await Task.Run(async () =>
            {
                DataTable dtUsertype = await this.iAuthorizationRepository.GetUsertype();
                foreach (DataRow rowut in dtUsertype.Rows)
                {
                    listUsertype.Add(new AuthList { id = rowut["SCENARIO_ID"].ToString(), itemName = rowut["PROPVALUE"].ToString() });
                }
                return listUsertype;
            });
        }
        public async Task<List<AuthList>> GetLOB()
        {
            this.iAuthorizationRepository.UserID = this.UserID;
            List<AuthList> listLOB = new List<AuthList>();
            return await Task.Run(async () =>
            {
                DataTable dtLOB = await this.iAuthorizationRepository.GetLOB();
                foreach (DataRow rowl in dtLOB.Rows)
                {
                    listLOB.Add(new AuthList { id = rowl["LOB_CODE"].ToString(), itemName = rowl["LOB"].ToString() });
                }
                return listLOB;
            });
        }

        public async Task<List<string>> GetLoggedInUserRegionBasedOnRoleAsync(LoggedUserDetails user)
        {
            List<string> listRegionBasedOnRole = new List<string>();
            return await Task.Run(async () =>
            {               
                DataTable dtRegionBasedOnRole = await this.iAuthorizationRepository.GetLoggedInUserRegionBasedOnRoleAsync(user);
                foreach (DataRow rowrb in dtRegionBasedOnRole.Rows)
                {
                    listRegionBasedOnRole.Add(rowrb[0].ToString());
                }
                return listRegionBasedOnRole;
            });
        }
        public async Task<string> GetRoleBasedRegion(string screenId,string userType)
        {
            this.iAuthorizationRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this.iAuthorizationRepository.GetRoleBasedRegion(screenId, userType);
            });
        }

        public async Task<List<ScreenAccessRole>> GetRolesCanAccessScreenList()
        {
            this.iAuthorizationRepository.UserID = this.UserID;
            List<ScreenAccessRole> listAccessScreenList = new List<ScreenAccessRole>();
            return await Task.Run(async () =>
            {                
                DataTable dtAccessScreenList = await this.iAuthorizationRepository.GetRolesCanAccessScreenList();
                foreach (DataRow rowsl in dtAccessScreenList.Rows)
                {
                    listAccessScreenList.Add(new ScreenAccessRole { ScreenId = rowsl["FUNCTION_ID"].ToString(), Roles = rowsl["SCREEN_ACCESS_ID"].ToString() });
                }
                return listAccessScreenList;
            });
        }

    }
}
