﻿using Dell.RP.BussinessServices.IServiceInterceptor.Role_Menu_Mgmt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dell.RP.Common.Models.Role_Menu_Mgmt;
using Dell.RP.DataAccess.IRepository.Role_Menu_Mgmt;

namespace Dell.RP.BussinessServices.ServiceInterceptor.Role_Menu_Mgmt
{
   public class MenuService : IMenuService
    {
        private readonly IMenuRepositry _iMenuRepository;
        public string UserID { get; set; }

        public MenuService(IMenuRepositry iMenuRepositry)
        {
            this._iMenuRepository = iMenuRepositry; 
        }

        public async Task<List<Menu>> GetMenus()
        {
            this._iMenuRepository.UserID = this.UserID;
            return await Task.Run(() =>
            {
                return this._iMenuRepository.GetMenus();
            });
        }
    }
}
