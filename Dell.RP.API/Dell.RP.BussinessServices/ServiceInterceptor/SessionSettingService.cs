﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dell.RP.Common.Models;
using Dell.RP.BussinessServices.IServiceInterceptor;
using Dell.RP.DataAccess.IRepository;

namespace Dell.RP.BussinessServices.ServiceInterceptor
{
    public class SessionSettingService : ISessionSettingService
    {
        private readonly ISessionSettingRepository iSessionSettingRepository;

        public SessionSettingService(ISessionSettingRepository iSessionSettingRepository)
        {
            this.iSessionSettingRepository = iSessionSettingRepository;
        }

        public async Task<string> GetSessionSettingsAsync(UserDetails user)
        {
            return await Task.Run(() =>
            {
                return this.iSessionSettingRepository.GetSessionSettingsAsync(user);
            });
        }

        public async Task<string> UpdateUserSessionAsync(updateUserSessionModel param)
        {
            return await Task.Run(() =>
            {
                return this.iSessionSettingRepository.UpdateUserSessionAsync(param);
            });
        }
    }
}
