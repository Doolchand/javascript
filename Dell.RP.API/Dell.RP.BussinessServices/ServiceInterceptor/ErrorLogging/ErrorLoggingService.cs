﻿using Dell.RP.BussinessServices.IServiceInterceptor.ErrorLogging;
using Dell.RP.Common.Models;
using Dell.RP.DataAccess.IRepository.ErrorLogging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Dell.RP.BussinessServices.ServiceInterceptor.ErrorLogging
{
    public class ErrorLoggingService : IErrorLoggingService
    {
        private readonly IErrorLoggingRepository iErrorLoggingRepository;
        public ErrorLoggingService(IErrorLoggingRepository iErrorLoggingRepository)
        {
            this.iErrorLoggingRepository = iErrorLoggingRepository;
        }
        public string UserID { get; set; }
        public async Task<List<ErrorImport>> GetErrorList(string procName)
        {
            this.iErrorLoggingRepository.UserID = this.UserID;
            List<ErrorImport> listErrorList = new List<ErrorImport>();
            return await Task.Run(async () =>
            {
                DataTable dtErrorList = await this.iErrorLoggingRepository.GetErrorList(procName);
                foreach (DataRow rowerr in dtErrorList.Rows)
                {
                    listErrorList.Add(new ErrorImport
                    {

                        ErrorId = int.Parse(rowerr["ERROR_ID"].ToString()),
                        ErrorReason = rowerr["ERROR_REASON"].ToString(),
                        ErrorRecord = rowerr["ERROR_MSG"].ToString(),
                        ErrorDate = DateTime.Parse(rowerr["SYS_CREATED_DATE"].ToString()).ToString()
                    });
                }
                return listErrorList;
            });
        }
    }
}
