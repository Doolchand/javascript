﻿using Dell.RP.BussinessServices.IServiceInterceptor.Utility;
using Dell.RP.Common.Models.Utility;
using Dell.RP.DataAccess.IRepository.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Dell.RP.BussinessServices.ServiceInterceptor.Utility
{
    public class UtilityService : IUtilityService
    {

        private readonly IUtilityRepository _iUtilityService;

        public UtilityService(IUtilityRepository iUtilityService)
        {
            _iUtilityService = iUtilityService;
        }
        //public string UserID
        //{
        //    get
        //    {
        //        throw new NotImplementedException();
        //    }

        //    set
        //    {
        //        throw new NotImplementedException();
        //    }
        //}
        public string UserID { get; set; }
        public async Task<List<Attributes>> GetAttributeNames(Int32 dimension,string userId)
        {
            List<Attributes> listAttributeNames = new List<Attributes>();
            return await Task.Run(async () =>
            {             
                
                DataTable dtAttributeNames = await this._iUtilityService.GetAttributeNames(dimension, userId);
                foreach (DataRow rowatr in dtAttributeNames.Rows)
                {
                    listAttributeNames.Add(new Attributes
                    {
                        AttributeCode = rowatr["Attribute_Code"].ToString(),
                        AttributeName = rowatr["Attribute_Name"].ToString()
                    });
                }
                return listAttributeNames;
            });
        }
    }
}
