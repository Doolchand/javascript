﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.DemandSplit;
using Dell.RP.API.Dell.RP.Common.DemandSplit;
using Dell.RP.API.Dell.RP.Common.Models.DemandSplit;
using Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.DemandSplit;

namespace Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.DemandSplit
{
    public class DemandSplitService : IDemandSplit
    {
        private readonly IDemandSplitRepository  _IDemandSplitRepository;

        public DemandSplitService(IDemandSplitRepository iDemandSplitRepository)
        {
            this._IDemandSplitRepository = iDemandSplitRepository;
        }

        public string UserID { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public async Task<List<ItemType>> getProduct()
        {
             
            return await Task.Run(() =>
            {
                return this._IDemandSplitRepository.getProduct();
            });
        }

        public async Task<List<ItemType>> getChennal()
        {

            return await Task.Run(() =>
            {
                return this._IDemandSplitRepository.getChennal();
            });
        }


        public async Task<List<ItemType>> getGeography()
        {

            return await Task.Run(() =>
            {
                return this._IDemandSplitRepository.getGeography();
            });
        }

        public async Task<List<SearchQuery>> GetDemandSplitViewResult(DemandSearch selectedSearch)
        {

            return await Task.Run(() =>
            {
                return this._IDemandSplitRepository.GetDemandSplitViewResult(selectedSearch);
            });
        }


        public async Task<Dictionary<string,string>> GetChannelIdSplit()
        {

            return await Task.Run(() =>
            {
                return this._IDemandSplitRepository.GetChannelIdSplit();
            });
        }

        public async Task<Dictionary<string, string>> GetGeographyIdSplit()
        {

            return await Task.Run(() =>
            {
                return this._IDemandSplitRepository.GetGeographyIdSplit();
            });
        }
        


             public async Task<Dictionary<string, string>> GetProductIdSplit()
        {

            return await Task.Run(() =>
            {
                return this._IDemandSplitRepository.GetProductIdSplit();
            });
        }

        

        public async Task<Dictionary<string, string>> GetSupplychainSplit()
        {

            return await Task.Run(() =>
            {
                return this._IDemandSplitRepository.GetSupplychainSplit();
            });
        }

        //public int DemandSplitBulkInsert(DataTable dt, string extra_ID, string userName);
        //{

        //   return this._IDemandSplitRepository.GetSupplychainSplit();

        //}


        public int DemandSplitBulkInsert  (DataTable dt, string extra_ID, string userName)
        {

                return _IDemandSplitRepository.DemandSplitBulkInsert(dt, extra_ID, userName);
            
        }




    }
}
