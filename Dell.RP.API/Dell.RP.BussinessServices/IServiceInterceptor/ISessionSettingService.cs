﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dell.RP.Common.Models;

namespace Dell.RP.BussinessServices.IServiceInterceptor
{
    public interface ISessionSettingService
    {
        Task<string> GetSessionSettingsAsync(UserDetails user);

        Task<string> UpdateUserSessionAsync(updateUserSessionModel param);
    }
}
