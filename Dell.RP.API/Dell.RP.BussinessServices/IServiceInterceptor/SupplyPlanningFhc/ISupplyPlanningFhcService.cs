﻿using Dell.RP.API.Dell.RP.Common.Models.SupplyPlanningFhc;
using Dell.RP.BussinessServices.IServiceInterceptor;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.Common.Models;
using System.Data;

namespace Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.SupplyPlanningFhc
{
    public interface ISupplyPlanningFhcService : IServiceHelper
    {
        Task<List<DropDown>> GetManSite();

        string GetRPStatus();

        List<DropDown> BindPOUILock();

        Task<List<Get_SupplyPlanningFhc_Export_Details>> GetRPSupplyPlanningFhcExportDetails(Export_Parameters exportParams);

        Task<List<DropDown>> GetFamilyParentDD();

        string InsertFHC(List<RPSupplyPlan_Import_Params> importData);

        List<Error_Grid_Details> getErrGridDetails();

        string GetLockStatusFHC(string flag);

        string GetValidRecStatusFHC();

        int UpdateAutoUnlockUIFHC();
    }
}
