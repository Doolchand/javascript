﻿using Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement;
using Dell.RP.BussinessServices.IServiceInterceptor;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.Common.Models;
using System.Data;

namespace Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.FullFillmentSiteManagement
{
    public interface IFullFillmentSiteManagement : IServiceHelper
    {
        Task<List<DropDown>> GetMasterDataData(string key);
        Task<List<DropDown>> GetAccountNameFullfilment();
        Task<List<DropDown>> GetGeoCountries();
        Task<List<DropDown>> GetChannelSegments();
        Task<string> GetGeoTreeData();
        Task<string> GetChannelTreeData();
        Task<string> GetProductTreeData(string strString);

        Task<string> GetProductFHCTreeData(string strString);
        Task<string> GetProductFGATreeData(string strString);

        Task<List<FulfillmentDetail>> GetFulFillmentSearchResult(FulFillmentSearchInput searchInput);
        Task<int> DeleteRoutingDetails(string singleDeleteFlag, string ID);
        Task<int> DeleteRoutingDetailsHeader(string singleDeleteFlag, string ID);
        Task<List<FulfillmentRuleDetails>> GetFulfillmentRetrive(FulFillmentSearchInput retrieveInput);

        Task<List<string>> GetChannelType(string channelid);

        string getRegionName(string country);
        List<string> GetNonVirtualSites(string regionCode);

        List<DropDown> GetFSAFiscalCalendarInfo_BeginDate();
        List<DropDown> GetFSAFiscalCalendarInfo_EndDate();

        bool IsVirtualSite(string siteName);
        List<RoutingDetail> retrieveRoutingDetailsByRoutingId(string routingID);

        List<RoutingDetail> retrieveRoutingDetails(FulFillmentSearchInput retrieveInput);
        bool InsertValidateEffectiveDates(FullFillmentRuleData FSAEntity);
        int AddRoutingdetails(FullFillmentRuleData Full);
        int AddFullFillmentsites(FullFillmentRuleData itFC);
        bool UpdateValidateEffectiveDates(UpdateValidateDateInputParams lstUpdateValidateParams);
        int updatefulfillment(FullFillmentRuleData updateFulfillment);
        int DeleteFulfillDetails(FullFillmentRuleData lstFulfillDeleteSite);
    }
}
