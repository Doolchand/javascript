﻿using Dell.RP.API.Dell.RP.Common.Models;
using Dell.RP.API.Dell.RP.Common.Models.SourcingRule;
using Dell.RP.API.Dell.RP.Common.Models.SourcingRule1;
using Dell.RP.BussinessServices.IServiceInterceptor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.SourcingRule1
{
    public interface ISourcingRuleService: IServiceHelper
    {
        Task<List<DropDown>> GetChannelProducts();

        Task<List<DropDown>> GetChannelGeo();

        Task<List<Site>> GetSites();

        Task<string> GetProductTreeData(string strString);

        Task<List<SourcingRuleGrid>> GetSourcingRuleDetailsAsync(SourcingRuleFilter sourcingRuleFilter);

        Task<Dictionary<string, SourcingRuleLineItem>> GetSourcingRuleLineItems(string startChannelId, string startProductId);
        Task<List<SourcingRule>> GetSourcingRules(string parentProductSearchFilter, string parentChannelSearchFilter);
        Task<Dictionary<string, SourcingRuleLineItem>> GetSingleRuleLineItems(string ChannelId, string ProductId);
        Task<List<SourcingRule>> GetAllSourcingRules();
        Task<int> DeleteSourcingRuleCollection(string productId, string channelId);
        Task<int> AddSourcingRuleCollection(SourcingRuleSaveClass ruleToSave);

    }
}
