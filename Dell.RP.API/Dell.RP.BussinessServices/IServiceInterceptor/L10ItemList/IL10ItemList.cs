﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.Common.Models.L10ItemList;
using Dell.RP.API.Dell.RP.Common.Models.MultiTierBOM;
using Dell.RP.BussinessServices.IServiceInterceptor;

namespace Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.L10ItemList
{
    public interface IL10ItemList : IServiceHelper
    {
        Task<List<l10Item>> Getl10FilterViewResult(l10Item item);

        Task<List<l10Item>> Getl10FilterViewResultExcel(l10Item item);
        Task<string> Updatel10ItemListDetails(List<l10Item> l10ItemList);
        Task<List<l10Item>> Getl10ItemList();
    }
}
