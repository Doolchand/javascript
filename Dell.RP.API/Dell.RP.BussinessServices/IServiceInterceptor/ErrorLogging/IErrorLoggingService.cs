﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dell.RP.Common.Models;

namespace Dell.RP.BussinessServices.IServiceInterceptor.ErrorLogging
{
    public interface IErrorLoggingService : IServiceHelper
    {
        Task<List<ErrorImport>> GetErrorList(string procName);
    }
}
