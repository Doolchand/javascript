﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.BussinessServices.IServiceInterceptor
{
   public interface IServiceHelper
    {
        string UserID { get; set; }
    }
}
