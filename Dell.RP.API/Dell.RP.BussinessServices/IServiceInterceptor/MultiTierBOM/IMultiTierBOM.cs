﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Dell.RP.API.Dell.RP.Common.Models.MultiTierBOM;
using Dell.RP.BussinessServices.IServiceInterceptor;

namespace Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.MultiTierBOM
{
    public interface IMultiTierBOM : IServiceHelper
    {
        Task<List<MultiTierBOMDetails>> GetFilterViewResult(MultiTierBOMDetails input, int fromNumber, int count);
        Task<string> UpdateMultiTierBOMDetails(List<MultiTierBOMDetails> input);
        Task<List<CommodityCodeItem>> GetCommodityCodeDDL();
        Task<List<MultiTierBOMDetails>> GetMultiTierViewResultExcelAsync(MultiTierBOMDetails item);
    }
}
