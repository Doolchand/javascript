﻿using Dell.RP.API.Dell.RP.Common.DemandSplit;
using Dell.RP.API.Dell.RP.Common.Models.DemandSplit;
using Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement;
using Dell.RP.BussinessServices.IServiceInterceptor;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.DemandSplit
{
    public interface IDemandSplit : IServiceHelper
    {
        Task<List<ItemType>> getProduct();
        Task<List<ItemType>> getChennal();
        Task<List<ItemType>> getGeography();
       // Task<List<ItemType>> GetChannelIdSplit();

        Task< Dictionary<string ,string>> GetChannelIdSplit();
        Task<Dictionary<string, string>> GetGeographyIdSplit();
        Task<Dictionary<string, string>> GetProductIdSplit();

        Task<Dictionary<string, string>> GetSupplychainSplit();
       // Task<Int32> DemandSplitBulkInsert();
        int DemandSplitBulkInsert(DataTable dt , string extra_ID, string userName);


        Task<List<SearchQuery>> GetDemandSplitViewResult(DemandSearch selectedSearch);



    }

 

}
