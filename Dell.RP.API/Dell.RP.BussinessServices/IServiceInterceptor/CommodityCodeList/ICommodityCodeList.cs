﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dell.RP.API.Dell.RP.Common.Models.CommodityCodeListDetails;
using Dell.RP.API.Dell.RP.Common.Models.MultiTierBOM;
using Dell.RP.BussinessServices.IServiceInterceptor;

namespace Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.CommodityCodeList
{
    public interface ICommodityCodeList : IServiceHelper
    {
        Task<List<CommodityCodeListDetails>> GetFilterViewResult(CommodityCodeListDetails input);
        Task<string> UpdateCommodityCodeListDetails(List<CommodityCodeListDetails> input);
    }
}
