﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dell.RP.Common.Models.Role_Menu_Mgmt;

namespace Dell.RP.BussinessServices.IServiceInterceptor.Role_Menu_Mgmt
{
    public interface IAuthorizationService : IServiceHelper
    {
        Task<List<AuthList>> GetRoleList(string userType);
        Task<List<AuthList>> GetUserGroupList();
        Task<List<string>> GetRegionList();
        Task<List<Role>> GetUserRoles();

        Task<List<AuthList>>GetUserNameList(string tenant, List<AuthList> region);
        Task<List<UserDetails>> GetUserDetails(FilterDetails filters);
        Task<string> AddUsers(UserDetails user);
        Task<string> UpdateUsers(List<UserDetails> user);
        Task<List<ScreenAuthorization>> GetScreenAuthorizationList();
        Task<List<string>> GetLoggedInUserRegionBasedOnRoleAsync(LoggedUserDetails user);
        Task<List<AuthList>> GetUsertype();
        Task<List<AuthList>> GetLOB();
        Task<string> GetRoleBasedRegion(string screenId, string userType);

        Task<List<ScreenAccessRole>> GetRolesCanAccessScreenList();
    }
}
