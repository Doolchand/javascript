﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dell.RP.Common.Models.Utility;

namespace Dell.RP.BussinessServices.IServiceInterceptor.Utility
{
    public interface IUtilityService : IServiceHelper
    {
        Task<List<Attributes>> GetAttributeNames(Int32 dimension,string userId);
    }
}
