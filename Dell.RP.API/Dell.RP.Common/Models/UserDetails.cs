﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.Common.Models
{
    public class updateUserSessionModel
    {
        public string UserID { get; set; }
        public string Region { get; set; }
        public string tokenID { get; set; }
        public string state { get; set; }
        public string applicationUrl { get; set; }
        public string loggedPage { get; set; }

    }
    public class sessionSettingModel
    {
        public string setIdleTime { get; set; }
        public string setTimeout { get; set; }
    }
    public class UserDetails
    {
        public string UserID { get; set; }
    }

    public class UserDetailsRole
    {
        public string UserID { get; set; }
        public string screenId { get; set; }
    }
}
