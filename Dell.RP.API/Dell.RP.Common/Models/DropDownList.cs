﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.Common.Models
{
    public  class DropDownList
    {
        public string dropDownText { get; set; }
        public string dropDownValue { get; set; }
    }
}
