﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.MultiTierBOM
{
    public class CommodityCodeItem
    {
        public string itemName { get; set; }
        public string id { get; set; }
    }
}
