﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.MultiTierBOM
{
    public class MultiTierBOMDetails
    {
        public string itemId { get; set; }
        public string commodityCode { get; set; }
        public string itemDescription { get; set; }
        public Boolean multiTier { get; set; }
        public string updatedBy { get; set; }
        public string updatedDate { get; set; }
    }
}
