﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.Common.Models.Utility
{
    public class Attributes
    {
        
            public string AttributeCode { get; set; }
            public string AttributeName { get; set; }
        
    }
}
