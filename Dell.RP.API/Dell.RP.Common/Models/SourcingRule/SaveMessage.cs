﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SourcingRule
{
    public class SaveMessage
    {
        public string Message { get; set; }

        public int NumberOfRowImpacted { get; set; }

        public bool IsSuccess { get; set; }
    }
}
