﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SourcingRule1
{
    public class SourcingRuleFilter
    {
        public string ChannelGeoId { get; set; }
        public string ChannelGeoName { get; set; }

        public string ProductId { get; set; }
        public string  ProductName { get; set; }

        public string ChannelProdId { get; set; }
        public string ChannelProdName { get; set; }
    }
}
