﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SourcingRule1
{
    public class Routing
    {
       
        public string KitLocation { get; set; }
        public string BoxLocation { get; set; }

        public string MergeLocation { get; set; }

        public double KitBoxSplit { get; set; }
        
        public double BoxMergeSplit { get; set; }
        

        public Routing(string kitLoc, string boxLoc, double kbSplit)
        {
            this.KitLocation = kitLoc;
            this.BoxLocation = boxLoc;
            this.KitBoxSplit = kbSplit;
        }

        public string addBoxMergeRoute(string boxLoc, string mergeLoc, double bmSplit)
        {
            //if (this.boxLocation.Equals(boxLoc))
            if (true)
            {
                this.MergeLocation = mergeLoc;
                this.BoxMergeSplit = bmSplit;

                return Boolean.TrueString;
            }

            //return Boolean.FalseString;
        }

        public Routing(string kitLoc, string boxLoc, string mergeLoc, double kbSplit, double bmSplit)
        {
            this.KitLocation = kitLoc;
            this.BoxLocation = boxLoc;
            this.MergeLocation = mergeLoc;
            this.KitBoxSplit = kbSplit;
            this.BoxMergeSplit = bmSplit;
        }

        public override string ToString()
        {
            return "Kitting Site: " + this.KitLocation + " KitBoxSplit: " + this.KitBoxSplit + " Boxing Site: " + this.BoxLocation + " BoxMergeSplit: " + this.BoxMergeSplit + " Merging Site: " + this.MergeLocation;
        }

        public bool isComplete()
        {
            if (!String.IsNullOrEmpty(this.KitLocation) && !String.IsNullOrEmpty(this.BoxLocation) && !String.IsNullOrEmpty(this.MergeLocation))
                return true;

            return false;
        }
    }
}
