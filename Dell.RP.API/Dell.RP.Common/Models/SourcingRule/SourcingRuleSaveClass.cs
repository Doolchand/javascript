﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SourcingRule
{
    public class SourcingRuleSaveClass
    {
        public List<SourcingSaveLineItem> ItemsList;

        public SourcingRuleSaveClass()
        {
            this.ItemsList = new List<SourcingSaveLineItem>();
        }
    }
}
