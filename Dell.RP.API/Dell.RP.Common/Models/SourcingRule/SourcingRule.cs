﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SourcingRule1
{
    public class SourcingRule
    {
     

        public string SourcingRuleId { get; set; }
        public string Channel_id { get; set; }
        public string Product_id { get; set; }
        static int arrayCounter = 0;
        public int From_cal_id { get; set; }
        public int To_cal_id { get; set; }

        public List<Routing> ListRouting { get; set; }


        Dictionary<string, Routing> hashRouting = new Dictionary<string, Routing>();

        public Dictionary<string, Routing> HashRouting
        {
            get { return hashRouting; }
            set { hashRouting = value; }
        }


        public SourcingRule(string channel_id, string product_id, int from_cal_id, int to_cal_id)
        {
            this.Channel_id = channel_id;
            this.Product_id = product_id;
            this.From_cal_id = from_cal_id;
            this.To_cal_id = to_cal_id;
        }

        public SourcingRule() {; }

        public void addRouting(Routing routing)
        {
            this.ListRouting.Add(routing);
        }
        Dictionary<string, List<RawRouting>> hashRawRoutings = new Dictionary<string, List<RawRouting>>();

        public Dictionary<string, List<RawRouting>> HashRawRoutings
        {
            get { return hashRawRoutings; }
            set { hashRawRoutings = value; }
        }


        public bool addRawRouting(string route_id, RawRouting rawRouting)
        {
            List<RawRouting> tempList;

            List<RawRouting> tempListLocal;

            bool tempStatus = HashRawRoutings.TryGetValue(route_id, out tempList);

            if (!tempStatus)
            {
                tempListLocal = new List<RawRouting>();

                tempListLocal.Add(rawRouting);

                HashRawRoutings.Add(route_id, tempListLocal);
            }
            else
            {
                tempList.Add(rawRouting);
            }


            return true;
        }

        public void processRawRoutings()
        {

            //listRouting

            IEnumerator iEnumeratorKeys = HashRawRoutings.Keys.GetEnumerator();

            List<string> listKeys = new List<string>();

            while (iEnumeratorKeys.MoveNext())
            {
                listKeys.Add(iEnumeratorKeys.Current.ToString());
            }

            IEnumerator<string> iEnumerator = listKeys.GetEnumerator();

            //IEnumerator iEnumeratorRawRoutings = hashRawRoutings.GetEnumerator();                  



            string tempRawRouteKey = String.Empty;

            string tempRouteKey = String.Empty;

            int globalDuplicateCount = 1;

            while (iEnumerator.MoveNext())
            {
                tempRawRouteKey = iEnumerator.Current;

                List<RawRouting> tempListRawRouting = null;

                if (HashRawRoutings.ContainsKey(tempRawRouteKey))
                {
                    HashRawRoutings.TryGetValue(tempRawRouteKey, out tempListRawRouting);
                }

                if (HashRawRoutings.TryGetValue(tempRawRouteKey, out tempListRawRouting))
                {
                    IEnumerator<RawRouting> iEnumeratorRawRouting = tempListRawRouting.GetEnumerator();

                    RawRouting tempRawRouting = null;

                    Routing tempRouting = null;

                    bool flag = false;

                    while (iEnumeratorRawRouting.MoveNext())
                    {
                        tempRawRouting = iEnumeratorRawRouting.Current;

                        tempRouteKey = String.Empty;

                        if (tempRawRouting.From_location_type.Equals("KITTING"))
                        {
                            tempRouting = new Routing(tempRawRouting.From_location_id, tempRawRouting.To_location_id, tempRawRouting.Split);

                            tempRouteKey = tempRawRouteKey + tempRawRouting.To_location_id;

                            HashRouting.Add(tempRouteKey, tempRouting);
                        }
                        else
                        {
                            tempRouteKey = tempRawRouteKey + tempRawRouting.From_location_id;

                            //if (flag)
                            //{ tempRouteKey += tempRawRouting.To_location_id; flag = false; }

                            Routing tempRoutingLocal = null;

                            if (HashRouting.TryGetValue(tempRouteKey, out tempRoutingLocal))
                            {
                                if (tempRoutingLocal.isComplete())
                                {
                                    tempRouting = new Routing(tempRoutingLocal.KitLocation, tempRoutingLocal.BoxLocation, tempRoutingLocal.KitBoxSplit);

                                    tempRouting.addBoxMergeRoute(tempRawRouting.From_location_id, tempRawRouting.To_location_id, tempRawRouting.Split);

                                    flag = true;

                                    tempRouteKey = tempRawRouteKey + tempRawRouting.From_location_id + tempRawRouting.To_location_id;

                                    if (!HashRouting.ContainsKey(tempRouteKey))
                                        HashRouting.Add(tempRouteKey, tempRouting);
                                    else
                                        HashRouting.Add(tempRouteKey + (globalDuplicateCount++), tempRouting);
                                }
                                else
                                {
                                    tempRoutingLocal.addBoxMergeRoute(tempRawRouting.From_location_id, tempRawRouting.To_location_id, tempRawRouting.Split);

                                }
                            }
                            else
                            {
                                //Un-Reachable Code.
                            }
                        }


                        //if (tempRouting != null)
                        //    tempRouting.addBoxMergeRoute(tempRawRouting.From_location_id, tempRawRouting.To_location_id, tempRawRouting.Split);


                    }

                }



            }


        }

        public override string ToString()
        {

            string returnString = "\n===============================================================================================\n";

            returnString += "\nSourcingRuleId: " + this.SourcingRuleId + "\n";

            returnString += "Channel_Id: " + this.Channel_id + " Product_Id: " + this.Product_id + " Start Date: " + this.From_cal_id + " End Date: " + this.To_cal_id;

            //returnString += "\n Raw Routings Count: ";

            int listCount = HashRouting.Count;

            string tempString = "The value of listCount is :" + listCount;

            returnString += tempString;

            int i = 0;

            while (i < listCount)
            {
                string tempRouteString = HashRouting.ElementAt(i).Key;

                Routing tempRouting = HashRouting.ElementAt(i).Value;

                returnString += "\n Routing: ";

                returnString += "Key: " + tempRouteString + "-----Value: " + tempRouting.ToString();

                i++;
            }


            //IEnumerator<Routing> iEnumeratorTemp = listRouting.GetEnumerator();

            //while (iEnumeratorTemp.MoveNext())
            //{
            //    returnString += ( "\n Routing: " + (iEnumeratorTemp).ToString());
            //}

            return returnString;
        }
    }
}
