﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SourcingRule
{
    public enum SourcingLineItemType
    {
        KitSite,
        BoxSite,
        MergeSite
    }
    public enum SourcingRuleType
    {
        KitToBox,
        BoxToMerge
    }
}
