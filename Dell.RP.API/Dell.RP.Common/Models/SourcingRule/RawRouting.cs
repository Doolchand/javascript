﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SourcingRule1
{
    public class RawRouting
    {
        
        public string From_location_id { get; set; }

        public string To_location_id { get; set; }

        public double Split { get; set; }
        
        public string Route_id { get; set; }

        public string From_location_type { get; set; }

        public RawRouting(string from_location_id, string to_location_id, double split, string route_id, string from_location_type)
        {
            this.From_location_id = from_location_id;
            this.To_location_id = to_location_id;
            this.Split = split;
            this.Route_id = route_id;
            this.From_location_type = from_location_type;
        }

        public override string ToString()
        {
            return "RouteId: " + this.Route_id + "FromLocation: " + this.From_location_id + " ToLocation: " + this.To_location_id + " Split: " + this.Split + " FromLocationType: " + this.From_location_type;
        }
    }
}
