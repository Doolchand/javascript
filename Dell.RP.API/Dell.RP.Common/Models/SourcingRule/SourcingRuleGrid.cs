﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SourcingRule1
{
    public class SourcingRuleGrid
    {
        public string Channel { get; set; }
        public string Product { get; set; }
        public string FromDateStr { get; set; }
        public string ToDateStr { get; set; }
        public string KitSite { get; set; }
        public string BoxSite { get; set; }
        public string BoxSplit { get; set; }
        public string MergeSite { get; set; }
        public string MergeSplit { get; set; }
       public string SourcingRuleKey { get; set; }
        public string SysLastModifiedby { get; set; }
        public string SysLastModifiedDate { get; set; }

        #region commented Code
        
        ////           EditRule CopyRule
        //public string ProductId { get; set; }       
        //public string ProductName { get; set; }

        //public string ChannelId { get; set; }
        //public string ChannelName { get; set; }
        //public string FromCalId { get; set; }
        //public string FromDate { get; set; }
        //public string ToCalId { get; set; }
        //public string ToDate { get; set; }

        //public string Kitting { get; set; }

        //public string Boxing { get; set; }

        //public string KbSplit { get; set; }
        //public string Merging { get; set; }

        //public string BmSplit { get; set; }

        //public string LastModifiedBy { get; set; }

        //public string LastModifiedDate { get; set; }

        #endregion
    }
}
