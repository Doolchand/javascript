﻿using Dell.RP.API.Dell.RP.Common.Models.SourcingRule1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SourcingRule
{
    public class SourcingSaveLineItem : SourcingRuleLineItem
    {
        public SourcingLineItemType ItemType;

        protected SourcingSaveLineItem()
        {
            this.ItemType = SourcingLineItemType.KitSite;
        }

        public SourcingSaveLineItem(string prodID, string prodName, string chanID, string chanName) : base(prodID, prodName, chanID, chanName)
        {
            this.ItemType = SourcingLineItemType.KitSite;
        }

        public SourcingSaveLineItem(SourcingRuleLineItem item) : base(item)
        {
            this.ItemType = SourcingLineItemType.KitSite;
        }
    }
}
