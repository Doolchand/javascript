﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SourcingRule
{
    public class Site
    {       
        public string SiteName { get; set; }
        public string SiteId { get; set; }
        public string LocationGroupId { get; set; }
        public string LocationGroupType { get; set; }
    }
}
