﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SourcingRule1
{
    public class ProductTree
    {
        public string AllProduct { get; set; }
        public string AllProductId { get; set; }

        public string LOBProd { get; set; }
        public string LOBProdId { get; set; }

        public string FamilyParent { get; set; }
        public string FamilyParentId { get; set; }

        public string BaseProd { get; set; }
        public string BaseProdId { get; set; }
    }
}
