﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SourcingRule1
{
    public class SourcingRuleLineItem
    {
        private const string keyAppender = "_";

        private string productID;

        private string productName;

        private string channelId;

        private string channelName;

        private DateTime fromDate;

        private string fromDateCalId = string.Empty;

        private DateTime toDate;

        private string toDateCalId = string.Empty;

        private string kitSite;

        private string boxSite;

        private string mergeSite;

        private int boxSplit;

        private int mergeSplit;

        private string parentRouteID;

        private string routeID;

        private string sysLastModifiedby;

        private string sysLastModifiedDate;

        public bool IsNewRule = false;

        public bool IsNewKit = false;

        public bool IsNewBox = false;

        public bool IsNewMerge = false;

        public string Channel
        {
            get
            {
                string str = string.Concat(this.channelName, "(", this.channelId, ")");
                return str;
            }
        }

        //    public string Channel { get; set; }
        public string ChannelId
        {
            get
            {
                return this.channelId;
            }
            set
            {
                this.channelId = value;
            }
        }

        public string ChannelName
        {
            get
            {
                return this.channelName;
            }
            set
            {
                this.channelName = value;
            }
        }

        public bool ChannelVisible { get; set; } = true;

        public string Product
        {
            get
            {
                string str = string.Concat(this.productName, "(", this.productID, ")");
                return str;
            }
        }
       // public string Product { get; set; }

        public string ProductID
        {
            get
            {
                return this.productID;
            }
            set
            {
                this.productID = value;
            }
        }

        public string ProductName
        {
            get
            {
                return this.productName;
            }
            set
            {
                this.productName = value;
            }
        }

        public bool ProductVisible { get; set; } = true;
        public DateTime FromDate
        {
            get
            {
                return this.fromDate;
            }
            set
            {
                this.fromDate = value;
            }
        }

        public string FromDateCalId
        {
            get
            {
                return this.fromDateCalId;
            }
            set
            {
                this.fromDateCalId = value;
            }
        }

        public string FromDateStr { get; set; }

        public bool FromDateVisible { get; set; } = true;

        public DateTime ToDate
        {
            get
            {
                return this.toDate;
            }
            set
            {
                this.toDate = value;
            }
        }

        public string ToDateCalId
        {
            get
            {
                return this.toDateCalId;
            }
            set
            {
                this.toDateCalId = value;
            }
        }

        public string ToDateStr { get; set; }

        public bool ToDateVisible { get; set; } = true;
        public string KitSite
        {
            get
            {
                return this.kitSite;
            }
            set
            {
                this.kitSite = value;
            }
        }
        public bool KitSiteVisible { get; set; } = true;
        public string BoxSite
        {
            get
            {
                return this.boxSite;
            }
            set
            {
                this.boxSite = value;
            }
        }
        public bool BoxSiteVisible { get; set; } = true;
        public int BoxSplit
        {
            get
            {
                return this.boxSplit;
            }
            set
            {
                this.boxSplit = value;
            }
        }
        public bool BoxSplitVisible { get; set; } = true;

        public string MergeSite
        {
            get
            {
                return this.mergeSite;
            }
            set
            {
                this.mergeSite = value;
            }
        }
        public bool MergeSiteVisible { get; set; } = true;
        public int MergeSplit
        {
            get
            {
                return this.mergeSplit;
            }
            set
            {
                this.mergeSplit = value;
            }
        }
        public bool MergeSplitVisible { get; set; } = true;

        public string ParentRouteID
        {
            get
            {
                return this.parentRouteID;
            }
            set
            {
                this.parentRouteID = value;
            }
        }     

        public string RouteID
        {
            get
            {
                return this.routeID;
            }
            set
            {
                this.routeID = value;
            }
        }

        public int RuleIndex
        {
            get;
            set;
        }

        public string SourcingRuleKey
        {
            get
            {
                string[] strArrays = new string[] { this.channelId, "_", this.productID, "_", this.fromDateCalId, "_", this.toDateCalId };
                return string.Concat(strArrays);
            }
        }

        public string SysLastModifiedby
        {
            get
            {
                return this.sysLastModifiedby;
            }
            set
            {
                this.sysLastModifiedby = value;
            }
        }

        public string SysLastModifiedDate
        {
            get
            {
                return this.sysLastModifiedDate;
            }
            set
            {
                this.sysLastModifiedDate = value;
            }
        }

        protected SourcingRuleLineItem()
        {
        }

        public bool EditButtonVisible { get; set; } = true;
        public bool DeleteButtonVisible { get; set; } = true;
        public bool CopyButtonVisible { get; set; } = true;

        public SourcingRuleLineItem(string prodID, string prodName, string chanID, string chanName)
        {
            this.productID = prodID;
            this.productName = prodName;
            this.channelId = chanID;
            this.channelName = chanName;
        }

        public SourcingRuleLineItem(SourcingRuleLineItem item)
        {
            this.productID = item.productID;
            this.productName = item.productName;
            this.channelId = item.channelId;
            this.channelName = item.channelName;
            this.boxSite = item.boxSite;
            this.boxSplit = item.boxSplit;
            this.fromDate = item.fromDate;
            this.fromDateCalId = item.fromDateCalId;
            this.kitSite = item.kitSite;
            this.mergeSite = item.mergeSite;
            this.mergeSplit = item.mergeSplit;
            this.parentRouteID = item.parentRouteID;
            this.productID = item.productID;
            this.productName = item.productName;
            this.routeID = item.routeID;
            this.toDate = item.toDate;
            this.toDateCalId = item.toDateCalId;
            this.sysLastModifiedby = item.sysLastModifiedby;
            this.sysLastModifiedDate = item.sysLastModifiedDate;
        }

        public override bool Equals(object obj)
        {
            bool flag;
            if (obj == null)
            {
                flag = false;
            }
            else if (!obj.GetType().Equals(this.GetType()))
            {
                flag = false;
            }
            else
            {
                SourcingRuleLineItem sourcingRuleLineItem = (SourcingRuleLineItem)obj;
                flag = ((this.productID == null || this.channelId == null || this.fromDateCalId == null || this.toDateCalId == null || this.kitSite == null || this.boxSite == null || this.mergeSite == null || sourcingRuleLineItem.productID == null || sourcingRuleLineItem.channelId == null || sourcingRuleLineItem.fromDateCalId == null || sourcingRuleLineItem.toDateCalId == null || sourcingRuleLineItem.kitSite == null || sourcingRuleLineItem.boxSite == null || sourcingRuleLineItem.mergeSite == null || !this.productID.Equals(sourcingRuleLineItem.productID) || !this.channelId.Equals(sourcingRuleLineItem.channelId) || !this.fromDateCalId.Equals(sourcingRuleLineItem.fromDateCalId) || !this.toDateCalId.Equals(sourcingRuleLineItem.toDateCalId) || !this.kitSite.Equals(sourcingRuleLineItem.kitSite) || !this.boxSite.Equals(sourcingRuleLineItem.boxSite) || !this.mergeSite.Equals(sourcingRuleLineItem.mergeSite) || !this.sysLastModifiedby.Equals(sourcingRuleLineItem.sysLastModifiedby) ? true : !this.sysLastModifiedDate.Equals(sourcingRuleLineItem.sysLastModifiedDate)) ? false : true);
            }
            return flag;
        }

        public override int GetHashCode()
        {
            int hashCode = this.productID.GetHashCode() + this.channelId.GetHashCode() + this.fromDateCalId.GetHashCode() + this.toDateCalId.GetHashCode() + this.kitSite.GetHashCode() + this.boxSite.GetHashCode() + this.mergeSite.GetHashCode() + this.boxSplit.GetHashCode() + this.mergeSplit.GetHashCode() + this.sysLastModifiedby.GetHashCode() + this.sysLastModifiedDate.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            object[] str = new object[] { this.productName, this.channelName, this.fromDate.ToString(), this.toDate.ToString(), this.kitSite, this.boxSite, this.mergeSite, this.routeID, this.parentRouteID, this.sysLastModifiedby, this.sysLastModifiedDate };
            return string.Format("Prod: {0}, Chan: {1}, fromDate: {2}, toDate: {3}, kitSite: {4}, boxSite: {5}, mergeSIte: {6}, routeID: {7}, parentRouteID: {8},sysLastModifiedby:{9},sysLastModifiedDate:{10} ", str);
        }

        #region Code commented

        //private const string keyAppender = "_";

        //public string ProductID { get; set; }

        //public string ProductName { get; set; }

        // public string Product
        //{
        //    get { return this.ProductName + "(" + this.ProductID + ")"; }
        //}

        //public string ChannelId { get; set; }
        //public string ChannelName { get; set; }

        //public string Channel
        //{
        //    get { return this.ChannelName + "(" + this.ChannelId + ")"; }
        //}

        //public DateTime FromDate { get; set; }


        //public string FromDateStr
        //{
        //    get
        //    {
        //        if (this.FromDateCalId.Equals(string.Empty))
        //            return this.FromDateCalId;
        //        else
        //            return this.FromDate.ToShortDateString();
        //    }
        //}


        //public string FromDateCalId { get; set; }

        //public DateTime ToDate { get; set; }

        //public string ToDateStr
        //{
        //    get
        //    {
        //        if (this.ToDateCalId.Equals(string.Empty))
        //            return this.ToDateCalId;
        //        else
        //            return this.ToDate.ToShortDateString();
        //    }
        //}

        //public string ToDateCalId { get; set; }
        //public string KitSite { get; set; }

        //public string BoxSite { get; set; }


        //public string MergeSite { get; set; }

        //public int BoxSplit { get; set; }

        //public int MergeSplit { get; set; }

        //public string ParentRouteID { get; set; }

        //public string RouteID { get; set; }


        //public string SourcingRuleKey
        //{
        //    get
        //    {
        //        return this.ChannelId + keyAppender + this.ProductID + keyAppender + this.FromDateCalId + keyAppender + this.ToDateCalId;
        //    }
        //}

        ////hide default constructor
        //private SourcingRuleLineItem() { }

        //public SourcingRuleLineItem(string prodID, string prodName, string chanID, string chanName)
        //{
        //    this.ProductID = prodID;
        //    this.ProductName = prodName;
        //    this.ChannelId = chanID;
        //    this.ChannelName = chanName;
        //}

        //public override int GetHashCode()
        //{
        //    return this.ProductID.GetHashCode() + this.ChannelId.GetHashCode() +
        //            this.FromDateCalId.GetHashCode() + this.ToDateCalId.GetHashCode() +
        //            this.KitSite.GetHashCode() + this.BoxSite.GetHashCode() + this.MergeSite.GetHashCode() + this.BoxSplit.GetHashCode() + this.MergeSplit.GetHashCode();
        //}

        //public override bool Equals(object obj)
        //{
        //    if (obj == null) return false;

        //    if (obj.GetType().Equals(this.GetType()))
        //    {
        //        SourcingRuleLineItem objComp = (SourcingRuleLineItem)obj;
        //        if (this.ProductID != null && this.ChannelId != null && this.FromDateCalId != null && this.ToDateCalId != null && this.KitSite != null && this.BoxSite != null && this.MergeSite != null
        //            && objComp.ProductID != null && objComp.ChannelId != null && objComp.FromDateCalId != null && objComp.ToDateCalId != null && objComp.KitSite != null && objComp.BoxSite != null && objComp.MergeSite != null
        //            && this.ProductID.Equals(objComp.ProductID) && this.ChannelId.Equals(objComp.ChannelId) && this.FromDateCalId.Equals(objComp.FromDateCalId) && this.ToDateCalId.Equals(objComp.ToDateCalId)
        //            && this.KitSite.Equals(objComp.KitSite) && this.BoxSite.Equals(objComp.BoxSite) && this.MergeSite.Equals(objComp.MergeSite))
        //            return true;
        //        else
        //            return false;
        //    }
        //    else
        //        return false;
        //}

        //public override string ToString()
        //{
        //    return string.Format("Prod: {0}, Chan: {1}, fromDate: {2}, toDate: {3}, kitSite: {4}, boxSite: {5}, mergeSIte: {6}, routeID: {7}, parentRouteID: {8}",
        //            new object[] { this.ProductName, this.ChannelName, this.FromDate.ToString(), this.ToDate.ToString(), this.KitSite, this.BoxSite, this.MergeSite, this.RouteID, this.ParentRouteID });
        //}

        #endregion
    }
}
