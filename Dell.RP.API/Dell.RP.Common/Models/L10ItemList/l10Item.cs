﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.L10ItemList
{
    public class l10Item
    {
        public string id { get; set; }
        public string description { get; set; }
        public string updatedBy { get; set; }
        public string l10Class { get; set; }
        public string lastModifiedBy { get; set; }
        public string lastModifiedDate { get; set; }
    }
}
