﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.Common.Models
{
    public class ErrorImport
    {
        public int ErrorId { get; set; }
        public string ErrorReason { get; set; }

        public string ErrorRecord { get; set; }
        public string ErrorDate { get; set; }
    }
}
