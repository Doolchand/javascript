﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class MainSiteDDLInputParams
    {
        public DropDown selectedCountry { get; set; }
        public string selectedGeoTreeNodeValue { get; set; }
        public DropDown selectedGeography { get; set; }
    }
}
