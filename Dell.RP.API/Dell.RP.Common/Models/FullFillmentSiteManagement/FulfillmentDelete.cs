﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class FulfillmentDelete
    {
        public List<FulfillmentDeleteInputParams> deleteParam { get; set; }
    }
}
