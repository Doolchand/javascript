﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class ChannelTreeData
    {
        public string AllChannelId { get; set; }
        public string AllChannel { get; set; }

        public string GroupId { get; set; }
        public string GroupName { get; set; }

        public string ChannelId { get; set; }
        public string ChannelName { get; set; }

        public string BaselId { get; set; }
        public string BaseName { get; set; }
    }
}
