﻿
namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class Family
    {
        public string FamilyName { get; set; }
        public string FamilyId { get; set; }
    }
}
