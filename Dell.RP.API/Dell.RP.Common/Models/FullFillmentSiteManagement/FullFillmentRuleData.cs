﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class FullFillmentRuleData
    {

    public string Geography{ get; set; }
    public string Channel{ get; set; }
    public string SupplyChainType{ get; set; }
    public string[] AllSites { get; set; }
    public string product_id{ get; set; }
    public string item1{ get; set; }
    public string Routing{ get; set; }
    public string Split{ get; set; }
    public string sites{ get; set; }
    public string ship_via{ get; set; }
    public string sys_source{ get; set; }
    public string sys_created_by{ get; set; }
    public string sys_creation_date{ get; set; }
    public string last_modified_by{ get; set; }
    public string last_modified_date{ get; set; }
    public string sys_Ent_State{ get; set; }
    public string producttype{ get; set; }
    public string geoLevel{ get; set; }
    public string channelLevel{ get; set; }
    public string accountName{ get; set; }
    public string accountId{ get; set; }
    public string effectiveBeginDate{ get; set; }
    public string effectiveEndDate{ get; set; }

    }
}
