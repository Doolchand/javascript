﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class FulfillmentDetail
    {
        //public string Id { get; set; }
                public string RoutingId { get; set; }
                public string ProductId                {get;set;}
                public string Item                     {get;set;}
                public string EffectiveStartDate       {get;set;}
                public string EffectiveEndDate         {get;set;}
                public string ProductFga               {get;set;}
                public string ChannelId                {get;set;}
                public string ChannelName              {get;set;}
                public string GeographyId              {get;set;}
                public string GeographyName            {get;set;}
                public string Ssc                      {get;set;}
                public string ShipVia                  {get;set;}
                public string AccountName              {get;set;}
                public string AccountId                {get;set;}
                public string SysLastModifiedBy        {get;set;}
                public string SysLastModifiedDate      {get;set;}
                public string ChannelLevel             {get;set;}
                public string GeographyLevel           {get;set;}
                public string Id                       {get;set;}
                public string Flag                     {get;set;}
                public string Ps                       {get;set;}
                //public string ProductFga { get; set; }
    }
}
