﻿namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class ItemType
    {
        public string ItemTypeName { get; set; }
        public string ItemTypeId { get; set; }

    }
}
