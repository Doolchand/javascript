﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class HierarchyLevel
    {
        public string LevelName { get; set; }
        public string LevelId { get; set; }
    }
}
