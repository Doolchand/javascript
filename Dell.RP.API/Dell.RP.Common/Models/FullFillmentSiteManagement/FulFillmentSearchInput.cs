﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class FulFillmentSearchInput
    {
        public string Channelid { get; set; }
        public string Geoid { get; set; }
        public string GeoType { get; set; }
        public string ChannelType { get; set; }
        public string Lookupproduct { get; set; }
        public string StrProductId { get; set; }
        public string Item { get; set; }
        public string ItemType { get; set; }
        public string Ssc { get; set; }
        public string ShipVia { get; set; }
        public bool UseShipVia { get; set; }
        public string StrsearcAccountID { get; set; }
        public string effectiveStartDate { get; set; }
        public string effectiveEndDate { get; set; }
    }
}
