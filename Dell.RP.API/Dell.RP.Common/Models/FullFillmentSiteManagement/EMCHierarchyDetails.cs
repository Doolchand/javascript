﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class EMCHierarchyDetails
    {
        public string Lob_Id { get; set; }
        public string Lob_Desc { get; set; }
        public string Brand_Id { get; set; }
        public string Brand_Desc { get; set; }
        public string Family_Parent_Id { get; set; }
        public string Family_Parent_Desc { get; set; }
        public string Family_Id { get; set; }
        public string Family_Desc { get; set; }
        public string Base_Id { get; set; }
        public string Base_Desc { get; set; }
        public string Item_Type { get; set; }
        public string UpdatedBY { get; set; }
        public string UpdatedDate { get; set; }
    }

    public class LobDetails
    {
        public string Lob_Id { get; set; }
    }

    public class CreateMemberDetails
    {
        public string MapID { get; set; }
        public string Parent_Member { get; set; }
        public string Base_Id { get; set; }
        public string Base_Desc { get; set; }
        public string Item_Type { get; set; }
        public string UpdatedBY { get; set; }
        public string UpdatedDate { get; set; }
    }
}
