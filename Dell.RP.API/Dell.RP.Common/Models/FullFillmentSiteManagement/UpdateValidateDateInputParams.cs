﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class UpdateValidateDateInputParams
    {
        public string routing { get; set; }
        public string strEffectiveBeginDate { get; set; }
        public string strEffectiveEndDate { get; set; }
    }
}
