﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class FhcTreeData
    {
        public string AllFHC { get; set; }
        public string AllFHCId { get; set; }

        public string FHC { get; set; }
        public string FHCId { get; set; }

        public string Fga { get; set; }
        public string FgaId { get; set; }
    }
}
