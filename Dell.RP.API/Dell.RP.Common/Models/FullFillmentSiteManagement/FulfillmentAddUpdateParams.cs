﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class FulfillmentAddUpdateParams
    {
        public FulFillmentSearchInput objAddUpdateParams { get; set; }

        public List<FulfillmentRuleDetails> objAddUpdateRuleData { get; set; }

        public FulfillmentRuleDetails objselectedRow { get; set; }

        public bool fulfillUpdateMode { get; set; }
        public string strIntermRoutingID { get; set; }
        public string typeValue { get; set; }

        public string longtick { get; set; }
        public string sysSource { get; set; }
        public string strUserName { get; set; }

        public string strAccountName { get; set; }
        public string strSysEnt { get; set; }
    }
}