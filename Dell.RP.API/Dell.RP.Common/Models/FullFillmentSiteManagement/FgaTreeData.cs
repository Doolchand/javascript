﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class FgaTreeData
    {
        public string AllFga { get; set; }
        public string AllFgaId { get; set; }

        public string Fga { get; set; }
        public string FgaId { get; set; }
    }
}
