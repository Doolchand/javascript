﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class FulfillmentRuleDetails
    {
        public string fulfillmentSiteId { get; set; }
        public string effectiveBeginDate { get; set; }
        public string effectiveEndDate { get; set; }
        public string beginDateValue { get; set; }
        public string endDateValue { get; set; }
        public string hiddenRoutingId { get; set; }
        public string hiddlenFlag { get; set; }
    }
}