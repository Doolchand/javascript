﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.FullFillmentSiteManagement
{
    public class GeoTreeData
    {
        public string AllRegion { get; set; }

        public string AllRegionId { get; set; }
        public string Region { get; set; }

        public string RegionId { get; set; }
        public string SubRegion { get; set; }
        public string SubRegionId { get; set; }

        public string Area { get; set; }
        public string AreaId { get; set; }

        public string Country { get; set; }
        public string CountryId { get; set; }
    }

}
