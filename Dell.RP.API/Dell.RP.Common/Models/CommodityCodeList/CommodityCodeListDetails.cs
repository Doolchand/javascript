﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.CommodityCodeListDetails
{
    public class CommodityCodeListDetails
    {
        public string commodityId { get; set; }
        public string commodityName { get; set; }
        public bool markFlag { get; set; }
        public string updatedBy { get; set; }
        public string updatedDate { get; set; }
    }
}
