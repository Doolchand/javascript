﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models
{
    public class DropDown
    {
        public string ItemName { get; set; }
        public string Id { get; set; }
    }

    public class MasterData
    {
        public string ItemName { get; set; }
        public string Id { get; set; }
        public string Key { get; set; } 
    }
}
