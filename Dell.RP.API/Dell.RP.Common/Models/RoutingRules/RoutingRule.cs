﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.RoutingRules
{
    public class RoutingRule
    {
       
        private string supplyChainType; // Use Enum Values out of Constants/SupplyChainType.cs
        private string routing_effective_startDate;
        private string routing_effective_endDate;
        private string mfg_effective_startDate;
        private string mfg_effective_endDate;
        private string splitPercent;

        /// <summary>
        /// 
        /// </summary>
        public string Mfg_effective_startDate
        {
            get { return mfg_effective_startDate; }
            set { mfg_effective_startDate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Mfg_effective_endDate
        {
            get { return mfg_effective_endDate; }
            set { mfg_effective_endDate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string SplitPercent
        {
            get { return splitPercent; }
            set { splitPercent = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<RoutingRuleLines> RoutingRuleLinesList
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string RoutingId
        {
            get; set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Type
        {
            get; set;
        }

        /// <summary>
        /// 
        /// </summary>
      

        /// <summary>
        /// 
        /// </summary>
       
        /// <summary>
        /// 
        /// </summary>
       

        /// <summary>
        /// 
        /// </summary>
        public string SupplyChainType
        {
            get { return supplyChainType; }
            set { supplyChainType = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Routing_effective_startDate
        {
            get { return routing_effective_startDate; }
            set { routing_effective_startDate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Routing_effective_endDate
        {
            get { return routing_effective_endDate; }
            set { routing_effective_endDate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string FgaItem { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        /// <param name="channel"></param>
        /// <param name="geography"></param>
        /// <param name="supplyChainType"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        //public RoutingRule(Product product, Channel channel, Geography geography, string supplyChainType, string routing_effective_startDate, string routing_effective_endDate, string mfg_effective_startDate, string mfg_effective_endDate, string splitPercent)
        //{
        //    this.product = product;
        //    this.channel = channel;
        //    this.geography = geography;
        //    this.supplyChainType = supplyChainType;
        //    this.routing_effective_startDate = routing_effective_startDate;
        //    this.routing_effective_endDate = routing_effective_endDate;
        //    this.mfg_effective_startDate = mfg_effective_startDate;
        //    this.mfg_effective_endDate = mfg_effective_endDate;
        //    this.splitPercent = splitPercent;
        //}

        /// <summary>
        /// 
        /// </summary>
        private string ruleId;

        /// <summary>
        /// 
        /// </summary>
        public string RuleId
        {
            get { return ruleId; }
            set { ruleId = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.ruleId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return ((RoutingRule)obj).RuleId.Equals(this.ruleId);
        }

        /// <summary>
        /// 
        /// </summary>
        public RoutingRuleLines[] routingRuleLines;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="routingId"></param>
        /// <param name="productId"></param>
        /// <param name="fgaItem"></param>
        /// <param name="type"></param>
        public RoutingRule(string routingId, string productId, string fgaItem, string type, string mfg_effectiveStartDate, string mfg_effectiveEndDate)
        {
            RoutingId = routingId;
            ProductId = productId;
            FgaItem = fgaItem;
            Type = type;
            mfg_effective_startDate = mfg_effectiveStartDate;
            mfg_effective_endDate = mfg_effectiveEndDate;
            RoutingRuleLinesList = new List<RoutingRuleLines>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="routingRuleLines"></param>
        /// <returns></returns>
        public bool addRoutingRuleLines(RoutingRuleLines routingRuleLines)
        {
            this.RoutingRuleLinesList.Add(routingRuleLines);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.RoutingId.GetHashCode();
        }

    }

}
 
