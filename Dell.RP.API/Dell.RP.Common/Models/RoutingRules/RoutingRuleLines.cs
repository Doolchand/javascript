﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.RoutingRules
{
    public class RoutingRuleLines
    {

        public string ID { get; set; }
        public string RoutingId { get; set; }
        public string FulfillmentSite { get; set; }
        public string ManufacturingSite { get; set; }
        public string PrimaryRouting { get; set; }
        public string AlternateRouting1 { get; set; }
        public string AlternateRouting2 { get; set; }
        public string Status { get; set; }
        public string Routing_StartDate { get; set; }
        public string Routing_EndDate { get; set; }
        public string MfgSplitPercent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public RoutingRuleLines()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="routingId"></param>
        /// <param name="fulfillmentSite"></param>
        /// <param name="manufacturingSite"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="primaryRouting"></param>
        /// <param name="alternateRouting1"></param>
        /// <param name="alternateRouting2"></param>
        /// <param name="status"></param>
        public RoutingRuleLines(string id, string routingId, string fulfillmentSite, string manufacturingSite, string routingStartDate, string routingEndDate, string primaryRouting, string alternateRouting1, string alternateRouting2, string status, string splitPercent)
        {
            ID = id;
            RoutingId = routingId;
            FulfillmentSite = fulfillmentSite;
            ManufacturingSite = manufacturingSite;
            PrimaryRouting = primaryRouting;
            AlternateRouting1 = alternateRouting1;
            AlternateRouting2 = alternateRouting2;
            Routing_StartDate = routingStartDate;
            Routing_EndDate = routingEndDate;
            Status = status;
            MfgSplitPercent = splitPercent;
        }
    }

}
