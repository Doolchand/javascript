﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.RoutingRules
{
    public class RoutingRulesMainGrid
    {

        public string ROUTING_ID { get; set; }
        public string PRODUCTFGA { get; set; }

        public string FLAG { get; set; }
        public string PRODUCT_ID { get; set; }
        public string ITEM { get; set; }
        public string TYPE { get; set; }
        public string FULFILLMENT_SITE_ID { get; set; }
        public string MANUFACTURING_SITE_ID { get; set; }
        public string PRIMARY_ROUTING { get; set; }
        public string ALTERNATE_ROUTING_1 { get; set; }
        public string ALTERNATE_ROUTING_2 { get; set; }
        public string ROUTING_EFFECTIVE_START_DATE { get; set; }

        public string ROUTING_EFFECTIVE_END_DATE { get; set; }
        public string MFG_EFFECTIVE_START_DATE { get; set; }
        public string MFG_EFFECTIVE_END_DATE { get; set; }
        public string MFG_SITE_SPLIT_PERCENT { get; set; }
        public string SYS_LAST_MODIFIED_BY { get; set; }
        public string SYS_LAST_MODIFIED_DATE { get; set; }
       
    }
}
