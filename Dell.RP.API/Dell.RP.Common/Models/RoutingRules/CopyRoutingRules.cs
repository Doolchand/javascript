﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.RoutingRules
{
    public class CopyRoutingRules
    {

        public string hfItemID { get; set; }
        public string hfFHCType { get; set; }
        public string ddlFGA { get; set; }
        public string routingId { get; set; }
        public string Product { get; set; }
    }
}
