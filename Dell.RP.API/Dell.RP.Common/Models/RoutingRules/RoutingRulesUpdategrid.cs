﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.RoutingRules
{
    public class RoutingRulesUpdategrid
    {

        public string Alternate_Routing1 { get; set; }
        public string Alternate_Routing2 { get; set; }
        public string Mfg_Site_Effective_Begin_Date { get; set; }
        public string Mfg_Site_Effective_End_Date { get; set; }



        public string Routing_Effective_End_Date { get; set; }
        public string Routing_Effective_Start_Date { get; set; }


        public string TreeValue { get; set; }
        
        public string fulfillmenT_SITE_ID { get; set; }
        public string manufacturinG_SITE_ID { get; set; }
        public string mfG_SITE_SPLIT_PERCENT { get; set; }
        public string primarY_ROUTING_DESC { get; set; }

        public string product { get; set; }





        public string strMFGSiteName { get; set; }
        public string strMFGBeginDate { get; set; }
        public string strMFGEndDate { get; set; }
        public string strFFSite { get; set; }

        public string strNewRoutingBeginDate { get; set; }
        public string strNewRoutingEndDate { get; set; }

        public string id { get; set; }

        public string strStatus { get; set; }
        public string strRoutingIdRule { get; set; }
        public string strRoutingIDGrid { get; set; }
        public string hdfTypeRetrieve { get; set; }
        public string primarY_ROUTING { get; set; }
        public string Alternate_Routing1_des { get; set; }
        public string Alternate_Routing2_des { get; set; }
        public string ROUTING_ID { get; set; }
        
    }
}
