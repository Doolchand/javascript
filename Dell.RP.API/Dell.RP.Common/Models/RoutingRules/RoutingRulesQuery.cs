﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.RoutingRules
{
    public class RoutingRulesQuery
    {

        public string Select { get; set; }
        public string Product_Item { get; set; }
        public string Mfg_Site_Effective_Begin_Date { get; set; }
        public string Mfg_Site_Effective_End_Date { get; set; }
        public string Manufacturing_Site { get; set; }
        public string Fulfillment_Site { get; set; }
        public string Split_Percentage { get; set; }
        public string Routing_Effective_Start_Date { get; set; }
        public string Routing_Effective_End_Date { get; set; }
        public string Primary_Routing { get; set; }
        public string Alternate_Routing1 { get; set; }
        public string Alternate_Routing2 { get; set; }
        public string Last_Modified_By { get; set; }
        public string Last_Modified_Date { get; set; }
        
    }
}
