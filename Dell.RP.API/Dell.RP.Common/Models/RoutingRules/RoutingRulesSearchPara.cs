﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.RoutingRules
{
    public class RoutingRulesSearchPara
    {
        public string Product { get; set; }
        public string Geo { get; set; }
        public string Fhctree { get; set; }

        public string hdfTypeSearch { get; set; }
        public string lblproduct { get; set; }

        public string hdfRetrieve { get; set; }
    }
}
