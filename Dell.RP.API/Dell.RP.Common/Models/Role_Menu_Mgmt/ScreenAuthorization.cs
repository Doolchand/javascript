﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.Common.Models.Role_Menu_Mgmt
{
    public class ScreenAuthorization
    {
        public string FunctionId { get; set; }
        public string FunctionName { get; set; }
        public string RoleId { get; set; }

    }
}
