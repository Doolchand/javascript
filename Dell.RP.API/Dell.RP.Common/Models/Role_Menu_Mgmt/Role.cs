﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.Common.Models.Role_Menu_Mgmt
{
   public class Role
    {
        //public string UserId { get; set; }
        public int RoleId { get; set; }

        public string RoleName { get; set; }
        public string UserType { get; set; }
    }
}
