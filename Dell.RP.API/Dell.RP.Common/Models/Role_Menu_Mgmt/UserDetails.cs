﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.Common.Models.Role_Menu_Mgmt
{
    public class UserDetails
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserDomain { get; set; }
        public string UserType { get; set; }
        public string Region { get; set; }
        public string UserEmail { get; set; }
        public string UserRole { get; set; }
        public string UserGroup { get; set; }
        public bool IsActive { get; set; }
        public string Lobs { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }

    }
    public class LoggedUserDetails
    {
        public string UserId { get; set; }      
    }
    
}
