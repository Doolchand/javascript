﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.Common.Models.Role_Menu_Mgmt
{
    public class FilterDetails
    {
        public List<Dropdown> Region { set; get; }
        public List<Dropdown> UserName { set; get; }
        public List<Dropdown> UserGroup { set; get; }
        public string UserType { get; set; }
        public string IsActive { set; get; }
    }
    public class Dropdown
    {
        public string id { get; set; }
        public string itemName { get; set; }
    }
}
