﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.Common.Models.Role_Menu_Mgmt
{
    /// <summary>
    /// Roles who can access UI screens
    /// </summary>
    public class ScreenAccessRole
    {
        public string ScreenId { get; set; }
        public string Roles { get; set; }

    }
}
