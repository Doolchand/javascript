﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.Common.Models.Role_Menu_Mgmt
{
    public class Menu
    {
        public int MenuItem { get; set; }
        public int ParentMenuItem { get; set; }
        public string MenuItemName { get; set; }
        public string MenuItemDesc { get; set; }

        public int IsAccessAllow { get; set; }
    }
}
