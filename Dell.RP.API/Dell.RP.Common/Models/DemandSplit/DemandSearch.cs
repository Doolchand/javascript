﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.DemandSplit
{
    public class DemandSearch
    {
        public string Product { get; set; }
        public string Chennal { get; set; }
        public string Geography { get; set; }
    }
}
