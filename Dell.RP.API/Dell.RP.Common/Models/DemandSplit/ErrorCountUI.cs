﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.DemandSplit
{
    public class ErrocountUI
    {
        public string Totalrow { get; set; }
        public int Totalfailed { get; set; }
        public int CorrectRecords { get; set; }
        public string successmsg { get; set; }
        public int Totalsuberror { get; set; }
        public string ErrorMessage { get; set; }

        public List<DemandSplitErrorgrid> demandSplitErrorgrids { get; set; }
    }
}
