﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.DemandSplit
{
    public class SearchQuery
    {
        public string Product_ID { get; set; }
        public string Channel_ID { get; set; }
        public string Geography_ID { get; set; }
        public string SSC { get; set; }
        public string WK0 { get; set; }
        public string WK1 { get; set; }
        public string WK2 { get; set; }
        public string WK3 { get; set; }
        public string WK4 { get; set; }
        public string WK5 { get; set; }
        public string WK6 { get; set; }
        public string WK7 { get; set; }
        public string WK8 { get; set; }
        public string WK9 { get; set; }
        public string WK10 { get; set; }
        public string WK11 { get; set; }
        public string WK12 { get; set; }
        public string WK13 { get; set; }
        public string WK14 { get; set; }
        public string WK15 { get; set; }
        public string WK16 { get; set; }
        public string WK17 { get; set; }
        public string WK18 { get; set; }
        public string WK19 { get; set; }
        public string WK20 { get; set; }
        public string WK21 { get; set; }
        public string WK22 { get; set; }
        public string WK23 { get; set; }
        public string WK24 { get; set; }
        public string WK25 { get; set; }
        public string WK26 { get; set; }
        public string Rest_Of_Horizon { get; set; }
        public string Sys_last_Modified_By{ get; set; }  // SYS_LAST_MODIFIED_BY
        public string Sys_last_Modified_Date { get; set; }//SYS_LAST_MODIFIED_DATE

    }
}
