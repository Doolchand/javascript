﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.DemandSplit
{
    public class DemandSplitErrorgrid
    {
        public string rowrumber { get; set; }

        public string errorcolumn { get; set; }
        public string error { get; set; }

      
    }
}
