﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SupplyPlanningFhc
{
    public class Export_Parameters
    {
        public string ReviewWeeks { get; set; }
        public string FhcID { get; set; }
        public string FamilyParent { get; set; }
        public string ManSite { get; set; }
        public string Region { get; set; }
    }
}
