﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SupplyPlanningFhc
{
    public class Error_Grid_Details
    {
        public string ItemID { get; set; }
        public string FromSite { get; set; }
        public string Geo { get; set; }
        public string Qty { get; set; }
        public string ShipDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ErrDesc { get; set; }
    }
}
