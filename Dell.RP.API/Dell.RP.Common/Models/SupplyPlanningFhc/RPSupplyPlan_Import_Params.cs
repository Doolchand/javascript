﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SupplyPlanningFhc
{
    public class RPSupplyPlan_Import_Params
    {
        public string item_id { get; set; }
        public string from_site { get; set; }
        public string geo { get; set; }
        public string updated_qty { get; set; }
        public string ship_date { get; set; }
        public string sys_last_modified_by { get; set; }
    }
}
