﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dell.RP.API.Dell.RP.Common.Models.SupplyPlanningFhc
{
    public class Get_SupplyPlanningFhc_Export_Details
    {
        public string FhcId { get; set; }
        public string FamParCode { get; set; }
        public string FamilyParent { get; set; }
        public string ManSite { get; set; }
        public string DemandGeo { get; set; }
        public string Region { get; set; }
        public string SscType { get; set; }
        public string ShipMode { get; set; }
        public string ShipToSite { get; set; }
        public string ShipDate { get; set; }
        public string DeliveryDate { get; set; }
        public string OrginialQty { get; set; }
        public string UpdatedQty { get; set; }
        public string UpdatedBy { get; set; }
    }
}
