﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.Common.Utility
{
    public interface ICommonUtility
    {
        void WriteLogInformation(string info1, string info2);
        void DeleteUnUsedFiles(string dirPath, int timeDurationDays);
        string GetAppRootPath();
        string GetExceptionFolderName();
        int GetDeleteFileDurationDaysForExceptionReports();
       

    }
}
