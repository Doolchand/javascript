﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Extensions.Options;

namespace Dell.RP.Common.Utility
{
    public class CommonUtility : ICommonUtility
    {
        private readonly string appRootPath;
        private readonly bool isLogEnabled;
        private readonly string logFolderName;
        private readonly int deleteFileDurationDaysForLog;
        private readonly string exceptionFolderName;
        private readonly int deleteFileDurationDaysForExceptionReports;
        public CommonUtility(IOptions<CommonAppConfig> appSettings)
        {
            //connenctionString = appSettings.Value.ConnectionString;
            appRootPath = appSettings.Value.AppRootPath;
            isLogEnabled = Convert.ToBoolean(appSettings.Value.IsLogEnable);
            logFolderName = appSettings.Value.LogFolderName;
            deleteFileDurationDaysForLog = Convert.ToInt32(appSettings.Value.DeleteFileDurationDaysForLog);
            exceptionFolderName = appSettings.Value.ExceptionReportFolderName;
            deleteFileDurationDaysForExceptionReports = Convert.ToInt32(appSettings.Value.DeleteFileDurationDaysForExceptionReport);

        }
        //public void WriteLogInformation(string filename, string info1, string info2)
        public void WriteLogInformation(string info1, string info2)
        {
            //parameters needs to get from common configurarion : toDo
            //bool isLogEnable = true;
            if (isLogEnabled == true)
            {
                //parameters needs to get from common configurarion : toDo
                //string appRootPath = GetAppRootPath();
               // int deleteLogDurationDays = -2;
               // string LogFolderName = "XML_Log\\";

                string logDirectoryPath = appRootPath + logFolderName;
                
                //if directory not exist then create directory
                if (!Directory.Exists(logDirectoryPath))
                {
                    Directory.CreateDirectory(logDirectoryPath);
                }

                //Delete unnecessory files base on given time duration
                DeleteUnUsedFiles(logDirectoryPath, deleteFileDurationDaysForLog);



                string logFileName = "xml_" + DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year + ".xml";

                string logFilePath = appRootPath + logFolderName + logFileName;


                //ConfigurationSettings.GetConfig("abc");
                StringBuilder sbuilder = new StringBuilder();
                using (StringWriter sw = new StringWriter(sbuilder))
                {
                    using (XmlTextWriter w = new XmlTextWriter(sw))
                    {
                        w.WriteStartElement("LogInfo");
                        w.WriteElementString("Time", DateTime.Now.ToString());
                        w.WriteElementString("Info1", info1);
                        w.WriteElementString("Info2", info2);
                        w.WriteEndElement();
                    }
                }
                using (StreamWriter w = new StreamWriter(logFilePath, true, Encoding.UTF8))
                {
                    w.WriteLine(sbuilder.ToString());
                }
            }
        }

        public void DeleteUnUsedFiles(string dirPath, int timeDurationDays)
        {
            //string[] files = Directory.GetFiles(dirPath);
            string[] files = Directory.GetFiles(dirPath, "*.*", SearchOption.AllDirectories);

            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);
                if (fi.LastAccessTime < DateTime.Now.AddDays(timeDurationDays))
                    fi.Delete();
            }
        }

        public string GetAppRootPath()
        {
            return appRootPath;
        }

        public string GetExceptionFolderName()
        {
            return exceptionFolderName;
        }
       
        public int GetDeleteFileDurationDaysForExceptionReports()
        {
            return deleteFileDurationDaysForExceptionReports;
        }
    }
}
