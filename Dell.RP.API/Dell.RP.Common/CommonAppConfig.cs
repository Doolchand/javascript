﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dell.RP.Common
{
    public class CommonAppConfig
    {
        public String AppRootPath { get; set; }

        public String IsLogEnable { get; set; }

        public String DeleteFileDurationDaysForLog { get; set; }

        public String LogFolderName { get; set; }

        public String DeleteFileDurationDaysForExceptionReport { get; set; }

        public String ExceptionReportFolderName { get; set; }
    }
}
