﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.DirectoryServices;

namespace Dell.RP.API
{
    public class IsUserInRole
    {

        public static bool IsInGroup1(string groupName)
        {
            var myIdentity = GetUserIdWithDomain();
            var myPrincipal = new WindowsPrincipal(myIdentity);
            /// Extra code
            var identity = WindowsIdentity.GetCurrent();
            List<string> GetAllRoles = new List<string>();

            foreach (var groupId in identity.Groups)
            {
                GetAllRoles.Add(groupId.Translate(typeof(NTAccount)).Value);

            }
            return  myPrincipal.IsInRole(groupName);
        }

        public static bool IsInGroup(string groupName)
        {
            var myIdentity = GetUserIdWithDomain();
            var myPrincipal = new WindowsPrincipal(myIdentity);
            /// Extra code
            var identity = WindowsIdentity.GetCurrent();
            List<string> GetAllRoles = new List<string>();
           
            foreach (var groupId in identity.Groups)
            {
               GetAllRoles.Add( groupId.Translate(typeof(NTAccount)).Value);
              
            }
            string domain = "ASIA-PACIFIC";
            string name = "SANDEEP_RASTOGI";
            ///
            using (var entry = new DirectoryEntry($"LDAP://" +domain ))
            {
                using (var searcher = new DirectorySearcher(entry))
                {
                    searcher.Filter = $"(sAMAccountName=" + name +")";
                    searcher.PropertiesToLoad.Add("displayName");
                    var searchResult = searcher.FindOne();

                    if (searchResult != null && searchResult.Properties.Contains("displayName"))
                    {
                        var displayName = searchResult.Properties["displayName"][0];
                    }
                    else
                    {
                        // user not found
                    }
                }
            }

            return myPrincipal.IsInRole(groupName);
        }

        public bool IsInGroup(List<string> groupNames)
        {
            var myIdentity = GetUserIdWithDomain();
            var myPrincipal = new WindowsPrincipal(myIdentity);

            return groupNames.Any(group => myPrincipal.IsInRole(group));
        }

        public static WindowsIdentity GetUserIdWithDomain()
        {
            var myIdentity = WindowsIdentity.GetCurrent();
            return myIdentity;
        }

        public static string GetUserId()
        {
            var id = GetUserIdWithDomain().Name.Split('\\');
            return id[1];
        }

        //public static string GetUserDisplayName()
        //{
        //    var id = GetUserIdWithDomain().Name.Split('\\');

        //    var dc = new PrincipalContext(ContextType.Domain, id[0]);
        //    var adUser = UserPrincipal.FindByIdentity(dc, id[1]);
        //    return adUser.DisplayName;

        //}


        //https://msdn.microsoft.com/en-us/library/ms676310(v=vs.85).aspx
        public static void AddMemberToGroup( )
        {
            // string bindString = "LDAP://ouddev.us.dell.com:389:636/CN=E2OPEN_MRP_ADMIN,DC=apac,DC=DELL,DC=com";
            string bindString = "LDAP://DC=apac,DC=dell,DC=com";
            string newMember = "CN = S,Jayakanth,OU = TestOU,DC = fabrikam,DC = com";

            DirectoryEntry deRoot = new DirectoryEntry("LDAP://RootDSE");

            if (deRoot != null)
            {
                string defaultNamingContext = deRoot.Properties["defaultNamingContext"].Value.ToString();
            }

            try
            {
                DirectoryEntry entry = new DirectoryEntry(bindString);
                DirectorySearcher search = new DirectorySearcher(entry);

                List<string> s = new List<string>();
                entry.Properties["member"].Add(newMember);
                foreach (PropertyValueCollection item in entry.Properties)
                {
                    s.Add(item.Value.ToString());
                }

                entry.CommitChanges();

            }

            catch (Exception e)
            {

                //Console.WriteLine("An error occurred.");

                //Console.WriteLine("{0}", e.Message);

                return;

            }

        }



    }

}
