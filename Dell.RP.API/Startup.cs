﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;
using Dell.RP.DataAccess;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Authentication;
using Dell.RP.BussinessServices.IServiceInterceptor.Role_Menu_Mgmt;
using Dell.RP.BussinessServices.ServiceInterceptor.Role_Menu_Mgmt;
using Dell.RP.DataAccess.IRepository.Role_Menu_Mgmt;
using Dell.RP.DataAccess.Repository.Role_Menu_Mgmt;
using Dell.RP.DataAccess.IRepository.ErrorLogging;
using Dell.RP.DataAccess.Repository.ErrorLogging;
using Dell.RP.BussinessServices.IServiceInterceptor.ErrorLogging;
using Dell.RP.BussinessServices.ServiceInterceptor.ErrorLogging;
using Dell.RP.DataAccess.IRepository;
using Dell.RP.BussinessServices.IServiceInterceptor;
using Dell.RP.DataAccess.Repository;
using Dell.RP.BussinessServices.ServiceInterceptor;
using Dell.RP.Common.Utility;
using Dell.RP.Common;
using Microsoft.AspNetCore.Server.IISIntegration;
using Dell.RP.BussinessServices.IServiceInterceptor.Utility;
using Dell.RP.BussinessServices.ServiceInterceptor.Utility;
using Dell.RP.DataAccess.IRepository.Utility;
using Dell.RP.DataAccess.Repository.Utility;
using Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.DataAccess.Repository.FullFillmentSiteManagement;
using Dell.RP.API.Dell.RP.DataAccess.Common;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.DemandSplit;
using Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.DemandSplit;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.DemandSplit;
using Dell.RP.API.Dell.RP.DataAccess.Repository.DemandSplit;

using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.MultiTierBOM;
using Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.MultiTierBOM;
using Dell.RP.API.Dell.RP.DataAccess.Repository.MultiTierBOM;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.MultiTierBOM;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.CommodityCodeList;
using Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.CommodityCodeList;
using Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.L10ItemList;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.L10ItemList;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.CommodityCodeList;
using Dell.RP.API.Dell.RP.DataAccess.Repository.CommodityCodeList;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.L10ItemList;
using Dell.RP.API.Dell.RP.DataAccess.Repository.L10ItemList;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.RoutingRules;
using Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.RoutingRules;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.RoutingRules;
using Dell.RP.API.Dell.RP.DataAccess.Repository.RoutingRules;
using Dell.RP.API.Dell.RP.DataAccess.Repository.SourcingRule1;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.SourcingRule1;
using Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.SourcingRule1;
using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.SourcingRule1;

using Dell.RP.API.Dell.RP.BussinessServices.IServiceInterceptor.SupplyPlanningFhc;
using Dell.RP.API.Dell.RP.BussinessServices.ServiceInterceptor.SupplyPlanningFhc;
using Dell.RP.API.Dell.RP.DataAccess.IRepository.SupplyPlanningFhc;
using Dell.RP.API.Dell.RP.DataAccess.Repository.SupplyPlanningFhc;

namespace Dell.RP.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

//remotevalue has been  change


        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc().AddJsonOptions(x => x.SerializerSettings.Formatting = Formatting.Indented);
            services.AddMvc().AddJsonOptions(x => x.SerializerSettings.NullValueHandling = NullValueHandling.Ignore);
            services.AddMvc().AddJsonOptions(x => x.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());
            //Add cors 
            string withOrigins = Configuration["WithOrigins"].ToString();
            services.AddCors(
               options => options.AddPolicy("AllowCors",
               builder =>
               {
                   builder
                   .WithOrigins(withOrigins)
                   .WithMethods("GET", "PUT", "POST", "DELETE")
                   .AllowAnyHeader()
                   .AllowAnyMethod()
                   .AllowCredentials();
               })
               );
            services.AddAuthorization(options =>
            {
                options.AddPolicy("AllUsers", policy => policy.RequireAuthenticatedUser());
            });
            // Adds a default in-memory implementation of IDistributedCache.
            services.AddDistributedMemoryCache();
            // Added cookieBuilder to avoid warnings.
            CookieBuilder cookieBuilder = new CookieBuilder();
            cookieBuilder.HttpOnly = true;
            cookieBuilder.Name = ".ASPNetCoreSession";
            
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.Cookie = cookieBuilder;
                options.IdleTimeout = TimeSpan.FromMinutes(20);
            });
            services.AddScoped<IMenuService, MenuService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IAuthorizationService, AuthorizationService>();
            services.AddScoped<IErrorLoggingService, ErrorLoggingService>();
            services.AddScoped<IUtilityService, UtilityService>();
            services.AddScoped<IFullFillmentSiteManagement, FullFillmentSiteManagementService>();
			services.AddScoped<IMultiTierBOM, MultiTierBOMService>();
            services.AddScoped<ICommodityCodeList, CommodityCodeListService>();
            services.AddScoped<IL10ItemList, L10ItemListService>();
            services.AddScoped<ISourcingRuleService, SourcingRuleService>();
            services.AddScoped<ISupplyPlanningFhcService, SupplyPlanningFhcService>();

            services.AddScoped<IMenuRepositry, MenuRepositry>();
            services.AddScoped<IRoleRepositry, RoleRepositry>();
            services.AddScoped<IAuthorizationRepository, AuthorizationRepository>();
            services.AddScoped<IErrorLoggingRepository, ErrorLoggingRepository>();
            services.AddScoped<IUtilityRepository, UtilityRepository>();
            services.AddScoped<IFullFillmentSiteManagementRepository, FullFillmentSiteManagementRepository>();
            services.AddScoped<ISessionSettingRepository, SessionSettingRepository>();
			services.AddScoped<ICommodityCodeListRepository, CommodityCodeListRepository>();
            services.AddScoped<IMultiTierBOMRepository, MultiTierBOMRepository>();
            services.AddScoped<IL10ItemListRepository, L10ItemListRepository>();
            services.AddScoped<ISourcingRuleRepository, SourcingRuleRepository>();
            services.AddScoped<ISupplyPlanningFhcRepository, SupplyPlanningFhcRepository>();

            services.AddScoped<ISessionSettingService, SessionSettingService>();
            services.AddScoped<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IClaimsTransformation, ClaimsTransformer>();
            services.AddScoped<ICommonUtility, CommonUtility>();           
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.Configure<CommonAppConfig>(Configuration.GetSection("CommonAppConfig"));
            services.AddTransient<ICommonData, CommonData>();

            // AutomaticAuthentication used instead of ForwardWindowsAuthentication.


            services.AddScoped<IDemandSplit, DemandSplitService>();
            services.AddScoped<IDemandSplitRepository, DemandSplitRepository>();



            services.AddScoped<IRoutingRules, RoutingRulesService>();
            services.AddScoped<IRoutingRulesRepository, RoutingRulesRepository>();

            services.Configure<IISOptions>(options =>
            {
                // options.ForwardWindowsAuthentication = true;
                options.AutomaticAuthentication = true;
                options.ForwardClientCertificate = false;
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IClaimsTransformation, ClaimsTransformer>();
            // Require to add authentication in IIS
            services.AddAuthentication(IISDefaults.AuthenticationScheme);
            //services.AddAuthentication(OpenIdConnectDefaults.AuthenticationScheme);
            services.AddSingleton<IConfiguration>(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            DefaultFilesOptions options = new DefaultFilesOptions();
            options.DefaultFileNames.Clear();
            options.DefaultFileNames.Add("Index.html");
            app.UseSession();
            app.UseDefaultFiles(options);
            app.UseStaticFiles();
            app.UseCors("AllowCors");
            
            if (env.IsDevelopment())
            {
               // app.UseDeveloperExceptionPage();
            }
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}");
            });
            // This statement is used to enable authentication in Asp.Net Core 2.0
            app.UseAuthentication();

        }
    }
}
